How to contribute:        
-------------------------------------------------------------------------------
There is two different ways to contribute.
    1) By reporting defects 
    2) By donating code

Reporting Defects:
-------------------------------------------------------------------------------
There are several types of defects that are worth reporting, these include:
   * Programming Bugs.
    When reporting bugs please make sure you describe how to reproduce the problem.
    If possible/applicable please create a quick example that reproduces your issue.
   * Design issues.
    If you know of a better technique, or feel that something could be done in a 
    better way please let us know. Please provide as much data to backup your case 
    as possible.
   * Language issues.
    English issues, such grammar, spell checking, or better wording.
    When reporting these type of issues, please make sure that they are in important 
    sections such in the function/class/enum description section. Or symbol names 
    such variables, classes, filenames, etc. that are visible to users. Comments
    that lay inside function there for low visibility will be consider low priority.
   * Others
    Any other type of issue please report it as well. Just make sure you make yourself
    clear, it can be reproduce, etc.

Donating Code
-------------------------------------------------------------------------------
Please request a merge/pull request. However please note that all contributions
will be consider donations to the project, as such they must follow these
guidelines: 

   * That the code you are asking to merge belongs to you.
    You follow the coding standard.
    https://gitlab.com/LIONant/LIONV2/blob/master/LION/Documents/Coding%20Standard.docx
   * You must provide a unit-test for the code if applicable. 

Please note that the code you are donating to the project may be rejected by 
LIONant.
