//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include <random>
#include "gb_base.h"

#if _X_TARGET_WINDOWS
    #include <windows.h>
#endif

//---------------------------------------------------------------------------------
// BUILDING MY OWN ENTITY
//---------------------------------------------------------------------------------
class my_entity : public gb_component::entity
{
    x_object_type( my_entity, is_quantum_lock_free, rtti(gb_component::entity), is_not_copyable, is_not_movable )

public: // --- class traits ---
        
    struct t_descriptors : t_parent::t_descriptors
    {
        x_constexprvar auto         t_category_string               = X_USTR("Entity/Example04 Entity");
        x_constexprvar auto         t_type_guid                     = gb_component::type_base::guid{ "Example04 Entity" };
        x_constexprvar auto         t_help                          = X_STR("This is an example render component used for unit testing");
        using                       t_type                          = gb_component::type_entity_pool<t_self>;
        x_constexprvar def          t_definition                    = def::MASK_DEFAULT;                   
    };
            
protected: // --- class hierarchy types ---

    struct mutable_data : t_parent::mutable_data
    {
        void onUpdateMutableData( const t_mutable_base& Src ) noexcept override
        {
            const mutable_data& NewSrc = reinterpret_cast<const mutable_data&>(Src);
            // copy the hold thing
            *this = NewSrc;
        }
            
        float m_Velocity=0;
    };
        
protected: // --- class hierarchy functions ---
        
    my_entity( const base_construct_info& C ) noexcept : t_parent( C )
    {
        // Generate random number from 5 to 1000-1
        std::random_device                  rd;
        std::mt19937                        mt(rd());
        std::uniform_int_distribution<int>  dist(5, 1000 );
        m_Death = dist(mt);
    }
        
    virtual void onExecute( void ) noexcept override
    {
        x_assert_quantum( m_Debug_LQ );
        x_assert_linear( m_Debug_LQMsg );
            
        t_parent::onExecute();
            
        mutable_data& T1 = getT1Data<mutable_data>();
            
        T1.m_Velocity--;
        if( --m_Death == 0 )
        {
            msgDestroy();
            m_GInterface.m_GameMgr.CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ) ).msgAddToWorld();
        }
    }
        
protected:  // --- class hierarchy hidden variables ---
        
    int m_Death;
};


//----------------------------------------------------------------------------------------

void ScriptMain( gb_game_graph::base& Base )
{
    //
    // Register Managers
    //
    using entity_mgr = gb_component_mgr::base;

    xndptr_s<entity_mgr> EntityMgr;
    EntityMgr.New( entity_mgr::guid("EntityMgr"), Base, X_USTR("EntityMgr") );

    //
    // Register components
    //
    Base.RegisterComponentAndDependencies<my_entity>( EntityMgr, Base.m_EndSyncPoint );

    //
    // Give the manager officially to the DataBase
    //
    Base.m_CompManagerDB.RegisterAndTransferOwner( EntityMgr );
}
