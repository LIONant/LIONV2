//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "gb_base.h"

#include "UnitTests/x_unit_test_network.h"
#include "UnitTests/x_unit_test_property.h"
#include "UnitTests/x_unit_test_scheduler.h"
#include "UnitTests/x_unit_test_scheduler_jobs.h"
#include "UnitTests/x_unit_test_qt_queue.h"
#include "UnitTests/x_unit_test_string.h"
#include "UnitTests/x_unit_test_ll_circular_queue.h"
#include "UnitTests/x_unit_test_file.h"
#include "UnitTests/x_unit_test_bitstream.h"
#include "UnitTests/x_unit_test_file_textfile.h"
#include "UnitTests/x_unit_test_file_serialfile.h"
#include "UnitTests/x_unit_test_memory_ops.h"
#include "UnitTests/x_unit_test_cmdline.h"
#include "UnitTests/x_unit_test_types.h"
#include "UnitTests/x_unit_test_propertyv2.h"

#include "UnitTests/gb_unit_test_01.h"
#include "UnitTests/gb_unit_test_02.h"
#include "UnitTests/gb_unit_test_03.h"
#include "UnitTests/gb_unit_test_04.h"
#include "UnitTests/gb_unit_test_05.h"

#define TEST(A) { xtimer Timer; X_LOG( #A " --> STARTED" ); g_context::get().m_Scheduler.Init(); Timer.Start(); A::Test(); Timer.End(); g_context::get().m_Scheduler.Shutdown(); X_LOG( #A " --> DONE, Time: %f seconds", xtimer::ToSeconds( Timer.getMilliseconds() ) );}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

int main( void )
{
    // initializes the global context
    g_context::Init();

    // Turn off the logging for the scheduler too much noise
    X_CMD_LOGGING( g_context::get().m_Scheduler_LogChannel.TurnOff() );

    if( (0) )
    {
        if( (1) ) TEST( x_unit_test::propertyv2             );
        if( (1) ) TEST( x_unit_test::property               );
        if( (1) ) TEST( x_unit_test::types                  );
        if( (1) ) TEST( x_unit_test::cmdline                );
        if( (1) ) TEST( x_unit_test::memory_ops             );
        if( (1) ) TEST( x_unit_test::serialfile             );
        if( (1) ) TEST( x_unit_test::network                );
        if( (1) ) TEST( x_unit_test::file                   );
        if( (1) ) TEST( x_unit_test::textfile               );
        if( (1) ) TEST( x_unit_test::bitstream              );
        if( (1) ) TEST( x_unit_test::ll_circular_queue      );
        if( (1) ) TEST( x_unit_test::string );
    //    if( (1) ) TEST( x_unit_test::scheduler          );
        if( (1) ) TEST( x_unit_test::scheduler_jobs     );
   //     if( (1) ) TEST( x_unit_test::qt_queue           );
    }

    if( (1) ) TEST( gb_unit_test_05 );

    if( (1) )
    {
        if( (1) ) TEST( gb_unit_test_01 );
        if( (1) ) TEST( gb_unit_test_02 );
       // if( (1) ) TEST( gb_unit_test_03 );
        if( (1) ) TEST( gb_unit_test_04 );
        if( (1) ) TEST( gb_unit_test_05 );
    }

    g_context::Kill();
    return 0;
}

