//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
// 3D Gizmos - https://github.com/CedricGuillemet/ImGuizmo

#include "eng_base.h"
#include "xPlugins/imgui/imgui_impl_engbase.h"
#include "xPlugins/imgui/imguiThemeLionStandard.h"
#include "xPlugins/imgui/imguiXProperty.h"
#include "xPlugins/imgui/imguiNodeGraph.h"
#include "..\IconFontCppHeaders\IconsFontAwesome.h"
#include "..\IconFontCppHeaders\IconsKenney.h"
#include "..\IconFontCppHeaders\IconsMaterialDesign.h"
#include "unittests/x_unit_test_property.h"
#include "..\GameLib\myGame.h"

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
    return 0;
}

#if 0
//#include "EditorLog.h"
//#include "EditorComponentExplorer.h"
//#include "EditorHierarchy.h"
//#include "EditorInspector.h"


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

static bool s_bPlaying = false;

struct myeng
{
    xndptr_s<eng_instance>      m_Instance      {};
    eng_device*                 m_pDevice       { nullptr };
    eng_window*                 m_pWindow       { nullptr };
    eng_view                    m_View          {};
    entity_property_window      m_ProEntityWin  {};
    entity_hierarchy            m_HierarchyWin  {};
    log_window                  m_LogWin        {};
    editor_component_explorer   m_ComponentList {};
    imgui_node_graph::base      m_NodeWin       {};
};

static xndptr_s<myeng>   MyEng;
static xndptr_s<mygame>  MyGame;


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void Init_App()
{
    g_context::Init();
    g_context::get().m_Scheduler.Init();
 //   g_context::get().m_Scheduler_LogChannel.TurnOff();
}

//-------------------------------------------------------------------------------------------
void Init_Engine( HINSTANCE hInstance, myeng& MyEng )
{
    // Create engine instance
    {
        eng_instance::setup         Setup( (void*)hInstance );

      //  Setup.m_bValidation = true;

        auto Err = eng_instance::CreateInstance( MyEng.m_Instance, Setup );
        x_assert( !Err );
    }

    // create a device
    {
        eng_device::setup    Setup;
        auto Err = MyEng.m_Instance->CreateDevice( MyEng.m_pDevice, Setup );
        x_assert( !Err );
    }

    // create a window 
    {
        eng_window::setup   Setup( *MyEng.m_pDevice );
        auto Err = MyEng.m_Instance->CreateWindow( MyEng.m_pWindow, Setup );  
        x_assert( !Err );

       // MyEng.m_pWindow->setBgColor( xcolor( 255, 0,0,255 ));
    }

    //
    // Initialize the scene
    //
    MyEng.m_View.setFov         ( xradian{ 91.5_deg } );
    MyEng.m_View.setFarZ        ( 256.0f );
    MyEng.m_View.setNearZ       ( 0.1f );
    MyEng.m_View.LookAt( 2.5f, xradian3( 0_deg, 0_deg, 0_deg ), xvector3(0) );
}

//-------------------------------------------------------------------------------------------
void MyLogOutput( const x_reporting::log_channel& Channel, int Type, const char* String, int Line, const char* file )
{
    const char* pType[] = { "INFO", "WARNING", "ERROR" };
    MyEng->m_LogWin.AddLog( "[%f][%s][%s] %s - (%s - %d)\n", xtimer::getNowMs(), Channel.getChannelName(), pType[1+Type], String, file, Line );
}

//-------------------------------------------------------------------------------------------
void Init_ImGui( myeng& MyEng )
{
    ImGui_engBase_Init( *MyEng.m_pDevice, *MyEng.m_pWindow, false );
    imguiThemeLionStandard();
    
    //
    // Add fonts
    //
    ImGuiIO& io = ImGui::GetIO();
    io.Fonts->AddFontDefault();

    // merge in icons from Font Awesome
    {
        static const ImWchar icons_ranges_fontawesome[]     = { ICON_MIN_FA, ICON_MAX_FA, 0 };
        static const ImWchar icons_ranges_kenney[]          = { ICON_MIN_KI, ICON_MAX_KI, 0 };
        static const ImWchar icons_ranges_materialdesign[]  = { ICON_MIN_MD, ICON_MAX_MD, 0 };
        ImFontConfig icons_config; 
        icons_config.MergeMode  = true; 
        icons_config.PixelSnapH = true;
        io.Fonts->AddFontFromFileTTF( "../fontawesome-webfont.ttf", 14.0f, &icons_config, icons_ranges_fontawesome);
        //io.Fonts->AddFontFromFileTTF( "../kenney-icon-font.ttf",    16.0f, &icons_config, icons_ranges_kenney);
      //  io.Fonts->AddFontFromFileTTF( "../MaterialIcons-Regular.ttf", 13.0f, &icons_config, icons_ranges_materialdesign);
        
    }

    //
    // Set the log function
    //
    g_context::get().m_pLogOuputFn =  MyLogOutput;
}

//-------------------------------------------------------------------------------------------
bool BeginFrame( myeng& MyEng )
{
    if( MyEng.m_pWindow->HandleEvents() == false )
        return false;

    //
    // Make the camera look at the center of the screen
    // Rotate slowly
    //
    MyEng.m_View = MyEng.m_pWindow->getActiveView();

    auto CameraAngles   = MyEng.m_View.getAngles(); 
    CameraAngles.m_Yaw += xradian{ 1_deg };
    MyEng.m_View.LookAt( 4.5, CameraAngles, xvector3(0) );
        
    MyEng.m_pWindow->BeginRender( MyEng.m_View );
    
    //
    // Get imGuid ready for action
    //
    ImGui_engBase_NewFrame();

    //
    // Need to create the maindoc window. This window will be consider our main application window.
    // We need this because the docking system need a parent window to dock. So we will keep this
    // maindoc window as big as the system window at all times.
    // Since it is a fake window in a way we want to make sure that we dont render any background
    // So we will make its background 100% transparent so we can see perfectly thought it.
    // The docking also space also tries not render a the background so we will also make it 100%
    // transparent.
    //
    const ImGuiStyle *  style               = &ImGui::GetStyle();
    const ImColor       TempBgColor         = style->Colors[ImGuiCol_WindowBg];
    const ImColor       TempChildBgColor    = style->Colors[ImGuiCol_ChildWindowBg];

    // Make sure that our main doc is as big as the system window 
    const auto& View = MyEng.m_pWindow->getActiveView();
    ImGui::SetNextWindowPos( ImVec2(0,0) );
    ImGui::SetWindowSize( "maindoc", ImVec2( (f32)View.getViewport().getWidth(), (f32)View.getViewport().getHeight()), 0 );

    // make 100% transparent
    static const ImVec4 col{0,0,0,0};
    ImGui::PushStyleColor(ImGuiCol_WindowBg, col);
    ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, col);

    // Now we can render the window which will take the hold screen
    static bool bOPen = false;
    ImGui::Begin( "maindoc", &bOPen, 
        ImGuiSetCond_Always                     |
        ImGuiWindowFlags_NoMove                 |
        ImGuiWindowFlags_NoResize               |
        ImGuiWindowFlags_NoInputs               |
        ImGuiWindowFlags_NoTitleBar             |
        ImGuiWindowFlags_NoSavedSettings        |
        ImGuiWindowFlags_NoScrollbar            |
        ImGuiWindowFlags_NoCollapse             |
  //      ImGuiWindowFlags_MenuBar                |
        ImGuiWindowFlags_NoFocusOnAppearing     |
        ImGuiWindowFlags_AlwaysUseWindowPadding |
        ImGuiWindowFlags_NoBringToFrontOnFocus );

    // Start the doc space now  
    ImGui::BeginDockspace();

    // Restore the original colors of the theme
    ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, TempChildBgColor);
    ImGui::PushStyleColor(ImGuiCol_WindowBg, TempBgColor );
    
    return true;
}

//-------------------------------------------------------------------------------------------
void EndFrame( myeng& MyEng )
{
    //
    // close the imgGui window
    //

    // Pop the orifinal colors of the theme
    ImGui::PopStyleColor(2); 

    // End the maindoc window    
    ImGui::EndDockspace();
    ImGui::End();

    // Now we can pop the other two transparent colors (child and window)
    ImGui::PopStyleColor(2); 

    //
    // Draw the gui
    //
    ImGui::Render();

    //
    // Tell the engine we are done rendering
    //
    MyEng.m_pWindow->EndRender();
    MyEng.m_pWindow->PageFlip();
}

//-------------------------------------------------------------------------------------------

void RenderEditorWindows( myeng& MyEng, mygame& MyGame )
{
    static bool     show_another_window = false;
    static bool     show_test_window    = false;
    static ImVec4   clear_col           = ImColor(114, 144, 154);

/*
    static bool Open2 = true;
    if( ImGui::BeginDock( "test3", &Open2 ) )
    {
        static float f = 0.0f;
        ImGui::Text("Hello, world!");
        if( ImGui::MyTreeNode( "What is this" ) )
        {
            if( ImGui::MyTreeNode( "Something" ) )
            {

            }
        }
    }
    ImGui::EndDock();
*/

    MyEng.m_NodeWin.Render();

    MyEng.m_ComponentList.Render( MyGame );

    static bool OpenLog = true;
    MyEng.m_LogWin.Draw( "Log", &OpenLog);

    // 1. Show a simple window
    // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
    static bool Open = true;
        static const ImVec4 col{0,0,0,0};
        ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, col);
    if( ImGui::BeginDock( "test1", &Open ) )
    {

        {
            static float f = 0.0f;
            ImGui::Text("Hello, world!");
            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
            ImGui::ColorEdit3("clear color", (float*)&clear_col);
            if (ImGui::Button("Test Window")) show_test_window ^= 1;
            if (ImGui::Button("Another Window")) show_another_window ^= 1;
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

            // in an imgui window somewhere...
            ImGui::Text( ICON_FA_PLAY " File" ); // use string literal concatenation, ouputs a file icon and File as a string.
        }
    }
    ImGui::EndDock();
        ImGui::PopStyleColor();

    ImGui::SetNextDock( ImGuiDockSlot_Left );
    MyEng.m_HierarchyWin.Render();


    ImGui::SetNextDock( ImGuiDockSlot_Bottom );
    if( MyEng.m_HierarchyWin.m_Select.getCount() == 1 )
    {
        if( MyEng.m_ProEntityWin.getEntity() != MyEng.m_HierarchyWin.m_Select[0] )
        {
            MyEng.m_ProEntityWin.setup( MyEng.m_HierarchyWin.m_Select[0] );
        }
    }

    MyEng.m_ProEntityWin.Update();
    MyEng.m_ProEntityWin.Render();


    // 2. Show another simple window, this time using an explicit Begin/End pair
    if (show_another_window)
    {
        const auto& View = MyEng.m_pWindow->getActiveView();
        ImGui::SetWindowPos( "MainMenuWindow", ImVec2( (f32)View.getViewport().getWidth()/2, (f32)0), 0 );
       // ImGui::SetWindowSize( "MainMenuWindow", ImVec2(0,16), ImGuiSetCond_Always );
        //ImGui::SetNextWindowSize(ImVec2(0,0), ImGuiSetCond_FirstUseEver);
        ImGui::Begin("MainMenuWindow", &show_another_window,
            ImGuiSetCond_Always                     |
            ImGuiWindowFlags_NoMove                 |
            ImGuiWindowFlags_NoResize               |
         //   ImGuiWindowFlags_NoInputs               |
            ImGuiWindowFlags_NoTitleBar             |
            ImGuiWindowFlags_NoSavedSettings        |
            ImGuiWindowFlags_NoScrollbar            |
            ImGuiWindowFlags_NoCollapse             |
      //      ImGuiWindowFlags_MenuBar                |
            ImGuiWindowFlags_NoFocusOnAppearing     |
      //      ImGuiWindowFlags_AlwaysUseWindowPadding |
          //  ImGuiWindowFlags_NoBringToFrontOnFocus 
          0
            );

        static ImVec2 ButtonSize (60,17);
        if( s_bPlaying )
        {
            static ImVec2 ButtonSize (25,17);
            bool Stop = false;
            Stop |= ImGui::Button( ICON_FA_PAUSE , ButtonSize);  ImGui::SameLine();
            Stop |= ImGui::Button( ICON_FA_STOP  , ButtonSize);  ImGui::SameLine();
            if(Stop)  s_bPlaying = false;
        }
        else
        {
            const bool Play = ImGui::Button( ICON_FA_PLAY  ,  ButtonSize);  ImGui::SameLine();
            if( Play ) s_bPlaying = true; 
        }

    static const char* test_data = "Menu";
      const char* items[] = { "Orange", "Blue", "Purple", "Gray", "Yellow", "Las Vegas" };
      int items_count = sizeof(items)/sizeof(*items);

      static int selected = -1;

      ImGui::Button(selected >= 0 ? items[selected] : "Menu", ButtonSize); ImGui::SameLine();
      if (ImGui::IsItemActive())          // Don't wait for button release to activate the pie menu
          ImGui::OpenPopup("##piepopup");
  
      ImVec2 pie_menu_center = ImGui::GetIO().MouseClickedPos[0];
      int n = ImGui::PiePopupSelectMenu(pie_menu_center, "##piepopup", items, items_count, &selected);
      if (n >= 0)
          printf("returned %d\n", n);

        ImGui::End();
    }

    // 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
    if (show_test_window)
    {
        ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);     // Normally user code doesn't need/want to call it because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
        ImGui::ShowTestWindow(&show_test_window);

    }
}



//-------------------------------------------------------------------------------------------

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
    //
    // First init the app
    //
    Init_App();

    //
    // Now we can have our engine and our game
    //
    MyEng.New();
    MyGame.New();

    //
    // Initialize all
    //
    Init_Engine( hInstance, MyEng );
    Init_ImGui( MyEng );

    MyGame->Initialize();
    MyEng->m_HierarchyWin.m_pBase = &MyGame[0];
    MyEng->m_HierarchyWin.Refresh();
    MyEng->m_ComponentList.Refresh( MyGame );

    X_LOG( "Initialization completed");

    for( int i = 0; i < 2; i++ )
    {
        xndptr_s<imgui_node_graph::node> NodeTest;
        NodeTest.New( imgui_node_graph::node::guid( imgui_node_graph::node::guid::RESET ), X_STR("TestNode"), ImVec2( 10, 10 ) );

        NodeTest->AddOuputPod( 
            imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }, 
            X_STR("MyPod3"), 
            imgui_node_graph::node::connection_pod::type::guid{ imgui_node_graph::node::connection_pod::type::guid::RESET } );

        NodeTest->AddOuputPod( 
            imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }, 
            X_STR("MyPod4"), 
            imgui_node_graph::node::connection_pod::type::guid{ imgui_node_graph::node::connection_pod::type::guid::RESET } );

        NodeTest->AddInputPod( 
            imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }, 
            X_STR("MyPod1"), 
            imgui_node_graph::node::connection_pod::type::guid{ imgui_node_graph::node::connection_pod::type::guid::RESET } );

        NodeTest->AddInputPod( 
            imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }, 
            X_STR("MyPod2"), 
            imgui_node_graph::node::connection_pod::type::guid{ imgui_node_graph::node::connection_pod::type::guid::RESET } );

        MyEng->m_NodeWin.AddNode( NodeTest );
    }

    //
    // Do the Integration
    //
    while( BeginFrame( MyEng ) )
    {
        //
        // 3d test
        //
        eng_draw::pipeline PipeLine;

        PipeLine.m_BLEND   = eng_draw::BLEND_OFF;
        PipeLine.m_ZBUFFER = eng_draw::ZBUFFER_OFF;

        auto& CmdList = MyEng->m_pWindow->getDisplayCmdList( 2 );
        CmdList.Draw( PipeLine, [&]( eng_draw& Draw )
        {
            auto View = MyEng->m_pWindow->getActiveView();
            Draw.ClearSampler();
            Draw.ClearScissor();
            Draw.DrawDebugMarker    ( View.getW2C(), xvector3(0), xcolor(~0) );
            Draw.DrawBBox           ( View.getW2C(), xvector3(0), xcolor::getColorCategory( 1 ) );
            Draw.DrawSphere         ( View.getW2C(), xcolor::getColorCategory( 2 ) );
        });
        MyEng->m_pWindow->SubmitDisplayCmdList( CmdList );

        if( s_bPlaying )
        {
            MyGame->setLoopMode(false);
            g_context::get().m_Scheduler.AddJobToQuantumWorld( MyGame );
            g_context::get().m_Scheduler.MainThreadStartsWorking();
        }

        //
        // Render the 2d windows
        //
        RenderEditorWindows( MyEng, MyGame );
        
        EndFrame( MyEng );
    }

    //
    // Shut down
    //
    ImGui_engBase_Shutdown();

    //
    // End the app
    //
    g_context::Kill();
    return 0;
}

#endif

