namespace my_game
{
    class entity_bullet : public gb_component::entity
    {
        x_object_type( entity_bullet, is_quantum_lock_free, rtti(gb_component::entity), is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_STR_U("Entity/Bullet");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Entity/Bullet::SpaceShip" };
            x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
            using                           t_type                      = gb_component::type_entity_pool<t_self>;
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
        };

    public:

        auto getShipGuid( void ) const { return m_ShipGuid; }
        auto& LinearSetup( gb_component::entity::guid gWhoShotMe ){ m_ShipGuid = gWhoShotMe; return *this; }

        
        struct delegates : public gb_component::entity::delegates
        {
            x_object_type( delegates, is_linear, rtti(gb_component::entity::delegates), is_not_copyable, is_not_movable )
            delegates(void) = default;
            
            physics_component::event_Collision::delegate<entity_bullet>    m_Collision     { &entity_bullet::msgCollision };

            struct table : t_parent::table
            {
                const definition m_CollisionDef{ offsetof(delegates, m_Collision), X_STR("Collision"), physics_component::event_Collision::getType() };
            };
        };

    protected: // --- class hierarchy types ---
    protected: // --- class hierarchy functions ---
        
        x_forceinline entity_bullet( const base_construct_info& C ) noexcept : t_parent( C ) {}

        virtual err onCheckResolve ( xvector<xstring>& WarningList ) const noexcept
        {
            if( getComponent<physics_component>() == nullptr )
                return x_error_code( errors, ERR_FAILURE, "We need to have a physics component for this entity" );

            return x_error_ok();
        }

        virtual void onExecute( void ) noexcept override
        {
            t_parent::onExecute();

            // When timer reaches zero then we destroy ourselves
            m_Timer -= getGlobalInterface().m_GameMgr.getDeltaTime();
            if( m_Timer <= 0 ) msgDestroy();
        }

        virtual prop_table& onPropertyTable ( void ) const noexcept override 
        { 
            static prop_table E( this, this, X_STR("Physics-Spaceships"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( t_parent::onPropertyTable() );
                E.AddProperty  ( X_STR("Timer"),       m_Timer );
                E.AddProperty  ( X_STR("ShipGuid"),    m_ShipGuid.m_Value );
            });

            return E; 
        }

        void msgCollision( gb_component::entity& Entity )
        {
            //
            // Deal with different types of collisions
            //
            if( !Entity.isKindOf<entity_bullet>() )
            {
                // If it is my own ship the we don't blow up
                if( Entity.getGuid() == m_ShipGuid )
                    return;
            }

            // Bullet hits anything else then both things blow up
            msgDestroy();
            Entity.msgDestroy();
        }

    protected:  // --- class hierarchy hidden variables ---
        
        float                                                   m_Timer                 { 5 };
        gb_component::entity::guid                              m_ShipGuid              { nullptr };
    };
}
