namespace my_game
{
    class entity_spaceship : public gb_component::entity
    {
        x_object_type( entity_spaceship, is_quantum_lock_free, rtti(gb_component::entity), is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_STR_U("Entity/Spaceship");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Entity/Bullet::Spaceship" };
            x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
            using                           t_type                      = gb_component::type_entity_pool<t_self>;
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
        };

    public: 

        auto& LinearSetup( gb_blueprint::master::guid gBullet ) noexcept
        {
            m_gBulletBlueprint = gBullet;
            return *this;
        }

    protected: // --- class hierarchy functions ---
        
        x_forceinline                       entity_spaceship        ( const base_construct_info& C )            noexcept : t_parent( C ) { ResetTimer(); }
        virtual         void                onResolve               ( void )                                    noexcept override;
        virtual         err                 onCheckResolve          ( xvector<xstring>& WarningList )   const   noexcept override;
        virtual         void                onExecute               ( void )                                    noexcept override;
        virtual         prop_table&         onPropertyTable         ( void )                            const   noexcept override;
        x_incppfile     void                ResetTimer              ( void )                                    noexcept;

    protected:  // --- class hierarchy hidden variables ---
        
        float                                                   m_WaitTimer             {};
        physics_component*                                      m_pPhysics              { nullptr };
        gb_blueprint::master::guid                              m_gBulletBlueprint      { nullptr };
    };
}
