#include "MyGame.h"

namespace my_game
{
    //------------------------------------------------------------------------------------------------
    
    void entity_spaceship::ResetTimer( void ) noexcept
    {
        getGlobalInterface().m_GameMgr.UseLinearTypes([&]( gb_game_graph::base::linear_types& LTypes )
        {
            m_WaitTimer = LTypes.m_SmallRnd.RandF32( 10.0f, 20.0f );
        }); 
    }

    //------------------------------------------------------------------------------------------------

    void entity_spaceship::onExecute( void ) noexcept
    {
        t_parent::onExecute();

        //
        // We have to wait until we can shot again
        //
        m_WaitTimer -= getGlobalInterface().m_GameMgr.getDeltaTime();
        if( m_WaitTimer > 0 ) 
            return;

        //
        // Collect the other ships
        //
        const xvector2               MyPos = m_pPhysics->getPos();
        xvector<physics_component*>  List;
    
        auto&       GameMgr = getGlobalInterface().m_GameMgr.SafeCast<game_mgr>();
        GameMgr.getPhysicsMgr().getClosestComponents( List, MyPos, 50.0f );

        //
        // Find the closest dot from me.
        //
        f32             ClosestDistance = 100000000.0f;
        s32             iClosest        = -1;

        for( s32 i=0; i<List.getCount(); i++ )
        {        
            const physics_component& Physics      = *List[i];

            // Skip ourselves
            if( m_pPhysics == &Physics )
                continue;

            // Make sure that are spaceship entities...
            if( Physics.getEntity().isKindOf<entity_spaceship>() == false )
                continue;

            // Check whether this component is the closest
            const f32 DistanceSquare = Physics.getPos().Dot( MyPos );

            if( DistanceSquare < ClosestDistance )
            {
                ClosestDistance = DistanceSquare;
                iClosest        = i;
            }
        }

        // We do not have anyone close to us
        if( iClosest == -1 )
            return;

        //
        // Find the closest dot from me.
        //
        const physics_component&            EnemyPhysics = *List[iClosest];
        xvector2                            Direction    = (EnemyPhysics.getVel() + EnemyPhysics.getPos()) - MyPos;
        Direction.Normalize();

        //
        // Create a bullet
        //
        //if( false )
        {
            auto& BulletBlueprint = GameMgr.m_BlueprintDB.getEntry( m_gBulletBlueprint );
            auto& BulletEntity    = BulletBlueprint.CreateEntity<entity_bullet>( gb_component::entity::guid( gb_component::entity::guid::RESET ), GameMgr );
            BulletEntity.LinearSetup( getGuid() ).getComponent<physics_component>()->LinearSetup( MyPos, Direction * 60.0f );
            BulletEntity.msgAddToWorld();
        }

        //
        // Reset timer
        //
        ResetTimer();
    }

    //------------------------------------------------------------------------------------------------

    void entity_spaceship::onResolve ( void ) noexcept
    {
        t_parent::onResolve();

        m_pPhysics = getComponent<physics_component>();
    }

    //------------------------------------------------------------------------------------------------

    entity_spaceship::err entity_spaceship::onCheckResolve ( xvector<xstring>& WarningList ) const noexcept
    {
        if( getComponent<physics_component>() == nullptr )
            return x_error_code( errors, ERR_FAILURE, "We need to have a physics component for this entity" );

        return x_error_ok();
    }


    //------------------------------------------------------------------------------------------------
    entity_spaceship::prop_table& entity_spaceship::onPropertyTable ( void ) const noexcept  
    { 
        static prop_table E( this, this, X_STR("Entity-SpaceShip"), [&](xproperty_v2::table& E)
        {
            E.AddChildTable( t_parent::onPropertyTable() );
            E.AddProperty  ( X_STR("BulletBlueprint"),     m_gBulletBlueprint.m_Value );
        });

        return E; 
    }
}