
#include "MyGame.h"

namespace my_game
{
    //------------------------------------------------------------------------------------------------

    void render_component::onResolve ( void ) noexcept
    {
        m_pPhysics = getEntity().getComponent<physics_component>();
    }

    //------------------------------------------------------------------------------------------------

    render_component::err render_component::onCheckResolve ( xvector<xstring>& WarningList ) const noexcept
    {
        if( getEntity().getComponent<physics_component>() == nullptr )
            return x_error_code( errors, ERR_FAILURE, "We need to have a physics component for this render component to work" );

        return x_error_ok();
    }
}
