#include "MyGame.h"

namespace my_game
{
    //-------------------------------------------------------------------------------

    void physics_mgr::Initialize( f32 Width, f32 Height ) noexcept
    {
        m_DebugRenderDelegate.Connect( getGameGraph<game_mgr>().getRenderMgr().m_Events.m_DebugRender );
        m_Bounds.m_Left     = m_Bounds.m_Top = 0;
        m_Bounds.m_Right    = Width;
        m_Bounds.m_Bottom   = Height;
    }

    //-------------------------------------------------------------------------------

    void physics_mgr::msgDebugRender ( eng_draw& Draw, const eng_view& View ) noexcept
    {
        return;

        eng_draw::buffers   PrimBuf     = Draw.popBuffers( (m_nRows + m_nColumns + 2 )*2, (m_nRows + m_nColumns + 2 )*2 );
        int                 offset      = 0;
        const f32           CellH       = m_Bounds.getHeight() / m_nRows;
        const f32           CellW       = m_Bounds.getWidth()  / m_nColumns;
         
        for( s32 y = 0; y <= m_nRows; y++ )
        {
            auto*           pVertex = &PrimBuf.m_Vertices[ offset ];
            auto*           pIndex  = &PrimBuf.m_Indices[ offset ];

            pVertex[0].setup( xvector3d( m_Bounds.m_Left,    m_Bounds.m_Top + y * CellH, 0),  xcolor( 0xff, 0, 0, 0x8f ) );
            pVertex[1].setup( xvector3d( m_Bounds.m_Right,   m_Bounds.m_Top + y * CellH, 0),  xcolor( 0xff, 0, 0, 0x8f ) );

            pIndex[0] = offset + 0;
            pIndex[1] = offset + 1;

            offset += 2;
        }

        for( s32 x = 0; x <= m_nColumns; x++ )
        {
            auto*           pVertex = &PrimBuf.m_Vertices[ offset ];
            auto*           pIndex  = &PrimBuf.m_Indices[ offset ];

            pVertex[0].setup( xvector3d( m_Bounds.m_Left + x * CellW, m_Bounds.m_Top,    0),  xcolor( 0xff, 0, 0, 0x8f ) );
            pVertex[1].setup( xvector3d( m_Bounds.m_Left + x * CellW, m_Bounds.m_Bottom, 0),  xcolor( 0xff, 0, 0, 0x8f ) );

            pIndex[0] = offset + 0;
            pIndex[1] = offset + 1;

            offset += 2;
        }

        Draw.DrawBufferLines( View.getPixelBased2D() );
    }

    //-------------------------------------------------------------------------------

    void physics_mgr::onExecute( void ) noexcept
    {
        x_assert_quantum( m_Debug_LQ );
        x_assert_linear( m_Debug_LQMsg );

        const auto DeltaTime    = m_TypeHead[0]->getGlobalInterface().m_GameMgr.getDeltaTime();
        const auto iT0          = m_TypeHead[0]->getGlobalInterface().m_SyncPoint.getICurrent();
        const auto iT1          = 1 - iT0;

        //
        // Clear T1
        //
        for( auto& Cell : m_World ) Cell.m_Head[iT1].store( nullptr, std::memory_order_relaxed ); 
        
        //
        // Compute all the components
        //
        x_job_block JobBlockTypes(X_WSTR("PhysicsTypes"));
        for( gb_component::type_base* pType : m_TypeHead )
        {
            JobBlockTypes.SubmitJob( [this,DeltaTime,iT0]
            {
                x_job_block JobBlockComponents(X_WSTR("PhysicsComponents"));
                JobBlockComponents.ForeachLog( m_World, 8, 10, [this,DeltaTime,iT0]( xbuffer_view<cell> CellList )
                {
                    const int iT1 = 1 - iT0;
                    for( cell& Cell : CellList )
                    {
                        const auto iThisCell = m_World.getIndexByEntry<int>(Cell);

                        // update the collisions of all the particles
                        for( auto pHead = Cell.m_Head[iT0].load( std::memory_order_relaxed ); pHead; )
                        {
                            auto& I_T0 = pHead->getT0Data<physics_component::mutable_data>();

                            if( pHead->isDeleted() )
                            {
                                pHead = I_T0.m_pNext;
                                continue;
                            }

                            // Let the component update all its messages
                            //pHead->qt_onRun();

                            // Advance the physics
                            auto& I_T1 = pHead->getT1Data<physics_component::mutable_data>();
                            I_T1.m_Pos = I_T0.m_Pos + I_T0.m_Vel * DeltaTime;
                            I_T1.m_Vel = I_T0.m_Vel;

                            //
                            // Check bound with world
                            //
                            if( I_T1.m_Pos.m_X >= m_Bounds.m_Right  || I_T1.m_Pos.m_X < m_Bounds.m_Left ) 
                            { 
                                I_T1.m_Pos.m_X =  I_T0.m_Pos.m_X; 
                                I_T1.m_Vel.m_X = -I_T1.m_Vel.m_X; 
                            } 

                            if( I_T1.m_Pos.m_Y >= m_Bounds.m_Bottom || I_T1.m_Pos.m_Y < m_Bounds.m_Top  ) 
                            { 
                                I_T1.m_Pos.m_Y =  I_T0.m_Pos.m_Y; 
                                I_T1.m_Vel.m_Y = -I_T1.m_Vel.m_Y; 
                            }

                            //
                            // Check collisions with other entities
                            //
                            int         iNextCell  = getGridIndexUnsafe( I_T1.m_Pos );
                            const auto& I_ReadOnly = *pHead->m_ConstData;

                            // Check collisions
                            for( auto pNextHead = (iNextCell == iThisCell)  ? 
                                                        I_T0.m_pNext        : 
                                                        m_World[ iNextCell ].m_Head[iT0].load( std::memory_order_relaxed ); 
                                 pNextHead; )
                            {
                                auto& J_T0 = pNextHead->getT0Data<physics_component::mutable_data>();

                                if( pNextHead->isDeleted() )
                                {
                                    pNextHead = J_T0.m_pNext;
                                    continue;
                                }

                                xvector2 I_CollisionVector = J_T0.m_Pos - I_T0.m_Pos;

                                // Collision with other objects
                                if( I_CollisionVector.getLengthSquared() > ( 3.0f * 3.0f) )
                                {
                                    pNextHead = J_T0.m_pNext;
                                    continue;
                                }

                                // Get the readonly data
                                const auto& J_ReadOnly = *pNextHead->m_ConstData;

                                // Check for collisions
                                if( I_ReadOnly.m_bHandleCollisions )
                                {
                                    pHead->getEvents().SafeCast<physics_component::events>().m_Collision.Notify( pNextHead->getEntity() );
                                }
                                else if( J_ReadOnly.m_bHandleCollisions )
                                {
                                    pNextHead->getEvents().SafeCast<physics_component::events>().m_Collision.Notify( pHead->getEntity() );
                                }
                                else
                                {
                                    I_CollisionVector.NormalizeSafe();

                                    I_T1.m_Vel  -= I_CollisionVector * (2.0f * I_CollisionVector.Dot( I_T1.m_Vel ));
                                    //I_T1.m_Pos   = I_T0.m_Pos;
                                    //iNextCell    = iThisCell;
                                    break;
                                }

                                //
                                // Get the next entry
                                //
                                pNextHead = J_T0.m_pNext;

                            } // End of inner loop

                            //
                            // Insert entry in the next cell
                            // 
                            {
                                auto& Head = m_World[iNextCell].m_Head[iT1];
                                I_T1.m_pNext = Head.load( std::memory_order_relaxed );
                                while( false == Head.compare_exchange_weak( I_T1.m_pNext, pHead ) );
                            }

                            //
                            // Get the next entry
                            //
                            pHead = I_T0.m_pNext;

                        }  // end of components
                    } // end of cells
                } );
                JobBlockComponents.Join();
            });
        }
        JobBlockTypes.Join();

        //
        // Done running components
        //
        int a = 22;
    }

    //-------------------------------------------------------------------------------

    void physics_mgr::getClosestComponents( xvector<physics_component*>& lComp, const xvector2 Point, const f32 Radius ) const noexcept
    {
        const auto RSquare  = Radius*Radius;
        const auto XMin     = getGridXSafe( Point.m_X - Radius );
        const auto XMax     = getGridXSafe( Point.m_X + Radius );
        const auto YMin     = getGridYSafe( Point.m_Y - Radius );
        const auto YMax     = getGridYSafe( Point.m_Y + Radius );
        const auto iT0      = m_TypeHead[0]->getGlobalInterface().m_SyncPoint.getICurrent();
                        
        for( auto y=YMin; y<YMax; y++ )
        for( auto x=XMin; x<XMax; x++ )
        {
            const auto& Cell = m_World[ x + y * m_nColumns ];

            for( auto pPhysics = Cell.m_Head[iT0].load( std::memory_order_relaxed ); pPhysics; )
            {
                auto&       T0Mutable   = pPhysics->getT0Data<physics_component::mutable_data>();
                const auto  LSquare     = (T0Mutable.m_Pos - Point).getLengthSquared();

                if( LSquare < RSquare )
                {
                    lComp.append( pPhysics );
                }

                pPhysics = T0Mutable.m_pNext;
            }
        }
    }

    //-------------------------------------------------------------------------------

    void physics_mgr::onAddToWorld( gb_component::base& Component ) noexcept
    {
        // We do not want it to be added into the generic world vector
        //t_parent::onAddToWorld( Component );

        x_assert( getGridIndexSafe( xvector2( m_Bounds.m_Left + m_Bounds.m_Right - 0.001f, m_Bounds.m_Top +  m_Bounds.m_Bottom - 0.001f ) ) == m_World.getCount<int>()-1  );
        x_assert( getGridIndexSafe( xvector2( m_Bounds.m_Left + m_Bounds.m_Right - 0.001f, m_Bounds.m_Top )   ) == m_nColumns -1  );
        x_assert( getGridIndexSafe( xvector2( m_Bounds.m_Left, m_Bounds.m_Top +  m_Bounds.m_Bottom - 0.001f  ) ) == (m_nRows -1)*m_nColumns  );

        physics_component&  Object  = Component.SafeCast<physics_component>();
        auto&               Mutable = const_cast<physics_component::mutable_data&>(Object.getT0Data<physics_component::mutable_data>());
        const auto          iT      = Object.getGlobalInterface().m_SyncPoint.getICurrent();
        const auto          iCell   = getGridIndexSafe( Mutable.m_Pos );
        auto&               Head    = m_World[iCell].m_Head[iT];

        // Make it ready
        Mutable.m_pNext = Head.load( std::memory_order_relaxed );
        while (false == Head.compare_exchange_weak(Mutable.m_pNext, &Object)) {}
    }

    //-------------------------------------------------------------------------------
    void physics_mgr::onRemoveFromWorld( gb_component::base& Component ) noexcept
    {
        // We do not need to remove it from the generic world vector since it has never added
        //t_parent::onRemoveFromWorld( Component );
    }

    //-------------------------------------------------------------------------------
    void physics_mgr::onDestroy( gb_component::base& Component ) noexcept
    {
        x_assert_quantum( m_Debug_LQ );
        x_assert_linear( m_Debug_LQMsg );

        if( Component.isInWorld() ) 
        {
            // Make sure we take it out of the world properly
            onRemoveFromWorld( Component );
        }

        getDelayDeleteList().append() = &Component;
    }

    //-------------------------------------------------------------------------------
    void physics_mgr::onEndOfFrameCleanUp ( void ) noexcept
    {
        auto& DeleteList = getDeleteList();

        //
        // Free memory for all the pending components
        //
        for( gb_component::base* pDestroy : DeleteList )
        {
            pDestroy->getGlobalInterface().m_Type.msgDestroy( *pDestroy );
        }

        // Reset the list
        DeleteList.DeleteAllEntries();

        // Update the next index
        m_iDeleteList++;
    }
}


