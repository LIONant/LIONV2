namespace my_game
{
    class physics_mgr;

    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class physics_component : public gb_component::base
    {
        x_object_type( physics_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

    public: // --- class traits ---

        // Customization of the physics for this particular example
        struct const_data : public gb_component::const_data
        {
            x_object_type( const_data, is_quantum_lock_free, rtti(gb_component::const_data), is_not_copyable, is_not_movable )
    
            x_constexprvar type t_type = 
            { 
                static_cast<const_data*>(nullptr),
                "space_ship_game/physics_component/const_data",
                X_STR("space_ship_game/physics_component"), 
                X_STR("No help!")
            };

            constexpr                       const_data          ( const guid Guid ) : gb_component::const_data( Guid ) {}
            virtual         const type&     getType             ( void )            const   noexcept override           { return t_type; }

            bool        m_bIgnoreCollisions = false;
            bool        m_bHandleCollisions = false;
        };

        // This structure contains detail information about our component that other system need
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_STR_U("Physics/ShipsPhy");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Physics-Spaceships" };
            x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
            using                           t_type                      = gb_component::type_pool<t_self>;
        };


        // Defines the events
        GB_COMPONENT_EVENT_TYPE_DEF( event_Collision, "physics_component::Collision", "This event is sent every time the components collides with something", gb_component::entity& )
        
        struct events : public gb_component::events
        {
            x_object_type( events, is_linear, rtti(gb_component::events), is_not_copyable, is_not_movable )

            events( void ) noexcept = default;
            event_Collision     m_Collision;

            struct table : t_parent::table
            {
                const definition m_Collision{ offsetof( events, m_Collision ), X_STR("OnCollision"), guid{"physics_component::OnCollision"}, event_Collision::getType() };
            };

            /*
            virtual const table& getTable( void ) const noexcept override
            {
                static const xarray<definition,1> Definitions =
                {
                    definition{ offsetof( events, m_Collision ), X_STR("OnCollision"), guid{"physics_component::OnCollision"}, event_Collision::getType() }
                };
                static const table Table
                {
                    &t_parent::getTable(), Definitions
                };
                return Table;
            }

            void onAppendEvents( xproperty_v2::table& E ) const
            {
                t_parent::onAppendEvents( E );
            }
            */
        };

    public: 

                        xrect       getBounds       ( f32 delta )                               const;
        x_forceinline   void        msgUpdatePosVel ( const xvector2 Pos, const xvector2 Vel )          noexcept { SendMsg( [this, Pos, Vel](){ this->onUpdatePosVel( Pos, Vel ); } ); }
        x_forceinline   xvector2    getPos          ( void )                                    const   noexcept { return getT0Data<mutable_data>().m_Pos; }
        x_forceinline   xvector2    getVel          ( void )                                    const   noexcept { return getT0Data<mutable_data>().m_Vel; }

        x_forceinline   auto&       LinearSetup     ( const_data& ConstData )           noexcept
        {
            m_ConstData.AddReference( ConstData );
            return *this;
        }

        x_forceinline   auto&       LinearSetup     ( const xvector2& Pos, const xvector2& Vel )        noexcept
        {
            auto& T1 = getT1Data<mutable_data>();
            T1.m_Pos        = Pos;
            T1.m_Vel        = Vel;
            return *this;
        }

    protected: 

        struct mutable_data : t_parent::mutable_data
        {
            virtual void onUpdateMutableData( const t_mutable_base& Src ) noexcept override 
            {
                const mutable_data& NewSrc = reinterpret_cast<const mutable_data&>(Src);
                m_Pos = NewSrc.m_Pos;
                m_Vel = NewSrc.m_Vel;
            } 

            xvector2                        m_Pos{0,0};
            xvector2                        m_Vel{0,0};
            physics_component*              m_pNext{nullptr};  
        };

    protected: 
        
        x_forceinline                       physics_component       ( const base_construct_info& C )                    noexcept : t_parent( C ){};
        virtual         prop_table&         onPropertyTable         ( void )                                            const noexcept override 
        { 
            static prop_table E( this, this, X_STR("Physics-Spaceships"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( t_parent::onPropertyTable() );
                E.AddProperty<u64>      ( X_STR("ConstantData"),    GB_PROP_CONSTANT_DATA( m_ConstData ),   X_STR("This is the component constant/read-only data. This data will be use by the component so it must be of the correct type." ) );
                E.AddProperty<xvector2> ( X_STR("Position"),        GB_PROP_MUTABLE_DATA(m_Pos),            X_STR("Position of the ship"));
                E.AddProperty<xvector2> ( X_STR("Velocity"),        GB_PROP_MUTABLE_DATA(m_Vel),            X_STR("Velocity of the ship"));
            });

            return E; 
        }


        x_forceinline   void                onUpdatePosVel          ( const xvector2 Pos, const xvector2 Vel )
        {
            mutable_data& T1 = getT1Data<mutable_data>();
            T1.m_Pos = Pos;
            T1.m_Vel = Vel;
        }

    protected: 

        x_ll_share_ref<const_data>  m_ConstData {};

    protected: 

        friend class physics_mgr;
        using gb_component::base::qt_onRun;
    };
}
