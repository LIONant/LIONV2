

namespace my_game
{
    class game_mgr final : public gb_game_graph::base
    {
        x_object_type( game_mgr, 
            is_quantum_lock_free, 
            rtti(gb_game_graph::base), 
            is_not_copyable, 
            is_not_movable )
  
    public: // --- class constructor ---

        x_constexprvar xuptr t_max_entities { 300000 };

                game_mgr        ( void ) noexcept : t_parent( t_max_entities ) {}
        auto&   getPhysicsMgr   ( void ) { return m_PhysicsMgr; }
        auto&   getRenderMgr    ( void ) { return m_RenderMgr; }

   
    protected: // --- class types ---

        using ai_mgr        = gb_component_mgr::base;
        using ai_sync       = gb_game_graph::sync_point;
        using physics_sync  = gb_game_graph::sync_point;

    protected: // --- class hierarchy functions ---
        
        virtual void onInitialize   ( void ) noexcept override;
        virtual void onEndFrame     ( void ) noexcept override;

    protected:

        ai_sync                 m_AISyncPoint       {};
        physics_sync            m_PhysicsSyncPoint  {};

        physics_mgr             m_PhysicsMgr        { *this };
        ai_mgr                  m_AIMgr             { ai_mgr::guid{"AIMgr"}, *this, X_WSTR("AIMgr") };
        render_mgr              m_RenderMgr         { *this };
    };
}
