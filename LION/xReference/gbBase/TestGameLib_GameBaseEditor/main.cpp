//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
// 3D Gizmos - https://github.com/CedricGuillemet/ImGuizmo


//#include "GameSpaceShipDataDriven/gm_Game.h"

#include "xTools/gb_editor_base/gbed_base.h"
#include "xTools/gb_editor_plugins/GameGraph/gbed_GameGraph.h"
#include "xTools/gb_editor_plugins/Blueprint/gbed_Blueprint.h"
#include "xTools/gb_editor_plugins/Layers/gbed_Layer.h"
#include "xTools/gb_editor_plugins/Log/gbed_Log.h"
#include "xTools/gb_editor_plugins/EntityComposer/gbed_EntityComposer.h"

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

struct editor
{
    xndptr_s<gbed_game_graph::frame>        m_xpFrame;
    xndptr_s<gbed_document::main>           m_xpDocument;
    xndptr_s<eng_instance>                  m_Instance      {};
    eng_device*                             m_pDevice       { nullptr };
    eng_window*                             m_pWindow       { nullptr };
};



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------
static
void InitEngine( editor& Editor, HINSTANCE hInstance = GetModuleHandle(NULL) )
{
    // Create engine instance
    {
        eng_instance::setup         Setup( (void*)hInstance );

        Setup.m_bValidation = false;

        auto Err = eng_instance::CreateInstance( Editor.m_Instance, Setup );
        x_assert( !Err );
    }

    // create a device
    {
        eng_device::setup    Setup;
        auto Err = Editor.m_Instance->CreateDevice( Editor.m_pDevice, Setup );
        x_assert( !Err );
    }

    // create a window 
    {
        eng_window::setup   Setup( *Editor.m_pDevice );
        auto Err = Editor.m_Instance->CreateWindow( Editor.m_pWindow, Setup );  
        x_assert( !Err );

        // MyEng.m_pWindow->setBgColor( xcolor( 255, 0,0,255 ));
    }

    //
    // Let the engine deal with initial events
    //
    Editor.m_pWindow->HandleEvents();
}

//-------------------------------------------------------------------------------------------

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
    //
    // First init the app
    //
    g_context::Init();
    g_context::get().m_Scheduler.Init();
 //   g_context::get().m_Scheduler_LogChannel.TurnOn();


    //
    // Construct the editor structure
    //
    xndptr_s<editor> xpEditor;
    xpEditor.New();

    //
    // Init the graphics
    //
    InitEngine( xpEditor );

    //
    // Initialize the editor/game frame work
    //
    {
        xndptr_s<gb_game_graph::base> xpGameMgr;

        //
        // Create the document
        //
        xpEditor->m_xpDocument.New()
            .setupProjectFileExt( L".lion.project" );

        //
        // Create frame
        //
        xpEditor->m_xpFrame.New( *xpEditor->m_xpDocument )
            .setupEngWindow( *xpEditor->m_pWindow );
    }
    
    //
    // Create some default tabs
    //
    xpEditor->m_xpFrame->CreateTab( "EDView",       ImGuiDockSlot_Tab );
    xpEditor->m_xpFrame->CreateTab( "Composer",     ImGuiDockSlot_Tab );
    xpEditor->m_xpFrame->CreateTab( "Log",          ImGuiDockSlot_Tab );
    xpEditor->m_xpFrame->CreateTab( "Scene",        ImGuiDockSlot_Left );
    xpEditor->m_xpFrame->CreateTab( "BP",           ImGuiDockSlot_Tab );
    xpEditor->m_xpFrame->CreateTab( "ConstD",       ImGuiDockSlot_Tab );
    xpEditor->m_xpFrame->CreateTab( "GameMgr",      ImGuiDockSlot_Tab );
    xpEditor->m_xpFrame->CreateTab( "Inspector",    ImGuiDockSlot_Bottom );
    

    X_LOG( "Initialization completed");

    //
    // Do the Integration
    //
    while( xpEditor->m_xpFrame->AdvanceLogic() )
    {
        // Nothing to do here
        //X_LOG( "Initialization completed");
    }

    //
    // End the app
    //
    g_context::Kill();
    return 0;
}

