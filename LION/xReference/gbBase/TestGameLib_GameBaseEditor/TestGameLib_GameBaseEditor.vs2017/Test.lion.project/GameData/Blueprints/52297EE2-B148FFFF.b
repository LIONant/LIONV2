
// New Types
//----------------------------------------------------------------------------------------------------------------------
<fff>.ANGLES3 <d>.BOOL <s>.BUTTON <e>.ENUM <f>.FLOAT <g>.GUID <d>.INT <cccc>.RGBA <s>.STRING <ff>.VECTOR2 <fff>.VECTOR3 

[ Blueprint ]
{ GUID:g             Type:g            }
//-----------------  -----------------  
  52297EE2:B148FFFF  F2F6D1A5:CB0F23F4

[ Components : 1 ]
{ Type:g             NGUID:c }
//-----------------  -------  
  32018E95:CB0F23F4     0

[ Properties : 5 ]
{ Type:<?>  Name:s                                  Value:<Type>      }
//--------  --------------------------------------  -----------------  
    .GUID   "Entity/gZone"                          0:0              
    .INT    "Comp[]"                                1                
    .INT    "Comp(0:18E9500)/Spawner/ReleaseCount"  100              
    .INT    "Comp(0:18E9500)/Spawner/DebugCount"    100              
    .GUID   "Comp(0:18E9500)/Spawner/Blueprint"     602D4C97:B12BFFFF
