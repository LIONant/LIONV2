
// New Types
//----------------------------------------------------------------------------------------------------------------------
<fff>.ANGLES3 <d>.BOOL <s>.BUTTON <e>.ENUM <f>.FLOAT <g>.GUID <d>.INT <cccc>.RGBA <s>.STRING <ff>.VECTOR2 <fff>.VECTOR3 

[ Blueprint ]
{ GUID:g             Type:g           }
//-----------------  ----------------  
  FBC4D67C:B0AEFFFF  7E7FB62:CB0F23F4

[ Components : 2 ]
{ Type:g             NGUID:c }
//-----------------  -------  
  157B6CD0:CB0F23F4     1
  41E1DB9C:CB0F23F4     0

[ Properties : 9 ]
{ Type:<?>  Name:s                                           Value:<Type>        }
//--------  -----------------------------------------------  -------------------  
  .GUID     "EntityBullet/Entity/gZone"                      0:0              
  .FLOAT    "EntityBullet/Timer"                             5                
  .GUID     "Delegates/Collision"                            E1DB9C00:8D3FFEC0
  .INT      "Comp[]"                                         2                
  .GUID     "Comp(0:7B6CD001)/Render-SpaceShip/ConstData"    967A41B4:B36A013A
  .GUID     "Comp(0:7B6CD001)/Delegates/riPhysics"           E1DB9C00:206665F8
  .VECTOR2  "Comp(0:E1DB9C00)/Physics-Spaceships/Position"   0                 0
  .VECTOR2  "Comp(0:E1DB9C00)/Physics-Spaceships/Velocity"   0                 0
  .GUID     "Comp(0:E1DB9C00)/Physics-Spaceships/ConstData"  967AD951:B3B10131
