
// New Types
//----------------------------------------------------------------------------------------------------------------------
<fff>.ANGLES3 <d>.BOOL <s>.BUTTON <e>.ENUM <f>.FLOAT <g>.GUID <d>.INT <cccc>.RGBA <s>.STRING <ff>.VECTOR2 <fff>.VECTOR3 

[ Blueprint ]
{ GUID:g             Type:g            }
//-----------------  -----------------  
  602D4C97:B12BFFFF  A7D5545C:CB0F23F4

[ Components : 2 ]
{ Type:g             NGUID:c }
//-----------------  -------  
  157B6CD0:CB0F23F4     1
  41E1DB9C:CB0F23F4     0

[ Properties : 8 ]
{ Type:<?>  Name:s                                           Value:<Type>                }
//--------  -----------------------------------------------  ---------------------------  
  .GUID     "Entity-SpaceShip/Entity/gZone"                  0:0              
  .GUID     "Entity-SpaceShip/BulletBlueprint"               FBC4D67C:B0AEFFFF
  .INT      "Comp[]"                                         2                
  .GUID     "Comp(0:7B6CD001)/Render-SpaceShip/ConstData"    967A4DCB:B0AF006B
  .GUID     "Comp(0:7B6CD001)/Delegates/riPhysics"           E1DB9C00:206665F8
  .VECTOR2  "Comp(0:E1DB9C00)/Physics-Spaceships/Position"   690.406           647.046  
  .VECTOR2  "Comp(0:E1DB9C00)/Physics-Spaceships/Velocity"   19.0452           7.92253  
  .GUID     "Comp(0:E1DB9C00)/Physics-Spaceships/ConstData"  967A10B8:B0B80066
