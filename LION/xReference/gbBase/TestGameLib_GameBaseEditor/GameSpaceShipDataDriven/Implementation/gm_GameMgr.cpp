//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------

void game_mgr::onInitialize( void ) noexcept
{
    //
    // Notify our parent about initialization
    //
    t_parent::onInitialize();

    //
    // Register managers
    //
    m_CompManagerDB.RegisterButRetainOwnership( m_AIMgr );            

    m_CompManagerDB.RegisterButRetainOwnership( m_RenderMgr );            
    const float         XRes = static_cast<f32>(m_RenderMgr.getWindow().getWidth());
    const float         YRes = static_cast<f32>(m_RenderMgr.getWindow().getHeight());

    m_CompManagerDB.RegisterButRetainOwnership( m_PhysicsMgr );
    m_PhysicsMgr.Initialize( XRes, YRes );

    //
    // Build the game graph
    //
    auto* pAISyncPoint = &m_AISyncPoint;
    if( true )
    {
        // Less precise but more parallel graph
        InitializeGraphConnection( m_StartSyncPoint,    m_PhysicsMgr,  m_PhysicsSyncPoint      );
        InitializeGraphConnection( m_PhysicsSyncPoint,  m_AIMgr,       m_EndSyncPoint          );
        InitializeGraphConnection( m_PhysicsSyncPoint,  m_RenderMgr,   m_EndSyncPoint          );

        pAISyncPoint = &m_EndSyncPoint;
    }
    else
    {
        // More precise graph because entities created in the AIMgr will be render this frame
        InitializeGraphConnection( m_StartSyncPoint,    m_PhysicsMgr,  m_PhysicsSyncPoint      );
        InitializeGraphConnection( m_PhysicsSyncPoint,  m_AIMgr,       m_AISyncPoint           );
        InitializeGraphConnection( m_AISyncPoint,       m_RenderMgr,   m_EndSyncPoint          );
    }
            
    //
    // Register all the component types
    //
    RegisterComponentAndDependencies<gb_component::entity>      ( m_AIMgr,      *pAISyncPoint );
    RegisterComponentAndDependencies<entity_bullet>             ( m_AIMgr,      *pAISyncPoint );
    RegisterComponentAndDependencies<spawner_component>         ( m_AIMgr,      *pAISyncPoint );
    RegisterComponentAndDependencies<entity_spaceship>          ( m_AIMgr,      *pAISyncPoint );
    RegisterComponentAndDependencies<physics_component>         ( m_PhysicsMgr, m_PhysicsSyncPoint );
    RegisterComponentAndDependencies<render_component>          ( m_RenderMgr,  m_EndSyncPoint );
}

//-------------------------------------------------------------------------------------------

void game_mgr::onBeginFrame( void ) noexcept
{
}

//-------------------------------------------------------------------------------------------

void game_mgr::onRenderEditorFrame( void ) noexcept
{
    m_RenderMgr.RenderEditorFrame();
}

//-------------------------------------------------------------------------------------------

void game_mgr::onEndFrame( void ) noexcept
{
    t_parent::onEndFrame();

    if( (m_nFrames%60)==0 )
    {
        X_LOG( "Frame: #%llu  SecondsPerFrame: %f", m_nFrames, xtimer::ToSeconds( m_Stats_LogicTime ) );
    }

    //
    // This is totally not the right place the handle the windows messages, it should be inside a input_mgr but
    // since this demo is simple this will work for now.
    //
    if( isEditorMode() == false ) m_bLoop = m_RenderMgr.getWindow().HandleEvents();
}
