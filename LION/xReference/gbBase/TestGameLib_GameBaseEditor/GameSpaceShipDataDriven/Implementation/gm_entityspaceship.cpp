//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------
    
void entity_spaceship::ResetTimer( void ) noexcept
{
    getGlobalInterface().m_GameMgr.UseLinearTypes([&]( gb_game_graph::base::linear_types& LTypes )
    {
        m_WaitTimer = LTypes.m_SmallRnd.RandF32( 10.0f, 20.0f );
    }); 
}

//------------------------------------------------------------------------------------------------

void entity_spaceship::onExecute( void ) noexcept
{
    t_parent::onExecute();

    //
    // We have to wait until we can shot again
    //
    m_WaitTimer -= getGlobalInterface().m_GameMgr.getDeltaTime();
    if( m_WaitTimer > 0 ) 
        return;

    //
    // Collect the other ships
    //
    const xvector2               MyPos = m_pPhysics->getPos();
    xvector<physics_component*>  List;
    
    auto&       GameMgr = getGlobalInterface().m_GameMgr.SafeCast<game_mgr>();
    GameMgr.getPhysicsMgr().getClosestComponents( List, MyPos, 50.0f );

    //
    // Find the closest dot from me.
    //
    f32             ClosestDistance = 100000000.0f;
    s32             iClosest        = -1;

    for( s32 i=0; i<List.getCount(); i++ )
    {        
        const physics_component& Physics      = *List[i];

        // Skip ourselves
        if( m_pPhysics == &Physics )
            continue;

        // Make sure that are spaceship entities...
        if( Physics.getEntity().isKindOf<entity_spaceship>() == false )
            continue;

        // Check whether this component is the closest
        const f32 DistanceSquare = Physics.getPos().Dot( MyPos );

        if( DistanceSquare < ClosestDistance )
        {
            ClosestDistance = DistanceSquare;
            iClosest        = i;
        }
    }

    // We do not have anyone close to us
    if( iClosest == -1 )
        return;

    //
    // Find the closest dot from me.
    //
    const physics_component&            EnemyPhysics = *List[iClosest];
    xvector2                            Direction    = (EnemyPhysics.getVel() + EnemyPhysics.getPos()) - MyPos;
    Direction.NormalizeSafe();

    //
    // Create a bullet
    //
    //if( false )
    {
        auto& BulletBlueprint = GameMgr.m_BlueprintDB.getEntry( m_gBulletBlueprint );
        auto& BulletEntity    = BulletBlueprint.CreateEntity<entity_bullet>( gb_component::entity::guid( gb_component::entity::guid::RESET ), GameMgr );
        BulletEntity.linearSetup( getGuid() ).getComponent<physics_component>()->linearSetup( MyPos, Direction * 60.0f );
        BulletEntity.msgAddToWorld();
    }

    //
    // Reset timer
    //
    ResetTimer();
}

//------------------------------------------------------------------------------------------------

entity_spaceship::err entity_spaceship::onCheckResolve ( xvector<xstring>& IssuesList ) const noexcept
{
    bool bErrors = false;

    if( getComponent<physics_component>() == nullptr )
    {
        IssuesList.append( X_STR("EntityShip: We need to have a physics component for this entity") );
        bErrors = true;
    }

    if(bErrors) return x_error_code( errors, ERR_FAILURE, "EntityShip Errors" );
    return x_error_ok();
}

//------------------------------------------------------------------------------------------------

void entity_spaceship::onResolve ( void ) noexcept
{
    m_pPhysics = getComponent<physics_component>();
    x_assert(m_pPhysics);
    t_parent::onResolve();
}

//------------------------------------------------------------------------------------------------
entity_spaceship::prop_table& entity_spaceship::onPropertyTable ( void ) const noexcept  
{ 
    static prop_table E( this, this, X_STR("Entity-SpaceShip"), [&](xproperty_v2::table& E)
    {
        E.AddChildTable( t_parent::onPropertyTable() );
        m_gBulletBlueprint.AddAsProperty( E, X_STR("BulletBlueprint"), X_STR( "This is the blueprint that will be spawn for bullets" ) );
    });

    return E; 
}
