//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------

physics_component::prop_table& physics_component::const_data::onPropertyTable( void ) const noexcept 
{ 
    static prop_table E( this, this, X_STR("RenderConst"), [&](xproperty_v2::table& E)
    {
        E.AddChildTable( t_parent::onPropertyTable() );
        E.AddProperty      ( X_STR("IgnoreCollisions"),    m_bIgnoreCollisions,   X_STR("We will ignore collisions for this object." ) );
        E.AddProperty      ( X_STR("HandleCollisions"),    m_bHandleCollisions,   X_STR("We will handle collisions for this object." ) );
    });

    return E; 
}


//-------------------------------------------------------------------------------

xrect physics_component::getBounds( f32 delta ) const
{   
    const auto& T0      = getT0Data<mutable_data>();
    const f32   R       = 1;
    const f32   EnergyR = (T0.m_Vel * delta).getLength();
    const f32   X       = T0.m_Pos.m_X;
    const f32   Y       = T0.m_Pos.m_Y;
    const xrect Bounds  = xrect(X - R, Y - R, X + R, Y + R).Inflate( EnergyR, EnergyR );

    return Bounds;
}

//-------------------------------------------------------------------------------

physics_component::err physics_component::onCheckResolve ( xvector<xstring>& IssuesList ) const noexcept
{
    bool bErrors = false;

    if( isConstDataValid() == false )
    {
        IssuesList.append( X_STR("PhysicsComponent: Please make sure you set the constdata") );
        bErrors = true;
    }

    if(bErrors) return x_error_code( errors, ERR_FAILURE, "RenderComponent Errors" );
    return x_error_ok();
}

//-------------------------------------------------------------------------------

physics_component::prop_table& physics_component::onPropertyTable( void ) const noexcept 
{ 
    static prop_table E( this, this, X_STR("Physics-Spaceships"), [&](xproperty_v2::table& E)
    {
        E.AddChildTable( t_parent::onPropertyTable() );
        E.AddProperty<xvector2> ( X_STR("Position"),        GB_PROP_MUTABLE_DATA(m_Pos),            X_STR("Position of the ship"));
        E.AddProperty<xvector2> ( X_STR("Velocity"),        GB_PROP_MUTABLE_DATA(m_Vel),            X_STR("Velocity of the ship"));
    });

    return E; 
}

//-------------------------------------------------------------------------------

physics_component& physics_component::linearSetupRnd  ( void ) noexcept
{
    auto&               GameMgr     = getGlobalInterface().m_GameMgr.SafeCast<game_mgr>();
    auto&               PhysicsMgr  = GameMgr.getPhysicsMgr();
    auto                Bounds      = PhysicsMgr.getBounds();

    const float         XRes        = Bounds.m_Right;
    const float         YRes        = Bounds.m_Bottom;

    xvector2 Pos;
    xvector2 Vel;

    getGlobalInterface().m_GameMgr.UseLinearTypes( [&](gb_game_graph::base::linear_types& LinearTypes )
    {
        Pos.m_X = LinearTypes.m_SmallRnd.RandF32( 0, XRes );
        Pos.m_Y = LinearTypes.m_SmallRnd.RandF32( 0, YRes );

        x_constexprvar f32 VelMag = 4.0f*10;
        Vel.m_X = LinearTypes.m_SmallRnd.RandF32( -VelMag, VelMag );
        Vel.m_Y = LinearTypes.m_SmallRnd.RandF32( -VelMag, VelMag );
    });

    return linearSetup( Pos, Vel );
}

