//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//------------------------------------------------------------------------------------------------

entity_bullet::err entity_bullet::onCheckResolve ( xvector<xstring>& IssuesList ) const noexcept
{
    bool bErrors = false;

    if( getComponent<physics_component>() == nullptr )
    {
        IssuesList.append( X_STR("EntityBullet: We need to have a physics component for this entity") );
        bErrors = true;
    }

    if( getDelegates().SafeCast<delegates>().m_Collision.m_Guid.m_Value == 0 )
    {
        IssuesList.append( X_STR("EntityBullet: The Delegate Collision must be set by the physics component") );
        bErrors = true;
    }

    if(bErrors) return x_error_code( errors, ERR_FAILURE, "EntityBullet Errors" );
    return x_error_ok();
}

//------------------------------------------------------------------------------------------------

void entity_bullet::onExecute( void ) noexcept
{
    t_parent::onExecute();

    // When timer reaches zero then we destroy ourselves
    m_Timer -= getGlobalInterface().m_GameMgr.getDeltaTime();
    if( m_Timer <= 0 ) msgDestroy();
}

//------------------------------------------------------------------------------------------------

entity_bullet::prop_table& entity_bullet::onPropertyTable ( void ) const noexcept 
{ 
    static prop_table E( this, this, X_STR("EntityBullet"), [&](xproperty_v2::table& E)
    {
        E.AddChildTable( t_parent::onPropertyTable() );
        E.AddProperty  ( X_STR("Timer"),       m_Timer, X_STR( "Count down timer used before the bullet dies") );
        E.AddProperty  ( X_STR("ShipGuid"),    m_ShipGuid.m_Value, xproperty_v2::flags::MASK_READ_ONLY, X_STR( "The spaceship guid that shot the bullet. This is how the bullet knows its owner." ) );
    });

    return E; 
}

//------------------------------------------------------------------------------------------------

void entity_bullet::msgCollision( gb_component::entity& Entity )
{
    //
    // Deal with different types of collisions
    //
    if( !Entity.isKindOf<entity_bullet>() )
    {
        // If it is my own ship the we don't blow up
        if( Entity.getGuid() == m_ShipGuid )
            return;
    }

    // Bullet hits anything else then both things blow up
    msgDestroy();
    Entity.msgDestroy();
}
