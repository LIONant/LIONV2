//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------

void render_mgr::InitEngine( HINSTANCE hInstance )
{
    // Create engine instance
    {
        eng_instance::setup         Setup( (void*)hInstance );

        Setup.m_bValidation = false;

        auto Err = eng_instance::CreateInstance( m_Instance, Setup );
        x_assert( !Err );
    }

    // create a device
    {
        eng_device::setup    Setup;
        auto Err = m_Instance->CreateDevice( m_pDevice, Setup );
        x_assert( !Err );
    }

    // create a window 
    {
        eng_window::setup   Setup( *m_pDevice );
        auto Err = m_Instance->CreateWindow( m_pWindow, Setup );  
        x_assert( !Err );

        // MyEng.m_pWindow->setBgColor( xcolor( 255, 0,0,255 ));
    }

    //
    // Initialize the scene
    //
    m_View.setFov         ( xradian{ 91.5_deg } );
    m_View.setFarZ        ( 256.0f );
    m_View.setNearZ       ( 0.1f );
    m_View.LookAt( 2.5f, xradian3( 0_deg, 0_deg, 0_deg ), xvector3(0) );


    //
    // Let the engine deal with initial events
    //
    m_pWindow->HandleEvents();
}

//------------------------------------------------------------------------------------------------

void render_mgr::setupEditor( eng_window& Window )
{
    m_pDevice = &Window.getDevice();
    m_pWindow = &Window;
}

//------------------------------------------------------------------------------------------------

void render_mgr::FrameBegin ( void ) noexcept
{
    if( !getGameGraph().isEditorMode() ) 
    {
        m_pWindow->BeginRender( m_View );
    }
}

//------------------------------------------------------------------------------------------------

void render_mgr::FrameEnd ( void ) noexcept
{
    //
    // Tell the engine we are done rendering
    //
    if( !getGameGraph().isEditorMode() )
    {
        m_pWindow->EndRender();
        m_pWindow->PageFlip();
    } 
}

//------------------------------------------------------------------------------------------------

void render_mgr::onExecute ( void ) noexcept
{
    // Officially start the frame
    FrameBegin();


    eng_draw::pipeline PipeLine;

    PipeLine.m_BLEND   = eng_draw::BLEND_OFF;
    PipeLine.m_ZBUFFER = eng_draw::ZBUFFER_OFF;
    PipeLine.m_CULL    = eng_draw::CULL_OFF;
    m_View.setViewport( m_pWindow->getActiveView().getViewport() );

    //
    // Render ships
    //
    x_job_block JobBlock(X_WSTR("RenderingTypes"));
    for( auto& pType : m_TypeHead ) 
    {
        //
        // We want each type to be render completely separately 
        //
        JobBlock.SubmitJob( [pType,this,PipeLine]()
        {
            //
            // Spread the work across multiple jobs
            //
            x_job_block JobBlock(X_WSTR("RenderingComponents"));       //g_context::get().m_Scheduler.getWorkerCount()*3, 
            JobBlock.ForeachLog( pType->getInWorld(), 8, 50, [this,PipeLine]( xbuffer_view<gb_component::base*> CompList )
            {
                //
                // Determine how many triangles and how many squares we need to draw 
                //
                int nSquares   = 0;
                int nTriangles = 0;

                //
                // Count triangles and square
                //
                for( gb_component::base* pEntry : CompList )
                {
                    const auto& Render    = pEntry->SafeCast<render_component>();
                    if( Render.getConstData<render_component::const_data>().m_Shape == render_component::const_data::shape::TRIANGLE ) nTriangles++;
                    else                                                                                                               nSquares++;
                }

                //
                // Render the actual ships and bullets
                //
                auto& CmdList = m_pWindow->getDisplayCmdList( 2 );
                CmdList.Draw( PipeLine, [&]( eng_draw& Draw )
                {
                    //
                    // Allocate all the triangles and square vertices and indices
                    //
                    const int         IndexCount        = nSquares * 6 + nTriangles * 3;
                    eng_draw::buffers PrimBuf           = Draw.popBuffers( nSquares * 4 + nTriangles * 3, IndexCount );
                    int               VertPrimOffset    = 0;
                    int               IndexPrimOffset   = 0;

                        x_constexprvar f32 s = 1.5f;
                    for( gb_component::base* pEntry : CompList )
                    {
                        auto& Render    = pEntry->SafeCast<render_component>();

                        // Let the component update all its messages
                        //Render.qt_onRun();

                        const auto& ConstData   = Render.getConstData<render_component::const_data>();
                        auto  Vel               = Render.m_Physics.m_pValue->getVel();
                        auto  Pos               = Render.m_Physics.m_pValue->getPos();

                        if( ConstData.m_Shape == render_component::const_data::shape::TRIANGLE )
                        {
                            if( Vel.getLengthSquared() < 0.0001f )
                            {
                                Vel.m_X = 0;
                                Vel.m_Y = 1;
                            }
                            else
                            {
                                Vel.Normalize();
                            }

                            xvector2 V1 = Vel * s*4;
                            xvector2 V2 = Vel * s;
                            V2.Rotate( PI_OVER2 );

                            PrimBuf.m_Vertices[VertPrimOffset+0].setup( xvector3d( Pos.m_X + V1.m_X, Pos.m_Y + V1.m_Y, 0), ConstData.m_Color );
                            PrimBuf.m_Vertices[VertPrimOffset+1].setup( xvector3d( Pos.m_X + V2.m_X, Pos.m_Y + V2.m_Y, 0), ConstData.m_Color );
                            PrimBuf.m_Vertices[VertPrimOffset+2].setup( xvector3d( Pos.m_X - V2.m_X, Pos.m_Y - V2.m_Y, 0), ConstData.m_Color );
        
                            PrimBuf.m_Indices[IndexPrimOffset+0] = VertPrimOffset+0;
                            PrimBuf.m_Indices[IndexPrimOffset+1] = VertPrimOffset+1;
                            PrimBuf.m_Indices[IndexPrimOffset+2] = VertPrimOffset+2;

                            VertPrimOffset  += 3;
                            IndexPrimOffset += 3;
                        }
                        else 
                        {
                            PrimBuf.m_Vertices[VertPrimOffset+0].setup( xvector3d( Pos.m_X-s, Pos.m_Y-s, 0), ConstData.m_Color );
                            PrimBuf.m_Vertices[VertPrimOffset+1].setup( xvector3d( Pos.m_X+s, Pos.m_Y-s, 0), ConstData.m_Color );
                            PrimBuf.m_Vertices[VertPrimOffset+2].setup( xvector3d( Pos.m_X+s, Pos.m_Y+s, 0), ConstData.m_Color );
                            PrimBuf.m_Vertices[VertPrimOffset+3].setup( xvector3d( Pos.m_X-s, Pos.m_Y+s, 0), ConstData.m_Color );
        
                            PrimBuf.m_Indices[IndexPrimOffset+0] = VertPrimOffset+0;
                            PrimBuf.m_Indices[IndexPrimOffset+1] = VertPrimOffset+1;
                            PrimBuf.m_Indices[IndexPrimOffset+2] = VertPrimOffset+2;
        
                            PrimBuf.m_Indices[IndexPrimOffset+3] = VertPrimOffset+0;
                            PrimBuf.m_Indices[IndexPrimOffset+4] = VertPrimOffset+2;
                            PrimBuf.m_Indices[IndexPrimOffset+5] = VertPrimOffset+3;

                            VertPrimOffset  += 4;
                            IndexPrimOffset += 6;
                        }
                    }

                    //
                    // Now that we have all the primitives in the buffer we can draw all with a single call
                    //
                    Draw.DrawBufferTriangles( m_View.getPixelBased2D(), 0, IndexCount );
                });

                //
                // Submit the cmd list to the renderer
                //
                m_pWindow->SubmitDisplayCmdList( CmdList );
            });
            JobBlock.Join();
        });
    }
    JobBlock.Join();

    //
    // Notify anyone that wants to render (this could be done in a different thread)
    //
    //if( false )
    {
        auto& CmdList = m_pWindow->getDisplayCmdList( 2 );
        CmdList.Draw( PipeLine, [&]( eng_draw& Draw )
        {
            Draw.ClearSampler();
            Draw.ClearScissor();
            m_Events.m_DebugRender.Notify( Draw, m_View );
        });
        m_pWindow->SubmitDisplayCmdList( CmdList );
    }

    //
    // Officially End the frame
    //
    FrameEnd();
}

