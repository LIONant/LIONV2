//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//---------------------------------------------------------------------------------

class spawner_component : public gb_component::base
{
    x_object_type( spawner_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

public: // --- class traits ---

    // This structure contains detail information about our component that other system need
    struct t_descriptors : t_parent::t_descriptors
    {
        x_constexprvar auto             t_category_string           = X_USTR("General/Spawner");
        x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Spawner" };
        x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
        x_constexprvar def              t_definition                = def::MASK_DEFAULT;
        using                           t_type                      = gb_component::type_pool<t_self>;
    };

protected: 
        
    x_forceinline                       spawner_component       ( const base_construct_info& C )                    noexcept : t_parent( C ){};
    virtual         prop_table&         onPropertyTable         ( void )                                            const noexcept override 
    { 
        static prop_table E( this, this, X_STR("Spawner"), [&](xproperty_v2::table& E)
        {
            E.AddChildTable( t_parent::onPropertyTable() );

            E.AddProperty                   ( X_STR("ReleaseCount"),  m_ReleaseCount,   X_STR("Number of entities that will be spawned in release mode." ) );
            E.AddProperty                   ( X_STR("DebugCount"),    m_DebugCount,   X_STR("Number of entities that will be spawned in debug mode." ) );
            m_gBlueprint.AddAsProperty      ( E, X_STR("Blueprint") );
        });

        return E; 
    }

    virtual err onCheckResolve ( xvector<xstring>& IssuesList ) const noexcept
    {
        if( m_gBlueprint.m_Value == 0 )
        {
            IssuesList.append( X_STR("SpawnComponent: Warning without assigning a blueprint it wont spawn anything") );    
        }
        return x_error_ok();
    }

    virtual void onExecute( void ) noexcept override
    {
        t_parent::onExecute();
        if( m_gBlueprint.m_Value == 0 ) return;

    #if _X_RELEASE
        const int Count = m_ReleaseCount;
    #else
        const int Count = m_DebugCount;
    #endif
        auto&               GameGraph = getGlobalInterface().m_GameMgr;
        auto&               Blueprint = GameGraph.m_BlueprintDB.getEntry( m_gBlueprint );
        
    #if _X_RELEASE
        x_job_block         JobBlock(X_WSTR("SpawnerCreateEntity"));
    #endif
        for( s32 i=0; i<Count; i++ )
        {
    #if _X_RELEASE
            JobBlock.SubmitJob( [this,&Blueprint,&GameGraph]()
            {
    #endif

                auto& Entity = Blueprint.CreateInstance( gb_component::entity::guid( gb_component::entity::guid::RESET ), GameGraph );
                auto pPhysics = Entity.getComponent<physics_component>();
                if(pPhysics) pPhysics->linearSetupRnd();
                Entity.msgAddToWorld();
    #if _X_RELEASE
            });
    #endif
        } 
    #if _X_RELEASE
        JobBlock.Join();
    #endif

        m_Entity.msgDestroy();
    }

protected: 

    gb_blueprint::guid          m_gBlueprint    {nullptr};
    int                         m_ReleaseCount  {0};
    int                         m_DebugCount    {0};
};

