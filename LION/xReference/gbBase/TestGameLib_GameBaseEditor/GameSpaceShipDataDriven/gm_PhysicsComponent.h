//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
class physics_mgr;

//---------------------------------------------------------------------------------
// BUILDING MY OWN ENTITY
//---------------------------------------------------------------------------------
class physics_component 
    : public gb_component::base
    , public gb_component::interface_base
{
    x_object_type( physics_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

public: // --- class traits ---

    // This structure contains detail information about our component that other system need
    struct t_descriptors : t_parent::t_descriptors
    {
        x_constexprvar auto             t_category_string           = X_USTR("Physics/ShipsPhy");
        x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Physics-Spaceships" };
        x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
        x_constexprvar def              t_definition                = def::MASK_DEFAULT;
        using                           t_type                      = gb_component::type_pool<t_self>;
    };

    static const gb_component::interface_type& getInterfaceType( void )      
    {                                                           
        x_constexprvar gb_component::interface_type EventType         
        {                                                       
            X_STR("PhysicsComponent"),                            
            gb_component::interface_type::guid{ "PhysicsComponentInterface" }, 
            X_STR("physics component interface")                                         
        };                                                      
        return EventType;                                       
    }      

    // Defines the events
    GB_COMPONENT_EVENT_TYPE_DEF( event_Collision, "physics_component::Collision", "This event is sent every time the components collides with something", gb_component::entity& )

protected:

    // Customization of the physics for this particular example
    struct const_data : public t_parent::const_data
    {
        x_object_type( const_data, is_quantum_lock_free, rtti( gb_component::base::const_data), is_not_copyable, is_not_movable )
    
        x_constexprvar type t_type = 
        { 
            static_cast<const_data*>(nullptr),
            "space_ship_game/physics_component/const_data",
            X_STR("space_ship_game/physics_component"), 
            X_STR("No help!")
        };

        constexpr                       const_data          ( const guid Guid ) : t_parent::const_data( Guid ) {}
        virtual         const type&     getType             ( void )            const   noexcept override           { return t_type; }
        virtual         prop_table&     onPropertyTable     ( void )            const   noexcept override;

        bool        m_bIgnoreCollisions = false;
        bool        m_bHandleCollisions = false;
    };

    struct events : public gb_component::events
    {
        x_object_type( events, is_linear, rtti(gb_component::events), is_not_copyable, is_not_movable )

        events( void ) noexcept = default;
        event_Collision     m_Collision;

        struct table : t_parent::table
        {
            const definition m_Collision        { offsetof( events, m_Collision ), X_STR("OnCollision"), guid{"physics_component::OnCollision"}, event_Collision::getType() };
            const definition m_ClassInterface   { x_offsetof_base<physics_component, physics_component>(), X_STR("iPhysics"), guid{"iPhysics"}, getInterfaceType() };
        };
    };

public: 

                    xrect       getBounds       ( f32 delta )                               const;
    x_forceinline   void        msgUpdatePosVel ( const xvector2 Pos, const xvector2 Vel )          noexcept { SendMsg( [this, Pos, Vel](){ this->onUpdatePosVel( Pos, Vel ); } ); }
    x_forceinline   xvector2    getPos          ( void )                                    const   noexcept { return getT0Data<mutable_data>().m_Pos; }
    x_forceinline   xvector2    getVel          ( void )                                    const   noexcept { return getT0Data<mutable_data>().m_Vel; }
    x_forceinline   auto&       linearSetup     ( const xvector2& Pos, const xvector2& Vel )        noexcept
    {
        auto& T1 = getT1Data<mutable_data>();
        T1.m_Pos        = Pos;
        T1.m_Vel        = Vel;
        return *this;
    }
    x_incppfile   physics_component&       linearSetupRnd  ( void )                                            noexcept;

protected: 

    struct mutable_data : t_parent::mutable_data
    {
        virtual void onUpdateMutableData( const t_mutable_base& Src ) noexcept override 
        {
            const mutable_data& NewSrc = reinterpret_cast<const mutable_data&>(Src);
            m_Pos = NewSrc.m_Pos;
            m_Vel = NewSrc.m_Vel;
        } 

        xvector2                        m_Pos{0,0};
        xvector2                        m_Vel{0,0};
        physics_component*              m_pNext{nullptr};  
    };

protected: 
        
    x_forceinline                       physics_component       ( const base_construct_info& C )                            noexcept : t_parent( C ){};
    virtual         err                 onCheckResolve          ( xvector<xstring>& WarningList )                   const   noexcept override;
    virtual         prop_table&         onPropertyTable         ( void )                                            const   noexcept override;
    x_forceinline   void                onUpdatePosVel          ( const xvector2 Pos, const xvector2 Vel )
    {
        mutable_data& T1 = getT1Data<mutable_data>();
        T1.m_Pos = Pos;
        T1.m_Vel = Vel;
    }

protected: 

    friend class physics_mgr;
    using gb_component::base::qt_onRun;
};

