//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class render_mgr final : public gb_component_mgr::base
{
    x_object_type( render_mgr, is_quantum_lock_free, rtti(gb_component_mgr::base), is_not_copyable, is_not_movable )

public: 
    
    struct events
    {
        x_message::event<eng_draw&, const eng_view&> m_DebugRender;
        
    } m_Events;

public: 
                            render_mgr                  ( gb_game_graph::base& GameGraph ) : 
                                gb_component_mgr::base
                                {
                                    guid{"RenderMgr"},
                                    GameGraph,
                                    X_USTR("RenderMgr")
                                } { setupAffinity( affinity::AFFINITY_MAIN_THREAD ); }
    void                    InitEngine                  ( HINSTANCE hInstance = GetModuleHandle(NULL) );
    void                    setupEditor                 ( eng_window& Window );
    auto&                   getWindow                   ( void ) { return *m_pWindow; }
    void                    RenderEditorFrame           ( void ) { onExecute(); }

protected:

    virtual         void                    onExecute               ( void )                noexcept override;
    x_incppfile     void                    FrameBegin              ( void )                noexcept;
    x_incppfile     void                    FrameEnd                ( void )                noexcept;

protected:

    xndptr_s<eng_instance>      m_Instance      {};
    eng_device*                 m_pDevice       { nullptr };
    eng_window*                 m_pWindow       { nullptr };
    eng_view                    m_View          {};

protected:
        
};
