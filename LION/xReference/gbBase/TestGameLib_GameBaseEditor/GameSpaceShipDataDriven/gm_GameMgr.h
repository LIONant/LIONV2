//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class game_mgr final : public gb_game_graph::base
{
    x_object_type( game_mgr, 
        is_quantum_lock_free, 
        rtti(gb_game_graph::base), 
        is_not_copyable, 
        is_not_movable )
  
public: // --- class constructor ---

    x_constexprvar xuptr t_max_entities { 300000 };

            game_mgr        ( void ) noexcept : t_parent( t_max_entities ) {}
    auto&   getPhysicsMgr   ( void ) { return m_PhysicsMgr; }
    auto&   getRenderMgr    ( void ) { return m_RenderMgr; }

   
protected: // --- class types ---

    using ai_mgr        = gb_component_mgr::base;
    using ai_sync       = gb_game_graph::sync_point;
    using physics_sync  = gb_game_graph::sync_point;

protected: // --- class hierarchy functions ---
        
    virtual void onInitialize       ( void ) noexcept override;
    virtual void onEndFrame         ( void ) noexcept override;
    virtual void onBeginFrame       ( void ) noexcept override;
    virtual void onRenderEditorFrame( void ) noexcept override;

protected:

    ai_sync                 m_AISyncPoint       {};
    physics_sync            m_PhysicsSyncPoint  {};

    physics_mgr             m_PhysicsMgr        { *this };
    ai_mgr                  m_AIMgr             { ai_mgr::guid{"AIMgr"}, *this, X_USTR("AIMgr") };
    render_mgr              m_RenderMgr         { *this };
};
