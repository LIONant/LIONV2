//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class entity_bullet : public gb_component::entity
{
    x_object_type( entity_bullet, is_quantum_lock_free, rtti(gb_component::entity), is_not_copyable, is_not_movable )

public: // --- class traits ---
        
    struct t_descriptors : t_parent::t_descriptors
    {
        x_constexprvar auto             t_category_string           = X_USTR("Entity/Bullet");
        x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Entity/Bullet::SpaceShip" };
        x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
        using                           t_type                      = gb_component::type_entity_pool<t_self>;
        x_constexprvar def              t_definition                = def::MASK_DEFAULT;
    };

public:

    auto getShipGuid( void ) const { return m_ShipGuid; }
    auto& linearSetup( gb_component::entity::guid gWhoShotMe ){ m_ShipGuid = gWhoShotMe; return *this; }

        
    struct delegates : public gb_component::entity::delegates
    {
        x_object_type( delegates, is_linear, rtti(gb_component::entity::delegates), is_not_copyable, is_not_movable )
        delegates(void) = default;
            
        physics_component::event_Collision::delegate<entity_bullet>    m_Collision     { &entity_bullet::msgCollision };

        struct table : t_parent::table
        {
            const definition m_CollisionDef{ offsetof(delegates, m_Collision), X_STR("Collision"), physics_component::event_Collision::getType() };
        };
    };

protected: // --- class hierarchy functions ---
        
    x_forceinline                       entity_bullet       ( const base_construct_info& C ) noexcept : t_parent( C ) {}
    virtual         err                 onCheckResolve      ( xvector<xstring>& IssuesList ) const noexcept;
    virtual         void                onExecute           ( void ) noexcept override;
    virtual         prop_table&         onPropertyTable     ( void ) const noexcept override; 
    x_incppfile     void                msgCollision        ( gb_component::entity& Entity );

protected:  // --- class hierarchy hidden variables ---
        
    float                                                   m_Timer                 { 5 };
    gb_component::entity::guid                              m_ShipGuid              { nullptr };
};

