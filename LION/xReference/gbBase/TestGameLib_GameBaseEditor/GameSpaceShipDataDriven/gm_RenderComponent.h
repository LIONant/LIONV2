//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
class render_mgr;

class render_component : public gb_component::base
{
    x_object_type( render_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

public: // --- class traits ---

    // This structure contains detail information about our component that other system need
    struct t_descriptors : t_parent::t_descriptors
    {
        x_constexprvar auto             t_category_string           = X_USTR("Render/SpaceshipsRender");
        x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{"Render-SpaceshipsRender" };
        x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
        x_constexprvar def              t_definition                = def::MASK_DEFAULT;
        using                           t_type                      = gb_component::type_pool<t_self>;
    };

protected: 

    // Customization of the physics for this particular example
    struct const_data : public t_parent::const_data
    {
        x_object_type( const_data, is_quantum_lock_free, rtti( gb_component::base::const_data), is_not_copyable, is_not_movable )
    
        x_constexprvar type t_type = 
        { 
            static_cast<const_data*>(nullptr),
            "Render/SpaceshipsRender::const_data",
            X_STR("space_ship_game/render_component"),
            X_STR("Details for the rendering component such the shape of the primitive and its color") 
        };

        enum class shape
        {
            TRIANGLE,
            SQUARE
        };

        virtual         prop_table&         onPropertyTable         ( void )                                            const noexcept override 
        { 
            static prop_table E( this, this, X_STR("Render-SpaceShip"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( t_parent::onPropertyTable() );
                E.AddProperty      ( X_STR("Color"),    m_Color,   X_STR("This is the color of the shape that we will render." ) );

                static const xarray< std::pair<int, xconst_str<xchar>>, 2> ShapeEnumList
                {
                    std::pair<int, xconst_str<xchar>>{ static_cast<int>(shape::TRIANGLE),   X_STR("Triangle")       }
                    ,   std::pair<int, xconst_str<xchar>>{ static_cast<int>(shape::SQUARE),     X_STR("Square")         }
                };
                E.AddProperty( X_STR("Shape"), m_Shape, xproperty_v2::info::enum_t( ShapeEnumList ), X_STR("Choose the shape that we will use to render with") );
            });

            return E; 
        }

        x_forceconst                const_data          ( const guid Guid ) : t_parent::const_data( Guid ) {}
        virtual       const type&   getType             ( void )            const   noexcept            { return t_type; }

        xcolor                      m_Color     { ~0 };
        shape                       m_Shape     { shape::SQUARE };
    };

    struct delegates : public gb_component::delegates
    {
        x_object_type( delegates, is_linear, rtti(gb_component::delegates), is_not_copyable, is_not_movable )

        delegates(void)=default;

        struct table : t_parent::table
        {
            definition  m_riPhysics { offsetof( render_component, m_Physics ),   X_STR("riPhysics"),     physics_component::getInterfaceType() };
        };
    };

protected: 

    x_forceinline                       render_component        ( const base_construct_info& C )            noexcept : gb_component::base(C) {};
    virtual         void                onResolve               ( void )                                    noexcept override;
    virtual         err                 onCheckResolve          ( xvector<xstring>& WarningList )   const   noexcept override;
    virtual         prop_table&         onPropertyTable         ( void )                            const   noexcept override 
    { 
        static prop_table E( this, this, X_STR("Render-SpaceShip"), [&](xproperty_v2::table& E)
        {
            E.AddChildTable( t_parent::onPropertyTable() );
        });

        return E; 
    }

protected: 
    
    gb_component::interface_ref<physics_component>  m_Physics;

protected: 

    friend class render_mgr;
    using gb_component::base::qt_onRun;
};

