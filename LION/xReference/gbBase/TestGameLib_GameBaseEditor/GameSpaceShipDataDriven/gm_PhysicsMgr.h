//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class physics_mgr final : public gb_component_mgr::base
{
    x_object_type( physics_mgr, is_quantum_lock_free, rtti(gb_component_mgr::base), is_not_copyable, is_not_movable )

public: 

    x_forceinline           physics_mgr                 ( gb_game_graph::base& GameGraph )                     noexcept : 
        gb_component_mgr::base
        { 
            guid{x_constStrCRC32("PhysicsMgr")}, 
            GameGraph, 
            X_USTR("PhysicsMgr") 
        } {}
    const xrect&            getBounds                   ( void )                                                                                    noexcept { return m_Bounds; }
    void                    getClosestComponents        ( xvector<physics_component*>& lComp, const xvector2 Point, const f32 Radius )  const       noexcept;
    void                    Initialize                  ( f32 Width, f32 Height )                                                                   noexcept;

protected:

    struct cell
    {
        xarray<x_atomic<physics_component*>,2> m_Head{ nullptr, nullptr };
    };

    x_constexprvar int                              m_nRows     = 64;
    x_constexprvar int                              m_nColumns  = 64;

    using world = xarray<cell,m_nRows*m_nColumns>; 

protected:

    virtual         void                    onExecute               ( void )                                    noexcept override;
    x_incppfile     void                    msgDebugRender          ( eng_draw& Draw, const eng_view& View )    noexcept;
    virtual         void                    onAddToWorld            ( gb_component::base& Component )           noexcept;
    virtual         void                    onRemoveFromWorld       ( gb_component::base& Component )           noexcept;
    virtual         void                    onDestroy               ( gb_component::base& Component )           noexcept;
    virtual         void                    onEndOfFrameCleanUp     ( void )                                    noexcept;
    virtual         void                    onReset                 ( void )                                    noexcept;

    x_forceinline   int                     getGridXUnsafe          ( const float x )                  const    noexcept { const auto Index = static_cast<s32>( (x - m_Bounds.m_Left)*(m_nColumns/m_Bounds.getWidth()  )); x_assert( Index < m_nColumns ); return Index; }         
    x_forceinline   int                     getGridYUnsafe          ( const float y )                  const    noexcept { const auto Index = static_cast<s32>( (y - m_Bounds.m_Top )*(m_nRows   /m_Bounds.getHeight() )); x_assert( Index < m_nRows );    return Index; }         
    x_forceinline   int                     getGridIndexUnsafe      ( const xvector2& Pos )            const    noexcept { const auto Index = getGridYUnsafe( Pos.m_Y ) * m_nColumns + getGridXUnsafe( Pos.m_X ); x_assert( Index < m_nRows*m_nColumns ); return Index; }         
    x_forceinline   int                     getGridXSafe            ( const float x )                  const    noexcept { const auto Index = x_Range(static_cast<s32>((x - m_Bounds.m_Left)*(m_nColumns / m_Bounds.getWidth())),  0, m_nColumns - 1 ); x_assert( Index < m_nColumns ); return Index; }
    x_forceinline   int                     getGridYSafe            ( const float y )                  const    noexcept { const auto Index = x_Range(static_cast<s32>((y - m_Bounds.m_Top) *(m_nRows    / m_Bounds.getHeight())), 0, m_nRows    - 1 ); x_assert( Index < m_nRows );    return Index; }
    x_forceinline   int                     getGridIndexSafe        ( const xvector2& Pos )            const    noexcept { const auto Index = getGridYSafe( Pos.m_Y ) * m_nColumns + getGridXSafe( Pos.m_X ); x_assert( Index < m_nRows*m_nColumns ); return Index;}         
 
    x_forceinline   auto&                   getDelayDeleteList      ( void )                                    noexcept { return (m_iDeleteList&1)==0? m_DestroyList  : m_DestroyList2; } 
    x_forceinline   auto&                   getDeleteList           ( void )                                    noexcept { return (m_iDeleteList&1)==0? m_DestroyList2 : m_DestroyList;  } 

protected:

    xrect                                           m_Bounds;
    world                                           m_World;
    xvector<gb_component::base*>                    m_DestroyList2      {};
    u8                                              m_iDeleteList = 0;

    x_message::delegate<physics_mgr, eng_draw&, const eng_view&> m_DebugRenderDelegate{ *this, &physics_mgr::msgDebugRender };
};
