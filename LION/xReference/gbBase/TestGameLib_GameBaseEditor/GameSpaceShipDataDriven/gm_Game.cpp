//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "gm_Game.h"

namespace game
{
    #include "Implementation/gm_EntityBullet.cpp"
    #include "Implementation/gm_EntitySpaceship.cpp"
    #include "Implementation/gm_GameMgr.cpp"
    #include "Implementation/gm_PhysicsComponent.cpp"
    #include "Implementation/gm_PhysicsMgr.cpp"
    #include "Implementation/gm_RenderComponent.cpp"
    #include "Implementation/gm_RenderMgr.cpp"
}

#if (_GB_BASE_DYNAMIC == false) && (_X_TARGET_DYNAMIC_LIB) 

__declspec(dllexport) void* CreateInstance( g_context& G, eng_window& Window )
{
    //
    // Hookup the low level libraries
    //
    g_context::Import( G );

    //
    // Create an instance of the game
    //
    xndptr_s<game::game_mgr> xpGameMgr;
    xpGameMgr.New();

    //
    // Setup the rendering
    //
    auto& RenderMgr = xpGameMgr->getRenderMgr();
    RenderMgr.setupEditor( Window );

    //
    // Officially return the pointer
    //
    return xpGameMgr.TransferOwnership();
}

#endif 

