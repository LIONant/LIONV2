//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      This are the macros that the user can override to change the app behavior
//      You should clone this file into your solution folder and change any of
//      these at will. In fact this file could really be a per user file.
//      Meaning it probably should not be check in into your source control.
//------------------------------------------------------------------------------
#define X_USER_SETTINGS_ASSERTS         false   // true to force asserts even in release
#define X_USER_SETTINGS_DEBUG_SYSTEMS   false   // true to force debug systems in release
#define X_USER_SETTINGS_EDITOR          false   // Indicate that this is an editor build
#define X_USER_SETTINGS_JOB_PROFILER    true     

#if _X_DEBUG
#define X_USER_SETTINGS_LOG             true    // turn logging on
#else
#define X_USER_SETTINGS_LOG             false    
#endif