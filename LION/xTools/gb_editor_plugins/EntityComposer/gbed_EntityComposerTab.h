//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


struct types_info
{
    using guid = imgui_node_graph::node::connection_pod::type::guid;

    x_constexprvar xcolor   s_EntityColor               { 151+10, 103+10, 255, 253 };
    x_constexprvar guid     s_EntityClassGuid           { gb_component::entity::t_class_guid.m_Value };
    x_constexprvar xcolor   s_ComponentColor            { 0, 150, 0, 255 };
    x_constexprvar guid     s_ComponentClassGuid        { gb_component::base::t_class_guid.m_Value };
    x_constexprvar xcolor   s_GameColor                 { 255, 30, 0 };
    x_constexprvar guid     s_GameGuid                  { 1u };
    x_constexprvar xcolor   s_EntityColorRef            { 151, 103, 253, 253 };
    x_constexprvar guid     s_EntityClassGuidRef        { gb_component::entity::t_class_guid.m_Value + 1 };
    x_constexprvar xcolor   s_BlueprintColor            { 100, 100, 200 };
    x_constexprvar guid     s_BlueprintClassGuid        { gb_blueprint::master::t_class_guid.m_Value };
    x_constexprvar xcolor   s_BlueprintCreateColor      { 100+20, 100+20, 200+20 };
    x_constexprvar guid     s_BlueprintCreateClassGuid  { gb_blueprint::master::t_class_guid.m_Value + 1 };
    x_constexprvar xcolor   s_ConstantDataColor         { 200, 100, 200, 255 };
    x_constexprvar guid     s_ConstantDataClassGuid     { 100u };
    x_constexprvar xcolor   s_EventDataColor            { 200, 200, 0, 255 };
    x_constexprvar xcolor   s_InterfaceDataColor        { 200, 100, 100, 255 };
};

class tab 
    : public imgui_node_graph::base
    , public xproperty_v2::base
    , public gbed_plugins::tab
{
    x_object_type( tab, rtti( imgui_node_graph::base, xproperty_v2::base, gbed_plugins::tab ) );

public:

    virtual const xproperty_v2::table& onPropertyTable( void ) const override;

public:

    x_incppfile                         tab                             ( const xstring::const_str& Str, gbed_frame::base& EditorFrame ); 
    x_incppfile         void            RenderInspector                 ( void );
    x_inline            bool            isEditingBlueprint              ( void ) const { return !!m_pBlueprint; }
    x_inline            bool            isEditingEntity                 ( void ) const { if( isEditingBlueprint() ) return false; return !!m_pEntity; }
    x_incppfile         void            Clear                           ( void );
    x_inline            auto*           getActiveBlueprint              ( void )                    { return m_pBlueprint; }
    x_inline            auto*           getEntity                       ( void ) const              { return m_pEntity; }
    virtual             const type&     getType                         ( void ) override; 
    x_inline            auto&           getLayerDoc                     ( void ) { return m_LayerDoc; }
    x_inline            auto&           getBlueprintDoc                 ( void ) { return m_BlueprintDoc; }


protected:

    struct graph_node;
    struct trick;
    struct editor_node;
    struct blue_print_node;
    struct entity_node;

    enum class open_selector
    {
        NONE
        , ENTITY
        , ENTITY_REF
        , COMPONENT
        , CONSTANT_DATA
        , BLUEPRINT
        , BLUEPRINT_CREATE
    };

protected:

    x_incppfile         void            SetBlueprint                    ( gb_blueprint::master* pBluePrint );
    x_incppfile         void            SetEntity                       ( gb_component::entity* pEntity, bool bSkipClear = false );

    x_incppfile         void            AddEditorNode                   ( void );

    x_incppfile         ImVec2          AddEditorNode                   (   const imgui_node_graph::node::guid                      gGameNodeGUID   
                                                                            , const imgui_node_graph::node::connection_pod::guid    gGameNodePodGUID
                                                                            , ImVec2                                                Pos );
    x_incppfile         void            AddEvents                       ( graph_node& Node, gb_component::base& Base );
    x_incppfile         ImVec2          AddEntity                       (   const imgui_node_graph::node::guid                      gEntityNodeGUID
                                                                            , const imgui_node_graph::node::guid                    gGameNodeGUID
                                                                            , const imgui_node_graph::node::connection_pod::guid    gGameNodePod
                                                                            , const imgui_node_graph::node::connection_pod::guid    gComponentPod
                                                                            , const imgui_node_graph::node::connection_pod::guid    gEntityPod
                                                                            , const imgui_node_graph::node::connection_pod::guid    gBlueprintPod
                                                                            , gb_component::base*                                   pComponent
                                                                            , ImVec2                                                Pos );
    x_incppfile         ImVec2          AddBlueprintEditNode            (   const imgui_node_graph::node::guid                      gEntityNodeGUID
                                                                            , const imgui_node_graph::node::connection_pod::guid    gEntityBlueprintPod
                                                                            , gb_blueprint::master*                                 pBlueprint
                                                                            , ImVec2                                                Pos );
    x_incppfile         ImVec2          AddComponent                    (   const imgui_node_graph::node::guid                      gEntityNodeGUID
                                                                            , const imgui_node_graph::node::connection_pod::guid    gEntityComponentPod
                                                                            , gb_component::base*                                   pComponent
                                                                            , ImVec2                                                Pos );
    x_incppfile         ImVec2          AddBlueprintNode                (   const imgui_node_graph::node::guid                      gComponentNodeGuid 
                                                                            , const imgui_node_graph::node::connection_pod::guid    gComponentBlueprintPod
                                                                            , const gb_blueprint::master::guid                      gBlueprintInstance
                                                                            , ImVec2                                                Pos );
    x_incppfile         ImVec2          AddEntityRefNode                (   const imgui_node_graph::node::guid                      gComponentNodeGuid 
                                                                            , const imgui_node_graph::node::connection_pod::guid    gComponentBlueprintPod
                                                                            , const gb_component::entity::guid                      gEntityInstance
                                                                            , ImVec2                                                Pos );
    x_incppfile         ImVec2          AddConstantNode                 (   const gb_component::const_data::guid                    gConstantDataInstace  
                                                                            , const gb_component::const_data::type::guid            gConstantDataType 
                                                                            , const imgui_node_graph::node::guid                    gNodeGuid 
                                                                            , const imgui_node_graph::node::connection_pod::guid    gConstantDataPod
                                                                            , ImVec2                                                Pos );
    x_incppfile         ImVec2          AddNodeProperties               ( graph_node& NodeTest, const imgui_node_graph::node::guid gNodeGuid, ImVec2 Pos );
    x_incppfile         void            HandleConstantDataSelector      ( void );
    x_incppfile         void            HandleBlueprintSelector         ( void );
    x_incppfile         void            HandleEntityRefSelector         ( void );
    x_incppfile         void            HandleEntitySelector            ( void );
    x_incppfile         void            HandleComponentSelector         ( void );
    x_incppfile         void            ChooseComponentSelector         ( void );
    virtual             void            onRender                        ( void ) noexcept override;
    virtual             void            onDeleteComponent               ( const imgui_node_graph::node& Node ) override;
    virtual             void            onDeleteLink                    ( const imgui_node_graph::link& Link ) override;
    virtual             void            onNotifyNewLinkConnection       ( const imgui_node_graph::link& Link ) override;
    virtual             bool            onLinkDrop                      ( imgui_node_graph::node::connection_pod& Pod, ImVec2 Pos ) override;
    virtual             void            onCloseProject                  ( void ) override { Clear(); }
    virtual             void            onAddNode                       ( imgui_node_graph::node& Node ) override;

    x_incppfile         void            msgSelChangeGeneral             ( const gbed_selection::document&, xproperty_v2::base* );
    x_incppfile         void            msgSelChangeEntity              ( const gbed_selection::document&, xproperty_v2::base* );
    x_incppfile         void            msgSelChangeBlueprint           ( const gbed_selection::document&, xproperty_v2::base* );
    x_incppfile         void            msgChangeHappen                 ( xproperty_v2::base& Prop );
    x_incppfile         void            msgNotifyDelete                 ( xproperty_v2::base& Prop );
    x_incppfile         void            msgQueryReloadGameMgr           ( xvector<xstring>& Issues );
    x_incppfile         void            SingleSelect                    ( gb_component::entity::guid gGuid,     xproperty_v2::base* pProps );
    x_incppfile         void            SingleSelect                    ( gb_blueprint::guid gGuid,             xproperty_v2::base* pProps );
    x_incppfile         void            SingleSelect                    ( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile         void            SingleSelect                    ( xproperty_v2::base* pProp );

protected:

    using hash_type_to_class = x_ll_mrmw_hash< types_info::guid, u64 >;

protected:

    gbed_game_graph::document&                  m_GameGraphDoc;
    gbed_layer::document&                       m_LayerDoc;
    gbed_blueprint::document&                   m_BlueprintDoc;
    gbed_constant_data::document&               m_ConstDataDoc;
    gbed_selection::document&                   m_SelectionDoc;

    gb_blueprint::master*                       m_pBlueprint            { nullptr };
    gb_component::entity*                       m_pEntity               { nullptr };
    imgui_node_graph::node::guid                m_gEntity               { nullptr };
    gb_component::entity::guid                  m_gBackupEntity         { nullptr };
        
    component_dlg                               m_ComponentList;
    constant_data_dlg                           m_ConstantDataList;
    gbed_blueprint::dlg                         m_BlueprintList;
    entity_dlg                                  m_EntityRefList;
        
    open_selector                               m_OpenSelector          { open_selector::NONE };
    hash_type_to_class                          m_GuidToClassGuid       { };
    ImVec2                                      m_NewPos                { };
    imgui_node_graph::node::connection_pod*     m_pSelectedPod          { nullptr };
    graph_node*                                 m_pGraphNode            { nullptr };
    ImVec2                                      m_GameNodeStartingPos   {};
    bool                                        m_bOpenErrorPopup       { false };
    bool                                        m_bSentNotification     { false };

    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>          m_delegateSelChangeEntity           { *this, &tab::msgSelChangeEntity }; 
    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>          m_delegateSelChangeBlueprint        { *this, &tab::msgSelChangeBlueprint }; 
//    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>        m_delegateSelChangeConstantData     { *this, &tab::msgSelChangeConstData }; 
    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>          m_delegateSelChangeGeneral          { *this, &tab::msgSelChangeGeneral }; 
    x_message::delegate<tab,xproperty_v2::base&>                                            m_deletegateChangeHappen            { *this, &tab::msgChangeHappen };
    x_message::delegate<tab,xproperty_v2::base&>                                            m_deletegateNotifyDelete            { *this, &tab::msgNotifyDelete };
    x_message::delegate<tab, xvector<xstring>&>                                             m_delegateQueryReloadGameMgr        { *this, &tab::msgQueryReloadGameMgr }; 
};



