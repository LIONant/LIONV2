//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class component_dlg
{
public:

    x_inline                    component_dlg       ( gbed_game_graph::document& GameGraphDoc ) : m_GameGraphDoc{GameGraphDoc} {}
    x_inline        void        NormalMode          ( void )    { m_Mode = view_mode::NORMAL; }
    x_inline        void        ComponentMode       ( void )    { m_Mode = view_mode::COMPONENT_ONLY; }
    x_inline        void        EntityMode          ( void )    { m_Mode = view_mode::ENTITY_ONLY; }
    x_inline        void        Open                ( void )    { ImGui::OpenPopup( "Component List" ); Refresh(); }
    x_inline        auto        getSelectedGuid     ( void ) const { return m_SelectedGuid; }
    x_inline        bool        isEntityMode        ( void ) const { return m_Mode == view_mode::ENTITY_ONLY; }
    x_inline        bool        isComponentMode     ( void ) const { return m_Mode == view_mode::COMPONENT_ONLY; }
    x_incppfile     bool        Render              ( void );

protected:

    struct type_component
    {
        xstring                         m_Name {};
        gb_component::type_base::guid   m_Guid {};
        xstring                         m_Help {};
    };

    struct type_group
    {
        xstring                     m_Name { };
        xvector<type_component>     m_TypeList  { };
        bool                        m_bOpen     { true };
    };

    enum class view_mode
    {
        NORMAL,
        COMPONENT_ONLY,
        ENTITY_ONLY
    };

protected:

    x_incppfile     void        Refresh             ( void );
    x_incppfile     void        RefreshComponent    ( gb_game_graph::base& GameGraph );
    x_incppfile     void        RefreshGroup        ( gb_game_graph::base& GameGraph );

protected:

    gbed_game_graph::document&          m_GameGraphDoc;
    xvector<type_group>                 m_GroupList         {};
    xvector<type_component>             m_ComponentList     {};
    ImGuiTextFilter                     m_Filter            {};
    view_mode                           m_Mode              {view_mode::NORMAL};
    gb_component::type_base::guid       m_SelectedGuid      {};
};
