//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

void component_dlg::Refresh()
{
    auto& GameGraph = m_GameGraphDoc.getGraph();
    RefreshGroup        ( GameGraph );
    RefreshComponent    ( GameGraph );
}

//-------------------------------------------------------------------------------------------

bool component_dlg::Render( void )
{
    // Reset selected value
    m_SelectedGuid.m_Value = 0;

    ImGui::SetNextWindowSizeConstraints(ImVec2(400, 200), ImVec2(400, 200)); 
            
    if( ImGui::BeginPopup("Component List") )
    {
        if( m_Mode == view_mode::ENTITY_ONLY )  ImGui::CollapsingHeader( "Entity Selector", ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_Bullet );
        else                                    ImGui::CollapsingHeader( "Component Selector", ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_Bullet );  
                    
        ImGui::Separator();
        if( ImGui::Button( ICON_FA_TIMES ) )
        {
            m_Filter.Clear();
        }
        ImGui::SameLine(); m_Filter.Draw(ICON_FA_SEARCH, -24.0f);

        ImGui::Separator();
        ImGui::BeginChild("item view");

        xuptr Index = 0;
        if (m_Filter.IsActive())
        {
            for( auto& Type : x_iter_ref( Index, m_ComponentList ) )
            {
                const char* pName = Type.m_Name;

                //
                // Filter base on mode
                //
                if( m_Mode != view_mode::NORMAL ) 
                {
                    if( x_strncmp( pName, "Entity", x_constStrLength("Entity") ) == 0 )
                    {
                        if( m_Mode == view_mode::COMPONENT_ONLY ) continue;
                    }
                    else
                    {
                        if( m_Mode == view_mode::ENTITY_ONLY ) continue;
                    }
                }



                const char* pEnd  = &Type.m_Name[ x_strlen(pName).m_Value ];
                if( m_Filter.PassFilter( pName, pEnd ) )
                {
                    //ImGui::Bullet();

                    ImGui::TextDisabled( ICON_FA_QUESTION_CIRCLE "  " );
                    if (ImGui::IsItemHovered())             
                    {
                        ImGui::BeginTooltip();
                        ImGui::PushTextWrapPos(450.0f);
                        ImGui::TextUnformatted( m_GameGraphDoc.getGraph().m_CompTypeDB.getEntry( Type.m_Guid ).getHelp().m_pValue );
                        ImGui::PopTextWrapPos();
                        ImGui::EndTooltip();
                    }
                    ImGui::SameLine();

                    if( ImGui::Selectable( pName ) )
                    {
                        m_SelectedGuid = Type.m_Guid;
                        ImGui::CloseCurrentPopup();
                    }
                }
            }
        }
        else
        {
            ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));
            xuptr GroupID = 0;
            for( auto& Group : x_iter_ref( GroupID, m_GroupList ) )
            {
                ImGui::AlignFirstTextHeightToWidgets();

                //
                // Filter base on mode
                //
                if( m_Mode != view_mode::NORMAL ) 
                {
                    if( x_strncmp( (const char*)Group.m_Name, "Entity", x_constStrLength("Entity") ) == 0 )
                    {
                        if( m_Mode == view_mode::COMPONENT_ONLY ) continue;
                    }
                    else
                    {
                        if( m_Mode == view_mode::ENTITY_ONLY ) continue;
                    }
                }

                if( ImGui::TreeNodeEx( Group.m_Name, ImGuiTreeNodeFlags_Framed ) )
                {
                    xuptr ID = 0;
                    for( auto& Type : x_iter_ref( ID, Group.m_TypeList ) )
                    {
                        ImGui::PushID(static_cast<int>(ID));
                        //ImGui::Bullet();

                        ImGui::TextDisabled( ICON_FA_QUESTION_CIRCLE "  " );
                        if (ImGui::IsItemHovered())             
                        {
                            ImGui::BeginTooltip();
                            ImGui::PushTextWrapPos(450.0f);
                            ImGui::TextUnformatted( m_GameGraphDoc.getGraph().m_CompTypeDB.getEntry( Type.m_Guid ).getHelp().m_pValue );
                            ImGui::PopTextWrapPos();
                            ImGui::EndTooltip();
                        }
                        ImGui::SameLine();

                        if( ImGui::Selectable( Type.m_Name ) )
                        {
                            m_SelectedGuid = Type.m_Guid;
                            ImGui::CloseCurrentPopup();
                        }
                        ImGui::PopID();
                    }
                    ImGui::TreePop();
                }
            }
            ImGui::PopStyleVar();
        }
        ImGui::EndChild();
        ImGui::EndPopup();
    }

    return !!m_SelectedGuid.m_Value;
}

//-------------------------------------------------------------------------------------------

void component_dlg::RefreshComponent( gb_game_graph::base& GameGraph )
{
    m_ComponentList.DeleteAllEntries();

    x_lk_guard_as_const_get_as_const( GameGraph.m_CompTypeDB.m_Vector, CompTypeDB ); 
    for( const auto& pNode : CompTypeDB )
    {
        auto& Entry  = *pNode->m_pEntry;
        auto& ComDef = m_ComponentList.append();

        ComDef.m_Guid = Entry.getGuid();
        ComDef.m_Name = Entry.getCategoryName().m_Char;
        ComDef.m_Help = Entry.getHelp();
    }
}

//-------------------------------------------------------------------------------------------

void component_dlg::RefreshGroup( gb_game_graph::base& GameGraph )
{
    m_GroupList.DeleteAllEntries();

    m_GroupList.append().m_Name = X_STR("Ungrouped");
    int Index = 0;

    x_lk_guard_as_const_get_as_const( GameGraph.m_CompTypeDB.m_Vector, CompTypeDB ); 
    for( const auto& pNode : CompTypeDB )
    {
        auto& Entry  = *pNode->m_pEntry;
        const auto& TypeName = Entry.getCategoryName();

        //
        // Find the right group
        //
        const bool bAppended = [&]
        {
            for( auto& Group : m_GroupList )
            {
                for( int i = 0; Group.m_Name[i] == TypeName.m_Char.m_pValue[i]; i++ )
                {
                    if( Group.m_Name[i] == TypeName.m_Char.m_pValue[i] && TypeName.m_Char.m_pValue[i+1] == '/' )
                    {
                        auto& Type = Group.m_TypeList.append();
                        Type.m_Name.Copy( (const char*)&TypeName.m_Char.m_pValue[i+2] );
                        Type.m_Guid = Entry.getGuid();
                        Type.m_Help.Copy( Entry.getHelp() );
                        return true;
                    }
                }
            }
            return false;
        }();

        //
        // Did not find the group then we must create the group or insert it into the ungroup group
        //
        if( bAppended == false )
        {
            // Get the index from where the name starts -1 if it did not find a group
            const int Index = [&]
            {
                for( int i = 0; TypeName.m_Char.m_pValue[i]; i++ )
                    if( TypeName.m_Char.m_pValue[i] == '/' ) return i;
                return -1;
            }();

            if( Index == -1 )
            {
                // insert into the ungroup group
                auto& Type = m_GroupList[0].m_TypeList.append();
                Type.m_Name.Copy( &TypeName.m_Char.m_pValue[Index+1] );
                Type.m_Guid = Entry.getGuid();
                Type.m_Help.Copy( Entry.getHelp() );
            }
            else
            {
                auto& Group = m_GroupList.append();
                Group.m_Name.Copy( TypeName.m_Char );
//                        Group.m_Name[Index] = 0;

                auto& Type = Group.m_TypeList.append();
                Type.m_Name.Copy( &TypeName.m_Char.m_pValue[Index+1] );
                Type.m_Guid = Entry.getGuid();
                Type.m_Help.Copy( Entry.getHelp() );
            }
        }
    }
}


