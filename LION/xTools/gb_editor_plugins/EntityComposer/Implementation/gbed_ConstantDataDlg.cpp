//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------
/*
void constant_data_dlg::Refresh( gb_game_graph::base& Base, gb_component::const_data::type::guid gType )
{
    m_gType = gType;
    m_pBase = &Base;

    RefreshList();

    // The type must be register!
    x_assert( Base.m_ConstDataTypeDB.Find( gType ) );
}
*/
//-------------------------------------------------------------------------------------------

void constant_data_dlg::Open( void )
{
    ImGui::OpenPopup( ICON_FA_SHIELD " ConstantData" );
}

//-------------------------------------------------------------------------------------------

bool constant_data_dlg::Render( void )
{
    ImGui::SetNextWindowSizeConstraints(ImVec2(400, 200), ImVec2(400, 200)); 
    bool bSelected = false;
            
    if( ImGui::BeginPopup( ICON_FA_SHIELD " ConstantData" ) )
    {
        ImGui::CollapsingHeader( ICON_FA_SHIELD " ConstantData", ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Framed );

        auto& Graph = m_View.getDocument().getGraphDoc().getGraph();

        ImGui::Separator();
        auto& Type = Graph.m_ConstDataTypeDB.getEntry( m_View.getTypeFilter() );
        ImGui::Text( ICON_FA_QUESTION_CIRCLE_O " " );  
        if (ImGui::IsItemHovered())             
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(450.0f);
            ImGui::TextUnformatted( Type.getHelp().m_pValue );
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
        ImGui::SameLine();
        ImGui::Text( "Type: %s", Type.getEditorName().m_pValue );
        ImGui::Separator();

        m_View.Render( nullptr );
        if( m_View.getSelected() )
        {
            ImGui::CloseCurrentPopup();
        }

        /*
        if( ImGui::Button( ICON_FA_FILE_O " Create New Entry", ImVec2( 200, 0 ) ) )
        {
            const auto gConstData = gb_component::const_data::guid{ gb_component::const_data::guid::RESET };
            m_pBase->m_ConstDataDB.RegisterButRetainOwnership( *m_pBase->m_ConstDataTypeDB.getEntry( m_gType ).New( gConstData ) );
            RefreshList();
        }

        ImGui::Separator();
        if( ImGui::Button( ICON_FA_TIMES ) )
        {
            m_Filter.Clear();
        }
        ImGui::SameLine(); 
        m_Filter.Draw("Filter", -100.0f);

        // ImGui::Separator();
        ImGui::BeginChild("item view");

        const bool bFilter = m_Filter.IsActive();
        for( auto& pConstData : m_List )
        {
            if( bFilter )
            {
                const char* pName = pConstData->getEditorName();
                const char* pEnd  = &pName[ x_strlen(pName).m_Value ];

                if( m_Filter.PassFilter( pName, pEnd ) == false )
                    continue;
            }

            const auto RefCount = pConstData->getReferenceCount();
            if( RefCount == 1 )
            {
                const auto Index = m_List.getIndexByEntry( pConstData );
                if( ImGui::Button( xstring::Make( ICON_FA_TIMES "##%d", Index) ) ) 
                {
                    m_iSelectedEntry = static_cast<int>(Index);
                    ImGui::OpenPopup( "Delete Constant Data" );
                }
            }
            else
            {
                ImGui::Text( "(%d) ", RefCount - 1 );
            }
            ImGui::SameLine();

            if( ImGui::Selectable( pConstData->getEditorName() ) )
            {
                m_iSelectedEntry = static_cast<int>(  m_List.getIndexByEntry( pConstData ) );
                ImGui::CloseCurrentPopup();
                bSelected = true;
            }
        }

        //
        // Deleting constant data
        //
        if( ImGui::BeginPopupModal( "Delete Constant Data" ) )
        {
            if( ImGui::Button( "Yes Delete", ImVec2(120,0) ) )
            {
                m_pBase->m_ConstDataDB.Delete( m_List[m_iSelectedEntry]->getGuid() );
                ImGui::CloseCurrentPopup();
                RefreshList();
                m_iSelectedEntry = -1;
            }

            ImGui::SameLine();
            if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
            {
                ImGui::CloseCurrentPopup();
                m_iSelectedEntry = -1;
            }

            ImGui::EndPopup();
        }
        ImGui::EndChild();
        */
        ImGui::EndPopup();
    }


    // I have selected something
    if( bSelected )
        return true;

    return false;
}

/*
//-------------------------------------------------------------------------------------------

gb_component::const_data& constant_data_dlg::getSelected( void )
{
    return *m_List[ m_iSelectedEntry ];
}

//-------------------------------------------------------------------------------------------

void constant_data_dlg::RefreshList( void )
{
    m_List.DeleteAllEntries();

    //
    // Collect all constant data instances of the require type
    //
    x_lk_guard_as_const_get_as_const( m_pBase->m_ConstDataDB.m_Vector, ConstDataDB );
    for( auto& pNode : ConstDataDB )
    {
        auto& Entry = *pNode->m_pEntry;
        if( m_gType == Entry.getType().getGuid() )
        {
            m_List.append() = &Entry;
        }
    }
}
*/

