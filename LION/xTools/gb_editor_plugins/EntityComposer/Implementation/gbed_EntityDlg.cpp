//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

bool entity_dlg::isCreated( void ) const
{
    return m_bCreated;
}

//-------------------------------------------------------------------------------------------

void entity_dlg::Refresh( gb_game_graph::base& Base )
{
    m_pBase = &Base;
}

//-------------------------------------------------------------------------------------------

void entity_dlg::Open( mode Mode )
{
    ImGui::OpenPopup( "Entity Selector" );
    m_Mode = Mode;
}

//-------------------------------------------------------------------------------------------

bool entity_dlg::Render( void )
{
    ImGui::SetNextWindowSizeConstraints(ImVec2(400, 200), ImVec2(400, 200)); 
            
    if( ImGui::BeginPopup( "Entity Selector" ) )
    {
        bool bSelected = false;

        ImGui::CollapsingHeader( "Entity Selector", ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Framed );

        ImGui::Separator();
        if( ImGui::Button( ICON_FA_TIMES ) )
        {
            m_Filter.Clear();
        }
        ImGui::SameLine(); 
        m_Filter.Draw("Filter", -100.0f);

        // ImGui::Separator();
        ImGui::BeginChild("item view");

        // See if the user wants us to filer entries for him
        const bool bFilter = m_Filter.IsActive();
        if ( bFilter || ImGui::TreeNodeEx("Entities", ImGuiTreeNodeFlags_DefaultOpen ) )
        {
            x_lk_guard_as_const_get_as_const( m_pBase->m_CompTypeDB.m_Vector, ComponentTypeLinearVector );
            xstring     Temp;
            for( const auto pTypeEntry : ComponentTypeLinearVector )
            {
                if( pTypeEntry->m_pEntry->isEntityType() == false ) continue;

                for( const auto pEntry : pTypeEntry->m_pEntry->getInWorld() )
                {
                    auto& Entity = pEntry->SafeCast<gb_component::entity>();
                    Entity.getGuid().getStringHex( Temp );  

                    if( bFilter )
                    {
                        const char* pName = Temp;
                        const char* pEnd  = &pName[ x_strlen(pName).m_Value ];

                        if( m_Filter.PassFilter( pName, pEnd ) == false )
                            continue;
                    }

                    if( Entity.isKindOf<gb_component::entity_static>() )        ImGui::TreeNodeEx( &Entity, ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_UNIVERSITY " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                    else if( Entity.isKindOf<gb_component::entity_dynamic>() )  ImGui::TreeNodeEx( &Entity, ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_LEAF " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                    else                                                        ImGui::TreeNodeEx( &Entity, ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_COGS " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                        
                    if (ImGui::IsItemClicked())
                    {
                        m_pSelected = &Entity;
                        bSelected = true;
                        m_bCreated = false;
                        ImGui::CloseCurrentPopup();
                    }
                }
            }
            if(!bFilter) ImGui::TreePop();
        }

        ImGui::EndChild();
        ImGui::EndPopup();

        if( bSelected ) 
        {
            return true;
        }
    }

    return false;
}

//-------------------------------------------------------------------------------------------

auto& entity_dlg::getSelected( void )
{
    return *m_pSelected; //*m_List[ m_iSelectedEntry ];
}

