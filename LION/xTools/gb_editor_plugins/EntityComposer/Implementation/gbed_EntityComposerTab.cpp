//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// HIDDEN TYPES
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------

struct tab::graph_node 
    : imgui_node_graph::node
{
    x_object_type( graph_node, rtti( imgui_node_graph::node ) ); 

    using node::node;

    void setColor( xcolor C ) { m_Color = C; }
            
    graph_node( tab& Win, guid Guid, xstring Name, ImVec2 Pos, bool bCloseWindow ) : 
        node( Guid, Name, Pos, bCloseWindow )
        , m_Graph{ Win } {}

    tab&                m_Graph;
    xproperty_v2::base* m_pProperty    { nullptr };
    xproperty_v2::base* m_pEditorProps { nullptr };
    u64                 m_InstanceGuid { 0 };
    bool                m_d;
};

//-------------------------------------------------------------------------------------------

struct tab::editor_node 
    : tab::graph_node
    , xproperty_v2::base
{
    x_object_type( editor_node, rtti( graph_node, xproperty_v2::base ) );

    using graph_node::graph_node;

    virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
    { 
        static prop_table E( this, [&](xproperty_v2::table& E)
        {
            //E.AddChildTable( t_parent::onPropertyTable() );
            E.AddScope( X_STR("Composer"), [&]( xproperty_v2::table& E )
            {
                /*
                E.AddProperty<xproperty_v2::button>
                ( 
                    X_STR("Clear")
                    , X_STATIC_LAMBDA_BEGIN (editor_node& Class, xproperty_v2::button& Data, const xproperty_v2::info::button& Details, bool isSend)
                    {
                        if (isSend) Data.m_Value = X_STR("Click to Clear") ;
                        else
                        {
                            Class.m_Graph.Clear();
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("Resets the entity editor, any unsaved data may be lost.") 
                );
                */
                        
                E.AddProperty<xproperty_v2::button>
                (
                    X_STR("Entity")
                    , X_STATIC_LAMBDA_BEGIN (editor_node& Class ) -> bool
                    {
                        if (Class.m_Graph.getEntity() == nullptr) return false;
                        return Class.m_Graph.isEditingBlueprint() == false;
                    }
                    X_STATIC_LAMBDA_END
                    , X_STATIC_LAMBDA_BEGIN (editor_node& Class, xproperty_v2::button& Data, const xproperty_v2::info::button& Details, bool isSend)
                    {
                        auto pEntity = Class.m_Graph.getEntity();
                        if ( pEntity == nullptr )
                        {
                            if (isSend) Data.m_Value = X_STR("Empty");
                            return;
                        }

                        if (isSend)
                        {
                            if( pEntity->isInWorld() )
                            {
                                Data.m_Value = X_STR("Edit");
                            }
                            else
                            {
                                Data.m_Value = X_STR("Save");
                            }
                        }
                        else
                        {
                            if( pEntity->isInWorld() )
                            {
                                auto pEDEntity = Class.m_Graph.getLayerDoc().findEDEntry( pEntity->getGuid() );
                                pEntity->msgRemoveFromWorld();

                                // User is asking us to edit this entity
                                Class.m_Graph.setReadOnly(false);

                                xstring StrGuid;
                                pEntity->getGuid().getStringHex( StrGuid );

                                if( pEDEntity )
                                {
                                    Class.m_Graph.getMainDoc().NotifyChangeHappen( xstring::Make("[COMPOSER] Modifying Editor Entity (%s)", StrGuid ), *pEDEntity );
                                }
                                else
                                {
                                    Class.m_Graph.getMainDoc().NotifyChangeHappen( xstring::Make("[COMPOSER] Modifying System Entity (%s)", StrGuid ), *pEntity->getLinearPropInterface() );
                                }
                            }
                            else
                            {
                                // User is asking us to verify and save the entity into the world
                                xvector<xstring> IssueList;
                                if( auto Err = pEntity->linearCheckResolve( IssueList ); Err.isError() )
                                {
                                    // Open error dialog
                                    Class.m_Graph.m_bOpenErrorPopup = true;
                                    Err.IgnoreError();
                                }
                                else
                                {
                                    Class.m_Graph.setReadOnly(true);
                                    pEntity->msgAddToWorld();
                                }
                            }
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("Edits or Saves the selected entity")
                );
       
                E.AddProperty<xproperty_v2::button>
                (
                    X_STR("Blueprint")
                    , X_STATIC_LAMBDA_BEGIN (editor_node& Class ) -> bool
                    {
                        if (Class.m_Graph.getEntity() == nullptr) return false;
                        return Class.m_Graph.isEditingBlueprint();
                    }
                    X_STATIC_LAMBDA_END
                    , X_STATIC_LAMBDA_BEGIN (editor_node& Class, xproperty_v2::button& Data, const xproperty_v2::info::button& Details, bool isSend)
                    {
                        auto pEntity = Class.m_Graph.getEntity();
                        if ( pEntity == nullptr )
                        {
                            if (isSend) Data.m_Value = X_STR("Empty");
                            return;
                        }

                        if (isSend)
                        {
                            if( Class.m_Graph.isReadOnly() )
                            {
                                Data.m_Value = X_STR("Edit");
                            }
                            else
                            {
                                Data.m_Value = X_STR("Save");
                            }
                        }
                        else
                        {
                            if( Class.m_Graph.isReadOnly() )
                            {
                                auto pBlueprint = Class.m_Graph.getActiveBlueprint();
                                x_assert(pBlueprint);

                                // User is asking us to edit this entity
                                Class.m_Graph.setReadOnly(false);

                                xstring StrGuid;
                                pBlueprint->getGuid().getStringHex( StrGuid );
                                auto pEDBLueprint = Class.m_Graph.getBlueprintDoc().findEDBlueprint( pBlueprint->getGuid() );

                                if( pEDBLueprint )
                                {
                                    pEDBLueprint->m_bChanged = true;
                                    Class.m_Graph.getMainDoc().NotifyChangeHappen( xstring::Make("[COMPOSER] Modifying Editor Entity (%s)", StrGuid ), *pEDBLueprint );
                                }
                                else
                                {
                                    Class.m_Graph.getMainDoc().NotifyChangeHappen( xstring::Make("[COMPOSER] Modifying System Entity (%s)", StrGuid ), *pBlueprint );
                                }
                            }
                            else
                            {
                                // User is asking us to verify and save the entity into the world
                                xvector<xstring> IssueList;
                                if( auto Err = pEntity->linearCheckResolve( IssueList ); Err.isError() )
                                {
                                    // Open error dialog
                                    Class.m_Graph.m_bOpenErrorPopup = true;
                                    Err.IgnoreError();
                                }
                                else
                                {
                                    auto pBlueprint = Class.m_Graph.getActiveBlueprint();
                                    pBlueprint->UpdateAsNew( *Class.m_Graph.getEntity() );
                                    // todo: all entities/or editor entities that uses this blueprint must be recreated

                                    Class.m_Graph.setReadOnly(true);
                                }
                            }
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("Edits or Saves the selected blueprint")
                );
            });
        });

        return E; 
    }
};

//-------------------------------------------------------------------------------------------

struct tab::blue_print_node 
    : tab::graph_node
    , xproperty_v2::base
{
    x_object_type( blue_print_node, rtti( graph_node, xproperty_v2::base ) );

    using graph_node::graph_node;

    virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
    { 
        static prop_table E( this, [&](xproperty_v2::table& E)
        {
            //E.AddChildTable( t_parent::onPropertyTable() );
            E.AddScope( X_STR(".Editor"), [&]( xproperty_v2::table& E )
            {
                E.AddProperty<xproperty_v2::button>
                ( 
                    X_STR("Save as new")
                    , X_STATIC_LAMBDA_BEGIN (blue_print_node& Class, xproperty_v2::button& Data, const xproperty_v2::info::button& Details, bool isSend)
                    {
                        if (isSend) Data.m_Value = X_STR("Click to Save") ;
                        else
                        {
                            auto& Master = Class.m_pProperty->SafeCast<gb_blueprint::master>();
                            Master.UpdateAsNew( *Class.m_Graph.m_pEntity ); 
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("Reset blue print to the current graph and properties. Note that all instances that depend on this blue print will get affected.") 
                );
            });
        });

        return E; 
    }
};

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// FUNCTIONS
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------

static const gbed_plugins::tab::type_harness<tab> s_TypeTab{ X_STR( "" ICON_FA_CUBES " Composer" ) };    
    
//-------------------------------------------------------------------------------------------

const gbed_plugins::tab::type& tab::getType( void ) 
{ 
    return s_TypeTab;  
}

//-------------------------------------------------------------------------------------------

const xproperty_v2::table& tab::onPropertyTable( void ) const
{
    static const xproperty_v2::table E( this, [&](xproperty_v2::table& E)
    {
    });
    return E;
}

//-------------------------------------------------------------------------------------------

tab::tab( const xstring::const_str& Str, gbed_frame::base& EditorFrame ) 
    : gbed_plugins::tab( Str, EditorFrame )
    , m_GameGraphDoc        { EditorFrame.getMainDoc().getSubDocument<gbed_game_graph::document>() }
    , m_LayerDoc            { EditorFrame.getMainDoc().getSubDocument<gbed_layer::document>() }
    , m_BlueprintList       { EditorFrame.getMainDoc().getSubDocument<gbed_blueprint::document>() }
    , m_BlueprintDoc        { EditorFrame.getMainDoc().getSubDocument<gbed_blueprint::document>() }
    , m_ConstantDataList    { EditorFrame.getMainDoc().getSubDocument<gbed_constant_data::document>() }
    , m_ConstDataDoc        { EditorFrame.getMainDoc().getSubDocument<gbed_constant_data::document>() }
    , m_SelectionDoc        { EditorFrame.getMainDoc().getSubDocument<gbed_selection::document>() }
    , m_ComponentList       { EditorFrame.getMainDoc().getSubDocument<gbed_game_graph::document>() }
{
    m_WindowName = X_STR( "" ICON_FA_CUBES " Composer");
    m_GuidToClassGuid.Initialize( 1000 );
    AddEditorNode();
    setReadOnly(true);

    m_delegateSelChangeEntity.Connect( m_SelectionDoc.m_Events.m_SelChangeEntity );
    m_delegateSelChangeBlueprint.Connect( m_SelectionDoc.m_Events.m_SelChangeBlueprint );
    m_delegateSelChangeGeneral.Connect( m_SelectionDoc.m_Events.m_SelChangeGeneral );
    m_deletegateChangeHappen.Connect( getMainDoc().m_Events.m_ChangeHappen );
    m_deletegateNotifyDelete.Connect( getMainDoc().m_Events.m_NotifyDelete );
    m_delegateQueryReloadGameMgr.Connect( m_GameGraphDoc.m_Events.m_queryReloadGameMgr );
}

//-------------------------------------------------------------------------------------------

void tab::RenderInspector( void )
{
//    m_Inspector.Update();
//    m_Inspector.Render();
}

//-------------------------------------------------------------------------------------------

void tab::Clear( void )
{
    // Make sure that everyone looking at this data doesn't
    if( m_bSentNotification == false )
    {
        SingleSelect( gb_component::entity::guid{ nullptr }, nullptr );
        if( isEditingBlueprint() )
        {
            SingleSelect( gb_blueprint::guid{ nullptr }, nullptr );
        }
    }

    // We were editing a blue print before so this means that the entity should not be deleted
    // or we created a blue print from scratch or entity from scratch
    if( m_pBlueprint || (m_pEntity && m_pEntity->isInWorld() == false ) )
    {
        if(m_pBlueprint)
        {
            m_pEntity->linearDestroy();
        }
        else
        {
            if( m_gBackupEntity == m_pEntity->getGuid() )
            {
                m_LayerDoc.DestroyEntity( m_pEntity->getGuid() );
            }
        }
    }

    m_pEntity    = nullptr;
    m_pBlueprint = nullptr;
    m_GuidToClassGuid.DeleteAllEntries();
    t_parent::Clear();
    AddEditorNode();
    setReadOnly(true);
}

//-------------------------------------------------------------------------------------------

void tab::SetBlueprint( gb_blueprint::master* pBluePrint )
{
    xmatrix4 L2W;
    L2W.setIdentity();
    L2W.setTranslation( xvector3( x_frand(-10,10) ) );
    auto& Entity = pBluePrint->CreateEntity<gb_component::entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), L2W, m_GameGraphDoc.getGraph() );

    Clear();
    m_pBlueprint = pBluePrint;
    SetEntity(&Entity,true);
}

//-------------------------------------------------------------------------------------------

void tab::SetEntity( gb_component::entity* pEntity, bool bSkipClear )
{
    if( isReadOnly() == false && m_pEntity != pEntity && m_pEntity && m_pEntity->isInWorld() == false )
    {
        xvector<xstring> Issues; 
        auto Err = m_pEntity->linearCheckResolve( Issues );
        if( Err.isError() )
        {
            // If there are errors in the entity definition we will just allow it to be deleted
            // todo: ideally we will restore the previously saved entity
            Err.IgnoreError();
        }
        else
        {
            m_pEntity->msgAddToWorld();    
        }
    }

    if(bSkipClear==false) Clear();
    m_pEntity = pEntity;
    m_gBackupEntity.m_Value =0;
    if( !m_pEntity ) return;
    m_gBackupEntity = m_pEntity->getGuid();

    setReadOnly(false);//m_pEntity->isInWorld());

    // Game node
    const imgui_node_graph::node::guid                  gEntityNodeGUID         { imgui_node_graph::node::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gEntityEntityPod        { imgui_node_graph::node::connection_pod::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gEntityComponentPod     { imgui_node_graph::node::connection_pod::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod     { imgui_node_graph::node::connection_pod::guid::RESET };
    ImVec2                                              Pos                     { m_GameNodeStartingPos };

    for( gb_component::base* pComponent = m_pEntity; 
                                pComponent; 
                                pComponent = pComponent->getNextComponent() )
    { 

        if( pComponent == m_pEntity )
        {
            Pos = AddEntity
            (
                gEntityNodeGUID
                , m_pGraphNode->getGuid()
                , m_pGraphNode->getPod(0).getGuid()
                , gEntityComponentPod
                , gEntityEntityPod
                , gEntityBlueprintPod
                , pComponent
                , Pos 
            );
        }
        else
        {
            Pos = AddComponent
            (
                gEntityNodeGUID,
                gEntityComponentPod,
                pComponent,
                Pos 
            );
        }
    }

    //
    // Finds an event pod given just the guid for the event
    //
    auto FindEventPod = [&]( u64 Guid ) -> imgui_node_graph::node::connection_pod*
    {
        for( auto& NodePair : m_NodeDB )
        {
            auto& Node = *NodePair.second;

            // We only care about Entity nodes or component nodes
            if( x_strcmp<xchar>( "Entity", Node.getName() ) && x_strcmp<xchar>( "Component", Node.getName() ) ) continue;

            for( auto& Pod : Node.getPods() )
            {
                if( Pod.m_bIsInput == false ) continue;
                if( Pod.m_UserData == Guid )  return &Pod;
            }
        }

        return nullptr;
    };

    //
    // Link up all the delegates
    //
    for( auto& NodePair : m_NodeDB )
    {
        auto& Node = *NodePair.second;

        // We only care about Entity nodes or component nodes
        if( x_strcmp<xchar>( "Entity", Node.getName() ) && x_strcmp<xchar>( "Component", Node.getName() ) ) continue;

        for( auto& Pod : Node.getPods() )
        {
            if( Pod.m_bIsInput == true ) continue;
            if( Pod.m_UserData == 0 )    continue;
            if(     gb_component::event_type::guid(Pod.m_gType.m_Value).getClassGuid()      != gb_component::event_type::t_class_guid
                &&  gb_component::interface_type::guid(Pod.m_gType.m_Value).getClassGuid()  != gb_component::interface_type::t_class_guid ) continue;

            auto* pConPod = FindEventPod( Pod.m_UserData );

            xndptr_s<imgui_node_graph::link>  Link;
            Link.New();
            Link->m_ConnectionHooks[0].m_gNode = pConPod->m_Node.getGuid();
            Link->m_ConnectionHooks[0].m_gPod  = pConPod->getGuid();
            Link->m_ConnectionHooks[1].m_gNode = Pod.m_Node.getGuid();
            Link->m_ConnectionHooks[1].m_gPod  = Pod.getGuid();
            Link->m_Guid.Reset();
            AddLink( std::move(Link) );
        }
    }

    //
    // Do Select
    //
    m_NodeDB[gEntityNodeGUID]->setSelected(true);
    //m_GameGraphDoc.m_Events.m_ActiveEntity.Notify( pEntity->getGuid(), pEntity->getLinearPropInterface() );

    //m_Inspector.SetProperty( pEntity->getLinearPropInterface() );

    //
    // Make it read only for now
    //
    setReadOnly(true);
}


//-------------------------------------------------------------------------------------------------

void tab::SingleSelect( xproperty_v2::base* pProp )
{
    if( isEditingBlueprint() )
    {
        SingleSelect( m_pBlueprint->getGuid(), pProp );    
    }
    else
    {
        if(m_pEntity) SingleSelect( m_pEntity->getGuid(), pProp );
        else m_SelectionDoc.Single( pProp );
    }
}

//-------------------------------------------------------------------------------------------------

void tab::SingleSelect( gb_component::entity::guid gGuid,     xproperty_v2::base* pProps )
{
    m_bSentNotification = true;
    m_SelectionDoc.Single( gGuid, pProps );
    m_bSentNotification = false;
}

//-------------------------------------------------------------------------------------------------

void tab::SingleSelect( gb_blueprint::guid gGuid,             xproperty_v2::base* pProps )
{
    m_bSentNotification = true;
    m_SelectionDoc.Single( gGuid, pProps );
    m_bSentNotification = false;
}

//-------------------------------------------------------------------------------------------------

void tab::SingleSelect( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps )
{
    m_bSentNotification = true;
    m_SelectionDoc.Single( gGuid, pProps );
    m_bSentNotification = false;
}


//-------------------------------------------------------------------------------------------------

void tab::msgSelChangeGeneral ( const gbed_selection::document& SelectionDoc, xproperty_v2::base* pProp )
{
    // Filter my own messages
    if( m_bSentNotification || isReadOnly() == false ) return;

    m_bSentNotification = true;
    if( pProp )
    {
        if( pProp->isKindOf<gb_component::entity>() )
        {
            auto& Entity        = pProp->SafeCast<gb_component::entity>();
            auto& EntityList    = SelectionDoc.getSelectedEntities();

            if( EntityList.getCount<int>() == 1 && m_pEntity != &Entity )
            {
                SetEntity( &Entity );
            }
        }
    }
    else
    {
        // SetEntity( nullptr );
    }
    m_bSentNotification = false;
}

//-------------------------------------------------------------------------------------------------

void tab::msgSelChangeEntity( const gbed_selection::document& SelectionDoc, xproperty_v2::base* pProp )
{
    // Filter my own messages
    if( m_bSentNotification || isReadOnly() == false ) return;

    m_bSentNotification = true;
    auto& lEntities = SelectionDoc.getSelectedEntities();

    if( lEntities.getCount<int>() == 1 )
    {
        auto* pEntity = m_GameGraphDoc.getGraph().findEntity( lEntities[0] );
        if( pEntity )
        {
            if( m_pEntity != pEntity )
            {
                SetEntity( pEntity );
            }
        }
        else
        {
            x_assert( false );
        }
    }
    else
    {
        SetEntity( nullptr );
    }
    m_bSentNotification = false;
}

//-------------------------------------------------------------------------------------------------

void tab::msgSelChangeBlueprint( const gbed_selection::document& SelectionDoc, xproperty_v2::base* pProp )
{
    // Filter my own messages
    if( m_bSentNotification || isReadOnly() == false ) return;

    m_bSentNotification = true;
    auto& lEntities = SelectionDoc.getSelectedBlueprints();

    if( lEntities.getCount<int>() == 1 )
    {
        auto* pBlueprint = m_GameGraphDoc.getGraph().m_BlueprintDB.Find( lEntities[0] );
        if( pBlueprint )
        {
            if( m_pBlueprint != pBlueprint )
            {
                SetBlueprint( pBlueprint );
            }
        }
        else
        {
            x_assert( false );
        }
    }
    else
    {
        SetBlueprint( nullptr );
    }
    m_bSentNotification = false;
}

//---------------------------------------------------------------------------------------------------------

void tab::msgChangeHappen( xproperty_v2::base& Prop )
{
    if( !(m_pBlueprint || m_pEntity) ) return;

    if( Prop.isKindOf<gb_component::base>() )
    {
        auto& Comp = Prop.SafeCast<gb_component::base>();

        if( Comp.isEntity() == false )
        {
            if( isEditingBlueprint() )
            {
                // todo: blue print needs update
            }
            else
            {
                auto pEdEntity = m_LayerDoc.findEDEntry( m_pEntity->getGuid() );
                if( pEdEntity )
                {
                    pEdEntity->m_bChanged = true;
                }
            }
        }
    }
}

//---------------------------------------------------------------------------------------------------------

void tab::msgNotifyDelete( xproperty_v2::base& Prop )
{
    if( !(m_pBlueprint || m_pEntity) ) return;

    if( Prop.isKindOf<gb_component::base>() )
    {
        auto& Comp = Prop.SafeCast<gb_component::base>();

        if( Comp.isEntity() )
        {
            auto& Entity = Comp.SafeCast<gb_component::entity>();
            if( Entity.getGuid() == m_pEntity->getGuid() )
            {
                // Some how the user just deleted the entity we are editing...
                // only thing we can do is to clean all
                SetEntity( nullptr );
            }
        }
    }
}

//-------------------------------------------------------------------------------------------------

void tab::msgQueryReloadGameMgr( xvector<xstring>& Issues )
{
    if( isReadOnly() == false )
    {
        Issues.append( xstring::Make( "You need to save the content of the composer before you can reload the game mgr" ) );
    }
    else
    {
        Clear();
    }
}


//-------------------------------------------------------------------------------------------------

void tab::onAddNode( imgui_node_graph::node& Node )
{
    if( m_bSentNotification ) return;

    // Call the default selection
    imgui_node_graph::base::onAddNode(Node);

    // Also let the Inspector know what is going on
    if( Node.isKindOf<editor_node>() )
    {
        auto& EditorNode = Node.SafeCast<editor_node>();
        SingleSelect( EditorNode.m_pEditorProps?EditorNode.m_pEditorProps:EditorNode.m_pProperty );
    }
    else if( Node.isKindOf<graph_node>() )
    {
        auto& GraphNode = Node.SafeCast<graph_node>();

        if( GraphNode.m_pProperty->isKindOf<gb_component::const_data>() )
        {
            auto& ConstData = GraphNode.m_pProperty->SafeCast<gb_component::const_data>();
            SingleSelect( ConstData.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
        }
        else if( GraphNode.m_pProperty->isKindOf<gb_blueprint::master>() )
        {
            auto& BluePrint = GraphNode.m_pProperty->SafeCast<gb_blueprint::master>();
            SingleSelect( BluePrint.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
        }
        else if( GraphNode.m_pProperty->isKindOf<gb_component::base>() )
        {
            SingleSelect( m_pEntity->getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
        }
        else 
        {
            SingleSelect( GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
        }
    }
    else
    {
        x_assert( false );
    }
}

//-------------------------------------------------------------------------------------------------

void tab::AddEditorNode( void )
{
    const imgui_node_graph::node::guid                  gGameNodeGUID        { imgui_node_graph::node::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gGameNodePodGUID     { imgui_node_graph::node::connection_pod::guid::RESET };
    ImVec2                                              Pos                  { 10, 10 };

    m_GameNodeStartingPos = AddEditorNode
    (
        gGameNodeGUID,
        gGameNodePodGUID,
        Pos
    );

    // Keep a shortcut to the game node
    m_pGraphNode = &m_NodeDB[ gGameNodeGUID ]->SafeCast<graph_node>();

    //
    // Add Basic Node classes
    //
    m_GuidToClassGuid.cpAddEntry( types_info::s_GameGuid.m_Value, []( types_info::guid& Entry ) 
    { 
        Entry = types_info::s_GameGuid;     
    });

    m_GuidToClassGuid.cpFindOrAddEntry( types_info::s_EntityClassGuid.m_Value, []( types_info::guid& Entry )
    {
        Entry = types_info::s_EntityClassGuid;
    });

    m_GuidToClassGuid.cpFindOrAddEntry( types_info::s_ComponentClassGuid.m_Value, []( types_info::guid& Entry )
    {
        Entry = types_info::s_ComponentClassGuid;
    });
}

//-------------------------------------------------------------------------------------------------
ImVec2 tab::AddEditorNode(
    const imgui_node_graph::node::guid                  gGameNodeGUID,   
    const imgui_node_graph::node::connection_pod::guid  gGameNodePodGUID,
    ImVec2                                              Pos
)
{
    xndptr_s<editor_node> NodeTest;  
    NodeTest.New( *this, gGameNodeGUID, X_STR("Composer"), Pos, false );
    NodeTest->m_pProperty = NodeTest;
    NodeTest->setColor( types_info::s_GameColor );

    NodeTest->AddOuputPod(
        gGameNodePodGUID, 
        X_STR("Entity"), 
        types_info::s_EntityClassGuid,
        types_info::s_EntityColor,
        false,
        X_STR(" Connect Add or Replace ") );

    Pos.x += 2*NodeTest->getSize().x;

    // Set the editor property of the entity
  //  NodeTest->m_pEditorProps = static_cast<xproperty_v2::base*>(m_BlueprintDoc.findEDEntry( pComponent->SafeCast<gb_component::entity>().getGuid() )); 

    NodeTest->EndSetup();

    AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );

    return Pos;
}

//-------------------------------------------------------------------------------------------------

void tab::AddEvents( graph_node& Node, gb_component::base& Base )
{
    //
    // Add events
    //
    {
        for( auto& E : Base.getEventTable() )
        {
            auto& Pod = Node.AddInputPod
            (
                imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }
                , E.m_Name
                , imgui_node_graph::node::connection_pod::type::guid{ E.m_EventType.m_Guid.m_Value }
                , ( E.m_EventType.m_Guid.getClassGuid() == gb_component::event_type::t_class_guid) ? types_info::s_EventDataColor : types_info::s_InterfaceDataColor 
                , false
                , X_STR(" Connect ")
            );

            Pod.m_UserData = Base.getEventGuid( E ).m_Value;

            int a=0;
        }
    }

    //
    // Add Delegates
    //
    for( auto& E : Base.getDelegateTable() )
    {
        auto& Pod = Node.AddOuputPod
        (
            imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }
            , E.m_Name
            , imgui_node_graph::node::connection_pod::type::guid{ E.m_EventType.m_Guid.m_Value }
            , ( E.m_EventType.m_Guid.getClassGuid() == gb_component::event_type::t_class_guid) ? types_info::s_EventDataColor : types_info::s_InterfaceDataColor 
            , true
            , X_STR(" Connect ") 
        );

        // get the value of the delegate and store it in the pod 
        if( E.m_EventType.m_Guid.getClassGuid() == gb_component::event_type::t_class_guid )
        {
            auto pFakeDelegate = ((gb_component::event_fake::delegate<gb_component::base>*)&((xbyte*)&Base.getDelegates())[E.m_Offset]);
            Pod.m_UserData = pFakeDelegate->m_Guid.m_Value;
        }
        else
        {
            x_assert( E.m_EventType.m_Guid.getClassGuid() == gb_component::interface_type::t_class_guid );
            auto pFakeDelegate = ((gb_component::interface_ref<void>*)&((xbyte*)&Base)[E.m_Offset]);
            Pod.m_UserData = pFakeDelegate->m_Guid.m_Value;
        }
    }
}

//-------------------------------------------------------------------------------------------------

ImVec2 tab::AddEntity(
    const imgui_node_graph::node::guid                  gEntityNodeGUID,
    const imgui_node_graph::node::guid                  gGameNodeGUID,
    const imgui_node_graph::node::connection_pod::guid  gGameNodePod,
    const imgui_node_graph::node::connection_pod::guid  gComponentPod,
    const imgui_node_graph::node::connection_pod::guid  gEntityPod,
    const imgui_node_graph::node::connection_pod::guid  gBlueprintPod,
    gb_component::base*                                 pComponent,
    ImVec2                                              Pos )
{
    xndptr_s<graph_node> NodeTest;

    m_gEntity = gEntityNodeGUID;

    NodeTest.New( *this, gEntityNodeGUID, X_STR("Entity"), Pos, false );

    NodeTest->AddInputPod(
        gEntityPod 
        , pComponent->getGlobalInterface().m_Type.getCategoryName().m_Char
        , types_info::s_EntityClassGuid
        , types_info::s_EntityColor
        , false
        , X_STR(" Connect or Replace ") );

    NodeTest->AddInputPod(
        gBlueprintPod 
        , X_STR("Blueprint.Create")
        , types_info::s_BlueprintCreateClassGuid
        , types_info::s_BlueprintCreateColor
        , false
        , X_STR(" Connect or Add ") );

    NodeTest->AddOuputPod(
        gComponentPod
        , gb_component::base::t_class_string
        , types_info::s_ComponentClassGuid
        , types_info::s_ComponentColor
        , false
        , X_STR(" Connect or Add ") );

    AddEvents( NodeTest, *pComponent );

    // Set the editor property of the entity
    NodeTest->m_pEditorProps = static_cast<xproperty_v2::base*>(m_LayerDoc.findEDEntry( pComponent->SafeCast<gb_component::entity>().getGuid() )); 
    NodeTest->m_pProperty    = pComponent->getLinearPropInterface();// reinterpret_cast<trick*>(pComponent)->getPropertyInterface();

    NodeTest->setColor( types_info::s_EntityColor );
    NodeTest->EndSetup();
    

    xndptr_s<imgui_node_graph::link>  Link;
    Link.New();
    Link->m_ConnectionHooks[0].m_gNode = gEntityNodeGUID;
    Link->m_ConnectionHooks[0].m_gPod  = gEntityPod;
    Link->m_ConnectionHooks[1].m_gNode = gGameNodeGUID;
    Link->m_ConnectionHooks[1].m_gPod  = gGameNodePod;
    Link->m_Guid.Reset();
    AddLink( std::move(Link) );


    // Pos.y += 20 +  NodeTest->getSize().y;

    //
    // If it has a blue print node then add it
    //
    auto gBlueprint = m_pEntity->SafeCast<gb_component::entity>().getBlueprintGuid();
    if( gBlueprint.m_Value )
    {
        //gBlueprint 
        auto& Master = m_GameGraphDoc.getGraph().m_BlueprintDB.getEntry( gBlueprint );

        AddBlueprintEditNode
        ( 
            gEntityNodeGUID
            , gBlueprintPod
            , &Master
            , ImVec2{ Pos.x - 240, 20 +  NodeTest->getSize().y }                             
        );
    }

    //
    // Add the rest
    //
    Pos.x += 2 * NodeTest->getSize().x;
    Pos = AddNodeProperties( NodeTest, gEntityNodeGUID, Pos );

    AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );

    return Pos;
}

//-------------------------------------------------------------------------------------------------

ImVec2 tab::AddBlueprintEditNode(
    const imgui_node_graph::node::guid                  gEntityNodeGUID,
    const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod,
    gb_blueprint::master*                               pBlueprint,
    ImVec2                                              Pos )
{
    xndptr_s<blue_print_node> NodeTest;
    const imgui_node_graph::node::guid                  gGuid       { imgui_node_graph::node::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gPodGuid    { imgui_node_graph::node::connection_pod::guid::RESET };
                    
    x_constexprvar auto gConnectionType = imgui_node_graph::node::connection_pod::type::guid{ gb_blueprint::master::t_class_guid.m_Value };

    NodeTest.New( *this, gGuid, X_STR("Blueprint.Create"), Pos, m_pBlueprint == nullptr );
    NodeTest->AddOuputPod
    (
        gPodGuid 
        , X_STR("Entity")
        , types_info::s_BlueprintCreateClassGuid
        , types_info::s_BlueprintCreateColor
        , false
        , X_STR(" Connect or Replace Self ") 
    );

    NodeTest->setColor( types_info::s_BlueprintCreateColor );
            
    xndptr_s<imgui_node_graph::link>  Link;
    Link.New();
    Link->m_ConnectionHooks[1].m_gNode = gGuid;
    Link->m_ConnectionHooks[1].m_gPod  = gPodGuid;
    Link->m_ConnectionHooks[0].m_gNode = gEntityNodeGUID;
    Link->m_ConnectionHooks[0].m_gPod  = gEntityBlueprintPod;
    Link->m_Guid.Reset();
    AddLink( std::move(Link) );
            
    //const auto posx = Pos.x + 2 * NodeTest->getSize().x;
    const auto posy = Pos.y;
    Pos.y += 20 +  NodeTest->getSize().y;

    NodeTest->m_pProperty           = pBlueprint;
    NodeTest->m_pEditorProps        = m_BlueprintDoc.findEDBlueprint( pBlueprint->getGuid() );

    NodeTest->EndSetup();
    AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );
    return Pos;
}

//-------------------------------------------------------------------------------------------------

ImVec2 tab::AddComponent(
    const imgui_node_graph::node::guid                  gEntityNodeGUID,
    const imgui_node_graph::node::connection_pod::guid  gEntityComponentPod,
    gb_component::base*                                 pComponent,
    ImVec2                                              Pos )
{
    xndptr_s<graph_node> NodeTest;
    const imgui_node_graph::node::guid                  gGuid       { imgui_node_graph::node::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gPodGuid    { imgui_node_graph::node::connection_pod::guid::RESET };
                    
    x_constexprvar auto gConnectionType = imgui_node_graph::node::connection_pod::type::guid{ gb_component::base::t_class_guid.m_Value };

    NodeTest.New( *this, gGuid, X_STR("Component"), Pos, true );
    NodeTest->AddInputPod
    (
        gPodGuid 
        , pComponent->getGlobalInterface().m_Type.getCategoryName().m_Char 
        , types_info::s_ComponentClassGuid
        , types_info::s_ComponentColor
        , false
        , X_STR(" Connect or Replace Self ") 
    );

    AddEvents( NodeTest, *pComponent );

    NodeTest->setColor( types_info::s_ComponentColor );
    NodeTest->m_pProperty = pComponent->getLinearPropInterface();
    x_assert( NodeTest->m_pEditorProps == nullptr );

    NodeTest->EndSetup();

    xndptr_s<imgui_node_graph::link>  Link;
    Link.New();
    Link->m_ConnectionHooks[0].m_gNode = gGuid;
    Link->m_ConnectionHooks[0].m_gPod  = gPodGuid;
    Link->m_ConnectionHooks[1].m_gNode = gEntityNodeGUID;
    Link->m_ConnectionHooks[1].m_gPod  = gEntityComponentPod;
    Link->m_Guid.Reset();
    AddLink( std::move(Link) );

    const auto posx = Pos.x + 2 * NodeTest->getSize().x;
    const auto posy = Pos.y;
    Pos.y += 20 +  NodeTest->getSize().y;

    AddNodeProperties( NodeTest, gGuid, ImVec2{ posx, posy } );
    AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );

    return Pos;
}

//-------------------------------------------------------------------------------------------------

ImVec2 tab::AddBlueprintNode
(
    const imgui_node_graph::node::guid                      gComponentNodeGuid 
    , const imgui_node_graph::node::connection_pod::guid    gComponentBlueprintPod
    , const gb_blueprint::master::guid                      gBlueprintInstance
    , ImVec2                                                Pos
)
{
    const imgui_node_graph::node::guid  gBlueprintNodeGuid( imgui_node_graph::node::guid::RESET );
    xndptr_s<graph_node>                NewNode;
            
    NewNode.New( *this, gBlueprintNodeGuid, gb_blueprint::master::t_class_string, Pos, true );
                
    auto& Blueprint   = m_pEntity->getGlobalInterface().m_GameMgr.m_BlueprintDB.getEntry( gBlueprintInstance );
    auto pEDBlueprint = m_BlueprintDoc.findEDBlueprint(gBlueprintInstance);
    
    NewNode->m_pProperty    = &Blueprint;
    NewNode->m_pEditorProps = pEDBlueprint;
    NewNode->m_InstanceGuid = gBlueprintInstance.m_Value;
    NewNode->setColor( types_info::s_BlueprintColor );

//            Pos.x += 2 * NewNode->getSize().x;

    const imgui_node_graph::node::connection_pod::guid gNodePod{ imgui_node_graph::node::connection_pod::guid::RESET };
    auto& Pod = NewNode->AddInputPod
    (
        gNodePod 
        , pEDBlueprint ? pEDBlueprint->m_Name : X_STR("Blueprint")
        , imgui_node_graph::node::connection_pod::type::guid{ gb_blueprint::master::t_class_guid.m_Value }
        , types_info::s_BlueprintColor
        , false
        , X_STR(" Connect or Replace Self ") 
    );
    Pod.m_UserData     = gBlueprintInstance.m_Value;

    NewNode->EndSetup();
    xndptr_s<imgui_node_graph::link>  Link;
    Link.New();
    Link->m_ConnectionHooks[0].m_gNode = gBlueprintNodeGuid;
    Link->m_ConnectionHooks[0].m_gPod  = gNodePod;
    Link->m_ConnectionHooks[1].m_gNode = gComponentNodeGuid;
    Link->m_ConnectionHooks[1].m_gPod  = gComponentBlueprintPod;
    Link->m_Guid.Reset();
    AddLink( std::move(Link) );

    Pos.y += 20 +  NewNode->getSize().y;
            
    Pos = AddNodeProperties( NewNode, gBlueprintNodeGuid, Pos );
    AddNode( xndptr_s<imgui_node_graph::node>{ NewNode.TransferOwnership() } );

    return Pos;
}

//-------------------------------------------------------------------------------------------------

ImVec2 tab::AddEntityRefNode
(
    const imgui_node_graph::node::guid                      gComponentNodeGuid 
    , const imgui_node_graph::node::connection_pod::guid    gComponentBlueprintPod
    , const gb_component::entity::guid                      gEntityInstance
    , ImVec2                                                Pos
)
{
    const imgui_node_graph::node::guid  gEntityRefNodeGuid( imgui_node_graph::node::guid::RESET );
    xndptr_s<graph_node>                NewNode;
            
    NewNode.New( *this, gEntityRefNodeGuid,X_STR("Entity.ref"), Pos, true );

    auto& Entity   = *m_pEntity->getGlobalInterface().m_GameMgr.findEntity<gb_component::entity>( gEntityInstance );
            
    NewNode->m_pProperty    = Entity.getLinearPropInterface();
    NewNode->m_pEditorProps = m_LayerDoc.findEDEntry( Entity.getGuid() );

    NewNode->setColor( types_info::s_EntityColorRef );
            
//    NewNode->m_InstanceGuid = gEntityInstance.m_Value;

//            Pos.x += 2 * NewNode->getSize().x;

    const imgui_node_graph::node::connection_pod::guid gNodePod{ imgui_node_graph::node::connection_pod::guid::RESET };
    auto& Pod = NewNode->AddInputPod
    (
        gNodePod 
        , Entity.getGlobalInterface().m_Type.getCategoryName().m_Char
        , imgui_node_graph::node::connection_pod::type::guid{ types_info::s_EntityClassGuidRef }
        , types_info::s_EntityColorRef
        , false
        , X_STR(" Connect or Replace Self ") 
    );
    Pod.m_UserData     = gEntityInstance.m_Value;

    NewNode->EndSetup();
    xndptr_s<imgui_node_graph::link>  Link;
    Link.New();
    Link->m_ConnectionHooks[0].m_gNode = gEntityRefNodeGuid;
    Link->m_ConnectionHooks[0].m_gPod  = gNodePod;
    Link->m_ConnectionHooks[1].m_gNode = gComponentNodeGuid;
    Link->m_ConnectionHooks[1].m_gPod  = gComponentBlueprintPod;
    Link->m_Guid.Reset();
    AddLink( std::move(Link) );

    Pos.y += 20 +  NewNode->getSize().y;
            
    Pos = AddNodeProperties( NewNode, gEntityRefNodeGuid, Pos );
    AddNode( xndptr_s<imgui_node_graph::node>{ NewNode.TransferOwnership() } );

    return Pos;
}

//-------------------------------------------------------------------------------------------------

ImVec2 tab::AddConstantNode(
    const gb_component::const_data::guid                gConstantDataInstace,  
    const gb_component::const_data::type::guid          gConstantDataType, 
    const imgui_node_graph::node::guid                  gNodeGuid, 
    const imgui_node_graph::node::connection_pod::guid  gConstantDataPod,
    ImVec2                                              Pos )
{
    m_GuidToClassGuid.cpFindOrAddEntry( gConstantDataType.m_Value, []( types_info::guid& Entry )
    {
        Entry = types_info::s_ConstantDataClassGuid;
    });

    const imgui_node_graph::node::guid  gConstantDataNodeGuid( imgui_node_graph::node::guid::RESET );
    xndptr_s<graph_node>                NewNode;
            
    NewNode.New( *this, gConstantDataNodeGuid, gb_component::const_data::type::t_class_string, Pos, true );
                
    auto& ConstantData   = m_pEntity->getGlobalInterface().m_GameMgr.m_ConstDataDB.getEntry( gConstantDataInstace );
    
    NewNode->m_pProperty            = &ConstantData;
    NewNode->m_pEditorProps         = m_ConstDataDoc.findEDConstData( gConstantDataInstace );
    NewNode->setColor( types_info::s_ConstantDataColor );
    NewNode->m_InstanceGuid = gConstantDataInstace.m_Value;

//            Pos.x += 2 * NewNode->getSize().x;

    const imgui_node_graph::node::connection_pod::guid gNodePod{ imgui_node_graph::node::connection_pod::guid::RESET };
    NewNode->AddInputPod
    (
        gNodePod 
        , m_GameGraphDoc.getGraph().m_ConstDataTypeDB.getEntry( gConstantDataType ).getEditorName()
        , imgui_node_graph::node::connection_pod::type::guid{ gConstantDataType.m_Value }
        , types_info::s_ConstantDataColor
        , false
        , X_STR(" Connect or Replace Self ") 
    );

    NewNode->EndSetup();
    xndptr_s<imgui_node_graph::link>  Link;
    Link.New();
    Link->m_ConnectionHooks[0].m_gNode = gConstantDataNodeGuid;
    Link->m_ConnectionHooks[0].m_gPod  = gNodePod;
    Link->m_ConnectionHooks[1].m_gNode = gNodeGuid;
    Link->m_ConnectionHooks[1].m_gPod  = gConstantDataPod;
    Link->m_Guid.Reset();
    AddLink( std::move(Link) );

    Pos.y += 20 +  NewNode->getSize().y;
            
    Pos = AddNodeProperties( NewNode, gConstantDataNodeGuid, Pos );
    AddNode( xndptr_s<imgui_node_graph::node>{ NewNode.TransferOwnership() } );

    return Pos;
}

//-------------------------------------------------------------------------------------------------

ImVec2 tab::AddNodeProperties( graph_node& NodeTest, const imgui_node_graph::node::guid gNodeGuid, ImVec2 Pos )
{
    xproperty_v2::entry Entry;

    //
    // process
    //
    auto Process = [&]()
    {
        if( Entry.m_Data.getTypeIndex() == xproperty_v2::prop_type::GUID 
            /*&& Entry.m_Data. Data.getFlags().m_READ_ONLY == false*/ )
        {
            const auto& PropInfo    = *reinterpret_cast<const xproperty_v2::info::guid*>( Entry.m_pDetails );
            const auto  gEntry      = Entry.m_Data.get<xproperty_v2::prop_type::GUID>();
            const auto  gClassGuid  = gb_component::extended_guid<void,void,0>{ PropInfo.m_Type }; 

            if( gClassGuid.getClassGuid() == gb_component::const_data::type::t_class_guid && gClassGuid.getNameCRC().isValid() )
            {
                const imgui_node_graph::node::connection_pod::guid          gConstantDataPod        { imgui_node_graph::node::connection_pod::guid::RESET };

                NodeTest.AddOuputPod( 
                    gConstantDataPod 
                    , Entry.m_Name
                    , imgui_node_graph::node::connection_pod::type::guid{ gClassGuid.m_Value }
                    , types_info::s_ConstantDataColor
                    , true
                    , X_STR(" Connect or Add New ") 
                );

                if( gEntry )
                {
                    Pos = AddConstantNode
                    (
                        gb_component::const_data::guid{ gEntry },  
                        gb_component::const_data::type::guid{ PropInfo.m_Type }, 
                        gNodeGuid, 
                        gConstantDataPod,
                        Pos 
                    );
                }
            }
            else if( gClassGuid.getClassGuid() == gb_blueprint::master::t_class_guid )
            {
                const imgui_node_graph::node::connection_pod::guid          gBlueprintDataPod        { imgui_node_graph::node::connection_pod::guid::RESET };
                        
                NodeTest.AddOuputPod( 
                    gBlueprintDataPod 
                    , Entry.m_Name
                    , imgui_node_graph::node::connection_pod::type::guid{ PropInfo.m_Type }
                    , types_info::s_BlueprintColor
                    , true
                    , X_STR(" Connect or Add New ") 
                );
                    
                if( gEntry )
                {
                    Pos = AddBlueprintNode
                    (
                        gNodeGuid 
                        , gBlueprintDataPod 
                        , gb_blueprint::master::guid{ gEntry }  
                        , Pos 
                    );
                }
            }
            else if( gClassGuid.getClassGuid() == gb_component::entity::t_class_guid )
            {
                if( PropInfo.m_Flags.m_READ_ONLY == true || PropInfo.m_Flags.m_NOT_VISIBLE == true )
                    return;

                const imgui_node_graph::node::connection_pod::guid          gEntityPod        { imgui_node_graph::node::connection_pod::guid::RESET };

                NodeTest.AddOuputPod( 
                    gEntityPod 
                    , Entry.m_Name
                    , imgui_node_graph::node::connection_pod::type::guid{ types_info::s_EntityClassGuidRef }
                    , types_info::s_EntityColorRef
                    , true
                    , X_STR(" Connect or Add New ") 
                );
                    
                if( gEntry )
                {
                    Pos = AddEntityRefNode
                    (
                        gNodeGuid 
                        , gEntityPod 
                        , gb_component::entity::guid{ gEntry }
                        , Pos 
                    );
                }
            }
        }
    };

    //
    // Enum properties
    //
    NodeTest.m_pProperty->EnumProperty( [&]() ->xproperty_v2::entry&
    {
        if( Entry.m_Data.isValid() ) 
        {
            if( x_strncmp<xchar>( Entry.m_Name, "Comp", 4 ) == 0 && ( Entry.m_Name[4] == '[' || Entry.m_Name[4] == '(') )
            {
                // skip components....
            }
            else
            {
                Process();
            }
        }
        Entry.m_Name.clear();
        return Entry;
    }, true, xproperty_v2::base::enum_mode::REALTIME );

    if( x_strncmp<xchar>( Entry.m_Name, "Comp", 4 ) == 0 && ( Entry.m_Name[4] == '[' || Entry.m_Name[4] == '(') )
    {
        // skip components....
    }
    else
    {
        Process();
    }
    

    return Pos;
}

//-------------------------------------------------------------------------------------------------

void tab::HandleConstantDataSelector( void )
{
    m_ConstantDataList.Render();
    auto pConstantData = m_ConstantDataList.getNewSelected();
    if( pConstantData == nullptr ) return;

    auto& ConstantData = *pConstantData; 
    if( m_pSelectedPod->m_bIsInput )
    {
        //... This pod could have many outputs

        // Find which nodes are connected to this constant data node and change their constant property
        // Change the current constant node with new properties
        xvector<imgui_node_graph::link::guid> gLinksList;
        getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

        // We will leave the old pos in place it looks cleaner
        // m_pSelectedPod->m_Node.setPos( m_NewPos );

        // TODO: Must delete all dependencies of the constant node
        for( const auto& gLink : gLinksList )
        {
            const auto&     Link        = *m_LinkDB[ gLink ];
            const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
            auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
            auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

            Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, ConstantData.getGuid().m_Value );
        }

        // Set the new constant data property pointer
        auto& GraphNode             = m_pSelectedPod->m_Node.SafeCast<graph_node>();
        GraphNode.m_pProperty       = &ConstantData;
        GraphNode.m_pEditorProps    = m_ConstDataDoc.findEDConstData( ConstantData.getGuid() );

        // Make the inspector to look at this constant data
        imgui_node_graph::base::SingleSelect( GraphNode.getGuid() );
        SingleSelect( ConstantData.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );

    }
    else 
    {
        // Any input for a constant data can only be one
        xvector<imgui_node_graph::link::guid> gLinksList;
        getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );
        x_assert( gLinksList.getCount() < 2 );
        bool bCreateNewContant = true;
                    
        for( const auto& gLink : gLinksList )
        {
            const auto&     Link        = *m_LinkDB[ gLink ];
            const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
            auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
            auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

            // If the user is asking us to create the same constant data then we are going to bail out
            if( Node.SafeCast<graph_node>().m_pProperty->SafeCast<gb_component::const_data>().getGuid() == ConstantData.getGuid() )
            {
                bCreateNewContant = false;
                break;
            }

            // Delete the link
            m_LinkDB.erase( gLink );

            // If there is no other node that uses the constant data then nuke it
            xvector<imgui_node_graph::link::guid> gOtherLinksList;
            getLinksConnectingFromPod( gOtherLinksList, OtherPod.getGuid() );

            if( gOtherLinksList.getCount() == 0 )
            {
                m_NodeDB.erase( Node.getGuid() );
            }
        }

        // If it has a link then Change the constant node with new property and new pos
        // else create a constant node assign new properties and new pos and create a new link
        if( bCreateNewContant )
        {
            auto& NodeGraph = m_pSelectedPod->m_Node.SafeCast<graph_node>();
            NodeGraph.m_pProperty->SetProperty( m_pSelectedPod->m_Name, ConstantData.getGuid().m_Value );

            // We need to create a node for this constant data
            AddConstantNode
            (
                ConstantData.getGuid(),
                ConstantData.getType().getGuid(),
                NodeGraph.getGuid(),
                m_pSelectedPod->getGuid(),
                m_NewPos
            );
        }
    }
}

//-------------------------------------------------------------------------------------------------

void tab::HandleBlueprintSelector( void )
{
    m_BlueprintList.Render();
    auto pBlueprint = m_BlueprintList.getNewSelected();
    if( pBlueprint == nullptr ) return;

    auto& Blueprint = *pBlueprint;

    /*
    if( m_pSelectedPod->m_bIsInput )
    {
        //... This pod could have many outputs

        // Find which nodes are connected to this constant data node and change their constant property
        // Change the current constant node with new properties
        xvector<imgui_node_graph::link::guid> gLinksList;
        getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

        // We will leave the old pos in place it looks cleaner
        // m_pSelectedPod->m_Node.setPos( m_NewPos );

        // TODO: Must delete all dependencies of the constant node
        for( const auto& gLink : gLinksList )
        {
            const auto&     Link        = *m_LinkDB[ gLink ];
            const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
            auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
            auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

            Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, Blueprint.getGuid().m_Value );
        }

        // Set the new constant data property pointer
        auto& GraphNode         = m_pSelectedPod->m_Node.SafeCast<graph_node>();
        GraphNode.m_pProperty   = &Blueprint;
                        
        // Make the inspector to look at this constant data
        SingleSelect( GraphNode.getGuid() );
        m_Inspector.SetProperty( &GraphNode );//.m_pProperty );
    }
    else
    */

//          x_assert( m_pSelectedPod->m_bIsInput );

//           const imgui_node_graph::node::guid                  gEntityNodeGUID,
//         const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod,
//       gb_blueprint::master*                               pBlueprint,
//     ImVec2                                              Pos )
 
    // Are we dealing with the blue print editor node?
    if( m_pSelectedPod->m_gType == types_info::s_BlueprintCreateClassGuid )
    {
//        x_assert( m_BlueprintList.isCreated() );
        // Setup a newblueprint
        Blueprint.UpdateAsNew( *m_pEntity );
        m_pEntity->setupBlueprintGuid( Blueprint.getGuid() );

        // We need to create a node for this constant data
        AddBlueprintEditNode
        (
            m_gEntity
            , m_pSelectedPod->m_Guid
            , &Blueprint
            , m_NewPos
        );
    }
    else
    {
        if( m_pSelectedPod->m_bIsInput )
        {
            //... This pod could have many outputs

            // Find which nodes are connected to this constant data node and change their constant property
            // Change the current constant node with new properties
            xvector<imgui_node_graph::link::guid> gLinksList;
            getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

            // We will leave the old pos in place it looks cleaner
            // m_pSelectedPod->m_Node.setPos( m_NewPos );

            // TODO: Must delete all dependencies of the constant node
            for( const auto& gLink : gLinksList )
            {
                const auto&     Link        = *m_LinkDB[ gLink ];
                const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
                auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

                Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, Blueprint.getGuid().m_Value );
            }

            // Set the new constant data property pointer
            auto& GraphNode         = m_pSelectedPod->m_Node.SafeCast<graph_node>();
            GraphNode.m_pProperty   = &Blueprint;
                        
            // Make the inspector to look at this constant data
            SingleSelect( Blueprint.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
            //SingleSelect( Blueprint.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty  );
            //NotifySingleSelect( &GraphNode ); //m_Events.m_ActiveBlueprint.Notify( Blueprint.getGuid(), GraphNode );
            //m_Inspector.SetProperty( &GraphNode );//.m_pProperty );
        }
        else
        {
//            if( m_BlueprintList.isCreated() == false )
            {
                if( m_pSelectedPod->getGuid().m_Value == m_pSelectedPod->m_UserData )
                    return;
            }

            // Delete any links connected to this pod
            xvector<imgui_node_graph::link::guid> ExistingLinks;
            getLinksConnectingFromPod( ExistingLinks, m_pSelectedPod->getGuid() );
            for( auto gLink : ExistingLinks )
            {
                DeleteLink( gLink );
            }

            // We need to create a node for this constant data
            AddBlueprintNode
            (
                m_pSelectedPod->m_Node.getGuid()
                , m_pSelectedPod->m_Guid
                , Blueprint.getGuid()
                , m_NewPos
            );

            // Also update the actual delegate
            m_pSelectedPod->m_Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( m_pSelectedPod->m_Name, Blueprint.getGuid().m_Value );
        }

        /*
        if( m_BlueprintList.isCreated() )
        {
        }
        else
        {
                
        }
        */
    }
            
/*
    {
        // Any input for a constant data can only be one
        xvector<imgui_node_graph::link::guid> gLinksList;
        getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );
        x_assert( gLinksList.getCount() < 2 );
        bool bCreateNewContant = true;
                    
        for( const auto& gLink : gLinksList )
        {
            const auto&     Link        = *m_LinkDB[ gLink ];
            const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
            auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
            auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

            // If the user is asking us to create the same constant data then we are going to bail out
            if( Node.SafeCast<graph_node>().m_pProperty->SafeCast<gb_blueprint::master>().getGuid() == Blueprint.getGuid() )
            {
                bCreateNewContant = false;
                break;
            }

            // Delete the link
            m_LinkDB.erase( gLink );

            // If there is no other node that uses the constant data then nuke it
            xvector<imgui_node_graph::link::guid> gOtherLinksList;
            getLinksConnectingFromPod( gOtherLinksList, OtherPod.getGuid() );

            if( gOtherLinksList.getCount() == 0 )
            {
                m_NodeDB.erase( Node.getGuid() );
            }
        }

        // If it has a link then Change the constant node with new property and new pos
        // else create a constant node assign new properties and new pos and create a new link
        if( bCreateNewContant )
        {
            auto& NodeGraph = m_pSelectedPod->m_Node.SafeCast<graph_node>();
            NodeGraph.m_pProperty->SetProperty( m_pSelectedPod->m_Name, Blueprint.getGuid().m_Value );

            // We need to create a node for this constant data
            AddBlueprint
            (
                Blueprint.getGuid(),
                Blueprint.getType().getGuid(),
                NodeGraph.getGuid(),
                m_pSelectedPod->getGuid(),
                m_NewPos
            );
        }
    }
    */
}

//-------------------------------------------------------------------------------------------------

void tab::HandleEntityRefSelector( void )
{
    if( m_EntityRefList.Render() == false ) return;
    auto& Entity = m_EntityRefList.getSelected();

    if( m_pSelectedPod->m_bIsInput )
    {
        //... This pod could have many outputs

        // Find which nodes are connected to this constant data node and change their constant property
        // Change the current constant node with new properties
        xvector<imgui_node_graph::link::guid> gLinksList;
        getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

        // We will leave the old pos in place it looks cleaner
        // m_pSelectedPod->m_Node.setPos( m_NewPos );

        // TODO: Must delete all dependencies of the constant node
        for( const auto& gLink : gLinksList )
        {
            const auto&     Link        = *m_LinkDB[ gLink ];
            const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
            auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
            auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

            Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, Entity.getGuid().m_Value );
        }

        // Set the new constant data property pointer
        auto& GraphNode             = m_pSelectedPod->m_Node.SafeCast<graph_node>();
        GraphNode.m_pProperty       = Entity.getLinearPropInterface();
        GraphNode.m_pEditorProps    = m_LayerDoc.findEDEntry( Entity.getGuid() );

        // Make the inspector to look at this constant data
        //SingleSelect( GraphNode.getGuid() );
        
        SingleSelect( Entity.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
        //m_GameGraphDoc.m_Events.m_ActiveEntity.Notify( Entity.getGuid(), &GraphNode );
        //m_Inspector.SetProperty( &GraphNode );//.m_pProperty );
    }
    else
    {
        if( m_EntityRefList.isCreated() == false )
        {
            if( m_pSelectedPod->getGuid().m_Value == m_pSelectedPod->m_UserData )
                return;
        }

        // Delete any links connected to this pod
        xvector<imgui_node_graph::link::guid> ExistingLinks;
        getLinksConnectingFromPod( ExistingLinks, m_pSelectedPod->getGuid() );
        for( auto gLink : ExistingLinks )
        {
            DeleteLink( gLink );
        }

        // We need to create a node for this constant data
        AddEntityRefNode
        (
            m_pSelectedPod->m_Node.getGuid()
            , m_pSelectedPod->m_Guid
            , Entity.getGuid()
            , m_NewPos
        );

        // Also update the actual delegate
        m_pSelectedPod->m_Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( m_pSelectedPod->m_Name, Entity.getGuid().m_Value );
    }
}

//-------------------------------------------------------------------------------------------------
        
void tab::HandleEntitySelector( void )
{
    auto& GraphNode = m_pSelectedPod->m_Node.SafeCast<graph_node>();
    x_assert( GraphNode.m_pProperty->isKindOf<tab>() == false );

    const auto Guid         = m_pEntity->getGuid();
    const bool inWorld      = m_pEntity->isInWorld();
    const bool isComposer   = m_pSelectedPod->m_bIsInput == false;
    const auto Pos          = isComposer ? m_NewPos : m_pSelectedPod->m_Node.getPos();

    //
    // If there is anything currently connected we must delete all connections
    //
    {
        xvector<imgui_node_graph::link::guid> gLinksList;
        getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

        x_assert( gLinksList.getCount() == 1 );
        {
            const auto&     Link        = *m_LinkDB[ gLinksList[0] ];
            const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
            const auto      gOtherNode  = Link.m_ConnectionHooks[iOther].m_gNode;
            
            x_assert( Link.m_ConnectionHooks[1 - iOther].m_gNode == m_pSelectedPod->m_Node.getGuid() );
            x_assert( Link.m_ConnectionHooks[1 - iOther].m_gPod  == m_pSelectedPod->m_Guid           );
            x_assert( m_NodeDB.find( gOtherNode ) != m_NodeDB.end() );

            // Delete the link to avoid destroying the Composer link
            m_LinkDB.erase( gLinksList[0] );
            if( isComposer )
            {
                if( isEditingBlueprint() )
                {
                    // Make sure we errase the link to the blueprint as well
                    gLinksList.DeleteAllEntries();

                    getLinksConnectingFromPod( gLinksList, m_NodeDB.find(gOtherNode)->second->getPod(1).getGuid() );
                    if( gLinksList.getCount() ) 
                    {
                        m_LinkDB.erase( gLinksList[0] );
                    }
                }
    
                // delete the entity with its dependencies
                DeleteNodeWithDependencies( gOtherNode );
            }
            else
            {
                if( isEditingBlueprint() )
                {
                    // Make sure we errase the link to the blueprint as well
                    gLinksList.DeleteAllEntries();
                    getLinksConnectingFromPod( gLinksList, m_NodeDB.find( m_pSelectedPod->m_Node.getGuid() )->second->getPod(1).getGuid() );
                    if( gLinksList.getCount() ) m_LinkDB.erase( gLinksList[0] ); 
                }

                // delete the entity with its dependencies
                DeleteNodeWithDependencies( m_pSelectedPod->m_Node.getGuid() );
            }
        }
    }

    const gb_component::entity::guid                    gEntityGuid          { Guid };//gb_component::entity::guid::RESET };
    const imgui_node_graph::node::guid                  gEntityNodeGUID      { imgui_node_graph::node::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gEntityEntityPod     { imgui_node_graph::node::connection_pod::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gEntityComponentPod  { imgui_node_graph::node::connection_pod::guid::RESET };
    const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod  { imgui_node_graph::node::connection_pod::guid::RESET };

    auto& EntityType = m_GameGraphDoc.getGraph().m_CompTypeDB.getEntry( m_ComponentList.getSelectedGuid() );
    m_pEntity = &m_GameGraphDoc.getGraph().CreateEntity( EntityType, gEntityGuid );

    AddEntity
    (
        gEntityNodeGUID
        , m_pGraphNode->getGuid()
        , m_pGraphNode->getPod(0).getGuid()
        , gEntityComponentPod
        , gEntityEntityPod
        , gEntityBlueprintPod
        , m_pEntity
        , Pos 
    );

    //
    // Add the blue print link
    //
    if( isEditingBlueprint() )
    {
        xndptr_s<imgui_node_graph::link> xpEntry;
        xpEntry.New();
        xpEntry->m_Guid.Reset();

        // Find the blueprint node
        imgui_node_graph::node* pBlueprintNode = nullptr;
        for( auto& Entry : m_NodeDB )
        {
            auto& E = Entry.second->SafeCast<graph_node>();
            if( E.m_pProperty && E.m_pProperty->isKindOf<gb_blueprint::master>() )
            {
                if( E.getName() == X_STR("Blueprint.Create") )
                {
                    pBlueprintNode = &E;
                    break;
                }
            }
        }
        x_assert( pBlueprintNode );

        xpEntry->m_ConnectionHooks[0].m_gNode   = gEntityNodeGUID;
        xpEntry->m_ConnectionHooks[0].m_gPod    = gEntityBlueprintPod;
        xpEntry->m_ConnectionHooks[1].m_gNode = pBlueprintNode->getGuid();
        xpEntry->m_ConnectionHooks[1].m_gPod  = pBlueprintNode->getPod(0).getGuid();
        AddLink( std::move(xpEntry) );
    }

    if( inWorld ) m_pEntity->linearAddToWorld();

    m_Selected.DeleteAllEntries();
    m_Selected.append( gEntityNodeGUID );
}

//-------------------------------------------------------------------------------------------------
        
void tab::HandleComponentSelector( void )
{
    auto& GraphNode = m_pSelectedPod->m_Node.SafeCast<graph_node>();

    if( m_pSelectedPod->m_bIsInput )
    {
        // Delete all
        {
            xvector<imgui_node_graph::link::guid> gLinksList;
            getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

            // Delete all the links from/to this node
            for( auto gLink : gLinksList )
            {
                m_LinkDB.erase( gLink );
            }

            // Delete Node
            DeleteNode( m_pSelectedPod->m_Node.getGuid() );
        }
        
        // Add the new component
        AddComponent(
            m_gEntity
            , m_NodeDB.find( m_gEntity )->second->getPod(2).getGuid() 
            , &m_GameGraphDoc.getGraph().CreateComponent( m_GameGraphDoc.getGraph().m_CompTypeDB.getEntry( m_ComponentList.getSelectedGuid() ), *m_pEntity )
            , m_NewPos );
    }
    else
    {
        if( GraphNode.m_pProperty->isKindOf<gb_component::entity>() == true )
        {
            AddComponent(
                m_pSelectedPod->m_Node.getGuid()
                , m_pSelectedPod->getGuid()
                , &m_GameGraphDoc.getGraph().CreateComponent( m_GameGraphDoc.getGraph().m_CompTypeDB.getEntry( m_ComponentList.getSelectedGuid() ), *m_pEntity )
                , m_NewPos );

        }
    }
}


//-------------------------------------------------------------------------------------------------

void tab::ChooseComponentSelector( void )
{
    if( m_ComponentList.Render() == false ) return;

    x_assert( m_ComponentList.getSelectedGuid().m_Value );
    if( m_ComponentList.isEntityMode() )
    {
        HandleEntitySelector();
    }
    else if( m_ComponentList.isComponentMode() )
    {
        HandleComponentSelector();
    }
}

//-------------------------------------------------------------------------------------------------

void tab::onRender( void ) noexcept
{
    const imgui_node_graph::node::guid LastSelected = ( m_Selected.getCount() >= 1 ) ? m_Selected[0] : imgui_node_graph::node::guid{ nullptr };

    if( m_pEntity == nullptr || m_pEntity->getGuid() != m_gBackupEntity ) return;

    //
    // Let the parent deal with all the work
    //
    t_parent::onRender();

    //
    // Check compilation situation
    //
    {
        const auto Scale = getScale();
        ImGui::SetWindowFontScale(Scale);

        for( gb_component::base* pEntry = m_pEntity; pEntry; pEntry = pEntry->getNextComponent() )
        {
            auto& Component         = *pEntry;

            xvector<xstring> IssueList;
            if( auto Error = Component.linearCheckResolve( IssueList ); IssueList.getCount()>0 || Error.isError() )
            {
                const auto Pos              = 
                [&]
                {
                    auto pProp = pEntry->getLinearPropInterface();
                    for( auto& Entry : m_NodeDB )
                    {
                        auto& E = Entry.second->SafeCast<graph_node>();
                        if( E.m_pProperty == pProp )
                        {
                            return E.getPos();
                        }
                    }

                    x_assert(false);
                    return ImVec2(0.0f,0.0f);
                }();
                const auto NodePos          = (m_CurrentPosition + Pos - ImVec2(-8, 16)) * Scale;

                ImGui::SetCursorPos( NodePos );
                if( Error.isError() ) 
                {
                    ImGui::TextColored( ImColor( 1.0f, 0.5f, 0.5f ), ICON_FA_TIMES_CIRCLE" Fail, Issue%c (%d)", IssueList.getCount<int>()>1?'s':' ', IssueList.getCount<int>() );
                }
                else
                {
                    ImGui::TextColored( ImColor( 1.0f, 0.8f, 0.0f ), ICON_FA_EXCLAMATION_TRIANGLE " Issue%c (%d)", IssueList.getCount<int>()>1?'s':' ', IssueList.getCount<int>() );
                }

                if (ImGui::IsItemHovered()) 
                {
                    ImGui::BeginTooltip();
                    ImGui::PushTextWrapPos(450.0f);
                        
                    if( Error.isError() ) 
                    {
                        ImGui::Text( ICON_FA_TIMES_CIRCLE " Compilation Fail: %s ", Error.getString() );
                    }
                    else
                    {
                        ImGui::Text( ICON_FA_CHECK_CIRCLE " Compilation: Successful" );
                    }

                    ImGui::Text( ICON_FA_EXCLAMATION_TRIANGLE " Issues%c (%d):", IssueList.getCount<int>()>1?'s':' ', IssueList.getCount<int>() );
                    for( auto& Str : IssueList )
                    {
                        ImGui::TextDisabled( "(%d) ", 1 + IssueList.getIndexByEntry<int>(Str) );
                        ImGui::SameLine(); ImGui::TextWrapped( Str );
                    }
                    ImGui::PopTextWrapPos();
                    ImGui::EndTooltip();
                }

                Error.IgnoreError();
            }
        }

        ImGui::SetWindowFontScale( 1.0f );
    }

    //
    // Deal with opening new selectors
    //
    switch( m_OpenSelector )
    {
        case open_selector::ENTITY:
        case open_selector::COMPONENT:
        {
            m_ComponentList.Open();
            break;
        }
        case open_selector::CONSTANT_DATA:
        {
            m_ConstantDataList.Open();
            break;
        }
        case open_selector::BLUEPRINT:
        {
            m_BlueprintList.Open();
            break;
        }
        case open_selector::ENTITY_REF:
        {
            m_EntityRefList.Open( gbed_entity_composer::entity_dlg::mode::NORMAL );
            break;
        }
        case open_selector::BLUEPRINT_CREATE:
        {
            m_BlueprintList.Open( );
            break;
        }
    }
    m_OpenSelector = open_selector::NONE;

    //
    // Handle selectors
    //
    HandleConstantDataSelector();
    ChooseComponentSelector();
    HandleBlueprintSelector();
    HandleEntityRefSelector();

    //
    // Set property into inspector
    //
    if( m_Selected.getCount() == 1 )
    {
        if( LastSelected != m_Selected[0] )
        {
            imgui_node_graph::node* pNode = m_NodeDB[ m_Selected[0] ];
            if( pNode )
            {
                graph_node& GraphNode = pNode->SafeCast<graph_node>();

                if( GraphNode.m_pProperty->isKindOf<gb_component::const_data>() )
                {
                    auto& ConstData = GraphNode.m_pProperty->SafeCast<gb_component::const_data>();
                    SingleSelect( ConstData.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
                }
                else if( GraphNode.m_pProperty->isKindOf<gb_blueprint::master>() )
                {
                    auto& BluePrint = GraphNode.m_pProperty->SafeCast<gb_blueprint::master>();
                    SingleSelect( BluePrint.getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
                }
                else if( GraphNode.m_pProperty->isKindOf<gb_component::base>() )
                {
                    SingleSelect( m_pEntity->getGuid(), GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
                }
                else 
                {
                    SingleSelect( GraphNode.m_pEditorProps?GraphNode.m_pEditorProps:GraphNode.m_pProperty );
                }
            }
        }
    }


    //
    // Open Dialogs
    //
    if( m_bOpenErrorPopup )
    {
        ImGui::OpenPopup( ICON_FA_TIMES_CIRCLE" Composer: Fail To Save Entity" );
        m_bOpenErrorPopup = false;
    }

    if( ImGui::BeginPopupModal( ICON_FA_TIMES_CIRCLE" Composer: Fail To Save Entity" ) )
    {
        ImGui::Text( "Fail to save entity due to errors, please check them and try again" ); 
        if( ImGui::Button( "OK", ImVec2(-1,0) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

//-------------------------------------------------------------------------------------------------

void tab::onDeleteComponent( const imgui_node_graph::node& Node )
{
    auto& GraphNode = Node.SafeCast<graph_node>();

    if( GraphNode.m_pProperty->isKindOf<gb_component::base>() )
    {
        if( GraphNode.m_pProperty->isKindOf<gb_component::entity>() )
        {
            /*
            if( GraphNode.m_pEditorProps )
            {
                m_LayerDoc.DestroyEntity( m_pEntity->getGuid() );
            }
            else
            {
                m_pEntity->linearDestroy();
            }
            */

            // We delete only the game entity we assume that the user will deal with the editor version of the entity if any
            m_pEntity->linearDestroy();
            m_pEntity = nullptr;
        }
        else
        {
            auto& BaseNode  = GraphNode.m_pProperty->SafeCast<gb_component::base>();
            if( m_pEntity ) 
            {
                m_pEntity->linearDestroyComponent( BaseNode );

                // This is to make sure the inspector or anyone else looking at this component changes its focus
                SingleSelect( gb_component::entity::guid{ nullptr }, nullptr );
            }
        }
    }
    else if( Node.isKindOf<blue_print_node>() )
    {
        if( m_pEntity ) m_pEntity->setupBlueprintGuid( gb_blueprint::master::guid{ nullptr } );
    }
}

//-------------------------------------------------------------------------------------------------

void tab::onDeleteLink( const imgui_node_graph::link& Link ) 
{
    auto& Node                  = *m_NodeDB[ Link.m_ConnectionHooks[1].m_gNode ];
    auto& Pod                   = Node.getPod( Link.m_ConnectionHooks[1].m_gPod );
    auto& DelegateGraphNode     = Node.SafeCast<graph_node>();

    types_info::guid gTypeInfo;
    if( m_GuidToClassGuid.cpGetOrFailEntry( Pod.m_gType.m_Value, [&]( types_info::guid& Entry ){ gTypeInfo = types_info::s_ConstantDataClassGuid; }) )
    {
        if( gTypeInfo == types_info::s_ConstantDataClassGuid )
        {
            Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( Pod.m_Name, 0 );
        }
    }
    else if(    gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::event_type::t_class_guid 
            ||  gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::interface_type::t_class_guid )
    {
        auto& DelegateComponent     = DelegateGraphNode.m_pProperty->SafeCast<gb_component::base>();
        DelegateGraphNode.m_pProperty->SetProperty( xstring::Make( "Delegates/%s", Pod.m_Name ), u64{0} );
        DelegateComponent.getEntity().linearResolve();
    }
    else if(    Pod.m_gType == types_info::s_BlueprintCreateClassGuid
            ||  Pod.m_gType == types_info::s_BlueprintClassGuid
            ||  Pod.m_gType == types_info::s_EntityClassGuidRef )
    {
            DelegateGraphNode.m_pProperty->SetProperty<u64>( Pod.m_Name, 0 );
    }
}

//-------------------------------------------------------------------------------------------------

void tab::onNotifyNewLinkConnection( const imgui_node_graph::link& Link ) 
{
    auto& Node = *m_NodeDB[ Link.m_ConnectionHooks[1].m_gNode ];
    auto& Pod  = Node.getPod( Link.m_ConnectionHooks[1].m_gPod );

    types_info::guid gTypeInfo;
    if( m_GuidToClassGuid.cpGetOrFailEntry( Pod.m_gType.m_Value, [&]( types_info::guid& Entry ){ gTypeInfo = types_info::s_ConstantDataClassGuid; }) )
    {
        if( gTypeInfo == types_info::s_ConstantDataClassGuid )
        {
            // Delete any links connected to this pod
            xvector<imgui_node_graph::link::guid> ExistingLinks;
            getLinksConnectingFromPod( ExistingLinks, Pod.getGuid() );
            for( auto gLink : ExistingLinks )
            {
                DeleteLink( gLink );
            }

            // Set the new guid into the constant property
            auto& ConstantNode      = *m_NodeDB[ Link.m_ConnectionHooks[0].m_gNode ];
            auto& ConstantGraphNode = ConstantNode.SafeCast<graph_node>();

            Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( Pod.m_Name, ConstantGraphNode.m_InstanceGuid );
        }
    }
    else if(    gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::event_type::t_class_guid 
            ||  gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::interface_type::t_class_guid )
    {
        auto&       EventNode           = *m_NodeDB[ Link.m_ConnectionHooks[0].m_gNode ];
        auto&       EventPod            = EventNode.getPod( Link.m_ConnectionHooks[0].m_gPod );
        auto&       DelegateGraphNode   = Node.SafeCast<graph_node>();

        // Delete any links connected to this pod
        xvector<imgui_node_graph::link::guid> ExistingLinks;
        getLinksConnectingFromPod( ExistingLinks, Pod.getGuid() );
        for( auto gLink : ExistingLinks )
        {
            DeleteLink( gLink );
        }

        // Update the pod user date with the new event guid
        Pod.m_UserData = EventPod.m_UserData;

        // Also update the actual delegate
        DelegateGraphNode.m_pProperty->SetProperty<u64>( xstring::Make( "Delegates/%s", Pod.m_Name ), EventPod.m_UserData );
        auto& DelegateComponent = DelegateGraphNode.m_pProperty->SafeCast<gb_component::base>();
                
        DelegateComponent.getEntity().linearResolve();
    }
    else if(    Pod.m_gType == types_info::s_BlueprintCreateClassGuid
            ||  Pod.m_gType == types_info::s_BlueprintClassGuid )
    {
        auto&       EventNode           = *m_NodeDB[ Link.m_ConnectionHooks[0].m_gNode ];
        auto&       EventPod            = EventNode.getPod( Link.m_ConnectionHooks[0].m_gPod );
        auto&       DelegateGraphNode   = Node.SafeCast<graph_node>();

        // Delete any links connected to this pod
        xvector<imgui_node_graph::link::guid> ExistingLinks;
        getLinksConnectingFromPod( ExistingLinks, Pod.getGuid() );
        for( auto gLink : ExistingLinks )
        {
            DeleteLink( gLink );
        }

        // Also update the actual delegate
        DelegateGraphNode.m_pProperty->SetProperty<u64>( Pod.m_Name, EventPod.m_UserData );
    }
}

//-------------------------------------------------------------------------------------------------

bool tab::onLinkDrop( imgui_node_graph::node::connection_pod& Pod, ImVec2 Pos )
{
    types_info::guid gType;
    if( u32( Pod.m_gType.m_Value ) == gb_component::const_data::type::t_class_guid.m_Value )
    {
        gType = types_info::s_ConstantDataClassGuid;
    }
    else if( u32( Pod.m_gType.m_Value) == types_info::s_BlueprintClassGuid.m_Value )
    {
        gType = types_info::s_BlueprintClassGuid;
    }
    else if( u32( Pod.m_gType.m_Value) == types_info::s_BlueprintCreateClassGuid.m_Value )
    {
        gType = types_info::s_BlueprintCreateClassGuid;
    }
    else if( u32( Pod.m_gType.m_Value) == types_info::s_EntityClassGuidRef.m_Value )
    {
        gType = types_info::s_EntityClassGuidRef;
    }
    else if( m_GuidToClassGuid.cpGetOrFailEntry( Pod.m_gType.m_Value, [&]( types_info::guid& Entry )
    {
        gType = Entry;
    }) == false ) return false;

    m_pSelectedPod  = &Pod;
    m_NewPos        = ( Pos - ImGui::GetWindowPos() ) / m_Scale - m_CurrentPosition ;
    switch( gType.m_Value )
    {
        case types_info::s_EntityClassGuid.m_Value:
        {
            m_OpenSelector = open_selector::ENTITY;
            m_ComponentList.EntityMode();
            break;
        }
        case types_info::s_GameGuid.m_Value:
        {
            break;
        }
        case types_info::s_ComponentClassGuid.m_Value:
        {
            m_OpenSelector = open_selector::COMPONENT;
            m_ComponentList.ComponentMode();
            break;
        }
        case types_info::s_ConstantDataClassGuid.m_Value:
        {
            m_OpenSelector = open_selector::CONSTANT_DATA;
            m_ConstantDataList.setFilterByType( gb_component::const_data::type::guid{ Pod.m_gType.m_Value } );
            break;
        }
        case types_info::s_BlueprintClassGuid.m_Value:
        {
            m_OpenSelector = open_selector::BLUEPRINT;
            break;
        }
        case types_info::s_EntityClassGuidRef.m_Value:
        {
            m_OpenSelector = open_selector::ENTITY_REF;
            m_EntityRefList.Refresh( m_GameGraphDoc.getGraph() );
            break;
        }
        case types_info::s_BlueprintCreateClassGuid.m_Value:
        {
            // We can only create entries if we are not editing the blueprint
            if( m_pBlueprint == nullptr )
            {
                m_OpenSelector = open_selector::BLUEPRINT_CREATE;
            }
            break;
        }
        default:
        {
            x_assume( false );
            break;
        }
    }

    return false;
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------



