//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class constant_data_dlg
{
public:

    x_incppfile                         constant_data_dlg       ( gbed_constant_data::document& Document ) : m_View{ Document } {}
    x_incppfile         void            Open                    ( void );
    x_incppfile         bool            Render                  ( void );
    x_inline            void            setFilterByType         ( gb_component::const_data::type::guid gType ) { m_View.setTypeFilter(gType); }
    x_inline            auto            getNewSelected          ( void ) { return m_View.getAndClearSelected(); }

protected:

    gbed_constant_data::view                m_View;
/*
protected:

    x_incppfile         void            RefreshList             ( void );

protected:

    xvector<gb_component::const_data*>      m_List;
    int                                     m_iSelectedEntry{ -1 };
    ImGuiTextFilter                         m_Filter;
    gb_component::const_data::type::guid    m_gType;
    gb_game_graph::base*                    m_pBase{ nullptr };
*/
};



