//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#ifndef _GBED_ENTITY_COMPOSER_H
#define _GBED_ENTITY_COMPOSER_H
#pragma once

#include "xTools/gb_editor_base/gbed_base.h"
#include "xTools/gb_editor_plugins/GameGraph/gbed_GameGraph.h"
#include "xTools/gb_editor_plugins/Blueprint/gbed_Blueprint.h"
#include "xTools/gb_editor_plugins/ConstantData/gbed_ConstantData.h"
#include "xTools/gb_editor_plugins/EntityComposer/gbed_EntityComposer.h"
#include "xPlugins/imgui/imguiNodeGraph.h"
#include "xTools/gb_editor_plugins/Inspector/gbed_Inspector.h"
#include "xTools/gb_editor_plugins/Layers/gbed_Layer.h"

namespace gbed_entity_composer
{
    #include "xTools/gb_editor_plugins/EntityComposer/gbed_ComponentDlg.h"
    #include "xTools/gb_editor_plugins/EntityComposer/gbed_ConstantDataDlg.h"
    #include "xTools/gb_editor_plugins/EntityComposer/gbed_EntityDlg.h"
    #include "xTools/gb_editor_plugins/EntityComposer/gbed_EntityComposerTab.h"
}

#endif
