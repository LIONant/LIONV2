//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class entity_dlg
{
public:

    enum class mode : u8
    {
        NORMAL,
        CREATE_ONLY
    };

public:

    x_incppfile                         entity_dlg                  ( void ) = default;
    x_incppfile         bool            isCreated                   ( void ) const;
    x_incppfile         void            Refresh                     ( gb_game_graph::base& Base );
    x_incppfile         void            Open                        ( mode Mode );
    x_incppfile         bool            Render                      ( void );
    x_incppfile         auto&           getSelected                 ( void );

protected:

    struct folder
    {
        xstring                             m_Name;
        xvector<folder>                     m_Folders       {};
        xvector<gb_blueprint::master*>      m_Bluesprints   {};
    };

protected:

    gb_component::entity*                           m_pSelected         { nullptr };
    folder                                          m_Folder            {};
    ImGuiTextFilter                                 m_Filter;
    gb_game_graph::base*                            m_pBase             { nullptr };
    mode                                            m_Mode;
    bool                                            m_bCreated          { false };
};



