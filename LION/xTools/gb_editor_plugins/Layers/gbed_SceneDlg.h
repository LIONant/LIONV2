//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class scene_dlg 
{
public:

    enum class state : u8
    {
        CLOSE,
        OPEN,
        OK,
        CANCEL
    };

public:

    state Dialog( const char* pStr, gbed_layer::document& Document )
    {
        state State = state::CLOSE;
        //ImGui::SetNextWindowSizeConstraints(ImVec2(400, 200), ImVec2(400, 200));
        ImGui::SetNextWindowSize(ImVec2(400, 200), ImGuiSetCond_FirstUseEver);
        if( ImGui::BeginPopupModal( pStr ) )
        {
            State = Guts( Document );
            ImGui::EndPopup();
        }
        return State;
    }

    auto& getActivePath( void ) const
    {
        return m_List[m_Index].m_FileName;
    }

protected:

    struct scene_node
    {
        xstring                             m_Name;
        gbed_layer::document::scene::guid   m_Guid;
        xwstring                            m_FileName;
    };

protected:

    state Guts( gbed_layer::document& Document )
    {
        state State = state::OPEN;

        if( m_bCollectedData == false )
        {
            Refresh( Document );
            m_bCollectedData = true;
            m_Index          = -1;
        }

        //
        // Update buttons
        //
        if( ImGui::Button( "Refresh" ) )
        {
            m_bCollectedData = false;
        }

        //
        // List of scenes
        //
        ImGui::Separator();
        ImGui::BeginChild( "Child", ImVec2(0,-20) );
        for( auto& Entry : m_List )
        {
            if( ImGui::Selectable( Entry.m_Name, m_Index == m_List.getIndexByEntry(Entry) ) )
            {
                m_Index = m_List.getIndexByEntry<int>(Entry);
            }
        }
        ImGui::EndChild();

        //
        // Action buttons
        // 
        if( ImGui::Button( "Load", ImVec2(120,0) ) )
        {
            if( m_Index != -1 ) State = state::OK;
            else                State = state::CANCEL; 
            m_bCollectedData = false;
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine(); if( ImGui::Button( "Cancel", ImVec2(120,0) ) )
        {
            State               = state::CANCEL;
            m_bCollectedData    = false;
            ImGui::CloseCurrentPopup();
        }

        return State;
    }

    void Refresh( gbed_layer::document& Document )
    {
        m_List.DeleteAllEntries();
        for( auto & p : std_fs::directory_iterator(Document.getScenePath()) )
        {
            xtextfile File;
            xwstring FileName;
                
            FileName.Copy( &std::wstring( p.path() )[0] );

            if( auto Err = File.openForReading( FileName ); Err.isOK() )
            {
                gbed_layer::document::scene Scene;

                File.ReadRecord();
                Scene.Load( File );

                auto& Node = m_List.append();
                    
                Node.m_Name         = Scene.m_Name;
                Node.m_Guid         = Scene.m_Guid;
                Node.m_FileName     = FileName;
            }
            else
            {
                X_LOG_CHANNEL_ERROR( Document.getMainDoc().m_LogChannel, "Error fail to examine a scene (%s) so it will be missing in the scene, %s"
                        , static_cast<const xchar*>( FileName.To<xchar>() )
                        , Err.getString() );
            }
        }
    }

protected:

    xvector<scene_node> m_List;
    bool                m_bCollectedData{ false };
    int                 m_Index = -1;
};

