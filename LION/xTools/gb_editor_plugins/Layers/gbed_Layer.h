//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#ifndef _GBED_LAYERS_H
#define _GBED_LAYERS_H
#pragma once

#include "xTools/gb_editor_base/gbed_base.h"
#include "xTools/gb_editor_plugins/GameGraph/gbed_GameGraph.h"
#include "xTools/gb_editor_plugins/Blueprint/gbed_Blueprint.h"

namespace gbed_layer
{
    #include "xTools/gb_editor_plugins/Layers/gbed_LayerDoc.h"
    #include "xTools/gb_editor_plugins/Layers/gbed_SceneDlg.h"
    #include "xTools/gb_editor_plugins/Layers/gbed_ZoneDlg.h"
    #include "xTools/gb_editor_plugins/Layers/gbed_LayerTab.h"
}

#endif
