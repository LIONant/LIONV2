//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class document : public gbed_document::base
{
    x_object_type( document, rtti( gbed_document::base ), is_not_copyable, is_not_movable )
    
public:

    using gbed_document::base::base;

    using                       t_layer_path        = gbed_document::path::filepath< struct tag >;
    using                       t_scene_path        = gbed_document::path::filepath< struct tag >;
    using                       t_edzone_path       = gbed_document::path::filepath< struct tag >;
    using                       t_edentity_path     = gbed_document::path::filepath< struct tag >;
    using                       t_entity_path       = gbed_document::path::filepath< struct tag >;

    x_constexprvar  auto        t_class_string      = X_STR("Layer");
        
    struct edzone
    {
        using guid = gb_component::zone::guid; 

        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );

        guid                                        m_Guid                  { nullptr };
        xstring                                     m_Name                  {};
        bool                                        m_bChanged              { false };
    };

    struct edentity_base { using guid = xguid<edentity_base>; };

    struct zone_folder 
    {
        using guid = xguid<zone_folder>;

        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );

        xstring                                     m_Name                  {};
        bool                                        m_bChanged              { false };
        bool                                        m_bExpanded             { false };
        guid                                        m_Guid                  {};
        guid                                        m_gParent               { nullptr };       // if parent is null then this is a zone 
            
        edzone::guid                                m_ZoneGuid              { nullptr };        // If this is null this folder is not a child of a zone

        xvector<edentity_base::guid>                m_lgEDEntity            {};
        xvector<guid>                               m_lgFolders             {};
    };

    struct edentity 
        : edentity_base
        , xproperty_v2::base
    {
        x_object_type( edentity, rtti(xproperty_v2::base) );

        x_inline                    edentity        ( gbed_game_graph::document& GameGraphDoc ) : m_GameGraphDoc{ GameGraphDoc } {}
        x_inline        auto        getGuid         ( void )                const   { return m_Guid; }
        virtual                    ~edentity        ( void )                        {}
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );

        virtual         const xproperty_v2::table& onPropertyTable( void ) const override
        {
            static prop_table E( this, [&](xproperty_v2::table& E)
            {
                E.AddScope( X_STR("Editor"), [&](xproperty_v2::table& E)
                {
                    E.AddProperty(X_STR("Name"),            m_Name,                 X_STR("Name of the Entity. Please note that this name is only used in the editor."));
                    E.AddProperty(X_STR("EditorGuid"),      m_Guid.m_Value,         X_STR("Guid used by the editor."), xproperty_v2::flags::MASK_READ_ONLY );
                    E.AddProperty(X_STR("TypeGuid"),        m_gEntityType.m_Value,  X_STR("Entity type guid."), xproperty_v2::flags::MASK_READ_ONLY );
                });    

                E.AddDynamicScope(
                    { nullptr }
                    , X_STATIC_LAMBDA_BEGIN (edentity& A) -> bool
                    {
                        return !!A.m_GameGraphDoc.getGraph().findEntity(A.m_gEntity);
                    }
                    X_STATIC_LAMBDA_END
                    , X_STATIC_LAMBDA_BEGIN (edentity& A) -> xproperty_v2::base*
                    {
                        return A.m_GameGraphDoc.getGraph().findEntity(A.m_gEntity)->getLinearPropInterface();
                    }
                    X_STATIC_LAMBDA_END
                );
            });

            return E;
        }

        guid                                        m_Guid                  { nullptr };
        xstring                                     m_Name                  {};
        bool                                        m_bChanged              { false };
        zone_folder::guid                           m_gZoneFolder           { nullptr };
        gb_component::type_base::guid               m_gEntityType           {}; 
        gb_component::entity::guid                  m_gEntity               { nullptr };
        xvector<xproperty_v2::entry>                m_lExtraProperties      {};
        gbed_game_graph::document&                  m_GameGraphDoc;
    };

    struct layer_folder
    {
        using guid = xguid<layer_folder>;

        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );

        xstring                                     m_Name                  {};
        bool                                        m_bChanged              { false };
        bool                                        m_bExpanded             { false };
        guid                                        m_Guid                  {};
        guid                                        m_gParent               { nullptr };        // If this is null it must be the a layer
        xvector<zone_folder::guid>                  m_lgZoneFolder          {};                 // Zones
        xvector<guid>                               m_lgFolders             {};                 // Layer Folders
    };

    struct scene
    {
        using guid = xguid<scene>;

        struct entry
        {
            layer_folder::guid                      m_gLayer                { nullptr };
            bool                                    m_bLoaded               { true };
        };

        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );

        guid                                        m_Guid                  { nullptr };
        xstring                                     m_Name                  { X_STR("No Scene Loaded") };
        xvector<entry>                              m_lLayers               {};
        bool                                        m_bChanged              { false };
    };

public:

    x_incppfile                                     document            ( const xstring::const_str Str, gbed_document::main& MainDoc );
    virtual                 const type&             getType             ( void ) override;
 
    x_incppfile    std::pair<err, scene&>           CreateNewScene      ( void );
    x_incppfile             layer_folder&           CreateFLayer        ( layer_folder::guid Guid = layer_folder::guid{layer_folder::guid::RESET} );
    x_incppfile             zone_folder&            CreateFZone         (   layer_folder::guid          gParentLayerOrFolder = layer_folder::guid{nullptr}
                                                                            , edzone::guid              gZone                = edzone::guid{edzone::guid::RESET}
                                                                            , zone_folder::guid         Guid                 = zone_folder::guid{zone_folder::guid::RESET}
                                                                        );
    x_incppfile             zone_folder&            CreateFZone         (   zone_folder::guid           gParentZoneOrFolder  = zone_folder::guid{nullptr}
                                                                            , edzone::guid              gZone                = edzone::guid{edzone::guid::RESET}
                                                                            , zone_folder::guid         Guid                 = zone_folder::guid{zone_folder::guid::RESET}
                                                                        );
    x_incppfile             layer_folder&           CreateFLayerFolder  ( layer_folder::guid gParentLayerOrFolder, layer_folder::guid Guid = layer_folder::guid{layer_folder::guid::RESET} );
    x_incppfile             zone_folder&            CreateFZoneFolder   ( zone_folder::guid  gParentZoneOrFolder, zone_folder::guid Guid = zone_folder::guid{zone_folder::guid::RESET} );

    x_incppfile             edentity&               CreateEDEntity      ( gb_component::type_base::guid gEntityType, gb_component::entity::guid InstanceGuid, zone_folder::guid gParentZoneOrFolder );
    
    x_incppfile std::tuple< err, edentity* >        CreateEDEntityFromBLueprint( zone_folder::guid gParentZoneOrFolder, gb_blueprint::guid gBlueprint );

    x_incppfile             edzone&                 CreateZone          ( edzone::guid gEDZone = edzone::guid{edzone::guid::RESET} );
    x_incppfile             void                    RefreshEDZones      ( void );
    x_incppfile             bool                    DoesFLayerUsedEDZone( layer_folder::guid gLayer, edzone::guid Guid ) const;

    x_inline                void                    setActiveFZone      ( zone_folder::guid gLayer ) { m_gActiveFZone = gLayer; }
    x_inline                auto                    getActiveFZone      ( void ) const { return m_gActiveFZone;      }
    x_inline                auto&                   getFLayerDBase      ( void ) const { return m_FLayerDBase;      }
    x_inline                auto&                   getFLayerDBase      ( void )       { return m_FLayerDBase;      }
    x_inline                auto&                   getFZoneDBase       ( void ) const { return m_FZoneDBase;       }
    x_inline                auto&                   getFZoneDBase       ( void )       { return m_FZoneDBase;       }
    x_inline                auto&                   getScene            ( void ) const { return m_Scene;            }
    x_inline                auto&                   getScene            ( void )       { return m_Scene;            }
    x_inline                auto&                   getGameGraphDoc     ( void )       { return m_GameGraphDoc;     }
    x_inline                auto&                   getEDZoneDBase      ( void ) const { return m_EDZoneDBase;      }
    x_inline                auto&                   getEDZoneDBase      ( void )       { return m_EDZoneDBase;      }
    x_inline                auto&                   getEDEntityDBase    ( void ) const { return m_EDEntityDBase;    }
    x_inline                auto&                   getEDEntityDBase    ( void )       { return m_EDEntityDBase;    }
    x_inline                auto&                   getScenePath        ( void ) const { return m_ScenePath;        }
    x_incppfile             err                     LoadScene           ( xstring ScenePath  );
    x_inline                auto&                   getMainDoc          ( void ) const { return gbed_document::base::m_MainDoc; }

    x_incppfile             edentity*               findEDEntry         ( gb_component::entity::guid );
    x_incppfile             edentity::guid          findEDEntryGuid     ( gb_component::entity::guid Guid ) const;
    x_incppfile             gb_component::entity&   CreateEntity        ( gb_component::type_base& EntityType, gb_component::entity::guid InstanceGuid, zone_folder::guid gZone = nullptr ) noexcept;
    x_incppfile             void                    DestroyEntity       ( gb_component::entity::guid );
 
    inline                  err                     New                 ( void ) { if( auto Error = onClose(); Error.isError() == false ) return onNew(); else return Error;  }
    inline                  err                     Save                ( void ) { return onSave(); }
    inline                  err                     Load                ( void ) { if( auto Error = onClose(); Error.isError() == false ) return onLoad(); else return Error; }
    inline                  err                     Close               ( void ) { return onClose(); }

protected:

    using hash_entity_to_edentity = x_ll_mrmw_hash<edentity::guid, u64, x_lk_spinlock::do_nothing>;

protected:

    virtual                 err                     onSave              ( void ) override;
    virtual                 err                     onLoad              ( void ) override;
    virtual                 err                     onNew               ( void ) override;
    virtual                 err                     onClose             ( void ) override;
    x_incppfile             void                    StartUp             ( void );
    x_incppfile             void                    msgChangeHappen     ( xproperty_v2::base& Prop );

protected:

    x_ll_dbase<layer_folder,layer_folder::guid>     m_FLayerDBase;
    x_ll_dbase<zone_folder,zone_folder::guid>       m_FZoneDBase;
    x_ll_dbase<edzone,edzone::guid>                 m_EDZoneDBase;
    x_ll_dbase<edentity,edentity::guid>             m_EDEntityDBase;
    hash_entity_to_edentity                         m_Entity2EDEntity; 
    zone_folder::guid                               m_gActiveFZone{ nullptr };
    t_layer_path                                    m_LayerPath;
    t_scene_path                                    m_ScenePath;
    t_edzone_path                                   m_EDZonePath;
    t_edentity_path                                 m_EDEntityPath;
    t_entity_path                                   m_EntityPath;
    gbed_game_graph::document&                      m_GameGraphDoc;
    scene                                           m_Scene;

    x_message::delegate<document,xproperty_v2::base&>                   m_deletegateChangeHappen            { *this, &document::msgChangeHappen };
};       

