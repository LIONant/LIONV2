//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class edzone_dlg 
{
public:

    enum class state : u8
    {
        CLOSE,
        OPEN,
        OK,
        CANCEL
    };

    state Dialog( const char* pStr, gbed_layer::document& Document, document::layer_folder::guid gLayer )
    {
        state State = state::CLOSE;
        ImGui::SetNextWindowSize(ImVec2(400, 200), ImGuiSetCond_FirstUseEver);
        if( ImGui::BeginPopupModal( pStr ) )
        {
            State = Guts( Document, gLayer );
            ImGui::EndPopup();
        }
        return State;
    }

    auto getEDZone( void ) const
    {
        return m_gbEDZone;
    }

protected:

    struct scene_node
    {
        xstring                             m_Name;
        gbed_layer::document::scene::guid   m_Guid;
        xwstring                            m_FileName;
    };

    state Guts( gbed_layer::document& Document, document::layer_folder::guid gLayer )
    {
        state State = state::OPEN;

        //
        // Update buttons
        //
        if( ImGui::Button( "Refresh" ) )
        {
            Document.RefreshEDZones();
            m_gbEDZone.m_Value = 0;
            m_bReset           = false;
        }
        else if( m_bReset )
        {
            m_gbEDZone.m_Value = 0;
            m_bReset = false;
        }

        //
        // List of scenes
        //
        ImGui::Separator();
        ImGui::BeginChild( "Child", ImVec2(0,-20) );
        {
            x_lk_guard_as_const_get_as_const( Document.getEDZoneDBase().m_Vector, ZoneDBase );
            xstring Temp;
            for( auto& pEntry : ZoneDBase )
            {
                auto& Entry = *pEntry->m_pEntry;

                const char* pName = Document.DoesFLayerUsedEDZone( gLayer, Entry.m_Guid )? Temp.setup( "%s <- Already in used by Layer!", Entry.m_Name ) : Entry.m_Name;

                if( ImGui::Selectable( pName, pName == &Entry.m_Name[0] && m_gbEDZone == Entry.m_Guid ) )
                {
                    m_gbEDZone = Entry.m_Guid;
                }
            }
            ImGui::EndChild();
        }

        //
        // Action buttons
        // 
        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            if( m_gbEDZone.m_Value ) State = state::OK;
            else                     State = state::CANCEL; 
            m_bReset = true;
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine(); if( ImGui::Button( "Cancel", ImVec2(120,0) ) )
        {
            State               = state::CANCEL;
            m_bReset            = true;
            ImGui::CloseCurrentPopup();
        }

        return State;
    }

    document::edzone::guid  m_gbEDZone = document::edzone::guid{ nullptr };
    bool                    m_bReset   = false;
};

