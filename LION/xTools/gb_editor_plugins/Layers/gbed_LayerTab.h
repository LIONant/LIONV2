//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class tab : public gbed_plugins::tab
{
public:

    x_incppfile             tab         ( const xstring::const_str& Str, gbed_frame::base& EditorFrame );
    virtual const type&     getType     ( void ) override;
    virtual void            onRender    ( void ) override;

protected:

    // Temporary storage of what node we have clicked to process selection at the end of the loop. May be a pointer to your own node type, etc.
    enum class open_popup : u8
    {
        NONE
        , MAIN_MENU          
        , RENAME_LAYER       
        , NEW_LAYER          
        , NEW_LAYER_FOLDER   
        , NEW_SCENE          
        , NEW_EDZONE 
        , NEW_EDZONE_FROM_ZONE
        , NEW_ZONE_FOLDER
        , NEW_RAW_EDENTITY
        , NEW_BLUEPRINT_EDENTITY
        , DELETE_EDENTITY
        , RENAME_ZONE_FOLDER 
        , RENAME_SCENE 
        , ARE_YOU_SURE
        , ERROR_DLG
        , LOAD_SCENE
    };

    struct popup_params
    {
        scene_dlg                                   m_SceneDlg;
        edzone_dlg                                  m_EDZoneDlg;
        xarray<char,128>                            m_EditField;
        gbed_layer::document::layer_folder::guid    m_LayerGuidField;
        gbed_layer::document::zone_folder::guid     m_ZoneGuidField;
        gbed_layer::document::edentity::guid        m_EDEntityGuid;
        gb_component::entity::guid                  m_EntityGuid;
        std::function<open_popup(bool bOK)>         m_CallBack;
    };

protected:

    gbed_layer::document&                       m_Document;
    gbed_selection::document&                   m_SelectionDoc; 
    ImGuiTextFilter                             m_Filter                    {};
    bool                                        m_bShowNoneditorEntities    { false };
    bool                                        m_bSceneExpanded            { true };
    popup_params                                m_Params                    {};
    gbed_blueprint::dlg                         m_BlueprintDlg;
};

