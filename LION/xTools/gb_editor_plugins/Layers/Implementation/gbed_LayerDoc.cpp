//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------
static const gbed_document::base::type_harness<document> s_TypeDoc{ document::t_class_string };    
    
//-------------------------------------------------------------------------------------------

const gbed_document::base::type& document::getType( void ) 
{ 
    return s_TypeDoc;  
}

//-------------------------------------------------------------------------------------------

void document::layer_folder::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "LayerHeader" );
    File.WriteField ( "Name:s", m_Name );
    File.WriteField ( "Guid:g", m_Guid );
    File.WriteField ( "gParent:g", m_gParent );
    File.WriteLine();

    File.WriteRecord( "Folders", m_lgFolders.getCount<int>() );
    for( auto& E : m_lgFolders )
    {
        File.WriteField( "gFolders:g", E );
        File.WriteLine();
    }

    File.WriteRecord( "ZoneFolders", m_lgZoneFolder.getCount<int>() );
    for( auto& E : m_lgZoneFolder )
    {
        File.WriteField( "gZoneFolder:g", E );
        File.WriteLine();
    }
}

//-------------------------------------------------------------------------------------------

void document::layer_folder::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("LayerHeader") )
        return;

    File.ReadLine();
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadField          ( "Guid:g", &m_Guid );
    File.ReadField          ( "gParent:g", &m_gParent );
    if( File.ReadRecord() == false ) return;

    //
    // Read folders
    //
    if( File.getRecordName() == X_STR("Folders" ) )
    {
        m_lgFolders.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgFolders.append();
                File.ReadField( "gFolders:g", &E );
            } 
        }

        if( File.ReadRecord() == false ) return;
    }
        
    //
    // Read entities
    //
    if( File.getRecordName() == X_STR("ZoneFolders" ) )
    {
        m_lgZoneFolder.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgZoneFolder.append();
                File.ReadField( "gZoneFolder:g", &E );
            } 
        }

        if( File.ReadRecord() == false ) return;
    }
}

//-------------------------------------------------------------------------------------------

void document::edzone::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "EDZoneHeader" );
    File.WriteField ( "Name:s", m_Name );
    File.WriteField ( "Guid:g", m_Guid );
    File.WriteLine();
}

//-------------------------------------------------------------------------------------------

void document::edzone::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("EDZoneHeader") )
        return;

    File.ReadLine();
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadField          ( "Guid:g", &m_Guid );
}

//-------------------------------------------------------------------------------------------

void document::zone_folder::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "ZoneHeader" );
    File.WriteField ( "Name:s", m_Name );
    File.WriteField ( "Guid:g", m_Guid );
    File.WriteField ( "gParent:g", m_gParent );
    File.WriteField ( "ZoneGuid:g", m_ZoneGuid );
    File.WriteLine();

    File.WriteRecord( "Folders", m_lgFolders.getCount<int>() );
    for( auto& E : m_lgFolders )
    {
        File.WriteField( "gFolders:g", E );
        File.WriteLine();
    }

    File.WriteRecord( "EDEntities", m_lgEDEntity.getCount<int>() );
    for( auto& E : m_lgEDEntity )
    {
        File.WriteField( "gEDEntity:g", E );
        File.WriteLine();
    }
}

//-------------------------------------------------------------------------------------------

void document::zone_folder::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("ZoneHeader") )
        return;

    File.ReadLine();
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadField          ( "Guid:g", &m_Guid );
    File.ReadField          ( "gParent:g", &m_gParent );
    File.ReadField          ( "ZoneGuid:g", &m_ZoneGuid );
    if( File.ReadRecord() == false ) return;

    //
    // Read folders
    //
    if( File.getRecordName() == X_STR("Folders" ) )
    {
        m_lgFolders.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgFolders.append();
                File.ReadField( "gFolders:g", &E );
            } 
        }

        if( File.ReadRecord() == false ) return;
    }
        
    //
    // Read entities
    //
    if( File.getRecordName() == X_STR("EDEntities" ) )
    {
        m_lgEDEntity.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgEDEntity.append();
                File.ReadField( "gEDEntity:g", &E );
            } 
        }

        if( File.ReadRecord() == false ) return;
    }
}

//-------------------------------------------------------------------------------------------

void document::edentity::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "EntityHeader" );
    File.WriteField ( "Guid:g",             m_Guid );
    File.WriteField ( "Name:s",             m_Name );
    File.WriteField ( "EntityGuid:g",       m_gEntity );
    File.WriteField ( "gEntityType:g",      m_gEntityType ); 
    File.WriteField ( "FZoneGuid:g",        m_gZoneFolder ); 
    File.WriteLine();

    //
    // Save properties
    //
    File.WriteRecord( "Properties", m_lExtraProperties.getCount<int>() );
    for( const auto& Entry : m_lExtraProperties )
    {
        Entry.SerializeOut(File);
    }
}

//-------------------------------------------------------------------------------------------

void document::edentity::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("EntityHeader") )
        return;

    File.ReadLine();
    File.ReadField          ( "Guid:g",         &m_Guid );
    File.ReadFieldXString   ( "Name:s",         m_Name );
    File.ReadField          ( "EntityGuid:g",   &m_gEntity ); 
    File.ReadField          ( "gEntityType:g",  &m_gEntityType ); 
    File.ReadField          ( "FZoneGuid:g",    &m_gZoneFolder ); 

    //
    // Load properties
    //
    m_lExtraProperties.DeleteAllEntries();
    if( File.getRecordName() == X_STR("Properties") )
    {
        const int Count = File.getRecordCount();
        for( int i=0; i<Count; ++i )
        {
            File.ReadLine();
            m_lExtraProperties.append().SerializeIn(File);
        }
    }
}

//-------------------------------------------------------------------------------------------
    
void document::scene::Save( xtextfile& File )
{
    File.WriteRecord( "Scene" );
    File.WriteField( "Guid:g", m_Guid );
    File.WriteField( "Name:s", (const char*)m_Name );
    File.WriteLine();

    File.WriteRecord( "Layers", m_lLayers.getCount<int>() );
    for( auto& EntryLayer : m_lLayers )
    {
        File.WriteField( "GUID:g",      EntryLayer.m_gLayer );
        File.WriteField( "bLoaded:h",   EntryLayer.m_bLoaded );
        File.WriteLine();
    }
}

//-------------------------------------------------------------------------------------------
    
void document::scene::Load( xtextfile& File )
{
    if( File.getRecordName() != X_STR("Scene") )
        return;

    File.ReadLine();
    File.ReadField          ( "Guid:g", &m_Guid );
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadRecord();

    //
    // Load layers
    //
    if( File.getRecordName() == X_STR("Layers") )
    {
        const int Count = File.getRecordCount();
        if( Count )
        {
            m_lLayers.DeleteAllEntries();
            for( int i = 0; i<Count; ++i )
            {
                File.ReadLine();
    
                auto& EntryLayer = m_lLayers.append();
                File.ReadField( "GUID:g",      &EntryLayer.m_gLayer );
                File.ReadField( "bLoaded:h",   &EntryLayer.m_bLoaded );
            }
        }
    }
}

//-------------------------------------------------------------------------------------------

document::document( const xstring::const_str Str, gbed_document::main& MainDoc ) 
    : gbed_document::base   { Str, MainDoc }
    , m_GameGraphDoc        { MainDoc.getSubDocument<gbed_game_graph::document>() } 
{
    m_FLayerDBase.Init( 1024 );
    m_FZoneDBase.Init( 1024 );
    m_EDZoneDBase.Init( 1024 );
    m_EDEntityDBase.Init( 1024*1024 );
    m_Entity2EDEntity.Initialize( 300000 );

    m_deletegateChangeHappen.Connect( getMainDoc().m_Events.m_ChangeHappen );
}

//-------------------------------------------------------------------------------------------

void document::StartUp( void )
{
    auto JITPath            = m_GameGraphDoc.getJITPath();
    m_LayerPath             = JITPath + std_fs::path("/Layers");
    m_ScenePath             = JITPath + std_fs::path("/Scenes");
    m_EDZonePath            = JITPath + std_fs::path("/EDZones");
    m_EDEntityPath          = JITPath + std_fs::path("/EDEntities");
    m_EntityPath            = m_GameGraphDoc.getGameDataPath() + std_fs::path("/Entities");
}

//-------------------------------------------------------------------------------------------

document::err document::onNew( void ) 
{
    // Make sure that we are doing the startup first
    StartUp();

    // Create the actual directory
    if (auto Error = m_MainDoc.CreateDir(m_ScenePath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_LayerPath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_EDZonePath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_EDEntityPath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_EntityPath); Error.isError()) 
        return Error;

    //
    // Create the default scene, layer, and zone
    //
    auto Res = CreateNewScene();
    if( Res.first.isError() ) return Res.first;

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

std::pair<document::err, document::scene&> document::CreateNewScene( void )
{
    // Close current scene
    onClose().IgnoreError();

    m_Scene.m_Name = X_STR("New Scene");
    m_Scene.m_bChanged = true;
    m_Scene.m_Guid.Reset();

    // Create the global/default zone
    auto& EDZone = CreateZone( edzone::guid{nullptr} );
    EDZone.m_Name = X_STR("Default");

    auto& FLayer = CreateFLayer();
    FLayer.m_Name = X_STR("First Layer");

    // Set the default zone active
    auto& FZone    = m_FZoneDBase.getEntry( FLayer.m_lgZoneFolder[0] );
    m_gActiveFZone = FZone.m_Guid;
    FZone.m_Name   = X_STR("Default");

    // Flush all the defaults to disk
    if( auto Err = onSave(); Err.isError() ) return { Err, m_Scene };

    return { x_error_ok(), m_Scene };
}

//-------------------------------------------------------------------------------------------

document::err document::onLoad( void )
{
    // Make sure that we are doing the startup first
    StartUp();

    //...
    xstring     ScenePathFile;

    //
    // Loading the last opened scene
    //
    {
        auto        Settings            = m_GameGraphDoc.getSettingsPath();
        xstring     Path;
        xstring     FilePath;
        xtextfile   File;

        // Convert to the path first
        Path.CopyAndConvert( &static_cast<std::wstring>(Settings)[0] );

        FilePath.setup( "%s/LastLoadedScene.txt", Path );
        if( auto Err = File.openForReading( FilePath.To<xwchar>() ); Err.isOK() )
        {
            if( false == File.ReadRecord() || File.getRecordName() != X_STR("Scene") )
            {
                X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error loading settings.last_open_scene trying to find record, %s", Err.getString() );
                return x_error_code( errors, ERR_FAILURE, "Fail to load settings.last_open_scene trying to find record" );
            }

            File.ReadLine();

            xstring     ScenePath;
            xstring     FileName;
            scene::guid Guid;
            File.ReadField( "GUID:g", &Guid );

            ScenePath.CopyAndConvert( &static_cast<std::wstring>(m_ScenePath)[0] );

            // Create file name 
            Guid.getStringHex( FileName );
            FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
            // Create finally the full filename path
            ScenePathFile.setup( "%s/%s.scene.txt", ScenePath, FileName );
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error load settings.last_open_scene, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to load settings.last_open_scene" );
        }
    }

    //
    // Do the actual loading
    //
    if( auto Error = LoadScene( ScenePathFile ); Error.isError() ) return Error;

    //
    // Set the default zone active
    //
    {
        x_lk_guard_as_const_get_as_const( m_FZoneDBase.m_Vector, ZoneDBase );
        m_gActiveFZone = ZoneDBase[0]->m_pEntry->getGuid();
    }

    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::LoadScene( xstring ScenePath )
{
    //
    // Close current scene        
    //
    if( auto Error = onClose(); Error.isError() ) return Error;

    //
    // Read the scene
    //
    {
        xtextfile   File;

        if( auto Err = File.openForReading( ScenePath.To<xwchar>() ); Err.isOK() )
        {
            File.ReadRecord();
            m_Scene.Load(File);
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error loading scene(%s), %s", static_cast<const char*>(ScenePath), Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to load scene" );
        }
    }

    //
    // Read Layers & FZones & EDEntities
    //
    {
        xstring     RootLayerPath;
        // xstring     RootScenePath;

        // Convert to the path first
        // RootScenePath.CopyAndConvert( &static_cast<std::wstring>(m_ScenePath)[0] );
        RootLayerPath.CopyAndConvert( &static_cast<std::wstring>(m_LayerPath)[0] );
        
        for( const auto LayerEntry : m_Scene.m_lLayers )
        {
            xtextfile   File;
            xstring     FileName;
            xstring     LayerFilePath;
            
            // Create file name 
            LayerEntry.m_gLayer.getStringHex( FileName );
            FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
            // Create finally the full filename path
            LayerFilePath.setup( "%s/%s.layer", RootLayerPath, FileName );

            // Read Layer's Root
            xstring     RootPath;
                    
            // Create the root file for this layer
            RootPath.setup( "%s/Root.flayer.txt", LayerFilePath );

            if( LayerEntry.m_bLoaded )
            {
                //
                // Load Game Entity
                //
                const auto LoadEntity = [&]( edentity& EDEntity ) -> document::err
                {
                    xtextfile   File;
                    xstring     FileName;
                    xwstring    FileRoot;

                    // Create file name 
                    EDEntity.m_gEntity.getStringHex( FileName );
                    FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
                    
                    // Create the root file for this layer
                    FileRoot.Copy( &static_cast<std::wstring>(m_EntityPath)[0] );
                    FileRoot.append( X_WSTR("/").m_pValue );
                    FileRoot.append( FileName.To<xwchar>() );
                    FileRoot.append( X_WSTR(".e").m_pValue );

                    if( auto Err = File.openForReading( FileRoot ); Err.isOK() )
                    {
                        if( File.ReadRecord() )
                        {
                            auto&   Graph   = m_GameGraphDoc.getGraph();
                            auto&   Entity  = Graph.LoadEntity( File );
                        }
                        else
                        {
                            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                        }
                    }
                    else
                    {
                        X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                        return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                    }

                    return x_error_ok();
                };

                //
                // Load zone children
                //
                const xfunction<document::err(zone_folder&)> LoadZoneChildren = [&]( zone_folder& ZoneParent ) -> document::err
                {
                    //
                    // Load the zones too
                    //
                    for( const auto gFolder : ZoneParent.m_lgFolders )
                    {
                        xtextfile   File;
                        xstring     FileName;
                        xstring     RootPath;

                        // Create file name 
                        gFolder.getStringHex( FileName );
                        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
                    
                        // Create the root file for this layer
                        RootPath.setup( "%s/%s.fzone.txt", LayerFilePath, FileName );
                        if( auto Err = File.openForReading( RootPath.To<xwchar>() ); Err.isOK() )
                        {
                            xndptr_s<zone_folder> xpFZone;
                            xpFZone.New();
                            if( File.ReadRecord() )
                            {
                                xpFZone->Load(File);
                                if( auto Err = LoadZoneChildren( *xpFZone ); Err.isError() ) return Err;
                                m_FZoneDBase.RegisterAndTransferOwner(xpFZone);
                            }
                        }
                        else
                        {
                            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                        }

                        return x_error_ok();
                    }

                    //
                    // Load the editor entities too
                    //
                    for( const auto gEDEntity : ZoneParent.m_lgEDEntity )
                    {
                        xtextfile   File;
                        xstring     FileName;
                        xwstring    FileRoot;

                        // Create file name 
                        gEDEntity.getStringHex( FileName );
                        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
                    
                        // Create the root file for this layer
                        FileRoot.Copy( &static_cast<std::wstring>(m_EDEntityPath)[0] );
                        FileRoot.append( X_WSTR("/").m_pValue );
                        FileRoot.append( FileName.To<xwchar>() );
                        FileRoot.append( X_WSTR(".edentity.txt").m_pValue );

                        if( auto Err = File.openForReading( FileRoot ); Err.isOK() )
                        {
                            xndptr_s<edentity> xpEDEntity;
                            xpEDEntity.New( m_GameGraphDoc );

                            if( File.ReadRecord() )
                            {
                                xpEDEntity->Load(File);
                                if( auto Error = LoadEntity( *xpEDEntity ); Error.isError() ) return Error;

                                // Add entity in the reverse lookup table
                                m_Entity2EDEntity.cpAddEntry( xpEDEntity->m_gEntity.m_Value
                                , [&]( edentity::guid& Entry )
                                {
                                    Entry = xpEDEntity->m_Guid;
                                });

                                // Transfer entity to dbase
                                m_EDEntityDBase.RegisterAndTransferOwner(xpEDEntity);
                            }
                        }
                        else
                        {
                            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                        }
                    }

                    return x_error_ok();
                };

                //
                // Load Layer Children
                //
                const xfunction<document::err(layer_folder&)> LoadLayerChildren = [&]( layer_folder& LayerParent ) -> document::err
                {
                    //
                    // Load layer children
                    //
                    for( const auto gFolder : LayerParent.m_lgFolders )
                    {
                        xtextfile   File;
                        xstring     FileName;
                        xstring     RootPath;

                        // Create file name 
                        gFolder.getStringHex( FileName );
                        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
                    
                        // Create the root file for this layer
                        RootPath.setup( "%s/%s.flayer.txt", LayerFilePath, FileName );
                        if( auto Err = File.openForReading( RootPath.To<xwchar>() ); Err.isOK() )
                        {
                            xndptr_s<layer_folder> xpLayer;
                            xpLayer.New();
                            if( File.ReadRecord() )
                            {
                                xpLayer->Load(File);
                                LoadLayerChildren( *xpLayer );
                                m_FLayerDBase.RegisterAndTransferOwner(xpLayer);
                            }
                        }
                        else
                        {
                            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                        }
                    }

                    //
                    // Load the zones too
                    //
                    for( const auto gFolder : LayerParent.m_lgZoneFolder )
                    {
                        xtextfile   File;
                        xstring     FileName;
                        xstring     RootPath;

                        // Create file name 
                        gFolder.getStringHex( FileName );
                        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
                    
                        // Create the root file for this layer
                        RootPath.setup( "%s/%s.fzone.txt", LayerFilePath, FileName );
                        if( auto Err = File.openForReading( RootPath.To<xwchar>() ); Err.isOK() )
                        {
                            xndptr_s<zone_folder> xpFZone;
                            xpFZone.New();
                            if( File.ReadRecord() )
                            {
                                xpFZone->Load(File);
                                if( auto Err = LoadZoneChildren( *xpFZone ); Err.isError() ) return Err;
                                m_FZoneDBase.RegisterAndTransferOwner(xpFZone);
                            }
                        }
                        else
                        {
                            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                        }
                    }

                    return x_error_ok();
                };

                //
                // Load Layer
                //
                if( auto Err = File.openForReading( RootPath.To<xwchar>() ); Err.isOK() )
                {
                    xndptr_s<layer_folder> xpLayer;
                    xpLayer.New();
                    if( File.ReadRecord() )
                    {
                        xpLayer->Load(File);
                        if( auto Err = LoadLayerChildren( *xpLayer ); Err.isError() ) return Err;
                        m_FLayerDBase.RegisterAndTransferOwner(xpLayer);
                    }
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                }
            }
        }
    }

    //
    // Read all the zones
    //
    RefreshEDZones();

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

void document::RefreshEDZones( void )
{
    m_EDZoneDBase.DeleteAllEntries();
    for( auto & p : std_fs::directory_iterator(m_EDZonePath) )
    {
        xtextfile File;
        xwstring FileName;
                
        FileName.Copy( &std::wstring( p.path() )[0] );

        if( auto Err = File.openForReading( FileName ); Err.isOK() )
        {
            xndptr_s<edzone> xpEDZone;
            xpEDZone.New();
            if( File.ReadRecord() )
            {
                xpEDZone->Load(File);
                m_EDZoneDBase.RegisterAndTransferOwner(xpEDZone);
            }            
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error fail to load one of the Zones descriptions (%s) so it will be missing in the list, %s"
                    , static_cast<const xchar*>( FileName.To<xchar>() )
                    , Err.getString() );
        }
    }
}

//-------------------------------------------------------------------------------------------

document::err document::onClose( void )
{
    // clear all the data
    m_FLayerDBase.DeleteAllEntries();
    m_FZoneDBase.DeleteAllEntries();
    m_EDZoneDBase.DeleteAllEntries();
    m_EDEntityDBase.DeleteAllEntries();
    m_Entity2EDEntity.DeleteAllEntries();
    m_gActiveFZone.m_Value = 0;
    m_Scene.m_lLayers.DeleteAllEntries();

    m_Scene.m_Guid.m_Value = 0;

    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::onSave( void ) 
{
    //
    // Saving the last opened scene
    //
    {
        auto        Settings            = m_GameGraphDoc.getSettingsPath();
        xstring     Path;
        xstring     FilePath;
        xtextfile   File;

        // Convert to the path first
        Path.CopyAndConvert( &static_cast<std::wstring>(Settings)[0] );

        FilePath.setup( "%s/LastLoadedScene.txt", Path );
        if( auto Err = File.openForWriting( FilePath.To<xwchar>() ); Err.isOK() )
        {
            File.WriteRecord( "Scene" );
            File.WriteField( "GUID:g", m_Scene.m_Guid );
            File.WriteField( "Name:s", m_Scene.m_Name );
            File.WriteLine();
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving settings.last_open_scene, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save settings.last_open_scene" );
        }
    }

    //
    // Saving Scene
    //
    if( m_Scene.m_bChanged )
    {
        xtextfile   File;
        xstring     Path;
        xstring     FileName;
        xstring     FilePath;

        // Convert to the path first
        Path.CopyAndConvert( &static_cast<std::wstring>(m_ScenePath)[0] );

        // Create file name 
        m_Scene.m_Guid.getStringHex( FileName );
        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
        // Create finally the full filename path
        FilePath.setup( "%s/%s.scene.txt", Path, FileName );

        if( auto Err = File.openForWriting( FilePath.To<xwchar>() ); Err.isOK() )
        {
            m_Scene.Save(File);
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene" );
        }
    }

    //
    // Saving Root layers (real layers)
    //
    {
        x_lk_guard_as_const_get_as_const( m_FLayerDBase.m_Vector, LayerDBase );

        xstring     Path;

        // Convert to the path first
        Path.CopyAndConvert( &static_cast<std::wstring>(m_LayerPath)[0] );

        for( auto& xpNode : LayerDBase )
        {
            auto& Entry = *xpNode->m_pEntry;

            if( Entry.m_gParent.m_Value )      continue;

            xtextfile   File;
            xstring     FileName;
            xstring     FilePath;
            
            // Create file name 
            Entry.m_Guid.getStringHex( FileName );
            FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
            // Create finally the full filename path
            FilePath.setup( "%s/%s.layer", Path, FileName );

            // Create directory if it does not exists
            create_directory( std_fs::path{ std::wstring{ FilePath.To<xwchar>() } });

            //
            // Save Game Entity
            //
            const auto SaveGameEntity = [&]( gb_component::entity& Entry ) -> document::err
            {
                xtextfile   File;
                xstring     FileName;
                xwstring    FileRoot;

                // Create file name 
                Entry.getGuid().getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                // Create finally the full filename path
                FileRoot.Copy( &static_cast<std::wstring>(m_EntityPath)[0] );
                FileRoot.append( X_WSTR("/").m_pValue );
                FileRoot.append( FileName.To<xwchar>() );
                FileRoot.append( X_WSTR(".e").m_pValue );

                if( auto Err = File.openForWriting( FileRoot ); Err.isOK() )
                {
                    xproperty_v2::entry::RegisterTypes( File );
                    auto& GameGraph = getGameGraphDoc().getGraph();
                    GameGraph.SaveEntity( Entry, File );
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.zones, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers.zones" );
                }
                return x_error_ok();
            };

            //
            // Save Editor Entity
            //
            const auto SaveEDEntity = [&]( edentity& Entry ) -> document::err
            {
                //
                // Save the actual zone
                //
                if( Entry.m_bChanged ) 
                {
                    xtextfile   File;
                    xstring     FileName;
                    xwstring    FileRoot;
                    auto&       GameGraph = getGameGraphDoc().getGraph();
                    auto        pEntity   = GameGraph.findEntity<gb_component::entity>(Entry.m_gEntity);

                    // bad situation wasnt able to find an entity
                    if( pEntity == nullptr )
                    {
                        xstring EditorGuidString;
                        xstring GameGuidString;
                        Entry.m_gEntity.getStringHex(GameGuidString);
                        Entry.m_Guid.getStringHex(EditorGuidString);

                        X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "fail to find a game entity:%s from editor entity:%s (%s) since it was not in the world"
                            , static_cast<const char*>(GameGuidString)
                            , static_cast<const char*>(Entry.m_Name)
                            , static_cast<const char*>(EditorGuidString) );

                        return x_error_code( errors, ERR_FAILURE, "Fail to find game entity" );
                    }

                    // check if entity has been compiled
                    if( pEntity->isInWorld() == false ) 
                    {
                        xstring GuidString;
                        Entry.m_gEntity.getStringHex(GuidString);
                        X_LOG_CHANNEL_WARNING( m_MainDoc.m_LogChannel, "did not save entity, %s (%s) since it was not in the world"
                            , static_cast<const char*>(Entry.m_Name)
                            , static_cast<const char*>(GuidString) );

                        return x_error_ok();
                    }

                    // Create file name 
                    Entry.m_Guid.getStringHex( FileName );
                    FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                    // Create finally the full filename path
                    FileRoot.Copy( &static_cast<std::wstring>(m_EDEntityPath)[0] );
                    FileRoot.append( X_WSTR("/").m_pValue );
                    FileRoot.append( FileName.To<xwchar>() );
                    FileRoot.append( X_WSTR(".edentity.txt").m_pValue );

                    if( auto Err = File.openForWriting( FileRoot ); Err.isOK() )
                    {
                        Entry.Save( File );
                        if( auto Error = SaveGameEntity(*pEntity); Error.isError() ) return Error;
                    }
                    else
                    {
                        X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.zones, %s", Err.getString() );
                        return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers.zones" );
                    }
                }

                Entry.m_bChanged = false;
                return x_error_ok();
            };

            //
            // Save Folder-Zone
            //
            const auto SaveFZone = [&]( zone_folder& E ) -> document::err
            {
                //
                // Save the actual zone
                //
                if( E.m_bChanged ) 
                {
                    xtextfile   File;
                    xstring     FileName;
                    xstring     FileRoot;

                    // Create file name 
                    E.m_Guid.getStringHex( FileName );
                    FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                    // Create finally the full filename path
                    FileRoot.setup( "%s/%s.fzone.txt", FilePath, FileName );

                    if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
                    {
                        E.Save( File );
                    }
                    else
                    {
                        X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.zones, %s", Err.getString() );
                        return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers.zones" );
                    }
                }

                E.m_bChanged = false;

                //
                // Save Zone Entities
                //
                for( const auto& gEDEntity : E.m_lgEDEntity )
                {
                    auto& EDEntity = m_EDEntityDBase.getEntry( gEDEntity );
                    if( auto Error = SaveEDEntity(EDEntity); Error.isError() ) return Error;
                }

                return x_error_ok(); 
            };

            //
            // Save Zone Children
            //
            const xfunction<document::err(zone_folder& Entry)> SaveZoneChildren = [&]( zone_folder& Entry ) -> document::err
            {
                for( const auto& gEntry : Entry.m_lgFolders )
                {
                    auto& E = m_FZoneDBase.getEntry( gEntry );

                    // Save the zone
                    if( auto Err = SaveFZone(E); Err.isError() ) return Err;

                    // Recurse for children
                    if( auto Err = SaveZoneChildren( E ); Err.isError() ) return Err;
                }

                return x_error_ok(); 
            };

            //
            // Save Layer Children
            //
            const xfunction<document::err(layer_folder& Entry)> SaveLayerChildren = [&]( layer_folder& Entry ) -> document::err
            {
                //
                // Save all the folders
                //
                for( const auto& gEntry : Entry.m_lgFolders )
                {
                    auto& E = m_FLayerDBase.getEntry( gEntry );
                    if( E.m_bChanged ) 
                    {
                        xtextfile   File;
                        xstring     FileName;
                        xstring     FileRoot;

                        // Create file name 
                        E.m_Guid.getStringHex( FileName );
                        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                        // Create finally the full filename path
                        FileRoot.setup( "%s/%s.flayer.txt", FilePath, FileName );

                        if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
                        {
                            E.Save( File );
                        }
                        else
                        {
                            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                        }
                    }

                    // Recurse the rest of the children
                    if( auto Err = SaveLayerChildren( E ); Err.isError() ) return Err;
                    E.m_bChanged = false;
                }

                //
                // Save all the FZones
                //
                for( const auto& gEntry : Entry.m_lgZoneFolder )
                {
                    auto& E = m_FZoneDBase.getEntry( gEntry );

                    // Save the zone
                    if( auto Err = SaveFZone(E); Err.isError() ) return Err;

                    // Save zone children
                    if( auto Err = SaveZoneChildren( E ); Err.isError() ) return Err;
                    E.m_bChanged = false;
                }

                return x_error_ok(); 
            };

            //
            // Save the layer
            //
            if( Entry.m_bChanged )
            {
                xstring     RootPath;
                    
                // Create the root file for this layer
                RootPath.setup( "%s/Root.flayer.txt", FilePath );

                if( auto Err = File.openForWriting( RootPath.To<xwchar>() ); Err.isOK() )
                {
                    Entry.Save( File );
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                }
            }

            //
            // Save all the Children from the layer
            //
            if( auto Err = SaveLayerChildren( Entry ); Err.isError() ) return Err;
            Entry.m_bChanged = false;
        }
    }

    //
    // Saving Zones
    //
    {
        x_lk_guard_as_const_get_as_const( m_EDZoneDBase.m_Vector, ZoneDBase );

        xstring     Path;

        // Convert to the path first
        Path.CopyAndConvert( &static_cast<std::wstring>(m_EDZonePath)[0] );

        for( auto& xpNode : ZoneDBase )
        {
            auto& Entry = *xpNode->m_pEntry;

            if( Entry.m_bChanged )
            {
                xtextfile   File;
                xstring     FileName;
                xstring     FilePath;
            
                // Create file name 
                Entry.m_Guid.getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                // Create finally the full filename path
                FilePath.setup( "%s/%s.zone.txt", Path, FileName );

                if( auto Err = File.openForWriting( FilePath.To<xwchar>() ); Err.isOK() )
                {
                    Entry.Save( File );
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.zones, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.zones" );
                }
            }

            // Make sure to indicate that we dont need to save this any more
            Entry.m_bChanged = false;
        }
    }

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

bool document::DoesFLayerUsedEDZone( layer_folder::guid gLayer, edzone::guid gEDZone ) const
{
    const auto gZone = m_EDZoneDBase.getEntry(gEDZone);

    auto FindParent = [&](layer_folder::guid gLayer)
    {
        auto* pEntry = &m_FLayerDBase.getEntry(gLayer);
        for( ; pEntry->m_gParent.m_Value; pEntry = &m_FLayerDBase.getEntry(pEntry->m_gParent) );
        return pEntry->m_Guid;
    };

    //
    // get top most parent for this layer
    //
    const auto gParent = FindParent(gLayer);

    //
    // Go trough all the layers and find if the zone exists inside the parent layer
    //
    x_lk_guard_as_const_get_as_const( m_FLayerDBase.m_Vector, LayerDBase );
    for( auto pEntry : LayerDBase )
    {
        auto& FLayerEntry   = *pEntry->m_pEntry;
        auto  gFLayerParent = layer_folder::guid{ nullptr };

        for( auto gFZoneEntry : FLayerEntry.m_lgZoneFolder )
        {
            auto& FZoneEntry = m_FZoneDBase.getEntry( gFZoneEntry );

            if( FZoneEntry.m_ZoneGuid == gEDZone ) 
            {
                if( gFLayerParent.m_Value == 0 ) gFLayerParent = FindParent( FLayerEntry.m_Guid ); 
                if( gFLayerParent == gParent ) return true;
            }
        }
    }

    return false;
}

//-------------------------------------------------------------------------------------------

document::edzone& document::CreateZone( edzone::guid Guid )
{
    xndptr_s<edzone> xpZone;
    xpZone.New();
    xpZone->m_Guid     = Guid;
    xpZone->m_Name     = X_STR( "New Zone" );
    xpZone->m_bChanged = true;

    // Transfer ownership to the database
    auto* pPtr = xpZone.getPtr();
    m_EDZoneDBase.RegisterAndTransferOwner( xpZone );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------

document::zone_folder& document::CreateFZone
(   
    layer_folder::guid          gParentLayerOrFolder
    , edzone::guid              gZone
    , zone_folder::guid         Guid
)
{
    xndptr_s<zone_folder> xpZone;
    xpZone.New();
    xpZone->m_Guid     = Guid;
    xpZone->m_Name     = X_STR( "New Zone" );
    xpZone->m_bChanged = true;
    xpZone->m_ZoneGuid = gZone;

    // append to parent this case is the layer
    auto& Layer = m_FLayerDBase.getEntry( gParentLayerOrFolder );
    Layer.m_lgZoneFolder.append( xpZone->m_Guid );
    Layer.m_bChanged = true;

    // Transfer ownership to the database
    auto* pPtr = xpZone.getPtr();
    m_FZoneDBase.RegisterAndTransferOwner( xpZone );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------

document::zone_folder& document::CreateFZone
(   
    zone_folder::guid           gParentZoneOrFolder
    , edzone::guid              gZone
    , zone_folder::guid         Guid
)
{
    xndptr_s<zone_folder> xpZone;
    xpZone.New();
    xpZone->m_Guid     = Guid;
    xpZone->m_Name     = X_STR( "New Zone" );
    xpZone->m_bChanged = true;
    xpZone->m_ZoneGuid = gZone;
    xpZone->m_gParent  = gParentZoneOrFolder;

    // append to parent
    if( gParentZoneOrFolder.m_Value )
    {
        auto& ZoneFolder = m_FZoneDBase.getEntry( gParentZoneOrFolder );
        ZoneFolder.m_lgFolders.append( xpZone->m_Guid );
        ZoneFolder.m_bChanged = true;
        if( gZone.m_Value == 0 ) xpZone->m_ZoneGuid = ZoneFolder.m_ZoneGuid;
    }

    // Transfer ownership to the database
    auto* pPtr = xpZone.getPtr();
    m_FZoneDBase.RegisterAndTransferOwner( xpZone );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------
    
document::zone_folder& document::CreateFZoneFolder( zone_folder::guid  gParentZoneOrFolder, zone_folder::guid Guid )
{
    auto& ZoneFolder = CreateFZone( gParentZoneOrFolder, edzone::guid{nullptr}, Guid );
    ZoneFolder.m_Name = X_STR( "New Folder" );
    return ZoneFolder; 
}

//-------------------------------------------------------------------------------------------
    
document::layer_folder& document::CreateFLayerFolder( layer_folder::guid gParentLayerOrFolder, layer_folder::guid Guid )
{
    xndptr_s<layer_folder> xpLayer;
    xpLayer.New();
    xpLayer->m_Guid           = Guid;;
    xpLayer->m_bChanged       = true;
    xpLayer->m_gParent        = gParentLayerOrFolder;
    xpLayer->m_Name           = X_STR( "New Folder" );

    // append to parent
    auto& LayerParent = m_FLayerDBase.getEntry( gParentLayerOrFolder );
    LayerParent.m_lgFolders.append( xpLayer->m_Guid ); 
    LayerParent.m_bChanged = true;

    // Transfer ownership to the database
    auto* pPtr = xpLayer.getPtr();
    m_FLayerDBase.RegisterAndTransferOwner( xpLayer );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------
    
document::layer_folder& document::CreateFLayer( layer_folder::guid Guid )
{
    xndptr_s<layer_folder> xpLayer;
    xpLayer.New();
    xpLayer->m_Guid             = Guid;
    xpLayer->m_Name             = X_STR( "New Layer" );
    xpLayer->m_bChanged         = true;

    // append to parent
    m_Scene.m_bChanged = true;
    auto& LayerEntry = m_Scene.m_lLayers.append();
    LayerEntry.m_bLoaded = true;
    LayerEntry.m_gLayer  = xpLayer->m_Guid;

    // Transfer ownership to the database
    auto* pPtr = xpLayer.getPtr();
    m_FLayerDBase.RegisterAndTransferOwner( xpLayer );

    // Every layer must have the global/default zone
    CreateFZone( LayerEntry.m_gLayer, edzone::guid{ nullptr }, zone_folder::guid{ zone_folder::guid::RESET } );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------

document::edentity::guid document::findEDEntryGuid( gb_component::entity::guid Guid ) const
{
    edentity::guid EdGuid{nullptr};

    m_Entity2EDEntity.cpGetOrFailEntry( Guid.m_Value
    , [&]( const edentity::guid& Entry )
    {
        EdGuid = Entry;
    });

    return EdGuid;
}

//-------------------------------------------------------------------------------------------

document::edentity* document::findEDEntry( gb_component::entity::guid Guid )
{
    const edentity::guid EdGuid = findEDEntryGuid(Guid);
    if( EdGuid.m_Value == 0 ) return nullptr;
    return &m_EDEntityDBase.getEntry( EdGuid );
}

//-------------------------------------------------------------------------------------------
    
document::edentity& document::CreateEDEntity( gb_component::type_base::guid gEntityType, gb_component::entity::guid InstanceGuid, zone_folder::guid gParentZoneOrFolder )
{
    xndptr_s<edentity> xpEDEntity;
    xpEDEntity.New( m_GameGraphDoc );
    xpEDEntity->m_Guid.Reset();
    xpEDEntity->m_Name             = X_STR( "New Entity" );
    xpEDEntity->m_gEntity          = InstanceGuid;
    xpEDEntity->m_gZoneFolder      = gParentZoneOrFolder;
    xpEDEntity->m_gEntityType      = gEntityType;
    xpEDEntity->m_bChanged         = true;
        
    // append to parent
    auto& Parent = m_FZoneDBase.getEntry( gParentZoneOrFolder );
    Parent.m_bChanged = true;
    Parent.m_lgEDEntity.append( xpEDEntity->m_Guid );

    // Add entity in the reverse lookup table
    m_Entity2EDEntity.cpAddEntry( InstanceGuid.m_Value
    , [&]( edentity::guid& Entry )
    {
        Entry = xpEDEntity->m_Guid;
    });

    // Transfer ownership to the database
    auto* pPtr = xpEDEntity.getPtr();
    m_EDEntityDBase.RegisterAndTransferOwner( xpEDEntity );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------
    
gb_component::entity& document::CreateEntity( gb_component::type_base& EntityType, gb_component::entity::guid InstanceGuid, zone_folder::guid gZone ) noexcept
{
    auto& EDEntity = CreateEDEntity( EntityType.getGuid(), InstanceGuid, gZone.m_Value ? gZone : m_gActiveFZone );
    auto& Entity   = m_GameGraphDoc.getGraph().CreateEntity( EntityType, InstanceGuid );
    EDEntity.m_Name         = EntityType.getCategoryName().m_Char;
    return Entity;
}

//-------------------------------------------------------------------------------------------

std::tuple< document::err, document::edentity* > document::CreateEDEntityFromBLueprint( zone_folder::guid gParentZoneOrFolder, gb_blueprint::guid gBlueprint )
{
    auto& BlueprintDoc  = m_MainDoc.getSubDocument<gbed_blueprint::document>();
    auto  pBlueprint    = getGameGraphDoc().getGraph().m_BlueprintDB.Find( gBlueprint );

    if( pBlueprint == nullptr )
    {
        return { x_error_code( errors, ERR_FAILURE, "Fail to find the blueprint" ), nullptr };
    }

    auto  pEDBlueprint  = BlueprintDoc.findEDBlueprint( pBlueprint->getGuid() );

    if( pEDBlueprint == nullptr )
    {
        return { x_error_code( errors, ERR_FAILURE, "Fail to find the edblueprint" ), nullptr };
    }

    gb_component::entity::guid  Guid{ gb_component::entity::guid::RESET };
    xvector<xstring>            Issues;
    auto& Entity        = pBlueprint->CreateEntity<gb_component::entity>( Guid, m_GameGraphDoc.getGraph() );
    if( auto Error = Entity.linearCheckResolve( Issues ); Error.isError() )
    {
        Entity.linearDestroy();
        const auto pErrorMsg = Error.getString();
        X_LOG_CHANNEL_ERROR( getMainDoc().m_LogChannel, "[LAYERDOC] Error creating entity due to errors in the blue print, %s. Please check blueprint for farther information", pErrorMsg );
        Error.IgnoreError();
        return { x_error_code( errors, ERR_FAILURE, pErrorMsg ), nullptr };
    }

    auto& EDEntity  = CreateEDEntity( Entity.getType().getGuid(), Entity.getGuid(), gParentZoneOrFolder );

    // Add entity to the world
    Entity.linearAddToWorld();

    // Add a reference to the blueprint
    pEDBlueprint->AddRef( Entity.getGuid() );

    // Done successfully 
    return { x_error_ok(), &EDEntity };
}

//-------------------------------------------------------------------------------------------

void document::DestroyEntity( gb_component::entity::guid Guid )
{
    auto* pEDEntity = findEDEntry( Guid );
    if( pEDEntity )
    {
        auto& FZone = m_FZoneDBase.getEntry( pEDEntity->m_gZoneFolder );
        
        bool bWasFound = false;
        for( auto& gEntry : FZone.m_lgEDEntity )
        {
            if( gEntry == pEDEntity->getGuid() )
            {
                bWasFound = true;
                FZone.m_lgEDEntity.DeleteWithCollapse( FZone.m_lgEDEntity.getIndexByEntry(gEntry) );
                break;
            }
        }
        x_assert(bWasFound);

        auto* pEntity = m_GameGraphDoc.getGraph().findEntity<gb_component::entity>( Guid );
        x_assert( pEntity );

        {
            xstring strGuid;
            Guid.getStringHex( strGuid );
            getMainDoc().NotifyDelete( 
                xstring::Make( "EDEntity [%s](%s) Will get deleted", static_cast<const char*>(pEDEntity->m_Name), static_cast<const char*>(strGuid) )
                , *pEntity->getLinearPropInterface() );
        }

        // Check if it has a blueprint
        const auto gBlueprint = pEntity->getBlueprintGuid();
        if( gBlueprint.m_Value )
        {
            auto& BlueprintDoc = m_MainDoc.getSubDocument<gbed_blueprint::document>();
            auto  pEDBlueprint = BlueprintDoc.findEDBlueprint( gBlueprint );
            if( pEDBlueprint )
            {
                pEDBlueprint->DeleteRef( pEntity->getGuid() );
            }
        }


        // Delete both structures
        m_EDEntityDBase.Delete( pEDEntity->getGuid() );
        pEntity->linearDestroy();
    }
    else
    {
        auto* pEntity = m_GameGraphDoc.getGraph().findEntity<gb_component::entity>( Guid );
        x_assert( pEntity );
    
        {
            xstring strGuid;
            Guid.getStringHex( strGuid );
            getMainDoc().NotifyDelete( 
                xstring::Make( "Entity (%s) Will get deleted", static_cast<const char*>(strGuid) )
                , *pEntity->getLinearPropInterface() );
        }

        // Delete structures
        pEntity->linearDestroy();
    }
}

//-------------------------------------------------------------------------------------------

void document::msgChangeHappen( xproperty_v2::base& Prop )
{
    if( Prop.isKindOf<edentity>() )
    {
        auto& EDEntity = Prop.SafeCast<edentity>();
        EDEntity.m_bChanged = true;
    }
}

