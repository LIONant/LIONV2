//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

static const gbed_plugins::tab::type_harness<tab> s_TypeTab{ X_STR( "" ICON_FA_IOXHOST " Scene" ) };    
    
//-------------------------------------------------------------------------------------------

const gbed_plugins::tab::type& tab::getType( void ) 
{ 
    return s_TypeTab;  
}

//-------------------------------------------------------------------------------------------

tab::tab( const xstring::const_str& Str, gbed_frame::base& EditorFrame )
    : gbed_plugins::tab     { Str, EditorFrame }
    , m_Document            { EditorFrame.getMainDoc().getSubDocument<gbed_layer::document>() }
    , m_SelectionDoc        { EditorFrame.getMainDoc().getSubDocument<gbed_selection::document>()  }
    , m_BlueprintDlg        { EditorFrame.getMainDoc().getSubDocument<gbed_blueprint::document>() }
{
}

//-------------------------------------------------------------------------------------------

void tab::onRender( void )
{
    open_popup  OpenWhichPopup = open_popup::NONE;

    auto& FLayerDBase       = m_Document.getFLayerDBase();
    auto& FZoneDBase        = m_Document.getFZoneDBase();
    auto& Scene             = m_Document.getScene();
    auto& GameGraph         = m_Document.getGameGraphDoc();
    auto& EDZoneDBase       = m_Document.getEDZoneDBase();
    auto& EDEntityDBase     = m_Document.getEDEntityDBase();
    auto& lSelectedEntity   = m_SelectionDoc.getSelectedEntities();

    //
    // New Layer Popup
    //
    const auto NewScenePopup = [&]()
    {
        if( OpenWhichPopup == open_popup::NEW_SCENE ) 
        {
            m_Params.m_EditField[0] = 0;
            ImGui::OpenPopup( ICON_FA_IOXHOST" New Scene" );
        }

        if( ImGui::BeginPopupModal( ICON_FA_IOXHOST" New Scene" ) )
        {
            ImGui::Text( "Scene Name: " ); 
            ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

            if( ImGui::Button( "OK", ImVec2(120,0) ) )
            {
                auto Res = m_Document.CreateNewScene();
                if( Res.first.isError() )
                {
                    OpenWhichPopup = open_popup::ERROR_DLG;
                    x_strcpy<xchar>( m_Params.m_EditField, Res.first.getString() );
                }
                else
                {
                    Res.second.m_Name.Copy( m_Params.m_EditField );
                    Res.second.m_bChanged = true;
                }
                ImGui::CloseCurrentPopup();
            }

            ImGui::SameLine();
            if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
            {
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }
    };

    //
    // Active Scene/Zone
    //
    {
        ImGui::PushStyleColor(ImGuiCol_ChildWindowBg,    ImColor(40,40,130,128));
        ImGui::BeginChild("scene_region", ImVec2(0,18), true, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar );

        ImGui::Text( " " ICON_FA_IOXHOST" %s", static_cast<const char*>(Scene.m_Name) );

        ImGui::EndChild();
        ImGui::PopStyleColor();

        /*
        ImGui::PushStyleColor(ImGuiCol_ChildWindowBg,    ImColor(40,130,40,128));
        ImGui::BeginChild("activezone_region", ImVec2(0,18), true, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar );
        auto pFZone = FZoneDBase.Find( m_Document.getActiveFZone() );
        if( pFZone )
        {
            ImGui::Text( " " ICON_FA_UNIVERSAL_ACCESS " Active: %s", static_cast<const char*>(pFZone->m_Name) );
        }
        else
        {
            ImGui::Text( " " ICON_FA_UNIVERSAL_ACCESS " Active: ????????" );
        }
        ImGui::EndChild();
        ImGui::PopStyleColor();
        */
    }

    //
    // Top Menu
    //
    if( ImGui::Button( ICON_FA_ARROW_CIRCLE_DOWN ) ) // "Menu"
    {
        OpenWhichPopup = open_popup::MAIN_MENU;
    }
        
    // If we don't have an active scene there is nothing to do
    if( Scene.m_Guid.m_Value == 0 )
    {
        if( OpenWhichPopup == open_popup::MAIN_MENU ) ImGui::OpenPopup( "MainMenuSceneLayer" );

        if( ImGui::BeginPopup( "MainMenuSceneLayer" ) )
        {
            if (ImGui::MenuItem( ICON_FA_IOXHOST "  New Scene ", ""))
            {
                OpenWhichPopup = open_popup::NEW_SCENE;
            }

            if (ImGui::MenuItem( ICON_FA_IOXHOST "  Load Scene ", ""))
            {
                OpenWhichPopup = open_popup::LOAD_SCENE;
            }

            ImGui::EndPopup();
        }

        // Open new scene popup if we have to
        NewScenePopup();

        //
        // Open a load scene
        //
        if( OpenWhichPopup == open_popup::LOAD_SCENE ) 
        {
            ImGui::OpenPopup( " " ICON_FA_IOXHOST" Scenes" );
        }

        {
            scene_dlg::state State = m_Params.m_SceneDlg.Dialog( " " ICON_FA_IOXHOST" Scenes", m_Document );
            if( State == scene_dlg::state::OK )
            {
                m_Document.LoadScene( m_Params.m_SceneDlg.getActivePath().To<xchar>() );
            }
        }

        // Nothing else to do
        return;
    }

    ImGui::SameLine(); if( ImGui::Button( ICON_FA_TIMES ) )
    {
        m_Filter.Clear();
    }
    ImGui::SameLine(); 
    m_Filter.Draw(ICON_FA_SEARCH, -24.0f);

    //
    // EDEntity Context Menu
    //
    const auto EDEntityContextMenu = [&]( gbed_layer::document::edentity& EDEntity )
    {
        ImGui::PushID(&EDEntity);
        if (ImGui::BeginPopupContextItem("Entity Context Menu"))
        {
            if (ImGui::MenuItem( ICON_FA_CHECK_SQUARE_O"  Delete Entity", ""))
            {
                OpenWhichPopup              = open_popup::DELETE_EDENTITY;
                m_Params.m_EntityGuid       = EDEntity.m_gEntity;
            }

            ImGui::EndPopup ();
        }
        ImGui::PopID();
    };

    //
    // Zone Context Menu
    //
    const auto ZoneContextMenu = [&]( gbed_layer::document::zone_folder& FZone )
    {
        ImGui::PushID(&FZone);
        if (ImGui::BeginPopupContextItem("Zone Context Menu"))
        {
            if (ImGui::MenuItem( ICON_FA_UNIVERSAL_ACCESS"  Set Active", ""))
            {
                m_Document.setActiveFZone( FZone.getGuid() );
            }

            if (ImGui::MenuItem( ICON_FA_CHECK_SQUARE_O"  New Raw Entity", ""))
            {
                OpenWhichPopup              = open_popup::NEW_RAW_EDENTITY;
                m_Params.m_ZoneGuidField    = FZone.getGuid();
            }

            if (ImGui::MenuItem( ICON_FA_CHECK_SQUARE_O"  New Entity from Blueprint", ""))
            {
                OpenWhichPopup              = open_popup::NEW_BLUEPRINT_EDENTITY;
                m_Params.m_ZoneGuidField    = FZone.getGuid();
            }

            ImGui::Separator();

            if (ImGui::BeginMenu( FZone.m_gParent.m_Value? ICON_FA_FOLDER_O"  Folders" : ICON_FA_UNIVERSAL_ACCESS"  FZones", ""))
            {
                if( FZone.m_gParent.m_Value )
                {
                    if (ImGui::MenuItem( ICON_FA_FOLDER_O"  Rename Folder ", ""))
                    {
                        OpenWhichPopup = open_popup::RENAME_ZONE_FOLDER;
                        m_Params.m_ZoneGuidField           = FZone.getGuid();
                        x_strcpy<xchar>( m_Params.m_EditField, FZone.m_Name );
                    }
                }
                else
                {
                    
                    if (ImGui::MenuItem( ICON_FA_UNIVERSAL_ACCESS"  Rename FZone ", ""))
                    {
                        OpenWhichPopup = open_popup::RENAME_ZONE_FOLDER;
                        m_Params.m_ZoneGuidField           = FZone.getGuid();

                        auto& EDZone = EDZoneDBase.getEntry( FZone.m_ZoneGuid );
                        x_strcpy<xchar>( m_Params.m_EditField, EDZone.m_Name );
                    }
                }

                if (ImGui::MenuItem( ICON_FA_FOLDER_O"  New Folder ", ""))
                {
                    OpenWhichPopup = open_popup::NEW_ZONE_FOLDER;
                    m_Params.m_ZoneGuidField        = FZone.getGuid();
                }

                if (ImGui::MenuItem( ICON_FA_FOLDER_O"  Move To", ""))
                {
                }

                if( FZone.m_gParent.m_Value )
                {
                    if (ImGui::MenuItem( ICON_FA_FOLDER_O"  Delete Folder", ""))
                    {
                    }
                }
                else
                {
                    if (ImGui::MenuItem( ICON_FA_UNIVERSAL_ACCESS"  Delete FZone", ""))
                    {
                    }
                }


                ImGui::EndMenu();
            }

            ImGui::EndPopup ();
        }
        ImGui::PopID();
    };

    //
    // Utility Functions
    //
    const std::function<void(gbed_layer::document::zone_folder&)> ProcessFZone = [&]( gbed_layer::document::zone_folder& FZone )
    {
        // Is this a real zone of just a folder
        if( FZone.m_gParent.m_Value )
        {
            FZone.m_bExpanded = FZone.m_bExpanded ? ImGui::TreeNode( &FZone, ICON_FA_FOLDER_OPEN_O " %s", FZone.m_Name ) :
                                                    ImGui::TreeNode( &FZone, ICON_FA_FOLDER_O      " %s", FZone.m_Name );
        }
        else
        {
            auto& EDZone = EDZoneDBase.getEntry( FZone.m_ZoneGuid );
            FZone.m_bExpanded = ImGui::TreeNode( &FZone, ICON_FA_UNIVERSAL_ACCESS " %s", EDZone.m_Name ) ;
        }

        ZoneContextMenu(FZone);

        if ( FZone.m_bExpanded )
        {
            //
            // Deal with additional folders
            //
            for( auto gZone : FZone.m_lgFolders )
            {
                auto& Z = FZoneDBase.getEntry( gZone );
                ProcessFZone(Z);
            }

            //
            // Deal with entities
            //
            int ID=0;
            for( auto gEntity : FZone.m_lgEDEntity )
            {
                auto* pEDEntity = EDEntityDBase.Find(gEntity);
                if( pEDEntity )
                {
                    //ImGui::PushID( ID++ );
                    {
                        //pEntity->getType().getComponentDefinition()
                        char    label[256];
                        xstring Str;

                        pEDEntity->m_gEntity.getStringHex( Str );
                        auto* pEntity = m_Document.getGameGraphDoc().getGraph().findEntity(pEDEntity->m_gEntity );

                        if( pEntity )
                        {
                            if( pEntity->isInWorld() )
                            {
                                if( pEDEntity->m_bChanged )
                                {
                                    sprintf_s(label, "" ICON_FA_PENCIL_SQUARE_O "  %s (%s)", static_cast<const char*>(pEDEntity->m_Name), static_cast<const char*>(Str) );
                                }
                                else
                                {
                                    sprintf_s(label, "" ICON_FA_CHECK_SQUARE_O "  %s (%s)", static_cast<const char*>(pEDEntity->m_Name), static_cast<const char*>(Str) );
                                }
                            }
                            else
                            {
                                sprintf_s(label, "" ICON_FA_PENCIL "  %s (%s)", static_cast<const char*>(pEDEntity->m_Name), static_cast<const char*>(Str) );
                            }
                        }
                        else
                        {
                            sprintf_s(label, "" ICON_FA_TIMES"  %s (%s)", static_cast<const char*>(pEDEntity->m_Name), static_cast<const char*>(Str) );
                        }

                        ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
                        for( auto gSel : lSelectedEntity )
                        {
                            if( gSel == pEDEntity->m_gEntity )
                            {
                                flags |= ImGuiTreeNodeFlags_Selected;
                                break;
                            }
                        }
 
                        ImGui::TreeNodeEx( pEDEntity, flags, label );
                        EDEntityContextMenu( *pEDEntity );
                        if (ImGui::IsItemClicked()) 
                        {
                            m_SelectionDoc.Single( pEDEntity->m_gEntity, pEDEntity );
                        }

                        //ImGui::Selectable( label ); 
                    }
                    //ImGui::PopID();
                }
            }

            ImGui::TreePop();
        }
    };

    const auto LayerContextMenu = [&]( gbed_layer::document::layer_folder& Layer, bool bLoaded = true, bool isLayer = false )
    {
        ImGui::PushID(&Layer);
        if (ImGui::BeginPopupContextItem(isLayer?"Layer Context Menu":"Layer Folder Context Menu"))
        {
            if (ImGui::BeginMenu( ICON_FA_SITEMAP"  Layer", ""))
            {
                if (ImGui::MenuItem( isLayer? ICON_FA_SITEMAP"  Rename Layer" : "  Rename Folder", ""))
                {
                    x_strcpy<xchar>( m_Params.m_EditField, Layer.m_Name );
                    OpenWhichPopup = open_popup::RENAME_LAYER;
                    m_Params.m_LayerGuidField     = Layer.m_Guid;
                }

                if (ImGui::MenuItem( ICON_FA_FOLDER_O"  New Folder ", ""))
                {
                    OpenWhichPopup = open_popup::NEW_LAYER_FOLDER;
                    m_Params.m_LayerGuidField        = Layer.m_Guid;
                }

                if( isLayer )
                {
                    if( bLoaded )
                    {
                        if ( ImGui::MenuItem( ICON_FA_SITEMAP"  Unload Layer ", ""))
                        {
                        }
                    }
                    else
                    {
                        if ( ImGui::MenuItem( ICON_FA_SITEMAP"  Load Layer ", ""))
                        {
                        }
                    }
                }

                if (ImGui::MenuItem( ICON_FA_SITEMAP"  Remove From Scene", ""))
                {
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu( ICON_FA_UNIVERSAL_ACCESS"  Zones", ""))
            {
                if (ImGui::MenuItem( "  New Zone ", ""))
                {
                    OpenWhichPopup      = open_popup::NEW_EDZONE;
                    m_Params.m_LayerGuidField    = Layer.getGuid();
                }

                if (ImGui::MenuItem( "  Add Existing Zone ", ""))
                {
                    OpenWhichPopup      = open_popup::NEW_EDZONE_FROM_ZONE;
                    m_Params.m_LayerGuidField    =  Layer.getGuid();
                }

                ImGui::EndMenu();
            }

            ImGui::EndPopup ();
        }
        ImGui::PopID();
    };

    const std::function<void(gbed_layer::document::layer_folder&)> ProcessLayerChildren = [&]( gbed_layer::document::layer_folder& Layer )
    {
        //
        // Deal with layer folders
        //
        for( auto gLayer : Layer.m_lgFolders )
        {
            auto& Layer = FLayerDBase.getEntry( gLayer );

            Layer.m_bExpanded = Layer.m_bExpanded ? ImGui::TreeNode( &Layer, ICON_FA_FOLDER_OPEN_O " %s", Layer.m_Name ) :
                                                    ImGui::TreeNode( &Layer, ICON_FA_FOLDER_O " %s", Layer.m_Name );
            LayerContextMenu(Layer);
            if ( Layer.m_bExpanded )
            {
                ProcessLayerChildren( Layer );
                ImGui::TreePop();
            }
        }

        //
        // Deal with zones
        //
        for( auto gZone : Layer.m_lgZoneFolder )
        {
            auto& Zone = FZoneDBase.getEntry( gZone );
            ProcessFZone(Zone);
        }
    };

    //
    // Render layers
    //
    ImGui::BeginChild("layer view");
    if( Scene.m_Guid.m_Value )
    {
        char    label[256];
        m_bShowNoneditorEntities = ImGui::TreeNode( this, ICON_FA_ANDROID " System Entities" );
        if(m_bShowNoneditorEntities)
        {
            auto& Graph = m_Document.getGameGraphDoc().getGraph();
            int   ID    = 0;

            x_lk_guard_as_const_get_as_const( Graph.m_CompTypeDB.m_Vector, ComponentTypeLinearVector );

            for( const auto pTypeEntry : ComponentTypeLinearVector )
            {
                if( pTypeEntry->m_pEntry->isEntityType() == false ) continue;

                const auto CategoryName = pTypeEntry->m_pEntry->getCategoryName();
                for( const auto pComponent : pTypeEntry->m_pEntry->getInWorld() )
                {
                    const auto& Entity      = pComponent->SafeCast<gb_component::entity>();
                    const auto  gEDEntity   = m_Document.findEDEntryGuid( Entity.getGuid() );
                    if( gEDEntity.m_Value ) continue;

                    //pEntity->getType().getComponentDefinition()
                    xstring Str;

                    Entity.getGuid().getStringHex( Str );
                    sprintf_s(label, "  " ICON_FA_CHECK_SQUARE_O" %s (%s)", CategoryName.m_Char.m_pValue, static_cast<const char*>(Str) );

                    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
                    for( auto gSel : lSelectedEntity )
                    {
                        if( gSel == Entity.getGuid() )
                        {
                            flags |= ImGuiTreeNodeFlags_Selected;
                            break;
                        }
                    }
 
                    ImGui::TreeNodeEx( &Entity, flags, label );
                    if (ImGui::IsItemClicked()) 
                    {
                        m_SelectionDoc.Single( Entity.getGuid(), const_cast<gb_component::entity&>(Entity).getLinearPropInterface() );
                    }
                }
            }
            ImGui::TreePop();
        }

        int ID=0;
        for( auto& SceneLayer : Scene.m_lLayers )
        {
            auto& Layer = FLayerDBase.getEntry( SceneLayer.m_gLayer );

            if( SceneLayer.m_bLoaded )
            {
                Layer.m_bExpanded = ImGui::TreeNode( &Layer, ICON_FA_SITEMAP " %s", Layer.m_Name );
                LayerContextMenu( Layer, true, true );
                if ( Layer.m_bExpanded )
                {
                    ProcessLayerChildren( Layer );
                    ImGui::TreePop();
                }

            }
            else
            {
                ImGui::PushID(ID++);
                {
                    char label[256];
                    sprintf_s(label, ICON_FA_SITEMAP " %s", static_cast<const char*>(Layer.m_Name) );
                    ImGui::Selectable( label ); 
                }
                LayerContextMenu( Layer, false, true );
                ImGui::PopID();
            }
        }
    }
    ImGui::EndChild();

    //
    // Deal with the Menu
    //
    if( OpenWhichPopup == open_popup::MAIN_MENU ) ImGui::OpenPopup( "MainMenuSceneLayer" );

    if( ImGui::BeginPopup( "MainMenuSceneLayer" ) )
    {
        if( ImGui::BeginMenu( ICON_FA_IOXHOST"  Scene" ) )
        {
            if (ImGui::MenuItem( "  Rename Scene ", ""))
            {
                OpenWhichPopup  = open_popup::RENAME_SCENE;
            }

            if (ImGui::MenuItem( "  Save Scene ", ""))
            {
                if( auto Err = m_Document.Save(); Err.isError() )
                {
                    OpenWhichPopup  = open_popup::ERROR_DLG;
                    x_strcpy<xchar>( m_Params.m_EditField, Err.getString() );
                }
            }

            if (ImGui::MenuItem( "  Load New Scene ", ""))
            {
                m_Params.m_CallBack = [=]( bool bOK )
                {
                    if( bOK )
                    {
                        this->m_Document.Save();
                    }

                    return open_popup::LOAD_SCENE;
                };

                OpenWhichPopup  = open_popup::ARE_YOU_SURE;
                x_strcpy<xchar>( m_Params.m_EditField, " Do you Want to Save the current scene? " );
            }

            if (ImGui::MenuItem( "  Close Scene ", ""))
            {
                m_Params.m_CallBack = [=]( bool bOK )
                {
                    if( bOK )
                    {
                        this->m_Document.Save();
                    }

                    this->m_Document.Close();
                    return open_popup::NONE;
                };

                OpenWhichPopup  = open_popup::ARE_YOU_SURE;
                x_strcpy<xchar>( m_Params.m_EditField, " Do you Want to Save the current scene? " );
                    
            }

            if (ImGui::MenuItem( "  New Scene ", ""))
            {
                m_Params.m_CallBack = [=]( bool bOK )
                {
                    if( bOK )
                    {
                        this->m_Document.Save();
                    }

                    return open_popup::NEW_SCENE;
                };

                OpenWhichPopup  = open_popup::ARE_YOU_SURE;
                x_strcpy<xchar>( m_Params.m_EditField, " Do you Want to Save the current scene? " );
            }
            
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu( ICON_FA_SITEMAP"  Layers", ""))
        {
            if (ImGui::MenuItem( "  New Layer ", ""))
            {
                OpenWhichPopup = open_popup::NEW_LAYER;
            }

            if (ImGui::MenuItem( "  Import Layer ", ""))
            {
            }

            ImGui::EndMenu();
        }

        ImGui::EndPopup();
    }

    //
    // Are you sure popup
    //
    if( OpenWhichPopup == open_popup::ARE_YOU_SURE )
    {
        ImGui::OpenPopup( " " ICON_FA_EXCLAMATION_CIRCLE " Warning!" );
    }

    if( ImGui::BeginPopupModal( " " ICON_FA_EXCLAMATION_CIRCLE " Warning!" ) )
    {
        ImGui::Text( m_Params.m_EditField ); 

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            OpenWhichPopup = m_Params.m_CallBack(true);
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            OpenWhichPopup = m_Params.m_CallBack(false);
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // NEW_BLUEPRINT_EDENTITY
    //
    if( OpenWhichPopup == open_popup::NEW_BLUEPRINT_EDENTITY )
    {
        m_BlueprintDlg.Open();
    }

    m_BlueprintDlg.Render();
    if( auto pBlueprint = m_BlueprintDlg.getNewSelected() )
    {
        auto [Error, pEDEntity] = m_Document.CreateEDEntityFromBLueprint( m_Params.m_ZoneGuidField, pBlueprint->getGuid() );
        (void)pEDEntity;
        if( Error.isError() )
        {
            sprintf_s( &m_Params.m_EditField[0], m_Params.m_EditField.getCount<int>(), "Error creating entity due to errors in the blue print, %s. Please check blueprint", Error.getString() );
            OpenWhichPopup = open_popup::ERROR_DLG;
            Error.IgnoreError();
        }
        /*
        gb_component::entity::guid Guid{ gb_component::entity::guid::RESET };
        auto& Entity    = pBlueprint->CreateEntity<gb_component::entity>( Guid, GameGraph.getGraph() );

        xvector<xstring> Issues;
        if( auto Error = Entity.linearCheckResolve( Issues ); Error.isError() )
        {
            Entity.linearDestroy();
            X_LOG_CHANNEL_ERROR( getMainDoc().m_LogChannel, "[LAYERTAB] Error creating entity due to errors in the blue print, %s", Error.getString() );
            OpenWhichPopup = open_popup::ERROR_DLG;
            sprintf_s( &m_Params.m_EditField[0], m_Params.m_EditField.getCount<int>(), "Error creating entity due to errors in the blue print, %s", Error.getString() );
        }
        else
        {
            auto& EDEntity  = m_Document.CreateEDEntity( Entity.getGuid(), m_Params.m_ZoneGuidField );
            Entity.linearAddToWorld();
            m_SelectionDoc.Single( Entity.getGuid(), &EDEntity );
        }
        */
    }

    //
    // NEW_RAW_EDENTITY
    //
    if( OpenWhichPopup == open_popup::NEW_RAW_EDENTITY )
    {
        auto& Entity = m_Document.CreateEntity
        ( 
            GameGraph.getGraph().m_CompTypeDB.getEntry( gb_component::entity::t_descriptors::t_type_guid )
            , gb_component::entity::guid::RESET
            , m_Params.m_ZoneGuidField 
        );

        // Add to world directly
        Entity.linearAddToWorld();
        m_SelectionDoc.Single( Entity.getGuid(), m_Document.findEDEntry(Entity.getGuid()) );
    }

    //
    // NEW_BLUEPRINT_EDENTITY
    //
    if( OpenWhichPopup == open_popup::DELETE_EDENTITY )
    {
        m_Document.DestroyEntity( m_Params.m_EntityGuid );
    }

    //
    // Zone Dialog
    //
    if( OpenWhichPopup == open_popup::NEW_EDZONE_FROM_ZONE ) 
    {
        ImGui::OpenPopup( " " ICON_FA_UNIVERSAL_ACCESS" Zone" );
    }

    {
        edzone_dlg::state State = m_Params.m_EDZoneDlg.Dialog( " " ICON_FA_UNIVERSAL_ACCESS" Zone", m_Document, m_Params.m_LayerGuidField );
        if( State == edzone_dlg::state::OK )
        {
            m_Document.CreateFZone( m_Params.m_LayerGuidField, m_Params.m_EDZoneDlg.getEDZone() );
        }
    }


    //
    // Load New Scene
    //
    if( OpenWhichPopup == open_popup::LOAD_SCENE ) 
    {
        ImGui::OpenPopup( " " ICON_FA_IOXHOST" Scenes" );
    }

    {
        scene_dlg::state State = m_Params.m_SceneDlg.Dialog( " " ICON_FA_IOXHOST" Scenes", m_Document );
        if( State == scene_dlg::state::OK )
        {
            m_Document.LoadScene( m_Params.m_SceneDlg.getActivePath().To<xchar>() );
        }
    }

    //
    // Rename Layer Popup
    //
    if( OpenWhichPopup == open_popup::RENAME_LAYER ) 
    {
        ImGui::OpenPopup( ICON_FA_SITEMAP" Rename Layer" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_SITEMAP" Rename Layer" ) )
    {
        ImGui::Text( "Layer Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Layer = FLayerDBase.getEntry( m_Params.m_LayerGuidField );
            Layer.m_Name.Copy( m_Params.m_EditField );
            Layer.m_bChanged = true;
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // Rename Scene Popup
    //
    if( OpenWhichPopup == open_popup::RENAME_SCENE ) 
    {
        ImGui::OpenPopup( ICON_FA_IOXHOST" Rename Scene" );
        x_strcpy<xchar>( m_Params.m_EditField, m_Document.getScene().m_Name );
    }

    if( ImGui::BeginPopupModal( ICON_FA_IOXHOST" Rename Scene" ) )
    {
        ImGui::Text( "Scene Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Scene = m_Document.getScene();
            Scene.m_Name.Copy( m_Params.m_EditField );
            Scene.m_bChanged = true;
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // Rename Layer Popup
    //
    if( OpenWhichPopup == open_popup::RENAME_ZONE_FOLDER ) 
    {
        ImGui::OpenPopup( ICON_FA_SITEMAP" Rename Zone Folder" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_SITEMAP" Rename Zone Folder" ) )
    {
        ImGui::Text( "Folder Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& FZone = FZoneDBase.getEntry( m_Params.m_ZoneGuidField );

            if( FZone.m_gParent.m_Value )
            {
                FZone.m_Name.Copy( m_Params.m_EditField );
                FZone.m_bChanged = true;
            }
            else
            {
                auto& EDZone = EDZoneDBase.getEntry( FZone.m_ZoneGuid );
                EDZone.m_Name.Copy( m_Params.m_EditField );
                EDZone.m_bChanged = true;
            }
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // New Layer Folder Popup
    //
    if( OpenWhichPopup == open_popup::NEW_LAYER_FOLDER ) 
    {
        m_Params.m_EditField[0] = 0;
        ImGui::OpenPopup( ICON_FA_SITEMAP" New Layer Folder" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_SITEMAP" New Layer Folder" ) )
    {
        ImGui::Text( "Folder Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Layer = m_Document.CreateFLayerFolder( m_Params.m_LayerGuidField );
            Layer.m_Name.Copy( m_Params.m_EditField );
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // New Layer Popup
    //
    if( OpenWhichPopup == open_popup::NEW_LAYER ) 
    {
        m_Params.m_EditField[0] = 0;
        ImGui::OpenPopup( ICON_FA_SITEMAP" New Layer" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_SITEMAP" New Layer" ) )
    {
        ImGui::Text( "Layer Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Layer = m_Document.CreateFLayer();
            Layer.m_Name.Copy( m_Params.m_EditField );
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // New Zone Popup
    //
    if( OpenWhichPopup == open_popup::NEW_EDZONE )
    {
        m_Params.m_EditField[0] = 0;
        ImGui::OpenPopup( ICON_FA_UNIVERSAL_ACCESS" New Zone" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_UNIVERSAL_ACCESS" New Zone" ) )
    {
        ImGui::Text( "Zone Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Zone = m_Document.CreateZone();
            Zone.m_Name.Copy( m_Params.m_EditField );
            auto& FZone = m_Document.CreateFZone( m_Params.m_LayerGuidField, Zone.m_Guid );
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
        
    //
    // New Zone Folder Popup
    //
    if( OpenWhichPopup == open_popup::NEW_ZONE_FOLDER )
    {
        m_Params.m_EditField[0] = 0;
        ImGui::OpenPopup( ICON_FA_UNIVERSAL_ACCESS" New Zone Folder" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_UNIVERSAL_ACCESS" New Zone Folder" ) )
    {
        ImGui::Text( "Folder Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& FZone = m_Document.CreateFZoneFolder( m_Params.m_ZoneGuidField );
            FZone.m_Name.Copy( m_Params.m_EditField );
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // Open new scene popup if we have to
    //
    NewScenePopup();

    //
    // Error popup
    //
    if( OpenWhichPopup == open_popup::ERROR_DLG ) 
    {
        ImGui::OpenPopup( " " ICON_FA_EXCLAMATION_CIRCLE " ERROR!" );
    }

    if( ImGui::BeginPopupModal( " " ICON_FA_EXCLAMATION_CIRCLE " ERROR!" ) )
    {
        ImGui::Text( m_Params.m_EditField ); 

        if( ImGui::Button( "OK", ImVec2(-1,0) ) )
        {
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }

}


