//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "xPlugins/imgui/imguiNodeGraph.h"
#include "EditorComponentList.h"
#include "EditorConstantDataList.h"
#include "EditorBlueprintList.h"
#include "EditorEntityRefList.h"


namespace gbed_node_entity_blueprint
{
    struct types_info
    {
        using guid = imgui_node_graph::node::connection_pod::type::guid;

        x_constexprvar xcolor   s_EntityColor               { 151+10, 103+10, 255, 253 };
        x_constexprvar guid     s_EntityClassGuid           { gb_component::entity::t_class_guid.m_Value };
        x_constexprvar xcolor   s_ComponentColor            { 0, 150, 0, 255 };
        x_constexprvar guid     s_ComponentClassGuid        { gb_component::base::t_class_guid.m_Value };
        x_constexprvar xcolor   s_GameColor                 { 255, 30, 0 };
        x_constexprvar guid     s_GameGuid                  { 1u };
        x_constexprvar xcolor   s_EntityColorRef            { 151, 103, 253, 253 };
        x_constexprvar guid     s_EntityClassGuidRef        { gb_component::entity::t_class_guid.m_Value + 1 };
        x_constexprvar xcolor   s_BlueprintColor            { 100, 100, 200 };
        x_constexprvar guid     s_BlueprintClassGuid        { gb_blueprint::master::t_class_guid.m_Value };
        x_constexprvar xcolor   s_BlueprintCreateColor      { 100+20, 100+20, 200+20 };
        x_constexprvar guid     s_BlueprintCreateClassGuid  { gb_blueprint::master::t_class_guid.m_Value + 1 };
        x_constexprvar xcolor   s_ConstantDataColor         { 200, 100, 200, 255 };
        x_constexprvar guid     s_ConstantDataClassGuid     { 100u };
        x_constexprvar xcolor   s_EventDataColor            { 200, 200, 0, 255 };
        x_constexprvar xcolor   s_InterfaceDataColor        { 200, 100, 100, 255 };
    };

    class inspector_window : public imgui_xproperty::window
    {
        x_object_type( inspector_window, rtti( imgui_xproperty::window) );

    public:

        friend class entity_item;
    
        inspector_window( void )
        {
            m_WindowName = X_STR( "" ICON_FA_SLIDERS " Inspector");
        }

        class entity_item : public imgui_property::item_topfolder<>
        {
        public:
            x_object_type( entity_item, rtti(imgui_property::item_topfolder<>), is_not_copyable, is_not_movable )
            xstring                 m_BackupHelp    {};

        public:

            entity_item     ( imgui_property::window& W ) : t_parent( W ) {}
            virtual void onGadgetRender( void ) override;
        };

        //------------------------------------------------------------------------------------------------------
        virtual xowner<imgui_property::item_topfolder<>*> onAddTopNode( void ) override
        {
            return x_new( entity_item, { *this } );
        }
    };

    class graph_window : public imgui_node_graph::base, public xproperty_v2::base
    {
        x_object_type( graph_window, rtti( imgui_node_graph::base, xproperty_v2::base ) );

    public:

        virtual const xproperty_v2::table& onPropertyTable( void ) const override
        {
            static const xproperty_v2::table E( this, [&](xproperty_v2::table& E)
            {
            });
            return E;
        }

    public:

        graph_window( gb_game_graph::base& Base ): m_GameMgr{ Base }
        {
            m_WindowName = X_STR( "" ICON_FA_CUBES " Composer");
            m_GuidToClassGuid.Initialize( 1000 );
            AddEntityEditorNode();
        }

        void RenderInspector( void )
        {
            m_Inspector.Update();
            m_Inspector.Render();
        }

        bool isEditingBlueprint(void)
        {
            return !!m_pBlueprint;
        }

        void Clear( void )
        {
            // We were editing a blue print before so this means that the entity should not be deleted
            // or we created a blue print from scratch or entity from scratch
            if( m_pBlueprint || (m_pEntity && m_pEntity->isInWorld() == false ) )
            {
                m_pEntity->linearDestroy();
            }

            m_pBlueprint = nullptr;
            m_Inspector.SetProperty(nullptr);
            m_GuidToClassGuid.DeleteAllEntries();
            t_parent::Clear();
            AddEntityEditorNode();
        }

        gb_blueprint::master* getActiveBlueprint( void )
        {
            return m_pBlueprint;
        }

        auto* getEntity(void) const
        {
            return m_pEntity;
        }


        void SetBlueprint( gb_blueprint::master* pBluePrint )
        {
            xmatrix4 L2W;
            L2W.setIdentity();
            L2W.setTranslation( xvector3( x_frand(-10,10) ) );
            auto& Entity = pBluePrint->CreateEntity<gb_component::entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), L2W, m_GameMgr );

            Clear();
            m_pBlueprint = pBluePrint;
            SetEntity(&Entity,true);
        }

        void SetEntity( gb_component::entity* pEntity, bool bSkipClear = false )
        {
            int a=0;
            if(bSkipClear==false) Clear();

            m_pEntity = pEntity;

            setReadOnly(false);//m_pEntity->isInWorld());

            m_ComponentList.Refresh( pEntity->getGlobalInterface().m_GameMgr );

            // Game node
            const imgui_node_graph::node::guid                  gEntityNodeGUID         { imgui_node_graph::node::guid::RESET };
            const imgui_node_graph::node::connection_pod::guid  gEntityEntityPod        { imgui_node_graph::node::connection_pod::guid::RESET };
            const imgui_node_graph::node::connection_pod::guid  gEntityComponentPod     { imgui_node_graph::node::connection_pod::guid::RESET };
            const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod     { imgui_node_graph::node::connection_pod::guid::RESET };
            ImVec2                                              Pos                     { m_GameNodeStartingPos };

            for( gb_component::base* pComponent = m_pEntity; 
                                     pComponent; 
                                     pComponent = pComponent->getNextComponent() )
            { 

                if( pComponent == m_pEntity )
                {
                    Pos = AddEntity
                    (
                        gEntityNodeGUID
                        , m_pGraphNode->getGuid()
                        , m_pGraphNode->getPod(0).getGuid()
                        , gEntityComponentPod
                        , gEntityEntityPod
                        , gEntityBlueprintPod
                        , pComponent
                        , Pos 
                    );
                }
                else
                {
                    Pos = AddComponent
                    (
                        gEntityNodeGUID,
                        gEntityComponentPod,
                        pComponent,
                        Pos 
                    );
                }
            }

            //
            // Finds an event pod given just the guid for the event
            //
            auto FindEventPod = [&]( u64 Guid ) -> imgui_node_graph::node::connection_pod*
            {
                for( auto& NodePair : m_NodeDB )
                {
                    auto& Node = *NodePair.second;

                    // We only care about Entity nodes or component nodes
                    if( x_strcmp<xchar>( "Entity", Node.getName() ) && x_strcmp<xchar>( "Component", Node.getName() ) ) continue;

                    for( auto& Pod : Node.getPods() )
                    {
                        if( Pod.m_bIsInput == false ) continue;
                        if( Pod.m_UserData == Guid )  return &Pod;
                    }
                }

                return nullptr;
            };

            //
            // Link up all the delegates
            //
            for( auto& NodePair : m_NodeDB )
            {
                auto& Node = *NodePair.second;

                // We only care about Entity nodes or component nodes
                if( x_strcmp<xchar>( "Entity", Node.getName() ) && x_strcmp<xchar>( "Component", Node.getName() ) ) continue;

                for( auto& Pod : Node.getPods() )
                {
                    if( Pod.m_bIsInput == true ) continue;
                    if( Pod.m_UserData == 0 )    continue;
                    if(     gb_component::event_type::guid(Pod.m_gType.m_Value).getClassGuid()      != gb_component::event_type::t_class_guid
                        &&  gb_component::interface_type::guid(Pod.m_gType.m_Value).getClassGuid()  != gb_component::interface_type::t_class_guid ) continue;

                    auto* pConPod = FindEventPod( Pod.m_UserData );

                    xndptr_s<imgui_node_graph::link>  Link;
                    Link.New();
                    Link->m_ConnectionHooks[0].m_gNode = pConPod->m_Node.getGuid();
                    Link->m_ConnectionHooks[0].m_gPod  = pConPod->getGuid();
                    Link->m_ConnectionHooks[1].m_gNode = Pod.m_Node.getGuid();
                    Link->m_ConnectionHooks[1].m_gPod  = Pod.getGuid();
                    Link->m_Guid.Reset();
                    AddLink( std::move(Link) );
                }
            }

            //
            // Do Select
            //
            m_NodeDB[gEntityNodeGUID]->setSelected(true);
            m_Inspector.SetProperty( pEntity->getLinearPropInterface() );
        }

    protected:

        struct trick : public gb_component::base
        {
            xproperty_v2::base* getPropertyInterface( void ) { return this; }
        };

        struct graph_node : imgui_node_graph::node, xproperty_v2::base
        {
            x_object_type( graph_node, rtti( imgui_node_graph::node, xproperty_v2::base ) );
            using node::node;

            void setColor( xcolor C ) { m_Color = C; }
            
            graph_node( graph_window& Win, guid Guid, xstring Name, ImVec2 Pos, bool bCloseWindow ) : 
                node( Guid, Name, Pos, bCloseWindow )
                , m_Graph{ Win } {}

            virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
            { 
                static prop_table E( this, [&](xproperty_v2::table& E)
                {
                    E.AddDynamicScope(
                        { nullptr }
                        , X_STATIC_LAMBDA_BEGIN (graph_node& A) -> xproperty_v2::base*
                        {
                            return A.m_pProperty;
                        }
                        X_STATIC_LAMBDA_END
                    );
                });

                return E; 
            }

            graph_window&       m_Graph;
            xproperty_v2::base* m_pProperty    { nullptr };
            u64                 m_InstanceGuid { 0 };
            bool                m_d;
        };

        struct editor_node : graph_node
        {
            x_object_type( editor_node, rtti( graph_node ) );
            using graph_node::graph_node;

            virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
            { 
                static prop_table E( this, [&](xproperty_v2::table& E)
                {
                    E.AddChildTable( t_parent::onPropertyTable() );
                    E.AddScope( X_STR("Composer"), [&]( xproperty_v2::table& E )
                    {
                        E.AddProperty<xproperty_v2::button>
                        ( 
                            X_STR("Clear")
                            , X_STATIC_LAMBDA_BEGIN (editor_node& Class, xproperty_v2::button& Data, const xproperty_v2::info::button& Details, bool isSend)
                            {
                                if (isSend) Data.m_Value = X_STR("Click to Clear") ;
                                else
                                {
                                    Class.m_Graph.Clear();
                                }
                            }
                            X_STATIC_LAMBDA_END
                            , X_STR("Resets the entity editor, any unsaved data may be lost.") 
                        );
                        
                        E.AddProperty<xproperty_v2::button>
                        (
                            X_STR("World")
                            , X_STATIC_LAMBDA_BEGIN (editor_node& Class ) -> bool
                            {
                                if (Class.m_Graph.getEntity() == nullptr) return false;
                                return Class.m_Graph.isEditingBlueprint() == false;
                            }
                            X_STATIC_LAMBDA_END
                            , X_STATIC_LAMBDA_BEGIN (editor_node& Class, xproperty_v2::button& Data, const xproperty_v2::info::button& Details, bool isSend)
                            {
                                auto pEntity = Class.m_Graph.getEntity();
                                if ( pEntity == nullptr )
                                {
                                    if (isSend) Data.m_Value = X_STR("Empty");
                                    return;
                                }

                                if (isSend)
                                {
                                    if( pEntity->isInWorld() )
                                    {
                                        Data.m_Value = X_STR("Take out");
                                    }
                                    else
                                    {
                                        Data.m_Value = X_STR("Put in");
                                    }
                                }
                                else
                                {
                                    Class.m_Graph.Clear();
                                }
                            }
                            X_STATIC_LAMBDA_END
                            , X_STR("Resets the entity editor, any unsaved data may be lost.")
                        );
       
                    });
                });

                return E; 
            }
        };

        struct blue_print_node : graph_node
        {
            x_object_type( blue_print_node, rtti( graph_node ) );
            using graph_node::graph_node;

            virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
            { 
                static prop_table E( this, [&](xproperty_v2::table& E)
                {
                    E.AddChildTable( t_parent::onPropertyTable() );
                    E.AddScope( X_STR(".Editor"), [&]( xproperty_v2::table& E )
                    {
                        E.AddProperty<xproperty_v2::button>
                        ( 
                            X_STR("Save as new")
                            , X_STATIC_LAMBDA_BEGIN (blue_print_node& Class, xproperty_v2::button& Data, const xproperty_v2::info::button& Details, bool isSend)
                            {
                                if (isSend) Data.m_Value = X_STR("Click to Save") ;
                                else
                                {
                                    auto& Master = Class.m_pProperty->SafeCast<gb_blueprint::master>();
                                    Master.UpdateAsNew( *Class.m_Graph.m_pEntity ); 
                                    Class.m_Graph.m_GameMgr.m_eventNewBlueprint.Notify( nullptr );
                                }
                            }
                            X_STATIC_LAMBDA_END
                            , X_STR("Reset blue print to the current graph and properties. Note that all instances that depend on this blue print will get affected.") 
                        );
                    });
                });

                return E; 
            }
        };

        enum class open_selector
        {
            NONE
            , ENTITY
            , ENTITY_REF
            , COMPONENT
            , CONSTANT_DATA
            , BLUEPRINT
            , BLUEPRINT_CREATE
        };

    protected:

        //-------------------------------------------------------------------------------------------------

        void AddEntityEditorNode( void )
        {
            const imgui_node_graph::node::guid                  gGameNodeGUID        { imgui_node_graph::node::guid::RESET };
            const imgui_node_graph::node::connection_pod::guid  gGameNodePodGUID     { imgui_node_graph::node::connection_pod::guid::RESET };
            ImVec2                                              Pos                  { 10, 10 };

            m_GameNodeStartingPos = AddEntityEditorNode
            (
                gGameNodeGUID,
                gGameNodePodGUID,
                Pos
            );

            // Keep a shortcut to the game node
            m_pGraphNode = &m_NodeDB[ gGameNodeGUID ]->SafeCast<graph_node>();

            //
            // Add Basic Node classes
            //
            m_GuidToClassGuid.cpAddEntry( types_info::s_GameGuid.m_Value, []( types_info::guid& Entry ) 
            { 
                Entry = types_info::s_GameGuid;     
            });

            m_GuidToClassGuid.cpFindOrAddEntry( types_info::s_EntityClassGuid.m_Value, []( types_info::guid& Entry )
            {
                Entry = types_info::s_EntityClassGuid;
            });

            m_GuidToClassGuid.cpFindOrAddEntry( types_info::s_ComponentClassGuid.m_Value, []( types_info::guid& Entry )
            {
                Entry = types_info::s_ComponentClassGuid;
            });
        }

        //-------------------------------------------------------------------------------------------------
        ImVec2 AddEntityEditorNode(
            const imgui_node_graph::node::guid                  gGameNodeGUID,   
            const imgui_node_graph::node::connection_pod::guid  gGameNodePodGUID,
            ImVec2                                              Pos
        )
        {
            xndptr_s<editor_node> NodeTest;  
            NodeTest.New( *this, gGameNodeGUID, X_STR("Composer"), Pos, false );
            NodeTest->m_pProperty = this;
            NodeTest->setColor( types_info::s_GameColor );

            NodeTest->AddOuputPod(
                gGameNodePodGUID, 
                X_STR("Entity"), 
                types_info::s_EntityClassGuid,
                types_info::s_EntityColor,
                false,
                X_STR(" Connect Add or Replace ") );

            Pos.x += 2*NodeTest->getSize().x;

            NodeTest->EndSetup();

            AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );

            return Pos;
        }

        //-------------------------------------------------------------------------------------------------
        void AddEvents( graph_node& Node, gb_component::base& Base )
        {
            //
            // Add events
            //
            {
                for( auto& E : Base.getEventTable() )
                {
                    auto& Pod = Node.AddInputPod
                    (
                        imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }
                        , E.m_Name
                        , imgui_node_graph::node::connection_pod::type::guid{ E.m_EventType.m_Guid.m_Value }
                        , ( E.m_EventType.m_Guid.getClassGuid() == gb_component::event_type::t_class_guid) ? types_info::s_EventDataColor : types_info::s_InterfaceDataColor 
                        , false
                        , X_STR(" Connect ")
                    );

                    Pod.m_UserData = Base.getEventGuid( E ).m_Value;

                    int a=0;
                }
            }

            //
            // Add Delegates
            //
            for( auto& E : Base.getDelegateTable() )
            {
                auto& Pod = Node.AddOuputPod
                (
                    imgui_node_graph::node::connection_pod::guid{ imgui_node_graph::node::connection_pod::guid::RESET }
                    , E.m_Name
                    , imgui_node_graph::node::connection_pod::type::guid{ E.m_EventType.m_Guid.m_Value }
                    , ( E.m_EventType.m_Guid.getClassGuid() == gb_component::event_type::t_class_guid) ? types_info::s_EventDataColor : types_info::s_InterfaceDataColor 
                    , true
                    , X_STR(" Connect ") 
                );

                // get the value of the delegate and store it in the pod 
                if( E.m_EventType.m_Guid.getClassGuid() == gb_component::event_type::t_class_guid )
                {
                    auto pFakeDelegate = ((gb_component::event_fake::delegate<gb_component::base>*)&((xbyte*)&Base.getDelegates())[E.m_Offset]);
                    Pod.m_UserData = pFakeDelegate->m_Guid.m_Value;
                }
                else
                {
                    x_assert( E.m_EventType.m_Guid.getClassGuid() == gb_component::interface_type::t_class_guid );
                    auto pFakeDelegate = ((gb_component::interface_ref<void>*)&((xbyte*)&Base)[E.m_Offset]);
                    Pod.m_UserData = pFakeDelegate->m_Guid.m_Value;
                }
            }
        }


        //-------------------------------------------------------------------------------------------------

        ImVec2 AddEntity(
            const imgui_node_graph::node::guid                  gEntityNodeGUID,
            const imgui_node_graph::node::guid                  gGameNodeGUID,
            const imgui_node_graph::node::connection_pod::guid  gGameNodePod,
            const imgui_node_graph::node::connection_pod::guid  gComponentPod,
            const imgui_node_graph::node::connection_pod::guid  gEntityPod,
            const imgui_node_graph::node::connection_pod::guid  gBlueprintPod,
            gb_component::base*                                 pComponent,
            ImVec2                                              Pos )
        {
            xndptr_s<graph_node> NodeTest;

            m_gEntity = gEntityNodeGUID;

            NodeTest.New( *this, gEntityNodeGUID, X_STR("Entity"), Pos, true );

            NodeTest->AddInputPod(
                gEntityPod 
                , pComponent->getGlobalInterface().m_Type.getCategoryName().m_Char
                , types_info::s_EntityClassGuid
                , types_info::s_EntityColor
                , false
                , X_STR(" Connect or Replace ") );

            NodeTest->AddInputPod(
                gBlueprintPod 
                , X_STR("Blueprint.Create")
                , types_info::s_BlueprintCreateClassGuid
                , types_info::s_BlueprintCreateColor
                , false
                , X_STR(" Connect or Add ") );

            NodeTest->AddOuputPod(
                gComponentPod
                , gb_component::base::t_class_string
                , types_info::s_ComponentClassGuid
                , types_info::s_ComponentColor
                , false
                , X_STR(" Connect or Add ") );

            AddEvents( NodeTest, *pComponent );

            NodeTest->setColor( types_info::s_EntityColor );
            NodeTest->EndSetup();

            xndptr_s<imgui_node_graph::link>  Link;
            Link.New();
            Link->m_ConnectionHooks[0].m_gNode = gEntityNodeGUID;
            Link->m_ConnectionHooks[0].m_gPod  = gEntityPod;
            Link->m_ConnectionHooks[1].m_gNode = gGameNodeGUID;
            Link->m_ConnectionHooks[1].m_gPod  = gGameNodePod;
            Link->m_Guid.Reset();
            AddLink( std::move(Link) );

            NodeTest->m_pProperty = reinterpret_cast<trick*>(pComponent)->getPropertyInterface();

           // Pos.y += 20 +  NodeTest->getSize().y;

            //
            // If it has a blue print node then add it
            //
            auto gBlueprint = m_pEntity->SafeCast<gb_component::entity>().getBlueprintGuid();
            if( gBlueprint.m_Value )
            {
                //gBlueprint 
                auto& Master = m_GameMgr.m_BlueprintDB.getEntry( gBlueprint );

                AddBlueprintEditNode
                ( 
                    gEntityNodeGUID
                    , gBlueprintPod
                    , &Master
                    , ImVec2{ Pos.x - 240, 20 +  NodeTest->getSize().y }                             
                );
            }

            //
            // Add the rest
            //
            Pos.x += 2 * NodeTest->getSize().x;
            Pos = AddNodeProperties( NodeTest, gEntityNodeGUID, Pos );

            AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );


            return Pos;
        }

        //-------------------------------------------------------------------------------------------------

        ImVec2 AddBlueprintEditNode(
            const imgui_node_graph::node::guid                  gEntityNodeGUID,
            const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod,
            gb_blueprint::master*                               pBlueprint,
            ImVec2                                              Pos )
        {
            xndptr_s<blue_print_node> NodeTest;
            const imgui_node_graph::node::guid                  gGuid       { imgui_node_graph::node::guid::RESET };
            const imgui_node_graph::node::connection_pod::guid  gPodGuid    { imgui_node_graph::node::connection_pod::guid::RESET };
                    
            x_constexprvar auto gConnectionType = imgui_node_graph::node::connection_pod::type::guid{ gb_blueprint::master::t_class_guid.m_Value };

            NodeTest.New( *this, gGuid, X_STR("Blueprint.Create"), Pos, m_pBlueprint == nullptr );
            NodeTest->AddOuputPod
            (
                gPodGuid 
                , X_STR("Entity")
                , types_info::s_BlueprintCreateClassGuid
                , types_info::s_BlueprintCreateColor
                , false
                , X_STR(" Connect or Replace Self ") 
            );

            NodeTest->setColor( types_info::s_BlueprintCreateColor );
            
            xndptr_s<imgui_node_graph::link>  Link;
            Link.New();
            Link->m_ConnectionHooks[1].m_gNode = gGuid;
            Link->m_ConnectionHooks[1].m_gPod  = gPodGuid;
            Link->m_ConnectionHooks[0].m_gNode = gEntityNodeGUID;
            Link->m_ConnectionHooks[0].m_gPod  = gEntityBlueprintPod;
            Link->m_Guid.Reset();
            AddLink( std::move(Link) );
            
            //const auto posx = Pos.x + 2 * NodeTest->getSize().x;
            const auto posy = Pos.y;
            Pos.y += 20 +  NodeTest->getSize().y;

            NodeTest->m_pProperty = pBlueprint;
            NodeTest->EndSetup();
            AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );
            return Pos;
        }

        //-------------------------------------------------------------------------------------------------

        ImVec2 AddComponent(
            const imgui_node_graph::node::guid                  gEntityNodeGUID,
            const imgui_node_graph::node::connection_pod::guid  gEntityComponentPod,
            gb_component::base*                                 pComponent,
            ImVec2                                              Pos )
        {
            xndptr_s<graph_node> NodeTest;
            const imgui_node_graph::node::guid                  gGuid       { imgui_node_graph::node::guid::RESET };
            const imgui_node_graph::node::connection_pod::guid  gPodGuid    { imgui_node_graph::node::connection_pod::guid::RESET };
                    
            x_constexprvar auto gConnectionType = imgui_node_graph::node::connection_pod::type::guid{ gb_component::base::t_class_guid.m_Value };

            NodeTest.New( *this, gGuid, X_STR("Component"), Pos, true );
            NodeTest->AddInputPod
            (
                gPodGuid 
                , pComponent->getGlobalInterface().m_Type.getCategoryName().m_Char 
                , types_info::s_ComponentClassGuid
                , types_info::s_ComponentColor
                , false
                , X_STR(" Connect or Replace Self ") 
            );

            AddEvents( NodeTest, *pComponent );

            NodeTest->setColor( types_info::s_ComponentColor );
            NodeTest->EndSetup();

            xndptr_s<imgui_node_graph::link>  Link;
            Link.New();
            Link->m_ConnectionHooks[0].m_gNode = gGuid;
            Link->m_ConnectionHooks[0].m_gPod  = gPodGuid;
            Link->m_ConnectionHooks[1].m_gNode = gEntityNodeGUID;
            Link->m_ConnectionHooks[1].m_gPod  = gEntityComponentPod;
            Link->m_Guid.Reset();
            AddLink( std::move(Link) );

            const auto posx = Pos.x + 2 * NodeTest->getSize().x;
            const auto posy = Pos.y;
            Pos.y += 20 +  NodeTest->getSize().y;

            NodeTest->m_pProperty = reinterpret_cast<trick*>(pComponent)->getPropertyInterface();

            AddNodeProperties( NodeTest, gGuid, ImVec2{ posx, posy } );
            AddNode( xndptr_s<imgui_node_graph::node>{ NodeTest.TransferOwnership() } );

            return Pos;
        }

        //-------------------------------------------------------------------------------------------------

        ImVec2 AddBlueprintNode
        (
            const imgui_node_graph::node::guid                      gComponentNodeGuid 
            , const imgui_node_graph::node::connection_pod::guid    gComponentBlueprintPod
            , const gb_blueprint::master::guid                      gBlueprintInstance
            , ImVec2                                                Pos
        )
        {
            const imgui_node_graph::node::guid  gBlueprintNodeGuid( imgui_node_graph::node::guid::RESET );
            xndptr_s<graph_node>                NewNode;
            
            NewNode.New( *this, gBlueprintNodeGuid, gb_blueprint::master::t_class_string, Pos, true );
                
            auto& Blueprint   = m_pEntity->getGlobalInterface().m_GameMgr.m_BlueprintDB.getEntry( gBlueprintInstance );
            NewNode->m_pProperty = &Blueprint;
            NewNode->setColor( types_info::s_BlueprintColor );
            NewNode->m_InstanceGuid = gBlueprintInstance.m_Value;

//            Pos.x += 2 * NewNode->getSize().x;

            const imgui_node_graph::node::connection_pod::guid gNodePod{ imgui_node_graph::node::connection_pod::guid::RESET };
            auto& Pod = NewNode->AddInputPod
            (
                gNodePod 
                , Blueprint.getName()
                , imgui_node_graph::node::connection_pod::type::guid{ gb_blueprint::master::t_class_guid.m_Value }
                , types_info::s_BlueprintColor
                , false
                , X_STR(" Connect or Replace Self ") 
            );
            Pod.m_UserData     = gBlueprintInstance.m_Value;

            NewNode->EndSetup();
            xndptr_s<imgui_node_graph::link>  Link;
            Link.New();
            Link->m_ConnectionHooks[0].m_gNode = gBlueprintNodeGuid;
            Link->m_ConnectionHooks[0].m_gPod  = gNodePod;
            Link->m_ConnectionHooks[1].m_gNode = gComponentNodeGuid;
            Link->m_ConnectionHooks[1].m_gPod  = gComponentBlueprintPod;
            Link->m_Guid.Reset();
            AddLink( std::move(Link) );

            Pos.y += 20 +  NewNode->getSize().y;
            
            Pos = AddNodeProperties( NewNode, gBlueprintNodeGuid, Pos );
            AddNode( xndptr_s<imgui_node_graph::node>{ NewNode.TransferOwnership() } );

            return Pos;
        }

        //-------------------------------------------------------------------------------------------------

        ImVec2 AddEntityRefNode
        (
            const imgui_node_graph::node::guid                      gComponentNodeGuid 
            , const imgui_node_graph::node::connection_pod::guid    gComponentBlueprintPod
            , const gb_component::entity::guid                      gEntityInstance
            , ImVec2                                                Pos
        )
        {
            const imgui_node_graph::node::guid  gEntityRefNodeGuid( imgui_node_graph::node::guid::RESET );
            xndptr_s<graph_node>                NewNode;
            
            NewNode.New( *this, gEntityRefNodeGuid,X_STR("Entity.ref"), Pos, true );

            auto& Entity   = *m_pEntity->getGlobalInterface().m_GameMgr.getEntity<gb_component::entity>( gEntityInstance );
            
            NewNode->m_pProperty = Entity.getLinearPropInterface();
            
            NewNode->setColor( types_info::s_EntityColorRef );
            
        //    NewNode->m_InstanceGuid = gEntityInstance.m_Value;

//            Pos.x += 2 * NewNode->getSize().x;

            const imgui_node_graph::node::connection_pod::guid gNodePod{ imgui_node_graph::node::connection_pod::guid::RESET };
            auto& Pod = NewNode->AddInputPod
            (
                gNodePod 
                , Entity.getGlobalInterface().m_Type.getCategoryName().m_Char
                , imgui_node_graph::node::connection_pod::type::guid{ types_info::s_EntityClassGuidRef }
                , types_info::s_EntityColorRef
                , false
                , X_STR(" Connect or Replace Self ") 
            );
            Pod.m_UserData     = gEntityInstance.m_Value;

            NewNode->EndSetup();
            xndptr_s<imgui_node_graph::link>  Link;
            Link.New();
            Link->m_ConnectionHooks[0].m_gNode = gEntityRefNodeGuid;
            Link->m_ConnectionHooks[0].m_gPod  = gNodePod;
            Link->m_ConnectionHooks[1].m_gNode = gComponentNodeGuid;
            Link->m_ConnectionHooks[1].m_gPod  = gComponentBlueprintPod;
            Link->m_Guid.Reset();
            AddLink( std::move(Link) );

            Pos.y += 20 +  NewNode->getSize().y;
            
            Pos = AddNodeProperties( NewNode, gEntityRefNodeGuid, Pos );
            AddNode( xndptr_s<imgui_node_graph::node>{ NewNode.TransferOwnership() } );

            return Pos;
        }

        //-------------------------------------------------------------------------------------------------

        ImVec2 AddConstantNode(
            const gb_component::const_data::guid                gConstantDataInstace,  
            const gb_component::const_data::type::guid          gConstantDataType, 
            const imgui_node_graph::node::guid                  gNodeGuid, 
            const imgui_node_graph::node::connection_pod::guid  gConstantDataPod,
            ImVec2                                              Pos )
        {
            m_GuidToClassGuid.cpFindOrAddEntry( gConstantDataType.m_Value, []( types_info::guid& Entry )
            {
                Entry = types_info::s_ConstantDataClassGuid;
            });

            const imgui_node_graph::node::guid  gConstantDataNodeGuid( imgui_node_graph::node::guid::RESET );
            xndptr_s<graph_node>                NewNode;
            
            NewNode.New( *this, gConstantDataNodeGuid, gb_component::const_data::type::t_class_string, Pos, true );
                
            auto& ConstantData   = m_pEntity->getGlobalInterface().m_GameMgr.m_ConstDataDB.getEntry( gConstantDataInstace );
            NewNode->m_pProperty = &ConstantData;
            NewNode->setColor( types_info::s_ConstantDataColor );
            NewNode->m_InstanceGuid = gConstantDataInstace.m_Value;

//            Pos.x += 2 * NewNode->getSize().x;

            const imgui_node_graph::node::connection_pod::guid gNodePod{ imgui_node_graph::node::connection_pod::guid::RESET };
            NewNode->AddInputPod
            (
                gNodePod 
                , m_GameMgr.m_ConstDataTypeDB.getEntry( gConstantDataType ).getEditorName()
                , imgui_node_graph::node::connection_pod::type::guid{ gConstantDataType.m_Value }
                , types_info::s_ConstantDataColor
                , false
                , X_STR(" Connect or Replace Self ") 
            );

            NewNode->EndSetup();
            xndptr_s<imgui_node_graph::link>  Link;
            Link.New();
            Link->m_ConnectionHooks[0].m_gNode = gConstantDataNodeGuid;
            Link->m_ConnectionHooks[0].m_gPod  = gNodePod;
            Link->m_ConnectionHooks[1].m_gNode = gNodeGuid;
            Link->m_ConnectionHooks[1].m_gPod  = gConstantDataPod;
            Link->m_Guid.Reset();
            AddLink( std::move(Link) );

            Pos.y += 20 +  NewNode->getSize().y;
            
            Pos = AddNodeProperties( NewNode, gConstantDataNodeGuid, Pos );
            AddNode( xndptr_s<imgui_node_graph::node>{ NewNode.TransferOwnership() } );

            return Pos;
        }

        //-------------------------------------------------------------------------------------------------

        ImVec2 AddNodeProperties( graph_node& NodeTest, const imgui_node_graph::node::guid gNodeGuid, ImVec2 Pos )
        {
            xproperty_v2::entry Entry;

            //
            // process
            //
            auto Process = [&]()
            {
                if( Entry.m_Data.getTypeIndex() == xproperty_v2::prop_type::GUID 
                    /*&& Entry.m_Data. Data.getFlags().m_READ_ONLY == false*/ )
                {
                    const auto& PropInfo    = *reinterpret_cast<const xproperty_v2::info::guid*>( Entry.m_pDetails );
                    const auto  gEntry      = Entry.m_Data.get<xproperty_v2::prop_type::GUID>();
                    const auto  gClassGuid  = gb_component::extended_guid<void,void,0>{ PropInfo.m_Type }; 

                    if( gClassGuid.getClassGuid() == gb_component::const_data::type::t_class_guid && gClassGuid.getNameCRC().isValid() )
                    {
                        const imgui_node_graph::node::connection_pod::guid          gConstantDataPod        { imgui_node_graph::node::connection_pod::guid::RESET };

                        NodeTest.AddOuputPod( 
                            gConstantDataPod 
                            , Entry.m_Name
                            , imgui_node_graph::node::connection_pod::type::guid{ gClassGuid.m_Value }
                            , types_info::s_ConstantDataColor
                            , true
                            , X_STR(" Connect or Add New ") 
                        );

                        if( gEntry )
                        {
                            Pos = AddConstantNode
                            (
                                gb_component::const_data::guid{ gEntry },  
                                gb_component::const_data::type::guid{ PropInfo.m_Type }, 
                                gNodeGuid, 
                                gConstantDataPod,
                                Pos 
                            );
                        }
                    }
                    else if( gClassGuid.getClassGuid() == gb_blueprint::master::t_class_guid )
                    {
                        const imgui_node_graph::node::connection_pod::guid          gBlueprintDataPod        { imgui_node_graph::node::connection_pod::guid::RESET };
                        
                        NodeTest.AddOuputPod( 
                            gBlueprintDataPod 
                            , Entry.m_Name
                            , imgui_node_graph::node::connection_pod::type::guid{ PropInfo.m_Type }
                            , types_info::s_BlueprintColor
                            , true
                            , X_STR(" Connect or Add New ") 
                        );
                    
                        if( gEntry )
                        {
                            Pos = AddBlueprintNode
                            (
                                gNodeGuid 
                                , gBlueprintDataPod 
                                , gb_blueprint::master::guid{ gEntry }  
                                , Pos 
                            );
                        }
                    }
                    else if( gClassGuid.getClassGuid() == gb_component::entity::t_class_guid )
                    {
                        if( PropInfo.m_Flags.m_READ_ONLY == true || PropInfo.m_Flags.m_NOT_VISIBLE == true )
                            return;

                        const imgui_node_graph::node::connection_pod::guid          gEntityPod        { imgui_node_graph::node::connection_pod::guid::RESET };

                        NodeTest.AddOuputPod( 
                            gEntityPod 
                            , Entry.m_Name
                            , imgui_node_graph::node::connection_pod::type::guid{ types_info::s_EntityClassGuidRef }
                            , types_info::s_EntityColorRef
                            , true
                            , X_STR(" Connect or Add New ") 
                        );
                    
                        if( gEntry )
                        {
                            Pos = AddEntityRefNode
                            (
                                gNodeGuid 
                                , gEntityPod 
                                , gb_component::entity::guid{ gEntry }
                                , Pos 
                            );
                        }
                    }
                }
            };

            //
            // Enum properties
            //
            NodeTest.m_pProperty->EnumProperty( [&]() ->xproperty_v2::entry&
            {
                if( Entry.m_Data.isValid() ) 
                {
                    if( x_strncmp<xchar>( Entry.m_Name, "Comp", 4 ) == 0 && ( Entry.m_Name[4] == '[' || Entry.m_Name[4] == '(') )
                    {
                        // skip components....
                    }
                    else
                    {
                        Process();
                    }
                }
                Entry.m_Name.clear();
                return Entry;
            }, true, xproperty_v2::base::enum_mode::REALTIME );

            if( x_strncmp<xchar>( Entry.m_Name, "Comp", 4 ) == 0 && ( Entry.m_Name[4] == '[' || Entry.m_Name[4] == '(') )
            {
                // skip components....
            }
            else
            {
                Process();
            }
    

            return Pos;
        }

        //-------------------------------------------------------------------------------------------------

        void HandleConstantDataSelector( void )
        {
            if( m_ConstantDataList.Render() == false ) return;

            auto& ConstantData = m_ConstantDataList.getSelected();
            if( m_pSelectedPod->m_bIsInput )
            {
                //... This pod could have many outputs

                // Find which nodes are connected to this constant data node and change their constant property
                // Change the current constant node with new properties
                xvector<imgui_node_graph::link::guid> gLinksList;
                getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

                // We will leave the old pos in place it looks cleaner
                // m_pSelectedPod->m_Node.setPos( m_NewPos );

                // TODO: Must delete all dependencies of the constant node
                for( const auto& gLink : gLinksList )
                {
                    const auto&     Link        = *m_LinkDB[ gLink ];
                    const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                    auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
                    auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

                    Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, ConstantData.getGuid().m_Value );
                }

                // Set the new constant data property pointer
                auto& GraphNode         = m_pSelectedPod->m_Node.SafeCast<graph_node>();
                GraphNode.m_pProperty   = &ConstantData;
                        
                // Make the inspector to look at this constant data
                SingleSelect( GraphNode.getGuid() );
                m_Inspector.SetProperty( &GraphNode );//.m_pProperty );
            }
            else 
            {
                // Any input for a constant data can only be one
                xvector<imgui_node_graph::link::guid> gLinksList;
                getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );
                x_assert( gLinksList.getCount() < 2 );
                bool bCreateNewContant = true;
                    
                for( const auto& gLink : gLinksList )
                {
                    const auto&     Link        = *m_LinkDB[ gLink ];
                    const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                    auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
                    auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

                    // If the user is asking us to create the same constant data then we are going to bail out
                    if( Node.SafeCast<graph_node>().m_pProperty->SafeCast<gb_component::const_data>().getGuid() == ConstantData.getGuid() )
                    {
                        bCreateNewContant = false;
                        break;
                    }

                    // Delete the link
                    m_LinkDB.erase( gLink );

                    // If there is no other node that uses the constant data then nuke it
                    xvector<imgui_node_graph::link::guid> gOtherLinksList;
                    getLinksConnectingFromPod( gOtherLinksList, OtherPod.getGuid() );

                    if( gOtherLinksList.getCount() == 0 )
                    {
                        m_NodeDB.erase( Node.getGuid() );
                    }
                }

                // If it has a link then Change the constant node with new property and new pos
                // else create a constant node assign new properties and new pos and create a new link
                if( bCreateNewContant )
                {
                    auto& NodeGraph = m_pSelectedPod->m_Node.SafeCast<graph_node>();
                    NodeGraph.m_pProperty->SetProperty( m_pSelectedPod->m_Name, ConstantData.getGuid().m_Value );

                    // We need to create a node for this constant data
                    AddConstantNode
                    (
                        ConstantData.getGuid(),
                        ConstantData.getType().getGuid(),
                        NodeGraph.getGuid(),
                        m_pSelectedPod->getGuid(),
                        m_NewPos
                    );
                }
            }
        }

        //-------------------------------------------------------------------------------------------------

        void HandleBlueprintSelector( void )
        {
            if( m_BlueprintList.Render() == false ) return;
            auto& Blueprint = m_BlueprintList.getSelected();

            /*
            if( m_pSelectedPod->m_bIsInput )
            {
                //... This pod could have many outputs

                // Find which nodes are connected to this constant data node and change their constant property
                // Change the current constant node with new properties
                xvector<imgui_node_graph::link::guid> gLinksList;
                getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

                // We will leave the old pos in place it looks cleaner
                // m_pSelectedPod->m_Node.setPos( m_NewPos );

                // TODO: Must delete all dependencies of the constant node
                for( const auto& gLink : gLinksList )
                {
                    const auto&     Link        = *m_LinkDB[ gLink ];
                    const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                    auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
                    auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

                    Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, Blueprint.getGuid().m_Value );
                }

                // Set the new constant data property pointer
                auto& GraphNode         = m_pSelectedPod->m_Node.SafeCast<graph_node>();
                GraphNode.m_pProperty   = &Blueprint;
                        
                // Make the inspector to look at this constant data
                SingleSelect( GraphNode.getGuid() );
                m_Inspector.SetProperty( &GraphNode );//.m_pProperty );
            }
            else
            */

  //          x_assert( m_pSelectedPod->m_bIsInput );

 //           const imgui_node_graph::node::guid                  gEntityNodeGUID,
   //         const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod,
     //       gb_blueprint::master*                               pBlueprint,
       //     ImVec2                                              Pos )
 
            // Are we dealing with the blue print editor node?
            if( m_pSelectedPod->m_gType == types_info::s_BlueprintCreateClassGuid )
            {
                x_assert( m_BlueprintList.isCreated() );
                // Setup a newblueprint
                Blueprint.setup( X_STR(""), X_STR("New blueprint"), *m_pEntity );
                m_pEntity->setupBlueprintGuid( Blueprint.getGuid() );

                // We need to create a node for this constant data
                AddBlueprintEditNode
                (
                    m_gEntity
                    , m_pSelectedPod->m_Guid
                    , &Blueprint
                    , m_NewPos
                );
            }
            else
            {
                if( m_pSelectedPod->m_bIsInput )
                {
                    //... This pod could have many outputs

                    // Find which nodes are connected to this constant data node and change their constant property
                    // Change the current constant node with new properties
                    xvector<imgui_node_graph::link::guid> gLinksList;
                    getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

                    // We will leave the old pos in place it looks cleaner
                    // m_pSelectedPod->m_Node.setPos( m_NewPos );

                    // TODO: Must delete all dependencies of the constant node
                    for( const auto& gLink : gLinksList )
                    {
                        const auto&     Link        = *m_LinkDB[ gLink ];
                        const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                        auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
                        auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

                        Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, Blueprint.getGuid().m_Value );
                    }

                    // Set the new constant data property pointer
                    auto& GraphNode         = m_pSelectedPod->m_Node.SafeCast<graph_node>();
                    GraphNode.m_pProperty   = &Blueprint;
                        
                    // Make the inspector to look at this constant data
                    SingleSelect( GraphNode.getGuid() );
                    m_Inspector.SetProperty( &GraphNode );//.m_pProperty );
                }
                else
                {
                    if( m_BlueprintList.isCreated() == false )
                    {
                        if( m_pSelectedPod->getGuid().m_Value == m_pSelectedPod->m_UserData )
                            return;
                    }

                    // Delete any links connected to this pod
                    xvector<imgui_node_graph::link::guid> ExistingLinks;
                    getLinksConnectingFromPod( ExistingLinks, m_pSelectedPod->getGuid() );
                    for( auto gLink : ExistingLinks )
                    {
                        DeleteLink( gLink );
                    }

                    // We need to create a node for this constant data
                    AddBlueprintNode
                    (
                        m_pSelectedPod->m_Node.getGuid()
                        , m_pSelectedPod->m_Guid
                        , Blueprint.getGuid()
                        , m_NewPos
                    );

                    // Also update the actual delegate
                    m_pSelectedPod->m_Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( m_pSelectedPod->m_Name, Blueprint.getGuid().m_Value );
                }

                /*
                if( m_BlueprintList.isCreated() )
                {
                }
                else
                {
                
                }
                */
            }
            
/*
            {
                // Any input for a constant data can only be one
                xvector<imgui_node_graph::link::guid> gLinksList;
                getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );
                x_assert( gLinksList.getCount() < 2 );
                bool bCreateNewContant = true;
                    
                for( const auto& gLink : gLinksList )
                {
                    const auto&     Link        = *m_LinkDB[ gLink ];
                    const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                    auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
                    auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

                    // If the user is asking us to create the same constant data then we are going to bail out
                    if( Node.SafeCast<graph_node>().m_pProperty->SafeCast<gb_blueprint::master>().getGuid() == Blueprint.getGuid() )
                    {
                        bCreateNewContant = false;
                        break;
                    }

                    // Delete the link
                    m_LinkDB.erase( gLink );

                    // If there is no other node that uses the constant data then nuke it
                    xvector<imgui_node_graph::link::guid> gOtherLinksList;
                    getLinksConnectingFromPod( gOtherLinksList, OtherPod.getGuid() );

                    if( gOtherLinksList.getCount() == 0 )
                    {
                        m_NodeDB.erase( Node.getGuid() );
                    }
                }

                // If it has a link then Change the constant node with new property and new pos
                // else create a constant node assign new properties and new pos and create a new link
                if( bCreateNewContant )
                {
                    auto& NodeGraph = m_pSelectedPod->m_Node.SafeCast<graph_node>();
                    NodeGraph.m_pProperty->SetProperty( m_pSelectedPod->m_Name, Blueprint.getGuid().m_Value );

                    // We need to create a node for this constant data
                    AddBlueprint
                    (
                        Blueprint.getGuid(),
                        Blueprint.getType().getGuid(),
                        NodeGraph.getGuid(),
                        m_pSelectedPod->getGuid(),
                        m_NewPos
                    );
                }
            }
            */
        }

        //-------------------------------------------------------------------------------------------------

        void HandleEntityRefSelector( void )
        {
            if( m_EntityRefList.Render() == false ) return;
            auto& Entity = m_EntityRefList.getSelected();

            if( m_pSelectedPod->m_bIsInput )
            {
                //... This pod could have many outputs

                // Find which nodes are connected to this constant data node and change their constant property
                // Change the current constant node with new properties
                xvector<imgui_node_graph::link::guid> gLinksList;
                getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

                // We will leave the old pos in place it looks cleaner
                // m_pSelectedPod->m_Node.setPos( m_NewPos );

                // TODO: Must delete all dependencies of the constant node
                for( const auto& gLink : gLinksList )
                {
                    const auto&     Link        = *m_LinkDB[ gLink ];
                    const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                    auto&           Node        = *m_NodeDB[ Link.m_ConnectionHooks[iOther].m_gNode ];
                    auto&           OtherPod    = Node.getPod( Link.m_ConnectionHooks[iOther].m_gPod );

                    Node.SafeCast<graph_node>().m_pProperty->SetProperty( OtherPod.m_Name, Entity.getGuid().m_Value );
                }

                // Set the new constant data property pointer
                auto& GraphNode         = m_pSelectedPod->m_Node.SafeCast<graph_node>();
                GraphNode.m_pProperty   = Entity.getLinearPropInterface();
                        
                // Make the inspector to look at this constant data
                SingleSelect( GraphNode.getGuid() );
                m_Inspector.SetProperty( &GraphNode );//.m_pProperty );
            }
            else
            {
                if( m_EntityRefList.isCreated() == false )
                {
                    if( m_pSelectedPod->getGuid().m_Value == m_pSelectedPod->m_UserData )
                        return;
                }

                // Delete any links connected to this pod
                xvector<imgui_node_graph::link::guid> ExistingLinks;
                getLinksConnectingFromPod( ExistingLinks, m_pSelectedPod->getGuid() );
                for( auto gLink : ExistingLinks )
                {
                    DeleteLink( gLink );
                }

                // We need to create a node for this constant data
                AddEntityRefNode
                (
                    m_pSelectedPod->m_Node.getGuid()
                    , m_pSelectedPod->m_Guid
                    , Entity.getGuid()
                    , m_NewPos
                );

                // Also update the actual delegate
                m_pSelectedPod->m_Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( m_pSelectedPod->m_Name, Entity.getGuid().m_Value );
            }
        }

        //-------------------------------------------------------------------------------------------------
        
        void HandleEntitySelector( void )
        {
            if( m_pSelectedPod->m_bIsInput )
            {
                //
                // TODO: Here the user wants us to replaze the entity however doing so is complex since we will have to update all the components as well
                //       So right now I will hack it as deletion of the entity with its dependencies

                // The properties should be the properties of the entity
                x_assert( m_pSelectedPod->m_Node.SafeCast<graph_node>().m_pProperty->isKindOf<graph_window>() == false );

                const auto Pos  = m_pSelectedPod->m_Node.getPos();
                const auto Guid = m_pEntity->getGuid();

                // If there is anything currently connected we must delete all connections
                {
                    xvector<imgui_node_graph::link::guid> gLinksList;
                    getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

                    x_assert( gLinksList.getCount() == 1 );
                    {
                        const auto&     Link        = *m_LinkDB[ gLinksList[0] ];
                        const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                        const auto      gOtherNode  = Link.m_ConnectionHooks[iOther].m_gNode;

                        x_assert( Link.m_ConnectionHooks[1 - iOther].m_gNode == m_pSelectedPod->m_Node.getGuid() );
                        x_assert( Link.m_ConnectionHooks[1 - iOther].m_gPod  == m_pSelectedPod->m_Guid           );
                        x_assert( m_NodeDB.find( gOtherNode ) != m_NodeDB.end() );

                        // Delete the link to avoid 
                        m_LinkDB.erase( gLinksList[0] );
                        DeleteNodeWithDependencies( m_pSelectedPod->m_Node.getGuid() );
                    }
                }

                //
                const gb_component::entity::guid                    gEntityGuid          { Guid };//gb_component::entity::guid::RESET };
                const imgui_node_graph::node::guid                  gEntityNodeGUID      { imgui_node_graph::node::guid::RESET };
                const imgui_node_graph::node::connection_pod::guid  gEntityEntityPod     { imgui_node_graph::node::connection_pod::guid::RESET };
                const imgui_node_graph::node::connection_pod::guid  gEntityComponentPod  { imgui_node_graph::node::connection_pod::guid::RESET };
                const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod  { imgui_node_graph::node::connection_pod::guid::RESET };
                const bool                                          inWorld              = m_pEntity->isInWorld();
                m_pEntity->linearDestroy();
                m_pEntity = &m_GameMgr.CreateEntity( m_GameMgr.m_CompTypeDB.getEntry( m_ComponentList.getSelectedGuid() ), gEntityGuid );
                AddEntity
                (
                    gEntityNodeGUID
                    , m_pGraphNode->getGuid()
                    , m_pGraphNode->getPod(0).getGuid()
                    , gEntityComponentPod
                    , gEntityEntityPod
                    , gEntityBlueprintPod
                    , m_pEntity
                    , Pos 
                );
                if( inWorld ) m_pEntity->linearAddToWorld();

                m_Selected.DeleteAllEntries();
                m_Selected.append( gEntityNodeGUID );
            }
            else
            {
                //
                // Are we dealing with the Game Node?
                //
                if( m_pSelectedPod->m_Node.SafeCast<graph_node>().m_pProperty->isKindOf<graph_window>() == true )
                {
                    // If there is anything currently connected we must delete all connections
                    {
                        xvector<imgui_node_graph::link::guid> gLinksList;
                        getLinksConnectingFromPod( gLinksList, m_pSelectedPod->getGuid() );

                        if( gLinksList.getCount() )
                        {
                            x_assert( gLinksList.getCount() == 1 );
                            {
                                const auto&     Link        = *m_LinkDB[ gLinksList[0] ];
                                const auto      iOther      = m_pSelectedPod->m_Guid == Link.m_ConnectionHooks[0].m_gPod ? 1 : 0;
                                const auto      gOtherNode  = Link.m_ConnectionHooks[iOther].m_gNode;

                                x_assert( Link.m_ConnectionHooks[1 - iOther].m_gNode == m_pSelectedPod->m_Node.getGuid() );
                                x_assert( Link.m_ConnectionHooks[1 - iOther].m_gPod  == m_pSelectedPod->m_Guid           );
                                x_assert( m_NodeDB.find( gOtherNode ) != m_NodeDB.end() );

                                // Delete the link to avoid 
                                m_LinkDB.erase( gLinksList[0] );
                                DeleteNodeWithDependencies( gOtherNode );
                            }
                        }
                    }

                    //
                    const gb_component::entity::guid                    gEntityGuid          { gb_component::entity::guid::RESET };
                    const imgui_node_graph::node::guid                  gEntityNodeGUID      { imgui_node_graph::node::guid::RESET };
                    const imgui_node_graph::node::connection_pod::guid  gEntityEntityPod     { imgui_node_graph::node::connection_pod::guid::RESET };
                    const imgui_node_graph::node::connection_pod::guid  gEntityComponentPod  { imgui_node_graph::node::connection_pod::guid::RESET };
                    const imgui_node_graph::node::connection_pod::guid  gEntityBlueprintPod  { imgui_node_graph::node::connection_pod::guid::RESET };
                    const bool                                          inWorld              = m_pEntity?m_pEntity->isInWorld():false;
                    if( m_pEntity ) m_pEntity->linearDestroy();
                    m_pEntity = &m_GameMgr.CreateEntity( m_GameMgr.m_CompTypeDB.getEntry( m_ComponentList.getSelectedGuid() ), gEntityGuid );
                    AddEntity
                    (
                        gEntityNodeGUID
                        , m_pGraphNode->getGuid()
                        , m_pGraphNode->getPod(0).getGuid()
                        , gEntityComponentPod
                        , gEntityEntityPod
                        , gEntityBlueprintPod
                        , m_pEntity
                        , m_NewPos 
                    );
                    if( inWorld ) m_pEntity->linearAddToWorld();

                    m_Selected.DeleteAllEntries();
                    m_Selected.append( gEntityNodeGUID );
                }

            }
        }

        //-------------------------------------------------------------------------------------------------
        
        void HandleComponentSelector( void )
        {
            if( m_pSelectedPod->m_bIsInput )
            {
            }
            else
            {
                if( m_pSelectedPod->m_Node.SafeCast<graph_node>().m_pProperty->isKindOf<gb_component::entity>() == true )
                {
                    AddComponent(
                        m_pSelectedPod->m_Node.getGuid(),
                        m_pSelectedPod->getGuid(),
                        &m_GameMgr.CreateComponent( m_GameMgr.m_CompTypeDB.getEntry( m_ComponentList.getSelectedGuid() ), *m_pEntity ),//->SafeCast<gb_component::entity>() ),
                        m_NewPos );

                }
            }

            //m_ComponentList.getSelectedGuid();
        }


        //-------------------------------------------------------------------------------------------------

        void ChooseComponentSelector( void )
        {
            if( m_ComponentList.Render() == false ) return;

            x_assert( m_ComponentList.getSelectedGuid().m_Value );
            if( m_ComponentList.isEntityMode() )
            {
                HandleEntitySelector();
            }
            else if( m_ComponentList.isComponentMode() )
            {
                HandleComponentSelector();
            }
        }

        //-------------------------------------------------------------------------------------------------

        void onRender( void ) noexcept override
        {
            const imgui_node_graph::node::guid LastSelected = ( m_Selected.getCount() >= 1 ) ? m_Selected[0] : imgui_node_graph::node::guid{ nullptr };

            //
            // Let the parent deal with all the work
            //
            t_parent::onRender();

            //
            // Deal with opening new selectors
            //
            switch( m_OpenSelector )
            {
                case open_selector::ENTITY:
                case open_selector::COMPONENT:
                {
                    m_ComponentList.Open();
                    m_ComponentList.Refresh( m_GameMgr );
                    break;
                }
                case open_selector::CONSTANT_DATA:
                {
                    m_ConstantDataList.Open();
                    break;
                }
                case open_selector::BLUEPRINT:
                {
                    m_BlueprintList.Open( editor_windows::blueprint_selector::mode::NORMAL );
                    break;
                }
                case open_selector::ENTITY_REF:
                {
                    m_EntityRefList.Open( editor_windows::entity_ref_selector::mode::NORMAL );
                    break;
                }
                case open_selector::BLUEPRINT_CREATE:
                {
                    m_BlueprintList.Open( editor_windows::blueprint_selector::mode::CREATE_ONLY );
                    break;
                }
            }
            m_OpenSelector = open_selector::NONE;

            //
            // Handle selectors
            //
            HandleConstantDataSelector();
            ChooseComponentSelector();
            HandleBlueprintSelector();
            HandleEntityRefSelector();

            //
            // Set property into inspector
            //
            if( m_Selected.getCount() == 1 )
            {
                if( LastSelected != m_Selected[0] )
                {
                    imgui_node_graph::node* pNode = m_NodeDB[ m_Selected[0] ];
                    if( pNode )
                    {
                        graph_node& GraphNode = pNode->SafeCast<graph_node>();
                        m_Inspector.SetProperty( &GraphNode );
                    }
                }
            }
        }

        //-------------------------------------------------------------------------------------------------

        virtual void onDeleteComponent( const imgui_node_graph::node& Node ) override
        {
            if( x_strcmp<xchar>( "Component", Node.getName()) == 0 )
            {
                auto& GraphNode = Node.SafeCast<graph_node>();
                auto& BaseNode  = GraphNode.m_pProperty->SafeCast<gb_component::base>();
                m_pEntity->linearDestroyComponent( BaseNode );
            }
            else if( Node.isKindOf<blue_print_node>() )
            {
                m_pEntity->setupBlueprintGuid( gb_blueprint::master::guid{ nullptr } );
            }
        }

        //-------------------------------------------------------------------------------------------------

        virtual void onDeleteLink( const imgui_node_graph::link& Link ) override 
        {
            auto& Node                  = *m_NodeDB[ Link.m_ConnectionHooks[1].m_gNode ];
            auto& Pod                   = Node.getPod( Link.m_ConnectionHooks[1].m_gPod );
            auto& DelegateGraphNode     = Node.SafeCast<graph_node>();

            types_info::guid gTypeInfo;
            if( m_GuidToClassGuid.cpGetOrFailEntry( Pod.m_gType.m_Value, [&]( types_info::guid& Entry ){ gTypeInfo = types_info::s_ConstantDataClassGuid; }) )
            {
                if( gTypeInfo == types_info::s_ConstantDataClassGuid )
                {
                    Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( Pod.m_Name, 0 );
                }
            }
            else if(    gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::event_type::t_class_guid 
                    ||  gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::interface_type::t_class_guid )
            {
                auto& DelegateComponent     = DelegateGraphNode.m_pProperty->SafeCast<gb_component::base>();
                DelegateGraphNode.m_pProperty->SetProperty( xstring::Make( "Delegates/%s", Pod.m_Name ), u64{0} );
                DelegateComponent.getEntity().linearResolve();
            }
            else if(    Pod.m_gType == types_info::s_BlueprintCreateClassGuid
                    ||  Pod.m_gType == types_info::s_BlueprintClassGuid
                    ||  Pod.m_gType == types_info::s_EntityClassGuidRef )
            {
                 DelegateGraphNode.m_pProperty->SetProperty<u64>( Pod.m_Name, 0 );
            }
        }

        //-------------------------------------------------------------------------------------------------

        virtual void onNotifyNewLinkConnection( const imgui_node_graph::link& Link ) override 
        {
            auto& Node = *m_NodeDB[ Link.m_ConnectionHooks[1].m_gNode ];
            auto& Pod  = Node.getPod( Link.m_ConnectionHooks[1].m_gPod );

            types_info::guid gTypeInfo;
            if( m_GuidToClassGuid.cpGetOrFailEntry( Pod.m_gType.m_Value, [&]( types_info::guid& Entry ){ gTypeInfo = types_info::s_ConstantDataClassGuid; }) )
            {
                if( gTypeInfo == types_info::s_ConstantDataClassGuid )
                {
                    // Delete any links connected to this pod
                    xvector<imgui_node_graph::link::guid> ExistingLinks;
                    getLinksConnectingFromPod( ExistingLinks, Pod.getGuid() );
                    for( auto gLink : ExistingLinks )
                    {
                        DeleteLink( gLink );
                    }

                    // Set the new guid into the constant property
                    auto& ConstantNode      = *m_NodeDB[ Link.m_ConnectionHooks[0].m_gNode ];
                    auto& ConstantGraphNode = ConstantNode.SafeCast<graph_node>();

                    Node.SafeCast<graph_node>().m_pProperty->SetProperty<u64>( Pod.m_Name, ConstantGraphNode.m_InstanceGuid );
                }
            }
            else if(    gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::event_type::t_class_guid 
                    ||  gb_component::event_type_base::guid{ Pod.m_gType.m_Value }.getClassGuid() == gb_component::interface_type::t_class_guid )
            {
                auto&       EventNode           = *m_NodeDB[ Link.m_ConnectionHooks[0].m_gNode ];
                auto&       EventPod            = EventNode.getPod( Link.m_ConnectionHooks[0].m_gPod );
                auto&       DelegateGraphNode   = Node.SafeCast<graph_node>();

                // Delete any links connected to this pod
                xvector<imgui_node_graph::link::guid> ExistingLinks;
                getLinksConnectingFromPod( ExistingLinks, Pod.getGuid() );
                for( auto gLink : ExistingLinks )
                {
                    DeleteLink( gLink );
                }

                // Update the pod user date with the new event guid
                Pod.m_UserData = EventPod.m_UserData;

                // Also update the actual delegate
                DelegateGraphNode.m_pProperty->SetProperty<u64>( xstring::Make( "Delegates/%s", Pod.m_Name ), EventPod.m_UserData );
                auto& DelegateComponent = DelegateGraphNode.m_pProperty->SafeCast<gb_component::base>();
                
                DelegateComponent.getEntity().linearResolve();
            }
            else if(    Pod.m_gType == types_info::s_BlueprintCreateClassGuid
                    ||  Pod.m_gType == types_info::s_BlueprintClassGuid )
            {
                auto&       EventNode           = *m_NodeDB[ Link.m_ConnectionHooks[0].m_gNode ];
                auto&       EventPod            = EventNode.getPod( Link.m_ConnectionHooks[0].m_gPod );
                auto&       DelegateGraphNode   = Node.SafeCast<graph_node>();

                // Delete any links connected to this pod
                xvector<imgui_node_graph::link::guid> ExistingLinks;
                getLinksConnectingFromPod( ExistingLinks, Pod.getGuid() );
                for( auto gLink : ExistingLinks )
                {
                    DeleteLink( gLink );
                }

                // Also update the actual delegate
                DelegateGraphNode.m_pProperty->SetProperty<u64>( Pod.m_Name, EventPod.m_UserData );
            }
        }

        //-------------------------------------------------------------------------------------------------

        virtual bool onLinkDrop( imgui_node_graph::node::connection_pod& Pod, ImVec2 Pos ) override
        {
            types_info::guid gType;
            if( ( Pod.m_gType.m_Value >> 32) == gb_component::const_data::type::t_class_guid.m_Value )
            {
                gType = types_info::s_ConstantDataClassGuid;
            }
            else if( u32( Pod.m_gType.m_Value) == types_info::s_BlueprintClassGuid.m_Value )
            {
                gType = types_info::s_BlueprintClassGuid;
            }
            else if( u32( Pod.m_gType.m_Value) == types_info::s_BlueprintCreateClassGuid.m_Value )
            {
                gType = types_info::s_BlueprintCreateClassGuid;
            }
            else if( u32( Pod.m_gType.m_Value) == types_info::s_EntityClassGuidRef.m_Value )
            {
                gType = types_info::s_EntityClassGuidRef;
            }
            else if( m_GuidToClassGuid.cpGetOrFailEntry( Pod.m_gType.m_Value, [&]( types_info::guid& Entry )
            {
                gType = Entry;
            }) == false ) return false;

            m_pSelectedPod  = &Pod;
            m_NewPos        = ( Pos - ImGui::GetWindowPos() ) / m_Scale - m_CurrentPosition ;
            switch( gType.m_Value )
            {
                case types_info::s_EntityClassGuid.m_Value:
                {
                    m_OpenSelector = open_selector::ENTITY;
                    m_ComponentList.EntityMode();
                    break;
                }
                case types_info::s_GameGuid.m_Value:
                {
                    break;
                }
                case types_info::s_ComponentClassGuid.m_Value:
                {
                    m_OpenSelector = open_selector::COMPONENT;
                    m_ComponentList.ComponentMode();
                    break;
                }
                case types_info::s_ConstantDataClassGuid.m_Value:
                {
                    m_OpenSelector = open_selector::CONSTANT_DATA;
                    m_ConstantDataList.Refresh( m_GameMgr, gb_component::const_data::type::guid{ Pod.m_gType.m_Value } );
                    break;
                }
                case types_info::s_BlueprintClassGuid.m_Value:
                {
                    m_OpenSelector = open_selector::BLUEPRINT;
                    m_BlueprintList.Refresh( m_GameMgr );
                    break;
                }
                case types_info::s_EntityClassGuidRef.m_Value:
                {
                    m_OpenSelector = open_selector::ENTITY_REF;
                    m_EntityRefList.Refresh( m_GameMgr );
                    break;
                }
                case types_info::s_BlueprintCreateClassGuid.m_Value:
                {
                    // We can only create entries if we are not editing the blueprint
                    if( m_pBlueprint == nullptr )
                    {
                        m_OpenSelector = open_selector::BLUEPRINT_CREATE;
                        m_BlueprintList.Refresh( m_GameMgr );
                    }
                    break;
                }
                default:
                {
                    x_assume( false );
                    break;
                }
            }

            return false;
        }

    protected:

        using hash_type_to_class = x_ll_mrmw_hash< types_info::guid, u64 >;

        gb_game_graph::base&                    m_GameMgr;
        gb_blueprint::master*                   m_pBlueprint            { nullptr };
        gb_component::entity*                   m_pEntity               { nullptr };
        imgui_node_graph::node::guid            m_gEntity               { nullptr };
        inspector_window                        m_Inspector             { };
        editor_windows::component_selector      m_ComponentList         { };
        editor_windows::constant_data_selector  m_ConstantDataList;
        editor_windows::blueprint_selector      m_BlueprintList;
        editor_windows::entity_ref_selector     m_EntityRefList;
        open_selector                           m_OpenSelector          { open_selector::NONE };
        hash_type_to_class                      m_GuidToClassGuid       { };
        ImVec2                                  m_NewPos                { };
        imgui_node_graph::node::connection_pod* m_pSelectedPod          { nullptr };
        graph_node*                             m_pGraphNode            { nullptr };
        ImVec2                                  m_GameNodeStartingPos   {};

        friend inspector_window::entity_item;
    };

    //---------------------------------------------------------------------------------------------------------

    void inspector_window::entity_item::onGadgetRender( void )
    {
        ImGui::AlignFirstTextHeightToWidgets();
        auto&                   Win         =   m_Window.SafeCast<inspector_window>();

        xproperty_v2::base* pProp = nullptr;
        if( Win.m_pProperty->isKindOf<graph_window::graph_node>() )
        {
            pProp = Win.m_pProperty->SafeCast<graph_window::graph_node>().m_pProperty;
            if( pProp->isKindOf<gb_component::base>() == false )
                pProp = nullptr;
        }
        else if( Win.m_pProperty->isKindOf<gb_component::base>() )
        {
            pProp = Win.m_pProperty;
            x_assert(pProp->isKindOf<gb_component::base>());
        }
                
        if( pProp )
        {
            const int               iTopFolder    = Win.getTopFolderIndex( this );
            auto&                   BaseComponent = pProp->SafeCast<gb_component::base>();
            auto&                   Entity        = BaseComponent.getEntity();
            gb_component::base&     Comp          = [&]() -> gb_component::base& 
                                                    {
                                                        gb_component::base* pComp       = nullptr;
                                                        int                 i           = 0;
                                                        
                                                        x_assert( iTopFolder != -1 );
                                                        for( pComp = &Entity; i != iTopFolder; pComp = pComp->getNextComponent(), i++ )
                                                        {
                                                            x_assert(pComp);
                                                        }

                                                        return *pComp;
                                                    }();

            const bool isEntity = iTopFolder == 0 && BaseComponent.isKindOf<gb_component::entity>() ;

                // Filter out: If we are a component only the first folder should be mark as such
            if( iTopFolder != 0 && BaseComponent.isKindOf<gb_component::entity>() == false )
                return;



            const ImGuiStyle *  style               = &ImGui::GetStyle();
            ImGui::PushStyleColor(ImGuiCol_Button, style->Colors[ImGuiCol_Header]);

            //
            // Deal with the active button
            //
            bool bActive = Comp.isActive();
            bool Press;

            if( m_BackupHelp.isEmpty() )
            {
                m_BackupHelp = m_Help;
            }
            m_Help = X_STR("");
            if( bActive ) 
            {
                Press = ImGui::Button( ICON_FA_CHECK_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
                if (ImGui::IsItemHovered())
                {
                    /*
                    ImGui::BeginTooltip();
                    ImGui::PushTextWrapPos(450.0f);
                    ImGui::TextUnformatted("Activated Component\nComponent will be executed when the game runs");
                    ImGui::PopTextWrapPos();
                    ImGui::EndTooltip();
                    */
                    m_Window.UpdateSelected ( this );
                    m_Help.Copy("Activated Component\nComponent will be executed when the game runs");
                }
            }
            else 
            {
                Press = ImGui::Button( ICON_FA_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
                if (ImGui::IsItemHovered())
                {
                    /*
                    ImGui::BeginTooltip();
                    ImGui::PushTextWrapPos(450.0f);
                    ImGui::TextUnformatted("Deactivated Component\nComponent does not execute when the game runs");
                    ImGui::PopTextWrapPos();
                    ImGui::EndTooltip();
                    */
                    m_Window.UpdateSelected ( this );
                    m_Help.Copy("Deactivated Component\nComponent does not execute when the game runs");
                }
            }

            //
            // Deal with the readiness of a component
            //
            xvector<xstring> WarningList;
            auto Error = Comp.linearCheckResolve( WarningList );
            if( Error.isError() ) ImGui::TextColored( ImColor(170,0,0,255), "Error" ); 
            else                  ImGui::TextColored( ImColor(0,255,0,255), "R" );
            ImGui::SameLine(); 
            if (ImGui::IsItemHovered())
            {
                m_Window.UpdateSelected ( this );
                if( Error.isError() )
                {
                    m_Help.Copy( Error.getString() );
                }
                else
                {
                    m_Help.Copy("Ready Component\nThis component has all the requirements needed to work property");
                }
            }

            //
            // Component menu
            //
            if( Press )  Comp.setActive( !bActive );
            ImGui::SameLine();

            //
            // Deal with the menu button
            //
            ImGui::PushItemWidth(-1);
            const char* pDisplay = "Component";
            if( isEntity ) pDisplay = "Entity"; 
            ImGui::Button( pDisplay, ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) ); 

            ImGui::PopStyleColor();
            ImGui::PopItemWidth();

            if( ImGui::IsItemActive() )
                ImGui::OpenPopup("##piepopup");

            const char* Entityitems[]    = { "Paste", "Copy", "Paste New", "Add New", "Delete"  };
            const char* Componentitems[] = { "Paste", "Copy", "Move Down", "Move Up", "Delete"  };
            static int selected = -1;

            const ImVec2 Pos = ImGui::GetIO().MouseClickedPos[0];
            int n = -1;
            if( isEntity )   n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Entityitems, static_cast<int>(x_countof(Entityitems)), &selected );
            else             n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Componentitems, static_cast<int>(x_countof(Componentitems)), &selected );


            if( m_Help[0] == 0 ) m_Help = m_BackupHelp;
        }
    }
}

