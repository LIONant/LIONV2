//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class view 
{
public:
    


public:

    x_incppfile                                     view                ( gbed_blueprint::document& Document );
    x_incppfile             void                    Render              ( gbed_selection::document* pSelectionDoc );
    x_inline                auto&                   getDocument         ( void ) { return m_Document; }
    x_incppfile             auto                    getNewSelected      ( void ) { return m_pSelected; }
    x_incppfile             auto                    isCreated           ( void ) const { return m_bCreated; }
    x_inline                auto                    getAndClearSelected ( void ) { auto t = m_pSelected; m_pSelected = nullptr; return t; }

protected:

    enum class open_popup : u8
    {
        NONE
        , MAIN_MENU
        , FOLDER_MENU
        , ENTRY_MENU
        , FOLDER_RENAME
        , FOLDER_NEW
        , FOLDER_DELETE
        , FOLDER_MOVE
        , ENTRY_CREATE
    };

    struct popup_params
    {
        document::folder::guid                      m_gFolder;
        document::edblueprint::guid                 m_gEntry;
        xarray<char,128>                            m_EditField;
    };

protected:

    document&                   m_Document;
    ImGuiTextFilter             m_Filter                    {};
    bool                        m_bShowNoneditorBlueprints  { false };
    popup_params                m_Params                    {};
    gb_blueprint::master*       m_pSelected                 { nullptr };
    bool                        m_bCreated                  { false }; 
};

