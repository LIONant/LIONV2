//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------

static const gbed_document::base::type_harness<document> s_Type{ document::t_class_string, -2 };    

//-------------------------------------------------------------------------------------------

const gbed_document::base::type& document::getType( void ) 
{ 
    return s_Type;  
}

//-------------------------------------------------------------------------------------------

void document::folder::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "Blueprint_Folder" );
    File.WriteField ( "Guid:g", m_Guid );
    File.WriteField ( "Name:s", m_Name );
    File.WriteField ( "gParent:g", m_gParent );
    File.WriteLine();

    File.WriteRecord( "Folder", m_lgFolder.getCount<int>() );
    for( auto& gFolder : m_lgFolder )
    {
        File.WriteField ( "Guid:g", gFolder );
        File.WriteLine();
    }

    File.WriteRecord( "gEDBlueprint", m_lgEDBlueprints.getCount<int>() );
    for( auto& gEDBlueprint : m_lgEDBlueprints )
    {
        File.WriteField ( "Guid:g", gEDBlueprint );
        File.WriteLine();
    }
}

//-------------------------------------------------------------------------------------------

void document::folder::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("Blueprint_Folder") )
        return;

    File.ReadLine();
    File.ReadField          ( "Guid:g", &m_Guid );
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadField          ( "gParent:g", &m_gParent );
    if( File.ReadRecord() == false ) return;

    //
    // Read folders
    //
    if( File.getRecordName() == X_STR("gFolders" ) )
    {
        m_lgFolder.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgFolder.append();
                File.ReadField( "Guid:g", &E );
            } 
        }

        if( File.ReadRecord() == false ) return;
    }
        
    //
    // Read Blueprint
    //
    if( File.getRecordName() == X_STR("gEDBlueprint" ) )
    {
        m_lgEDBlueprints.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgEDBlueprints.append();
                File.ReadField( "Guid:g", &E );
            } 
        }

        if( File.ReadRecord() == false ) return;
    }

    //
    // We just loaded so nothing has change
    //
    m_bChanged = false;
}

//-------------------------------------------------------------------------------------------

void document::edblueprint::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "EDBlueprint" );
    File.WriteField ( "Guid:g", m_Guid );
    File.WriteField ( "Name:s", m_Name );
    File.WriteField ( "gFolder:g", m_gFolder );
    File.WriteField ( "gBlueprint:g", m_gBlueprint );
    File.WriteLine();

    //
    // Save the reference table
    //
    File.WriteRecord( "References", m_RefDBase.getCount<int>() );
    for( const auto& E: m_RefDBase )
    {
        File.WriteField ( "gEntity:g", E );
        File.WriteLine();
    }
}

//-------------------------------------------------------------------------------------------
    
void document::edblueprint::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("EDBlueprint") )
        return;

    File.ReadLine();
    File.ReadField          ( "Guid:g", &m_Guid );
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadField          ( "gFolder:g", &m_gFolder );
    File.ReadField          ( "gBlueprint:g", &m_gBlueprint );

    //
    // Read reference Table
    //
    m_RefDBase.DeleteAllEntries();
    if( File.ReadRecord() && File.getRecordName() == X_STR("References") )
    {
        m_RefDBase.appendList( File.getRecordCount() );
        for( const auto& E: m_RefDBase )
        {
            File.ReadLine();
            File.ReadField ( "gEntity:g", &E );
        }
    }

    //
    // We just loaded so nothing has change
    //
    m_bChanged = false;

    //
    // todo: We are suposed to load properties here?
    //
    if( File.ReadRecord() == false ) return;

}

//-------------------------------------------------------------------------------------------

document::document( const xstring::const_str Str, gbed_document::main& MainDoc ) 
    : gbed_document::base   { Str, MainDoc }
    , m_GameGraphDoc        { MainDoc.getSubDocument<gbed_game_graph::document>() }
{
    m_EDBlueprintDBase.Init( 1024 );
    m_FolderDBase.Init( 1024 );
    m_BlueprintEDBlueprint.Initialize( 1024 );

     m_deletegateChangeHappen.Connect( getMainDoc().m_Events.m_ChangeHappen );
}

//-------------------------------------------------------------------------------------------

document::folder& document::CreateFolder( folder::guid gFolderParent )
{
    //
    // Create editor blueprint
    //
    xndptr_s<folder> xpEntry;
    xpEntry.New();
    xpEntry->m_Guid.Reset();
    xpEntry->m_Name         = X_STR("New Folder");
    xpEntry->m_bChanged     = true;
    xpEntry->m_gParent      = gFolderParent;

    //
    // Register with the parent folder
    //
    auto& Folder = ( gFolderParent.m_Value == 0 ) ? m_FolderRoot : m_FolderDBase.getEntry( gFolderParent );
    Folder.m_lgFolder.append( xpEntry->m_Guid );
    Folder.m_bChanged = true;

    //
    // Transfer the ownership to the data base
    //
    auto& Ptr = *xpEntry;
    m_FolderDBase.RegisterAndTransferOwner( xpEntry );

    return Ptr;
}

//-------------------------------------------------------------------------------------------

document::edblueprint::guid document::findEDBlueprintGuid( gb_blueprint::guid Guid ) const
{
    edblueprint::guid EdGuid{nullptr};

    m_BlueprintEDBlueprint.cpGetOrFailEntry( Guid.m_Value
    , [&]( const edblueprint::guid& Entry )
    {
        EdGuid = Entry;
    });

    return EdGuid;
}

//-------------------------------------------------------------------------------------------
document::edblueprint* document::findEDBlueprint( gb_blueprint::guid Guid )
{
    const edblueprint::guid EdGuid = findEDBlueprintGuid(Guid);
    if( EdGuid.m_Value == 0 ) return nullptr;
    return &m_EDBlueprintDBase.getEntry( EdGuid );
}

//-------------------------------------------------------------------------------------------

document::edblueprint& document::CreateEDBlueprint( folder::guid gFolder, gb_blueprint::guid gBlueprint )
{
    //
    // Create the game blueprint
    //
    if( gBlueprint.m_Value == 0 )
    {
        auto& Graph     = m_GameGraphDoc.getGraph();
        auto& BLueprint = Graph.CreateBlueprint();
        auto& Entity    = Graph.CreateEntity<gb_component::entity>( gb_component::entity::guid::RESET );
        BLueprint.setup( Entity );
        Entity.linearDestroy();
        gBlueprint = BLueprint.getGuid();
    }

    //
    // Create editor blueprint
    //
    xndptr_s<edblueprint> xpEntry;
    xpEntry.New( m_GameGraphDoc );
    xpEntry->m_Guid.Reset();
    xpEntry->m_Name         = X_STR("New Blueprint");
    xpEntry->m_bChanged     = true;
    xpEntry->m_gFolder      = gFolder;
    xpEntry->m_gBlueprint   = gBlueprint;

    //
    // Register the blueprint in the lookup hash
    //
    m_BlueprintEDBlueprint.cpAddEntry( gBlueprint.m_Value
    , [&]( edblueprint::guid& Entry )
    {
        Entry = xpEntry->m_Guid;
    });

    //
    // Register with the parent folder
    //
    auto& Folder = ( gFolder.m_Value == 0 ) ? m_FolderRoot : m_FolderDBase.getEntry( gFolder );
    Folder.m_lgEDBlueprints.append( xpEntry->m_Guid );
    Folder.m_bChanged = true;

    //
    // Transfer the ownership to the data base
    //
    auto& Ptr = *xpEntry;
    m_EDBlueprintDBase.RegisterAndTransferOwner( xpEntry );

    return Ptr;
}

//-------------------------------------------------------------------------------------------

document::err document::onSave( void ) 
{ 
    xstring     EDBlueprintPath;
    xstring     BlueprintPath;

    auto& Graph = m_GameGraphDoc.getGraph();

    // Convert to the path first
    EDBlueprintPath.CopyAndConvert( &static_cast<std::wstring>(m_EDBlueprintPath)[0] );
    BlueprintPath.CopyAndConvert( &static_cast<std::wstring>(m_BlueprintPath)[0] );

    //
    // Save Game Wise blueprint
    //
    const auto SaveBlueprint = [&]( edblueprint& E ) -> document::err
    {
        xtextfile   File;
        xstring     FileName;
        xstring     FileRoot;

        // Create file name 
        E.m_gBlueprint.getStringHex( FileName );
        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
        // Create finally the full filename path
        FileRoot.setup( "%s/%s.b", BlueprintPath, FileName );

        if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
        {
            auto& BP = Graph.m_BlueprintDB.getEntry(E.m_gBlueprint);

            xproperty_v2::entry::RegisterTypes(File);
            BP.Save(File);
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }

        return x_error_ok(); 
    };

    //
    // Save EDBlueprint
    //
    const auto SaveEDBlueprint = [&]( edblueprint& E ) -> document::err
    {
        if( E.m_bChanged ) 
        {
            xtextfile   File;
            xstring     FileName;
            xstring     FileRoot;

            // Create file name 
            E.m_Guid.getStringHex( FileName );
            FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
            // Create finally the full filename path
            FileRoot.setup( "%s/%s.edblueprint.txt", EDBlueprintPath, FileName );

            if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
            {
                E.Save( File );
                if( auto Error = SaveBlueprint(E); Error.isError() )
                    return Error;
            }
            else
            {
                X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
            }
        }

        return x_error_ok(); 
    };

    //
    // Save Folder Children
    //
    const xfunction<document::err(folder&)> SaveFolderChildren = [&]( folder& Folder ) -> document::err
    {
        //
        // Save all the folders
        //
        for( const auto& gEntry : Folder.m_lgFolder )
        {
            auto& E = m_FolderDBase.getEntry( gEntry );
            if( E.m_bChanged ) 
            {
                xtextfile   File;
                xstring     FileName;
                xstring     FileRoot;

                // Create file name 
                E.m_Guid.getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                // Create finally the full filename path
                FileRoot.setup( "%s/%s.folder.txt", EDBlueprintPath, FileName );

                if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
                {
                    E.Save( File );
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                }
            }

            // Recurse the rest of the children
            if( auto Err = SaveFolderChildren( E ); Err.isError() ) return Err;
            E.m_bChanged = false;
        }

        //
        // Save all the EDBlueprint
        //
        for( const auto& gEDBlueprint : Folder.m_lgEDBlueprints )
        {
            auto& E = m_EDBlueprintDBase.getEntry( gEDBlueprint );

            // Save the zone
            if( auto Err = SaveEDBlueprint(E); Err.isError() ) return Err;
            E.m_bChanged = false;
        }

        return x_error_ok(); 
    };

    //
    // Save the layer
    //
    if( m_FolderRoot.m_bChanged )
    {
        xtextfile   File;
        xstring     FileRoot;

        // Create finally the full filename path
        FileRoot.setup( "%s/Root.folder.txt", EDBlueprintPath );

        if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
        {
            m_FolderRoot.Save( File );
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }
    }

    // Recurse the rest of the children
    if( auto Err = SaveFolderChildren( m_FolderRoot ); Err.isError() ) return Err;
    m_FolderRoot.m_bChanged = false;
 
    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------
    
document::err document::onClose ( void ) 
{ 
    //
    // Delete all the editor related game based entry data
    //
    if( m_GameGraphDoc.isGraphValid() )
    {
        x_lk_guard_as_const_get_as_const( m_EDBlueprintDBase.m_Vector, BlueprintVector );
        auto& GameGraph = m_GameGraphDoc.getGraph();
        for( auto& xpE : BlueprintVector )
        {
            GameGraph.m_BlueprintDB.Delete( xpE->m_pEntry->m_gBlueprint );
        }
    }

    m_EDBlueprintDBase.DeleteAllEntries();
    m_FolderDBase.DeleteAllEntries();
    m_BlueprintEDBlueprint.DeleteAllEntries();

    x_destruct ( &m_FolderRoot ); 
    x_construct( folder, &m_FolderRoot );             

    m_EDBlueprintPath.clear();
    m_BlueprintPath.clear();

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

void document::StartUp( void )
{
    m_EDBlueprintPath   = m_GameGraphDoc.getJITPath()       + std_fs::path("/EDBlueprints");
    m_BlueprintPath     = m_GameGraphDoc.getGameDataPath()  + std_fs::path("/Blueprints");

    // Set the default name for the root editor
    m_FolderRoot.m_Name = X_STR( "Editor Blueprints" );
}

//-------------------------------------------------------------------------------------------
    
document::err document::onLoad ( void ) 
{
    //
    // Setup all the basic stuff
    //
    StartUp();

    //
    // Setup the basic vars
    //
    xstring     EDBlueprintPath;
    xstring     BlueprintPath;
    auto&       Graph = m_GameGraphDoc.getGraph();

    // Convert to the path first
    EDBlueprintPath.CopyAndConvert( &static_cast<std::wstring>(m_EDBlueprintPath)[0] );
    BlueprintPath.CopyAndConvert( &static_cast<std::wstring>(m_BlueprintPath)[0] );

    //
    // Load Game Wise Blueprint
    //
    const auto LoadBlueprint = [&]( edblueprint& E ) -> document::err
    {
        xtextfile   File;
        xstring     FileName;
        xstring     FileRoot;

        // Create file name 
        E.m_gBlueprint.getStringHex( FileName );
        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
        // Create finally the full filename path
        FileRoot.setup( "%s/%s.b", BlueprintPath, FileName );

        if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
        {
            auto& BLueprint = Graph.CreateBlueprint( E.m_gBlueprint );
            BLueprint.Load( File, Graph );
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }

        return x_error_ok(); 
    };

    //
    // Load EDBlueprint
    //
    const auto LoadEDBlueprint = [&]( edblueprint& E ) -> document::err
    {
        xtextfile   File;
        xstring     FileName;
        xstring     FileRoot;

        // Create file name 
        E.m_Guid.getStringHex( FileName );
        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
        // Create finally the full filename path
        FileRoot.setup( "%s/%s.edblueprint.txt", EDBlueprintPath, FileName );

        if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
        {
            E.Load( File );

            if( auto Error = LoadBlueprint(E); Error.isError() )
                return Error;

            //
            // Register the blueprint in the lookup hash
            //
            m_BlueprintEDBlueprint.cpAddEntry( E.m_gBlueprint.m_Value
            , [&]( edblueprint::guid& Entry )
            {
                Entry = E.getGuid();
            });
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }

        return x_error_ok(); 
    };

    //
    // Load Folder Children
    //
    const xfunction<document::err(folder&)> LoadFolderChildren = [&]( folder& Folder ) -> document::err
    {
        //
        // Load all the folders
        //
        for( const auto& gEntry : Folder.m_lgFolder )
        {
            xndptr_s<folder> xpEntry;
            xpEntry.New();
            {
                auto&       E = *xpEntry;
                xtextfile   File;
                xstring     FileName;
                xstring     FileRoot;

                // Create file name 
                gEntry.getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                // Create finally the full filename path
                FileRoot.setup( "%s/%s.folder.txt", EDBlueprintPath, FileName );

                if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
                {
                    E.Load( File );
                    File.close();

                    if( auto Err = LoadFolderChildren( E ); Err.isError() ) return Err;
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                }
            }
            m_FolderDBase.RegisterAndTransferOwner( xpEntry );
        }

        //
        // Load all the EDBlueprint
        //
        for( const auto& gEDBlueprint : Folder.m_lgEDBlueprints )
        {
            xndptr_s<edblueprint> xpEntry;
            xpEntry.New( m_GameGraphDoc );
            
            xpEntry->m_Guid = gEDBlueprint;

            if( auto Err = LoadEDBlueprint( *xpEntry ); Err.isError() ) return Err;

            // Transfer the ownership to the data base
            m_EDBlueprintDBase.RegisterAndTransferOwner( xpEntry );

        }

        return x_error_ok(); 
    };

    //
    // Load Root
    //
    {
        xtextfile   File;
        xstring     FileRoot;

        // Create finally the full filename path
        FileRoot.setup( "%s/Root.folder.txt", EDBlueprintPath );

        if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
        {
            m_FolderRoot.Load( File );
            File.close();

            if( auto Err = LoadFolderChildren( m_FolderRoot ); Err.isError() ) return Err;
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }
    }

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

document::err document::onNew( void )
{
    // Setup all the basic stuff
    StartUp();

    // Create the actual directory
    if (auto Error = m_MainDoc.CreateDir(m_EDBlueprintPath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_BlueprintPath); Error.isError()) 
        return Error;

    //
    // Save the root of the tree
    //
    m_FolderRoot.m_bChanged = true;
    if( auto Error = onSave(); Error.isError() ) return Error;

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

void document::msgChangeHappen( xproperty_v2::base& Prop )
{
    if( Prop.isKindOf<edblueprint>() )
    {
        auto& EDBlueprint = Prop.SafeCast<edblueprint>();
        EDBlueprint.m_bChanged = true;
    }
}




