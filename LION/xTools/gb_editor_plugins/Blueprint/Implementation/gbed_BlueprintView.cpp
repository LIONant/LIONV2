//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

view::view( gbed_blueprint::document& Document )
    : m_Document            { Document }
{
}

//-------------------------------------------------------------------------------------------

void view::Render( gbed_selection::document* pSelectionDoc ) 
{
    m_bCreated  = false;
    m_pSelected = nullptr;

    open_popup  OpenWhichPopup          = open_popup::NONE;

    auto&       GameGraphDoc            = m_Document.getGameGraphDoc();
    auto&       RootFolder              = m_Document.getRootFolder();
    auto&       FolderDBase             = m_Document.getFolderDBase();
    auto&       BlueprintDBase          = m_Document.getEDBlueprintDBase();

    //
    // Folder Context Menu
    //
    auto FolderContextMenu = [&]( const document::folder& Folder )
    {
        ImGui::PushID( &Folder );
        if( ImGui::BeginPopupContextItem( "BlueprintView Folder Menu" ) )
        {
            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  Rename Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_RENAME;
                m_Params.m_gFolder = Folder.getGuid();
                x_strcpy<xchar>( m_Params.m_EditField, Folder.m_Name );
            }

            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  New Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_NEW;
                m_Params.m_gFolder = Folder.getGuid();
            }

            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  Delete Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_DELETE;
                m_Params.m_gFolder = Folder.getGuid();
            }

            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  Move Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_MOVE;
                m_Params.m_gFolder = Folder.getGuid();
            }

            ImGui::Separator();

            if( ImGui::MenuItem( ICON_FA_CODEPEN "  New Blueprint" ) )
            {
                OpenWhichPopup = open_popup::ENTRY_CREATE;
                m_Params.m_gFolder = Folder.getGuid();
            }

            ImGui::EndMenu();
        }
        ImGui::PopID();
    };

    //
    // Editor Entry Context Menu
    //
    auto EntryContextMenu = [&]( const document::edblueprint& Entry )
    {
        ImGui::PushID( &Entry );
        if( ImGui::BeginPopupContextItem( "BlueprintView Entry Menu" ) )
        {
            ImGui::Text( ICON_FA_LINE_CHART"  References: %d ", Entry.m_RefDBase.getCount<int>() );

            ImGui::Separator();

            if( Entry.m_RefDBase.getCount<int>() )
            {
                ImGui::TextDisabled( ICON_FA_CODEPEN "  Delete Blueprint" );
            }
            else
            {
                if( ImGui::MenuItem( ICON_FA_CODEPEN "  Delete Blueprint" ) )
                {
                    OpenWhichPopup = open_popup::FOLDER_RENAME;
                    m_Params.m_gEntry = Entry.getGuid();
                }
            }

            if( ImGui::MenuItem( ICON_FA_CODEPEN "  Move Blueprint" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_RENAME;
                m_Params.m_gEntry = Entry.getGuid();
            }

            ImGui::EndMenu();
        }
        ImGui::PopID();
    };

    //
    // ProcessFolderChildren
    //
    const std::function<void(gbed_blueprint::document::folder&)> ProcessFolderChildren = [&]( gbed_blueprint::document::folder& Folder )
    {
        if( &Folder != &RootFolder )
        {
            Folder.m_bExpanded = Folder.m_bExpanded ? ImGui::TreeNode( &Folder, ICON_FA_FOLDER_OPEN_O " %s", Folder.m_Name ) :
                                                      ImGui::TreeNode( &Folder, ICON_FA_FOLDER_O      " %s", Folder.m_Name );
        }

        FolderContextMenu( Folder );
        if ( Folder.m_bExpanded || &Folder == &RootFolder )
        {
            //
            // Deal with all the children first
            //
            for( auto gFolder : Folder.m_lgFolder )
            {
                auto& Z = FolderDBase.getEntry( gFolder );
                ProcessFolderChildren(Z);
            }

            //
            // Deal with additional folders
            //
            for( auto gBlueprint : Folder.m_lgEDBlueprints )
            {
                auto pEDBlueprint = BlueprintDBase.Find( gBlueprint );
                if( pEDBlueprint )
                {
                    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
                    if( pSelectionDoc )
                    {
                        for( auto gSel : pSelectionDoc->getSelectedBlueprints() )
                        {
                            if( gSel == pEDBlueprint->m_gBlueprint )
                            {
                                flags |= ImGuiTreeNodeFlags_Selected;
                                break;
                            }
                        }
                    }

                    // Create the label
                    xstring             StrGuid;
                    xarray<char,256>    label;
                    pEDBlueprint->m_gBlueprint.getStringHex(StrGuid);
                    if( pEDBlueprint->m_bChanged )
                    {
                        sprintf_s( label, label.getCount<int>(), ICON_FA_PENCIL_SQUARE_O" %s (%s)", static_cast<const char*>(pEDBlueprint->m_Name), static_cast<const char*>(StrGuid) );
                    }
                    else
                    {
                        sprintf_s( label, label.getCount<int>(), ICON_FA_CHECK_SQUARE_O" %s (%s)", static_cast<const char*>(pEDBlueprint->m_Name), static_cast<const char*>(StrGuid) );
                    }

                    // display entry
                    ImGui::TreeNodeEx( pEDBlueprint, flags, label );
                    if (ImGui::IsItemClicked()) 
                    {
                        m_pSelected = GameGraphDoc.getGraph().m_BlueprintDB.Find( pEDBlueprint->m_gBlueprint );
                        if( pSelectionDoc )
                        {
                            pSelectionDoc->Single( pEDBlueprint->m_gBlueprint, pEDBlueprint );
                        }
                    }
                    EntryContextMenu( *pEDBlueprint );
                }
                else
                {
                
                }
            }

            //
            // Done with folder
            //
            if( &Folder != &RootFolder )
            {
                ImGui::TreePop();
            }
        }
    };

    //
    // Search bar
    //
    if( ImGui::Button( ICON_FA_ARROW_CIRCLE_DOWN ) ) // "Menu"
    {
        OpenWhichPopup = open_popup::MAIN_MENU;
    }

    ImGui::SameLine(); if( ImGui::Button( ICON_FA_TIMES ) )
    {
        m_Filter.Clear();
    }
         
    ImGui::SameLine(); m_Filter.Draw(ICON_FA_SEARCH, -24.0f);

    //
    // Main Render
    //
    ImGui::BeginChild("blueprint view");
    {
        m_bShowNoneditorBlueprints = ImGui::TreeNode( this, ICON_FA_ANDROID " System Blueprints" );
        if( m_bShowNoneditorBlueprints )
        {
            xstring StrGuid;
            char    label[256];
            auto&   Graph         = m_Document.getGameGraphDoc().getGraph();

            x_lk_guard_as_const_get_as_const( Graph.m_BlueprintDB.m_Vector, BlueprintLinearVector );

            for( const auto& pEntry : BlueprintLinearVector )
            {
                auto& Blueprint     = *pEntry->m_pEntry;
                auto  gEDBlueprint  = m_Document.findEDBlueprintGuid( Blueprint.getGuid() ); 
                if( gEDBlueprint.m_Value )
                    continue;

                ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
                if( pSelectionDoc )
                {
                    for( auto gSel : pSelectionDoc->getSelectedBlueprints() )
                    {
                        if( gSel == Blueprint.getGuid() )
                        {
                            flags |= ImGuiTreeNodeFlags_Selected;
                            break;
                        }
                    }
                }                
                const char* pType = "Unkown Entity Type";
                if( auto p = Graph.m_CompTypeDB.Find( Blueprint.getBundle().m_EntityTypeGuid ); p ) pType = p->getCategoryName().m_Char.m_pValue;

                // Create the label
                Blueprint.getGuid().getStringHex(StrGuid);
                sprintf_s(label, ICON_FA_CHECK_SQUARE_O" %s (%s)", pType, static_cast<const char*>(StrGuid) );
                
                // display entry
                ImGui::TreeNodeEx( &Blueprint, flags, label );
                if (ImGui::IsItemClicked()) 
                {
                    m_pSelected = &Blueprint;
                    if( pSelectionDoc )
                    {
                        pSelectionDoc->Single( Blueprint.getGuid(), &Blueprint );
                    }
                }
            }

            ImGui::TreePop();
        }

        // Process all the folders
        ProcessFolderChildren( RootFolder );

        // Done
        ImGui::EndChild();
    }

    //
    // MAIN_MENU
    //
    if( OpenWhichPopup == open_popup::MAIN_MENU )
    {
        ImGui::OpenPopup( "MainMenuBlueprintView" );
    }

    if( ImGui::BeginPopup( "MainMenuBlueprintView" ) )
    {
        if( ImGui::MenuItem( ICON_FA_FOLDER_O "  New Folder" ) )
        {
            OpenWhichPopup = open_popup::FOLDER_NEW;
            m_Params.m_gFolder = RootFolder.getGuid();
        }

        ImGui::Separator();

        if( ImGui::MenuItem( ICON_FA_CODEPEN "  New Blueprint" ) )
        {
            OpenWhichPopup = open_popup::ENTRY_CREATE;
            m_Params.m_gFolder = RootFolder.getGuid();
        }

        ImGui::EndPopup();
    }


    //
    // FOLDER_NEW
    //
    if( OpenWhichPopup == open_popup::FOLDER_NEW )
    {
        m_Params.m_EditField[0] = 0;
        ImGui::OpenPopup( ICON_FA_FOLDER_O" New Folder" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_FOLDER_O" New Folder" ) )
    {
        ImGui::Text( "Folder Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Folder = m_Document.CreateFolder( m_Params.m_gFolder );
            Folder.m_Name.Copy( m_Params.m_EditField );
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // FOLDER_RENAME
    //
    if( OpenWhichPopup == open_popup::FOLDER_RENAME ) 
    {
        ImGui::OpenPopup( ICON_FA_FOLDER_O" Rename Folder" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_FOLDER_O" Rename Folder" ) )
    {
        ImGui::Text( "Folder Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Folder = FolderDBase.getEntry( m_Params.m_gFolder );
            Folder.m_Name.Copy( m_Params.m_EditField );
            Folder.m_bChanged = true;
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // ENTRY_CREATE
    //
    if( OpenWhichPopup == open_popup::ENTRY_CREATE ) 
    {
        auto& EDBlueprint = m_Document.CreateEDBlueprint( m_Params.m_gFolder );
            
        m_pSelected = &GameGraphDoc.getGraph().m_BlueprintDB.getEntry( EDBlueprint.m_gBlueprint );
        m_bCreated  = true;

        if( pSelectionDoc ) pSelectionDoc->Single( EDBlueprint.m_gBlueprint, &EDBlueprint );
    }
}

