//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class document : public gbed_document::base
{
    x_object_type( document, rtti(gbed_document::base), is_not_copyable, is_not_movable )

public:
    using base::base;

    using                   t_edblueprintpath       = gbed_document::path::filepath<struct tag>;
    using                   t_blueprintpath         = gbed_document::path::filepath<struct tag>;
    x_constexprvar  auto    t_class_string          = X_STR("Blueprint");

    struct edblueprint_base
    {
        using guid = xguid<edblueprint_base>;
    };

    struct folder
    {
        using guid = xguid<folder>;

        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );

        guid                            m_Guid              { nullptr };
        xstring                         m_Name              {};
        guid                            m_gParent           { nullptr };
        xvector<guid>                   m_lgFolder          {};
        xvector<edblueprint_base::guid> m_lgEDBlueprints    {};
        bool                            m_bChanged          { false };
        bool                            m_bExpanded         { false };
    };

    struct edblueprint 
        : edblueprint_base
        , xproperty_v2::base
    {

        x_object_type( edblueprint, rtti(xproperty_v2::base) );

        x_inline                    edblueprint     ( gbed_game_graph::document& GameGraphDoc ) : m_GameGraphDoc{ GameGraphDoc } {}            
        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );
        virtual                    ~edblueprint     ( void ){}

        virtual         const xproperty_v2::table& onPropertyTable( void ) const override
        {
            // "This is a test",
            static const xproperty_v2::table E( this, [&](xproperty_v2::table& E)
            {
                E.AddScope( X_STR("Editor"), [&](xproperty_v2::table& E)
                {
                    E.AddProperty(X_STR("Name"),            m_Name,                 X_STR("Name of the Entity. Please note that this name is only used in the editor."));
                    E.AddProperty(X_STR("EditorGuid"),      m_Guid.m_Value,         X_STR("Guid used by the editor."), xproperty_v2::flags::MASK_READ_ONLY );
                });

                E.AddDynamicScope(
                    { nullptr }
                    , X_STATIC_LAMBDA_BEGIN (edblueprint& A) -> bool
                    {
                        return !!A.m_GameGraphDoc.getGraph().m_BlueprintDB.Find(A.m_gBlueprint);
                    }
                    X_STATIC_LAMBDA_END
                    , X_STATIC_LAMBDA_BEGIN (edblueprint& A) -> xproperty_v2::base*
                    {
                        return A.m_GameGraphDoc.getGraph().m_BlueprintDB.Find(A.m_gBlueprint);
                    }
                    X_STATIC_LAMBDA_END
                );
            });
        
            return E;
        }

        void AddRef     ( gb_component::entity::guid gEntity ) { m_bChanged = true; m_RefDBase.append(gEntity); }
        void DeleteRef  ( gb_component::entity::guid gEntity ) { m_bChanged = true; for( const auto& E : m_RefDBase ) if( E == gEntity ) { m_RefDBase.DeleteWithCollapse( m_RefDBase.getIndexByEntry( E ) ); return; } /*x_assert(false);*/ }

        gbed_game_graph::document&              m_GameGraphDoc;
        guid                                    m_Guid              { nullptr };
        xstring                                 m_Name              {};
        folder::guid                            m_gFolder           { nullptr };
        gb_blueprint::guid                      m_gBlueprint        { nullptr };
        bool                                    m_bChanged          { false };
        xvector<gb_component::entity::guid>     m_RefDBase          {};
    };

public:

    x_inline                                        document            ( const xstring::const_str Str, gbed_document::main& MainDoc );
    x_incppfile             edblueprint&            CreateEDBlueprint   ( folder::guid gFolder = folder::guid{ nullptr }, gb_blueprint::guid gBlueprint = gb_blueprint::guid{ nullptr } );
    x_incppfile             void                    DeleteEDBlueprint   ( void );
    x_incppfile             folder&                 CreateFolder        ( folder::guid gFolderParent = folder::guid{ nullptr } );
    virtual                 const type&             getType             ( void ) override;
    x_inline                auto&                   getFolderDBase      ( void ) { return m_FolderDBase;        }
    x_inline                auto&                   getEDBlueprintDBase ( void ) { return m_EDBlueprintDBase;   }
    x_inline                auto&                   getRootFolder       ( void ) { return m_FolderRoot;         }
    x_inline                auto&                   getGameGraphDoc     ( void ) { return m_GameGraphDoc;       }
    x_incppfile             edblueprint::guid       findEDBlueprintGuid ( gb_blueprint::guid Guid ) const;
    x_incppfile             edblueprint*            findEDBlueprint     ( gb_blueprint::guid Guid );

protected:

    using hash_blueprint_to_edblueprint = x_ll_mrmw_hash<edblueprint::guid, u64, x_lk_spinlock::do_nothing>;

protected:

    virtual                 err                     onSave              ( void ) override; 
    virtual                 err                     onLoad              ( void ) override;
    virtual                 err                     onNew               ( void ) override;
    virtual                 err                     onClose             ( void ) override;
    x_incppfile             void                    StartUp             ( void );
    x_incppfile             void                    msgChangeHappen     ( xproperty_v2::base& Prop );

protected:

    folder                                          m_FolderRoot                {};
    x_ll_dbase<edblueprint,edblueprint::guid>       m_EDBlueprintDBase          {};
    x_ll_dbase<folder,folder::guid>                 m_FolderDBase               {};
    hash_blueprint_to_edblueprint                   m_BlueprintEDBlueprint      {}; 
    t_edblueprintpath                               m_EDBlueprintPath           {};
    t_blueprintpath                                 m_BlueprintPath             {};
    gbed_game_graph::document&                      m_GameGraphDoc;

    x_message::delegate<document, xproperty_v2::base&>                                          m_deletegateChangeHappen            { *this, &document::msgChangeHappen };
};

