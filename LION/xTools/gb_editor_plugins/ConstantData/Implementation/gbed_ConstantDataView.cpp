//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------

view::view( document&  Document )
    : m_Document     { Document }
{
}

//-------------------------------------------------------------------------------------------

void view::Render( gbed_selection::document* pSelectionDoc ) 
{
    m_pSelected = nullptr;
    m_bCreated  = false;

    open_popup  OpenWhichPopup          = open_popup::NONE;
    auto&       RootFolder              = m_Document.getRootFolder();
    auto&       FolderDBase             = m_Document.getFolderDBase();
    auto&       EDConstantDataDBase     = m_Document.getEDConstDataDBase();
    auto&       GameGraphDoc            = m_Document.getGraphDoc();

    //
    // Context Folder Menu
    //
    const auto FolderMenu = [&]( document::folder& Folder )
    {
        ImGui::PushID(&Folder);
        if( ImGui::BeginPopupContextItem( ICON_FA_ARROW_CIRCLE_DOWN " ConstantFolderMenu" ) )
        {
            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  Rename Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_RENAME;
                m_Params.m_gFolder = Folder.getGuid();
                x_strcpy<xchar>( m_Params.m_EditField, Folder.m_Name );
            }

            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  New Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_NEW;
                m_Params.m_gFolder = Folder.getGuid();
            }

            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  Delete Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_DELETE;
                m_Params.m_gFolder = Folder.getGuid();
            }

            if( ImGui::MenuItem( ICON_FA_FOLDER_O "  Move Folder" ) )
            {
                OpenWhichPopup = open_popup::FOLDER_MOVE;
                m_Params.m_gFolder = Folder.getGuid();
            }

            ImGui::Separator();

            if( ImGui::MenuItem( ICON_FA_CODEPEN "  New ConstantData" ) )
            {
                OpenWhichPopup = open_popup::ENTRY_CREATE;
                m_Params.m_gFolder = Folder.getGuid();
            }

            ImGui::EndPopup();
        }
        ImGui::PopID();
    };

    //
    // System Entries
    //
    const auto ProcessSystemEntries = [&]( document::folder& Folder )
    {
        auto& Graph = GameGraphDoc.getGraph();

        Folder.m_bExpandedSystem = ImGui::TreeNode( &Folder, ICON_FA_ANDROID " System Entries" );

        if(Folder.m_bExpandedSystem)
        {
            x_lk_guard_as_const_get_as_const( Graph.m_ConstDataDB.m_Vector, ConstDataDB );

            for( auto& Entry : ConstDataDB )
            {
                auto& ConstEntry = *Entry->m_pEntry;
                
                if( Folder.m_gType != ConstEntry.getType().getGuid() ) 
                    continue;

                if( m_Document.findEDConstDataGuid( ConstEntry.getGuid() ).m_Value )
                    continue;

                ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
                if( pSelectionDoc )
                {
                    for( auto gSel : pSelectionDoc->getSelectedConstData() )
                    {
                        if( gSel == ConstEntry.getGuid() )
                        {
                            flags |= ImGuiTreeNodeFlags_Selected;
                            break;
                        }
                    }
                }

                // Create the label
                xstring             StrGuid;
                xarray<char,256>    label;
                ConstEntry.getGuid().getStringHex(StrGuid);
                sprintf_s( label, label.getCount<int>(), ICON_FA_CHECK_SQUARE_O" (%s)", static_cast<const char*>(StrGuid) );
                
                // display entry
                ImGui::TreeNodeEx( &ConstEntry, flags, label );
                if (ImGui::IsItemClicked()) 
                {
                    m_pSelected = &ConstEntry;
                    if( pSelectionDoc )
                    {
                        pSelectionDoc->Single( ConstEntry.getGuid(), &ConstEntry );
                    }
                }
            }

            // Done with tree
            ImGui::TreePop();
        }
    };

    //
    // Utility Functions
    //
    xarray<u64,16> Data;
    const xfunction_withptr<void(document::folder&)> ProcessFolderChildren{ reinterpret_cast<xbyte&>(Data[0]), Data.getByteSize(), [&]( document::folder& Folder )
    {
        if( Folder.m_gParent.m_Value )
        {
            Folder.m_bExpanded = Folder.m_bExpanded ? ImGui::TreeNode( &Folder, ICON_FA_FOLDER_OPEN_O " %s", Folder.m_Name ) :
                                                      ImGui::TreeNode( &Folder, ICON_FA_FOLDER_O      " %s", Folder.m_Name );
        }
        else
        {
            auto& Type = GameGraphDoc.getGraph().m_ConstDataTypeDB.getEntry( Folder.m_gType );
         //   ImGui::Text( ICON_FA_QUESTION_CIRCLE_O "" );
         //   ImGui::SameLine();
            Folder.m_bExpanded = ImGui::TreeNode( &Folder, ICON_FA_SHIELD " %s", Folder.m_Name );
    // ImGui::Text( ICON_FA_QUESTION_CIRCLE_O " " );  
            if (ImGui::IsItemHovered())             
            {
                ImGui::BeginTooltip();
                ImGui::PushTextWrapPos(450.0f);
                ImGui::TextUnformatted( Type.getHelp().m_pValue );
                ImGui::PopTextWrapPos();
                ImGui::EndTooltip();
            }
        }

        FolderMenu( Folder );
        if( Folder.m_bExpanded )
        {
            if( Folder.m_gParent.m_Value == 0 )
            {
                ProcessSystemEntries(Folder);
            }

            for( auto gFolder : Folder.m_lgFolder )
            {
                auto& Z = FolderDBase.getEntry( gFolder );
                ProcessFolderChildren(Z);
            }

            //
            // Deal with additional folders
            //
            int ID = 0;
            for( auto gConstantData : Folder.m_lgEDConstData )
            {
                auto pEDConstantData = EDConstantDataDBase.Find( gConstantData );
                if( pEDConstantData )
                {
                    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
                    if( pSelectionDoc )
                    {
                        for( auto gSel : pSelectionDoc->getSelectedConstData() )
                        {
                            if( gSel == pEDConstantData->m_gConstData )
                            {
                                flags |= ImGuiTreeNodeFlags_Selected;
                                break;
                            }
                        }
                    }

                    // Create the label
                    xstring             StrGuid;
                    xarray<char,256>    label;
                    pEDConstantData->m_gConstData.getStringHex(StrGuid);
                    if( pEDConstantData->m_bChanged )
                    {
                        sprintf_s( label, label.getCount<int>(), ICON_FA_PENCIL_SQUARE_O" %s (%s)", static_cast<const char*>(pEDConstantData->m_Name), static_cast<const char*>(StrGuid) );
                    }
                    else
                    {
                        sprintf_s( label, label.getCount<int>(), ICON_FA_CHECK_SQUARE_O" %s (%s)", static_cast<const char*>(pEDConstantData->m_Name), static_cast<const char*>(StrGuid) );
                    }
                
                    // display entry
                    ImGui::TreeNodeEx( pEDConstantData, flags, label );
                    if (ImGui::IsItemClicked()) 
                    {
                        m_pSelected = GameGraphDoc.getGraph().m_ConstDataDB.Find( pEDConstantData->m_gConstData );
                        if( pSelectionDoc )
                        {
                            pSelectionDoc->Single( pEDConstantData->m_gConstData, pEDConstantData );
                        }
                    }
                }
            }

            // Done with tree
            ImGui::TreePop();
        }
    }};

    //
    // Search Bar
    //
    if( ImGui::Button( ICON_FA_ARROW_CIRCLE_DOWN ) ) // "Menu"
    {
        OpenWhichPopup = open_popup::MAIN_MENU;
    }

    ImGui::SameLine(); if( ImGui::Button( ICON_FA_TIMES ) )
    {
        m_Filter.Clear();
    }
         
    ImGui::SameLine(); m_Filter.Draw(ICON_FA_SEARCH, -24.0f);

        
    if( OpenWhichPopup == open_popup::MAIN_MENU )
    {
        ImGui::OpenPopup( ICON_FA_ARROW_CIRCLE_DOWN " MainMenuConstantData" );;
    }

    if( ImGui::BeginPopup( ICON_FA_ARROW_CIRCLE_DOWN " MainMenuConstantData" ) )
    {
        if (ImGui::BeginMenu( ICON_FA_IOXHOST "  View As", ""))
        {
            if (ImGui::MenuItem( "  Folder View", ""))
            {
            }

            if (ImGui::MenuItem( "  New Folder ", ""))
            {
            }

            if (ImGui::MenuItem( "  Move To", ""))
            {
            }

            if (ImGui::MenuItem( "  Delete", ""))
            {
            }

            ImGui::EndMenu();
        }

        if (ImGui::MenuItem( ICON_FA_IOXHOST "  Load Scene ", ""))
        {
            // OpenWhichPopup = open_popup::LOAD_SCENE;
        }

        ImGui::EndPopup();
    }

    //
    // Render layers
    //
    ImGui::BeginChild("constant data view");
    for( auto& gFolder : RootFolder.m_lgFolder )
    {
        auto& Folder = FolderDBase.getEntry( gFolder );
        if( m_gType.m_Value )
        {
            if( Folder.m_gType == m_gType ) ProcessFolderChildren(Folder);
        }
        else
        {
            ProcessFolderChildren(Folder);
        }
    }
    ImGui::EndChild();


    //
    // FOLDER_NEW
    //
    if( OpenWhichPopup == open_popup::FOLDER_NEW )
    {
        m_Params.m_EditField[0] = 0;
        ImGui::OpenPopup( ICON_FA_FOLDER_O" New Folder" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_FOLDER_O" New Folder" ) )
    {
        ImGui::Text( "Folder Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& FZone = m_Document.CreateFolder( m_Params.m_gFolder );
            FZone.m_Name.Copy( m_Params.m_EditField );
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // FOLDER_RENAME
    //
    if( OpenWhichPopup == open_popup::FOLDER_RENAME ) 
    {
        ImGui::OpenPopup( ICON_FA_FOLDER_O" Rename Folder" );
    }

    if( ImGui::BeginPopupModal( ICON_FA_FOLDER_O" Rename Folder" ) )
    {
        ImGui::Text( "Folder Name: " ); 
        ImGui::SameLine(); ImGui::InputText( "", m_Params.m_EditField, m_Params.m_EditField.getCount() );

        if( ImGui::Button( "OK", ImVec2(120,0) ) )
        {
            auto& Folder = FolderDBase.getEntry( m_Params.m_gFolder );
            Folder.m_Name.Copy( m_Params.m_EditField );
            Folder.m_bChanged = true;
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    //
    // ENTRY_CREATE
    //
    if( OpenWhichPopup == open_popup::ENTRY_CREATE )
    {
        auto& Entry = m_Document.CreateEntry( m_Params.m_gFolder );
        m_pSelected = &GameGraphDoc.getGraph().m_ConstDataDB.getEntry( Entry.m_gConstData );
        m_bCreated  = true;
    }
}
