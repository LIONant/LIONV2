//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------

static const gbed_plugins::tab::type_harness<tab> s_TypeTab{ X_STR( "" ICON_FA_SHIELD " ConstD" ) };    
    
//-------------------------------------------------------------------------------------------

const gbed_plugins::tab::type& tab::getType( void ) 
{ 
    return s_TypeTab;  
}

//-------------------------------------------------------------------------------------------

tab::tab( const xstring::const_str& Str, gbed_frame::base& EditorFrame )
    : gbed_plugins::tab     { Str, EditorFrame }
    , m_View                { EditorFrame.getMainDoc().getSubDocument<document>() }
    , m_SelectionDoc        { EditorFrame.getMainDoc().getSubDocument<gbed_selection::document>()}
{
}

//-------------------------------------------------------------------------------------------

void tab::onRender( void ) 
{
    m_View.Render( &m_SelectionDoc );
}