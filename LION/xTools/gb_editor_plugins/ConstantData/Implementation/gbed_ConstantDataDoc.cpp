//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

static const gbed_document::base::type_harness<document> s_Type{ document::t_class_string, -2 };    

//-------------------------------------------------------------------------------------------

const gbed_document::base::type& document::getType( void ) 
{ 
    return s_Type;  
}

//-------------------------------------------------------------------------------------------

void document::folder::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "ConstDataFolder" );
    File.WriteField ( "Guid:g", m_Guid );
    File.WriteField ( "Name:s", m_Name );
    File.WriteField ( "gParent:g", m_gParent );
    File.WriteField ( "gType:g", m_gType );
    File.WriteLine();

    File.WriteRecord( "Folder", m_lgFolder.getCount<int>() );
    for( auto& gFolder : m_lgFolder )
    {
        File.WriteField ( "Guid:g", gFolder );
        File.WriteLine();
    }

    File.WriteRecord( "gEDConstData", m_lgEDConstData.getCount<int>() );
    for( auto& gEDBlueprint : m_lgEDConstData )
    {
        File.WriteField ( "Guid:g", gEDBlueprint );
        File.WriteLine();
    }
}

//-------------------------------------------------------------------------------------------

void document::folder::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("ConstDataFolder") )
        return;

    File.ReadLine();
    File.ReadField          ( "Guid:g", &m_Guid );
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadField          ( "gParent:g", &m_gParent );
    if( File.ReadRecord() == false ) return;

    //
    // Read folders
    //
    if( File.getRecordName() == X_STR("gFolders" ) )
    {
        m_lgFolder.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgFolder.append();
                File.ReadField( "Guid:g", &E );
            } 
        }

        if( File.ReadRecord() == false ) return;
    }
        
    //
    // Read gEDConstData
    //
    if( File.getRecordName() == X_STR("gEDConstData" ) )
    {
        m_lgEDConstData.DeleteAllEntries();

        const int Count = File.getRecordCount();
        if( Count > 0 )
        {
            for( int i=0; i<Count; i++ )
            {
                File.ReadLine();

                auto& E = m_lgEDConstData.append();
                File.ReadField( "Guid:g", &E );
            } 
        }
    }

    //
    // Just loaded so nothing has change
    //
    m_bChanged = false;
}

//-------------------------------------------------------------------------------------------

void document::edconstdata::Save( xtextfile& File )
{
    //
    // Save header
    //
    File.WriteRecord( "EDConstData" );
    File.WriteField ( "Guid:g", m_Guid );
    File.WriteField ( "Name:s", m_Name );
    File.WriteField ( "gFolder:g", m_gFolder );
    File.WriteField ( "gType:g", m_gType );
    File.WriteField ( "gConstData:g", m_gConstData );
    File.WriteLine();
}

//-------------------------------------------------------------------------------------------
    
void document::edconstdata::Load( xtextfile& File )
{
    //
    // Load header
    //
    if( File.getRecordName() != X_STR("EDConstData") )
        return;

    File.ReadLine();
    File.ReadField          ( "Guid:g", &m_Guid );
    File.ReadFieldXString   ( "Name:s", m_Name );
    File.ReadField          ( "gFolder:g", &m_gFolder );
    File.ReadField          ( "gType:g", &m_gType );
    File.ReadField          ( "gConstData:g", &m_gConstData );

    //
    // Just loaded so nothing has change
    //
    m_bChanged = false;
}

//-------------------------------------------------------------------------------------------

document::document( const xstring::const_str Str, gbed_document::main& MainDoc ) 
    : gbed_document::base   { Str, MainDoc }
    , m_GameGraphDoc        { MainDoc.getSubDocument<gbed_game_graph::document>() }
{

    m_EDConstDataDBase.Init( 1024 );
    m_FolderDBase.Init( 1024 );
    m_ConstData2EDConstData.Initialize( 1024 );

    m_deletegateChangeHappen.Connect( getMainDoc().m_Events.m_ChangeHappen );
}

//-------------------------------------------------------------------------------------------

document::err document::onSave( void ) 
{ 
    xstring     EDConstDataPath;
    xstring     ConstDataPath;

    // Convert to the path first
    EDConstDataPath.CopyAndConvert( &static_cast<std::wstring>(m_EDConstDataPath)[0] );
    ConstDataPath.CopyAndConvert( &static_cast<std::wstring>(m_ConstDataPath)[0] );

    //
    // Save EDConstData & Game ConstData
    //
    const auto SaveEDConstData = [&]( edconstdata& E ) -> document::err
    {
        if( E.m_bChanged ) 
        {
            //
            // Save the edconstdata
            //
            {
                xtextfile   File;
                xstring     FileName;
                xstring     FileRoot;

                // Create file name 
                E.m_Guid.getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                // Create finally the full filename path
                FileRoot.setup( "%s/%s.edconstd.txt", EDConstDataPath, FileName );

                if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
                {
                    E.Save( File );
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                }
            }
                
            //
            // Save the actual const data
            //
            {
                xtextfile   File;
                xstring     FileName;
                xstring     FileRoot;

                E.m_gConstData.getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';

                // Create finally the full filename path
                FileRoot.setup( "%s/%s.cd", ConstDataPath, FileName );

                if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
                {
                    auto& GameGraph = m_GameGraphDoc.getGraph();
                    auto& Entry = GameGraph.m_ConstDataDB.getEntry( E.m_gConstData );
                    Entry.Save( File );
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                }
            }

            //
            // saved successful 
            //
            E.m_bChanged = false;
        }

        return x_error_ok(); 
    };

    //
    // Save Folder Children
    //
    const xfunction<document::err(folder&)> SaveFolderChildren = [&]( folder& Folder ) -> document::err
    {
        //
        // Save all the folders
        //
        for( const auto& gEntry : Folder.m_lgFolder )
        {
            auto& E = m_FolderDBase.getEntry( gEntry );
            if( E.m_bChanged ) 
            {
                xtextfile   File;
                xstring     FileName;
                xstring     FileRoot;

                // Create file name 
                E.m_Guid.getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                // Create finally the full filename path
                FileRoot.setup( "%s/%s.folder.txt", EDConstDataPath, FileName );

                if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
                {
                    E.Save( File );
                }
                else
                {
                    X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                    return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                }
            }

            // Recurse the rest of the children
            if( auto Err = SaveFolderChildren( E ); Err.isError() ) return Err;
            E.m_bChanged = false;
        }

        //
        // Save all the EDBlueprint
        //
        for( const auto& gEDBlueprint : Folder.m_lgEDConstData )
        {
            auto& E = m_EDConstDataDBase.getEntry( gEDBlueprint );

            // Save the zone
            if( auto Err = SaveEDConstData(E); Err.isError() ) return Err;
            E.m_bChanged = false;
        }

        return x_error_ok(); 
    };

    //
    // Save the layer
    //
    if( m_FolderRoot.m_bChanged )
    {
        xtextfile   File;
        xstring     FileRoot;

        // Create finally the full filename path
        FileRoot.setup( "%s/Root.folder.txt", EDConstDataPath );

        if( auto Err = File.openForWriting( FileRoot.To<xwchar>() ); Err.isOK() )
        {
            m_FolderRoot.Save( File );
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }
    }

    // Recurse the rest of the children
    if( auto Err = SaveFolderChildren( m_FolderRoot ); Err.isError() ) return Err;
    m_FolderRoot.m_bChanged = false;
 
    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------
document::err document::onClose ( void ) 
{ 
    //
    // Delete all the editor related game based constant data
    //
    if( m_GameGraphDoc.isGraphValid() )
    {
        x_lk_guard_as_const_get_as_const( m_EDConstDataDBase.m_Vector, ConstDataVector );
        auto& GameGraph = m_GameGraphDoc.getGraph(); 
        for( auto& xpE : ConstDataVector )
        {
            GameGraph.m_ConstDataDB.Delete( xpE->m_pEntry->m_gConstData );
        }
    }

    m_EDConstDataDBase.DeleteAllEntries();
    m_FolderDBase.DeleteAllEntries();
    m_ConstData2EDConstData.DeleteAllEntries();

    x_destruct ( &m_FolderRoot ); 
    x_construct( folder, &m_FolderRoot );             
    
    m_EDConstDataPath.clear();
    m_ConstDataPath.clear();

    m_FolderRoot.m_Name = X_STR("Editor ConstantData");

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------
    
document::err document::onLoad ( void ) 
{ 
    //
    // Setup all the basic stuff
    //
    StartUp();

    //
    // Create Key Type folders
    //
    CreateDefaultFolders();

    //
    // Setup the basic vars
    //
    auto&               GameGraph           = m_GameGraphDoc.getGraph();
    const xstring       EDConstDataPath     = [&]{ xstring t; t.CopyAndConvert( &static_cast<std::wstring>(m_EDConstDataPath)[0] ); return t; }();
    const xstring       ConstDataPath       = [&]{ xstring t; t.CopyAndConvert( &static_cast<std::wstring>(m_ConstDataPath)[0] ); return t; }();

    //
    // Load Game ConstData
    //
    const auto LoadConstData = [&]( edconstdata& E ) -> document::err
    {
        xtextfile   File;
        xstring     FileName;
        xstring     FileRoot;

        E.m_gConstData.getStringHex( FileName );
        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';

        // Create finally the full filename path
        FileRoot.setup( "%s/%s.cd", ConstDataPath, FileName );

        if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
        {
            auto&                               Type      = GameGraph.m_ConstDataTypeDB.getEntry( E.m_gType );
            xndptr_s<gb_component::const_data>  XEntry{ Type.New( E.m_gConstData ) };
            
            XEntry->Load( File );

            // Register the game constant data with the lookup table
            m_ConstData2EDConstData.cpAddEntry( E.m_gConstData.m_Value, [&]( edconstdata::guid& Guid )
            {
                Guid = E.m_Guid;    
            });

            // Transfer to dbase
            GameGraph.m_ConstDataDB.RegisterAndTransferOwner( XEntry );
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }

        return x_error_ok(); 
    };

    //
    // Load EDConstData
    //
    const auto LoadEDConstData = [&]( edconstdata& E ) -> document::err
    {
        xtextfile   File;
        xstring     FileName;
        xstring     FileRoot;

        // Create file name 
        E.m_Guid.getStringHex( FileName );
        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
        // Create finally the full filename path
        FileRoot.setup( "%s/%s.edconstd.txt", EDConstDataPath, FileName );

        if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
        {
            E.Load( File );
            File.close();
            if( auto Err = LoadConstData(E); Err.isError() ) return Err;
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }
                
        return x_error_ok(); 
    };

    //
    // Load Folder Children
    //
    const xfunction<document::err(folder&)> LoadFolderChildren = [&]( folder& Folder ) -> document::err
    {
        //
        // Load all the folders
        //
        for( const auto& gEntry : Folder.m_lgFolder )
        {
            xtextfile   File;
            xstring     FileName;
            xstring     FileRoot;

            // Create file name 
            gEntry.getStringHex( FileName );
            FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
            // Create finally the full filename path
            FileRoot.setup( "%s/%s.folder.txt", EDConstDataPath, FileName );

            if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
            {
                xndptr_s<folder>    xpFolder;
                auto                pPtr = m_FolderDBase.Find( gEntry );
                xstring             Name;

                // if we can not find it add one
                if( pPtr == nullptr)
                {
                    xpFolder.New();
                    pPtr = xpFolder.getPtr(); 
                }
                else
                {
                    Name = pPtr->m_Name;
                }

                // Load the data
                pPtr->Load( File );

                // In case they change the name to something new
                if( Name.isValid() )
                {
                    if( pPtr->m_Name != Name )
                    {
                        pPtr->m_Name = Name;
                        pPtr->m_bChanged = true;
                    }
                }
                File.close();

                // Recurse the rest of the children
                if( auto Err = LoadFolderChildren( *pPtr ); Err.isError() ) return Err;

                // Transfer entry to dbase if we did not have it already
                if( xpFolder.isValid() )
                {
                    m_FolderDBase.RegisterAndTransferOwner( xpFolder );
                }
            }
            else
            {
                X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error fail to find/load an constant type on disk, %s", Err.getString() );
                return x_error_code( errors, ERR_FAILURE, "Fail to load constantdata.folders" );
            }
        }

        //
        // Load all the EDConstData
        //
        for( const auto& gEDBlueprint : Folder.m_lgEDConstData )
        {
            xndptr_s<edconstdata> xpEntry;
            xpEntry.New( m_GameGraphDoc );

            xpEntry->m_Guid = gEDBlueprint;

            // Save the zone
            if( auto Err = LoadEDConstData( *xpEntry ); Err.isError() ) return Err;

            // Tranfer to dbase
            m_EDConstDataDBase.RegisterAndTransferOwner( xpEntry );
        }

        return x_error_ok(); 
    };

    //
    // Load the Root
    //
    {
        xtextfile   File;
        xstring     FileRoot;

        // Create finally the full filename path
        FileRoot.setup( "%s/Root.folder.txt", EDConstDataPath );

        if( auto Err = File.openForReading( FileRoot.To<xwchar>() ); Err.isOK() && File.ReadRecord() )
        {
            m_FolderRoot.Load( File );
        }
        else
        {
            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
        }

        // Recurse the rest of the children
        if( auto Err = LoadFolderChildren( m_FolderRoot ); Err.isError() ) return Err;
    }


    return x_error_ok(); 
}


//-------------------------------------------------------------------------------------------

document::folder& document::CreateFolder( folder::guid gParent, folder::guid gGuid )
{
    xndptr_s<folder> xpFolder;
    xpFolder.New();
    xpFolder->m_Guid     = gGuid;
    xpFolder->m_Name     = X_STR( "New Zone" );
    xpFolder->m_bChanged = true;
    xpFolder->m_gParent  = gParent;

    if( gParent.m_Value )
    {
        auto& Entry = m_FolderDBase.getEntry( gParent );
        xpFolder->m_gType = Entry.m_gType;

        Entry.m_lgFolder.append( xpFolder->m_Guid );
    }

    // Transfer ownership to the database
    auto* pPtr = xpFolder.getPtr();
    m_FolderDBase.RegisterAndTransferOwner( xpFolder );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------

document::edconstdata::guid document::findEDConstDataGuid(  gb_component::const_data::guid Guid ) const
{
    edconstdata::guid EdGuid{nullptr};

    m_ConstData2EDConstData.cpGetOrFailEntry( Guid.m_Value
    , [&]( const edconstdata::guid& Entry )
    {
        EdGuid = Entry;
    });

    return EdGuid;
}

//-------------------------------------------------------------------------------------------
document::edconstdata* document::findEDConstData(  gb_component::const_data::guid Guid )
{
    const edconstdata::guid EdGuid = findEDConstDataGuid(Guid);
    if( EdGuid.m_Value == 0 ) return nullptr;
    return &m_EDConstDataDBase.getEntry( EdGuid );
}

//-------------------------------------------------------------------------------------------

document::edconstdata& document::CreateEntry( folder::guid gParent )
{
    auto& GameGraph = m_GameGraphDoc.getGraph();
    auto& Folder    = m_FolderDBase.getEntry( gParent );
    auto& Type      = GameGraph.m_ConstDataTypeDB.getEntry( Folder.m_gType );

    //
    // Create the guid for the gamebase constant data
    //
    gb_component::const_data::guid gConstData;
    gConstData.Reset();

    //
    // Create and initialize ed constant data
    //
    xndptr_s<edconstdata> xpEntry;
    xpEntry.New( m_GameGraphDoc );
    xpEntry->m_Guid.Reset();
    xpEntry->m_gType        = Folder.m_gType;
    xpEntry->m_Name         = X_STR( "New Entry" );
    xpEntry->m_gFolder      = gParent;
    xpEntry->m_gConstData   = gConstData; 
    xpEntry->m_bChanged     = true;

    //
    // Create the game base version of the constant data
    //
    xndptr_s<gb_component::const_data> XEntry{ Type.New( gConstData ) };
    GameGraph.m_ConstDataDB.RegisterAndTransferOwner( XEntry );

    //
    // Put entry in the parentlist
    //
    Folder.m_lgEDConstData.append( xpEntry->m_Guid );
    Folder.m_bChanged = true;

    //
    // Register the game constant data with the lookup table
    //
    m_ConstData2EDConstData.cpAddEntry( gConstData.m_Value, [&]( edconstdata::guid& Guid )
    {
        Guid = xpEntry->m_Guid;    
    });

    //
    // Transfer ownership to the database
    //
    auto* pPtr      = xpEntry.getPtr();
    m_EDConstDataDBase.RegisterAndTransferOwner( xpEntry );

    return *pPtr; 
}

//-------------------------------------------------------------------------------------------

void document::CreateDefaultFolders( void )
{
    auto& GameGraph = m_GameGraphDoc.getGraph();

    x_lk_guard_as_const_get_as_const( GameGraph.m_ConstDataTypeDB.m_Vector, ConstDataVector );

    m_FolderRoot.m_Guid.Reset();
    m_FolderRoot.m_Name         = X_STR( "Root" );
    m_FolderRoot.m_bChanged     = true;
    for( auto& ConstType : ConstDataVector )
    {
        auto& TypeEntry = *ConstType->m_pEntry;

        // The top type folders have the same guid as the types themselves 
        auto& Folder    = CreateFolder( folder::guid{ nullptr }, folder::guid{TypeEntry.getGuid().m_Value} );  

        Folder.m_Name.Copy( TypeEntry.getEditorName() );
        Folder.m_gType = TypeEntry.getGuid();

        // At it to the root folder
        m_FolderRoot.m_lgFolder.append() = Folder.m_Guid;
    }
}

//-------------------------------------------------------------------------------------------

void document::StartUp( void )
{
    m_EDConstDataPath   = m_GameGraphDoc.getJITPath()       + std_fs::path("/EDConstData");
    m_ConstDataPath     = m_GameGraphDoc.getGameDataPath()  + std_fs::path("/ConstData");
}

//-------------------------------------------------------------------------------------------

document::err document::onNew( void )
{
    // First lets do the start up
    StartUp();

    // Create the actual directory
    if (auto Error = m_MainDoc.CreateDir(m_EDConstDataPath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_ConstDataPath); Error.isError()) 
        return Error;

    // Create Key Type folders
    CreateDefaultFolders();

    // Let's Save New created project
    m_FolderRoot.m_bChanged = true;
    if( auto Error = onSave(); Error.isError() ) return Error;

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

void document::msgChangeHappen( xproperty_v2::base& Prop )
{
    if( Prop.isKindOf<edconstdata>() )
    {
        auto& EDConstData = Prop.SafeCast<edconstdata>();
        EDConstData.m_bChanged = true;
    }
}


