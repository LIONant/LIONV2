//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class document : public gbed_document::base
{
    x_object_type( document, rtti(gbed_document::base), is_not_copyable, is_not_movable )

public:
    using base::base;

    using                   t_edblueprintpath       = gbed_document::path::filepath<struct tag>;
    using                   t_blueprintpath         = gbed_document::path::filepath<struct tag>;
    x_constexprvar  auto    t_class_string          = X_STR("ConstantData");

    struct edconstdata_base
    {
        using guid = xguid<edconstdata_base>;
    };

    struct folder
    {
        using guid = xguid<folder>;

        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );

        guid                                    m_Guid              { nullptr };
        xstring                                 m_Name              {};
        guid                                    m_gParent           { nullptr };
        gb_component::const_data::type::guid    m_gType             {};
        xvector<guid>                           m_lgFolder          {};
        xvector<edconstdata_base::guid>         m_lgEDConstData     {};
        bool                                    m_bChanged          { false };
        bool                                    m_bExpanded         { false };
        bool                                    m_bExpandedSystem   { false };
    };

    struct edconstdata 
        : edconstdata_base
        , xproperty_v2::base
    {
        x_object_type( edconstdata, rtti(xproperty_v2::base) );

        x_inline                    edconstdata     ( gbed_game_graph::document& GameGraphDoc ) : m_GameGraphDoc{ GameGraphDoc } {}            
        x_inline        auto        getGuid         ( void )                const { return m_Guid; }
        x_incppfile     void        Save            ( xtextfile& File );
        x_incppfile     void        Load            ( xtextfile& File );
        virtual                    ~edconstdata     ( void ){}

        virtual         const xproperty_v2::table& onPropertyTable( void ) const override
        {
            // "This is a test",
            static const xproperty_v2::table E( this, [&](xproperty_v2::table& E)
            {
                E.AddScope( X_STR("Editor"), [&](xproperty_v2::table& E)
                {
                    E.AddProperty(X_STR("Name"),            m_Name,                 X_STR("Name of the Entity. Please note that this name is only used in the editor."));
                    E.AddProperty(X_STR("EditorGuid"),      m_Guid.m_Value,         X_STR("Guid used by the editor."), xproperty_v2::flags::MASK_READ_ONLY );
                    E.AddProperty(X_STR("TypeGuid"),        m_gType.m_Value,        X_STR("Guid used by the editor."), xproperty_v2::flags::MASK_READ_ONLY );
                });

                E.AddDynamicScope(
                    { nullptr }
                    , X_STATIC_LAMBDA_BEGIN (edconstdata& A) -> bool
                    {
                        return !!A.m_GameGraphDoc.getGraph().m_ConstDataDB.Find(A.m_gConstData);
                    }
                    X_STATIC_LAMBDA_END
                    , X_STATIC_LAMBDA_BEGIN (edconstdata& A) -> xproperty_v2::base*
                    {
                        return A.m_GameGraphDoc.getGraph().m_ConstDataDB.Find(A.m_gConstData);
                    }
                    X_STATIC_LAMBDA_END
                );
            });
        
            return E;
        }

        guid                                    m_Guid              { nullptr };
        xstring                                 m_Name              {};
        gb_component::const_data::type::guid    m_gType             {};
        folder::guid                            m_gFolder           { nullptr };
        gb_component::const_data::guid          m_gConstData        { nullptr };
        bool                                    m_bChanged          { false };
        gbed_game_graph::document&              m_GameGraphDoc;
    };

public:

    x_inline                                    document                ( const xstring::const_str Str, gbed_document::main& MainDoc );
    x_incppfile             edconstdata&        CreateEDConstData       ( folder::guid gFolder, gb_blueprint::guid gBlueprint );
    x_incppfile             void                DeleteEDConstData       ( void );
    x_incppfile             void                getEDConstData          ( void );
    x_incppfile             folder&             CreateFolder            ( folder::guid gParent, folder::guid gGuid = folder::guid::RESET );
    x_incppfile             edconstdata&        CreateEntry             ( folder::guid gParent );
    virtual                 const type&         getType                 ( void ) override;
    x_inline                auto&               getFolderDBase          ( void ) { return m_FolderDBase; }
    x_inline                auto&               getEDConstDataDBase     ( void ) { return m_EDConstDataDBase; }
    x_inline                auto&               getRootFolder           ( void ) { return m_FolderRoot; }
    x_inline                auto&               getGraphDoc             ( void ) { return m_GameGraphDoc; }
    x_incppfile             edconstdata::guid   findEDConstDataGuid     ( gb_component::const_data::guid Guid ) const;
    x_incppfile             edconstdata*        findEDConstData         ( gb_component::const_data::guid Guid );

protected:

    using hash_constdata_to_edconstdata = x_ll_mrmw_hash<edconstdata::guid, u64, x_lk_spinlock::do_nothing>;

protected:

    virtual                 err                 onSave                  ( void ) override; 
    virtual                 err                 onLoad                  ( void ) override;
    virtual                 err                 onNew                   ( void ) override; 
    virtual                 err                 onClose                 ( void ) override;
    x_incppfile             void                CreateDefaultFolders    ( void );
    x_incppfile             void                StartUp                 ( void );
    x_incppfile             void                msgChangeHappen         ( xproperty_v2::base& Prop );

protected:

    folder                                          m_FolderRoot                {};
    x_ll_dbase<edconstdata,edconstdata::guid>       m_EDConstDataDBase          {};
    x_ll_dbase<folder,folder::guid>                 m_FolderDBase               {};
    hash_constdata_to_edconstdata                   m_ConstData2EDConstData     {}; 
    t_edblueprintpath                               m_EDConstDataPath           {};
    t_blueprintpath                                 m_ConstDataPath             {};
    gbed_game_graph::document&                      m_GameGraphDoc;

    x_message::delegate<document, xproperty_v2::base&>                                          m_deletegateChangeHappen            { *this, &document::msgChangeHappen };
};
