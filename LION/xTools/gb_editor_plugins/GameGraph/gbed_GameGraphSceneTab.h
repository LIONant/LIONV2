//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class scene_tab : public gbed_plugins::tab
{
public:

    x_incppfile             scene_tab   ( const xstring::const_str& Str, gbed_frame::base& EditorFrame );

protected:

    virtual const type&     getType     ( void ) override;
    virtual void            onRender    ( void ) override;

protected:

    enum class open_popup : u8
    {
        NONE
        , DISPLAY_ERROR
        , PROJECT_CREATE_NEW_DLG
        , PROJECT_CREATE_NEW
        , PROJECT_SAVE
        , PROJECT_LOAD
        , PROJECT_CLOSE
    };

    struct params
    {
        xarray< char, 256 >         m_String01    {};
        xarray< char, 256 >         m_String02    {};
    };

    enum class reload_type : u8
    {
        DEBUG,
        RELEASE
    };

protected:

    document&       m_Document;
    params          m_Params            {};
    int             m_SelectedSpeed     { 0 };
    bool            m_PlayModeF2F       { false };
    bool            m_FullWindow        { false };
    reload_type     m_ReloadType        { reload_type::DEBUG };
    bool            m_ReloadAuto        { false };
};


