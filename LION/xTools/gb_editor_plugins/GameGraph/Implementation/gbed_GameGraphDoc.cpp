//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

static const gbed_document::base::type_harness<document> s_Type{ document::t_class_string, -50 };    
    
//-------------------------------------------------------------------------------------------

const gbed_document::base::type& document::getType( void ) 
{ 
    return s_Type;  
}

//-------------------------------------------------------------------------------------------

document::document( const xstring::const_str Str, gbed_document::main& MainDoc ) 
    : gbed_document::base   { Str, MainDoc }
{
    //
    // Initializing the Symbol Handler
    // Used to debug the DLLS
    //
    DWORD  error;
    HANDLE hProcess;

    SymSetOptions( SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS );

    hProcess = GetCurrentProcess();

    if (!SymInitialize(hProcess, NULL, TRUE))
    {
        // SymInitialize failed
        error = GetLastError();
        X_LOG("SymInitialize returned error : %d\n", error);
    }
}

//-------------------------------------------------------------------------------------------

void document::StartUp( void )
{
    m_JITPath           = m_MainDoc.getProjectPath() + std_fs::path("/Editor");
    m_SettingsPath      = m_MainDoc.getProjectPath() + std_fs::path("/Settings");
    m_GameDataPath      = m_MainDoc.getProjectPath() + std_fs::path("/GameData");
    m_GraphDLLPath      = m_MainDoc.getProjectPath() + std_fs::path("/GameData/Game.dll");
    m_GraphPDBPath      = m_MainDoc.getProjectPath() + std_fs::path("/GameData/Game.pdb");
    m_SrcGraphDLLPath   = m_MainDoc.getCurrentPath() + std_fs::path("/x64/Debug/GameCore.vs2017.dll");

    // Create a temporary file name for when we need to play and save our entities
    xstring FileName;
    xguid<int>( xguid<int>::RESET ).getStringHex( FileName, '-' );
    m_TempFileName.setup( "temp:/lion_%s.tmp", FileName );
}

//-------------------------------------------------------------------------------------------

document::err document::onNew( void ) 
{
    StartUp();

    // Create the actual directory
    if (auto Error = m_MainDoc.CreateDir(m_JITPath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_SettingsPath); Error.isError()) 
        return Error;

    if (auto Error = m_MainDoc.CreateDir(m_GameDataPath); Error.isError()) 
        return Error;

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

document::err document::onLoad( void )
{
    StartUp();

    if (auto Error = LoadGraph(); Error.isError()) 
        return Error;

    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

document::err document::onClose( void )
{
    if( m_xpGraph.isValid() )
    {
        m_xpGraph.Delete();
        if( m_GraphDLL ) FreeLibrary( m_GraphDLL );
        if( m_GraphPDBModule ) SymUnloadModule64( GetCurrentProcess(), m_GraphPDBModule );
        m_GraphPDBModule = 0;
        m_GraphDLL = 0;
    }

    m_JITPath.clear();       
    m_SettingsPath.clear();
    m_GameDataPath.clear(); 
    m_bPlaying          = false; 
    m_bPaused           = false;
    m_PlaybackSpeed     = 1.0f;

    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::Play( void )
{
    if( false == m_bPlaying )
    {
        xtextfile File;
        if( auto Err = File.openForWriting( m_TempFileName.To<xwchar>(), xtextfile::access_type::MASK_BINARY ); Err.isError() )
        {
            X_LOG("Fail to save the entities before starting to play the game (%s)", Err.getString() );
            Err.IgnoreError();

            return x_error_code( errors, ERR_FAILURE, "Fail to open the temporary file require to save the current state of the game");
        }

        m_xpGraph->linearSaveGame( File );
        m_bPlaying = true;
    }

    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::StopPlay( void )
{
    if( m_bPlaying )
    {
        m_xpGraph->linearDeleteAllEntities();
        if( auto Err = m_xpGraph->linearLoadGame( m_TempFileName.To<xwchar>(), true ); Err.isError() )
        {
            Err.IgnoreError();
            return x_error_code( errors, ERR_FAILURE, "Fail to restore the previous state of the game");        
        }
        m_bPlaying = false;
    }

    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::LoadGraph( void )
{
    x_assert( m_GraphDLL == 0 );

    //
    // Load the symbol information for debugging
    //
    m_GraphPDBModule = SymLoadModuleExW( GetCurrentProcess(),       // target process 
                        NULL,                                       // handle to image - not used
                        &std::wstring( m_GraphDLLPath )[0],         // name of image file
                        NULL,                                       // name of module - not required
                        0,                                          // base address - not required
                        0,                                          // size of image - not required
                        NULL,                                       // MODLOAD_DATA used for special cases 
                        0);                                         // flags - not required
    if ( m_GraphPDBModule == 0 ) 
    {
        // SymLoadModuleEx failed
        DWORD error = GetLastError();
        X_LOG( "Fail to load the debug symbols for the game.pdb, SymLoadModuleEx returned error : %d", error);
        return x_error_code( errors, ERR_FAILURE, "Fail to load the debug symbols for the game.pdb");
    }

    //
    // Load the DLL proper
    //
    m_GraphDLL = LoadLibrary( &std::wstring( m_GraphDLLPath )[0] );
    if( NULL == m_GraphDLL ) return x_error_code( errors, ERR_FAILURE, "Fail to load the game.dll");        

    //
    // Get pointer to our function using GetProcAddress:
    //
    using   create_fnptr    = gb_game_graph::base*(*)(g_context&,eng_window&);
    auto    pCreateInstance = (create_fnptr)GetProcAddress( m_GraphDLL, "?CreateInstance@@YAPEAXAEAUg_context@@AEAVeng_window@@@Z" );
    auto    onError         = [&](void)
    {
        if( m_GraphDLL ) FreeLibrary( m_GraphDLL );
        if( m_GraphPDBModule ) SymUnloadModule64( GetCurrentProcess(), m_GraphPDBModule );
        m_GraphDLL = 0;
        m_GraphPDBModule = 0;
    };

    if( pCreateInstance == nullptr )
    {
        onError();
        return x_error_code( errors, ERR_FAILURE, "Fail to find the CreateInstance Function. Please check to make sure you are exporting a function with the following signature: gb_game_graph::base* CreateInstance( g_context&, eng_window& )");
    }


    // If the function address is valid, call the function. 
    auto pGameMgr = pCreateInstance( g_context::get(), *m_pFrame->getWindow() );
    if( pGameMgr == nullptr )
    {
        onError();
        return x_error_code( errors, ERR_FAILURE, "Fail to find the CreateInstance Function. Please check to make sure you are exporting a function with the following signature: gb_game_graph::base* CreateInstance( g_context&, eng_window& )");
    }

    //
    // Setup the new graph
    //
    m_xpGraph.setup( pGameMgr );
    m_xpGraph->Initialize( true );

    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::ReloadGraph( void )
{
    constexpr auto      TempEntity      = X_WSTR("temp:/Lion-ReloadGameMgrDLL-Entity.lion.bin"); 
    constexpr auto      TempBP          = X_WSTR("temp:/Lion-ReloadGameMgrDLL-BP.lion.bin"); 
    constexpr auto      TempConst       = X_WSTR("temp:/Lion-ReloadGameMgrDLL-Const.lion.bin"); 
    bool                bWeSavedData    = false;
    err                 Error           = x_error_ok();
    std::error_code     ec;
    xvector<std::pair<gb_component::const_data::type::guid,gb_component::const_data::guid>> lConstData;
    xvector<gb_blueprint::guid>             lBluePrint;

    //
    // Check if we can reaload the game_mgr
    //
    {
        xvector<xstring> Issues;    
        
        m_Events.m_queryReloadGameMgr.Notify( Issues );

        if( Issues.getCount() )
        {
            for( auto& Issue : Issues )
            {
                X_LOG("Can't reload the game manager because : %s",static_cast<const char*>(Issue) );
            }

            return x_error_code( errors, ERR_FAILURE, "Fail to reload the gamemgr due to dependent editors. Please check the log for farther information.");
        }
    }

    //
    // Check to see if there is a src dll
    //
    if( std_fs::exists( m_SrcGraphDLLPath, ec ) == false )
    {
        xwstring Tre;
        Tre.Copy( &std::wstring(m_SrcGraphDLLPath)[0] );
        X_LOG("Fail to find a src dll (%s)", &Tre.To<xchar>()[0] );
        return x_error_code( errors, ERR_FAILURE, "Fail to find a src dll");
    }
    
    //
    // Check to see if there is a src pdb
    //
    std::wstring SymbolSrcFile;
    {
        SymbolSrcFile = m_SrcGraphDLLPath;
        int l = static_cast<int>(SymbolSrcFile.length());
        l -= 3;
        SymbolSrcFile[l++] = 'p'; 
        SymbolSrcFile[l++] = 'd'; 
        SymbolSrcFile[l++] = 'b'; 
    }

    if( std_fs::exists( SymbolSrcFile, ec ) == false )
    {
        xwstring Tre;
        Tre.Copy( &SymbolSrcFile[0] );
        X_LOG("Fail to find a src dll (%s)", &Tre.To<xchar>()[0] );
        return x_error_code( errors, ERR_FAILURE, "Fail to find a src pdb");
    }

    //
    // Delete files just in case
    //
    if(0)
    {
        std::wstring A;
        A = xfile::getTempPath();
        A += L"Lion-ReloadGameMgrDLL-Entity.lion.bin";
        std_fs::remove( A, ec );

        A = xfile::getTempPath();
        A += L"Lion-ReloadGameMgrDLL-BP.lion.bin";
        std_fs::remove( A, ec );

        A = xfile::getTempPath();
        A += L"Lion-ReloadGameMgrDLL-Const.lion.bin";
        std_fs::remove( A, ec );
    }

    //
    // Save game
    //
    x_job_block Block( X_WSTR("ReloadGGraph") );
    if( m_xpGraph.isValid() )
    {
        bWeSavedData = true;

        //
        // Save all blueprints
        //
        Block.SubmitJob( [&]()
        {
            xtextfile File;
            if( auto Err = File.openForWriting( TempBP, xtextfile::access_type::MASK_BINARY ); Err.isError() )
            {
                X_LOG("Fail to save the blueprints before reloading the game dll (%s)", Err.getString() );
                Err.IgnoreError();

                Error = x_error_code( errors, ERR_FAILURE, "Fail to save the blueprints before reloading the game dll. Fail to open the temporary file require to save the current state of the game");
                return;
            }

            xproperty_v2::entry::RegisterTypes( File );
            x_lk_guard_as_const_get_as_const( m_xpGraph->m_BlueprintDB.m_Vector, BlueprintVector );
            for( auto& pBPEntry : BlueprintVector )
            {
                auto& Entry = *pBPEntry->m_pEntry;
                lBluePrint.append(Entry.getGuid());
                Entry.Save( File );
            }
        });

        //
        // Save all constant data
        //
        Block.SubmitJob( [&]()
        {
            xtextfile File;
            if( auto Err = File.openForWriting( TempConst, xtextfile::access_type::MASK_BINARY ); Err.isError() )
            {
                X_LOG("Fail to save the constdata before reloading the game dll (%s)", Err.getString() );
                Err.IgnoreError();

                Error = x_error_code( errors, ERR_FAILURE, "Fail to save the constdata before reloading the game dll. Fail to open the temporary file require to save the current state of the game");
                return ;
            }

            xproperty_v2::entry::RegisterTypes( File );
            x_lk_guard_as_const_get_as_const( m_xpGraph->m_ConstDataDB.m_Vector, ConstDataVector );
            for( auto& pCEntry : ConstDataVector )
            {
                auto& Entry = *pCEntry->m_pEntry;
                lConstData.append(std::pair<gb_component::const_data::type::guid,gb_component::const_data::guid>{ Entry.getType().getGuid(), Entry.getGuid() } );
                Entry.Save( File );
            }
        });        

        //
        // Save all entities
        //
        Block.SubmitJob( [&]()
        {
            xtextfile File;
            if( auto Err = File.openForWriting( TempEntity, xtextfile::access_type::MASK_BINARY ); Err.isError() )
            {
                X_LOG("Fail to save the entities before reloading the game dll (%s)", Err.getString() );
                Err.IgnoreError();

                Error = x_error_code( errors, ERR_FAILURE, "Fail to save the entities before reloading the game dll. Fail to open the temporary file require to save the current state of the game");
                return ;
            }

            m_xpGraph->linearSaveGame( File );
        });

        //
        // Sync all jobs
        //
        Block.Join();
        if( Error.isError() ) return Error;
    }

    //
    // Delete the game
    //
    m_xpGraph.Delete();
    if( m_GraphDLL ) 
    {
        FreeLibrary( m_GraphDLL );
        SymUnloadModule64( GetCurrentProcess(), m_GraphPDBModule );
        m_GraphPDBModule = 0;
        m_GraphDLL = 0;
    }

    //
    // Copy DLL from temp folder to final folder
    //
    Block.SubmitJob( [&]()
    {
        std_fs::copy_file( m_SrcGraphDLLPath, m_GraphDLLPath, std_fs::copy_options::overwrite_existing , ec );
        if( ec )
        {
            const char* pPtr = &ec.message()[0];
            X_LOG("Fail to copy the new DLL into the destination folder(%s)", pPtr );
            Error = x_error_code( errors, ERR_FAILURE, "Fail to copy the new DLL into the destination folder");
            return;
        }
    });

    Block.SubmitJob( [&]()
    {
        std_fs::copy_file( SymbolSrcFile, m_GraphPDBPath, std_fs::copy_options::overwrite_existing , ec );
        if( ec )
        {
            const char* pPtr = &ec.message()[0];
            X_LOG("Fail to copy the new PDB into the destination folder(%s)", pPtr );
            Error = x_error_code( errors, ERR_FAILURE, "Fail to copy the new PDB into the destination folder");
            return;
        }
    });

    Block.Join();
    if( Error.isError() ) return Error;

    //
    // Load the New DLL
    //
    if( auto Err = LoadGraph(); Err.isError() ) return Err;

    //
    // Reload all the game entities
    //
    if( bWeSavedData )
    {
        err Error = x_error_ok();

        //
        // Load Blueprints
        //
        if( lBluePrint.getCount() )
        Block.SubmitJob( [&]()
        {
            xtextfile File;
            if( auto Err = File.openForReading( TempBP ); Err.isError() )
            {
                X_LOG("Fail to load the blueprint before reloading the game dll (%s)", Err.getString() );
                Err.IgnoreError();

                Error = x_error_code( errors, ERR_FAILURE, "Fail to load the blueprint before reloading the game dll. Fail to open the temporary file require to save the current state of the game");
                return ;
            }

            File.ReadRecord();
            for( auto Guid : lBluePrint )
            {
                auto& BP = m_xpGraph->CreateBlueprint( Guid );
                BP.Load( File, *m_xpGraph );
            }
        });

        //
        // Load ConstData
        //
        if( lConstData.getCount() )
        Block.SubmitJob( [&]()
        {
            xtextfile File;
            if( auto Err = File.openForReading( TempConst ); Err.isError() )
            {
                X_LOG("Fail to load the constdata before reloading the game dll (%s)", Err.getString() );
                Err.IgnoreError();

                Error = x_error_code( errors, ERR_FAILURE, "Fail to load the constdata before reloading the game dll. Fail to open the temporary file require to save the current state of the game");
                return;
            }

            auto& ConstDataDB = m_xpGraph->m_ConstDataDB;
            File.ReadRecord();
            for( auto CData : lConstData )
            {
                auto&                               Type      = m_xpGraph->m_ConstDataTypeDB.getEntry( CData.first );
                xndptr_s<gb_component::const_data>  XEntry{ Type.New( CData.second ) };
                XEntry->Load( File );
                ConstDataDB.RegisterAndTransferOwner( XEntry );
            }
        });

        //
        // Sync jobs
        //
        Block.Join();
        if( Error.isError() ) return Error;

        //
        // Load Entities
        //
        if( auto Err = m_xpGraph->linearLoadGame( TempEntity, true ); Err.isError() )
        {
            X_LOG("Fail to restore the previous state of the game(%s)", static_cast<const char*>( Err.getString() ) );
            Err.IgnoreError();
            return x_error_code( errors, ERR_FAILURE, "Fail to restore the previous state of the game");        
        }
    }

    return x_error_ok();
}


