//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

void view::Render ( void )
{
    //
    // Search Bar
    //
    if( ImGui::Button( ICON_FA_ARROW_CIRCLE_DOWN ) ) // "Menu"
    {
      //  OpenWhichPopup = open_popup::MAIN_MENU;
    }

    ImGui::SameLine(); if( ImGui::Button( ICON_FA_TIMES ) )
    {
        m_Filter.Clear();
    }
         
    ImGui::SameLine(); m_Filter.Draw(ICON_FA_SEARCH, -24.0f);

    auto& Graph = m_Document.getGraph();
    ImGui::BeginChild("game mgr view");
    {
        x_lk_guard_as_const_get_as_const( Graph.m_CompManagerDB.m_Vector, ManagerDB );

        {
            auto& MgrEntry = Graph;
            ImGuiTreeNodeFlags flags = 0;//(( m_pSelected == &MgrEntry ) ? ImGuiTreeNodeFlags_Selected : 0);

            bool bExpand = ImGui::TreeNodeEx( &MgrEntry, flags, "GameMgr" );
            if (ImGui::IsItemClicked()) 
            {
                //m_pSelected = &MgrEntry;
               // if( pSelectionDoc )
                {
                  //  pSelectionDoc->Single( ConstEntry.getGuid(), &ConstEntry );
                }
            }
            if( bExpand )
            {
                for( auto& Entry : ManagerDB )
                {
                    auto& MgrEntry = *Entry->m_pEntry;

                    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf 
                        | ImGuiTreeNodeFlags_NoTreePushOnOpen 
                        | (( m_pSelected == &MgrEntry ) ? ImGuiTreeNodeFlags_Selected : 0);

                    ImGui::TreeNodeEx( &MgrEntry, flags, MgrEntry.getName().m_Char.m_pValue );
                    if (ImGui::IsItemClicked()) 
                    {
                        m_pSelected = &MgrEntry;
                       // if( pSelectionDoc )
                        {
                          //  pSelectionDoc->Single( ConstEntry.getGuid(), &ConstEntry );
                        }
                    }
                }

                // Done with tree
                ImGui::TreePop();
            }
        }
    }
    ImGui::EndChild();
}
