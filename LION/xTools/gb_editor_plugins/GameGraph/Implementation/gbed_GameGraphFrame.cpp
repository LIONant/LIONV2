//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------
void frame::onRenderScene      ( void )
{
    //
    // Render a default scene... will change in the future
    //
    eng_draw::pipeline PipeLine;

    PipeLine.m_BLEND   = eng_draw::BLEND_OFF;
    PipeLine.m_ZBUFFER = eng_draw::ZBUFFER_OFF;

    auto& CmdList = m_pWindow->getDisplayCmdList( 100 );
    CmdList.Draw( PipeLine, [&]( eng_draw& Draw )
    {
        auto View = m_pWindow->getActiveView();
        Draw.ClearSampler();
        Draw.ClearScissor();
        Draw.DrawDebugMarker    ( View.getW2C(), xvector3(0), xcolor(~0) );
        Draw.DrawBBox           ( View.getW2C(), xvector3(0), xcolor::getColorCategory( 1 ) );
        Draw.DrawSphere         ( View.getW2C(), xcolor::getColorCategory( 2 ) );
    });
    m_pWindow->SubmitDisplayCmdList( CmdList );
}

//-------------------------------------------------------------------------------------------

DWORD handler(DWORD code) 
{
//log(�);
    return EXCEPTION_EXECUTE_HANDLER;
}

bool frame::onAdvanceLogic( void )
{
    if( BeginFrame() == false )
        return false;

    if( m_Document.isGraphValid() )
    {
        auto& Graph = m_Document.getGraph();
        __try 
        {
            if( m_Document.isPlaying() && m_Document.isPaused() == false )
            {
                Graph.setTimeScale( m_Document.getPlaybackSpeed() );
                Graph.setLoopMode(false);
                g_context::get().m_Scheduler.AddJobToQuantumWorld( Graph );
                g_context::get().m_Scheduler.MainThreadStartsWorking();
            }
            else
            {
                // Render the scene if we need to
                Graph.RenderEditorFrame();
            }
        } 
        __except( handler(GetExceptionCode()) )
        {
            x_assert(false);
        }
    }

    //
    // Now we can render the windows
    //
    onRenderWindows();

    //
    // Done with the frame
    //
    EndFrame();

    //
    // Let the system know that we are at the end of the frame
    //
    getMainDoc().m_Events.m_EndOfFrame.Notify();

    return true;
}


