//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

static const gbed_plugins::tab::type_harness<scene_tab> s_TypeSceneTab
{ 
    X_STR( "" ICON_FA_VIDEO_CAMERA " EDView" )
    , scene_tab::type::flags::MASK_MENU_BAR 
        | scene_tab::type::flags::MASK_CUSTOM_BGCOLOR 
        | scene_tab::type::flags::MASK_DISPLAY_ON_EMPTY_PROJECT
    , -100
    , xcolor( 0u ) 
};  

//-------------------------------------------------------------------------------------------

const scene_tab::type& scene_tab::getType( void ) 
{ 
    return s_TypeSceneTab;  
}

//-------------------------------------------------------------------------------------------

scene_tab::scene_tab( const xstring::const_str& Str, gbed_frame::base& EditorFrame )
    : gbed_plugins::tab     { Str, EditorFrame }
    , m_Document            { EditorFrame.getMainDoc().getSubDocument<document>() }
{
}

//-------------------------------------------------------------------------------------------

void scene_tab::onRender( void )
{
    static const ImVec2 ButtonSize (60, 18);
    open_popup          OpenWhichPopup = open_popup::NONE;

    //
    // Main Drop down Menu
    //
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.30f, 0.30f, 0.30f, 0.78f));
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(10.0f, 2.0f));
    if (ImGui::BeginMenuBar())
    {
        if( ImGui::BeginMenu( " " ICON_FA_ARROW_CIRCLE_DOWN " " ) )
        {
            if (ImGui::BeginMenu("Project"))
            {
                if (ImGui::BeginMenu("  New "))
                {
                    if (ImGui::MenuItem("  Project ", "CTRL+W"))
                    {
                        OpenWhichPopup = open_popup::PROJECT_CREATE_NEW_DLG;
                    }

                    // ShowExampleMenuFile();
                    ImGui::EndMenu();
                }

                if (ImGui::MenuItem("  Save ", "CTRL+W"))
                {
                    OpenWhichPopup = open_popup::PROJECT_SAVE;
                }

                if (ImGui::MenuItem("  Open ", "CTRL+W"))
                {
                    OpenWhichPopup = open_popup::PROJECT_LOAD;
                }

                if (ImGui::MenuItem("  Close ", "CTRL+W"))
                {
                    OpenWhichPopup = open_popup::PROJECT_CLOSE;
                }

                if (ImGui::MenuItem("  Recent Project ", "CTRL+W"))
                {

                }

                // ShowExampleMenuFile();
                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Windows"))
            {
                for( auto* pNext = gbed_plugins::tab::type::getHead(); pNext; pNext = pNext->m_pNext )
                {
                    if (ImGui::BeginMenu( pNext->m_TypeName.m_pValue ))
                    {
                        if (ImGui::MenuItem("   New"))
                        {
                            m_EditorFrame.getTabList().append( pNext->New( m_EditorFrame ) );
                        }

                        for( auto& pTab : m_EditorFrame.getTabList() )
                        {
                            if( pTab->getType().getGuid() != pNext->getGuid() )
                                continue;

                            if (ImGui::MenuItem( xstring::Make( "%s %p", pTab->isOpen()? ICON_FA_EYE : ICON_FA_EYE_SLASH, &*pTab )))
                            {
                                pTab->setOpen( true );
                            }
                        }

                        ImGui::EndMenu();
                    }
                }

                //ImGui::Separator();
                ImGui::EndMenu();
            }
            ImGui::EndMenu();
        }

        //
        // Play/Pause/Stop Menu
        //
        x_constexprvar float SpeedTable[]
        {
            1.0f,           // 0
            2.0f,           // 1
            4.0f,           // 2
            8.0f,           // 3
            0.50f,          // 4
            0.25f,          // 5
            0.01f,          // 6
        };

        if (m_Document.isPlaying())
        {
            static ImVec2 ButtonSize (25, 17);
            if (m_PlayModeF2F == false && m_Document.isPaused() )
            {
                ImGui::SameLine(); if (ImGui::Button(ICON_FA_PLAY, ButtonSize))
                {
                    m_Document.setPause(false);
                }
            }
            else
            {
                if ( m_PlayModeF2F == false )
                {
                    ImGui::SameLine(); if (ImGui::Button(ICON_FA_PAUSE, ButtonSize))
                    {
                        m_Document.setPause(true);
                    }
                }
                else
                {
                    m_Document.setPause(true);
                    ImGui::SameLine(); if (ImGui::Button(ICON_FA_STEP_FORWARD, ButtonSize))
                    {
                        m_Document.setPause(false);
                    }
                }
            }

            ImGui::SameLine(); if (ImGui::Button(ICON_FA_STOP, ButtonSize))
            {
                m_Document.setPause(false);
                m_Document.StopPlay();
            }
        }
        else
        {
            static const char* pStringPlay[] =
            {
                { ICON_FA_PLAY " x1"},
                { ICON_FA_PLAY " x2"},
                { ICON_FA_PLAY " x4"},
                { ICON_FA_PLAY " x8"},
                { ICON_FA_PLAY " 50%"},
                { ICON_FA_PLAY " 25%"},
                { ICON_FA_PLAY " 1%"},
            };
            static const char* pStringFFPlay[] =
            {
                { ICON_FA_STEP_FORWARD " x1"},
                { ICON_FA_STEP_FORWARD " x2"},
                { ICON_FA_STEP_FORWARD " x4"},
                { ICON_FA_STEP_FORWARD " x8"},
                { ICON_FA_STEP_FORWARD " 50%"},
                { ICON_FA_STEP_FORWARD " 25%"},
                { ICON_FA_STEP_FORWARD " 1%"},
            };

            ImGui::SameLine(); if (ImGui::Button( m_PlayModeF2F ? pStringFFPlay[m_SelectedSpeed] : pStringPlay[m_SelectedSpeed], ButtonSize))
            {
                m_Document.setPause(false);
                m_Document.Play();
            }
        }

        // Set the speed of the playback
        {
            const auto FSpeed = SpeedTable[m_SelectedSpeed];
            if( FSpeed != m_Document.getPlaybackSpeed() ) 
                m_Document.setPlaybackSpeed( FSpeed );
        }

        //
        // Playback Speed menu
        //
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 2.0f));
        ImGui::SameLine(); if (ImGui::BeginMenu(ICON_FA_SORT_DESC "##PlayGame" ))
        {
            if (ImGui::MenuItem(m_PlayModeF2F ? ICON_FA_CHECK" Frame by Frame" : "   Frame by Frame"))   { m_PlayModeF2F = !m_PlayModeF2F; }
            if (ImGui::MenuItem(m_FullWindow  ? ICON_FA_CHECK" Full Window" : "   Full Window" ) ){ m_FullWindow = !m_FullWindow; }
            ImGui::Separator();

            if (ImGui::MenuItem( m_SelectedSpeed == 0 ? ICON_FA_CHECK" Speed x1" : "   Speed x1"))   m_SelectedSpeed = 0;
            if (ImGui::BeginMenu((m_SelectedSpeed >= 2 && m_SelectedSpeed <= 4) ? ICON_FA_CHECK" Faster" : "   Faster"))
            {
                if (ImGui::MenuItem(m_SelectedSpeed == 2 ? ICON_FA_CHECK" Speed x2" : "   Speed x2"))   m_SelectedSpeed = 1;
                if (ImGui::MenuItem(m_SelectedSpeed == 3 ? ICON_FA_CHECK" Speed x4" : "   Speed x4"))   m_SelectedSpeed = 2;
                if (ImGui::MenuItem(m_SelectedSpeed == 4 ? ICON_FA_CHECK" Speed x8" : "   Speed x8"))   m_SelectedSpeed = 3;
                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu((m_SelectedSpeed >= 5 && m_SelectedSpeed <= 7) ? ICON_FA_CHECK" Slower" : "   Slower"))
            {
                if (ImGui::MenuItem(m_SelectedSpeed == 5 ? ICON_FA_CHECK" Speed 50%" : "   Speed 50%"))   m_SelectedSpeed = 4;
                if (ImGui::MenuItem(m_SelectedSpeed == 6 ? ICON_FA_CHECK" Speed 25%" : "   Speed 25%"))   m_SelectedSpeed = 5;
                if (ImGui::MenuItem(m_SelectedSpeed == 7 ? ICON_FA_CHECK" Speed  1%" : "   Speed  1%"))   m_SelectedSpeed = 6;
                ImGui::EndMenu();
            }
            ImGui::EndMenu();
        }
        ImGui::PopStyleVar();

        //
        // Reload button
        //
        static const char* pBuildType[] =
        {
            " " ICON_FA_REFRESH " Debug "
            , ICON_FA_REFRESH " Release"
        };

        ImGui::SameLine(); 
        if( false == m_ReloadAuto)
        {
            if( ImGui::Button( pBuildType[static_cast<int>(m_ReloadType)], ImVec2(80, 18) ) )
            {
                if( auto Err = m_Document.ReloadGraph(); Err.isError() )
                {
                    x_strcpy<xchar>( m_Params.m_String01, Err.getString() );
                    Err.IgnoreError();
                    OpenWhichPopup = open_popup::DISPLAY_ERROR;
                }
            }
        }
        else
        {
            ImGui::Text( "%s ", pBuildType[static_cast<int>(m_ReloadType)] );
        }

        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 2.0f));
        ImGui::SameLine(); if (ImGui::BeginMenu(ICON_FA_SORT_DESC "##Reload"))
        {
            if (ImGui::MenuItem( m_ReloadType == reload_type::DEBUG   ? ICON_FA_CHECK" Debug " : "   Debug" ))      { m_ReloadType = reload_type::DEBUG; }
            gbed_plugins::MenuToolTip( "Will reload the debug version of the game.dll" );

            if (ImGui::MenuItem( m_ReloadType == reload_type::RELEASE ? ICON_FA_CHECK" Release " : "   Release"))   { m_ReloadType = reload_type::RELEASE; }
            gbed_plugins::MenuToolTip( "Will reload the release version of the game.dll" );

            ImGui::Separator();
            if (ImGui::MenuItem( true ==  m_ReloadAuto ? ICON_FA_CHECK " Automatic " : "   Automatic " ))   { m_ReloadAuto = true; }
            gbed_plugins::MenuToolTip( "Will auto reload the game.dll when ever it detects a change" );

            if (ImGui::MenuItem( false == m_ReloadAuto ? ICON_FA_CHECK " Manual " : "   Manual "))          { m_ReloadAuto = false; }
            gbed_plugins::MenuToolTip( "Will create a button allowing the user to reload the game.dll whenever he chooses" );

            ImGui::EndMenu();
        }
        ImGui::PopStyleVar();

        //
        // End menu bar
        //
        ImGui::EndMenuBar();
    }
    ImGui::PopStyleVar();
    ImGui::PopStyleColor();

    //
    // PROJECT_CLOSE
    //
    if( OpenWhichPopup == open_popup::PROJECT_CLOSE )
    {
        auto Err = m_Document.getMainDoc().CloseProject();
        if( Err.isError() )
        {
            //todo: display error
            Err.IgnoreError();
        }
    }

    //
    // PROJECT_LOAD
    //
    if( OpenWhichPopup == open_popup::PROJECT_LOAD )
    {
        auto Err = m_Document.getMainDoc().LoadProject( gbed_filebrowser::BrowseFolder( m_Document.getMainDoc().getCurrentPath() ) );
        if( Err.isError() )
        {
            //todo: display error
            Err.IgnoreError();
        }
    }

    //
    // PROJECT_SAVE
    //
    if( OpenWhichPopup == open_popup::PROJECT_SAVE )
    {
        auto Err = m_Document.getMainDoc().SaveProject();
        if( Err.isError() )
        {
            //todo: display error
            Err.IgnoreError();
        }
    }

    //
    // PROJECT_CREATE_NEW_DLG
    //
    if( OpenWhichPopup == open_popup::PROJECT_CREATE_NEW_DLG )
    {
        std::wstring P = m_Document.getMainDoc().getCurrentPath();
        std::string Temp{ P.begin(), P.end() };
        const char* p = Temp.c_str();
        x_strcpy<char>( m_Params.m_String02, p );
        m_Params.m_String01[0] = 0;

        ImGui::OpenPopup( " New Project" );
    }
   
    if (ImGui::BeginPopupModal( " New Project", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
    {
        // Some black space
        ImGui::Text("");

        // Name of the path where the project will be created
        ImGui::Text(" Project Path: "); ImGui::SameLine(); ImGui::InputText("##NewProjecPath", m_Params.m_String02, m_Params.m_String02.getCount<int>() );

        // Let the user see very large paths
        if (ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(450.0f);
            ImGui::TextUnformatted(m_Params.m_String02);
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }

        // Let the user browse for paths
        ImGui::SameLine(); if (ImGui::Button(" Browse "))
        {
            std::wstring aPath = gbed_filebrowser::BrowseFolder( m_Document.getMainDoc().getCurrentPath() );
            std::string Temp{ aPath.begin(), aPath.end() };
            const char* p = Temp.c_str();
            if(p[0]) x_strcpy<char>( m_Params.m_String02, p);
        }

        // Collect the project name from the user
        ImGui::Text(" Project Name: "); ImGui::SameLine(); ImGui::InputText("##NewProjecName", m_Params.m_String01, m_Params.m_String01.getCount<int>() );
        ImGui::SameLine(); ImGui::Text(".lion.project  ");

        // Some additional black space
        ImGui::Text("");

        ImGui::Separator();

        if (ImGui::Button("OK", ImVec2(120, 0)))
        { 
            xwstring temp;

            temp.CopyAndConvert<xchar>( xstring::Make( "%s/%s", static_cast<const char*>(m_Params.m_String02), static_cast<const char*>(m_Params.m_String01) ) );
            auto Err = getMainDoc().CreateNewProject( gbed_document::path::project{ &temp[0] }  );

            if( Err.isError() )
            {
                //todo: display error
                Err.IgnoreError();
            }

            // Done
            ImGui::CloseCurrentPopup(); 
        }
        
        ImGui::SameLine(); if (ImGui::Button("Cancel", ImVec2(120, 0))) 
        { 
            ImGui::CloseCurrentPopup(); 
        }
        ImGui::EndPopup();
    }


    //
    // Error
    //
    if( OpenWhichPopup == open_popup::DISPLAY_ERROR )
    {
        ImGui::OpenPopup( " " ICON_FA_EXCLAMATION_CIRCLE " ERROR!" );
    }
    
    if( ImGui::BeginPopupModal( " " ICON_FA_EXCLAMATION_CIRCLE " ERROR!" ) )
    {
        ImGui::Text( m_Params.m_String01 ); 

        if( ImGui::Button( "OK", ImVec2(-1,0) ) )
        {
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }

}
