//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

// Includes 
#include <Windows.h>
#include <Dbghelp.h>
#pragma comment(lib,"Dbghelp.lib")

#include "xTools/gb_editor_plugins/GameGraph/gbed_GameGraph.h"
#include "xPlugins/imgui/imgui_impl_engbase.h"
#include "3rdParty/IconFontCppHeaders/IconsFontAwesome.h"
#include "xTools/gb_editor_plugins/FolderBrowser/FolderBrowser.h"
#include "xTools/gb_editor_plugins/Selection/gbed_Selection.h"

// CPP dependencies
namespace gbed_game_graph
{
    #include "xTools/gb_editor_plugins/GameGraph/Implementation/gbed_GameGraphDoc.cpp"
    #include "xTools/gb_editor_plugins/GameGraph/Implementation/gbed_GameGraphFrame.cpp"
    #include "xTools/gb_editor_plugins/GameGraph/Implementation/gbed_GameGraphSceneTab.cpp"
    #include "xTools/gb_editor_plugins/GameGraph/Implementation/gbed_GameGraphView.cpp"
    #include "xTools/gb_editor_plugins/GameGraph/Implementation/gbed_GameGraphTab.cpp"
}






