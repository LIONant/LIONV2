//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class frame;
class document : public gbed_document::base
{
    x_object_type( document, rtti( gbed_document::base ), is_not_copyable, is_not_movable )

public:
    using gbed_document::base::base;

    using t_game_data_path      = gbed_document::path::filepath<struct tag>;
    using t_jit_path            = gbed_document::path::filepath<struct tag>;
    using t_path                = gbed_document::path::filepath<struct tag>;
    using t_settings            = gbed_document::path::filepath<struct tag>;
    using t_graph_dll_src_path  = gbed_document::path::filepath<struct tag>;
    using t_graph_dll_dst_path  = gbed_document::path::filepath<struct tag>;
    using t_graph_pdb_dst_path  = gbed_document::path::filepath<struct tag>;

    x_constexprvar  auto        t_class_string = X_STR("GameGraph");

    struct events
    {
        x_message::event<xvector<xstring>&> m_queryReloadGameMgr;

    } m_Events;

public:

    x_incppfile                             document                ( const xstring::const_str Str, gbed_document::main& MainDoc );
    x_forceinline           bool            isGraphValid            ( void ) const { return m_xpGraph.isValid(); }
    x_forceinline           auto&           getGraph                ( void )       { return *m_xpGraph; }
    virtual                 const type&     getType                 ( void ) override;
    
    x_forceinline           auto&           setupFrame              ( frame& Frame ) { m_pFrame = &Frame; return *this; }             

    x_incppfile             err             Play                    ( void );
    x_incppfile             err             StopPlay                ( void );
    x_forceconst            bool            isPlaying               ( void ) const { return m_bPlaying; }

    x_forceinline           void            setPause                ( bool bPause ) { m_bPaused = bPause; }
    x_forceconst            bool            isPaused                ( void ) const { return m_bPaused; }

    x_forceinline           void            setPlaybackSpeed        ( float PlaybacSpeed ) { m_PlaybackSpeed = PlaybacSpeed; }
    x_forceconst            float           getPlaybackSpeed        ( void ) const { return m_PlaybackSpeed; }

    x_inline                auto            getJITPath              ( void ) const { return m_JITPath; }
    x_inline                auto            getSettingsPath         ( void ) const { return m_SettingsPath; }
    x_inline                auto            getGameDataPath         ( void ) const { return m_GameDataPath; }
    x_incppfile             err             ReloadGraph             ( void );
    x_incppfile             err             LoadGraph               ( void );

protected:

    virtual                 err             onSave                  ( void ) override { return x_error_ok(); }
    virtual                 err             onLoad                  ( void ) override;
    virtual                 err             onNew                   ( void ) override;
    virtual                 err             onClose                 ( void ) override;
    x_incppfile             void            StartUp                 ( void );

protected:

    xndptr_s<gb_game_graph::base>       m_xpGraph           {};
    t_jit_path                          m_JITPath           {};
    t_settings                          m_SettingsPath      {};
    t_game_data_path                    m_GameDataPath      {};
    xstring                             m_TempFileName      {};
    bool                                m_bPlaying          { false };
    bool                                m_bPaused           { false };
    float                               m_PlaybackSpeed     { 1.0f  };
    t_graph_dll_src_path                m_SrcGraphDLLPath   {};
    t_graph_dll_dst_path                m_GraphPDBPath      {};
    t_graph_pdb_dst_path                m_GraphDLLPath      {};
    HMODULE                             m_GraphDLL          {0}; 
    DWORD64                             m_GraphPDBModule    {0};
    frame*                              m_pFrame;
};


