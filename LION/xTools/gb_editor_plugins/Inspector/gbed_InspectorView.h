//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class entity_view 
    : public imgui_xproperty::window
{
    x_object_type( entity_view, rtti( imgui_xproperty::window ) );

public:

    class entity_item : public imgui_property::item_topfolder<>
    {
        x_object_type( entity_item, rtti(imgui_property::item_topfolder<>), is_not_copyable, is_not_movable )

    public:

        x_inline                            entity_item             ( imgui_property::window& W ) : t_parent( W ) {}
        virtual         void                onGadgetRender          ( void ) override;

    public:

        xstring m_BackupHelp    {};
    };

public:

    x_incppfile                                                 entity_view                 ( gbed_frame::base& EditorFrame ); 

protected:

    virtual         xowner<imgui_property::item_topfolder<>*>   onAddTopNode                ( void ) override { return x_new( entity_item, { *this } ); }
    virtual         void                                        onRender                    ( void ) noexcept override;
    virtual         void                                        onNotifyChange              ( imgui_property::base_item& BaseItem ) override;

protected:

    gbed_game_graph::document& m_GameGraphDoc;

    friend class entity_item;
};


