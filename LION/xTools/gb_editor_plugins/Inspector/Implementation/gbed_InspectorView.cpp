//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------

entity_view::entity_view( gbed_frame::base& EditorFrame )
    : m_GameGraphDoc{ EditorFrame.getMainDoc().getSubDocument<gbed_game_graph::document>() }
{
}

//---------------------------------------------------------------------------------------------------------

void entity_view::onRender( void ) noexcept
{
    Update();
    imgui_xproperty::window::onRender();
}

//---------------------------------------------------------------------------------------------------------

void entity_view::onNotifyChange( imgui_property::base_item& BaseItem )
{
    t_parent::onNotifyChange( BaseItem ); 
    m_GameGraphDoc.getMainDoc().NotifyChangeHappen( xstring::Make( "[INSPECTOR] %s Changed", static_cast<const char*>(BaseItem.m_Name) ), *m_pProperty );
}

//---------------------------------------------------------------------------------------------------------

void entity_view::entity_item::onGadgetRender( void )
{
    ImGui::AlignFirstTextHeightToWidgets();

    auto&                   Win         = m_Window.SafeCast<gbed_inspector::entity_view>();
    xproperty_v2::base*     pProp       = Win.getProperty();
    if( nullptr == pProp ) return;
    
    enum class order
    {
        NONE,
        EDITOR_ENTITY_COMPONENT,
        ENTITY_COMPONENT,
        COMPONENT,
    };

    //
    // Determine is we are interested in these properties
    //
    order                   Order      = order::NONE;
    gb_component::base*     pBaseComp  = nullptr;
    gb_component::entity*   pEntity    = nullptr;

    if( pProp->isKindOf<gbed_layer::document::edentity>() )
    {
        auto& EDEntity = pProp->SafeCast<gbed_layer::document::edentity>();
        pEntity   = EDEntity.m_GameGraphDoc.getGraph().findEntity( EDEntity.m_gEntity );
        pBaseComp = pEntity;

        if( pEntity && Win.getTopFolderIndex( this ) > 0 )
        {
            pProp = pEntity->getLinearPropInterface();
            Order = order::EDITOR_ENTITY_COMPONENT;
        }
        else
        {
            pProp = nullptr;
        }
    }
    else if( pProp->isKindOf<gb_component::base>() )
    {
        pBaseComp  = &pProp->SafeCast<gb_component::base>();
        pEntity    = &pBaseComp->getEntity();
        Order      = pBaseComp->isEntity() ? order::ENTITY_COMPONENT : order::COMPONENT;
    }
    else 
    {
        pProp = nullptr;
    }

    if( pProp )
    {
        gb_component::base&     Comp          = Order == order::COMPONENT? *pBaseComp : [&]() -> gb_component::base& 
                                                {
                                                    gb_component::base* pComp       = nullptr;
                                                    int                 i           = Order == order::EDITOR_ENTITY_COMPONENT? 1 : 0;
                                                    const int           iTopFolder  = Win.getTopFolderIndex( this );
                                                        
                                                    x_assert( iTopFolder != -1 );
                                                    for( pComp = pEntity; i != iTopFolder; pComp = pComp->getNextComponent(), i++ )
                                                    {
                                                        x_assert(pComp);
                                                    }

                                                    return *pComp;
                                                }();

        const bool isEntity = Comp.isKindOf<gb_component::entity>() ;

        const ImGuiStyle *  style               = &ImGui::GetStyle();
        ImGui::PushStyleColor(ImGuiCol_Button, style->Colors[ImGuiCol_Header]);

        //
        // Deal with the active button
        //
        bool bActive = Comp.isActive();
        bool Press;

        if( m_BackupHelp.isEmpty() )
        {
            m_BackupHelp = m_Help;
        }
        m_Help = X_STR("");
        if( bActive ) 
        {
            Press = ImGui::Button( ICON_FA_CHECK_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
            if (ImGui::IsItemHovered())
            {
                /*
                ImGui::BeginTooltip();
                ImGui::PushTextWrapPos(450.0f);
                ImGui::TextUnformatted("Activated Component\nComponent will be executed when the game runs");
                ImGui::PopTextWrapPos();
                ImGui::EndTooltip();
                */
                m_Window.UpdateSelected ( this );
                m_Help.Copy("Activated Component\nComponent will be executed when the game runs");
            }
        }
        else 
        {
            Press = ImGui::Button( ICON_FA_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
            if (ImGui::IsItemHovered())
            {
                /*
                ImGui::BeginTooltip();
                ImGui::PushTextWrapPos(450.0f);
                ImGui::TextUnformatted("Deactivated Component\nComponent does not execute when the game runs");
                ImGui::PopTextWrapPos();
                ImGui::EndTooltip();
                */
                m_Window.UpdateSelected ( this );
                m_Help.Copy("Deactivated Component\nComponent does not execute when the game runs");
            }
        }

        //
        // Deal with the readiness of a component
        //
        xvector<xstring> WarningList;
        auto Error = Comp.linearCheckResolve( WarningList );
        if( Error.isError() ) ImGui::TextColored( ImColor(170,0,0,255), "Error" ); 
        else                  ImGui::TextColored( ImColor(0,255,0,255), "R" );
        ImGui::SameLine(); 
        if (ImGui::IsItemHovered())
        {
            m_Window.UpdateSelected ( this );
            if( Error.isError() )
            {
                m_Help.Copy( Error.getString() );
            }
            else
            {
                m_Help.Copy("Ready Component\nThis component has all the requirements needed to work property");
            }
        }
        Error.IgnoreError();
 
        //
        // Component menu
        //
        if( Press )  Comp.setActive( !bActive );
        ImGui::SameLine();

        //
        // Deal with the menu button
        //
        ImGui::PushItemWidth(-1);
        const char* pDisplay = "Component";
        if( isEntity ) pDisplay = "Entity"; 
        ImGui::Button( pDisplay, ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) ); 

        ImGui::PopStyleColor();
        ImGui::PopItemWidth();

        if( ImGui::IsItemActive() )
            ImGui::OpenPopup("##piepopup");

        const char* Entityitems[]    = { "Paste", "Copy", "Paste New", "Add New", "Delete"  };
        const char* Componentitems[] = { "Paste", "Copy", "Move Down", "Move Up", "Delete"  };
        static int selected = -1;

        const ImVec2 Pos = ImGui::GetIO().MouseClickedPos[0];
        int n = -1;
        if( isEntity )   n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Entityitems, static_cast<int>(x_countof(Entityitems)), &selected );
        else             n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Componentitems, static_cast<int>(x_countof(Componentitems)), &selected );


        if( m_Help[0] == 0 ) m_Help = m_BackupHelp;
    }
}



