//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------

static const gbed_plugins::tab::type_harness<tab> s_TypeTab{ X_STR( "" ICON_FA_SLIDERS " Inspector") };    

//-------------------------------------------------------------------------------------------

const gbed_plugins::tab::type& tab::getType( void ) 
{ 
    return s_TypeTab;  
}

//---------------------------------------------------------------------------------------------------------

tab::tab( const xstring::const_str& Str, gbed_frame::base& EditorFrame )
    : gbed_plugins::tab( Str, EditorFrame )
    , m_GameGraphDoc{ EditorFrame.getMainDoc().getSubDocument<gbed_game_graph::document>() }
    , m_EntityView{ EditorFrame }
    , m_SelectionDoc{ EditorFrame.getMainDoc().getSubDocument<gbed_selection::document>()  }
{
    m_delegateSelChangeEntity.Connect       ( m_SelectionDoc.m_Events.m_SelChangeEntity       );
    m_delegateSelChangeBlueprint.Connect    ( m_SelectionDoc.m_Events.m_SelChangeBlueprint    );
    m_delegateSelChangeConstantData.Connect ( m_SelectionDoc.m_Events.m_SelChangeConstantData );
    m_delegateSelChangeGeneral.Connect      ( m_SelectionDoc.m_Events.m_SelChangeGeneral      );
    m_delegateQueryReloadGameMgr.Connect    ( m_GameGraphDoc.m_Events.m_queryReloadGameMgr    );
}

//---------------------------------------------------------------------------------------------------------

void tab::onRender( void ) noexcept
{
    //
    // Make sure that the selection is up to date
    //
    m_SelectionDoc.ValidateSelection();

    switch( m_Mode )
    {
        case mode::ENTITY:
        {
            if( m_SelectionDoc.getSelectedEntities().getCount() )
            {
                m_EntityView.Update();
                m_EntityView.Render();
            }
            else
            {
                // Some how the entity that we were pointing is gone
                ClearSelection();
            }
            break;
        }
        case mode::BLUEPRINT:
        {
            if( m_SelectionDoc.getSelectedBlueprints().getCount() )
            {
                m_EntityView.Update();
                m_EntityView.Render();
            }
            else
            {
                // Some how the blueprint we were pointing is gone
                ClearSelection();
            }
            break;
        }
        case mode::CONSTANT_DATA:
        {
            if( m_SelectionDoc.getSelectedConstData().getCount() )
            {
                m_EntityView.Update();
                m_EntityView.Render();
            }
            else
            {
                // Some how the blueprint we were pointing is gone
                ClearSelection();
            }
            break;
        }
        case mode::GENERAL:
        {
            m_EntityView.Update();
            m_EntityView.Render();
            break;
        }
    }
}

//---------------------------------------------------------------------------------------------------------

void tab::ClearSelection( void )
{
    m_Mode                          = mode::NONE;
    m_EntityView.SetProperty( nullptr );
}

//---------------------------------------------------------------------------------------------------------

void tab::msgSelChangeEntity( const gbed_selection::document& SelectionDoc, xproperty_v2::base* pProperty )
{
    if( SelectionDoc.getSelectedEntities().getCount() == 0 )
    {
        ClearSelection();
        return;
    }

    // Set new selection
    m_Mode              = mode::ENTITY;

    // Make the view point at our entity
    m_EntityView.SetProperty( pProperty );
}

//---------------------------------------------------------------------------------------------------------

void tab::msgSelChangeBlueprint( const gbed_selection::document& SelectionDoc, xproperty_v2::base* pProperty )
{
    if( SelectionDoc.getSelectedBlueprints().getCount() == 0 )
    {
        ClearSelection();
        return;
    }

    // Set new selection
    m_Mode              = mode::BLUEPRINT;

    // Make the view point at our blueprint
    m_EntityView.SetProperty( pProperty );
}

//---------------------------------------------------------------------------------------------------------

void tab::msgSelChangeConstData( const gbed_selection::document& SelectionDoc, xproperty_v2::base* pProperty )
{
    if( SelectionDoc.getSelectedConstData().getCount() == 0 )
    {
        ClearSelection();
        return;
    }

    // Set new selection
    m_Mode                  = mode::CONSTANT_DATA;

    // Make the view point at our blueprint
    m_EntityView.SetProperty( pProperty );
}

//---------------------------------------------------------------------------------------------------------

void tab::msgSelChangeGeneral( const gbed_selection::document& SelectionDoc, xproperty_v2::base* pProperty )
{
    // We always clear for this type of msg
    ClearSelection();

    // if the pointer is null then nothing to do since everything is clear already
    if( pProperty == nullptr )
        return;

    // Set new selection
    m_Mode              = mode::GENERAL;

    // Make the view point at our blueprint
    m_EntityView.SetProperty( pProperty );
}

//---------------------------------------------------------------------------------------------------------

void tab::msgQueryReloadGameMgr( xvector<xstring>& Issues )
{
    m_EntityView.SetProperty( nullptr );
}

