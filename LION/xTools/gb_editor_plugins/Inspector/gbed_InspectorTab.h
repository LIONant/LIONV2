//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class tab 
    : public gbed_plugins::tab
{
    x_object_type( tab, rtti( gbed_plugins::tab ) );

public:

    x_incppfile                                                 tab                         ( const xstring::const_str& Str, gbed_frame::base& EditorFrame ); 
    virtual         const gbed_plugins::tab::type&              getType                     ( void ) override;

protected:

    enum class mode : u8 
    {
        NONE
        , ENTITY
        , BLUEPRINT
        , CONSTANT_DATA
        , GENERAL
    };

protected:

    virtual         void                                        onRender                    ( void ) noexcept override;
    virtual         void                                        onCloseProject              ( void ) override { m_EntityView.SetProperty(nullptr); }
    x_incppfile     void                                        msgSelChangeEntity          ( const gbed_selection::document&,    xproperty_v2::base* pProperty );
    x_incppfile     void                                        msgSelChangeBlueprint       ( const gbed_selection::document&,    xproperty_v2::base* pProperty );
    x_incppfile     void                                        msgSelChangeConstData       ( const gbed_selection::document&,    xproperty_v2::base* pProperty );
    x_incppfile     void                                        msgSelChangeGeneral         ( const gbed_selection::document&,    xproperty_v2::base* pProperty );
    x_incppfile     void                                        msgQueryReloadGameMgr       ( xvector<xstring>& Issues );
    x_incppfile     void                                        ClearSelection              ( void );

protected:

    mode                                                    m_Mode                      { mode::NONE };
    entity_view                                             m_EntityView;
    gbed_game_graph::document&                              m_GameGraphDoc;
    gbed_selection::document&                               m_SelectionDoc;

    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>        m_delegateSelChangeEntity          { *this, &tab::msgSelChangeEntity }; 
    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>        m_delegateSelChangeBlueprint       { *this, &tab::msgSelChangeBlueprint }; 
    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>        m_delegateSelChangeConstantData    { *this, &tab::msgSelChangeConstData }; 
    x_message::delegate<tab, const gbed_selection::document&, xproperty_v2::base*>        m_delegateSelChangeGeneral         { *this, &tab::msgSelChangeGeneral }; 
    x_message::delegate<tab, xvector<xstring>&>                                           m_delegateQueryReloadGameMgr       { *this, &tab::msgQueryReloadGameMgr }; 

    friend class entity_item;
};


