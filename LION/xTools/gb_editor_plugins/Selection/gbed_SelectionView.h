//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class view 
{
public:

    x_incppfile                     view                ( document&  m_Document );
    x_incppfile     void            Render              ( const xvector<gb_component::const_data::guid>& lSelectedConstantData );
    x_inline        auto&           getDocument         ( void ) { return m_Document; }
    x_inline        auto            getSelected         ( void ) { return m_pSelected; }
    x_inline        auto            getAndClearSelected ( void ) { auto t = m_pSelected; m_pSelected = nullptr; return t; }
    x_inline        void            setTypeFilter       ( gb_component::const_data::type::guid gType ) { m_gType = gType; }
    x_inline        auto            getTypeFilter       ( void ) const { return m_gType; }

protected:
    
    enum class open_popup : u8
    {
        NONE
        , MAIN_MENU
        , FOLDER_RENAME
        , FOLDER_NEW
        , FOLDER_DELETE
        , FOLDER_MOVE
        , ENTRY_CREATE
    };

protected:

    struct popup_params
    {
        document::folder::guid      m_gFolder;
        xarray<char,128>            m_EditField;
    };

protected:

    document&                               m_Document;
    ImGuiTextFilter                         m_Filter                    {};
    popup_params                            m_Params                    {};
    gb_component::const_data*               m_pSelected                 { nullptr };
    bool                                    m_bCreated                  { false };
    gb_component::const_data::type::guid    m_gType                     { gb_component::const_data::type::guid{ nullptr } };
};

