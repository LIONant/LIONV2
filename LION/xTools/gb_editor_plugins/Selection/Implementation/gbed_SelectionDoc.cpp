//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

static const gbed_document::base::type_harness<document> s_Type
{ 
    document::t_class_string
    , -10
};    
    
//-------------------------------------------------------------------------------------------

const gbed_document::base::type& document::getType( void ) 
{ 
    return s_Type;  
}

//-------------------------------------------------------------------------------------------

document::document( const xstring::const_str Str, gbed_document::main& MainDoc ) 
    : gbed_document::base   { Str, MainDoc }
    , m_GameGraphDoc        { MainDoc.getSubDocument<gbed_game_graph::document>() }
{
}

//-------------------------------------------------------------------------------------------

document::err document::onLoad( void )
{
    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

document::err document::onSave( void )
{
    return x_error_ok(); 
}

//-------------------------------------------------------------------------------------------

document::err document::onClose( void )
{
    m_SelectedEntity.DeleteAllEntries();
    m_SelectedBlueprints.DeleteAllEntries();
    m_SelectedConstData.DeleteAllEntries();

    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::onNew( void )
{
    return x_error_ok();
} 

//-------------------------------------------------------------------------------------------

void document::Single( gb_component::entity::guid gGuid, xproperty_v2::base* pProps )
{
    if( m_SelectedEntity.getCount() == 0 && gGuid.m_Value == 0 ) return;

    m_SelectedEntity.DeleteAllEntries();
    if( gGuid.m_Value )
    {
        auto pEntity = m_GameGraphDoc.getGraph().findEntity<gb_component::entity>( gGuid );
        if( pEntity )
        {
            m_SelectedEntity.append( gGuid );
        }
    }
    m_Events.m_SelChangeEntity.Notify( *this, pProps );
}

//-------------------------------------------------------------------------------------------

void document::MultipleAdd ( gb_component::entity::guid gGuid, xproperty_v2::base* pProps )
{
    x_assert( m_SelectedBlueprints.getCount() == 0 );
    x_assert( m_SelectedConstData.getCount()  == 0 );
    x_assert( m_SelectedEntity.getCount()     >  0 );

    // Search to make sure we dont add duplicates
    bool bAdd = true;
    for( auto& gE : m_SelectedEntity )
    {
        if( gE == gGuid ) 
        {
            bAdd = false;
            break;
        }
    }

    if( bAdd ) 
    {
        m_SelectedEntity.append( gGuid );
        m_Events.m_SelChangeEntity.Notify( *this, pProps );
    }
}

//-------------------------------------------------------------------------------------------

void document::MultipleRemove ( gb_component::entity::guid gGuid, xproperty_v2::base* pProps )
{
    x_assert( m_SelectedBlueprints.getCount() == 0 );
    x_assert( m_SelectedConstData.getCount()  == 0 );
    x_assert( m_SelectedEntity.getCount()     >  0 );

    // Search and remove entry
    for( auto& gE : m_SelectedEntity )
    {
        if( gE == gGuid ) 
        {
            m_SelectedEntity.DeleteWithCollapse( m_SelectedEntity.getIndexByEntry(gE) ); 
            m_Events.m_SelChangeEntity.Notify( *this, pProps );
            break;
        }
    }
}

//-------------------------------------------------------------------------------------------

void document::Single ( gb_blueprint::guid gGuid, xproperty_v2::base* pProps )
{
    if( m_SelectedBlueprints.getCount() == 0 && gGuid.m_Value == 0 ) return;

    m_SelectedBlueprints.DeleteAllEntries();
    if( gGuid.m_Value )
    {
        auto pBlueprint = m_GameGraphDoc.getGraph().m_BlueprintDB.Find( gGuid );
        if( pBlueprint )
        {
            m_SelectedBlueprints.append( gGuid );
        }
    }
    m_Events.m_SelChangeBlueprint.Notify( *this, pProps );
}

//-------------------------------------------------------------------------------------------

void document::MultipleAdd ( gb_blueprint::guid gGuid, xproperty_v2::base* pProps )
{
}

//-------------------------------------------------------------------------------------------

void document::MultipleRemove ( gb_blueprint::guid gGuid, xproperty_v2::base* pProps )
{
}

//-------------------------------------------------------------------------------------------

void document::Single( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps )
{
    if( m_SelectedConstData.getCount() == 0 && gGuid.m_Value == 0 ) return;

    m_SelectedConstData.DeleteAllEntries();
    if( gGuid.m_Value )
    {
        auto pConstData = m_GameGraphDoc.getGraph().m_ConstDataDB.Find( gGuid );
        if( pConstData )
        {
            m_SelectedConstData.append( gGuid );
        }
    }
    m_Events.m_SelChangeConstantData.Notify( *this, pProps );
}

//-------------------------------------------------------------------------------------------

void document::MultipleAdd( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps )
{
}

//-------------------------------------------------------------------------------------------
void document::MultipleRemove( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps )
{
}

//-------------------------------------------------------------------------------------------

void document::ValidateSelection( void )
{
    //
    // Validate Entities
    // 
    for( int i=0; i<m_SelectedEntity.getCount<int>(); ++i )
    {
        if( nullptr == m_GameGraphDoc.getGraph().findEntity<gb_component::entity>( m_SelectedEntity[i] ) )
        {
            m_SelectedEntity.DeleteWithSwap(i);
            --i;
        }
    }

    //
    // Validate Blueprints
    // 
    for( int i=0; i<m_SelectedBlueprints.getCount<int>(); ++i )
    {
        if( nullptr == m_GameGraphDoc.getGraph().m_BlueprintDB.Find( m_SelectedBlueprints[i] ) )
        {
            m_SelectedBlueprints.DeleteWithSwap(i);
            --i;
        }
    }

    //
    // Validate ConstData
    // 
    for( int i=0; i<m_SelectedConstData.getCount<int>(); ++i )
    {
        if( nullptr == m_GameGraphDoc.getGraph().m_ConstDataDB.Find( m_SelectedConstData[i] ) )
        {
            m_SelectedConstData.DeleteWithSwap(i);
            --i;
        }
    }

}

//-------------------------------------------------------------------------------------------

void document::Single( xproperty_v2::base* pProps )
{
    m_Events.m_SelChangeGeneral.Notify( *this, pProps );    
}



