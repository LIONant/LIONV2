//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class document : public gbed_document::base
{
    x_object_type( document, rtti(gbed_document::base), is_not_copyable, is_not_movable )

public:
    using base::base;

    x_constexprvar  auto    t_class_string          = X_STR("Selection");

    struct events
    {
        x_message::event<const document&,   xproperty_v2::base*>    m_SelChangeGeneral;
        x_message::event<const document&,   xproperty_v2::base*>    m_SelChangeEntity;
        x_message::event<const document&,   xproperty_v2::base*>    m_SelChangeBlueprint;
        x_message::event<const document&,   xproperty_v2::base*>    m_SelChangeConstantData;
    };

public:

    x_incppfile                                 document                ( const xstring::const_str Str, gbed_document::main& MainDoc );
    virtual                 const type&         getType                 ( void ) override;

    x_incppfile             void                Single                  ( gb_component::entity::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                AndSingle               ( gb_component::entity::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                MultipleAdd             ( gb_component::entity::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                MultipleRemove          ( gb_component::entity::guid gGuid, xproperty_v2::base* pProps );
    x_inline                auto&               getSelectedEntities     ( void ) const { return m_SelectedEntity; }
                        
    x_incppfile             void                Single                  ( gb_blueprint::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                AndSingle               ( gb_blueprint::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                MultipleAdd             ( gb_blueprint::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                MultipleRemove          ( gb_blueprint::guid gGuid, xproperty_v2::base* pProps );
    x_inline                auto&               getSelectedBlueprints   ( void ) const { return m_SelectedBlueprints; }
                        
    x_incppfile             void                Single                  ( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                AndSingle               ( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                MultipleAdd             ( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps );
    x_incppfile             void                MultipleRemove          ( gb_component::const_data::guid gGuid, xproperty_v2::base* pProps );
    x_inline                auto&               getSelectedConstData    ( void ) const { return m_SelectedConstData; }

    x_incppfile             void                Single                  ( xproperty_v2::base* pProps );

    x_incppfile             void                ValidateSelection       ( void );

public:

    events m_Events;

protected:

    virtual                 err                 onSave                  ( void ) override; 
    virtual                 err                 onLoad                  ( void ) override;
    virtual                 err                 onNew                   ( void ) override; 
    virtual                 err                 onClose                 ( void ) override;
    
protected:

    gbed_game_graph::document&                      m_GameGraphDoc;
    xvector<gb_component::entity::guid>             m_SelectedEntity        {};
    xvector<gb_blueprint::guid>                     m_SelectedBlueprints    {};
    xvector<gb_component::const_data::guid>         m_SelectedConstData     {};
};


