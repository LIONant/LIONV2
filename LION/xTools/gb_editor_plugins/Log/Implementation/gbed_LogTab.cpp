//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------
static const gbed_plugins::tab::type_harness<tab> s_Type
{ 
    X_STR( "" ICON_FA_STACK_OVERFLOW " Log" )
    , tab::type::flags::MASK_DISPLAY_ON_EMPTY_PROJECT 
};    

//-------------------------------------------------------------------------------------------
const gbed_plugins::tab::type& tab::getType( void ) 
{ 
    return s_Type;  
}

//-------------------------------------------------------------------------------------------

tab::tab( const xstring::const_str& Str, gbed_frame::base& EditorFrame ) 
    : gbed_plugins::tab{ Str, EditorFrame }
    , m_LogDoc{ EditorFrame.getMainDoc().getSubDocument<gbed_log::document>() }
{
}

//-------------------------------------------------------------------------------------------

void tab::onRender( void )
{
    open_popup WhichPopupToOpen = open_popup::NONE;

    //
    // Main menu
    //
    if( ImGui::Button( ICON_FA_ARROW_CIRCLE_DOWN ) ) // "Menu"
    {
    }

    //
    // Clear the log
    // This needs to be done before the lock
    ImGui::SameLine(); if( ImGui::Button( "Reset Log" ) )
    {
        m_LogDoc.Clear();
    }
    
    //
    // Lock data here
    //
    x_lk_guard_as_const_get_as_const( m_LogDoc.getLockedKeyData(), KeyData );
        
    ImGui::SameLine(); if( ImGui::Button( ICON_FA_CLIPBOARD ) )
    {
        WhichPopupToOpen = open_popup::COPY_LOG_TO_CLIPBOARD;
    }

    ImGui::SameLine(); if( ImGui::Button( ICON_FA_TIMES ) )
    {
        m_Filter.Clear();
    }

    ImGui::SameLine(); m_Filter.Draw(ICON_FA_SEARCH, -24.0f);
    ImGui::Separator();


    ImGui::BeginChild("scrolling", ImVec2(0,0), false, ImGuiWindowFlags_HorizontalScrollbar);
    if( WhichPopupToOpen == open_popup::COPY_LOG_TO_CLIPBOARD )
    {
        ImGui::LogToClipboard();
    }

    if( m_Filter.IsActive() )
    {
        const char* buf_begin   = KeyData.m_Buf.begin();
        const char* line        = buf_begin;

        for( int line_no = 0; line != nullptr; line_no++ )
        {
            const char* line_end = (line_no < KeyData.m_LineOffsets.Size) ? buf_begin + KeyData.m_LineOffsets[line_no] : nullptr;

            if( m_Filter.PassFilter(line, line_end) )
            {
                ImGui::TextUnformatted(line, line_end);
            }

            line = line_end && line_end[1] ? line_end + 1 : nullptr;
        }
    }
    else
    {
        ImGui::TextUnformatted( KeyData.m_Buf.begin() );
    }

    //
    // Scroll down to the last line when new data comes in
    //
    if( m_LastLineCount != KeyData.m_LineOffsets.Size )
    {
        ImGui::SetScrollHere(1.0f);
        m_LastLineCount = KeyData.m_LineOffsets.Size;
    }

    ImGui::EndChild();
}

