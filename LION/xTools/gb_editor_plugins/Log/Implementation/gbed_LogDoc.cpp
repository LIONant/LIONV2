//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------
static const gbed_document::base::type_harness<document> s_TypeDoc{ document::t_class_string };    
    
//-------------------------------------------------------------------------------------------

const gbed_document::base::type& document::getType( void ) 
{ 
    return s_TypeDoc;  
}

//-------------------------------------------------------------------------------------------

document::document( const xstring::const_str Str, gbed_document::main& MainDoc ) 
    : gbed_document::base   { Str, MainDoc }
{
    m_deletegateNewLogMessage.Connect( MainDoc.m_Events.m_LogMsg );
}

//-------------------------------------------------------------------------------------------

void document::Clear( void )     
{
    x_lk_guard_get( m_LockedKeyData, KeyData );
    KeyData.m_Buf.clear(); 
    KeyData.m_LineOffsets.clear(); 
}

//-------------------------------------------------------------------------------------------

document::err document::onSave( void ) 
{
    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::onLoad( void ) 
{
    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

document::err document::onNew( void ) 
{
    return x_error_ok();
}

//-------------------------------------------------------------------------------------------
document::err document::onClose( void ) 
{
    return x_error_ok();
}

//-------------------------------------------------------------------------------------------

void document::msgNewLogMessage( xstring& Str )
{
    x_lk_guard_get( m_LockedKeyData, KeyData );

    auto old_size = KeyData.m_Buf.size();
    
    KeyData.m_Buf.append( Str );

    for( int new_size = KeyData.m_Buf.size(); old_size < new_size; old_size++ )
    {
        if( KeyData.m_Buf[old_size] == '\n' )
            KeyData.m_LineOffsets.push_back(old_size);
    }
}

