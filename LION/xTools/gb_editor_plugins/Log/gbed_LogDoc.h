//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class document : public gbed_document::base
{
    x_object_type( document, rtti( gbed_document::base ), is_not_copyable, is_not_movable )

    x_constexprvar  auto        t_class_string      = X_STR("Log");

public:

    using gbed_document::base::base;

    struct keydata
    {
        ImGuiTextBuffer                     m_Buf;
        ImVector<int>                       m_LineOffsets;        // Index to lines offset
    };

    using lock              = x_lk_spinlock::base<x_lk_spinlock::non_reentrance>;
    using keydata_locked    = x_locked_object< keydata, lock >;

public:

    x_incppfile                                     document            ( const xstring::const_str Str, gbed_document::main& MainDoc );
    x_incppfile             void                    Clear               ( void );
    x_incppfile             auto&                   getLockedKeyData    ( void ) const { return m_LockedKeyData; }
    virtual                 const type&             getType             ( void ) override;

protected:

    virtual                 err                     onSave              ( void ) override;
    virtual                 err                     onLoad              ( void ) override;
    virtual                 err                     onNew               ( void ) override;
    virtual                 err                     onClose             ( void ) override;
    x_incppfile             void                    msgNewLogMessage    ( xstring& Str );

protected:

    keydata_locked                          m_LockedKeyData;

    x_message::delegate<document,xstring&>  m_deletegateNewLogMessage          { *this, &document::msgNewLogMessage };
};




