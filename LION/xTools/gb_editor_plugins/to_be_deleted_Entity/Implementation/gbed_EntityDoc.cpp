//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

namespace gbed_layer
{
    //-------------------------------------------------------------------------------------------
    static const gbed_document::base::type_harness<document> s_TypeDoc{ document::t_class_string };    
    
    //-------------------------------------------------------------------------------------------

    const gbed_document::base::type& document::getType( void ) 
    { 
        return s_TypeDoc;  
    }

    //-------------------------------------------------------------------------------------------

    document::document( const xstring::const_str Str, gbed_document::main& MainDoc ) 
        : gbed_document::base   { Str, MainDoc }
        , m_GameGraphDoc        { MainDoc.getSubDocument<gbed_game_graph::document>() } 
    {
        m_FLayerDBase.Init( 1024 );
        m_FZoneDBase.Init( 1024 );
        m_ZoneDBase.Init( 1024 );
    }

    //-------------------------------------------------------------------------------------------

    document::err document::onNew( void ) 
    {
        m_LayerPath     = m_MainDoc.getJitMainPath() + std_fs::path("/Layers");
        m_ScenePath     = m_MainDoc.getJitMainPath() + std_fs::path("/Scenes");
        m_ZonePath      = m_MainDoc.getJitMainPath() + std_fs::path("/Zones");

        // Create the actual directory
        if (auto Error = m_MainDoc.CreateDir(m_ScenePath); Error.isError()) 
            return Error;

        if (auto Error = m_MainDoc.CreateDir(m_LayerPath); Error.isError()) 
            return Error;

        if (auto Error = m_MainDoc.CreateDir(m_ZonePath); Error.isError()) 
            return Error;

        return x_error_ok(); 
    }

    //-------------------------------------------------------------------------------------------

    document::err document::onLoad( void ) 
    {
        return x_error_ok(); 
    }

    //-------------------------------------------------------------------------------------------

    document::scene& document::CreateNewScene( void )
    {
        // clear all the data
        m_FLayerDBase.DeleteAllEntries();
        m_FZoneDBase.DeleteAllEntries();
        m_ActiveZone.m_Value = 0;
        m_Scene.m_lLayers.DeleteAllEntries();

        // Create new scene
        m_Scene.m_bChanged = true;
        m_Scene.m_Guid.Reset();
        m_Scene.m_Name = X_STR("New Scene");

        return m_Scene;
    }

    //-------------------------------------------------------------------------------------------

    document::err document::onSave( void ) 
    {
        //
        // Saving Scene
        //
        if( m_Scene.m_bChanged )
        {
            xtextfile   File;
            xstring     Path;
            xstring     FileName;
            xstring     FilePath;

            // Convert to the path first
            Path.CopyAndConvert( &static_cast<std::wstring>(m_ScenePath)[0] );

            // Create file name 
            m_Scene.m_Guid.getStringHex( FileName );
            FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
            // Create finally the full filename path
            FilePath.setup( "%s/%s.scene.txt", Path, FileName );

            if( auto Err = File.openForWriting( FilePath.To<xwchar>() ); Err.isError() )
            {
                File.WriteRecord( "Scene" );
                File.WriteField( "Guid:g", m_Scene.m_Guid );
                File.WriteField( "Name:s", (const char*)m_Scene.m_Name );
                File.WriteLine();

                File.WriteRecord( "Layers", m_Scene.m_lLayers.getCount<int>() );
                for( auto& EntryLayer : m_Scene.m_lLayers )
                {
                    File.WriteField( "GUID:g",      EntryLayer.m_gLayer );
                    File.WriteField( "bLoaded:h",   EntryLayer.m_bLoaded );
                    File.WriteLine();
                }
            }
            else
            {
                X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene, %s", Err.getString() );
                return x_error_code( errors, ERR_FAILURE, "Fail to save scene" );
            }
        }

        //
        // Saving Root layers (real layers)
        //
        {
            x_lk_guard_as_const_get_as_const( m_FLayerDBase.m_Vector, LayerDBase );

            xstring     Path;

            // Convert to the path first
            Path.CopyAndConvert( &static_cast<std::wstring>(m_LayerPath)[0] );

            for( auto& xpNode : LayerDBase )
            {
                auto& Entry = *xpNode->m_pEntry;

                if( Entry.m_gParent.m_Value == 0 )      continue;

                xtextfile   File;
                xstring     FileName;
                xstring     FilePath;
            
                // Create file name 
                Entry.m_Guid.getStringHex( FileName );
                FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                // Create finally the full filename path
                FilePath.setup( "%s/%s.layer", Path, FileName );

                // Create directory if it does not exists
                create_directory( std_fs::path{ std::wstring{ FilePath.To<xwchar>() } });

                std::function<document::err(zone_folder& Entry)> SaveZone = [&]( zone_folder& E ) -> document::err
                {
                    if( E.m_bChanged ) 
                    {
                        xtextfile   File;
                        xstring     FileName;
                        xstring     FilePath;

                        // Create file name 
                        Entry.m_Guid.getStringHex( FileName );
                        FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                        // Create finally the full filename path
                        FilePath.setup( "%s/%s.fzone.txt", FilePath, FileName );

                        if( auto Err = File.openForWriting( FilePath.To<xwchar>() ); Err.isError() )
                        {
                            E.Save( File );
                        }
                        else
                        {
                            X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.zones, %s", Err.getString() );
                            return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers.zones" );
                        }
                    }

                    E.m_bChanged = false;

                    return x_error_ok(); 
                };

                std::function<document::err(zone_folder& Entry)> SaveZoneChildren = [&]( zone_folder& Entry ) -> document::err
                {
                    for( const auto& gEntry : Entry.m_lgFolders )
                    {
                        auto& E = m_FZoneDBase.getEntry( gEntry );

                        // Save the zone
                        if( auto Err = SaveZone(E); Err.isError() ) return Err;

                        // Recurse for children
                        if( auto Err = SaveZoneChildren( E ); Err.isError() ) return Err;
                    }

                    return x_error_ok(); 
                };

                std::function<document::err(layer_folder& Entry)> SaveLayerChildren = [&]( layer_folder& Entry ) -> document::err
                {
                    //
                    // Save all the folders
                    //
                    for( const auto& gEntry : Entry.m_lgFolders )
                    {
                        auto& E = m_FLayerDBase.getEntry( gEntry );
                        if( E.m_bChanged ) 
                        {
                            xtextfile   File;
                            xstring     FileName;
                            xstring     FilePath;

                            // Create file name 
                            Entry.m_Guid.getStringHex( FileName );
                            FileName[ x_strstr<xchar>( &FileName[0], ":" ) ] = '-';
            
                            // Create finally the full filename path
                            FilePath.setup( "%s/%s.flayer.txt", FilePath, FileName );

                            if( auto Err = File.openForWriting( FilePath.To<xwchar>() ); Err.isError() )
                            {
                                E.Save( File );
                            }
                            else
                            {
                                X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                                return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                            }
                        }

                        // Recurse the rest of the children
                        if( auto Err = SaveLayerChildren( E ); Err.isError() ) return Err;
                        E.m_bChanged = false;
                    }

                    //
                    // Save all the Zones
                    //
                    for( const auto& gEntry : Entry.m_lgZoneFolder )
                    {
                        auto& E = m_FZoneDBase.getEntry( gEntry );

                        // Save the zone
                        if( auto Err = SaveZone(E); Err.isError() ) return Err;

                        // Save zone children
                        if( auto Err = SaveZoneChildren( E ); Err.isError() ) return Err;
                        E.m_bChanged = false;
                    }

                    return x_error_ok(); 
                };

                //
                // Save the layer
                //
                if( Entry.m_bChanged )
                {
                    xstring     RootPath;
                    
                    // Create the root file for this layer
                    RootPath.setup( "%s/Root.layer.txt", FilePath );

                    if( auto Err = File.openForWriting( RootPath.To<xwchar>() ); Err.isError() )
                    {
                        Entry.Save( File );
                    }
                    else
                    {
                        X_LOG_CHANNEL_ERROR( m_MainDoc.m_LogChannel, "Error saving scene.layers.properties, %s", Err.getString() );
                        return x_error_code( errors, ERR_FAILURE, "Fail to save scene.layers" );
                    }
                }

                //
                // Save all the Children from the layer
                //
                if( auto Err = SaveLayerChildren( Entry ); Err.isError() ) return Err;
                Entry.m_bChanged = false;
            }
        }

        return x_error_ok(); 
    }

    //-------------------------------------------------------------------------------------------

    document::zone_folder& document::CreateFZone
    (   
        layer_folder::guid          gParentLayerOrFolder
        , zone::guid                gZone
    )
    {
        xndptr_s<zone_folder> xpZone;
        xpZone.New();
        xpZone->m_Guid.Reset();
        xpZone->m_Name     = X_STR( "New Zone" );
        xpZone->m_bChanged = true;
        xpZone->m_ZoneGuid = gZone;

        // append to parent this case is the layer
        auto& Layer = m_FLayerDBase.getEntry( gParentLayerOrFolder );
        Layer.m_lgZoneFolder.append( xpZone->m_Guid );

        // Transfer ownership to the database
        auto* pPtr = xpZone.getPtr();
        m_FZoneDBase.RegisterAndTransferOwner( xpZone );

        return *pPtr; 
    }

    //-------------------------------------------------------------------------------------------

    document::zone_folder& document::CreateFZone
    (   
        zone_folder::guid           gParentZoneOrFolder
        , zone::guid                gZone
    )
    {
        xndptr_s<zone_folder> xpZone;
        xpZone.New();
        xpZone->m_Guid.Reset();
        xpZone->m_Name     = X_STR( "New Zone" );
        xpZone->m_bChanged = true;
        xpZone->m_ZoneGuid = gZone;
        xpZone->m_gParent  = gParentZoneOrFolder;

        // append to parent
        if( gParentZoneOrFolder.m_Value )
        {
            auto& ZoneFolder = m_FZoneDBase.getEntry( gParentZoneOrFolder );
            ZoneFolder.m_lgFolders.append( xpZone->m_Guid );
            if( gZone.m_Value == 0 ) xpZone->m_ZoneGuid = ZoneFolder.m_ZoneGuid;
        }

        // Transfer ownership to the database
        auto* pPtr = xpZone.getPtr();
        m_FZoneDBase.RegisterAndTransferOwner( xpZone );

        return *pPtr; 
    }

    //-------------------------------------------------------------------------------------------
    
    document::zone_folder& document::CreateFZoneFolder( zone_folder::guid  gParentZoneOrFolder )
    {
        auto& ZoneFolder = CreateFZone( gParentZoneOrFolder );
        ZoneFolder.m_Name = X_STR( "New Folder" );
        x_assert( ZoneFolder.m_ZoneGuid.m_Value == 0 );
        return ZoneFolder; 
    }

    //-------------------------------------------------------------------------------------------
    
    document::layer_folder& document::CreateFLayerFolder( layer_folder::guid gParentLayerOrFolder )
    {
        xndptr_s<layer_folder> xpLayer;
        xpLayer.New();
        xpLayer->m_Guid.Reset();
        xpLayer->m_bChanged       = true;
        xpLayer->m_gParent        = gParentLayerOrFolder;
        xpLayer->m_Name           = X_STR( "New Folder" );

        // append to parent
        auto& LayerParent = m_FLayerDBase.getEntry( gParentLayerOrFolder );
        LayerParent.m_lgFolders.append( xpLayer->m_Guid ); 

        // Transfer ownership to the database
        auto* pPtr = xpLayer.getPtr();
        m_FLayerDBase.RegisterAndTransferOwner( xpLayer );

        return *pPtr; 
    }

    //-------------------------------------------------------------------------------------------
    
    document::layer_folder& document::CreateFLayer( void )
    {
        xndptr_s<layer_folder> xpLayer;
        xpLayer.New();
        xpLayer->m_Guid.Reset();
        xpLayer->m_Name             = X_STR( "New Layer" );
        xpLayer->m_bChanged         = true;

        // append to parent
        auto& LayerEntry = m_Scene.m_lLayers.append();
        LayerEntry.m_bLoaded = true;
        LayerEntry.m_gLayer  = xpLayer->m_Guid;

        // Transfer ownership to the database
        auto* pPtr = xpLayer.getPtr();
        m_FLayerDBase.RegisterAndTransferOwner( xpLayer );

        return *pPtr; 
    }
}