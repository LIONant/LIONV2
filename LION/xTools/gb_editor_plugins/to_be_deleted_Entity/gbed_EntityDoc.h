//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

namespace gbed_layer
{
    class document : public gbed_document::base
    {
        x_object_type( document, rtti( gbed_document::base ), is_not_copyable, is_not_movable )
    
    public:

        using gbed_document::base::base;

        using                       t_layer_path        = gbed_document::path::filepath< struct tag >;
        using                       t_scene_path        = gbed_document::path::filepath< struct tag >;
        using                       t_zone_path         = gbed_document::path::filepath< struct tag >;
        using                       t_entity_path       = gbed_document::path::filepath< struct tag >;
        using                       t_edentity_path     = gbed_document::path::filepath< struct tag >;

        x_constexprvar  auto        t_class_string      = X_STR("Layer");
        
        struct zone
        {
            using guid = gb_component::zone::guid; 
            guid                                        m_Guid                  { nullptr };
            xstring                                     m_Name                  {};
        };

        struct edentity
        {
            using guid = gb_component::entity::guid;
            guid                                        m_Guid                  { nullptr };
            xstring                                     m_Name                  {};
        };

        struct zone_folder 
        {
            using guid = xguid<zone_folder>;

            auto getGuid( void ) { return m_Guid; }

            xstring                                     m_Name                  {};
            bool                                        m_bChanged              { false };
            bool                                        m_bExpanded             { false };
            guid                                        m_Guid                  {};
            guid                                        m_gParent               { nullptr };       // if parent is null then this is a zone 
            
            zone::guid                                  m_ZoneGuid              { nullptr };        // If this is null this folder is not a child of a zone

            xvector<edentity::guid>                     m_lgEntity              {};
            xvector<guid>                               m_lgFolders             {};

            void Save( xtextfile& File )
            {
                //
                // Save header
                //
                File.WriteRecord( "ZoneHeader" );
                File.WriteField ( "Name:s", m_Name );
                File.WriteField ( "Guid:g", m_Guid );
                File.WriteField ( "gParent:g", m_gParent );
                File.WriteField ( "ZoneGuid:g", m_ZoneGuid );
                File.WriteLine();

                File.WriteRecord( "Folders", m_lgFolders.getCount<int>() );
                for( auto& E : m_lgFolders )
                {
                    File.WriteField( "gFolders:g", E );
                    File.WriteLine();
                }

                File.WriteRecord( "Entities", m_lgEntity.getCount<int>() );
                for( auto& E : m_lgEntity )
                {
                    File.WriteField( "gEntity:g", E );
                    File.WriteLine();
                }
            }
        };

        struct layer_folder
        {
            using guid = xguid<layer_folder>;

            auto getGuid( void ) { return m_Guid; }

            xstring                                     m_Name                  {};
            bool                                        m_bChanged              { false };
            bool                                        m_bExpanded             { false };
            guid                                        m_Guid                  {};
            guid                                        m_gParent               { nullptr };        // If this is null it must be the a layer
            xvector<zone_folder::guid>                  m_lgZoneFolder          {};                 // Zones
            xvector<guid>                               m_lgFolders             {};                 // Layer Folders

            void Save( xtextfile& File )
            {
                //
                // Save header
                //
                File.WriteRecord( "LayerHeader" );
                File.WriteField ( "Name:s", m_Name );
                File.WriteField ( "Guid:g", m_Guid );
                File.WriteField ( "gParent:g", m_gParent );
                File.WriteLine();

                File.WriteRecord( "Folders", m_lgFolders.getCount<int>() );
                for( auto& E : m_lgFolders )
                {
                    File.WriteField( "gFolders:g", E );
                    File.WriteLine();
                }

                File.WriteRecord( "ZoneFolders", m_lgZoneFolder.getCount<int>() );
                for( auto& E : m_lgZoneFolder )
                {
                    File.WriteField( "gZoneFolder:g", E );
                    File.WriteLine();
                }
            }
        };

        struct scene
        {
            using guid = xguid<scene>;

            struct entry
            {
                layer_folder::guid                      m_gLayer                { nullptr };
                bool                                    m_bLoaded               { true };
            };

            guid                                        m_Guid                  { nullptr };
            xstring                                     m_Name                  { X_STR("No Scene Loaded") };
            xvector<entry>                              m_lLayers               {};
            bool                                        m_bChanged              { false };
        };

    public:

        x_incppfile                             document            ( const xstring::const_str Str, gbed_document::main& MainDoc );
        virtual                 const type&     getType             ( void ) override;
 
        x_incppfile             scene&          CreateNewScene      ( void );
        x_incppfile             layer_folder&   CreateFLayer        ( void );
        x_incppfile             zone_folder&    CreateFZone         (   layer_folder::guid          gParentLayerOrFolder = layer_folder::guid{nullptr}
                                                                        , zone::guid                gZone                = zone::guid{zone::guid::RESET}
                                                                    );
        x_incppfile             zone_folder&    CreateFZone         (   zone_folder::guid           gParentZoneOrFolder  = zone_folder::guid{nullptr}
                                                                        , zone::guid                gZone                = zone::guid{zone::guid::RESET}
                                                                    );
        x_incppfile             layer_folder&   CreateFLayerFolder  ( layer_folder::guid gParentLayerOrFolder );
        x_incppfile             zone_folder&    CreateFZoneFolder   ( zone_folder::guid  gParentZoneOrFolder );

        x_inline                auto&           getFLayerDBase      ( void ) const { return m_FLayerDBase; }
        x_inline                auto&           getFLayerDBase      ( void )       { return m_FLayerDBase; }
        x_inline                auto&           getFZoneDBase       ( void ) const { return m_FZoneDBase; }
        x_inline                auto&           getFZoneDBase       ( void )       { return m_FZoneDBase; }
        x_inline                auto&           getScene            ( void ) const { return m_Scene; }
        x_inline                auto&           getGameGraphDoc     ( void )       { return m_GameGraphDoc; }
        x_inline                auto&           getZoneDBase        ( void ) const { return m_ZoneDBase; }
        x_inline                auto&           getZoneDBase        ( void )       { return m_ZoneDBase; }
        x_inline                auto&           getEDEntityDBase    ( void ) const { return m_EDEntityDBase; }
        x_inline                auto&           getEDEntityDBase    ( void )       { return m_EDEntityDBase; }

    protected:

        virtual err     onSave  ( void ) override;
        virtual err     onLoad  ( void ) override;
        virtual err     onNew   ( void ) override;

    protected:

        x_ll_dbase<layer_folder,layer_folder::guid>     m_FLayerDBase;
        x_ll_dbase<zone_folder,zone_folder::guid>       m_FZoneDBase;
        x_ll_dbase<zone,zone::guid>                     m_ZoneDBase;
        x_ll_dbase<edentity,edentity::guid>             m_EDEntityDBase;
        zone_folder::guid                               m_ActiveZone{ nullptr };
        t_layer_path                                    m_LayerPath;
        t_scene_path                                    m_ScenePath;
        t_zone_path                                     m_ZonePath;
        gbed_game_graph::document&                      m_GameGraphDoc;
        scene                                           m_Scene;
    };
}