//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "xPlugins/imgui/imgui_impl_engbase.h"
#include "xPlugins/imgui/imguiThemeLionStandard.h"
#include "xPlugins/imgui/imguiXProperty.h"
#include "3rdParty/IconFontCppHeaders/IconsFontAwesome.h"
#include "3rdParty/IconFontCppHeaders/IconsKenney.h"
#include "3rdParty/IconFontCppHeaders/IconsMaterialDesign.h"
#include "xTools/gb_editor_plugins/FolderBrowser/FolderBrowser.h"

namespace gbed_frame
{
    //-------------------------------------------------------------------------------------------
    base::~base( void )
    {
        //
        // Shut down
        //
        ImGui_engBase_Shutdown();
    }

    //-------------------------------------------------------------------------------------------
    base::base( gbed_document::main& Document ) 
        : m_Document{ Document }
    {
        m_delegatemsgCloseProject.Connect( m_Document.m_Events.m_CloseProject );
    }

    //-------------------------------------------------------------------------------------------

    base& base::setupEngWindow( eng_window& Window )  
    { 
        //
        // Save the window
        //
        m_pWindow = &Window;

        //
        // Initialize the view
        //
        m_View.setFov         ( xradian{ 91.5_deg } );
        m_View.setFarZ        ( 256.0f );
        m_View.setNearZ       ( 0.1f );
        m_View.LookAt( 2.5f, xradian3( 0_deg, 0_deg, 0_deg ), xvector3(0) );

        // User Initialize
        onSetup(); 

        return *this;
    }

    //-------------------------------------------------------------------------------------------
    bool base::AdvanceLogic( void )
    { 
        return onAdvanceLogic(); 
    }

    //-------------------------------------------------------------------------------------------

    void base::onSetup( void )
    {
        InitializeImGui();
    }

    //-------------------------------------------------------------------------------------------
    void base::InitializeImGui( void )
    {
        ImGui_engBase_Init(m_pWindow->getDevice(), *m_pWindow, false);
        imguiThemeLionStandard();

        //
        // Add fonts
        //
        ImGuiIO& io = ImGui::GetIO();
        io.Fonts->AddFontDefault();

        // merge in icons from Font Awesome
        {
            static const ImWchar icons_ranges_fontawesome[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
            static const ImWchar icons_ranges_kenney[] = { ICON_MIN_KI, ICON_MAX_KI, 0 };
            static const ImWchar icons_ranges_materialdesign[] = { ICON_MIN_MD, ICON_MAX_MD, 0 };
            ImFontConfig icons_config;
            icons_config.MergeMode = true;
            icons_config.PixelSnapH = true;
            io.Fonts->AddFontFromFileTTF("../fontawesome-webfont.ttf", 14.0f, &icons_config, icons_ranges_fontawesome);
            //io.Fonts->AddFontFromFileTTF( "../kenney-icon-font.ttf",    16.0f, &icons_config, icons_ranges_kenney);
            //  io.Fonts->AddFontFromFileTTF( "../MaterialIcons-Regular.ttf", 13.0f, &icons_config, icons_ranges_materialdesign);
        }

        //
        // Set the log function
        //
        //g_context::get().m_pLogOuputFn = MyLogOutput;
    }

    //-------------------------------------------------------------------------------------------
    void base::onBegingRender( void )
    {
        m_pWindow->BeginRender( m_View );
    }

    //-------------------------------------------------------------------------------------------
    void base::onEndRender( void )
    {
        m_pWindow->EndRender();
        m_pWindow->PageFlip();
    }
    
    //-------------------------------------------------------------------------------------------
    bool base::BeginFrame( void )
    {
        if( m_pWindow->HandleEvents() == false )
            return false;

        //
        // Make the camera look at the center of the screen
        // Rotate slowly
        //
        m_View = m_pWindow->getActiveView();

        auto CameraAngles   = m_View.getAngles(); 
        CameraAngles.m_Yaw += xradian{ 1_deg };
        m_View.LookAt( 4.5, CameraAngles, xvector3(0) );
        
        //
        // We are ready to beging rendering
        //
        onBegingRender();
    
        //
        // Get imGuid ready for action
        //
        ImGui_engBase_NewFrame();

        //
        // Need to create the maindoc window. This window will be consider our main application window.
        // We need this because the docking system need a parent window to dock. So we will keep this
        // maindoc window as big as the system window at all times.
        // Since it is a fake window in a way we want to make sure that we dont render any background
        // So we will make its background 100% transparent so we can see perfectly thought it.
        // The docking also space also tries not render a the background so we will also make it 100%
        // transparent.
        //
        const ImGuiStyle *  style               = &ImGui::GetStyle();
        const ImColor       TempBgColor         = style->Colors[ImGuiCol_WindowBg];
        const ImColor       TempChildBgColor    = style->Colors[ImGuiCol_ChildWindowBg];

        // Make sure that our main doc is as big as the system window 
        const auto& View = m_pWindow->getActiveView();
        ImGui::SetNextWindowPos( ImVec2(0,0) );
        ImGui::SetWindowSize( "maindoc", ImVec2( (f32)View.getViewport().getWidth(), (f32)View.getViewport().getHeight()), 0 );

        // make 100% transparent
        static const ImVec4 col{0,0,0,0};
        ImGui::PushStyleColor(ImGuiCol_WindowBg, col);
        ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, col);

        // Now we can render the window which will take the hold screen
        static bool bOPen = false;
        ImGui::Begin( "maindoc", &bOPen, 
            ImGuiSetCond_Always                     |
            ImGuiWindowFlags_NoMove                 |
            ImGuiWindowFlags_NoResize               |
            ImGuiWindowFlags_NoInputs               |
            ImGuiWindowFlags_NoTitleBar             |
            ImGuiWindowFlags_NoSavedSettings        |
            ImGuiWindowFlags_NoScrollbar            |
            ImGuiWindowFlags_NoCollapse             |
      //      ImGuiWindowFlags_MenuBar                |
            ImGuiWindowFlags_NoFocusOnAppearing     |
            ImGuiWindowFlags_AlwaysUseWindowPadding |
            ImGuiWindowFlags_NoBringToFrontOnFocus );

        // Start the doc space now  
        ImGui::BeginDockspace();

        // Restore the original colors of the theme
        ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, TempChildBgColor);
        ImGui::PushStyleColor(ImGuiCol_WindowBg, TempBgColor );
    
        return true;
    }

    //-------------------------------------------------------------------------------------------
    void base::EndFrame( void )
    {
        //
        // close the imgGui window
        //

        // Pop the orifinal colors of the theme
        ImGui::PopStyleColor(2); 

        // End the maindoc window    
        ImGui::EndDockspace();
        ImGui::End();

        // Now we can pop the other two transparent colors (child and window)
        ImGui::PopStyleColor(2); 

        //
        // Draw the gui
        //
        ImGui::Render();

        //
        // Tell the engine we are done rendering
        //
        onEndRender();
    }

    //-------------------------------------------------------------------------------------------
    base::err base::CreateTab( const char* pString, ImGuiDockSlot Slot )
    {
        for( auto* pNext = gbed_plugins::tab::type::getHead(); pNext; pNext = pNext->m_pNext )
        {
            if ( x_strstr<xchar>( pNext->m_TypeName.m_pValue, pString ) >= 0 )
            {
                auto& Entry = m_lTab.append( pNext->New( *this ) );
                Entry->setupDockSlot( Slot );
                return x_error_ok();
            }
        }

        return x_error_code( errors, ERR_FAILURE, "Count not find the tab" );
    }

    //-------------------------------------------------------------------------------------------
    void base::onRenderWindows    ( void )
    {
        //
        // Panels
        //
        for( int i=0; i<m_lTab.getCount(); i++ )
        {
            auto& xpTab = m_lTab[i];

            if( xpTab->isOpen() )
            {
                xpTab->Render();
            }
            else if( xpTab->getType().m_Flags.m_DELETE_WHEN_CLOSE )
            {
                m_lTab.DeleteWithCollapse(i);
                --i;
            }
        }
    }

    //-------------------------------------------------------------------------------------------
    void base::onCloseProject( void )
    {
        //
        // Panels
        //
        for( int i=0; i<m_lTab.getCount(); i++ )
        {
            auto& xpTab = m_lTab[i];
            xpTab->onCloseProject();
        }
    }

    //-------------------------------------------------------------------------------------------
    bool base::onAdvanceLogic( void )
    {
        if( BeginFrame() == false )
            return false;

        //
        // Now we can render the windows
        //
        onRenderWindows();

        //
        // Done with the frame
        //
        EndFrame();

        //
        // Let the system know that we are at the end of the frame
        //
        getMainDoc().m_Events.m_EndOfFrame.Notify();
        return true;
    }

}