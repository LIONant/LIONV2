//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "xTools/gb_editor_base/gbed_base.h"

namespace gbed_document
{
    //---------------------------------------------------------------------------------------------------------
    static x_message::event<xstring&> s_eventGlobalLog;

    void MyLogOutput( const x_reporting::log_channel& Channel, int Type, const char* String, int Line, const char* file )
    {
        const char* pType[] = { "INFO", "WARNING", "ERROR" };
        s_eventGlobalLog.Notify( xstring::Make( "[%f][%s][%s] %s - (%s - %d)\n", xtimer::getNowMs(), Channel.getChannelName(), pType[1+Type], String, file, Line ) );
    }

    //---------------------------------------------------------------------------------------------------------
    void main::msgGlobalLog( xstring& Str )
    {
        m_Events.m_LogMsg.Notify(Str);
    }

    //---------------------------------------------------------------------------------------------------------

    main::main( void ) : base( t_class_string, *this )
    { 
        // Hook ourself to the global log pump
        m_GlobalLogDelegate.Connect( s_eventGlobalLog );
        g_context::get().m_pLogOuputFn = MyLogOutput;

        // Get the current path for reference
        m_EditorPath  = std_fs::current_path();

        // Create all other documents
        for( auto* pNext = base::type::getHead(); pNext; pNext = pNext->m_pNext )
        {
            if( pNext == &mains_type::s_Type ) continue;

            // Create a new instance of a sub editor
            X_LOG_CHANNEL_INFO(m_LogChannel, "Initializing: %s", pNext->m_TypeName.m_pValue );
            m_lxpSubEditors.append( pNext->New( pNext->m_TypeName, *this ) );
            X_LOG_CHANNEL_INFO(m_LogChannel, "Done Initializing: %s", pNext->m_TypeName.m_pValue );
        }
    }

    //---------------------------------------------------------------------------------------------------------
    main::~main( void )
    {
    }

    //---------------------------------------------------------------------------------------------------------
    base::err main::CreateDir(const std_fs::path& Path ) const
    {
        std::error_code StdEC;
        if (create_directory(Path, StdEC) == false)
        {
            const auto Str = StdEC.message();
            X_LOG_CHANNEL_ERROR(m_LogChannel, "System Error: %s", Str.c_str());
            return x_error_code(errors, ERR_FAILURE, "Fail to create a directory structure");
        }
        return x_error_ok();
    }

    //---------------------------------------------------------------------------------------------------------
    base::err main::CreateNewAssetLib( const path::assets& NewLibrary ) const
    {
        err  Error;

        // Create the default library
        auto AssetPath = m_AssetPaths.m_Path + path::assets("/") + NewLibrary + std_fs::path(".assetlib");
        
        Error = CreateDir(AssetPath);
        if (Error.isError()) return Error;

        // Create the directory for the src assets
        Error = CreateDir(AssetPath + std_fs::path("/SrcAssets"));
        if (Error.isError()) return Error;

        // Create the directory for the key files
        Error = CreateDir(AssetPath + std_fs::path("/CompilerKeys"));
        if (Error.isError()) return Error;

        return Error;
    }

    //---------------------------------------------------------------------------------------------------------
    
    void main::StartUp( const path::project& ProjectPath )
    {
        m_AssetPaths.m_Path = ProjectPath + std_fs::path("/Assets");
        m_BuildPaths.m_Path = ProjectPath + std_fs::path("/Builds");
    }

    //---------------------------------------------------------------------------------------------------------
    
    void main::NotifyChangeHappen( xstring Context, xproperty_v2::base& Prop ) 
    { 
        X_LOG_CHANNEL_INFO( m_LogChannel
            , Context.isEmpty() ? "Change Happen but not valid context given" : static_cast<const char*>(Context)
        );

        m_Events.m_ChangeHappen.Notify(Prop); 
    }

    //---------------------------------------------------------------------------------------------------------
    
    void main::NotifyDelete( xstring Context, xproperty_v2::base& Prop ) 
    { 
        X_LOG_CHANNEL_INFO( m_LogChannel
            , Context.isEmpty() ? "Deletion Happen but not valid context given" : static_cast<const char*>(Context)
        );

        m_Events.m_NotifyDelete.Notify(Prop); 
    }

    //---------------------------------------------------------------------------------------------------------

    base::err main::LoadProject( const path::project& UsrProjectPath )
    {
        // Make sure that we have a close project
        if( auto Error = CloseProject(); Error.isError() ) return Error;

        //
        // lets do the loading part
        //
        if( std::wstring{ UsrProjectPath}.find( m_ProjectExtension ) == std::wstring::npos )
        {
            return x_error_code( errors, ERR_FAILURE, "Wrong extension for project" );        
        }
        StartUp( UsrProjectPath );

        //
        // Setup the main path
        //
        m_ProjectPath.assign( UsrProjectPath );

        return onLoad();
    }

    //---------------------------------------------------------------------------------------------------------

    base::err main::CreateNewProject( const path::project& UsrProjectPath )
    {
        err  Error;

        //
        // Create the main directory
        //
        auto ProjectPath = UsrProjectPath + m_ProjectExtension;
        StartUp( ProjectPath );

        // Create the main directory
        std::error_code ec;
        remove_all(ProjectPath, ec);
        x_assert( !ec );
        Error = CreateDir(ProjectPath);
        if (Error.isError()) return Error;

        //
        // Create the asset directory
        //
        {
            // Create the assets
            Error = CreateDir(m_AssetPaths.m_Path);
            if (Error.isError()) return Error;

            // Create a default library
            Error = CreateNewAssetLib("Default");
            if (Error.isError()) return Error;
            m_AssetPaths.m_DefaultLibPath = m_AssetPaths.m_Path + "/Default";
        }

        //
        // Create the build path structure
        //
        {
            // Create the default library
            Error = CreateDir(m_BuildPaths.m_Path);
            if (Error.isError()) return Error;
        }

        //
        // Finally copy the project path
        //
        m_ProjectPath = ProjectPath;

        //
        // Call onNew
        //
        onNew();

        return Error;
    }

    //---------------------------------------------------------------------------------------------------------
    
    base::err main::onSave( void )
    {
        //
        // Check if we have a project open
        //
        if( m_ProjectPath.empty() )
            return x_error_code( errors, ERR_FAILURE, "You don't have a project open yet" );

        //
        // Notify all editors that we are saving
        //
        bool bHadErrors = false;
        for( auto& xpEditor : m_lxpSubEditors )
        {
            if( auto Error = xpEditor->onSave(); Error.isError() )
            {
                bHadErrors = true;

                 X_LOG_CHANNEL_ERROR( m_LogChannel
                     , "Saving Error, Editor (%s) With Message (%s) "
                     , static_cast<const char*>( xpEditor->getType().m_TypeName.m_pValue )
                     , static_cast<const char*>( Error.getString()) 
                 );
                Error.IgnoreError();
            }
        }

        //
        // Error reporting
        //
        if( bHadErrors ) return x_error_code( errors, ERR_FAILURE, "We had errors while closing the project, please check the log");
        return x_error_ok();
    }

    //---------------------------------------------------------------------------------------------------------
    
    base::err main::onClose( void )
    {
        //
        // Notify all editors that we are closing
        //
        bool bHadErrors = false;
        for( auto& xpEditor : m_lxpSubEditors )
        {
            if( auto Error = xpEditor->onClose(); Error.isError() )
            {
                bHadErrors = true;

                 X_LOG_CHANNEL_ERROR( m_LogChannel
                     , "Closing Error, Editor (%s) With Message (%s) "
                     , static_cast<const char*>( xpEditor->getType().m_TypeName.m_pValue )
                     , static_cast<const char*>( Error.getString()) 
                 );
                Error.IgnoreError();
            }
        }

        //
        // Reset members
        //
        m_ProjectPath.clear();
        m_BuildPaths.m_Path.clear();
        m_AssetPaths.m_Path.clear();
        m_AssetPaths.m_DefaultLibPath.clear();

        //
        // Error reporting
        //
        if( bHadErrors ) return x_error_code( errors, ERR_FAILURE, "We had errors while Closing the project, please check the log");
        return x_error_ok();
    }

    //---------------------------------------------------------------------------------------------------------
    
    base::err main::onLoad( void )
    {
        //
        // Notify all editors that we are saving
        //
        bool bHadErrors = false;
        for( auto& xpEditor : m_lxpSubEditors )
        {
            if( auto Error = xpEditor->onLoad(); Error.isError() )
            {
                bHadErrors = true;

                 X_LOG_CHANNEL_ERROR( m_LogChannel
                     , "Load Error, Editor (%s) With Message (%s) "
                     , static_cast<const char*>( xpEditor->getType().m_TypeName.m_pValue )
                     , static_cast<const char*>( Error.getString()) 
                 );
                Error.IgnoreError();
            }
        }

        //
        // Error reporting
        //
        if( bHadErrors ) return x_error_code( errors, ERR_FAILURE, "We had errors while Loading a project, please check the log");
        return x_error_ok();
    }

    //---------------------------------------------------------------------------------------------------------
    
    base::err main::onNew( void )
    {
        //
        // Notify all editors that we are saving
        //
        bool bHadErrors = false;
        for( auto& xpEditor : m_lxpSubEditors )
        {
            if( auto Error = xpEditor->onNew(); Error.isError() )
            {
                bHadErrors = true;

                 X_LOG_CHANNEL_ERROR( m_LogChannel
                     , "New Project Error, Editor (%s) With Message (%s) "
                     , static_cast<const char*>( xpEditor->getType().m_TypeName.m_pValue )
                     , static_cast<const char*>( Error.getString()) 
                 );
                Error.IgnoreError();
            }
        }

        //
        // Error reporting
        //
        if( bHadErrors ) return x_error_code( errors, ERR_FAILURE, "We had errors while creating a new project, please check the log");
        return x_error_ok();
    }
}
