//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


namespace gbed_document
{
    //-------------------------------------------------------------------------------------------

    namespace mains_type
    {
        static const base::type_harness<main> s_Type{ main::t_class_string, -100 };    
    }

    //-------------------------------------------------------------------------------------------

    const base::type& main::getType( void ) 
    { 
        return mains_type::s_Type;  
    }

    //-------------------------------------------------------------------------------------------

    base::type::type( const xstring::const_str Str, int Weight ) 
        : m_TypeName    { Str       }
        , m_Weight      { Weight    }
        , m_pNext       { nullptr   }
    { 
        if( getHead() == nullptr )
        {
            getHead() = this;
        }
        else if( getHead()->m_Weight > m_Weight )
        {
            m_pNext = getHead();
            getHead() = this;
        }
        else
        {
            // We will do an insert short
            auto* pNext = getHead();
            for( ; pNext->m_pNext; pNext = pNext->m_pNext )
            {
                if( pNext->m_pNext->m_Weight > Weight )
                {
                    m_pNext = pNext->m_pNext;
                    pNext->m_pNext = this;
                    break;
                }
            }

            // Added at the end of the list if we have added yet
            if( m_pNext == nullptr )
            {
                pNext->m_pNext = this;
            }
        }
    }

    //-------------------------------------------------------------------------------------------

    base::type*& base::type::getHead( void )
    { 
        static base::type* pHead{ nullptr }; 
        return pHead; 
    }

    //-------------------------------------------------------------------------------------------
    
    base::guid base::type::getGuid( void ) const
    { 
        return { t_class_guid, m_TypeName.m_pValue }; 
    }

    //-------------------------------------------------------------------------------------------



}



