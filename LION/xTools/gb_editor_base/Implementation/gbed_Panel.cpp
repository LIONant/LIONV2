//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


namespace gbed_plugins
{
    //-------------------------------------------------------------------------------------------

    tab::type::type( const xstring::const_str Str, flags Flags, int Weight, xcolor Color ) 
        : m_TypeName{ Str }
        , m_pNext{ nullptr } 
        , m_Flags{ Flags }
        , m_Weight{ Weight }
        , m_CustomBgColor{ Color }
    { 
        if( getHead() == nullptr )
        {
            getHead() = this;
        }
        else if( getHead()->m_Weight > m_Weight )
        {
            m_pNext = getHead();
            getHead() = this;
        }
        else
        {
            // We will do an insert short
            auto* pNext = getHead();
            for( ; pNext->m_pNext; pNext = pNext->m_pNext )
            {
                if( pNext->m_pNext->m_Weight > Weight )
                {
                    m_pNext = pNext->m_pNext;
                    pNext->m_pNext = this;
                    break;
                }
            }

            // Added at the end of the list if we have added yet
            if( m_pNext == nullptr )
            {
                pNext->m_pNext = this;
            }
        }
    }

    //-------------------------------------------------------------------------------------------

    tab::type*& tab::type::getHead( void )
    { 
        static tab::type* pHead{ nullptr }; 
        return pHead; 
    }

    //-------------------------------------------------------------------------------------------
    
    tab::guid tab::type::getGuid( void ) const
    { 
        return { t_class_guid, m_TypeName.m_pValue }; 
    }

    //-------------------------------------------------------------------------------------------

    tab::tab( const xstring::const_str& Str, gbed_frame::base& EditorFrame )
        : m_EditorFrame{ EditorFrame }
    {
        m_TabName.setup( "%s##%p", Str.m_pValue, this );
    }

    //-------------------------------------------------------------------------------------------

    tab::~tab( void )
    {
    }

    //-------------------------------------------------------------------------------------------

    gbed_document::main& tab::getMainDoc( void ) 
    { 
        return m_EditorFrame.getMainDoc(); 
    }

    //-------------------------------------------------------------------------------------------

    void tab::Render( void )
    {
        const auto bEmptyProject = getMainDoc().isProjectEmpty();

        ImGui::SetNextDock( m_DockSlot );

        auto& Type = getType();
    
        if( Type.m_Flags.m_CUSTOM_BGCOLOR ) 
        {
            auto V4 = Type.m_CustomBgColor.getRGBA();
            ImVec4 Col{ V4.m_X, V4.m_Y, V4.m_Z, V4.m_W };
            ImGui::PushStyleColor( ImGuiCol_ChildWindowBg, Col );
        }


        if (ImGui::BeginDock( m_TabName, &m_OpenPanel, Type.m_Flags.m_MENU_BAR?ImGuiWindowFlags_MenuBar:0 )) 
        {
            if( Type.m_Flags.m_TAB_CONTAINER )
            {
                 ImGui::BeginDockspace();
            }

            if( bEmptyProject == false )
            {
                onRender();
            }
            else
            {
                if( Type.m_Flags.m_DISPLAY_ON_EMPTY_PROJECT ) 
                {
                    onRender();
                }
            }

            if( Type.m_Flags.m_TAB_CONTAINER )
            {
                 ImGui::EndDockspace();
            }
        }
        ImGui::EndDock();

        if( Type.m_Flags.m_CUSTOM_BGCOLOR ) 
        {
            ImGui::PopStyleColor();
        }
    }
}