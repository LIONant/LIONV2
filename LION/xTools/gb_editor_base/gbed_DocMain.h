//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------
inline
std_fs::path operator + (const std_fs::path& A, const std_fs::path& B)
{
    std_fs::path temp{ A };
    temp += B;
    return temp;
}

//---------------------------------------------------------------------------------------------------------

namespace gbed_document
{
    namespace path
    {
        template< typename T >
        using filepath = std_fs::path;

        using project           = filepath<struct tag>;
        using assets            = filepath<struct tag>;
        using build             = filepath<struct tag>;
    };

    //---------------------------------------------------------------------------------------------------------

    class main : public base
    {
    public:

        using base::base;
        x_constexprvar  auto        t_class_string = X_STR("Main");

        struct events
        {
            x_message::event<xstring&>                                          m_LogMsg;
            x_message::event<>                                                  m_EndOfFrame;
            x_message::event<>                                                  m_CloseProject;
            x_message::event<xproperty_v2::base&>                               m_ChangeHappen;
            x_message::event<xproperty_v2::base&>                               m_NotifyDelete;
        };

    public:

        events                                  m_Events;
        X_CMD_LOGGING(x_reporting::log_channel  m_LogChannel{ "GB_EDITOR",   true });

    public:

        x_incppfile                             main                ( void );
        virtual                                ~main                ( void );
        x_forceinline           auto&           setupProjectFileExt ( const path::project Extension ){ m_ProjectExtension.assign( Extension ); return *this; }
        x_incppfile             err             CreateNewProject    ( const path::project& ProjectPath );
        x_incppfile             err             LoadProject         ( const path::project& ProjectPath );
        inline                  err             CloseProject        ( void ) { m_Events.m_CloseProject.Notify(); return onClose(); }
        inline                  err             SaveProject         ( void ) { return onSave(); }
        x_incppfile             err             CreateNewAssetLib   ( const path::assets&  AssetPath   ) const;
        x_forceconst            const auto&     getCurrentPath      ( void ) const { return m_EditorPath; }
        x_forceconst            const auto&     getProjectPath      ( void ) const { return m_ProjectPath; }
        x_forceinline           bool            isProjectEmpty      ( void ) const { return m_ProjectPath.empty(); }
        x_incppfile             void            NotifyChangeHappen  ( xstring Context, xproperty_v2::base& Prop );
        x_incppfile             void            NotifyDelete        ( xstring Context, xproperty_v2::base& Prop );
        virtual                 const type&     getType             ( void ) override;

        x_incppfile             err             CreateDir           ( const std_fs::path& Path )    const;
        
        template< typename T >
        inline                  T&              getSubDocument      ( void )
        {
            x_constexprvar guid Guid{ t_class_guid, T::t_class_string.m_pValue };  
            for( auto& xpEntry : m_lxpSubEditors )
            {
                if( Guid == xpEntry->getType().getGuid() )
                {
                    return xpEntry->SafeCast<T>();
                }
            }

            x_assume( false );
            return m_lxpSubEditors[0]->SafeCast<T>();
        }

    protected:

        struct asset_paths
        {
            std_fs::path                        m_Path;
            path::assets                        m_DefaultLibPath;
        };

        struct buid_paths
        {
            std_fs::path                        m_Path;
            //path::build                         m_BuildPath;
        };

    protected:

        void                                msgGlobalLog        ( xstring& Str );

        inline                  err         New                 ( void ) { x_assert(false); return onNew();  }
        x_incppfile             void        StartUp             ( const path::project& ProjectPath );
        inline                  err         Load                ( void ) { x_assert(false); return onNew();  }
        inline                  err         Save                ( void ) { x_assert(false); return onNew();  }

        virtual                 err         onNew               ( void ) override;
        virtual                 err         onSave              ( void ) override;
        virtual                 err         onLoad              ( void ) override;
        virtual                 err         onClose             ( void ) override;


    protected:

        path::project                       m_ProjectExtension  {};
        std_fs::path                        m_EditorPath        {};
        path::project                       m_ProjectPath       {};
        buid_paths                          m_BuildPaths        {};
        asset_paths                         m_AssetPaths        {};
        xvector<xndptr_s<base>>             m_lxpSubEditors     {};

        x_message::delegate<main,xstring&>  m_GlobalLogDelegate { *this, &main::msgGlobalLog };
    };
};


