//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

namespace gbed_plugins { class tab; }

namespace gbed_frame
{
    class base
    {
    public:

        enum errors : u8
        {
            ERR_OK,
            ERR_FAILURE
        };

        using err = x_err<errors>;

    public:

        x_incppfile                             base                ( gbed_document::main& Document );
        virtual                                ~base                ( void );
        x_incppfile     base&                   setupEngWindow      ( eng_window& Window );
        x_incppfile     bool                    AdvanceLogic        ( void );
        x_inline        gbed_document::main&    getMainDoc          ( void ) { return m_Document; }
        x_incppfile     err                     CreateTab           ( const char* pString, ImGuiDockSlot Slot = ImGuiDockSlot_Float );
        x_inline        auto&                   getTabList          ( void ) { return m_lTab; }
        x_forceinline   auto                    getWindow           ( void ) { return m_pWindow; }

    protected:

        virtual         void                    onSetup             ( void );
        virtual         void                    onRenderWindows     ( void );
        virtual         bool                    onAdvanceLogic      ( void );
        virtual         void                    onBegingRender      ( void );
        virtual         void                    onEndRender         ( void );

        x_incppfile     void                    InitializeImGui     ( void );
        x_incppfile     bool                    BeginFrame          ( void );
        x_incppfile     void                    EndFrame            ( void );

        virtual         void                    onCloseProject      ( void );

    protected:

        gbed_document::main&                        m_Document;
        eng_window*                                 m_pWindow           { nullptr };
        eng_view                                    m_View              {};
        xvector<xndptr_s<gbed_plugins::tab>>        m_lTab              {}; 

        x_message::delegate<base>                   m_delegatemsgCloseProject { *this, &base::onCloseProject };

    };
}


