//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

namespace gbed_frame { class  base; }

namespace gbed_plugins
{

    inline void MenuToolTip( const char* pfmt )
    {
        ImGui::SameLine();
        ImGui::TextDisabled( u8"\uf059" );
        if (ImGui::IsItemHovered())             
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(450.0f);
            ImGui::TextUnformatted( pfmt );
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    class tab
    {
    public:

        x_object_type( tab, rtti_start );

        x_constexprvar  auto        t_class_string = X_STR("Editor Tab");
        x_constexprvar  auto        t_class_guid   = gb_component::class_guid{ "Editor Tab" };
        
    public:

        using type_guid = gb_component::named_guid<tab>;
        using guid      = gb_component::extended_guid<tab, type_guid, t_class_guid.m_Value >;

        struct type
        {
            X_DEFBITS( flags, u8, 0u
                , X_DEFBITS_ARG( bool, DELETE_WHEN_CLOSE, 1 )
                , X_DEFBITS_ARG( bool, TAB_CONTAINER, 1 )
                , X_DEFBITS_ARG( bool, MENU_BAR, 1 )
                , X_DEFBITS_ARG( bool, CUSTOM_BGCOLOR, 1 )
                , X_DEFBITS_ARG( bool, DISPLAY_ON_EMPTY_PROJECT, 1 )
            );

            x_incppfile                                     type        ( const xstring::const_str Str, flags Flags = flags::MASK_DEFAULT, int Weight = 0, xcolor Col = xcolor{~0u} );
            x_incppfile static      type*&                  getHead     ( void );
            x_incppfile             guid                    getGuid     ( void )                            const;
            virtual                 xndptr_s<tab>           New         ( gbed_frame::base& EditorFrame )   const = 0;

            type*                       m_pNext{ nullptr };
            const xstring::const_str    m_TypeName;
            const flags                 m_Flags;
            const int                   m_Weight;
            xcolor                      m_CustomBgColor;
        };

        template< typename T_TYPE >
        struct type_harness : type
        {
            using type::type;

            virtual xndptr_s<tab>   New         ( gbed_frame::base& EditorFrame )   const override   
            { 
                return x_new( T_TYPE, { m_TypeName, EditorFrame } );  
            }
        };

    public:

        x_incppfile                             tab             ( const xstring::const_str& Str, gbed_frame::base& EditorFrame );
        virtual                                ~tab             ( void );
        virtual         const type&             getType         ( void ) = 0;
        x_incppfile     void                    Render          ( void );
        inline          bool                    isOpen          ( void ) const { return m_OpenPanel; }
        inline          void                    setOpen         ( bool bOpen ) { m_OpenPanel = bOpen; }
        inline          void                    setupDockSlot   ( ImGuiDockSlot DockSlot ) { m_DockSlot = DockSlot; }
        x_incppfile     gbed_document::main&    getMainDoc      ( void );

    protected:

        virtual         void            onRender                ( void ) = 0;
        virtual         void            onCloseProject          ( void ) {}

    protected:

        xstring                 m_TabName;
        bool                    m_OpenPanel{ true };
        gbed_frame::base&       m_EditorFrame;
        ImGuiDockSlot           m_DockSlot = ImGuiDockSlot_Tab;

        friend class gbed_frame::base;
    };
}


