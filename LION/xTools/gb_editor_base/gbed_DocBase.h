//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

namespace gbed_document
{
    class main;

    class base
    {
        x_object_type( base, rtti_start, is_not_copyable, is_not_movable )

    public:

        enum errors : u8
        {
            ERR_OK,
            ERR_FAILURE,
        };

        using err = x_err<errors>;

        x_constexprvar  auto        t_class_string = X_STR("Editor Document");
        x_constexprvar  auto        t_class_guid   = gb_component::class_guid{ "Editor Document" };

        using type_guid = gb_component::named_guid<base>;
        using guid      = gb_component::extended_guid<base, type_guid, t_class_guid.m_Value >;

        struct type
        {
            x_incppfile                                 type        ( const xstring::const_str Str, int Weight = 0 );
            x_incppfile static      type*&              getHead     ( void );
            x_incppfile             guid                getGuid     ( void )                            const;
            virtual                 xndptr_s<base>      New         ( const xstring::const_str Str, main& MainDoc )   const = 0;

            type*                       m_pNext{ nullptr };
            const xstring::const_str    m_TypeName;
            const int                   m_Weight;
        };

        template< typename T_TYPE >
        struct type_harness : type
        {
            using type::type;

            virtual xndptr_s<base>   New         ( const xstring::const_str Str, main& MainDoc )   const override   
            { 
                return x_new( T_TYPE, { Str, MainDoc } );  
            }
        };

    public:

        inline                          base            ( const xstring::const_str Str, main& MainDoc ) : m_MainDoc{ MainDoc } { m_DocName.Copy( Str ); }
        virtual                        ~base            ( void ) { }
        inline          auto&           getMainDoc      ( void ) { return m_MainDoc; }
        virtual         const type&     getType         ( void ) = 0;

    public:

        base*       m_pNext { nullptr };
        xstring     m_DocName;

    protected:

        virtual err     onSave  ( void ) = 0;
        virtual err     onLoad  ( void ) = 0;
        virtual err     onNew   ( void ) = 0;
        virtual err     onClose ( void ) = 0;

    protected:

        main&   m_MainDoc;

        friend class main;
    };
}


