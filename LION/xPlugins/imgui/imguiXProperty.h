//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
// 3D Gizmos - https://github.com/CedricGuillemet/ImGuizmo

#include "imguiPropertyWindow.h"

namespace imgui_xproperty
{
    struct prop_data
    {
        int             m_iProp;
    };

    template< int T_DIMENSIONS >
    using item_float        = imgui_property::item_float<T_DIMENSIONS, prop_data>;
    template< int T_DIMENSIONS >
    using item_int          = imgui_property::item_int<T_DIMENSIONS, prop_data>; 

    using item_folder       = imgui_property::item_folder<prop_data>;
    using item_topfolder    = imgui_property::item_topfolder<>;
    using item_combo        = imgui_property::item_combo<prop_data>;
    using item_color        = imgui_property::item_color<prop_data>;
    using item_bool         = imgui_property::item_checkbox<prop_data>;
    using item_button       = imgui_property::item_button<prop_data>;
    using item_string       = imgui_property::item_string<prop_data>;
    using item_guid         = imgui_property::item_guid<prop_data>;
          
    //-------------------------------------------------------------------------------------------
    class window : public imgui_property::window
    {
        x_object_type( window, rtti( imgui_property::window) );

    public:

        void SetProperty( xproperty_v2::base* pProperty )
        {
            m_pProperty = pProperty; 
            m_ItemChanges.DeleteAllEntries();
            m_ProCollection.DeleteAllEntries();
            RefreshTree();
        }

        auto getProperty( void ) const { return m_pProperty; }

    protected:
        
        struct changes
        {
            imgui_property::base_item&  m_BaseItem;
            int                         m_iProp;
        };

    protected:

        //------------------------------------------------------------------------------------------------------
        virtual void onNotifyGuidClick( imgui_property::base_item& BaseItem ) override
        {
            item_guid& ItemGuid = BaseItem.SafeCast<item_guid>();
            prop_data& PropData = ItemGuid.getUserData<prop_data>();
        }

        //------------------------------------------------------------------------------------------------------
        virtual void PropQuerySet( xfunction_view<const xproperty_v2::entry*(int Index)> Function )
        {
            m_pProperty->SetProperties( Function );
        }

        //------------------------------------------------------------------------------------------------------
        virtual void PropQueryGet( xfunction_view<xproperty_v2::entry*(int Index)> Function ) const
        {
            m_pProperty->GetProperties( Function );
        }

        //------------------------------------------------------------------------------------------------------
        virtual void PropEnum( xfunction_view<xproperty_v2::entry&(void)> Function, bool bFullPath = true, xproperty_v2::base::enum_mode Mode  = xproperty_v2::base::enum_mode::REALTIME ) const
        {
            m_pProperty->EnumProperty( Function, bFullPath, Mode );
        }

        //------------------------------------------------------------------------------------------------------
        inline
        virtual void onPrintHelp( void ) override
        {
            if( m_pSelectedItem )
            {
                prop_data& PropData = m_pSelectedItem->getUserData<prop_data>();
                if( m_pSelectedItem->m_bTreeNode == false ) ImGui::TextDisabled( (const char*)m_ProCollection[ PropData.m_iProp ].m_Name );
            }

            t_parent::onPrintHelp();
        }

        //------------------------------------------------------------------------------------------------------
        inline
        virtual void onUpdate( void )
        {
            // Make sure we clear all our entries
            m_ItemChanges.DeleteAllEntries();

            // Lets start collecting changes
            imgui_property::window::onUpdate();

            // If nothing has change then there is nothing to update
            if( m_ItemChanges.getCount() == 0 )
                return;

            //
            // Check to see if any of the properties that has change require a refresh all
            //

            //
            // Set the new value into our object
            //
            PropQuerySet( [&](const int Index) -> const xproperty_v2::entry*
            {
                if( Index >= m_ItemChanges.getCount<int>() ) return nullptr;

                //
                // Set the next property to set
                //
                const changes& Changes = m_ItemChanges[ Index ];

                // Make sure that the property is updated with the item data
                UpdatePropertyFromItem( m_ProCollection[ Changes.m_iProp ], Changes.m_BaseItem );

                // Now we can set it
                return &m_ProCollection[ Changes.m_iProp ];
            });

            //
            // Get values from the object back to the items
            //
            if( m_ItemChanges.getCount<int>() )
            {
                auto Process = [&]( const int Index )
                {
                    const changes&  Changes = m_ItemChanges[ Index ];
                    auto&           Prop    = m_ProCollection[ Changes.m_iProp ];

                    // Update the item data as well
                    UpdateItemFromProperty( Changes.m_BaseItem, Prop );
                };

                PropQueryGet( [&]( const int Index ) -> xproperty_v2::entry*
                {
                    if( Index >= m_ItemChanges.getCount<int>() ) return nullptr;
                    if( Index > 0 ) Process( Index-1 ); 

                    //
                    // Tell which property to get next
                    //
                    const changes&  Changes = m_ItemChanges[ Index ];
                    return &m_ProCollection[ Changes.m_iProp ];
                });

                Process( m_ItemChanges.getCount<int>()-1 );
            }
        }

        //------------------------------------------------------------------------------------------------------
        virtual void onNotifyChange( imgui_property::base_item& BaseItem )
        {
            const prop_data& Data = BaseItem.getUserData<prop_data>();
            m_ItemChanges.append( BaseItem, Data.m_iProp );
        }

        //------------------------------------------------------------------------------------------------------
        void UpdatePropertyFromItem( xproperty_v2::entry& Prop, imgui_property::base_item& BaseItem )
        {
            switch( Prop.m_Data.getTypeIndex() )
            {
                case xproperty_v2::prop_type::ANGLES3:
                {
                    const auto& I   = BaseItem.SafeCast<item_float<3>>();
                    auto&       R   = Prop.m_Data.get<xproperty_v2::prop_type::ANGLES3>();
                    R.m_Pitch       = xdegree{ I.m_Value[0] };
                    R.m_Yaw         = xdegree{ I.m_Value[1] };
                    R.m_Roll        = xdegree{ I.m_Value[2] };
                    break;
                }
                case xproperty_v2::prop_type::BOOL:
                {
                    const auto& I = BaseItem.SafeCast<item_bool>();
                    auto&       B = Prop.m_Data.get<xproperty_v2::prop_type::BOOL>();
                    B = I.m_Value;
                    break;
                }
                case xproperty_v2::prop_type::BUTTON:
                {
                    const auto& I = BaseItem.SafeCast<item_button>();
                    auto&       B = Prop.m_Data.get<xproperty_v2::prop_type::BUTTON>();
                    break;
                }
                case xproperty_v2::prop_type::ENUM:
                {
                    const auto& I       = BaseItem.SafeCast<item_combo>();
                    auto&       Enum    = Prop.m_Data.get<xproperty_v2::prop_type::ENUM>();
                    Enum                = I.m_List[ I.m_Value ];
                    break;
                }
                case xproperty_v2::prop_type::FLOAT:
                {
                    const auto&     I   = BaseItem.SafeCast<item_float<1>>();
                    auto&           F32 = Prop.m_Data.get<xproperty_v2::prop_type::FLOAT>();
                    F32                 = I.m_Value[0];
                    break;
                }
                case xproperty_v2::prop_type::GUID:
                {
                    const auto&     I       = BaseItem.SafeCast<item_guid>();
                    auto&           Guid    = Prop.m_Data.get<xproperty_v2::prop_type::GUID>();
                    Guid                    = I.m_Value;
                    break;
                }
                case xproperty_v2::prop_type::INT:
                {
                    const auto&     I   = BaseItem.SafeCast<item_int<1>>();
                    auto&           S32 = Prop.m_Data.get<xproperty_v2::prop_type::INT>();
                    S32                 = I.m_Value[0];
                    break;
                }
                case xproperty_v2::prop_type::RGBA:
                {
                    const auto& I       = BaseItem.SafeCast<item_color>();
                    auto&       Col     = Prop.m_Data.get<xproperty_v2::prop_type::RGBA>();

                    Col.setupFromRGBA( xvector4( 
                        I.m_Value[0],
                        I.m_Value[1],
                        I.m_Value[2],
                        I.m_Value[3] ) );
                    break;
                }
                case xproperty_v2::prop_type::STRING:
                {
                    const auto& I     = BaseItem.SafeCast<item_string>();
                    auto&       Str   = Prop.m_Data.get<xproperty_v2::prop_type::STRING>();
                    Str.Copy( I.m_Value );
                    break;
                }
                case xproperty_v2::prop_type::VECTOR2:
                {
                    const auto& I  = BaseItem.SafeCast<item_float<2>>();
                    auto&       V2 = Prop.m_Data.get<xproperty_v2::prop_type::VECTOR2>();
                    V2.setup( I.m_Value[0], I.m_Value[1] );
                    break;
                }
                case xproperty_v2::prop_type::VECTOR3:
                {
                    const auto&     I  = BaseItem.SafeCast<item_float<3>>();
                    auto&           V3 = Prop.m_Data.get<xproperty_v2::prop_type::VECTOR3>();
                    V3.setup( I.m_Value[0], I.m_Value[1], I.m_Value[2] );
                    break;
                }
                default: 
                    x_assert( false );
            }
        }

        //------------------------------------------------------------------------------------------------------

        void UpdateItemFromProperty( imgui_property::base_item& BaseItem, xproperty_v2::entry& Prop )
        {
            switch( Prop.m_Data.getTypeIndex() )
            {
                case xproperty_v2::prop_type::ANGLES3:
                {
                    auto&       I   = BaseItem.SafeCast<item_float<3>>();
                    const auto& R1  = Prop.m_Data.get<xproperty_v2::prop_type::ANGLES3>();
                    const auto  R2  = xradian3( xdegree(I.m_Value[0]), xdegree(I.m_Value[1]), xdegree(I.m_Value[2]) );
                    const xquaternion A( R1 );
                    const xquaternion B( R2 ); 
                    const auto DotDiff = A.Dot( B );
                    if( x_Abs(DotDiff) < 0.98f )
                    {
                        I.m_Backup[0] = I.m_Value[0] = R1.m_Pitch.getDegrees().m_Value;
                        I.m_Backup[1] = I.m_Value[1] = R1.m_Yaw.getDegrees().m_Value;
                        I.m_Backup[2] = I.m_Value[2] = R1.m_Roll.getDegrees().m_Value;
                    }

                    I.m_MaxValue[0]      = 360.0f;//R3.m_Max.m_Pitch.getDegrees().m_Value;
                    I.m_MaxValue[1]      = 360.0f;//R3.m_Max.m_Yaw.getDegrees().m_Value;
                    I.m_MaxValue[2]      = 360.0f;//R3.m_Max.m_Roll.getDegrees().m_Value;
                    I.m_MinValue[0]      = 0.0f;//R3.m_Min.m_Pitch.getDegrees().m_Value;
                    I.m_MinValue[1]      = 0.0f;//R3.m_Min.m_Yaw.getDegrees().m_Value;
                    I.m_MinValue[2]      = 0.0f;//R3.m_Min.m_Roll.getDegrees().m_Value;
                    break;
                }
                case xproperty_v2::prop_type::BOOL:
                {
                    auto&       I = BaseItem.SafeCast<item_bool>();
                    const auto& B = Prop.m_Data.get<xproperty_v2::prop_type::BOOL>();
                    I.m_Backup = I.m_Value = B;
                    break;
                }
                case xproperty_v2::prop_type::BUTTON:
                {
                    auto&       I = BaseItem.SafeCast<item_button>();
                    const auto& B = Prop.m_Data.get<xproperty_v2::prop_type::BUTTON>();
                    I.m_Backup = I.m_Value = false;
                    I.m_Label  = B.m_Value;
                    break;
                }
                case xproperty_v2::prop_type::ENUM:
                {
                    auto&       I               = BaseItem.SafeCast<item_combo>();
                    const auto& PropEnum        = Prop.m_Data.get<xproperty_v2::prop_type::ENUM>();
                    auto*       pDetails        = reinterpret_cast<const xproperty_v2::info::enum_t*>(Prop.m_pDetails);
                
                    // Copy all the values
                    I.m_List.DeleteAllEntries();
                    for( int i = 0; i<pDetails->m_List.getCount<int>(); i++ )
                    {
                        auto& Str = I.m_List.append();
                        Str.Copy( pDetails->m_List[i].second );
                        if( PropEnum == Str ) I.m_Value = i;
                    }

                    break;
                }
                case xproperty_v2::prop_type::FLOAT:
                {
                    auto&           I           = BaseItem.SafeCast<item_float<1>>();
                    const auto&     F32         = Prop.m_Data.get<xproperty_v2::prop_type::FLOAT>();
                    auto*           pDetails    = reinterpret_cast<const xproperty_v2::info::f32*>(Prop.m_pDetails);
                    I.m_Backup[0] = I.m_Value[0] = F32;
                    I.m_MinValue[0] = pDetails->m_Min;
                    I.m_MaxValue[0] = pDetails->m_Max;
                    break;
                }
                case xproperty_v2::prop_type::GUID:
                {
                    auto&           I           = BaseItem.SafeCast<item_guid>();
                    const auto&     Guid        = Prop.m_Data.get<xproperty_v2::prop_type::GUID>();
                    auto*           pDetails    = reinterpret_cast<const xproperty_v2::info::guid*>(Prop.m_pDetails);

                    I.m_Backup    = I.m_Value  = Guid;
                    I.m_Category  = pDetails->m_Category;
                    I.m_Type      = pDetails->m_Type;
                    I.m_EditStyle = pDetails->m_EditStyle;
                    break;
                }
                case xproperty_v2::prop_type::INT:
                {
                    auto&           I           = BaseItem.SafeCast<item_int<1>>();
                    const auto&     S32         = Prop.m_Data.get<xproperty_v2::prop_type::INT>();
                    auto*           pDetails    = reinterpret_cast<const xproperty_v2::info::s32*>(Prop.m_pDetails);

                    I.m_Backup[0] = I.m_Value[0] = S32;
                    if(pDetails)
                    {
                        I.m_MinValue  = pDetails->m_Min;
                        I.m_MaxValue  = pDetails->m_Max;
                    }
                    else
                    {
                        I.m_MinValue = 0;
                        I.m_MaxValue = 0xfffff;
                    }
                    break;
                }
                case xproperty_v2::prop_type::RGBA:
                {
                    auto&           I           = BaseItem.SafeCast<item_color>();
                    const auto&     Col         = Prop.m_Data.get<xproperty_v2::prop_type::RGBA>();
                    auto            F32Col      = Col.getRGBA();

                    I.m_Backup[0] = I.m_Value[0] = F32Col.m_X;
                    I.m_Backup[1] = I.m_Value[1] = F32Col.m_Y;
                    I.m_Backup[2] = I.m_Value[2] = F32Col.m_Z;
                    I.m_Backup[3] = I.m_Value[3] = F32Col.m_W;
                    break;
                }
                case xproperty_v2::prop_type::STRING:
                {
                    auto&           I           = BaseItem.SafeCast<item_string>();
                    auto&           Str         = Prop.m_Data.get<xproperty_v2::prop_type::STRING>();

                    I.m_Backup.CopyFromTo( I.m_Value.CopyFromTo( Str ) );
                    break;
                }
                case xproperty_v2::prop_type::VECTOR2:
                {
                    auto&           I           = BaseItem.SafeCast<item_float<2>>();
                    const auto&     V2          = Prop.m_Data.get<xproperty_v2::prop_type::VECTOR2>();
                    auto*           pDetails    = reinterpret_cast<const xproperty_v2::info::vector2*>(Prop.m_pDetails);

                    I.m_Backup[0] = I.m_Value[0] = V2.m_X;
                    I.m_Backup[1] = I.m_Value[1] = V2.m_Y;
                    I.m_MinValue[0]  = pDetails->m_Min.m_X;
                    I.m_MinValue[1]  = pDetails->m_Min.m_Y;
                    I.m_MaxValue[0]  = pDetails->m_Max.m_X;
                    I.m_MaxValue[1]  = pDetails->m_Max.m_Y;
                    break;
                }
                case xproperty_v2::prop_type::VECTOR3:
                {
                    auto&           I           = BaseItem.SafeCast<item_float<3>>();
                    const auto&     V3          = Prop.m_Data.get<xproperty_v2::prop_type::VECTOR3>();
                    auto*           pDetails    = reinterpret_cast<const xproperty_v2::info::vector3*>(Prop.m_pDetails);

                    I.m_Backup[0]   = I.m_Value[0] = V3.m_X;
                    I.m_Backup[1]   = I.m_Value[1] = V3.m_Y;
                    I.m_Backup[2]   = I.m_Value[2] = V3.m_Z;
                    I.m_MinValue[0]  = pDetails->m_Min.m_X;
                    I.m_MinValue[1]  = pDetails->m_Min.m_Y;
                    I.m_MinValue[2]  = pDetails->m_Min.m_Z;
                    I.m_MaxValue[0]  = pDetails->m_Max.m_X;
                    I.m_MaxValue[1]  = pDetails->m_Max.m_Y;
                    I.m_MaxValue[2]  = pDetails->m_Max.m_Z;
                    break;
                }
                default: 
                    x_assert( false );
            }
        }

        //------------------------------------------------------------------------------------------------------

        imgui_property::base_item& CreateItemFromProperty( xproperty_v2::entry& Prop, int iProp )
        {
            imgui_property::base_item*  pItem = nullptr;
            switch( Prop.m_Data.getTypeIndex() )
            {
                case  xproperty_v2::prop_type::ANGLES3:
                {
                    auto& I = *x_new( item_float<3>, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;

                    I.m_pDetailFmtString = "%g deg";
                    I.m_Step             =   0.5f;
                    I.m_DimensionName[0] = "Pitch( X )";
                    I.m_DimensionName[1] = "Yaw( Y )";
                    I.m_DimensionName[2] = "Roll( Z )";
                    break;
                }
                case  xproperty_v2::prop_type::BOOL:
                {
                    auto& I = *x_new( item_bool, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::BUTTON:
                {
                    auto& I = *x_new( item_button, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::ENUM:
                {
                    auto& I = *x_new( item_combo, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::FLOAT:
                {
                    auto& I = *x_new( item_float<1>, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::GUID:
                {
                    auto& I = *x_new( item_guid, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::INT:
                {
                    auto& I = *x_new( item_int<1>, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::RGBA:
                {
                    auto& I = *x_new( item_color, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::STRING:
                {
                    auto& I = *x_new( item_string, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::VECTOR2:
                {
                    auto& I = *x_new( item_float<2>, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;
                    break;
                }
                case  xproperty_v2::prop_type::VECTOR3:
                {
                    auto& I = *x_new( item_float<3>, { *this } );
                    I.m_iProp = iProp;
                    pItem = &I;

                    auto pDetails = reinterpret_cast<xproperty_v2::info::vector3*>(Prop.m_pDetails);
                    switch( pDetails->m_GizmoUnitType )
                    {
                        case xproperty_v2::info::vector3::gizmo_units::NUMBER:         break;
                        case xproperty_v2::info::vector3::gizmo_units::POSITION_CM:    
                            I.m_pDetailFmtString = "%g cm";
                        break;
                        case xproperty_v2::info::vector3::gizmo_units::UNIT_SCALE:     
                            I.m_pDetailFmtString = "%g %%";
                            I.m_Step             =   0.05f;
                        break;
                    }

                    break;
                }
                default: 
                    x_assert( false );
            }

            //
            // Count all the '/'
            //
            const int iCol = [&]
            {
                int iCol = 0;
                for( int i=0; Prop.m_Name[i]; i++ ) if( Prop.m_Name[i] == '/' ) iCol++;
                return iCol;
            }();
                
            //
            // Find the name of the property
            //
            const char* const pName = 
            [&]
            {
                int L = Prop.m_Name.getLength().m_Value;
                while( L > 0 && Prop.m_Name[L] != '/' ) L--;
                //x_assert( L > 1 );
                return &Prop.m_Name[L+1];
            }();

            const u8 Alpha = 64;

            //
            // Set the data
            //
            pItem->m_Name.Copy( pName );
            if( Prop.m_pDetails )
            {
                pItem->m_Help       = Prop.m_pDetails->m_Help;
                pItem->m_bReadOnly  = Prop.m_pDetails->m_Flags.m_READ_ONLY;
            }

            pItem->m_BkColor = xcolor::getColorCategory( iCol );
            pItem->m_BkColor.m_A = Alpha;

            if( pItem->isKindOf<item_guid>() )
            {
                auto& ItemGuid = pItem->SafeCast<item_guid>();

                ItemGuid.m_ButtonColor = ImGui::GetColorU32( ImGuiCol_Button );
            }

            return *pItem;
        }

        //------------------------------------------------------------------------------------------------------
        virtual xowner<imgui_property::item_topfolder<>*> onAddTopNode( void )
        {
            return x_new( imgui_property::item_topfolder<>, { *this } );
        }

        //------------------------------------------------------------------------------------------------------

        void RefreshTree( void )
        {
        /*
            if( m_pProperty == nullptr )
            {
                m_TreeNodes.DeleteAllEntries();
                return;
            }
        */
            m_TreeNodes.DeleteAllEntries();
            m_ProCollection.DeleteAllEntries();
            if( m_pProperty == nullptr ) return;

            //
            // Enumerate the properties 
            //
            PropEnum( [&]() -> xproperty_v2::entry&
            {
                return m_ProCollection.append();
            });

            //
            // Build the tree from the property collection
            //
            xarray< xchar,256>              CurrMainPath    {0};
            xarray< xchar,256>              CurrSubMainPath {0};
            int                             Length          {0};
            item_topfolder*                 pTopFolder      {nullptr};

            xuptr iProp = 0;
            for( auto& Prop : x_iter_ref( iProp, m_ProCollection ) )
            {
                int offset = 0;
 
                //
                // Find or create a new top folder entry for this property
                // if we need to.
                // Note for entities we handle the special case of components
                //
                if( x_strncmp<xchar>( Prop.m_Name, "Comp", 4 ) == 0 && ( Prop.m_Name[4] == '[' || Prop.m_Name[4] == '(') )
                {
                    // Is this the root of the components if so lets remove it 
                    if(Prop.m_Name[5] == ')' || Prop.m_Name[5] == ']')
                    {
                        m_ProCollection.DeleteWithCollapse( iProp, 1 );
                        iProp--;
                        continue;
                    }

                    bool bSubpathChanged = false;
                    for( offset=6; Prop.m_Name[offset]!=')' && Prop.m_Name[offset]!=']'; offset++ )
                    {
                        if( CurrSubMainPath[offset] != Prop.m_Name[offset] ) bSubpathChanged = true;
                        CurrSubMainPath[offset] = Prop.m_Name[offset];
                    }

                    if( bSubpathChanged ) 
                    {
                        CurrMainPath[Length] = 0;
                        Length = 0;
                    }
                    
                    // skip ']/'
                    offset+=2;
                }

                if (x_strncmp<xchar>(&Prop.m_Name[offset], "Delegates/", 10) == 0)
                {
                }
                else if( offset )
                {
                    // If it is a new component type then create a new top node
                    if(Length==0)
                    {
                        for( Length = 0; '/' != (CurrMainPath[Length] = Prop.m_Name[offset+Length]); Length++ );
                        CurrMainPath[Length] = 0;

                        // Update the top folder
                        pTopFolder          = &AddTreeTopNode( onAddTopNode() );
                        pTopFolder->m_Name.Copy( CurrMainPath );
                        pTopFolder->m_Help.Copy( Prop.m_Name );

                        // Need to finish the string before the property name
                        int k=Length;
                        for( int i=Length; Prop.m_Name[i] ; ++i )
                            if( Prop.m_Name[i] == '/' ) k = i;
                        pTopFolder->m_Help[k] = 0;
                        
                        // finish the current string
                        CurrMainPath[Length] = '/';
                        Length++;
                    }
                }
                else for( int i=0; Prop.m_Name[i] != '/'; i++ )
                {
                    if( i >= Length || CurrMainPath[i] != Prop.m_Name[i] )
                    {
                        for( Length = 0; '/' != (CurrMainPath[Length] = Prop.m_Name[Length]); Length++ );
                        CurrMainPath[Length] = 0;

                        // Update the top folder
                        pTopFolder          = &AddTreeTopNode( onAddTopNode() );
                        pTopFolder->m_Name.Copy( CurrMainPath );
                        pTopFolder->m_Help.Copy( Prop.m_Name );

                        // Need to finish the string before the property name
                        int k=i;
                        for( int i=0; Prop.m_Name[i] ; ++i )
                            if( Prop.m_Name[i] == '/' ) k = i;
                        pTopFolder->m_Help[k] = 0;

                        // finish the current string
                        CurrMainPath[Length] = '/';
                        Length++;
                        break;
                    }
                }

                //
                // Create the item
                //
                auto& Item = CreateItemFromProperty( Prop, static_cast<int>( iProp ) ); 
                UpdateItemFromProperty( Item, Prop );
                pTopFolder->m_PropList.append().setup( &Item );
            }
        }

    protected:

        xproperty_v2::base*             m_pProperty     { nullptr };
        xvector<xproperty_v2::entry>    m_ProCollection {};
        xvector<changes>                m_ItemChanges   {};
    };
}


