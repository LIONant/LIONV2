//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

void imguiThemeLionStandard( void )
{
    ImGuiStyle& style = ImGui::GetStyle();

    style.WindowPadding            = ImVec2(2, 2);
    style.ChildWindowRounding      = 8.0f;
    style.WindowRounding           = 5.0f;
    style.FramePadding             = ImVec2(4, 3);
    style.FrameRounding            = 2.0f;
    style.ItemSpacing              = ImVec2(2, 2);
    style.ItemInnerSpacing         = ImVec2(4, 2);
    style.TouchExtraPadding        = ImVec2(0, 0);
    style.IndentSpacing            = 10.0f;
    style.ScrollbarSize            = 15.0f;
    style.ScrollbarRounding        = 0.0f;
    style.GrabMinSize              = 16.0f;
    style.GrabRounding             = 3.0f;
    style.WindowTitleAlign         = ImVec2( 0,    0.5f );
    style.ButtonTextAlign          = ImVec2( 0.5f, 0.5f );


    style.Colors[ImGuiCol_Text]                  = ImVec4(0.80f, 0.80f, 0.80f, 1.00f);
    style.Colors[ImGuiCol_TextDisabled]          = ImVec4(0.67f, 0.67f, 0.67f, 0.77f);
    style.Colors[ImGuiCol_WindowBg]              = ImVec4(0.36f, 0.36f, 0.36f, 0.96f);
    style.Colors[ImGuiCol_ChildWindowBg]         = ImVec4(0.30f, 0.30f, 0.30f, 0.78f);
    style.Colors[ImGuiCol_PopupBg]               = ImVec4(0.44f, 0.58f, 0.66f, 0.78f);
    style.Colors[ImGuiCol_Border]                = ImVec4(0.29f, 0.29f, 0.29f, 0.90f);
    style.Colors[ImGuiCol_BorderShadow]          = ImVec4(0.92f, 0.91f, 0.88f, 0.00f);
    style.Colors[ImGuiCol_FrameBg]               = ImVec4(0.39f, 0.38f, 0.37f, 1.00f);
    style.Colors[ImGuiCol_FrameBgHovered]        = ImVec4(0.65f, 0.65f, 0.20f, 0.78f);
    style.Colors[ImGuiCol_FrameBgActive]         = ImVec4(0.29f, 0.43f, 0.36f, 0.78f);
    style.Colors[ImGuiCol_TitleBg]               = ImVec4(0.38f, 0.53f, 0.57f, 0.93f);
    style.Colors[ImGuiCol_TitleBgCollapsed]      = ImVec4(0.22f, 0.59f, 0.69f, 0.93f);
    style.Colors[ImGuiCol_TitleBgActive]         = ImVec4(0.22f, 0.59f, 0.69f, 0.93f);
    style.Colors[ImGuiCol_MenuBarBg]             = ImVec4(0.30f, 0.30f, 0.30f, 0.78f);//ImVec4(175.0f/255, 68.0f/255, 40.0f/255, 233.0f/255); // 175 68 40 233
    style.Colors[ImGuiCol_ScrollbarBg]           = ImVec4(1.00f, 0.98f, 0.95f, 0.00f);
    style.Colors[ImGuiCol_ScrollbarGrab]         = ImVec4(0.00f, 0.00f, 0.00f, 0.21f);
    style.Colors[ImGuiCol_ScrollbarGrabHovered]  = ImVec4(0.90f, 0.91f, 0.00f, 0.78f);
    style.Colors[ImGuiCol_ScrollbarGrabActive]   = ImVec4(0.25f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_ComboBg]               = ImVec4(0.44f, 0.44f, 0.44f, 0.95f);
    style.Colors[ImGuiCol_CheckMark]             = ImVec4(0.25f, 1.00f, 1.00f, 0.80f);
    style.Colors[ImGuiCol_SliderGrab]            = ImVec4(0.00f, 0.00f, 0.00f, 0.14f);
    style.Colors[ImGuiCol_SliderGrabActive]      = ImVec4(0.25f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_Button]                = ImVec4(0.49f, 0.49f, 0.49f, 0.87f);
    style.Colors[ImGuiCol_ButtonHovered]         = ImVec4(0.99f, 1.00f, 0.22f, 0.86f);
    style.Colors[ImGuiCol_ButtonActive]          = ImVec4(0.25f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_Header]                = ImVec4(0.21f, 0.48f, 0.69f, 0.87f);
    style.Colors[ImGuiCol_HeaderHovered]         = ImVec4(0.21f, 0.48f, 0.69f, 0.87f);
    style.Colors[ImGuiCol_HeaderActive]          = ImVec4(0.21f, 0.48f, 0.69f, 0.87f);
    style.Colors[ImGuiCol_Column]                = ImVec4(0.00f, 0.00f, 0.00f, 0.32f);
    style.Colors[ImGuiCol_ColumnHovered]         = ImVec4(0.25f, 1.00f, 1.00f, 0.78f);
    style.Colors[ImGuiCol_ColumnActive]          = ImVec4(0.25f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_ResizeGrip]            = ImVec4(0.00f, 0.00f, 0.00f, 0.04f);
    style.Colors[ImGuiCol_ResizeGripHovered]     = ImVec4(0.25f, 1.00f, 1.00f, 0.78f);
    style.Colors[ImGuiCol_ResizeGripActive]      = ImVec4(0.25f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_CloseButton]           = ImVec4(0.40f, 0.39f, 0.38f, 0.16f);
    style.Colors[ImGuiCol_CloseButtonHovered]    = ImVec4(0.40f, 0.39f, 0.38f, 0.39f);
    style.Colors[ImGuiCol_CloseButtonActive]     = ImVec4(0.40f, 0.39f, 0.38f, 1.00f);
    style.Colors[ImGuiCol_PlotLines]             = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
    style.Colors[ImGuiCol_PlotLinesHovered]      = ImVec4(0.25f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_PlotHistogram]         = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
    style.Colors[ImGuiCol_PlotHistogramHovered]  = ImVec4(0.25f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_TextSelectedBg]        = ImVec4(0.25f, 1.00f, 1.00f, 0.43f);
    style.Colors[ImGuiCol_ModalWindowDarkening]  = ImVec4(0.50f, 0.32f, 0.32f, 0.73f);
}