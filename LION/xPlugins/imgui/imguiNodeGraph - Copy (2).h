#include <math.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <stdint.h>

namespace imgui_node_graph
{
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #define sizeof_array(t) (sizeof(t) / sizeof(t[0]))
    const float NODE_SLOT_RADIUS = 5.0f;
    const ImVec2 NODE_WINDOW_PADDING(8.0f, 8.0f);
    #define MAX_CONNECTION_COUNT 32

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static inline ImVec2 operator+(const ImVec2& lhs, const ImVec2& rhs) { return ImVec2(lhs.x+rhs.x, lhs.y+rhs.y); }
    static inline ImVec2 operator-(const ImVec2& lhs, const ImVec2& rhs) { return ImVec2(lhs.x-rhs.x, lhs.y-rhs.y); }

    static uint32_t s_id = 0;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    struct connection_type
    {
        using guid = xguid<connection_type>;
        xstring             m_Name;                     // Name of the type of connection such int etc..
        guid                m_Guid;                     // guid that describes this type uniquely
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    struct node_type
    {
        using guid = xguid<connection_type>;
        xstring             m_Name;
        guid                m_Guid;                     
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    struct connection
    {
        using guid = xguid<connection>;

        ImVec2                  m_Pos;
        connection_type::guid   m_gType;
        guid                    m_Guid;

        inline connection()
        {
            m_Pos.x = m_Pos.y = 0.0f;
            m_Input = 0;
        }

        struct connection*          m_Input;
        std::vector<connection*>    m_Output;
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    struct node_base
    {
        ImVec2                      m_Pos;
        ImVec2                      m_Size;
        int                         m_ID;
        const char*                 m_Name;
        std::vector<connection*>    m_InputConnections;
        std::vector<connection*>    m_OutputConnections;
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Node types

    static struct node_type s_nodeTypes[] =
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Math

        {
            "Multiply",
            // Input connections
            {
                { "Input1", ConnectionType_Float },
                { "Input2", ConnectionType_Float },
            },
            // Output
            {
                { "Out", ConnectionType_Float },
            },
        },

        {
            "Add",
            // Input connections
            {
                { "Input1", ConnectionType_Float },
                { "Input2", ConnectionType_Float },
            },
            // Output
            {
                { "Out", ConnectionType_Float },
            },
        },
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void setupConnections(std::vector<connection*>& connections, ConnectionDesc* connectionDescs)
    {
        for (int i = 0; i < MAX_CONNECTION_COUNT; ++i)
        {
            const ConnectionDesc& desc = connectionDescs[i];

            if (!desc.name)
                break;

            connection* con = new connection;
            con->m_Desc = desc;

            connections.push_back(con);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static node_base* createNodeFromType(ImVec2 pos, node_type* nodeType)
    {
        node_base* node = new node_base;
        node->m_ID = s_id++;
        node->m_Name = nodeType->name;

        ImVec2 titleSize = ImGui::CalcTextSize(node->m_Name);

        titleSize.y *= 3;

        setupConnections(node->m_InputConnections, nodeType->inputConnections);
        setupConnections(node->m_OutputConnections, nodeType->outputConnections);

        // Calculate the size needed for the whole box

        ImVec2 inputTextSize(0.0f, 0.0f);
        ImVec2 outputText(0.0f, 0.0f);

        for (connection* c : node->m_InputConnections)
        {
            ImVec2 textSize = ImGui::CalcTextSize(c->m_Desc.name);
            inputTextSize.x = std::max<float>(textSize.x, inputTextSize.x);

            c->m_Pos = ImVec2(0.0f, titleSize.y + inputTextSize.y + textSize.y / 2.0f);

            inputTextSize.y += textSize.y;
            inputTextSize.y += 4.0f;		// size between text entries
        }

        inputTextSize.x += 40.0f;

        // max text size + 40 pixels in between

        float xStart = inputTextSize.x;

        // Calculate for the outputs

        for (connection* c : node->m_OutputConnections)
        {
            ImVec2 textSize = ImGui::CalcTextSize(c->m_Desc.name);
            inputTextSize.x = std::max<float>(xStart + textSize.x, inputTextSize.x);
        }

        node->m_Pos = pos;
        node->m_Size.x = inputTextSize.x;	
        node->m_Size.y = inputTextSize.y + titleSize.y;

        inputTextSize.y = 0.0f;

        // set the positions for the output nodes when we know where the place them

        for (connection* c : node->m_OutputConnections)
        {
            ImVec2 textSize = ImGui::CalcTextSize(c->m_Desc.name);

            c->m_Pos = ImVec2(node->m_Size.x, titleSize.y + inputTextSize.y + textSize.y / 2.0f);

            inputTextSize.y += textSize.y;
            inputTextSize.y += 4.0f;		// size between text entries
        }

        // calculate the size of the node depending on nuber of connections

        return node;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    node_base* createNodeFromName(ImVec2 pos, const char* name)
    {
        for (int i = 0; i < (int)sizeof_array(s_nodeTypes); ++i)
        {
            if (!strcmp(s_nodeTypes[i].name, name))
                return createNodeFromType(pos, &s_nodeTypes[i]);
        }

        return 0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    enum DragState 
    {
        DragState_Default,
        DragState_Hover,
        DragState_BeginDrag,
        DragState_Draging,
        DragState_Connect,
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    struct DragNode
    {
        ImVec2 pos;
        connection* con;
    };

    static DragNode s_dragNode;
    static DragState s_dragState = DragState_Default;

    static xvector<node_base*> s_nodes;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
    static void saveNodes(const char* filename)
    {
        json_t* root = json_object();
        json_t* nodes = json_array();

        for (Node* node : s_nodes)
        {
            json_t* item = json_object();
            json_object_set_new(item, "type", json_string(node->name));
            json_object_set_new(item, "id", json_integer(node->id));
            json_object_set_new(item, "pos", json_pack("{s:f, s:f}", "x",  node->pos.x, "y", node->pos.y));
            json_array_append_new(nodes, item);
        }

        // save the nodes

        json_object_set_new(root, "nodes", nodes);

        if (json_dump_file(root, filename, JSON_INDENT(4) | JSON_PRESERVE_ORDER) != 0)
            printf("JSON: Unable to open %s for write\n", filename);
    }
    */

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void BezierCurve( ImDrawList* pDrawList, ImVec2 p1, ImVec2 p2 )
    {
        // Bezier control point of the links
        static const ImVec2 link_cp(50,0);
        static const int    SegmentSteps = 24;
      
        const auto          cp1 = p1 + link_cp;
        const auto          cp2 = p2 - link_cp;

        pDrawList->AddBezierCurve(p1, cp1, cp2, p2, ImColor(200,200,100), 3.0f, SegmentSteps );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static bool isConnectorHovered(connection* c, ImVec2 offset)
    {
        ImVec2 mousePos = ImGui::GetIO().MousePos;
        ImVec2 conPos = offset + c->m_Pos;

        float xd = mousePos.x - conPos.x;
        float yd = mousePos.y - conPos.y;

        return ((xd * xd) + (yd *yd)) < (NODE_SLOT_RADIUS * NODE_SLOT_RADIUS); 
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static connection* getHoverCon(ImVec2 offset, ImVec2* pos)
    {
        for (node_base* node : s_nodes)
        {
            ImVec2 nodePos = node->m_Pos + offset;

            for (connection* con : node->m_InputConnections)
            {
                if (isConnectorHovered(con, nodePos))
                {
                    *pos = nodePos + con->m_Pos;
                    return con;
                }
            }

            for (connection* con : node->m_OutputConnections)
            {
                if (isConnectorHovered(con, nodePos))
                {
                    *pos = nodePos + con->m_Pos;
                    return con;
                }
            }
        }
    
        s_dragNode.con = 0;
        return 0; 
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void updateDraging(ImVec2 offset)
    {
        switch (s_dragState)
        {
            case DragState_Default:
            {
                ImVec2 pos;
                connection* con = getHoverCon(offset, &pos);

                if (con)
                {
                    s_dragNode.con = con;
                    s_dragNode.pos = pos; 
                    s_dragState = DragState_Hover;
                    return;
                }
                
                break;
            }

            case DragState_Hover:
            {
                ImVec2 pos;
                connection* con = getHoverCon(offset, &pos);

                // Make sure we are still hovering the same node

                if (con != s_dragNode.con)
                {
                    s_dragNode.con = 0;
                    s_dragState = DragState_Default;
                    return;
                }

                if (ImGui::IsMouseClicked(0) && s_dragNode.con) 
                    s_dragState = DragState_Draging;

                break;
            }

            case DragState_BeginDrag:
            {
                break;
            }

            case DragState_Draging:
            {
                ImDrawList* drawList = ImGui::GetWindowDrawList();

                drawList->ChannelsSetCurrent(0); // Background

                BezierCurve(drawList, s_dragNode.pos, ImGui::GetIO().MousePos );

                if (!ImGui::IsMouseDown(0))
                {
                    ImVec2 pos;
                    connection* con = getHoverCon(offset, &pos);

                    // Make sure we are still hovering the same node

                    if (con == s_dragNode.con)
                    {
                        s_dragNode.con = 0;
                        s_dragState = DragState_Default;
                        return;
                    }

                    // Lets connect the nodes.
                    // TODO: Make sure we connect stuff in the correct way!

                    con->m_Input = s_dragNode.con;
                    s_dragNode.con = 0;
                    s_dragState = DragState_Default;
                }

                break;
            }

            case DragState_Connect:
            {
                break;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void displayNode(ImDrawList* drawList, ImVec2 offset, node_base* node, int& node_selected)
    {
        int node_hovered_in_scene = -1;
        bool open_context_menu = false;

        ImGui::PushID(node->m_ID);
        ImVec2 node_rect_min = offset + node->m_Pos;

        // Display node contents first
        drawList->ChannelsSetCurrent(1); // Foreground
        bool old_any_active = ImGui::IsAnyItemActive();

        // Draw title in center

        ImVec2 textSize = ImGui::CalcTextSize(node->m_Name);

        ImVec2 pos = node_rect_min + NODE_WINDOW_PADDING;
        pos.x = node_rect_min.x + (node->m_Size.x / 2) - textSize.x / 2;

        ImGui::SetCursorScreenPos(pos);
        //ImGui::BeginGroup(); // Lock horizontal position
        ImGui::Text("%s", node->m_Name);
        //ImGui::SliderFloat("##value", &node->Value, 0.0f, 1.0f, "Alpha %.2f");
        //float dummy_color[3] = { node->Pos.x / ImGui::GetWindowWidth(), node->Pos.y / ImGui::GetWindowHeight(), fmodf((float)node->ID * 0.5f, 1.0f) };
        //ImGui::ColorEdit3("##color", &dummy_color[0]);

        // Save the size of what we have emitted and weither any of the widgets are being used
        bool node_widgets_active = (!old_any_active && ImGui::IsAnyItemActive());
        //node->size = ImGui::GetItemRectSize() + NODE_WINDOW_PADDING + NODE_WINDOW_PADDING;
        ImVec2 node_rect_max = node_rect_min + node->m_Size;

        // Display node box
        drawList->ChannelsSetCurrent(0); // Background

        ImGui::SetCursorScreenPos(node_rect_min);
        ImGui::InvisibleButton("node", node->m_Size);

        if (ImGui::IsItemHovered())
        {
            node_hovered_in_scene = node->m_ID;
            open_context_menu |= ImGui::IsMouseClicked(1);
        }

        bool node_moving_active = false;
    
        if (ImGui::IsItemActive() && !s_dragNode.con)
            node_moving_active = true;

        ImU32 node_bg_color = node_hovered_in_scene == node->m_ID ? ImColor(75,75,75) : ImColor(60,60,60);
        drawList->AddRectFilled(node_rect_min, node_rect_max, node_bg_color, 4.0f); 

        ImVec2 titleArea = node_rect_max;
        titleArea.y = node_rect_min.y + 30.0f;

        // Draw text bg area
        drawList->AddRectFilled(node_rect_min + ImVec2(1,1), titleArea, ImColor(100,0,0), 4.0f); 
        drawList->AddRect(node_rect_min, node_rect_max, ImColor(100,100,100), 4.0f); 

        ImVec2 off;

        //offset.y += 40.0f;

        //offset = offset + node_rect_min;

        offset = node_rect_min + ImVec2(0,40.0f);

        off.x = node_rect_min.x;
        off.y = node_rect_min.y;

        for (connection* con : node->m_InputConnections)
        {
            ImGui::SetCursorScreenPos(offset + ImVec2(10.0f, 0));
            ImGui::Text("%s", con->m_Desc.name);

            ImColor conColor = ImColor(150, 150, 150);

            if (isConnectorHovered(con, node_rect_min))
                conColor = ImColor(200, 200, 200);

            drawList->AddCircleFilled(node_rect_min + con->m_Pos, NODE_SLOT_RADIUS, conColor); 

            offset.y += textSize.y + 2.0f;
        }

        offset = node_rect_min;
        offset.y += 40.0f;

        for (connection* con : node->m_OutputConnections)
        {
            textSize = ImGui::CalcTextSize(con->m_Desc.name);

            ImGui::SetCursorScreenPos(offset + ImVec2(con->m_Pos.x - (textSize.x + 10.0f), 0));
            ImGui::Text("%s", con->m_Desc.name);

            ImColor conColor = ImColor(150, 150, 150);

            if (isConnectorHovered(con, node_rect_min))
                conColor = ImColor(200, 200, 200);

            drawList->AddCircleFilled(node_rect_min + con->m_Pos, NODE_SLOT_RADIUS, conColor); 

            offset.y += textSize.y + 2.0f;
        }


        //for (int i = 0; i < node->outputConnections.size(); ++i)
        //	drawList->AddCircleFilled(offset + node->outputSlotPos(i), NODE_SLOT_RADIUS, ImColor(150,150,150,150));

        if (node_widgets_active || node_moving_active)
            node_selected = node->m_ID;

        if (node_moving_active && ImGui::IsMouseDragging(0))
            node->m_Pos = node->m_Pos + ImGui::GetIO().MouseDelta;

        //ImGui::EndGroup();

        ImGui::PopID();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO: Ugly fix: me

    node_base* findNodeByCon(connection* findCon)
    {
        for (node_base* node : s_nodes)
        {
            for (connection* con : node->m_InputConnections)
            {
                if (con == findCon)
                    return node;
            }

            for (connection* con : node->m_OutputConnections)
            {
                if (con == findCon)
                    return node;
            }
        }

        return 0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void renderLines(ImDrawList* drawList, ImVec2 offset)
    {
        for (node_base* node : s_nodes)
        {
            for (connection* con : node->m_InputConnections)
            {
                if (!con->m_Input)
                    continue;

                node_base* targetNode = findNodeByCon(con->m_Input);

                if (!targetNode)
                    continue;

                BezierCurve(drawList, 
                    offset + targetNode->m_Pos + con->m_Input->m_Pos, 
                    offset + node->m_Pos + con->m_Pos );
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void ShowExampleAppCustomNodeGraph(bool* opened)
    {
        ImGui::SetNextWindowSize(ImVec2(700,600), ImGuiSetCond_FirstUseEver);
        if (!ImGui::Begin("Example: Custom Node Graph Simple", opened))
        {
            ImGui::End();
            return;
        }

        bool open_context_menu = false;
        int node_hovered_in_list = -1;
        int node_hovered_in_scene = -1;

        static int node_selected = -1;
        static ImVec2 scrolling = ImVec2(0.0f, 0.0f);

        ImGui::SameLine();
        ImGui::BeginGroup();

        // Create our child canvas
        //ImGui::Text("Hold middle mouse button to scroll (%.2f,%.2f)", scrolling.x, scrolling.y);
        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(1,1));
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
        ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, ImColor(40,40,40,200));
        ImGui::BeginChild("scrolling_region", ImVec2(0,0), true, ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoMove);
        ImGui::PushItemWidth(120.0f);


        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        draw_list->ChannelsSplit(2);
        //ImVec2 offset = ImGui::GetCursorScreenPos() - scrolling;

        //displayNode(draw_list, scrolling, s_emittable, node_selected);
        //displayNode(draw_list, scrolling, s_emitter, node_selected);

        const auto WinPos           = ImGui::GetWindowPos();
        const auto RelativeOrigin   = WinPos - scrolling;

        for (node_base* node : s_nodes)
            displayNode(draw_list, RelativeOrigin, node, node_selected);

        updateDraging( RelativeOrigin );
        renderLines(draw_list, RelativeOrigin );

        draw_list->ChannelsMerge();

        // Open context menu
        if (!ImGui::IsAnyItemHovered() && ImGui::IsMouseHoveringWindow() && ImGui::IsMouseClicked(1))
        {
            node_selected = node_hovered_in_list = node_hovered_in_scene = -1;
            open_context_menu = true;
        }
        if (open_context_menu)
        {
            ImGui::OpenPopup("context_menu");
            if (node_hovered_in_list != -1)
                node_selected = node_hovered_in_list;
            if (node_hovered_in_scene != -1)
                node_selected = node_hovered_in_scene;
        }

        // Draw context menu
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8,8));
        if (ImGui::BeginPopup("context_menu"))
        {
                if (ImGui::MenuItem("Load graph..."))
                {
                    /*
                    char path[1024];
                    if (Dialog_open(path))
                    {
                        printf("file to load %s\n", path);
                    }
                    */
                }

                if (ImGui::MenuItem("Save graph..."))
                {
                    /*
                    char path[1024];
                    if (Dialog_save(path))
                    {
                        saveNodes(path);
                    }
                    */
                }


            /*
            Node* node = node_selected != -1 ? &nodes[node_selected] : NULL;
            ImVec2 scene_pos = ImGui::GetMousePosOnOpeningCurrentPopup() - offset;
            if (node)
            {
                ImGui::Text("Node '%s'", node->Name);
                ImGui::Separator();
                if (ImGui::MenuItem("Rename..", NULL, false, false)) {}
                if (ImGui::MenuItem("Delete", NULL, false, false)) {}
                if (ImGui::MenuItem("Copy", NULL, false, false)) {}
            }
            */
            //else

            for (int i = 0; i < (int)sizeof_array(s_nodeTypes); ++i)
            {
                if (ImGui::MenuItem(s_nodeTypes[i].name))
                {
                    node_base* node = createNodeFromType( 
                         ImGui::GetIO().MousePos - WinPos + scrolling , &s_nodeTypes[i]);
                    s_nodes.append(node);
                }
            }

            ImGui::EndPopup();
        }
        ImGui::PopStyleVar();

        // Scrolling
        if (ImGui::IsWindowHovered() && !ImGui::IsAnyItemActive() && ImGui::IsMouseDragging(2, 0.0f))
            scrolling = scrolling - ImGui::GetIO().MouseDelta;

        ImGui::PopItemWidth();
        ImGui::EndChild();
        ImGui::PopStyleColor();
        ImGui::PopStyleVar(2);
        ImGui::EndGroup();

        ImGui::End();
    }
}