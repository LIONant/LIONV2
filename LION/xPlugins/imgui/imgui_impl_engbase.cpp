//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
// LION binding with ImGui

#include "eng_base.h"
#include <imgui.h>

//------------------------------------------------------------------------------------------------------------

#define GETINSTANCE                                                                                     \
    ImGuiIO&        io = ImGui::GetIO();                                                                \
    imgui_instance_data& Instance  = *reinterpret_cast<imgui_instance_data*>(io.UserData);

//------------------------------------------------------------------------------------------------------------
struct imgui_instance_data 
{
    eng_device&                     m_Device;
    eng_window&                     m_Window;
     
    eng_sampler::handle             m_hFontSampler          {};
    xtimer                          m_Time                  {};
    
    //------------------------------------------------------------------------------------------------------------
    imgui_instance_data( eng_device& D, eng_window& W ) : m_Device{D}, m_Window{W}{}

    //------------------------------------------------------------------------------------------------------------
    bool CreateFontsTexture( ImGuiIO& io )
    {
        // Build texture atlas
        unsigned char*  pixels;
        int             width, height;

        // Load as RGBA 32-bits (75% of the memory is wasted, but default font is so small) because it is more likely to be compatible with user's existing shaders. 
        // If your ImTextureId represent a higher-level concept than just a GL texture id, consider calling GetTexDataAsAlpha8() instead to save on GPU memory.
        io.Fonts->GetTexDataAsRGBA32( &pixels, &width, &height );   

        xbitmap                 Bitmap;
        eng_texture::handle     hTexture;
        xafptr<xcolor>          BitmapWithOffset;

        BitmapWithOffset.Alloc( 1 + width*height );
        BitmapWithOffset.CopyFromTo( xbuffer_view<xcolor>
        { 
            reinterpret_cast<xcolor*>(pixels), 
            static_cast<u64>(width)*static_cast<u64>(height) 
        });

        // Set the offset to zero
        BitmapWithOffset[0] = 0;

        // Now we can build the bitmap
        Bitmap.setupFromColor( 
            static_cast<u32>(width), 
            static_cast<u32>(height), 
            BitmapWithOffset,
            false );

        m_Device.CreateTexture( hTexture, Bitmap );

        eng_sampler::setup Setup( hTexture );
        Setup.m_AdressMode_U    = eng_sampler::ADDRESS_MODE_WRAP;
        Setup.m_AdressMode_V    = eng_sampler::ADDRESS_MODE_WRAP;
        Setup.m_MipmapMode      = eng_sampler::MIPMAP_MODE_LINEAR;
        Setup.m_MipmapMag       = eng_sampler::MIPMAP_SAMPLER_LINEAR;
        Setup.m_MipmapMag       = eng_sampler::MIPMAP_SAMPLER_LINEAR;
        Setup.m_MaxAnisotropy   = 8;
        m_Device.CreateSampler( m_hFontSampler, Setup );

        // We could put here the texture id if we wanted 
        io.Fonts->TexID = nullptr;

        return true;
    }

    //------------------------------------------------------------------------------------------------------------

    bool CreateDeviceObjects( ImGuiIO& io )
    {
        //
        // Create font texture
        //
        CreateFontsTexture( io );

        return true;
    }


    //------------------------------------------------------------------------------------------------------------
    void NewFrame( ImGuiIO& io )
    {
        //
        // Make sure everything is constructed
        //
        if( m_hFontSampler.isValid() == false )
        {
            CreateDeviceObjects( io );
        }

        //
        // Setup display size (every frame to accommodate for window resizing)
        //
        const int display_w = m_Window.getWidth();
        const int display_h = m_Window.getHeight();

        io.DisplaySize              = ImVec2((float)display_w, (float)display_h);

        float w = io.DisplaySize.x;
        float h = io.DisplaySize.y;

        io.DisplayFramebufferScale  = ImVec2(w > 0 ? ((float)display_w / w) : 0, h > 0 ? ((float)display_h / h) : 0);

        //
        // Setup time step
        //
        io.DeltaTime = static_cast<f32>( xtimer::ToSeconds( m_Time.TripNanoseconds() ));

        //
        // Update the mouse
        //
        {
            const auto& Mouse = m_Window.getLocalMouse();
            io.MousePos = [&]
            {
                const auto V3 = Mouse.getValue( eng_input::mouse::analog::POS_ABS );
                return ImVec2{ V3.m_X, V3.m_Y }; 
            }();

            io.MouseDown[0] = Mouse.isPressed( eng_input::mouse::digital::BTN_LEFT   );
            io.MouseDown[1] = Mouse.isPressed( eng_input::mouse::digital::BTN_RIGHT  );
            io.MouseDown[2] = Mouse.isPressed( eng_input::mouse::digital::BTN_MIDDLE );

            io.MouseWheel = [&]
            {
                const auto V3 = Mouse.getValue( eng_input::mouse::analog::WHEEL_REL );
                return V3.m_X;
            }();
        }

        //
        // Update keys
        //
        {
            const auto& Keyboard = m_Window.getLocalKeyboard();

            // Modifiers are not reliable across systems
            io.KeyCtrl  = Keyboard.isPressed( eng_input::keyboard::digital::KEY_LCONTROL ) || Keyboard.isPressed( eng_input::keyboard::digital::KEY_RCONTROL );
            io.KeyShift = Keyboard.isPressed( eng_input::keyboard::digital::KEY_LSHIFT   ) || Keyboard.isPressed( eng_input::keyboard::digital::KEY_RSHIFT   );
            io.KeyAlt   = Keyboard.isPressed( eng_input::keyboard::digital::KEY_LALT     ) || Keyboard.isPressed( eng_input::keyboard::digital::KEY_RALT     );
            io.KeySuper = Keyboard.isPressed( eng_input::keyboard::digital::KEY_LWIN     ) || Keyboard.isPressed( eng_input::keyboard::digital::KEY_RWIN     );

            bool bPresses = false;
            for( int i = 1; i < static_cast<int>( eng_input::keyboard::digital::ENUM_COUNT); i++ )
            {
                io.KeysDown[i] = Keyboard.isPressed( static_cast<eng_input::keyboard::digital>(i) );

                int c = Keyboard.isPressed( static_cast<eng_input::keyboard::digital>(i) );
                if( c )  bPresses |= true;
            }

            // Write into text boxes
            if( bPresses && !(Keyboard.isPressed( eng_input::keyboard::digital::KEY_LCONTROL ) | Keyboard.isPressed( eng_input::keyboard::digital::KEY_RCONTROL )) ) 
                io.AddInputCharacter( Keyboard.getLatestChar() );
        }

        // Start the frame
        ImGui::NewFrame();
    }

    //------------------------------------------------------------------------------------------------------------
    void Render( ImGuiIO& io, ImDrawData* draw_data )
    {
        if( draw_data->CmdListsCount == 0 ) return;

        auto&           CmdList     = m_Window.getDisplayCmdList( 1000000 );
        const auto&     View        = m_Window.getActiveView();
        const xmatrix4  L2C          = View.getPixelBased2D();

        if( View.getViewport().getWidth() == 0 || View.getViewport().getHeight() == 0 ) return; 

        // This is a hack and is showing something is wrong
        const float HalfPixel =  -(1.0f/m_hFontSampler->getTextureHandle()->getWidth());

        eng_draw::pipeline PipeLine;

        // Scale the clip rectangles
        draw_data->ScaleClipRects(io.DisplayFramebufferScale);


        PipeLine.m_CULL    = eng_draw::CULL_OFF;
        PipeLine.m_BLEND   = eng_draw::BLEND_ALPHA_ORIGINAL;
        PipeLine.m_ZBUFFER = eng_draw::ZBUFFER_OFF;

        CmdList.Draw( PipeLine, [&]( eng_draw& Draw )
        {
            Draw.setSampler( m_hFontSampler );

            for( int n = 0; n < draw_data->CmdListsCount; n++ )
            {
                const ImDrawList&   CmdList     = *draw_data->CmdLists[n];
                auto                Buffers     = Draw.popBuffers( CmdList.VtxBuffer.Size, CmdList.IdxBuffer.Size );
                int                 IndexOffset = 0;

                //
                // Convert the imgui-verts into draw verts
                //
                for( int i = 0; i < CmdList.VtxBuffer.Size; ++i )
                {
                    const auto&     ImguiV = CmdList.VtxBuffer.Data[i];
                    auto&           DrawV  = Buffers.m_Vertices[i];        
                    const xcolor    C      = x_EndianSwap<u32>( ImguiV.col );

                    DrawV.setup( ImguiV.pos.x, ImguiV.pos.y, 0, ImguiV.uv.x + HalfPixel, ImguiV.uv.y, C );
                }

                //
                // Copy over the indices
                //
                for( u32 i = 0; i < static_cast<u32>(CmdList.IdxBuffer.Size); ++i )                                                   
                    Buffers.m_Indices[ i ] = CmdList.IdxBuffer.Data[ i ];

                //
                // Render each of the display lists
                //
                for (int i = 0; i < CmdList.CmdBuffer.Size; i++)
                {
                    const ImDrawCmd& cmd = CmdList.CmdBuffer[i];
                    if( cmd.UserCallback )
                    {
                        cmd.UserCallback( &CmdList, &cmd );
                    }
                    else
                    {
                        Draw.setScissor( xirect
                        { 
                            static_cast<int>(cmd.ClipRect.x), 
                            static_cast<int>(cmd.ClipRect.y), 
                            static_cast<int>(cmd.ClipRect.z), 
                            static_cast<int>(cmd.ClipRect.w) 
                        });
                        
                        //
                        // Draw Triangles
                        //
                        Draw.DrawBufferTriangles( L2C, IndexOffset, cmd.ElemCount );
                    }

                    IndexOffset += cmd.ElemCount;
                }
            }
        });

        //
        // Submit the display list 
        //
        m_Window.SubmitDisplayCmdList( CmdList );
    }
};

//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------

void ImGui_engBase_RenderDrawLists( ImDrawData* draw_data )
{
    GETINSTANCE;
    Instance.Render( io, draw_data );
}


//------------------------------------------------------------------------------------------------------------

void ImGui_engBase_NewFrame()
{
    GETINSTANCE;
    Instance.NewFrame( io );
}

//------------------------------------------------------------------------------------------------------------

bool ImGui_engBase_Init( eng_device& Device, eng_window& Window, bool install_callbacks )
{
    //
    // Initialize the instance
    //
    xndptr_s<imgui_instance_data> Ptr;
    Ptr.New( Device, Window );

    ImGuiIO& io = ImGui::GetIO();
    io.KeyMap[ImGuiKey_Tab]         = static_cast<int>( eng_input::keyboard::digital::KEY_TAB       );                         // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
    io.KeyMap[ImGuiKey_LeftArrow]   = static_cast<int>( eng_input::keyboard::digital::KEY_LEFT      );
    io.KeyMap[ImGuiKey_RightArrow]  = static_cast<int>( eng_input::keyboard::digital::KEY_RIGHT     );
    io.KeyMap[ImGuiKey_UpArrow]     = static_cast<int>( eng_input::keyboard::digital::KEY_UP        );
    io.KeyMap[ImGuiKey_DownArrow]   = static_cast<int>( eng_input::keyboard::digital::KEY_DOWN      );
    io.KeyMap[ImGuiKey_PageUp]      = static_cast<int>( eng_input::keyboard::digital::KEY_PAGEUP    );
    io.KeyMap[ImGuiKey_PageDown]    = static_cast<int>( eng_input::keyboard::digital::KEY_PAGEDOWN  );
    io.KeyMap[ImGuiKey_Home]        = static_cast<int>( eng_input::keyboard::digital::KEY_HOME      );
    io.KeyMap[ImGuiKey_End]         = static_cast<int>( eng_input::keyboard::digital::KEY_END       );
    io.KeyMap[ImGuiKey_Delete]      = static_cast<int>( eng_input::keyboard::digital::KEY_DELETE    );
    io.KeyMap[ImGuiKey_Backspace]   = static_cast<int>( eng_input::keyboard::digital::KEY_BACKSPACE );
    io.KeyMap[ImGuiKey_Enter]       = static_cast<int>( eng_input::keyboard::digital::KEY_ENTER     );
    io.KeyMap[ImGuiKey_Escape]      = static_cast<int>( eng_input::keyboard::digital::KEY_ESCAPE    );
    io.KeyMap[ImGuiKey_A]           = static_cast<int>( eng_input::keyboard::digital::KEY_A         );
    io.KeyMap[ImGuiKey_C]           = static_cast<int>( eng_input::keyboard::digital::KEY_C         );
    io.KeyMap[ImGuiKey_V]           = static_cast<int>( eng_input::keyboard::digital::KEY_V         );
    io.KeyMap[ImGuiKey_X]           = static_cast<int>( eng_input::keyboard::digital::KEY_X         );
    io.KeyMap[ImGuiKey_Y]           = static_cast<int>( eng_input::keyboard::digital::KEY_Y         );
    io.KeyMap[ImGuiKey_Z]           = static_cast<int>( eng_input::keyboard::digital::KEY_Z         );

    //
    // Let the begin
    //
    Ptr->m_Time.Start();

    //
    // Setup data for the imgui
    //
    io.RenderDrawListsFn  = ImGui_engBase_RenderDrawLists;       // Alternatively you can set this to NULL and call ImGui::GetDrawData() after ImGui::Render() to get the same ImDrawData pointer.
    io.UserData           = Ptr.TransferOwnership();

#if _X_TARGET_WINDOWS
    io.ImeWindowHandle = (HWND)Window.getSystemWindowHandle(); 
#endif

    return true;
}

//------------------------------------------------------------------------------------------------------------

void ImGui_engBase_Shutdown( void )
{
    ImGuiIO&        io = ImGui::GetIO();
    x_delete( io.UserData );
    io.UserData = nullptr;
}


