//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

namespace imgui_property
{
    //-------------------------------------------------------------------------------------------
    struct empty{};

    //-------------------------------------------------------------------------------------------

    template< typename T_BASE2 = empty >
    struct item_topfolder;
    struct base_item;

    //-------------------------------------------------------------------------------------------

    class window 
    {
    public:

        x_object_type( window, rtti_start );

    public:

        template< typename T >
        T& AddTreeTopNode( xowner<T*> pTreeTopNode )
        {
            return m_TreeNodes.append().setup( pTreeTopNode );
        }

        void    Update              ( void );
        void    Render              ( void )            { onRender(); }
     //   void    setupFloatingWindow ( bool bFloating )  { m_bFloatWindow = bFloating; }

    public: // system public functions

        void    UpdateSelected      ( base_item* pSelected );
        void    RenderBackground    ( xcolor Color, bool bAdvance = true );
        int     getTopFolderIndex   ( item_topfolder<>* pTopFolder ) const
        {
            for( int i = 0; i < m_TreeNodes.getCount(); i++ )
            {
                if( &m_TreeNodes[i][0] == pTopFolder )
                    return i;
            }
            return -1;
        }

    protected:

                void ShowHelpMarker         ( const char* desc );
        virtual void onRender               ( void );
        virtual void onPrintHelp            ( void );
        virtual void onNotifyChange         ( base_item& BaseItem ){}
        virtual void onNotifyGuidClick      ( base_item& BaseItem ){}
        virtual void onUpdate               ( void );

    protected:

    //    xstring                             m_WindowName    { X_STR( "Some property window" ) };
        base_item*                          m_pSelectedItem {nullptr};
        xvector<xndptr_s<item_topfolder<>>> m_TreeNodes     {};
        int                                 m_nRows         {0};
        bool                                m_isOpen        { true };
    //    bool                                m_bFloatWindow  { false };

        template< typename T> friend struct item_folder; 
        template< typename T> friend struct item_guid;
    };

    //-------------------------------------------------------------------------------------------
    struct base_item
    {
        x_object_type( base_item, rtti_start, is_not_copyable, is_not_movable )

        window&     m_Window;      
        xstring     m_Name          { X_STR("It must have a name") };
        bool        m_bTreeNode     { false };
        bool        m_bReadOnly     { true };
        xstring     m_Help          { X_STR("No help given") };
        xcolor      m_BkColor       { 255, 255, 255, 15};

                        base_item           ( window& W ) : m_Window(W){}
        virtual void*   onGetUserData       ( void ) = 0;
        virtual void    onRender            ( void ) = 0;
        virtual bool    onUpdate            ( void ) = 0;
        virtual void    onNotifyChange      ( void ) = 0;
        virtual         ~base_item          ( void ){}
        virtual void    RenderBackground    ( bool bAdvance = true )
        {
            m_Window.RenderBackground( m_BkColor, bAdvance );
        }

        void DrawDisable( const ImVec2& pos )
        {
            ImGui::GetWindowDrawList()->AddRectFilled( pos, ImVec2( pos.x + ImGui::GetContentRegionAvailWidth(), pos.y + ImGui::GetTextLineHeight()), xcolor(50,120,0,0) );
        }

        template< typename T >
        T& getUserData ( void ){ return *reinterpret_cast<T*>( onGetUserData() ); }

        friend class window;
    };

    //-------------------------------------------------------------------------------------------

    template< int T_DIMENSIONS, typename T_BASE2 = empty >
    struct item_float : base_item, T_BASE2 
    {
        x_object_type( item_float, rtti(base_item), is_not_copyable, is_not_movable )

        enum class edit_style
        {
            DRAG,
            SLIDER,
            EDIT
        };

        xarray<float,T_DIMENSIONS>  m_Value             { 0 };
        xarray<float,T_DIMENSIONS>  m_Backup            { 0 };
        xarray<char*,3>             m_DimensionName     {"X", "Y", "Z"};
        bool                        m_bOpen             { false };
        xarray<float,T_DIMENSIONS>  m_MinValue          { 0 };
        xarray<float,T_DIMENSIONS>  m_MaxValue          { 0 };
        float                       m_Step              { 0.01f };
        char*                       m_pFmtString        { "%.3f"};
        char*                       m_pDetailFmtString  { "%g"};
        edit_style                  m_EditStyle         { edit_style::DRAG };
        edit_style                  m_EditStyleDetails  { edit_style::DRAG };

        static_assert( T_DIMENSIONS>=1 && T_DIMENSIONS<=3, "Wrong Dimension Number" );

                     item_float     ( window& W ) : base_item(W){}

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange ( void ) override
        {
            // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            for( int i = 0; i < T_DIMENSIONS; i++ )
            {
                if( m_Value[i] != m_Backup[i] )
                {
                    onNotifyChange();
                    *m_Backup = *m_Value;
                   return true;
                }
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();
            ImGui::PushID( this );
            {
                if( T_DIMENSIONS == 1 )
                {
                    ImGui::Bullet();
                    ImGui::Selectable( m_Name );
                    m_bOpen = false;
                }
                else
                {
                    m_bOpen = ImGui::TreeNode( "Vector3", m_Name );
                }
                m_Window.UpdateSelected( this );

                ImGui::NextColumn();
            
                static const ImVec4 ColOnWhite[]
                {  
                    ImVec4( 1.0f, 0.6f, 0.6f, 1.f ),
                    ImVec4( 0.2f, 0.7f, 0.2f, 1.f ),
                    ImVec4( 0.6f, 0.6f, 1.0f, 1.f )
                };

                static const ImVec4 ColOnBackground[]
                {  
                    ImVec4( 0.7f, 0.2f, 0.2f, 1.f ),
                    ImVec4( 0.4f, 1.0f, 4.f,  1.f ), 
                    ImVec4( 0.4f, 0.4f, 1.f,  1.f ) 
                };

                ImGuiStyle * style = &ImGui::GetStyle();
                if( m_bReadOnly == false )
                {
                    ImGui::PushItemWidth( -1 );
                    ImGui::BeginGroup();

                    if( T_DIMENSIONS > 1 ) ImGui::PushItemWidth( ImGui::GetContentRegionAvailWidth()/T_DIMENSIONS - style->ItemInnerSpacing.x/T_DIMENSIONS );
                    for( int i = 0; i < T_DIMENSIONS; i++ )
                    {
                        ImGui::PushID(m_Value + i); 
                        if( T_DIMENSIONS != 1 ) ImGui::PushStyleColor( ImGuiCol_Text, ColOnWhite[i] );
                        x_assert( m_MinValue[i] < m_MaxValue[i] );     
                        switch( m_EditStyle )
                        {
                            case edit_style::DRAG:      ImGui::DragFloat  ( "##value", &m_Value[i], m_Step, m_MinValue[i], m_MaxValue[i], m_pFmtString ); break; 
                            case edit_style::SLIDER:    ImGui::SliderFloat( "##value", &m_Value[i], m_MinValue[i], m_MaxValue[i], m_pFmtString ); break; 
                            case edit_style::EDIT:      ImGui::InputFloat ( "##value", &m_Value[i], m_MinValue[i], m_MaxValue[i] ); break; 
                        }
                         
                        if( i < (T_DIMENSIONS-1) )ImGui::SameLine( 0, 2 ); 
                        if( T_DIMENSIONS != 1 ) ImGui::PopStyleColor(1); 
                        ImGui::PopID();
                    }
                    if( T_DIMENSIONS > 1 ) ImGui::PopItemWidth();
                    ImGui::EndGroup();

                }
                else
                {
                    ImVec2 pos = ImGui::GetCursorScreenPos();
                    if( T_DIMENSIONS == 1 )
                    {
                        ImGui::PushItemWidth( -1 );
                        {
                            ImGui::Button( xstring::Make( m_pFmtString, m_Value[0] ), ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                        }
                        ImGui::PopItemWidth();
                    }
                    else
                    {
                        const float W = ImGui::GetContentRegionAvailWidth()/T_DIMENSIONS - style->ItemInnerSpacing.x/T_DIMENSIONS;
                        for( int i = 0; i < T_DIMENSIONS; i++ )
                        {
                            if( T_DIMENSIONS != 1 )ImGui::PushStyleColor( ImGuiCol_Text, ColOnWhite[i] );
                            ImGui::Button( xstring::Make( m_pFmtString, m_Value[i] ), ImVec2(W, 0 ) );
                            if( i < (T_DIMENSIONS-1) )ImGui::SameLine( 0, 2 ); 
                            if( T_DIMENSIONS != 1 ) ImGui::PopStyleColor(1); 
                        } 
                    }
                    DrawDisable( pos );
                }
                ImGui::NextColumn();

                //
                // Add the actual editing fields
                //
                if( m_bOpen )
                {
                    for( int i = 0; i < T_DIMENSIONS; i++ )
                    {
                        ImGui::PushID(m_Value + T_DIMENSIONS + i);
                        {
                            ImGui::AlignFirstTextHeightToWidgets();
                            RenderBackground();
                            ImGui::Bullet();
                            ImGui::Selectable( m_DimensionName[i] );
                            m_Window.UpdateSelected( this );
                            ImGui::NextColumn();

                            ImGui::PushItemWidth(-1);
                            ImGui::PushStyleColor(ImGuiCol_Text,            ColOnWhite[i] );

                            if( m_bReadOnly == false )
                            {
                                x_assert( m_MinValue[i] < m_MaxValue[i] );
                                switch( m_EditStyleDetails )
                                {
                                    case edit_style::DRAG:    ImGui::DragFloat  ( "##value", &m_Value[i], m_Step, m_MinValue[i], m_MaxValue[i], m_pDetailFmtString ); break; 
                                    case edit_style::SLIDER:  ImGui::SliderFloat( "##value", &m_Value[i],         m_MinValue[i], m_MaxValue[i], m_pDetailFmtString ); break; 
                                    case edit_style::EDIT:    ImGui::InputFloat ( "##value", &m_Value[i],         m_MinValue[i], m_MaxValue[i] ); break; 
                                }
                            }
                            else
                            {
                                ImVec2 pos = ImGui::GetCursorScreenPos();
                                ImGui::Button( xstring::Make( m_pDetailFmtString, m_Value[i]), ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                                DrawDisable( pos );
                            }
                            ImGui::PopStyleColor(1);
                            ImGui::PopItemWidth();
                            ImGui::NextColumn();
                        }
                        ImGui::PopID();
                    }
                    ImGui::TreePop();
                }
            }
            ImGui::PopID();
        }
    };

    //-------------------------------------------------------------------------------------------

    template< int T_DIMENSIONS, typename T_BASE2 = empty >
    struct item_int : base_item, T_BASE2
    {
        x_object_type( item_int, rtti(base_item), is_not_copyable, is_not_movable )

        enum class edit_style
        {
            DRAG,
            SLIDER,
            EDIT
        };

        xarray<int,T_DIMENSIONS>    m_Value             { 0 };
        xarray<int,T_DIMENSIONS>    m_Backup            { 0 };
        xarray<char*,3>             m_DimensionName     {"X", "Y", "Z"};
        bool                        m_bOpen             { false };
        int                         m_MinValue          { 0 };
        int                         m_MaxValue          { 0 };
        float                       m_Step              { 1.0f };
        char*                       m_pFmtString        { "%d"};
        char*                       m_pDetailFmtString  { "%d"};
        edit_style                  m_EditStyle         { edit_style::DRAG };
        edit_style                  m_EditStyleDetails  { edit_style::DRAG };


        static_assert( T_DIMENSIONS>=1 && T_DIMENSIONS<=3, "Wrong Dimension Number" );

                        item_int     ( window& W ) : base_item(W){}

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange ( void ) override
        {
            // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            for( int i = 0; i < T_DIMENSIONS; i++ )
            {
                if( m_Value[i] != m_Backup[i] )
                {
                    onNotifyChange();
                    *m_Backup = *m_Value;
                    return true;
                }
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();

            ImGui::PushID( this );
            {
                if( T_DIMENSIONS == 1 )
                {
                    ImGui::Bullet();
                    ImGui::Selectable( m_Name );
                    m_bOpen = false;
                }
                else
                {
                    m_bOpen = ImGui::TreeNode( "Vector3", m_Name );
                }
                m_Window.UpdateSelected( this );
                ImGui::NextColumn();
            
                static const ImVec4 ColOnWhite[]
                {  
                    ImVec4( 1.0f, 0.4f, 0.4f, 1.f ),
                    ImVec4( 0.2f, 0.7f, 0.2f, 1.f ),
                    ImVec4( 0.4f, 0.4f, 1.0f, 1.f )
                };

                static const ImVec4 ColOnBackground[]
                {  
                    ImVec4( 0.7f, 0.2f, 0.2f, 1.f ),
                    ImVec4( 0.4f, 1.0f, 4.f,  1.f ), 
                    ImVec4( 0.4f, 0.4f, 1.f,  1.f ) 
                };

                ImGuiStyle * style = &ImGui::GetStyle();
                if( m_bReadOnly == false )
                {
                    ImGui::BeginGroup();
                    
                    if( T_DIMENSIONS > 1 ) ImGui::PushItemWidth( ImGui::GetContentRegionAvailWidth()/T_DIMENSIONS - style->ItemInnerSpacing.x/T_DIMENSIONS );
                    else ImGui::PushItemWidth( -1 ); 
                    for( int i = 0; i < T_DIMENSIONS; i++ )
                    {
                        ImGui::PushID(m_Value + i); 
                        if( T_DIMENSIONS != 1 ) ImGui::PushStyleColor( ImGuiCol_Text, ColOnWhite[i] ); 
                        switch( m_EditStyle )
                        {
                            case edit_style::DRAG:                                              ImGui::DragInt    ( "##value", &m_Value[i], m_Step, m_MinValue, m_MaxValue ); break; 
                            case edit_style::SLIDER:    x_assert( m_MinValue < m_MaxValue );    ImGui::SliderInt  ( "##value", &m_Value[i], m_MinValue, m_MaxValue ); break; 
                            case edit_style::EDIT:                                              ImGui::InputInt   ( "##value", &m_Value[i], m_MinValue, m_MaxValue ); break; 
                        }
                         
                        if( i < (T_DIMENSIONS-1) )ImGui::SameLine( 0, 2 ); 
                        if( T_DIMENSIONS != 1 ) ImGui::PopStyleColor(1); 
                        ImGui::PopID();
                    }
                    ImGui::PopItemWidth();
                    ImGui::EndGroup();

                }
                else
                {
                    ImVec2 pos = ImGui::GetCursorScreenPos();
                    if( T_DIMENSIONS == 1 )
                    {
                        ImGui::PushItemWidth( -1 );
                        {
                            ImGui::Button( xstring::Make( m_pFmtString, m_Value[0] ), ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                        }
                        ImGui::PopItemWidth();
                    }
                    else
                    {
                        const float W = ImGui::GetContentRegionAvailWidth()/T_DIMENSIONS - style->ItemInnerSpacing.x/T_DIMENSIONS;
                        for( int i = 0; i < T_DIMENSIONS; i++ )
                        {
                            if( T_DIMENSIONS != 1 )ImGui::PushStyleColor( ImGuiCol_Text, ColOnWhite[i] );
                            ImGui::Button( xstring::Make( m_pFmtString, m_Value[i] ), ImVec2(W, 0 ) );
                            if( i < (T_DIMENSIONS-1) )ImGui::SameLine( 0, 2 ); 
                            if( T_DIMENSIONS != 1 ) ImGui::PopStyleColor(1); 
                        } 
                    }
                    DrawDisable( pos );
                }

                ImGui::NextColumn();

                //
                // Add the actual editing fields
                //
                if( m_bOpen )
                {
                    for( int i = 0; i < T_DIMENSIONS; i++ )
                    {
                        ImGui::PushID(m_Value + T_DIMENSIONS + i);
                        {
                            ImGui::AlignFirstTextHeightToWidgets();
                            RenderBackground();
                            ImGui::Bullet();
                            ImGui::Selectable( m_DimensionName[i] );
                            m_Window.UpdateSelected( this );
                            ImGui::NextColumn();

                            ImGui::PushItemWidth(-1);
                            ImGui::PushStyleColor(ImGuiCol_Text,            ColOnWhite[i] );

                            if( m_bReadOnly == false )
                            {
                                switch( m_EditStyleDetails )
                                {
                                    case edit_style::DRAG:                                              ImGui::DragInt  ( "##value", &m_Value[i], m_Step, m_MinValue, m_MaxValue, m_pDetailFmtString ); break; 
                                    case edit_style::SLIDER:    x_assert( m_MinValue < m_MaxValue );    ImGui::SliderInt( "##value", &m_Value[i],         m_MinValue, m_MaxValue, m_pDetailFmtString ); break; 
                                    case edit_style::EDIT:                                              ImGui::InputInt ( "##value", &m_Value[i],         m_MinValue, m_MaxValue ); break; 
                                }
                            }
                            else
                            {
                                ImVec2 pos = ImGui::GetCursorScreenPos();
                                ImGui::Button( xstring::Make( m_pDetailFmtString, m_Value[i]), ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                                DrawDisable( pos );
                            }
                            ImGui::PopStyleColor(1);
                            ImGui::PopItemWidth();
                            ImGui::NextColumn();
                        }
                        ImGui::PopID();
                    }
                    ImGui::TreePop();
                }
            }
            ImGui::PopID();
        }
    };

    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 = empty >
    struct item_combo : base_item, T_BASE2
    {
        x_object_type( item_combo, rtti(base_item), is_not_copyable, is_not_movable )

        xvector<xstring>    m_List      {};
        int                 m_Value     { -1 };
        int                 m_Backup    { -1 };

                     item_combo     ( window& W ) : base_item(W){}

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange( void ) override
        {
             // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            if( m_Value != m_Backup )
            {
                onNotifyChange();
                m_Backup = m_Value;
                return true;
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();

            ImGui::PushID( this );
            ImGui::AlignFirstTextHeightToWidgets();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
            {
                ImGui::AlignFirstTextHeightToWidgets();
                ImGui::Bullet();
                ImGui::Selectable( m_Name );
                m_Window.UpdateSelected( this );
                ImGui::NextColumn();

                ImGui::PushItemWidth(-1);
                // Combo using proper array. You can also pass a callback to retrieve array value, no need to create/copy an array just for that.
                if( m_bReadOnly )
                {
                    ImVec2 pos = ImGui::GetCursorScreenPos();
                    ImGui::PushItemWidth( -1 );
                    {
                        ImGui::Button( m_List[m_Value], ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                    }
                    ImGui::PopItemWidth();
                    DrawDisable( pos );
                }
                else
                {
                    ImGui::Combo( "combo scroll", &m_Value, (const char**)&m_List[0], m_List.getCount<int>() );   
                }

                ImGui::PopItemWidth();
                ImGui::NextColumn();
            }
            ImGui::PopID();
        }
    };

    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 = empty >
    struct item_checkbox : base_item, T_BASE2
    {
        x_object_type( item_checkbox, rtti(base_item), is_not_copyable, is_not_movable )

        bool        m_Value     { false };
        bool        m_Backup    { false };

                        item_checkbox     ( window& W ) : base_item(W){}

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange( void ) override
        {
             // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            if( m_Value != m_Backup )
            {
                onNotifyChange();
                m_Backup = m_Value;
                return true;
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();
            ImGui::PushID( this );
            ImGui::AlignFirstTextHeightToWidgets();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
            {
                ImGui::AlignFirstTextHeightToWidgets();
                ImGui::Bullet();
                ImGui::Selectable( m_Name );
                m_Window.UpdateSelected( this );
                ImGui::NextColumn();

                ImGui::PushItemWidth(-1);
                ImVec2 pos = ImGui::GetCursorScreenPos();
                if( m_Value ) ImGui::Checkbox( "TRUE",  &m_Value );
                else          ImGui::Checkbox( "FALSE", &m_Value );
                if( m_bReadOnly )
                {
                    DrawDisable(pos);
                    m_Value = m_Backup;
                }

                ImGui::PopItemWidth();
                ImGui::NextColumn();
            }
            ImGui::PopID();
        }
    };
    
    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 = empty >
    struct item_color : base_item, T_BASE2
    {
        x_object_type( item_color, rtti(base_item), is_not_copyable, is_not_movable )

        xarray<float,4>       m_Value  { 1.f };
        xarray<float,4>       m_Backup { 1.f };

                        item_color     ( window& W ) : base_item(W){}

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange( void ) override
        {
             // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            if( m_Value[0] != m_Backup[0] ||
                m_Value[1] != m_Backup[1] ||
                m_Value[2] != m_Backup[2] ||
                m_Value[3] != m_Backup[3])
            {
                onNotifyChange();
                m_Backup = m_Value;
                return true;
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();
            ImGui::PushID( this );
            ImGui::AlignFirstTextHeightToWidgets();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
            {
                ImGui::Bullet();
                ImGui::Selectable( m_Name );
                m_Window.UpdateSelected( this );
                ImGui::NextColumn();
                ImVec2 pos = ImGui::GetCursorScreenPos();
                ImGui::PushItemWidth(-1);
                ImGui::ColorEdit4( "Choose color", m_Value );
                if( m_bReadOnly )
                {
                    m_Value = m_Backup;
                    DrawDisable(pos); 
                }
                ImGui::PopItemWidth();
                ImGui::NextColumn();
            }
            ImGui::PopID();
        }
    };

    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 = empty >
    struct item_button : base_item, T_BASE2
    {
        x_object_type( item_button, rtti(base_item), is_not_copyable, is_not_movable )

        bool                    m_Value     { false };
        bool                    m_Backup    { false };
        xstring                 m_Label     { X_STR("Button") };

                        item_button     ( window& W ) : base_item(W){}

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange( void ) override
        {
             // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            if( m_Value != m_Backup )
            {
                onNotifyChange();
                m_Backup = m_Value;
                return true;
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();
            ImGui::PushID( this );
            ImGui::AlignFirstTextHeightToWidgets();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
            {
                ImGui::Bullet();
                ImGui::Selectable( m_Name );
                m_Window.UpdateSelected( this );
                ImGui::NextColumn();
                ImGui::PushItemWidth(-1);
                if( m_bReadOnly )
                {
                    ImVec2 pos = ImGui::GetCursorScreenPos();
                    ImGui::Button( m_Label, ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                    DrawDisable(pos);
                }
                else
                {
                    m_Value = ImGui::Button( m_Label, ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                }
                ImGui::PopItemWidth();
                ImGui::NextColumn();
            }
            ImGui::PopID();
        }
    };

    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 = empty >
    struct item_string : base_item, T_BASE2
    {
        x_object_type( item_string, rtti(base_item), is_not_copyable, is_not_movable )

        xarray<xchar,256>       m_Value     { 0 };
        xarray<xchar,256>       m_Backup    { 0 };

                        item_string     ( window& W ) : base_item(W){}

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange( void ) override
        {
             // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            if( x_strcmp<xchar>( m_Value, m_Backup ) )
            {
                onNotifyChange();
                x_strcpy<xchar>( m_Backup, m_Value );
                return true;
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();
            ImGui::PushID( this );
            ImGui::AlignFirstTextHeightToWidgets();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
            {
                ImGui::AlignFirstTextHeightToWidgets();
                ImGui::Bullet();
                ImGui::Selectable( m_Name );
                m_Window.UpdateSelected( this );
                ImGui::NextColumn();

                ImGui::PushItemWidth(-1);
                if( m_bReadOnly )
                {
                    ImVec2 pos = ImGui::GetCursorScreenPos();
                    ImGui::Text( m_Value );
                    DrawDisable(pos);
                }
                else
                {
                    ImGui::InputText( "hello...", m_Value, m_Value.getCount() );
                }
                ImGui::PopItemWidth();
                ImGui::NextColumn();
            }
            ImGui::PopID();
        }
    };

    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 = empty >
    struct item_guid : base_item, T_BASE2
    {
        x_object_type( item_guid, rtti(base_item), is_not_copyable, is_not_movable )

        using edit_style = xproperty_v2::info::guid::edit_style;

        u64                     m_Value         { 0 };
        u64                     m_Backup        { 0 };
        u64                     m_Type          { 0 };
        xstring::const_str      m_Category      {nullptr};
        xstring                 m_Label         { X_STR("None") }; 
        bool                    m_isPress       { false };
        ImColor                 m_ButtonColor   {~0};
        edit_style              m_EditStyle     { edit_style::DEFAULT };

                        item_guid       ( window& W ) : base_item(W){}
        
        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        virtual void onNotifyChange( void ) override
        {
             // Nothing to do the user should override this
        }

        virtual void onNotifyGuidClick( void ) 
        {
             // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            if( m_isPress )
            {
                onNotifyGuidClick();
                m_Window.onNotifyGuidClick( *this );
            }

            if( m_Value != m_Backup )
            {
                onNotifyChange();
                m_Backup = m_Value;
                return true;
            }
            return false;
        }

        virtual void onRender( void ) override
        {
            RenderBackground();
            ImGui::PushID( this );
            ImGui::AlignFirstTextHeightToWidgets();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
            {
                ImGui::Bullet();
                ImGui::Selectable( m_Name );
                m_Window.UpdateSelected( this );
                ImGui::NextColumn();

                ImGui::PushItemWidth(-1);
                ImGui::PushStyleColor(ImGuiCol_Button, m_ButtonColor );
            //    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImColor::HSV(i/7.0f, 0.7f, 0.7f));
            //    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImColor::HSV(i/7.0f, 0.8f, 0.8f));

                ImVec2 pos = ImGui::GetCursorScreenPos();
                m_isPress = ImGui::Button( xstring::Make("%X:%X", u32(m_Value>>32), u32(m_Value) ), ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                    /*
                switch( m_EditStyle )
                {
                    case edit_style::DEFAULT:
                    {
                        m_isPress = ImGui::Button( m_Label, ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                        break;
                    }
                    default://case edit_style::RAW:
                    {
                        m_isPress = ImGui::Button( xstring::Make("%X:%X", u32(m_Value>>32), u32(m_Value) ), ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) );
                        break;
                    }
                    default: x_assert( false );
                }
                    */

                if( m_bReadOnly )
                {
                    DrawDisable(pos);
                    m_isPress = false;
                }
                    
                ImGui::PopStyleColor(1);
                ImGui::PopItemWidth();
                ImGui::NextColumn();
            }
            ImGui::PopID();
        }
    };

    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 = empty >
    struct item_folder : base_item, T_BASE2
    {
        x_object_type( item_folder, rtti(base_item), is_not_copyable, is_not_movable )

        item_folder     ( window& W ) : base_item(W)
        {
            m_bTreeNode = true;
        }

        virtual void* onGetUserData     ( void ) { return (T_BASE2*)this; }

        template< typename T >
        T& CreateItem( void )
        {
            xowner<auto*> pItem = x_new( T, { m_Window } );
            m_PropList.append().setup( pItem );
            return *pItem;
        }

        virtual void onNotifyChange( void ) override
        {
             // Nothing to do the user should override this
        }

        virtual bool onUpdate( void ) override final
        {
            bool bUpdated = false;
            for( auto& Entry : m_PropList )
            {
                if( Entry->onUpdate() )
                {
                    bUpdated = true;
                    if( !Entry->m_bTreeNode )
                        m_Window.onNotifyChange( Entry );
                }
            }

            return bUpdated;
        }

        virtual void onGadgetRender( void )
        {
            ImGui::Text("");
        }

        virtual void onRender( void ) override
        {
            RenderBackground();

            //
            // Render my self completely
            //
            ImGui::PushID( this );
            {
                ImGuiTreeNodeFlags Flags = 0;
                if( m_isTopTree )
                {
                    Flags |= ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_DefaultOpen;
                }

                // Create the folder
                m_isOpen = ImGui::TreeNodeEx( m_Name, Flags );

                m_Window.UpdateSelected( this );
                ImGui::NextColumn();

                ImGui::AlignFirstTextHeightToWidgets();
                onGadgetRender();
                ImGui::NextColumn();

                //
                // if I am open the render my children
                //
                if( m_isOpen )
                {
                    for( auto& Entry : m_PropList )
                    {
                        // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
                        ImGui::AlignFirstTextHeightToWidgets();  
                        Entry->onRender();
                    }

                    
                    //
                    // Pop tree
                    //
                    ImGui::TreePop();
                }
            }
            ImGui::PopID();
        }

        xvector<xndptr_s<base_item>>    m_PropList;
        bool                            m_isOpen    = false;
        bool                            m_isTopTree = false;
    };

    //-------------------------------------------------------------------------------------------
    template< typename T_BASE2 >
    struct item_topfolder : item_folder<T_BASE2>
    {
        x_object_type( item_topfolder, rtti(item_folder), is_not_copyable, is_not_movable )

        item_topfolder     ( window& W ) : item_folder( W )
        {
            m_isTopTree = true;
        }
    };


    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    inline
    void DrawSplitter(bool split_vertically, float thickness, float* size0, float max_size )
    {
        ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1,1,1,0.1f));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0,0,0,0));
        ImGui::Button("##Splitter", ImVec2(!split_vertically ? thickness : -1.0f, split_vertically ? thickness : -1.0f));
        ImGui::PopStyleColor(2);

        if ( ImGui::IsItemActive() )
        {
            const float mouse_delta = split_vertically ? ImGui::GetIO().MouseDelta.y : ImGui::GetIO().MouseDelta.x;

            // Apply resize
            *size0 -= mouse_delta;
            if( *size0 < 50 ) *size0 = 50;
            if( *size0 > (max_size - 50) ) *size0 = (max_size - 50);
        }
    }

    //-------------------------------------------------------------------------------------------
    inline
    void window::onUpdate( void )
    {
        for( auto& Entry : m_TreeNodes )
        {
            if( Entry->onUpdate() )
            {
                if( !Entry->m_bTreeNode )
                    onNotifyChange( Entry );
            }
        }
    }

    //-------------------------------------------------------------------------------------------
    inline
    void window::Update( void )
    {
        onUpdate();
    }

    //-------------------------------------------------------------------------------------------
    inline
    void window::RenderBackground( xcolor BkColor, bool bAdvance )
    {
        ImVec2 pos = ImGui::GetCursorScreenPos();
        auto wrap_width = pos.x + ImGui::GetContentRegionAvailWidth();
      //  pos.x = 0;

        if( m_nRows&1)
        {
            ImGui::GetWindowDrawList()->AddRectFilled( pos, ImVec2(  pos.x + wrap_width, pos.y + ImGui::GetTextLineHeight()), BkColor.m_Value );
        }
        else
        {
            f32 H, S, V;
            BkColor.getHSV( H, S, V );
            xcolor DarkBk;
            DarkBk.setupFromHSV( H,S,V*0.8f);
            DarkBk.m_A = BkColor.m_A;
            ImGui::GetWindowDrawList()->AddRectFilled( pos, ImVec2(  pos.x + wrap_width, pos.y + ImGui::GetTextLineHeight()), DarkBk.m_Value );
        }
        if(bAdvance) m_nRows++;
    }

    //-------------------------------------------------------------------------------------------
    inline
    void window::ShowHelpMarker( const char* desc )
    {
        ImGui::TextDisabled("(?)");
        if (ImGui::IsItemHovered())             
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(450.0f);
            ImGui::TextUnformatted(desc);
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    //-------------------------------------------------------------------------------------------
    inline
    void window::UpdateSelected ( base_item* pSelected )
    {
        if (ImGui::IsItemHovered()) 
        {
            m_pSelectedItem = pSelected;
        }
    }

    //-------------------------------------------------------------------------------------------
    inline
    void window::onPrintHelp( void )
    {
       // ShowHelpMarker( m_Help );
       // ImGui::SameLine(); 
        if(m_pSelectedItem && m_pSelectedItem->m_Help ) ImGui::TextWrapped( m_pSelectedItem->m_Help );
        else                                            ImGui::TextWrapped( "Help..." );
    }


    //-------------------------------------------------------------------------------------------
    inline
    void window::onRender( void )
    {
        m_nRows = 0;
        m_pSelectedItem = nullptr;

/*
        //
        // Create the window
        //
        ImGui::SetNextWindowSize( ImVec2(430,450), ImGuiSetCond_FirstUseEver );
       
        bool bRender = false;
        if( m_bFloatWindow )
        {
            bRender = ImGui::Begin( m_WindowName, &m_isOpen, ImGuiWindowFlags_ShowBorders );
        }
        else
        {
            bRender = ImGui::BeginDock( m_WindowName, &m_isOpen );
        }
        
        //
        // Render the actual window
        //
        if( bRender )
 */
        {
            auto WindSize = ImGui::GetWindowSize();

             static float size0 =  100;

            ImGui::BeginChild("Sub1", ImVec2(ImGui::GetWindowContentRegionWidth(), ImGui::GetWindowHeight() - size0 ) );
            {
                ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));
                ImGui::Columns(2);
                ImGui::Separator();
                {
                    //
                    // Render items
                    //
                    for( auto& Entry : m_TreeNodes )
                    {
                        // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
                      //  ImGui::AlignFirstTextHeightToWidgets();  
                        Entry->onRender();
                    }
                }
                ImGui::PopStyleVar();
            }
            ImGui::EndChild();

            //
            // Display the help
            //
            ImGui::Columns(1);
            DrawSplitter( true, 3, &size0, ImGui::GetWindowHeight() );
            ImGui::BeginChild("Sub2");
            onPrintHelp();
            ImGui::EndChild();
        }
        
/*
        //
        // End the window
        //
        if( m_bFloatWindow )
        {
            ImGui::End();
        }
        else
        {
            ImGui::EndDock();
        }
        */
    }
}