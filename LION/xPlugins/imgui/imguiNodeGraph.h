//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include <math.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <map>

static inline ImVec2 operator+(const ImVec2& lhs, const ImVec2& rhs)    { return ImVec2(lhs.x+rhs.x, lhs.y+rhs.y); }
static inline ImVec2 operator-(const ImVec2& lhs, const ImVec2& rhs)    { return ImVec2(lhs.x-rhs.x, lhs.y-rhs.y); }
static inline ImVec2 operator*(const ImVec2& lhs, const float x)        { return ImVec2(lhs.x*x, lhs.y*x); }
static inline ImVec2 operator/(const ImVec2& lhs, const float x)        { return ImVec2(lhs.x/x, lhs.y/x); }
static inline ImVec2 operator/(const ImVec2& lhs, const ImVec2& rhs)    { return ImVec2(lhs.x/rhs.x, lhs.y/rhs.y); }

namespace imgui_node_graph
{
    class node 
    {
        x_object_type( node, rtti_start );

    public:

        using guid = xguid<node>;

        struct connection_pod
        {
            using guid = xguid<connection_pod>;

            struct type 
            {
                using guid = xguid<type>;
            };

            guid getGuid( void ) const { return m_Guid; }

            guid                    m_Guid;
            xstring                 m_Name;
            type::guid              m_gType;
            node&                   m_Node;
            ImVec2                  m_Pos;
            xcolor                  m_Color { 255, 0, 255, 255 };
            xstring                 m_Hint;
            bool                    m_bIsInput;
            u64                     m_UserData;                     // Data that can be used for the client application for whatever it feels like
            bool                    m_CloseButton;
           
            inline connection_pod( node& Node ) : m_Node { Node }{}
        };

        struct connection_hook
        {
            node::guid                  m_gNode;
            node::connection_pod::guid  m_gPod;
        };

        const auto& getName( void ) const { return m_Name; }
        void setPos( const ImVec2& Pos )  { m_Pos = Pos; }
        auto getPos( void ) const { return m_Pos; }
        node( guid Guid, xstring Name, ImVec2 Pos, bool bCloseWindow )
        {
            m_Guid          = Guid;
            m_Name          = Name;
            m_Pos           = Pos;
            m_bCloseButton  = bCloseWindow;
        }

        void EndSetup( void )
        {
            m_Size.y = x_Max( 120.0f, (m_ConnectionPods.getCount<int>() + 2) * 16.0f );
            int x=0;
            for ( auto& con : m_ConnectionPods )
            {
                x = x_Max( x, con.m_Name.getLength().m_Value );
            }

            m_Size.x = 150.0f;//x_Min( 180, 16*4 + x * 8.0f );
        }

        guid getGuid( void ) const { return m_Guid; }

        auto& getPods(void) { return m_ConnectionPods; }

        connection_pod& getPod( int Index )
        {
            x_assert( Index < m_ConnectionPods.getCount() );
            return m_ConnectionPods[Index];
        }

        connection_pod& getPod( connection_pod::guid gPod ) 
        {
            for( auto& Pod : m_ConnectionPods )
            {
                if( Pod.m_Guid == gPod )
                {
                    return Pod;
                }
            }
         
            x_assume( false );
        }
        const connection_pod& getPod( connection_pod::guid gPod ) const
        {
            for( const auto& Pod : m_ConnectionPods )
            {
                if( Pod.m_Guid == gPod )
                {
                    return Pod;
                }
            }
         
            x_assume( false );
        }

        auto getSize( void ) const { return m_Size; }

        void setSelected( bool isOn ) { m_bSelected = isOn; }

        connection_pod& AddInputPod( connection_pod::guid Guid, const xstring& Name, connection_pod::type::guid Type, xcolor Color, bool CloseButton, xstring Hint = {} )
        {
            auto& Pod = m_ConnectionPods.append( *this );
            Pod.m_bIsInput = true;
            Pod.m_gType    = Type;
            Pod.m_Guid     = Guid;
            Pod.m_Name.Copy( Name );
            Pod.m_Color    = Color;
            Pod.m_Hint     = Hint;
            Pod.m_CloseButton = CloseButton;

            return Pod;
        }

        connection_pod& AddOuputPod( connection_pod::guid Guid, const xstring& Name, connection_pod::type::guid Type, xcolor Color, bool CloseButton, xstring Hint = {} )
        {
            auto& Pod = m_ConnectionPods.append( *this );
            Pod.m_bIsInput = false;
            Pod.m_gType    = Type;
            Pod.m_Guid     = Guid;
            Pod.m_Name.Copy( Name );
            Pod.m_Color    = Color;
            Pod.m_Hint     = Hint;
            Pod.m_CloseButton = CloseButton;

            return Pod;
        }

        virtual ~node( void ) {}

    protected:

        x_constexprvar int      TEXT_OFFSET         = 16+5;
        x_constexprvar float    NODE_SLOT_RADIUS    = 16.0f/2.0f;

        //-------------------------------------------------------------------------------------------------------------------

        bool isConnectorHovered( connection_pod& Pod, ImVec2 offset)
        {
            ImVec2 mousePos = ImGui::GetIO().MousePos;
            ImVec2 conPos   = offset +  Pod.m_Pos;

            float xd = mousePos.x - conPos.x;
            float yd = mousePos.y - conPos.y;

            return ((xd * xd) + (yd *yd)) < (NODE_SLOT_RADIUS * NODE_SLOT_RADIUS ); 
        }

        //-------------------------------------------------------------------------------------------------------------------
        virtual void onRenderConnection( connection_pod& Pod )
        {
            // ALign right for outputs
            if( Pod.m_bIsInput == false )
            {
                const auto Size = ImGui::CalcTextSize( Pod.m_Name );
                ImGui::SetCursorPos( ImGui::GetCursorPos() - ImVec2( Size.x, 0 ) );
            }

            // Print text
            ImGui::Text(xstring::Make( Pod.m_Name ) );
        }
        
        //-------------------------------------------------------------------------------------------------------------------
        struct onrender_ret
        {
            connection_pod*     m_pConnectionPod      { nullptr };
            bool                m_bDraggingWindow     { false   };
            bool                m_bSelectedWindow     { false   };
            bool                m_bCloseWindow        { true    };
        };

        virtual onrender_ret onRender( ImDrawList& drawList, ImVec2 RelativePos, connection_pod::type::guid gType, float Scale )
        {
            onrender_ret Ret;
            const bool bDraggingLink = !!gType.m_Value;

            //
            // Render selected box
            //
            if( m_bSelected )
            {
                x_constexprvar float Thickness = 4.0f;
                const auto NodePos  = (RelativePos + m_Pos - ImVec2(Thickness,Thickness) ) * Scale;
                const auto NodeSize = ( (m_bCollapse ? ImVec2( m_Size.x, 16 ) : m_Size) + ImVec2(Thickness,Thickness)*2) * Scale;

                ImGui::SetCursorPos( NodePos );
                ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, ImColor(255,255,255,240));
                ImGui::BeginChild( static_cast<ImGuiID>(xuptr(this)+10), NodeSize, true, ImGuiWindowFlags_NoScrollbar );
                ImGui::EndChild();
                ImGui::PopStyleColor();
            }

            //
            // Begin the child window
            //
            ImGui::SetCursorPos( (RelativePos + m_Pos) * Scale);
            ImGui::PushStyleColor( ImGuiCol_ChildWindowBg,  ImColor(40,40,40,240));
            ImGui::PushStyleColor( ImGuiCol_Header,         ImColor( m_Color.m_Value ) );
            ImGui::PushStyleColor( ImGuiCol_HeaderHovered,  ImColor( m_Color.MultiplyWithHSV( xvector3( 1,1,0.8f ) ).m_Value ) );
            if( m_bCollapse )  ImGui::BeginChild( static_cast<ImGuiID>(xuptr(this)), ImVec2( m_Size.x, 16 ) * Scale, true, ImGuiWindowFlags_NoScrollbar );
            else               ImGui::BeginChild( static_cast<ImGuiID>(xuptr(this)), m_Size * Scale, true, ImGuiWindowFlags_NoScrollbar );
            ImGui::SetWindowFontScale(Scale);
            const auto WinPos = ImGui::GetWindowPos();
            
            //
            // Create the title bar
            //
            if( m_bCloseButton )
            {
                m_bCollapse = !ImGui::CollapsingHeader
                ( 
                    m_Name
                    , &Ret.m_bCloseWindow
                    , (m_bCollapse?0:ImGuiTreeNodeFlags_DefaultOpen)  
                        | ImGuiTreeNodeFlags_OpenOnArrow                  
                        | ImGuiTreeNodeFlags_Framed 
                );

                Ret.m_bCloseWindow = !Ret.m_bCloseWindow;
            }
            else
            {
                m_bCollapse = !ImGui::CollapsingHeader
                ( 
                    m_Name
                    , (m_bCollapse ? 0 : ImGuiTreeNodeFlags_DefaultOpen)  
                        | ImGuiTreeNodeFlags_OpenOnArrow                  
                        | ImGuiTreeNodeFlags_Framed 
                );

                Ret.m_bCloseWindow = false;
            }

            //
            // Render the window
            //
            if( m_bCollapse )
            {
                auto  InPos  = ImVec2(0,        -4)*Scale;
                auto  OutPos = ImVec2(m_Size.x, -4)*Scale;
                for( auto& con : m_ConnectionPods )
                {
                    if( con.m_bIsInput )
                    {
                        // Update the connection position
                        con.m_Pos = InPos;
                        InPos.y += 2 * Scale;
                    }
                    else
                    {
                        // Update the connection position
                        con.m_Pos = OutPos;
                        OutPos.y += 2 * Scale;
                    }
                }
            }
            else
            {
                //
                // Draw input/ouputs
                //
                for ( auto& con : m_ConnectionPods )
                {
                    const auto  Pos             = ImGui::GetCursorPos();
                    bool        bConnecting     = false;

                    if( con.m_bIsInput )
                    {
                        // Update the connection position
                        con.m_Pos = Pos;

                        // Render the pod button
                        if( con.m_gType == gType || bDraggingLink == false )
                        {
                            int e = 1;

                            bConnecting = isConnectorHovered(con,WinPos+ ImVec2(16/2,16/2)*Scale) ;

                            ImGui::PushStyleColor(ImGuiCol_FrameBg, ImColor( con.m_Color.MultiplyWithHSV( xvector3(1,1,0.7f) ).m_Value ));
                            ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, ImColor( con.m_Color.m_Value ));
                            ImGui::RadioButton("", &e, bConnecting ); ImGui::SameLine();
                            ImGui::PopStyleColor(2);

                            if( bConnecting && con.m_Hint.isValid() )
                            {
                                ImGui::BeginTooltip();
                                ImGui::TextUnformatted( con.m_Hint );
                                ImGui::EndTooltip();
                            }
                        }
                    
                        // Prepare the position for the user gizmos
                        ImGui::SetCursorPos(ImVec2(Pos.x+TEXT_OFFSET*Scale, Pos.y) );
                    }
                    else
                    {
                        // Update the connection position
                        con.m_Pos = ImVec2(m_Size.x* Scale, Pos.y);

                        // Render the pod button
                        ImGui::SetCursorPos( con.m_Pos - ImVec2(16,0) * Scale );
                    
                        if( con.m_gType == gType || bDraggingLink == false )
                        {
                            int e = 1;

                            bConnecting = isConnectorHovered(con,WinPos+ ImVec2(-16/2,16/2)*Scale);
                            ImGui::PushStyleColor(ImGuiCol_FrameBg, ImColor( con.m_Color.MultiplyWithHSV( xvector3(1,1,0.7f) ).m_Value ));
                            ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, ImColor( con.m_Color.m_Value ));
                            ImGui::RadioButton("", &e, bConnecting); ImGui::SameLine();
                            ImGui::PopStyleColor(2);

                            if( bConnecting && con.m_Hint.isValid() )
                            {
                                ImGui::BeginTooltip();
                                ImGui::TextUnformatted( con.m_Hint );
                                ImGui::EndTooltip();
                            }
                        }

                        // Prepare the position for the user gizmos
                        ImGui::SetCursorPos(ImVec2((m_Size.x-(TEXT_OFFSET))*Scale, Pos.y) );
                    }

                    if( bConnecting )//ImGui::IsItemHovered() )//|| bConnecting )//bConnecting || (ImGui::IsMouseDown(0) && ImGui::IsItemHovered()) )
                    {
                        Ret.m_pConnectionPod = &con;
                    }

                    // Let the user render the gizmo however he wants
                    onRenderConnection( con );
                }

                //
                // Create Dragging area and detect if dragging
                //
                ImGui::SetCursorPos( ImVec2(0, 16)*Scale );
                ImGui::InvisibleButton( "DragNodeWindow", (m_Size-ImVec2(0, 16))*Scale );
                Ret.m_bSelectedWindow |= (ImGui::IsItemActive() && ImGui::IsMouseDown( 0 ) && !bDraggingLink);
            }

            // Allow to drag from the title bar as well
            ImGui::SetCursorPos( ImVec2(16, 0)*Scale );
            ImGui::InvisibleButton( "DragNodeWindow", (ImVec2(m_Size.x-16,16))*Scale );
            Ret.m_bSelectedWindow |= ( ImGui::IsItemActive() && ImGui::IsMouseDown( 0 ) && !bDraggingLink );

            //
            // Dragging window
            //
            if ( Ret.m_bSelectedWindow && ImGui::IsMouseDragging(0) )
            {
                Ret.m_bSelectedWindow = false;
                Ret.m_bDraggingWindow = true;
               // m_Pos = m_Pos + ImGui::GetIO().MouseDelta / Scale;
            }

            //
            // Terminate all
            // 
            ImGui::EndChild();
            ImGui::PopStyleColor();
            ImGui::PopStyleColor();
            ImGui::PopStyleColor();

            //X_LOG("%d",Ret.m_pConnectionPod!=nullptr);
/*
            if( Ret.m_pConnectionPod )
            {
                static int a=0;
            }
  */
            return Ret;
        }

    protected:

        guid                        m_Guid                  {};
        xstring                     m_Name                  {};
        xvector<connection_pod>     m_ConnectionPods        {};
        ImVec2                      m_Pos                   {};
        ImVec2                      m_Size                  { 120, 120 };
        xcolor                      m_Color                 { 128, 128, 128, 255 };
        bool                        m_bCollapse             { false };
        bool                        m_bSelected             { false };
        bool                        m_bCloseButton          { true  };

        friend class base;
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    struct link
    {
        x_object_type( link, rtti_start );

        using guid = xguid<link>;

        struct drag
        {
            node::connection_pod*   m_pConnection   { nullptr };
            link::guid              m_LinkGuid;
            node::guid              m_gNode;
        };

        guid getGuid( void ) const { return m_Guid; }

        guid                                m_Guid;
        xarray<node::connection_hook,2>     m_ConnectionHooks;        // 0 -- Input, 1 -- Output
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    class base
    {
        x_object_type( base, rtti_start );

    public:

        void Render( void )
        {
            onRender();
        }

        void AddNode( xndptr_s<node>& Entry )
        {
            const auto Guid = Entry->getGuid();
            auto& E = *Entry; 
            m_NodeDB[ Guid ].move( Entry );
            onAddNode( E );
        }

    protected:

        enum class state
        {
            NORMAL,
            DRAGGING_SELECTION_BOX,
            DRAGGING_LINK,
            MOVING_BOXES,
            TRANSFORMING_VIEW,
            SCALING_VIEW
        };
    
    protected:

        void setReadOnly(bool bReadOnly) { m_bReadOnly = bReadOnly; }
        bool isReadOnly( void ) const { return m_bReadOnly; }

        void Clear( void )
        {
            m_State                         = state::NORMAL;
            m_RelativeCenter                = ImVec2{0,0};      
            m_CurrentPosition               = ImVec2{0,0};

            m_NodeDB.clear();     
            m_LinkDB.clear();     

            m_Scale                         = 1.0f;
            m_Selected.DeleteAllEntries();
        }

        void AddLink( xndptr_s<link>&& Entry )
        {
            const auto Guid = Entry->getGuid();
            m_LinkDB[ Guid ].move( Entry );
        }

        void getLinksConnectingFromPod( xvector<link::guid>& LinkList, node::connection_pod::guid gPod )
        {
            for( auto& Link : m_LinkDB )
            {
                if( Link.second->m_ConnectionHooks[1].m_gPod != gPod && Link.second->m_ConnectionHooks[0].m_gPod != gPod )
                    continue;
                LinkList.append( Link.second->getGuid() );
            }
        }

        void BezierCurve( ImDrawList* pDrawList, ImVec2 p1, ImVec2 p2, ImColor Color, float Scale )
        {
            // Bezier control point of the links
            static const ImVec2 link_cp(80,0);
            static const int    SegmentSteps = 24;
      
            const auto          cp1 = p1 + link_cp * Scale;
            const auto          cp2 = p2 - link_cp * Scale;

            // ImColor(200,200,100)
            pDrawList->AddBezierCurve(p1, cp1, cp2, p2, Color, 3.0f*Scale, SegmentSteps );
        }

        inline static float ImVec2Dot(const ImVec2& S1,const ImVec2& S2) {return (S1.x*S2.x+S1.y*S2.y);}


        // Based on: http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
        inline static float GetSquaredDistancePointSegment( const ImVec2& P, const ImVec2& S1, const ImVec2& S2) 
        {
          const float l2 = (S1.x-S2.x)*(S1.x-S2.x)+(S1.y-S2.y)*(S1.y-S2.y);
          if (l2 < 0.0000001f) return (P.x-S2.x)*(P.x-S2.x)+(P.y-S2.y)*(P.y-S2.y);   // S1 == S2 case
          
          ImVec2 T(S2 - S1);
          const float tf = ImVec2Dot(P - S1, T)/l2;
          const float minTf = 1.f < tf ? 1.f : tf;
          const float t = 0.f > minTf ? 0.f : minTf;
          T.x = S1.x + T.x * t;  // T = Projection on the segment
          T.y = S1.y + T.y * t;  // T = Projection on the segment
          return (P.x-T.x)*(P.x-T.x)+(P.y-T.y)*(P.y-T.y);
        }

        inline static float GetSquaredDistanceToBezierCurve(const ImVec2& point,const ImVec2& p1,const ImVec2& p2,const ImVec2& p3,const ImVec2& p4)   
        {
            x_constexprvar int                          num_segments    = 2;   // Num Sampling Points In between p1 and p4
            const static xarray<ImVec4,num_segments>    weights         = []
            {
                xarray<ImVec4,num_segments> weights;

                // This are needed for computing distances: cannot be zero
                x_assert(num_segments>0);    
                for (int i = 1; i <= num_segments; i++) 
                {
                    float t = (float)i/(float)(num_segments+1);
                    float u = 1.0f - t;
                    weights[i-1].x=u*u*u;
                    weights[i-1].y=3*u*u*t;
                    weights[i-1].z=3*u*t*t;
                    weights[i-1].w=t*t*t;
                }

                return weights;
            }();

            float minSquaredDistance = FLT_MAX, tmp;   // FLT_MAX is probably in <limits.h>
            ImVec2 L = p1,tp;
            for (int i = 0; i < num_segments; i++)  
            {
                const ImVec4& W=weights[i];
                tp.x = W.x*p1.x + W.y*p2.x + W.z*p3.x + W.w*p4.x;
                tp.y = W.x*p1.y + W.y*p2.y + W.z*p3.y + W.w*p4.y;

                tmp = GetSquaredDistancePointSegment(point,L,tp);
                if (minSquaredDistance>tmp) minSquaredDistance=tmp;
                L=tp;
            }

            tp = p4;
            tmp = GetSquaredDistancePointSegment(point,L,tp);
            if (minSquaredDistance>tmp) minSquaredDistance=tmp;

            return minSquaredDistance;
        }

        virtual void onDeleteLink( const link& Link ) {}
        virtual void onAddNode( node& Node ){ SingleSelect( Node.getGuid() ); }

        void DeleteLink( link::guid gLink )
        {
            auto    LinkPair    = m_LinkDB.find( gLink );
            auto&   Link        = *LinkPair->second;

            onDeleteLink( Link );

            m_LinkDB.erase( Link.getGuid() );
        }

        void renderLinks( ImDrawList& DrawList, ImVec2 offset, float Scale )
        {
            const auto      WinPos              = ImGui::GetWindowPos();
            link::guid      gDeleteLink         = link::guid{ nullptr };
            int             ButtonIndex = 0;
            for( const auto& pNode : m_LinkDB )
            {
                const auto& Link = *pNode.second;

                ImVec2 Pos[2];
                bool   AddCloseButton[2]{false,false};
                ImColor Color;
                for( int i = 0; i < 2; i++ ) 
                {
                    auto& Hook = Link.m_ConnectionHooks[i];
                    auto  NodeIter = m_NodeDB.find( Hook.m_gNode );
                    x_assert( NodeIter != m_NodeDB.end() );
                    auto& Node = *NodeIter->second;
                    for( auto& Pod : Node.m_ConnectionPods )
                    {
                        if( Pod.m_Guid == Hook.m_gPod )
                        {
                            Pos[i] = offset + Pod.m_Pos + (Node.m_Pos + ImVec2(0,16/2)) * Scale;
                            AddCloseButton[i] = Pod.m_CloseButton;
                            Color = ImColor( Pod.m_Color.m_Value ); 
                        }
                    }
                }
                
                BezierCurve
                (   
                    &DrawList
                    , Pos[1] + (AddCloseButton[1] ? ImVec2(16,0) * Scale : ImVec2(0,0))
                    , Pos[0] - (AddCloseButton[0] ? ImVec2(16,0) * Scale : ImVec2(0,0))
                    , Color
                    , Scale 
                ); 
                
                if( AddCloseButton[1] )
                {
                    ImGui::SetCursorPos( Pos[1] - WinPos - ImVec2(0,16/2) * Scale );
                    ImGui::PushID(ButtonIndex++);
                    if( ImGui::Button( "x", ImVec2(16,16) * Scale ) && m_bReadOnly == false )
                    {
                        gDeleteLink = Link.getGuid();
                    }
                    ImGui::PopID();
                }
                
                if( AddCloseButton[0] )
                {
                    ImGui::SetCursorPos( Pos[0] - WinPos - ImVec2(16,16/2) * Scale );
                    ImGui::PushID(ButtonIndex++);
                    if( ImGui::Button( "x", ImVec2(16,16) * Scale ) && m_bReadOnly == false )
                    {
                        gDeleteLink = Link.getGuid();
                    }
                    ImGui::PopID();
                }
            }

            // Delete link if we have to
            if( gDeleteLink.m_Value ) 
            {
                DeleteLink( gDeleteLink );
            }
        }

        void renderGrid( ImDrawList& DrawList, float Scale )
        {
            const auto      WinPos              = ImGui::GetWindowPos();
            const auto      canvasSize          = ImGui::GetWindowSize();
            const ImU32&    GRID_COLOR          = ImColor( 0,0,0,16 );
            const float&    GRID_SZ             = Scale * 100.0f;
            const float     grid_Line_width     = Scale * 1;
                
            //
            // Major 
            //                
            for (float x = fmodf(m_CurrentPosition.x * m_Scale,GRID_SZ); x < canvasSize.x; x += GRID_SZ)
                DrawList.AddLine(ImVec2(x,0.0f)+WinPos, ImVec2(x,canvasSize.y)+WinPos, GRID_COLOR,grid_Line_width);
                
            for (float y = fmodf(m_CurrentPosition.y* m_Scale,GRID_SZ); y < canvasSize.y; y += GRID_SZ)
                DrawList.AddLine(ImVec2(0.0f,y)+WinPos, ImVec2(canvasSize.x,y)+WinPos, GRID_COLOR,grid_Line_width);

            //
            // Minor 
            //                
            for (float x = fmodf(m_CurrentPosition.x* m_Scale,GRID_SZ/5); x < canvasSize.x; x += GRID_SZ/5)
                DrawList.AddLine(ImVec2(x,0.0f)+WinPos, ImVec2(x,canvasSize.y)+WinPos, GRID_COLOR,grid_Line_width);
                
            for (float y = fmodf(m_CurrentPosition.y* m_Scale,GRID_SZ/5); y < canvasSize.y; y += GRID_SZ/5)
                DrawList.AddLine(ImVec2(0.0f,y)+WinPos, ImVec2(canvasSize.x,y)+WinPos, GRID_COLOR,grid_Line_width);
        }

        float getScale( void )
        {
            return m_Scale; //ImGui::GetIO().FontGlobalScale;
        }

        virtual void onDeleteComponent( const node& Node )
        {
        }

        void DeleteNode( node::guid gNode )
        {
            node* pNode = m_NodeDB.find( gNode )->second;

            //
            // Delete associated links 
            //
            int                   iDelete = 0;
            xvector<link::guid>   DeleteLinks;
            for( auto& LinkPair : m_LinkDB )
            {
                auto& Link = *LinkPair.second;

                if( Link.m_ConnectionHooks[0].m_gNode == gNode ||
                    Link.m_ConnectionHooks[1].m_gNode == gNode ) 
                {
                    DeleteLinks.append() = Link.m_Guid; 
                }
            }

            // Do the actual deletion
            for( auto& L : DeleteLinks )
            {
                DeleteLink( L );
                //m_LinkDB.erase( L );
            }

            //
            // Delete node
            //
            onDeleteComponent( *pNode );
            m_NodeDB.erase( gNode );
        }

        //-------------------------------------------------------------------------------------------------------------------

        void DeleteNodeWithDependencies( node::guid gNode, xvector<node::guid>* pNodeList = nullptr )
        {
            xvector<node::guid>* pNewpNodeList = pNodeList;

            if( pNewpNodeList == nullptr )
            {
                if( m_NodeDB.find( gNode ) == m_NodeDB.end() ) return;
                pNewpNodeList = x_new( xvector<node::guid> );
            }

            // Add node to the going to be deleted list
            pNewpNodeList->append( gNode );

            // Collect all the links
            // This is the recursive part
            for( auto& LinkPair : m_LinkDB )
            {
                const auto& Link    = *LinkPair.second;
                const auto  iOther  = Link.m_ConnectionHooks[0].m_gNode == gNode ? 1 : Link.m_ConnectionHooks[1].m_gNode == gNode ? 0 : -1;
                if( iOther == -1 ) continue;

                // Do we have this node already?
                const bool bAddNode = [&]
                {
                    for( const auto gnode : *pNewpNodeList )
                    {
                        if( Link.m_ConnectionHooks[iOther].m_gNode != gnode )
                            continue;
                        return false;
                    }
                    return true;
                }();

                if( bAddNode ) DeleteNodeWithDependencies( Link.m_ConnectionHooks[iOther].m_gNode, pNewpNodeList );
            }

            // Delete Nodes and Links
            // The top node is known by its null
            if( pNodeList == nullptr )
            {
                for( const auto gnode : *pNewpNodeList )
                {
                    DeleteNode( gnode );
                }

                // we memory
                x_delete( pNewpNodeList );
            }
        }


        void HandleInput( void )
        {
            if( !ImGui::IsWindowHovered() || ImGui::IsAnyItemActive() )
                return;

            const auto OldScale = m_Scale;
            const auto MousePos = ImGui::GetIO().MousePos;

            // Deal with mouse wheel scaling... no state change needed for this
            if( ImGui::GetIO().MouseWheel )
            {
                m_Scale += m_Scale*0.2f * ImGui::GetIO().MouseWheel;
            }

            switch( m_State )
            {
                case state::NORMAL:
                {
                    if( ImGui::IsWindowHovered() && !ImGui::IsAnyItemActive() )
                    {
                        if( ImGui::IsMouseDragging( 1, 0.0f ) )
                        {
                            m_State = state::TRANSFORMING_VIEW;
                            if( ImGui::IsMouseDown( 0 ) )
                            {
                                m_Scale += m_Scale*0.01f * ImGui::GetIO().MouseDelta.y;
                            }
                            else
                            {
                                // Translate the world
                                m_CurrentPosition = m_CurrentPosition + ImGui::GetIO().MouseDelta / m_Scale;
                            }
                        }
                        else if( ImGui::IsMouseDragging( 0, 0.0f ) )
                        {
                            m_State = state::DRAGGING_SELECTION_BOX;
                            m_SelectionBox[0] = MousePos;
                            m_SelectionBox[1] = MousePos;
                            m_AdditiveSelection = ( ImGui::GetIO().KeyShift );
                        }
                    }
                    break;
                }
                case state::DRAGGING_SELECTION_BOX:
                {
                    if( ImGui::IsMouseDragging( 0, 0.0f ) )
                    {
                        m_SelectionBox[1] = MousePos;
                    }
                    else
                    {
                        m_State = state::NORMAL;
                    }
                    break;
                }
                case state::TRANSFORMING_VIEW:
                {
                    if( ImGui::IsMouseDragging( 1, 0.0f ) )
                    {
                        if( ImGui::IsMouseDown( 0 ) )
                        {
                            m_Scale += m_Scale*0.01f * ImGui::GetIO().MouseDelta.y;
                        }
                        else
                        {
                            // Translate the world
                            m_CurrentPosition = m_CurrentPosition + ImGui::GetIO().MouseDelta / m_Scale;
                        }
                    }
                    else
                    {
                        m_State = state::NORMAL;
                    }
                    break;
                }
            }

            //
            // Finish dealing with the scale
            //                        
                 
            // Magnet to 1 or enforce the range
            if( x_Abs(m_Scale - 1.0f) < 0.04f ) m_Scale = 1.0f;
            else                                m_Scale = x_Range( m_Scale, 0.2f, 2.0f );

            // Transform the world base on the scale to make it scaling easier for the user
            if( OldScale != m_Scale )
            {
                const auto WinPos             = ImGui::GetWindowPos();
                const auto ScaleCenter        = MousePos - WinPos;
                const auto to_center_old      = ScaleCenter / OldScale;
                const auto to_center_new      = ScaleCenter / m_Scale;
                const auto to_center_offset   = to_center_new - to_center_old;

                m_CurrentPosition = m_CurrentPosition + to_center_offset;
            }

        }

        virtual void onNotifyNewLinkConnection( const link& Link ) {}

        virtual bool onLinkDrop( node::connection_pod& Pod, ImVec2 Pos )
        {
            return false;
        }

        void SingleSelect( node::guid gNode )
        {
            for( auto& NodePair : m_NodeDB )
            {
                auto& Node = *NodePair.second;
                Node.m_bSelected = false;
                if( Node.m_Guid == gNode ) Node.m_bSelected = true;
            }
        }

        virtual void onRender( void )
        {
            /*
            bool bRender;
            if( m_bFloatWindow )
            {
                bRender = ImGui::Begin( m_WindowName, &m_bOpen, ImGuiWindowFlags_ShowBorders );
            }
            else
            {
                bRender = ImGui::BeginDock( m_WindowName, &m_bOpen, ImGuiWindowFlags_ShowBorders );
            }

            if( !bRender )
            {
                if( m_bFloatWindow )
                {
                    ImGui::End();
                }
                else
                {
                    ImGui::EndDock();
                }
                return;
            }
            */

            // Create our child canvas
            //ImGui::Text("Hold middle mouse button to scroll (%.2f,%.2f)", scrolling.x, scrolling.y);
            ImGui::PushStyleVar(ImGuiStyleVar_FramePadding,  ImVec2(1,1));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
            ImGui::PushStyleColor(ImGuiCol_ChildWindowBg,    ImColor(40,40,40,0));

            static ImVec2 WInSize;

            ImGui::BeginChild("scrolling_region", ImVec2(0,0), true, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar );// ImGuiWindowFlags_HorizontalScrollbar);
            ImGui::PushItemWidth(120.0f);
            WInSize = ImGui::GetWindowSize( );

            //
            // Deal with user input 
            //
            HandleInput();

            //
            // Get the window position
            //
            const auto  WinPos          = ImGui::GetWindowPos();
            const auto  RelativePos     = WinPos + m_CurrentPosition * m_Scale + ImVec2( -ImGui::GetScrollX(), -ImGui::GetScrollY() );

            auto&       DrawList        = *ImGui::GetWindowDrawList();
            const auto  Scale           = getScale();

            //
            // Render the grid
            //
            renderGrid( DrawList, Scale );

            //
            // Render Links
            //
            renderLinks( DrawList, RelativePos, Scale );

            //
            // Render Nodes
            //
            DrawList.ChannelsSplit(2);
            xvector<node::guid> DeleteNodes;
            m_Selected.DeleteAllEntries();
            for( auto& NodePair : m_NodeDB )
            {
                auto& Entry = *NodePair.second;
                if(Entry.m_bSelected ) m_Selected.append( Entry.m_Guid );

                //
                // Cull nodes outside the window
                //
                {
                    // Need to be overly conservative in the culling because imgui will try to close headers that are not in the screen
                    static const auto OverConservativeCulling = ImVec2(6,6);
                    const auto NodePos  = (m_CurrentPosition + Entry.m_Pos) * Scale + OverConservativeCulling;
                    const auto NodeSize = Entry.m_Size * Scale                      - (OverConservativeCulling*2);
                    const auto WinPos   = ImVec2(0,0);
                    const auto WinSize  = ImGui::GetWindowSize();

                    if( NodePos.x >= (WinPos.x + WinSize.x) || NodePos.y >= (WinPos.y + WinSize.y) )
                        continue; 

                    if( (NodePos.x + NodeSize.x) <= WinPos.x || (NodePos.y + NodeSize.y) <= WinPos.y )
                        continue; 

                    // Take advantage of the loop and check for the selection state as well
                    if( m_State == state::DRAGGING_SELECTION_BOX )
                    {
                        const auto Pos = ImGui::GetWindowPos();

                        const float BoxMinX = x_Min( m_SelectionBox[0].x, m_SelectionBox[1].x ) - Pos.x - OverConservativeCulling.x;
                        const float BoxMinY = x_Min( m_SelectionBox[0].y, m_SelectionBox[1].y ) - Pos.y - OverConservativeCulling.y;
                        const float BoxMaxX = x_Max( m_SelectionBox[0].x, m_SelectionBox[1].x ) - Pos.x + OverConservativeCulling.x;
                        const float BoxMaxY = x_Max( m_SelectionBox[0].y, m_SelectionBox[1].y ) - Pos.y + OverConservativeCulling.y;

                        if( NodePos.x < BoxMaxX                 && 
                            NodePos.y < BoxMaxY                 &&
                            (NodePos.x + NodeSize.x) > BoxMinX  &&
                            (NodePos.y + NodeSize.y) > BoxMinY )
                        {
                            Entry.m_bSelected = true;
                        }
                        else
                        {
                            if( !m_AdditiveSelection ) Entry.m_bSelected = false;
                        }
                    }
                }

                //
                // Render a node
                //
                auto RenderInfo = Entry.onRender( DrawList, m_CurrentPosition, m_DragLink.m_pConnection ? m_DragLink.m_pConnection->m_gType : node::connection_pod::type::guid{ nullptr }, Scale );
                if(m_bReadOnly == false && RenderInfo.m_bCloseWindow )
                {
                    DeleteNodes.append( Entry.m_Guid );
                    continue;
                }

                //
                // Check for selecting nodes
                //
                if( m_State == state::NORMAL && ( RenderInfo.m_bSelectedWindow || RenderInfo.m_bDraggingWindow ) )
                {
                    m_State             = state::MOVING_BOXES;
                    m_AdditiveSelection = ( ImGui::GetIO().KeyShift );
                                    
                    if( RenderInfo.m_bSelectedWindow && !m_AdditiveSelection ) 
                    {
                        // Unselect everything else
                        for( auto& CheckEntryPair : m_NodeDB )
                        {
                            auto& CheckEntry = *CheckEntryPair.second;
                            if( CheckEntry.m_bSelected ) CheckEntry.m_bSelected = false;
                        }
                    }

                    // Select our node
                    Entry.m_bSelected = true;
                }

                //
                // Render selection box
                //
                if( m_State == state::DRAGGING_SELECTION_BOX )
                {
                    x_constexprvar float Thickness = 3.0f;
                    DrawList.AddRect( m_SelectionBox[0], m_SelectionBox[1], ~0, 0, 0, Thickness * Scale );
                }

                //
                // Handle the dragging links case
                //
                if( m_bReadOnly == false && m_State != state::DRAGGING_LINK && m_State != state::MOVING_BOXES )
                {
                    if( RenderInfo.m_pConnectionPod && m_State != state::DRAGGING_SELECTION_BOX )
                    {
                        // Do we start dragging some link?
                        if( ImGui::IsMouseDown(0) && m_DragLink.m_pConnection == nullptr )
                        {
                            m_DragLink.m_pConnection    = RenderInfo.m_pConnectionPod;
                            m_DragLink.m_gNode          = Entry.m_Guid;
                            m_State                     = state::DRAGGING_LINK;
                        }
                    }
                }
                else if( m_State != state::MOVING_BOXES )
                {
                    //
                    // Tell the user if this is going to be ok or not
                    //
                    if (m_DragLink.m_pConnection == nullptr) {}
                    else if( RenderInfo.m_pConnectionPod )
                    {
                        //
                        // User is trying to see where to connect
                        //
                        if( ImGui::IsMouseDown( 0 ) )
                        {
                            if( m_DragLink.m_pConnection->m_bIsInput != RenderInfo.m_pConnectionPod->m_bIsInput &&
                                m_DragLink.m_pConnection->m_gType == RenderInfo.m_pConnectionPod->m_gType )
                            {
                                ImGui::BeginTooltip();
                                ImGui::TextColored( ImColor( 0,255,0), "  GOOD  " );
                                ImGui::EndTooltip();
                            }
                            else
                            {
                                ImGui::BeginTooltip();
                                ImGui::TextColored( ImColor( 255,200,200), "  BAD Looking for:\n %s  ", (const char*)m_DragLink.m_pConnection->m_Name );
                                ImGui::EndTooltip();
                            }
                        }
                        //
                        // Lets try to connect
                        //
                        else 
                        {
                            // Lets connect the nodes.
                            if(    m_DragLink.m_pConnection             != RenderInfo.m_pConnectionPod                 
                                && m_DragLink.m_pConnection->m_bIsInput != RenderInfo.m_pConnectionPod->m_bIsInput 
                                && m_DragLink.m_pConnection->m_gType    == RenderInfo.m_pConnectionPod->m_gType )
                            {
                                const int       Index = m_DragLink.m_pConnection->m_bIsInput?0:1;
                                xndptr_s<link>  Link;

                                Link.New();
                                Link->m_ConnectionHooks[Index].m_gNode   = m_DragLink.m_pConnection->m_Node.m_Guid;
                                Link->m_ConnectionHooks[Index].m_gPod    = m_DragLink.m_pConnection->m_Guid;
                                Link->m_ConnectionHooks[1-Index].m_gNode = RenderInfo.m_pConnectionPod->m_Node.m_Guid;
                                Link->m_ConnectionHooks[1-Index].m_gPod  = RenderInfo.m_pConnectionPod->m_Guid;

                                // Compute a unique ID for this link base on the other guids
                                Link->m_Guid.m_Value = Link->m_ConnectionHooks[0].m_gNode.m_Value   
                                                       ^ Link->m_ConnectionHooks[0].m_gPod.m_Value    
                                                       ^ Link->m_ConnectionHooks[1].m_gNode.m_Value   
                                                       ^ Link->m_ConnectionHooks[1].m_gPod.m_Value;

                                // Try to find the node first if we can not find it then just add it
                                const auto ExistinLinkPair = m_LinkDB.find( Link->m_Guid );
                                if( ExistinLinkPair == m_LinkDB.end() )
                                {
                                    onNotifyNewLinkConnection( Link );
                                    AddLink( std::move(Link) );
                                }
                                else
                                {
                                    // We already have this link
                                    // We don't do duplicate links
                                }
                            }

                            // back to normal state
                            m_State                     = state::NORMAL;
                            m_DragLink.m_pConnection    = nullptr;
                        }
                    }
                }
            }

            //
            // Handle the case where the user wants us to add a new node by dropping the link in the air
            //
            if( m_State == state::DRAGGING_LINK && m_DragLink.m_pConnection && false == ImGui::IsMouseDown(0) )
            {
                if( onLinkDrop( *m_DragLink.m_pConnection, ImGui::GetIO().MousePos ) )
                {

                }

                // back to normal state
                m_State                     = state::NORMAL;
                m_DragLink.m_pConnection    = nullptr;
            }

            //
            // Handle the rendering of the dragging link
            //
            if( ImGui::IsMouseDown(0) )
            {
                if( m_DragLink.m_pConnection )
                {
                    // Background
                    DrawList.ChannelsSetCurrent(0); 

                    const xarray<ImVec2,2> Pos = [&]
                    {
                        xarray<ImVec2,2> Pos;
                        const int        iInput = m_DragLink.m_pConnection->m_bIsInput?1:0;
                        Pos[iInput]     = RelativePos + m_DragLink.m_pConnection->m_Pos + (m_DragLink.m_pConnection->m_Node.m_Pos + ImVec2(0,16/2)) * Scale;
                        Pos[1-iInput]   = ImGui::GetIO().MousePos;
                        return Pos;
                    }();

                    BezierCurve( &DrawList, Pos[0], Pos[1], ImColor(~0), Scale );
                }
            }
            else
            {
                m_DragLink.m_pConnection    = nullptr;
            }

            //
            // Delete any pending nodes
            //
            if( DeleteNodes.getCount() )
            {
                for( const auto Guid : DeleteNodes )
                {
                    DeleteNode( Guid );
                }
            }

            //
            // Move Nodes
            //
            if( m_State == state::MOVING_BOXES )
            {
                const auto MouseDelta = ImGui::GetIO().MouseDelta / m_Scale;
                for( auto& EntryPair : m_NodeDB )
                {
                    auto& Entry = *EntryPair.second;
                    if( Entry.m_bSelected )
                    {
                        Entry.m_Pos = Entry.m_Pos + MouseDelta;
                    }
                }

                // Reset to normal
                if( !ImGui::IsMouseDown( 0 ) )
                    m_State = state::NORMAL;
            }


            DrawList.ChannelsMerge();

            ImGui::PopItemWidth();
            ImGui::EndChild();
            ImGui::PopStyleColor();
            ImGui::PopStyleVar(2);

            //
            // End the window
            //
            /*
            if( m_bFloatWindow )
            {
                ImGui::End();
            }
            else
            {
                ImGui::EndDock();
            }
            */
        }

   protected:

        state                                                   m_State             { state::NORMAL };
        ImVec2                                                  m_RelativeCenter    {0,0};      // This center is the center of the virtual world. It changes in the renormalization step.
        ImVec2                                                  m_CurrentPosition   {0,0};      // Current position of the virtual space relative to the relative-center
        std::map<node::guid, xndptr_s<node>>                    m_NodeDB            {};
        std::map<link::guid, xndptr_s<link>>                    m_LinkDB            {};
        bool                                                    m_bOpen             { true };
        float                                                   m_Scale             { 1.0f };
        xvector<node::guid>                                     m_Selected          {};

        xarray<ImVec2,2>                                        m_SelectionBox      {};
        bool                                                    m_AdditiveSelection {false};
        bool                                                    m_bFloatWindow      { false };
        link::drag                                              m_DragLink          {};
        xstring                                                 m_WindowName        { X_STR("Node Graph") };
        bool                                                    m_bReadOnly         { false };
    };
}