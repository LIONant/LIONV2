//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component
{
    class entity;

    //------------------------------------------------------------------------------
    // Description:
    //      This class gives the common functionality for a given type of component
    //      It particular their allocation and deletion.
    //      It also provides the mechanism to identify each component type
    //------------------------------------------------------------------------------
    class type_base 
    {
        x_object_type( type_base, is_linear, rtti_start, is_not_copyable, is_not_movable )

    public:

        using guid = extended_guid<type_base,type_base,gb_component::base::t_class_guid.m_Value>;


    public:
        constexpr                                   type_base           ( void ) = delete;
        virtual                                    ~type_base           ( void )                                                            noexcept = default; 
        x_orforceconst                              type_base           ( gb_component_mgr::base& ComponentMgr, gb_game_graph::base& GameMgr, gb_game_graph::sync_point& SyncPoint, int CompactComponentOffset ) noexcept :
                                                                            m_GlobalInterface
                                                                            { 
                                                                                ComponentMgr, 
                                                                                *this, 
                                                                                GameMgr,
                                                                                SyncPoint,
                                                                                CompactComponentOffset
                                                                            } { x_assert_linear( m_Debug_LQ ); }

        x_forceinline   base::global_interface&     getGlobalInterface      ( void )                                                        noexcept { return m_GlobalInterface; }
        virtual         entity&                     CreateEntity            ( void )                                                        noexcept { x_assume(0); return *(entity*)(nullptr); }
        virtual         base&                       CreateComponent         ( entity& Entity, gb_component::narrow_count NarrowCount )      noexcept = 0;
        virtual         void                        msgDestroy              ( base& Component )                                             noexcept = 0;
        virtual         guid                        getGuid                 ( void )                                                const   noexcept = 0;
        virtual         const xconst_str_u          getCategoryName         ( void )                                                const   noexcept = 0;
        virtual         const xstring::const_str    getHelp                 ( void )                                                const   noexcept = 0;
        virtual         base::t_descriptors::def    getComponentDefinition  ( void )                                                const   noexcept = 0;
        virtual         bool                        isEntityType            ( void )                                                const   noexcept = 0;
        x_forceinline   auto&                       getInWorld              ( void )                                                        noexcept { return m_InWorld; }

    protected:

        using global_interface = base::global_interface;
        
    protected:
        
        x_inline        void                    linearAddToWorld        ( gb_component::base& Comp )                                        noexcept;
        x_inline        void                    linearRemoveFromWorld   ( gb_component::base& Comp )                                        noexcept;
        x_inline        void                    qt_Execute              ( void )                                                            noexcept;
        virtual         int&                    getComponentIndex       ( gb_component::base& Comp )                                const   noexcept = 0;

    protected:

        x_debug_linear_quantum          m_Debug_LQ              {};
        xvector<gb_component::base*>    m_InWorld               {};
        global_interface                m_GlobalInterface;

    protected:

        friend class gb_component_mgr::base;
    };

    //------------------------------------------------------------------------------
    // Description:
    //      Specializes the component type into a particular memory scheme for components.
    //      This is the class that it is used as the default to allocate component types.
    //------------------------------------------------------------------------------
    template< typename T_COMP >
    class type_pool : public type_base
    {
        x_object_type( type_pool, is_linear, rtti(type_base), is_not_copyable, is_not_movable )

    public:

        using t_component = T_COMP;

    public:

        constexpr                                       type_pool                   (   gb_component_mgr::base&       ComponentMgr, 
                                                                                        gb_game_graph::base&          GameMgr, 
                                                                                        gb_game_graph::sync_point&    SyncPoint )           noexcept            : type_base( ComponentMgr, GameMgr, SyncPoint, sizeof(t_component) ) { compact_component::PrivateRegisterConstData(GameMgr); }
        virtual                                        ~type_pool                   ( void )                                                noexcept override   = default;
        virtual             guid                        getGuid                     ( void )                                        const   noexcept override   { return t_component::t_descriptors::t_type_guid;                   }
        virtual             const xconst_str_u          getCategoryName             ( void )                                        const   noexcept override   { return t_component::t_descriptors::t_category_string;             }
        virtual             const xstring::const_str    getHelp                     ( void )                                        const   noexcept override   { return t_component::t_descriptors::t_help;                        }
        virtual     base::t_descriptors::def            getComponentDefinition      ( void )                                        const   noexcept override   { return t_component::t_descriptors::t_definition;                  }
        virtual             bool                        isEntityType                ( void )                                        const   noexcept override   { return false; }

    protected:

        struct compact_component final : t_component
        {
            x_constexprvar bool t_has_constdata_v   = false == std::is_same< typename gb_component::base::const_data,   typename T_COMP::const_data>::value;
            x_constexprvar bool t_has_events_v      = false == std::is_same< typename gb_component::base::events,       typename T_COMP::events>::value; 
            x_constexprvar bool t_has_delegates_v   = false == std::is_same< typename gb_component::base::delegates,    typename T_COMP::delegates>::value; 
            x_constexprvar bool t_is_entity_v       = std::is_base_of< gb_component::entity, t_component >::value; 

            struct t_mutable_data final : t_component::mutable_data   {};
            struct delegates      final : t_component::delegates      { delegates   ( void ) noexcept = default; };
            struct events         final : t_component::events         { events      ( void ) noexcept = default; };
            struct const_data     final : t_component::const_data     { const_data  ( void ) noexcept = default; };

            x_object_type( compact_component, is_quantum_lock_free, rtti(t_component), is_not_copyable, is_not_movable )

            x_forceinline               compact_component                ( const typename t_component::base_construct_info& C ) : t_component(C) {}

            x_forceinline static void   Destruction                      ( t_mutable_data* pData )
            {
                (void)x_destruct( pData );
            }

            struct mutable_data 
            { 
                alignas(  t_mutable_data ) xbyte m_Data[sizeof( t_mutable_data)];
                ~mutable_data( void ) noexcept
                {
                    compact_component::Destruction( reinterpret_cast< t_mutable_data*>( &m_Data ) );

                    // Indebug we clear the data
                    X_DEBUG_CMD( x_memset( m_Data, '?', sizeof(t_mutable_data) ) );
                }
            };

            x_forceinline static
            void PrivateRegisterConstData( gb_game_graph::base& Graph )
            {
                if constexpr ( t_has_constdata_v )
                {
                    Graph.m_ConstDataTypeDB.RegisterButRetainOwnership( typename T_COMP::const_data::t_type );
                }
            }

            const xproperty_v2::table& onPropertyTable ( void ) const noexcept override
            {
                //
                // Most components will be very simple so nothing to do at this level so lets keep it simple then
                //
                x_constexprvar bool bSimple = 
                    false == t_is_entity_v
                    && (sizeof(typename delegates::table) / sizeof(typename delegates::definition) == 0)
                    && false == t_has_constdata_v ;

                if constexpr ( bSimple )
                {
                    return t_component::onPropertyTable();
                }

                //
                // Ok it seems that we need more properties then lets start a new block
                //
                static const xproperty_v2::table E( this, this, [&](xproperty_v2::table& E)
                {
                    auto& ParentPropTable = t_component::onPropertyTable(); 
                    E.AddChildTable( ParentPropTable );

                    //
                    // if it has constant data then enumerate it
                    //
                    if constexpr ( t_has_constdata_v )
                    {
                        E.AddScope( ParentPropTable.m_pTable->m_pName, [&](auto& E )
                        {
                            E.AddProperty<u64>      ( X_STR("ConstData"),    GB_PROP_CONSTANT_DATA( m_ConstData ),   X_STR("This is the component constant/read-only data. This data will be use by the component so it must be of the correct type." ) );
                        });
                    }

                    //
                    // Add Delegates if we got any
                    //
                    if constexpr ( t_has_delegates_v )
                    {
                        auto& Table = getDelegateTable();
                        if( Table.getCount() > 0 )
                        {
                            E.AddScope( X_STR("Delegates"), [&]( xproperty_v2::table& E )
                            {
                                for( auto& Entry : Table )
                                {
                                    if( Entry.m_EventType.m_Guid.getClassGuid() == event_type::t_class_guid )
                                    {
                                        auto pFakeDelegate = ((event_fake::delegate<compact_component>*)&((xbyte*)&m_Delegates)[Entry.m_Offset]);

                                        E.AddProperty
                                        ( 
                                            Entry.m_Name 
                                            , pFakeDelegate->m_Guid.m_Value
                                            , xproperty_v2::info::guid
                                                {
                                                    Entry.m_EventType.m_Name
                                                    , Entry.m_EventType.m_Guid.m_Value
                                                }
                                            , Entry.m_Help
                                        );
                                    }
                                    else
                                    {
                                        x_assert( Entry.m_EventType.m_Guid.getClassGuid() == interface_type::t_class_guid );

                                        auto pFakeInterface = ((interface_ref<void>*)&((xbyte*)this)[Entry.m_Offset]);

                                        E.AddProperty
                                        ( 
                                            Entry.m_Name 
                                            , pFakeInterface->m_Guid.m_Value
                                            , xproperty_v2::info::guid
                                                {
                                                    Entry.m_EventType.m_Name
                                                    , Entry.m_EventType.m_Guid.m_Value
                                                }
                                            , Entry.m_Help
                                        );
                                    }
                                }
                            });
                        }
                    }

                    //
                    // Entities deal with saving Components
                    //
                    if constexpr ( t_is_entity_v )
                    {
                        E.AddDynamicList
                        ( 
                            X_STR("Comp")
                            , X_STATIC_LAMBDA_BEGIN ( compact_component& A, void*& pNext ) -> u64
                            {
                                gb_component::base*& pCompNext = reinterpret_cast<gb_component::base*&>(pNext);
                                if( pCompNext == nullptr )  pCompNext = A.m_pNextComp;
                                else                        pCompNext = pCompNext->m_pNextComp;
                                if( pCompNext ) return pCompNext->getNarrowGUID().m_Value;
                                return ~0ul;
                            }
                            X_STATIC_LAMBDA_END
                            , X_STATIC_LAMBDA_BEGIN ( compact_component& A, u64 id, bool isSend ) -> xproperty_v2::base*
                            {
                                for( auto* pNext = A.m_pNextComp; pNext ; pNext = pNext->m_pNextComp )
                                {
                                    if( id == pNext->getNarrowGUID().m_Value ) return pNext;
                                }    

                                return nullptr;
                            }
                            X_STATIC_LAMBDA_END
                            , X_STATIC_LAMBDA_BEGIN ( compact_component& A, int& Count, bool isSend )
                            {
                                if(isSend)
                                {
                                    Count = 0;
                                    for( auto* pNext = A.m_pNextComp; pNext ; pNext = pNext->m_pNextComp ) Count++;
                                }
                            }
                            X_STATIC_LAMBDA_END
                            //, xproperty_v2::flags::MASK_NOT_VISIBLE | xproperty_v2::flags::MASK_FORCE_SAVE 
                        );
                    }
                });

                return E; 
            }

            virtual         void                onResolve               ( void )                            noexcept override
            {
                //
                // Let the parent deal with any resolve issues
                //
                t_parent::onResolve();

                //
                // Resolved delegates
                //
                if constexpr ( t_has_delegates_v )
                {
                    auto& Entity = getEntity();

                    for( auto& Map : getDelegateTable() )
                    {
                        if( Map.m_EventType.m_Guid.getClassGuid() == event_type::t_class_guid )
                        {
                            auto pDelegate = ((event_fake::delegate<compact_component>*)&((xbyte*)&m_Delegates)[Map.m_Offset]);

                            // make sure that it is disconnected
                            if(pDelegate->isConnected()) pDelegate->Disconnect();
                    
                            const auto Guid = pDelegate->m_Guid;

                            // if we don't need to connect to something then we are done
                            if( Guid.m_Value == 0 ) continue;

                            // get the component that has the event
                            auto pComponent = Entity.getComponent( Guid.getNarrowGuid() );
                            if( pComponent == nullptr )
                            {
                                pDelegate->m_Guid = 0;
                                continue;
                            }

                            // get the event
                            auto pEvent = pComponent->getEventByGuid( Guid );
                            x_assert(pEvent);

                            // Connect with event                    
                            pDelegate->Connect( *this, *pEvent );
                        }
                        else
                        {
                            auto pInterfaceRef = ((interface_ref<void>*)&((xbyte*)this)[Map.m_Offset]);
                            const auto Guid = pInterfaceRef->m_Guid;

                            // if we don't need to connect to something then we are done
                            if( Guid.m_Value == 0 ) continue;

                            // get the component that has the interface
                            auto pComponent = Entity.getComponent( Guid.getNarrowGuid() );
                            if(pComponent == nullptr ) 
                            {
                                pInterfaceRef->m_Guid = 0;
                                continue;
                            }

                            // Connect the reference with the actual interface
                            pInterfaceRef->m_pValue = pComponent->getInterfaceByGuid( Guid );
                        }
                    }
                }
            }

            virtual       gb_component::base::mutable_data&         getMutableData(const u32 I) noexcept override 
            { 
                x_assume( I == 0 || I == 1 );
                return *reinterpret_cast<gb_component::base::mutable_data*>( &m_MutableData[I].m_Data[0] ); 
            }

            virtual gb_component::delegates& getDelegates ( void ) noexcept override
            {
                return m_Delegates; 
            }

            virtual const xbuffer_view<const typename delegates::definition>& getDelegateTable( void ) const noexcept override
            {
                x_constexprvar auto TableSize = sizeof(typename delegates::table);
                x_constexprvar auto Count     = TableSize / sizeof(typename delegates::definition);

                static const typename delegates::table                          DelegateTable;
                static const xbuffer_view<const typename delegates::definition> DelegateTableView
                { 
                    (const typename delegates::definition*)&DelegateTable
                    , Count 
                };
                return DelegateTableView;
            }

            virtual gb_component::events& getEvents ( void ) noexcept override
            {
                return m_Events; 
            }

            virtual const xbuffer_view<const typename events::definition>& getEventTable ( void ) const noexcept
            {
                static const typename events::table                             EventTable;
                static const xbuffer_view<const typename events::definition>    EventTableView
                { 
                    (const typename events::definition*)&EventTable
                    , sizeof(typename events::table) / sizeof(typename events::definition) 
                };
                return EventTableView;
            }

            virtual const gb_component::base::const_data& internal_ConstData( void ) const noexcept override
            {
                return static_cast<const const_data&>(m_ConstData);
            }

            virtual bool internal_ConstDataValid ( void ) const noexcept
            {
                return m_ConstData.isValid();
            }


            mutable_data                                            m_MutableData[2];       // we want this data to remain uninitialize in the constructor seems is going to be constructed outside
            events                                                  m_Events            {};
            delegates                                               m_Delegates         {};
            x_ll_share_ref<const_data>                              m_ConstData         {};
            int                                                     m_Index             { -1    };  
        };

        using               pool                    = x_ll_page_pool_jitc< compact_component, t_component::t_descriptors::t_pool_paged_size >;

    protected:

        virtual     base&       CreateComponent                 ( entity& Entity, gb_component::narrow_count NarrowCount )                      noexcept override;
        virtual     int&        getComponentIndex               ( gb_component::base& Comp )                     const   noexcept override final
        {
            return Comp.SafeCast<compact_component>().m_Index;
        }

        virtual     void        msgDestroy                      ( base& Component )                                      noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert( getComponentIndex(Component) == -1 );
            m_MemoryPool.push( *reinterpret_cast<compact_component*>( &Component ) );
        }

    protected:
        
        pool                        m_MemoryPool     {};
    };

    //------------------------------------------------------------------------------
    // Description:
    //      For component entities there is a need to farther specialize the type pool.
    //      Because entity components require special creation/construction.
    //------------------------------------------------------------------------------
    template< typename T_COMP >
    class type_entity_pool final : public type_pool<T_COMP>
    {
        x_object_type( type_entity_pool, is_linear, rtti(type_pool<T_COMP>), is_not_copyable, is_not_movable )

    public:
        
        using               compact_component = typename t_parent::compact_component;

    public:

        x_forceconst type_entity_pool( gb_component_mgr::base& ComponentMgr, gb_game_graph::base& GameMgr, gb_game_graph::sync_point& SyncPoint ) noexcept : t_parent{ ComponentMgr, GameMgr, SyncPoint } {}
        virtual             bool                        isEntityType                ( void )                                        const   noexcept override   { return true; }

    protected:

        virtual entity& CreateEntity( void ) noexcept override;
        virtual void msgDestroy ( base& Component ) noexcept override;
    };
}
