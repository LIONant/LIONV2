//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component  { class base; class type_base; class entity; }
namespace gb_game_graph { class sync_point; class base; }

namespace gb_component_mgr
{
    //------------------------------------------------------------------------------
    // Description:
    //      A component manager deals with components.
    //      They run them, create them, add them to the world, etc.
    //      In fact the collection of managers is what the game world as made of.
    //      Every manager gets updated every frame
    //      Ones a manager runs it updates all the components associated with it.
    //      Ones it has done its job it will notify its gb_trigger
    //      This class is the base class for all game component managers
    //------------------------------------------------------------------------------
    class base : 
        public x_ll_object_harness< base, x_job<10> >
        , public xproperty_v2::base
    {
        x_object_type( base, is_quantum_lock_free, rtti(xproperty_v2::base), is_not_copyable, is_not_movable )

    public:
        
        x_constexprvar bool t_is_component_mgr  = true;
        using               guid                = xguid<t_self>;
        
    public:
            
        x_forceinline                                   base                    ( guid Guid, gb_game_graph::base& GameGraph, const xconst_str_u Name )      noexcept;
        virtual                                        ~base                    ( void )                                                                    noexcept = default;
        x_inline        void                            msgDestroy              ( gb_component::base& Component )                                           noexcept;
        x_incppfile     void                            linearDestroy           ( gb_component::base& Component )                                           noexcept;
        x_forceinline   x_ll_circular_pool&             getMsgAllocQueue        ( void )                                                                    noexcept { return m_MsgAllocPool; }
        constexpr       const guid                      getGuid                 ( void )                                                            const   noexcept { return m_Guid;     }
        constexpr       auto&                           getName                 ( void )                                                            const   noexcept { return m_Name; }
//        x_inline        void                            setup                   ( guid Guid, const char* pName  )                                           noexcept; 
        x_inline        void                            RegisterComponentType   ( gb_component::type_base& Type )                                           noexcept;
        x_inline        void                            UnregisterComponentType ( gb_component::type_base& Type )                                           noexcept;
        x_forceinline   void                            msgAddToWorld           ( gb_component::base& Component )                                           noexcept{ SendMsg([&]{ onAddToWorld( Component );      } ); }
        x_forceinline   void                            msgRemoveFromWorld      ( gb_component::base& Component )                                           noexcept{ SendMsg([&]{ onRemoveFromWorld( Component ); } ); }
        
        template< typename T_GRAPH_TYPE = gb_game_graph::base >
        x_forceinline   T_GRAPH_TYPE&                   getGameGraph            ( void )                                                                    noexcept;
        x_inline        void                            linearReset             ( void )                                                                    noexcept { onReset(); }

    protected:
            
        enum : u32
        {
            MAX_SYNC_POINTS = 8
        };
            
    protected:
            
        virtual         void                            onRemoveFromWorld       ( gb_component::base& Component )                                           noexcept;
        virtual         void                            onAddToWorld            ( gb_component::base& Component )                                           noexcept;
        virtual         void                            onDestroy               ( gb_component::base& Component )                                           noexcept;
        virtual         void                            onExecute               ( void )                                                                    noexcept;
        virtual         void                            onEndOfFrameCleanUp     ( void )                                                                    noexcept;
        virtual         void                            onReset                 ( void )                                                                    noexcept { onEndOfFrameCleanUp(); }
        virtual         prop_table&                     onPropertyTable         ( void )                                                              const noexcept override;
        virtual         void                            qt_onRun                ( void )                                                                    noexcept override final;
        virtual         void                            qt_onDone               ( void )                                                                    noexcept override final;
        x_inline        void                            DirectDestroy           ( gb_component::base& Component )                                           noexcept;
        x_inline        void                            linearAddToWorld        ( gb_component::base& Component )                                           noexcept { onAddToWorld( Component ); }
        x_forceinline   static void                     CallOnRun               ( gb_component::base& Component )                                           noexcept;

    protected:
        
        gb_game_graph::base&                                    m_GameGraph;
        xvector<gb_component::type_base*>                       m_TypeHead          {};
        xvector<gb_component::base*>                            m_DestroyList       {};
        const guid                                              m_Guid;
        const xconst_str_u                                      m_Name;

    protected:
        
        friend class gb_game_graph::sync_point;
        friend class gb_component::entity;
    };
};


