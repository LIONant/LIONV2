//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component_mgr {

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT MANAGER BASE
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

x_forceinline                      
base::base( guid Guid, gb_game_graph::base& GameGraph, const xconst_str_u Name ) noexcept  
    : m_GameGraph{ GameGraph } 
    , x_ll_object_harness( GameGraph.getMsgPool() )
    , m_Guid{ Guid }
    , m_Name{ Name }
{
    X_CMD_JOB_PROFILER_ON( m_JobName = Name );
    x_assert( m_Name.m_Char.m_pValue );
    x_assert( m_Name.m_WChar.m_pValue );
}

//------------------------------------------------------------------------------
x_inline        
void base::msgDestroy( gb_component::base& Component ) noexcept
{ 
    //
    // Lets mark the component as deleted
    //
    auto LocalFlags = Component.m_qtFlags.load( std::memory_order_relaxed );
    do
    {
        if( LocalFlags.m_DELETED ) return;
        const gb_component::base::flags NewFlags = LocalFlags | gb_component::base::flags::MASK_DELETED;
        if( Component.m_qtFlags.compare_exchange_weak( LocalFlags, NewFlags, std::memory_order_relaxed ) )
            break;
    } while( true );

    //
    // Ok let's remove this guy
    //
    SendMsg( [&](){ onDestroy( Component ); } );
}


//------------------------------------------------------------------------------
//virtual
x_inline
void base::onEndOfFrameCleanUp( void ) noexcept
{
    //
    // Free memory for all the pending components
    //
    for( gb_component::base* pDestroy : m_DestroyList )
    {
        pDestroy->getGlobalInterface().m_Type.msgDestroy( *pDestroy );
    }

    // Reset the list
    m_DestroyList.DeleteAllEntries();
}

//------------------------------------------------------------------------------
//virtual
inline
void base::qt_onRun( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    //
    // Force a lock here
    //           
    while( false == TryToLockMsgQueue() ){}

    //
    // Start processing
    //
    {
        x_assert_linear( m_Debug_LQMsg );
        onExecute();
    }
            
    //
    // Remove the consuming flag
    //
    UnLockMsgQueue();
}
        
//------------------------------------------------------------------------------
// virtual
inline
void base::qt_onDone( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    for( int i = 0; i < m_nTriggers; i++ )
    {
        // must notify the trigger that we are done
        NotifyTrigger( *m_TriggerList[i] );
    }
}
            
//------------------------------------------------------------------------------
// Description:
//      Removes the component from the world, which means it won't be updated any more
//------------------------------------------------------------------------------
x_inline
void base::onRemoveFromWorld( gb_component::base& Component ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );

    auto    GI   = Component.getGlobalInterface();
    GI.m_Type.linearRemoveFromWorld( Component );
    
    // Remove the flags that is in the world
    Component.removeFlags( gb_component::base::flags::MASK_IN_THE_WORLD );

    // Some people may still be accessing the component but we will notify it that it is been taken out of the world
    Component.onNotifyOutWorld();
}
    
//------------------------------------------------------------------------------
// Description:
//      Adds the component into the world by adding it into our active list
//      and notifying the component itself.
//------------------------------------------------------------------------------
x_inline
void base::onAddToWorld( gb_component::base& Component ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );

    auto&   GI   = Component.getGlobalInterface();
     
    //
    // We also need to sync the component with its sync point
    // We also need to prepare the flags
    //
    x_constexprvar auto  RemoveFlags = ~( gb_component::base::flags::MASK_T0 );
    const auto           AddFlags    = gb_component::base::flags::MASK_IN_THE_WORLD | ( gb_component::base::flags::t_flags{ (u32)GI.m_SyncPoint.getICurrent() } & gb_component::base::flags::MASK_T0 );

    // Set the flags
    for( gb_component::base::flags LocalFlags = Component.m_qtFlags.load(); 
         Component.m_qtFlags.compare_exchange_weak( LocalFlags, (LocalFlags & RemoveFlags) | AddFlags ) == false; );

    x_assert( (( Component.m_qtFlags.load().m_Value ^ GI.m_SyncPoint.getICurrent() ) & gb_component::base::flags::MASK_T0.m_Value) == 0 );

    // Before to put it in the quantum world we will send a notification
    // last chance for the user to do something safely
    Component.onNotifyInWorld();

    // Officially add the component into the world
    GI.m_Type.linearAddToWorld( Component );
}
   
//------------------------------------------------------------------------------
// Description:
//      Registers a component type in the manager
//------------------------------------------------------------------------------
x_inline
void base::RegisterComponentType( gb_component::type_base& Type ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );

    m_TypeHead.append( &Type );
}

//------------------------------------------------------------------------------
// Description:
//      Removes the component from the world, which means it won't be updated any more
//------------------------------------------------------------------------------
x_inline
void base::UnregisterComponentType ( gb_component::type_base& Type ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );

    for( int i = 0; i < m_TypeHead.getCount(); ++i )
    {
        if( m_TypeHead[i] == &Type )
        {
            m_TypeHead.DeleteWithSwap( i );
            break;
        }
    }
}
 
//------------------------------------------------------------------------------
// Description:
//      It will run each component as a independent job
//      This is not the most optimum way to do this.
//------------------------------------------------------------------------------
x_inline
void base::onExecute( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );
    
    // Start running components
    X_CMD_JOB_PROFILER_ON   ( x_job_block JobBlock( m_JobName  ));
    X_CMD_JOB_PROFILER_OFF  ( x_job_block JobBlock( X_WSTR("") ));
    for( auto pType : m_TypeHead )
        JobBlock.SubmitJob( [pType]( void )
    {
        pType->qt_Execute();
    });
    JobBlock.Join();
}

//------------------------------------------------------------------------------
// Description:
//      Lets the type remove the component officially
//------------------------------------------------------------------------------
x_inline
void base::onDestroy( gb_component::base& Component ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );

    if( Component.isInWorld() ) 
    {
        // Make sure we take it out of the world properly
        onRemoveFromWorld( Component );
    }

    // We are adding this component to the to be deleted list
    m_DestroyList.append( &Component );
}

//------------------------------------------------------------------------------
// Description:
//      Unlike ondestroy this version does not delay the release of memory it does
//      it immediately 
//------------------------------------------------------------------------------
x_inline        
void base::DirectDestroy(  gb_component::base& Component ) noexcept 
{ 
    if( Component.isInWorld() ) 
    {
        // Make sure we take it out of the world properly
        onRemoveFromWorld( Component );
    }

    // Directly destroy it
    Component.getGlobalInterface().m_Type.msgDestroy( Component );
}

//------------------------------------------------------------------------------
// Description:
//      This function just cal on run for the component. This is done like this 
//      because c++ friend can not survive inheritance.
//------------------------------------------------------------------------------
x_forceinline 
void base::CallOnRun( gb_component::base& Component ) noexcept
{
    Component.qt_onRun();
}
    
//------------------------------------------------------------------------------
// Description:
//------------------------------------------------------------------------------
template< typename T_GRAPH_TYPE > x_forceinline
T_GRAPH_TYPE& base::getGameGraph( void ) noexcept
{
    return m_GameGraph.template SafeCast<T_GRAPH_TYPE>();
}

    
///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
