//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component {


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT TYPE BASE
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
x_inline 
void type_base::linearAddToWorld( gb_component::base& Comp ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert( getComponentIndex( Comp ) == -1 );
    getComponentIndex( Comp ) = m_InWorld.getCount<int>();
    m_InWorld.append( &Comp );
}
        
//------------------------------------------------------------------------------
x_inline 
void type_base::linearRemoveFromWorld( gb_component::base& Comp ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert( Comp.isInWorld() );

    const auto  LastIndex      = m_InWorld.getCount() - 1;
    auto&       LastEntry      = *m_InWorld[ LastIndex ];
    auto&       LastEntryIndex = LastEntry.getGlobalInterface().m_Type.getComponentIndex( LastEntry );
    auto&       ComponentIndex = getComponentIndex( Comp );
     
    x_assert( LastEntryIndex == LastIndex );

    LastEntryIndex = ComponentIndex;
    m_InWorld.DeleteWithSwap( ComponentIndex );
    ComponentIndex = -1;

    x_assert( LastEntryIndex == -1 || m_InWorld[ LastEntryIndex ] == &LastEntry );
}
        
//------------------------------------------------------------------------------
x_inline 
void type_base::qt_Execute( void ) noexcept
{
    // Start running components
    x_job_block JobBlock( getCategoryName().m_WChar );
    JobBlock.ForeachLog( m_InWorld, 8, 10, []( gb_component::base*& pComp )
    {
        pComp->qt_onRun();
    });
    JobBlock.Join();
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT TYPE POOL
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Description:
//          Allocates a component as well as construct it
//------------------------------------------------------------------------------
template< typename T_COMP >
void type_entity_pool<T_COMP>::msgDestroy ( base& Component ) noexcept
{
    x_assert_quantum( this->m_Debug_LQ );
    
    // Let the manager know that it must remove all references for this entity
    Component.getGlobalInterface().m_GameMgr.qt_onEntityDestroy( Component.SafeCast<entity>() );
    
    // Now you can deal with the rest
    t_parent::msgDestroy( Component );
}
    
//------------------------------------------------------------------------------
// Description:
//          Allocates a component as well as construct it
//------------------------------------------------------------------------------
template< typename T_COMP >
entity& type_entity_pool<T_COMP>::CreateEntity( void ) noexcept
{
    static_assert( std::is_base_of<gb_component::entity, T_COMP>::value, "This is not an entity" );
    //x_assert_quantum( this->m_Debug_LQ );
    
    compact_component&  Node    = this->m_MemoryPool.popDontConstruct();
    const auto          iTx     = (t_parent::m_GlobalInterface.m_SyncPoint.getICurrent() & T_COMP::flags::MASK_T0.m_Value);

    // construct mutable data
    x_construct( typename compact_component::t_mutable_data, { &Node.m_MutableData[0] } );
    x_construct( typename compact_component::t_mutable_data, { &Node.m_MutableData[1] } );

    // construct mutable data

    // Construct our component in with the right order (mutable data first)
    (void)x_construct( compact_component, &Node,
    {
        //gb_component::base::base_construct_info
        {
            Node,
            t_parent::m_GlobalInterface,
            (iTx) ? T_COMP::flags::MASK_T0 : T_COMP::flags::MASK_ZERO,
             *reinterpret_cast<gb_component::base::mutable_data*>( &Node.m_MutableData[iTx].m_Data[0] )
        }
    });
    
    // Ok we are ready to return the component
    return Node;
}
    
//------------------------------------------------------------------------------
// Description:
//      Allocates a component as well as construct it
//------------------------------------------------------------------------------
template< typename T_COMP > x_inline
base& type_pool<T_COMP>::CreateComponent( entity& Entity, gb_component::narrow_count NarrowCount ) noexcept
{
//    x_assert_quantum( m_Debug_LQ );

    // Create an un-constructed object
    compact_component& Node     = m_MemoryPool.popDontConstruct();
    const auto          iTx     = (t_parent::m_GlobalInterface.m_SyncPoint.getICurrent() & T_COMP::flags::MASK_T0.m_Value);

    // construct mutable data
    x_construct( typename compact_component::t_mutable_data, { &Node.m_MutableData[0] } );
    x_construct( typename compact_component::t_mutable_data, { &Node.m_MutableData[1] } );

    // Construct our component in with the right order (mutable data first)
    (void)x_construct( compact_component, &Node, 
    { 
        //gb_component::base::base_construct_info
        { 
            Entity, 
            m_GlobalInterface, 
            (iTx) ? T_COMP::flags::MASK_T0 : T_COMP::flags::MASK_ZERO,
            *reinterpret_cast<gb_component::base::mutable_data*>( &Node.m_MutableData[iTx].m_Data[0] )
        } 
    });

    // Add the component to the entity
    Entity.linearInternalAddComponent( Node, NarrowCount );
        
    // Ok we are ready to return the component
    return Node;
}

///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
