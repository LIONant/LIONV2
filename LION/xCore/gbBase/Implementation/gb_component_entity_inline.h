//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component {


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT ENTITY
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Description:
//      component entity
//------------------------------------------------------------------------------
x_forceinline 
entity::entity( const base_construct_info& C ) noexcept : base{ C } 
{ 
    x_assert( isEntity() ); 
}

//------------------------------------------------------------------------------
// Description:
//      Adds components to an entity. Please make sure the entity is not in the world.
//------------------------------------------------------------------------------
x_inline 
void entity::linearInternalAddComponent( base& Component, narrow_count NarrowCount ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert_lqnone( m_Debug_LQMsg );
// TODO: Check that the game is not working
//    x_assert( isInWorld() == false );

    // Create a narrow GUID for our component
    if( NarrowCount.isNull() )
    {
        NarrowCount = linearCreateNarrowCount(Component.getType().getGuid());
    }

    // Set the narrow GUID
    auto Flags = Component.m_qtFlags.load( std::memory_order_relaxed );
    Flags.m_NARROW_COUNT = NarrowCount.m_Value;
    Component.m_qtFlags.store( Flags, std::memory_order_relaxed );
    
    // Link the component into the entity
    Component.m_pNextComp = m_pNextComp;
    m_pNextComp = &Component;
}

//------------------------------------------------------------------------------
// Description:
//      Add the entity to the world, the entity will add alls its components to the world too
//------------------------------------------------------------------------------
x_inline 
void entity::msgAddToWorld( void ) noexcept
{
    // Entity must be resolved before entering the world
    if( isResolved() == false ) linearResolve();

    x_assert_quantum( m_Debug_LQ );
    m_GInterface.m_Manager.msgAddToWorld( *this );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.msgAddToWorld( *pNext );
    }
}

//------------------------------------------------------------------------------
// Description:
//      Gives a chance to all the components to resolve themselves before entering the world
//      This function will be done automatically
//------------------------------------------------------------------------------
x_forceinline   
entity::err entity::linearCheckResolve( xvector<xstring>& WarningList ) const noexcept 
{ 
    if( err Err = onCheckResolve( WarningList ); Err.isError() ) return Err;
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        err Err = pNext->onCheckResolve( WarningList );
        if( Err.isError() ) return Err;
    }

    return x_error_ok();
}

//------------------------------------------------------------------------------
// Description:
//      Gives a chance to all the components to resolve themselves before entering the world
//      This function will be done automatically
//------------------------------------------------------------------------------
x_forceinline       
void entity::linearResolve( void ) noexcept
{ 
    // x_assert( isResolved() == false ); 
    // x_assert( isInWorld() == false );
    
    // Double check to see that everything is resolving
    // you can have warnings but no errors
    // X_DEBUG_CMD( xvector<xstring> TempWarnings{}; );
    // x_assert( linearCheckResolve( TempWarnings ).isError() == false );
    
    onResolve();
     
    appendFlags( flags::MASK_RESOLVED );
}

//------------------------------------------------------------------------------
// Description:
//      Add the entity to the world, the entity will add alls its components to the world too
//------------------------------------------------------------------------------
x_inline 
void entity::linearAddToWorld( void ) noexcept
{
    // Entity must be resolved before entering the world
    if( isResolved() == false ) linearResolve();

    x_assert_quantum( m_Debug_LQ );
    m_GInterface.m_Manager.linearAddToWorld( *this );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.linearAddToWorld( *pNext );
    }
}

//------------------------------------------------------------------------------
// Description:
//      Destroys the entity and its components
//------------------------------------------------------------------------------
x_inline 
void entity::msgDestroy( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    //
    // Officially notify the manager to destroy the components
    //
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.msgDestroy( *pNext );
    }
    m_GInterface.m_Manager.msgDestroy( *this );
}

//------------------------------------------------------------------------------
// Description:
//      Destroys the entity and its components, this function should be call from a thread safe place
//------------------------------------------------------------------------------
x_inline            
void entity::linearDestroy( void ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.linearDestroy( *pNext );
    }
    m_GInterface.m_Manager.linearDestroy( *this );
}

//------------------------------------------------------------------------------
// Description:
//      Will destroy a single component from the entity. 
//      The component must be in the entity.
//      WARNING!!! The component at this point is gone. (memory is invalid)
//------------------------------------------------------------------------------
x_inline            
void entity::linearDestroyComponent( xowner<base&> component ) noexcept
{
    //
    // Unlink the component
    //
    if ( m_pNextComp == &component ) 
    {
        m_pNextComp = component.m_pNextComp;
    }
    else 
    {
        auto* pNext = m_pNextComp;
        for ( ; pNext; pNext = pNext->m_pNextComp ) 
        {
            if ( pNext->m_pNextComp == &component ) 
            {
                pNext->m_pNextComp = component.m_pNextComp;
                break;
            }
        }
        x_assert( pNext );
    }                       

    //
    // Officially destroy the component here
    //
    component.m_GInterface.m_Manager.linearDestroy( component );
    // WARNING!!! The component at this point is gone. (memory is invalid)
}

//------------------------------------------------------------------------------
// Description:
//      Removes the entity from the world
//------------------------------------------------------------------------------
x_inline 
void entity::msgRemoveFromWorld( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    m_GInterface.m_Manager.msgRemoveFromWorld( *this );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        m_GInterface.m_Manager.msgRemoveFromWorld( *pNext );
    }
}

//------------------------------------------------------------------------------
// Description:
//      Gets an entity of a certain type
//------------------------------------------------------------------------------
template< typename T >  x_inline
T* entity::getComponent( gb_component::base* pFrom ) const noexcept
{
    static_assert( std::is_base_of<gb_component::base, T>::value,                                           "T in the template should be of a component" );
    static_assert( std::is_base_of<gb_component::type_base::guid, decltype(T::t_descriptors::t_type_guid)>::value,    "This type should be a gb_component::type_base::guid" );

    // If the entity is of the same type as the component the user is looking for then there is a problem
    // because the user already has have it and should know the type of the entity.
    x_assert( T::t_descriptors::t_type_guid != getGlobalInterface().m_Type.getGuid() );

    gb_component::base* p = getComponent( T::t_descriptors::t_type_guid, pFrom );
    if( p ) return &p->SafeCast<T>();
    return nullptr;
}

//------------------------------------------------------------------------------
// Description:
//      Given the type of a component via its type guid it will return such a component if 
//      it can find it.
//------------------------------------------------------------------------------
x_inline 
gb_component::base* entity::getComponent( gb_component::type_base::guid TypeGuid, gb_component::base* pFrom ) const noexcept
{
    if( pFrom )     pFrom = pFrom->m_pNextComp;
    else            pFrom = m_pNextComp;

    for( gb_component::base* p = pFrom; p ; p = p->m_pNextComp )
    {
        if( TypeGuid == p->getType().getGuid() )
            return p; 
    }

    return nullptr;
}


//------------------------------------------------------------------------------
// Description:
//      Entity will resolve all its components
//------------------------------------------------------------------------------
// virtual
inline
void entity::onResolve ( void ) noexcept
{
    t_parent::onResolve();

    for( auto* pComp = getNextComponent(); pComp; pComp = pComp->getNextComponent() )
    {
        pComp->onResolve();
    }
}

//------------------------------------------------------------------------------
// Description:
//      The entity will check all it components
//------------------------------------------------------------------------------
// virtual
inline
entity::err entity::onCheckResolve( xvector<xstring>& WarningList ) const noexcept
{
    for( auto* pComp = getNextComponent(); pComp; pComp = pComp->getNextComponent() )
    {
        auto Err = pComp->onCheckResolve( WarningList );
        if( Err.isError() )
        {
            return Err;
        }
    }
    return x_error_ok();
}

//------------------------------------------------------------------------------
// Description:
//      Creates a unique ID for each component for the entity
//------------------------------------------------------------------------------
x_inline 
narrow_count entity::linearCreateNarrowCount( type_base::guid TypeGuid ) noexcept
{
    int                         nEntries = 0;
    xarray<narrow_count,0xff>   Table;

    // Fill the table first to know which guids have been used
    for( auto* pComp = getNextComponent(); pComp; pComp = pComp->getNextComponent() )
    {
      //  if( pComp->getType().getGuid() == TypeGuid )
        {
            const auto Index = pComp->m_qtFlags.load( std::memory_order_relaxed ).m_NARROW_COUNT;
            Table[ nEntries++ ].m_Value = Index;
        }
    }

    // quick cases
    if( nEntries == 0 ) return narrow_count{0u};
    if( nEntries == 1 ) return narrow_count{ Table[0].m_Value == 0 ? 1u : 0u };

    // Otherwise we want to choose the lowest possible value
    Table.QSort([](const narrow_count& A, const narrow_count& B ) -> int
    {
        if( A.m_Value < B.m_Value ) return -1;
        return A.m_Value > B.m_Value;

    }, nEntries );

    // Now lets find the hole
    narrow_count Smallest = narrow_count{0u};
    for( int i = 0; i < nEntries; i++ )
    {
        if( Smallest.m_Value < Table[i].m_Value )
        {
            return Smallest;
        }
        else
        {
            Smallest.m_Value++;
        }
    }

    if( Smallest.m_Value < Table.getCount<u8>() ) return Smallest;
    
    // Did not found a GUID
    x_assert( false );
    return narrow_count{nullptr};
}

//------------------------------------------------------------------------------

template< typename T_COMPONENT > x_inline 
narrow_count entity::linearCreateNarrowCount( void ) noexcept
{
    static_assert( std::is_base_of<gb_component::base,T_COMPONENT>::value, "Must be a component type" );
    return linearCreateNarrowCount( T_COMPONENT::t_descriptors::t_type_guid );
}

//------------------------------------------------------------------------------
/*
x_inline
const xproperty_v2::table&  entity::onPropertyTable ( void ) const noexcept 
{ 
    x_assert_quantum( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg ); 
    static const xproperty_v2::table E( [&](xproperty_v2::table& E)
    {
        E.AddChildTable( base::onPropertyTable() );
        E.AddDynamicList( X_STR("Component")
            , 
            X_STATIC_LAMBDA_BEGIN ( entity& A, void*& pNext ) -> int
            {
                gb_component::base*& pCompNext = reinterpret_cast<gb_component::base*&>(pNext);
                if( pCompNext == nullptr )
                {
                    pCompNext = A.m_pNextComp;
                    return pCompNext->getNarrowGUID();
                }
                else
                {
                    pCompNext = pCompNext->m_pNextComp;
                    if( pCompNext ) return pCompNext->getNarrowGUID();
                    return -1;
                }
            }
            X_STATIC_LAMBDA_END
            ,
            X_STATIC_LAMBDA_BEGIN ( entity& A, int id, bool isSend ) -> xproperty_v2::base*
            {
                for( auto* pNext = A.m_pNextComp; pNext ; pNext = pNext->m_pNextComp )
                {
                    if( id == pNext->getNarrowGUID() ) return pNext;
                }    

                return nullptr;
            }
            X_STATIC_LAMBDA_END
            ,
            X_STATIC_LAMBDA_BEGIN ( entity& A, int& Count, bool isSend )
            {
                if(isSend)
                {
                    Count = 0;
                    for( auto* pNext = A.m_pNextComp; pNext ; pNext = pNext->m_pNextComp ) Count++;
                }
            }
            X_STATIC_LAMBDA_END
            //,
            //xproperty_v2::flags::MASK_NOT_VISIBLE | xproperty_v2::flags::MASK_FORCE_SAVE 
        );
    }); 
    return E; 
}
*/

///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
