//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "gb_base.h"

namespace gb_blueprint {

//------------------------------------------------------------------------------

void master::UpdateAsNew( const gb_component::entity& Entity ) 
{ 
    m_Bundle.m_lEntityPropertiesV2.DeleteAllEntries(); 
    Entity.linearSaveToBlueprintBundle( m_Bundle ); 
}

//------------------------------------------------------------------------------

void master::setup( const gb_component::entity& Entity ) 
{ 
    Entity.linearSaveToBlueprintBundle( m_Bundle ); 
}

//------------------------------------------------------------------------------

gb_component::entity& master::CreateInstance( gb_component::entity::guid Guid, gb_game_graph::base& GameMgr ) const
{
    //
    // Create the entity
    //
    auto& Entity = gb_component::entity::linearLoadFromBlueprintBundle( Guid, m_Bundle, GameMgr );
    Entity.setupBlueprintGuid( m_Guid );

    return Entity;
}

//------------------------------------------------------------------------------

gb_component::entity& master::CreateInstance( gb_component::entity::guid Guid, const xmatrix4& L2W, gb_game_graph::base& GameMgr ) const
{
    //
    // Create the entity
    //
    auto& Entity = gb_component::entity::linearLoadFromBlueprintBundle( Guid, m_Bundle, GameMgr );
    Entity.setupBlueprintGuid( m_Guid );

    //
    // Apply the relative position
    //
    Entity.linearTransformSet( L2W, []( xmatrix4& DestL2W, const xmatrix4& SrcL2W )
    {
        DestL2W = SrcL2W;
    });

    return Entity;
}

//------------------------------------------------------------------------------

gb_component::entity& master::CreateInstance(  gb_component::entity::guid Guid, const bundled_entity& InstanceOverrides, gb_game_graph::base& GameMgr ) const
{
    //
    // Create the entity
    //
    auto& Entity = gb_component::entity::linearLoadFromBlueprintBundle( Guid, m_Bundle, GameMgr );
    Entity.setupBlueprintGuid( m_Guid );

    //
    // Override any properties with the Instance
    //
    if( InstanceOverrides.m_lEntityPropertiesV2.getCount() > 0 )
        Entity.linearPropSetV2( [&]( const int Index ) -> const xproperty_v2::entry*
        {
            return Index < InstanceOverrides.m_lEntityPropertiesV2.getCount<int>() ? &InstanceOverrides.m_lEntityPropertiesV2[Index] : nullptr;
        });

    //
    // Override any properties with the Instance
    //
    /*
    if( InstanceOverrides.m_lEntityProperties.m_List.getCount() > 0 )
    Entity.linearPropSet([&]( xproperty_query& Query, const xproperty_data*& pData )
    {
        //
        // are we done?
        //
        if( Query.getPropertyIndex() >= InstanceOverrides.m_lEntityProperties.m_List.getCount() ) 
        {
            return false;
        }

        //
        // Tell which property to get next
        //
        pData = &InstanceOverrides.m_lEntityProperties.m_List[ Query.getPropertyIndex() ];

        return true;
    });
    */

    return Entity;
}

//------------------------------------------------------------------------------

void master::Save ( xtextfile& TextFile ) noexcept
{
    m_Bundle.SaveAsEntity( "Blueprint", gb_component::entity::guid{ m_Guid.m_Value }, TextFile );    
}

//------------------------------------------------------------------------------

void master::Load ( xtextfile& TextFile, gb_game_graph::base& GameMgr ) noexcept
{
    gb_component::entity::guid Guid;
    x_assert( TextFile.getRecordName() == X_STR("Blueprint") );

    m_Bundle.LoadAsEntity( Guid, TextFile );
    
    m_Guid.m_Value = Guid.m_Value;    
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------

void bundled_entity::SaveAsEntity( const char* pHeader, gb_component::entity::guid Guid, xtextfile& TextFile ) noexcept
{
    //
    // Save the entity
    //
    TextFile.WriteRecord( pHeader );
    TextFile.WriteField( "GUID:g",          Guid.m_Value );
    TextFile.WriteField( "Type:g",          m_EntityTypeGuid.m_Value );
    TextFile.WriteLine();

    //
    // Save each of the components general information
    //
    TextFile.WriteRecord( "Components", m_lComponentTypes.getCount<int>() );
    for( const auto& Entry : m_lComponentTypes )
    {
        TextFile.WriteField( "Type:g",  Entry.m_gCompType.m_Value );
        TextFile.WriteField( "NGUID:c", Entry.m_NarrowGUID );
        TextFile.WriteLine();
    }

    //
    // Save Properties V2
    //
    TextFile.WriteRecord( "Properties", m_lEntityPropertiesV2.getCount<int>() );
    for( const auto& Entry : m_lEntityPropertiesV2 )
    {
        Entry.SerializeOut( TextFile );
    }

    //
    // Save Properties
    //
    /*
    TextFile.WriteRecord( "Properties", m_lEntityProperties.m_List.getCount<int>() );
    for( const auto& Entry : m_lEntityProperties.m_List )
    {
        Entry.SerializeOut( TextFile );
    }
    */
}

//------------------------------------------------------------------------------

void bundled_entity::SaveAsBlueprintInstance( const char* pHeader, gb_component::entity::guid Guid, gb_blueprint::guid BPGuid, xtextfile& TextFile ) noexcept
{
    //
    // Save the entity
    //
    TextFile.WriteRecord( pHeader );
    TextFile.WriteField( "GUID:g",          Guid.m_Value );
    TextFile.WriteField( "Blueprint:g",     BPGuid.m_Value );
    TextFile.WriteLine();

    //
    // Save Properties V2
    //
    TextFile.WriteRecord( "Properties", m_lEntityPropertiesV2.getCount<int>() );
    for( const auto& Entry : m_lEntityPropertiesV2 )
    {
        Entry.SerializeOut( TextFile );
    }

    //
    // Save Properties
    //
/*
    TextFile.WriteRecord( "Properties", m_lEntityProperties.m_List.getCount<int>() );
    for( const auto& Entry : m_lEntityProperties.m_List )
    {
        Entry.SerializeOut( TextFile );
    }
    */
}

//------------------------------------------------------------------------------

void bundled_entity::LoadAsEntity( gb_component::entity::guid& Guid, xtextfile& TextFile ) noexcept
{
    //
    // Load basic info
    //
    TextFile.ReadLine();
    TextFile.ReadField( "GUID:g",          &Guid.m_Value                );
    TextFile.ReadField( "Type:g",          &m_EntityTypeGuid.m_Value    );

    //
    // Load all the component types
    //
    x_verify( TextFile.ReadRecord() );
    if( TextFile.getRecordName() == X_STR( "Components" ) )
    {
        const int nComponents = TextFile.getRecordCount();
        m_lComponentTypes.New( nComponents );
        for( int i = 0; i < nComponents; ++i )
        {
            auto& Entry = m_lComponentTypes[i];
            TextFile.ReadLine();
            TextFile.ReadField( "Type:g",  &Entry.m_gCompType.m_Value );
            TextFile.ReadField( "NGUID:c", &Entry.m_NarrowGUID );
        }

        //
        // Get the next record
        //
        TextFile.ReadRecord();
    }

    //
    // Load all the properties V2
    //
    if( TextFile.getRecordName() == X_STR( "Properties" ) )
    {
        const int nProperties = TextFile.getRecordCount();
        for( int i = 0; i < nProperties; ++i )
        {
            TextFile.ReadLine();
            m_lEntityPropertiesV2.append().SerializeIn( TextFile );
        }

        //
        // Get the next record
        //
        TextFile.ReadRecord();
    }
/*
    //
    // Load all the properties
    //
    if( TextFile.getRecordName() == X_STR( "Properties" ) )
    {
        const int nProperties = TextFile.getRecordCount();
        for( int i = 0; i < nProperties; ++i )
        {
            TextFile.ReadLine();
            m_lEntityProperties.m_List.append().SerializeIn( TextFile );
        }

        //
        // Get the next record
        //
        TextFile.ReadRecord();
    }
    */
}

//------------------------------------------------------------------------------

void bundled_entity::LoadAsBlueprintInstance( gb_component::entity::guid& Guid, gb_blueprint::guid& BPGuid, xtextfile& TextFile ) noexcept
{
    //
    // Load basic info
    //
    TextFile.ReadLine();
    TextFile.ReadField( "GUID:g",           &Guid.m_Value       );
    TextFile.ReadField( "Blueprint:g",      &BPGuid.m_Value     );

    //
    // Load all the properties
    //
    if( TextFile.ReadRecord() && TextFile.getRecordName() == X_STR( "Properties" ) )
    {
        const int nProperties = TextFile.getRecordCount();
        for( int i = 0; i < nProperties; ++i )
        {
            TextFile.ReadLine();
            m_lEntityPropertiesV2.append().SerializeIn( TextFile );
        }

        //
        // Get the next record
        //
        TextFile.ReadRecord();
    }

/*
    //
    // Load all the properties
    //
    TextFile.ReadRecord();
    if( TextFile.getRecordName() == X_STR( "Properties" ) )
    {
        const int nProperties = TextFile.getRecordCount();
        for( int i = 0; i < nProperties; ++i )
        {
            TextFile.ReadLine();
            m_lEntityProperties.m_List.append().SerializeIn( TextFile );
        }

        //
        // Get the next record
        //
        TextFile.ReadRecord();
    }
    */
}

//------------------------------------------------------------------------------
// END
//------------------------------------------------------------------------------        
}
