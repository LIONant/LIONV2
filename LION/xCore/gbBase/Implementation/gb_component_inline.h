//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component {

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT BASE
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------
// Description:
//      Only constructor for a component
//------------------------------------------------------------------------------
x_forceinline
base::base( const base_construct_info& C ) noexcept :  
    x_ll_object_harness< base > { C.m_GI.m_Manager.getMsgAllocQueue()                },
    m_qtFlags                   { C.m_Flags                                          },
    m_Entity                    { C.m_Entity                                         },
    m_GInterface                { C.m_GI                                             }
{ 
    x_assert_linear( m_Debug_LQ );
}

//------------------------------------------------------------------------------
// Description:
//      Goes through all the objects messages and tries to process them
//------------------------------------------------------------------------------
inline
void base::t_ProcessMessages( void ) noexcept
{
    const auto ICurrent     = flags::t_flags{ static_cast<u32>(m_GInterface.m_SyncPoint.getICurrent()) }; 
          auto LocalFlags   = m_qtFlags.load( std::memory_order_relaxed );
    const auto Diff         = ( LocalFlags ^ ICurrent ) & flags::MASK_T0;

    if( Diff.m_Value ) do 
    {
        const auto      RemoveFlags = ~(LocalFlags & flags::MASK_T0) ;
        const auto      AddFlags    = ICurrent & flags::MASK_T0;
        const flags     NewFlags    { (LocalFlags & RemoveFlags) | AddFlags };

        if( m_qtFlags.compare_exchange_weak( LocalFlags, NewFlags ) )
        {
            const auto iT0 = AddFlags & flags::MASK_T0;
            x_assume( static_cast<xuptr>(!iT0.m_Value) <= 1 );
            getMutableData( !iT0.m_Value ).onUpdateMutableData( getMutableData( iT0.m_Value ) );
            break;
        }

    } while( true );
}


//------------------------------------------------------------------------------
// Description:
//          Appends flags to the component flag field
//------------------------------------------------------------------------------
x_inline 
base::flags base::appendFlags( flags Flags ) noexcept
{
    flags LocalFlags = m_qtFlags.load( std::memory_order_relaxed );
    flags NewValue; 
    do
    {
        if( (LocalFlags & Flags ) == Flags ) return LocalFlags;
        NewValue = LocalFlags | Flags;

    } while( !m_qtFlags.compare_exchange_weak( LocalFlags, NewValue ) );

    return NewValue;
}
    
//------------------------------------------------------------------------------
// Description:
//          Remoces flags to the component flag field
//------------------------------------------------------------------------------
x_inline 
base::flags base::removeFlags( const flags Flags ) noexcept
{
    const auto  AndFlags    = ~Flags;
    flags       LocalFlags  = m_qtFlags.load( std::memory_order_relaxed );
    flags       NewValue; 
    do
    {
        if( (LocalFlags & Flags) == flags::MASK_ZERO )
        {
            NewValue = LocalFlags;
            break;
        }
        NewValue = LocalFlags & AndFlags;

    } while( !m_qtFlags.compare_exchange_weak( LocalFlags, NewValue ) );

    x_assert( hasFlags(Flags) == false );
    return NewValue;
}


//------------------------------------------------------------------------------
// Description:
//      This function makes sure that all mutations are linearized
//      The Manager will run this function every frame.
//      Any pending messages will be handle
//      The component hierarchy will have a change to update itself thought onExecute
//      We will also lock our component from processing more messages until the sync point
//      is trigger.
//------------------------------------------------------------------------------
inline
void base::qt_onRun( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    //
    // We must be able to lock our component
    //
    while( false == TryToLockMsgQueue() ){}

    //
    // Now process any pending messages
    //
    {
        x_assert_linear( m_Debug_LQMsg );
        
        // Allow the user to compute its component if need be
        onExecute();
    }

    //
    // Unlock
    //
    UnLockMsgQueue();
}


//------------------------------------------------------------------------------
// Description:
//      This function lets the component knows that it just been added to the world
//------------------------------------------------------------------------------
inline
void base::onNotifyInWorld( void ) noexcept
{
}


//------------------------------------------------------------------------------
// Description:
//      checks to see if the constant data has been assign
//------------------------------------------------------------------------------
x_forceinline   
bool base::isConstDataValid( void ) const noexcept 
{ 
    return internal_ConstDataValid(); 
} 

//------------------------------------------------------------------------------
// Description:
//      Gets the constant data for the component, if it is not set it will assert
//------------------------------------------------------------------------------
template< typename T_CLASS > x_forceinline
const T_CLASS& base::getConstData ( void ) const noexcept 
{ 
    x_assert(isConstDataValid()); 
    return internal_ConstData().SafeCast<const T_CLASS>(); 
} 

//------------------------------------------------------------------------------
// Description:
//      The official function to get the constant data for TIME0
//------------------------------------------------------------------------------
template< typename T_CLASS > x_forceinline
const T_CLASS& base::getT0Data( void ) const noexcept
{
    x_assert_quantum( m_Debug_LQ );
    //x_assert( m_GInterface.m_SyncPoint.getICurrent() & flags::MASK_T0.m_Value );
    return reinterpret_cast<const T_CLASS&>( getMutableData( m_GInterface.m_SyncPoint.getICurrent() ));
}

//------------------------------------------------------------------------------
// Description:
//      The official function to get the mutable data for TIME1
//------------------------------------------------------------------------------
template< typename T_CLASS > x_forceinline   
T_CLASS& base::getT1Data( void )   noexcept 
{ 
    x_assert_quantum( m_Debug_LQ ); 
    return reinterpret_cast<T_CLASS&>( getMutableData(1 - m_GInterface.m_SyncPoint.getICurrent()) ); 
}

//------------------------------------------------------------------------------
// Description:
//          Checks to see if it is an entity
//------------------------------------------------------------------------------
x_orforceconst  
bool base::isEntity ( void ) const noexcept 
{ 
    return static_cast<base*>(&m_Entity) == this;        
}

//------------------------------------------------------------------------------
// Description:
//      Gets the Narrow GUID of a component. This narrow uniquely identifies
//      the component inside the entity.
//------------------------------------------------------------------------------
x_forceinline   
base::narrow_guid base::getNarrowGUID( void ) const noexcept 
{ 
    return narrow_guid{ getType().getGuid().getNameCRC(), getNarrowCount() }; 
}


//------------------------------------------------------------------------------
// Description:
//          This function will be call when the entity has been finalize.
//          It will copy any data from T1 into T0 to make sure everyone
//           has access to the starting data.
//------------------------------------------------------------------------------
// virtual
inline
void base::onResolve( void ) noexcept
{
    const int iT0 = m_qtFlags.load().m_T0;
    getMutableData(iT0).onUpdateMutableData( getMutableData(1-iT0) );
}

//------------------------------------------------------------------------------
// Description:
//          This function should return if the component has all the dependencies
//          That it needs. If it has everything it needs should return ok other wise
//          it should return an error. If the component wants to issue any warnings
//          it can append them to the list with an xstring. Please note that this
//          function will be call by the editor many times. So it should have no
//          state changes.
//------------------------------------------------------------------------------
// virtual
inline
base::err base::onCheckResolve( xvector<xstring>& WarningList ) const noexcept 
{
     return x_error_ok();
}

//------------------------------------------------------------------------------
// Description:
//          Simple utility function used to help queries for constant data
//------------------------------------------------------------------------------
template< typename T_CLASS > x_inline        
void base::SendReciveConstantData( xproperty_query& Query, x_ll_share_ref<T_CLASS>& ConstData ) const noexcept
{
    if( Query.isReceiving() )
    {
        const const_data::guid Guid{ Query.getProp<xprop_guid>().getGUID().m_Value };
                
        if( Guid.m_Value )
        {
            const auto& ConstDataMgr = getGlobalInterface().m_GameMgr.m_ConstDataDB;
            ConstData = const_cast<const_data&>( ConstDataMgr.getEntry( Guid ) ).SafeCast<T_CLASS>();
        }
        else
        {
            ConstData.setNull();
        }
    }
    else
    {
        if( ConstData.isValid() )
        {
            xprop_guid::Send
            ( 
                Query, 
                ConstData->getGuid().m_Value, 
                T_CLASS::t_type.getEditorName(),
                T_CLASS::t_type.getGuid().m_Value
            );
        }
        else
        {
            xprop_guid::Send
            ( 
                Query, 
                0, 
                T_CLASS::t_type.getEditorName(),
                T_CLASS::t_type.getGuid().m_Value 
            );
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
