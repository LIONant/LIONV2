//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "gb_base.h"

namespace gb_component {

x_constexprvar xarray<xstring::const_str,50> s_IntToString = 
{
    X_STR( "C[0]"), X_STR( "C[1]"), X_STR( "C[2]"), X_STR( "C[3]"), X_STR( "C[4]"), X_STR( "C[5]"), X_STR( "C[6]"), X_STR( "C[7]"), X_STR( "C[8]"), X_STR( "C[9]"), 
    X_STR("C[10]"), X_STR("C[11]"), X_STR("C[12]"), X_STR("C[13]"), X_STR("C[14]"), X_STR("C[15]"), X_STR("C[16]"), X_STR("C[17]"), X_STR("C[18]"), X_STR("C[19]"),
    X_STR("C[20]"), X_STR("C[21]"), X_STR("C[22]"), X_STR("C[23]"), X_STR("C[24]"), X_STR("C[25]"), X_STR("C[26]"), X_STR("C[27]"), X_STR("C[28]"), X_STR("C[29]"),
    X_STR("C[30]"), X_STR("C[31]"), X_STR("C[32]"), X_STR("C[33]"), X_STR("C[34]"), X_STR("C[35]"), X_STR("C[36]"), X_STR("C[37]"), X_STR("C[38]"), X_STR("C[39]"),
    X_STR("C[40]"), X_STR("C[41]"), X_STR("C[42]"), X_STR("C[43]"), X_STR("C[44]"), X_STR("C[45]"), X_STR("C[46]"), X_STR("C[47]"), X_STR("C[48]"), X_STR("C[49]")
};

//------------------------------------------------------------------------------
// Description:
//      Save the entity with its components
//------------------------------------------------------------------------------        
void entity::linearSaveToBlueprintBundle( gb_blueprint::bundled_entity& Bundle ) const noexcept
{
    //
    // Set the entity guid type
    //
    Bundle.m_EntityTypeGuid = getType().getGuid();

    //
    // Copy all the component types
    //
    {
        int nComponents = [&]{ int c=0; for( auto* pComp = m_pNextComp ; pComp ; pComp = pComp->m_pNextComp, c++ )(void)nullptr; return c; }();
        Bundle.m_lComponentTypes.New( nComponents );
        nComponents = 0;
        for( auto* pComp = m_pNextComp; pComp; pComp = pComp->m_pNextComp )
        {
            auto& Entry = Bundle.m_lComponentTypes[nComponents++];
            Entry.m_gCompType   = pComp->getType().getGuid();
            Entry.m_NarrowGUID  = pComp->getNarrowGUID();
        }
    }

    //
    // Collect all properties V2
    //
    EnumProperty( [&]() -> xproperty_v2::entry&
    {
        // Just collect them all inside an static array
        return Bundle.m_lEntityPropertiesV2.append();
    }, true, xproperty_v2::base::enum_mode::SAVE );

    //
    int a = 22;
}

//------------------------------------------------------------------------------
// Description:
//      Save the entity with its components
//------------------------------------------------------------------------------        
entity& entity::linearLoadFromBlueprintBundle( guid EntityGuid, const gb_blueprint::bundled_entity& Bundle, gb_game_graph::base& GameMgr ) noexcept
{
    //
    // Create the entity 
    //
    entity& Entity  = GameMgr.CreateEntity(
        GameMgr.m_CompTypeDB.getEntry( Bundle.m_EntityTypeGuid ), 
        EntityGuid );

    //
    // Create all the components
    //
    for( int i = Bundle.m_lComponentTypes.getCount<int>()-1; i>=0; --i )
    {
        auto& Entry = Bundle.m_lComponentTypes[i];
        GameMgr.CreateComponent( 
            GameMgr.m_CompTypeDB.getEntry( Entry.m_gCompType ),
            Entity,
            Entry.m_NarrowGUID.getNarrowCount() );
    }

    //
    // Load/Set and set all the properties V2
    //
    Entity.SetProperties( [&]( const int Index ) -> const xproperty_v2::entry*
    {
        return Index < Bundle.m_lEntityPropertiesV2.getCount<int>() ? &Bundle.m_lEntityPropertiesV2[Index] : nullptr;
    });

    return Entity;
}


//------------------------------------------------------------------------------
// Description:
//          Sending a message to change the transform
//------------------------------------------------------------------------------
void entity::msgTransformSet( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) ) noexcept 
{ 
    SendMsg( [this,Callback,L2W]()
    {
        onTransformSet( L2W, Callback ); 
    });
}

//------------------------------------------------------------------------------
// Description:
//------------------------------------------------------------------------------
const xproperty_v2::table& entity::onPropertyTable( void ) const noexcept 
{
    static prop_table E( this, this, X_STR("Entity"), [&](xproperty_v2::table& E)
    {
        E.AddChildTable( t_parent::onPropertyTable() );
        E.AddProperty
        ( 
            X_STR("GUID")
            , m_Guid.m_Value
            , xproperty_v2::info::guid{ t_class_string, t_class_guid.m_Value }
            , xproperty_v2::flags::MASK_DONT_SAVE | xproperty_v2::flags::MASK_READ_ONLY 
            , X_STR("Global Unique Identifier (GUID) of the entity.")
        );
        E.AddProperty
        ( 
            X_STR("gZone")
            , m_ZoneGuid.m_Value
            , xproperty_v2::info::guid{ t_class_string, t_class_guid.m_Value }
            , xproperty_v2::flags::MASK_FORCE_SAVE | xproperty_v2::flags::MASK_READ_ONLY 
            , X_STR("GUID for the zone which this entity is located at.")
        );
        E.AddProperty<bool>
        ( 
            X_STR("isInWorld") 
            , X_STATIC_LAMBDA_BEGIN ( t_self& Class, bool& Data, const xproperty_v2::info::boolean& Details, bool isSend )
            {
                if( isSend ) Data = Class.isInWorld();
            }
            X_STATIC_LAMBDA_END
            , xproperty_v2::flags::MASK_DONT_SAVE | xproperty_v2::flags::MASK_READ_ONLY 
            , X_STR("Tells if the entity is currently in the world. FALSE is not in the world, TRUE is in the world.")
        );
    });

    return E; 
}

//------------------------------------------------------------------------------
// END
//------------------------------------------------------------------------------        
}
