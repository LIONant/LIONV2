 //----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_blueprint
{
    struct bundled_entity;
    class master;
    struct guid : xguid<bundled_entity>
    {
        using xguid<bundled_entity>::xguid;
        constexpr    guid( void )                               noexcept = default;
        x_forceconst guid( u32 GUID )                           noexcept : xguid( (u64{GUID}<<16) | 0xffff ) {}

        template< typename ...T_ARGS > 
        void AddAsProperty( xproperty_v2::table& E, xconst_str<xchar> Str, T_ARGS&& ...Args ) const noexcept;
    };
}

namespace gb_component
{

    struct zone
    {
        using guid = xguid<zone>;

    };

    //------------------------------------------------------------------------------
    // Description:
    //      The entity is the container for a collection of components for a single entity.
    //      Generally speaking you will have as many entities and game object instances.
    //      A Entity also has the GUID for an instance. This GUID together with the component type
    //      allows any component access to any other entity component.
    //------------------------------------------------------------------------------
    class entity : public base
    {
        x_object_type( entity, is_quantum_lock_free, rtti(base), is_not_copyable, is_not_movable )

    public:

        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto                 t_category_string           = X_USTR("Entity/Basic");
            x_constexprvar auto                 t_type_guid                 = gb_component::type_base::guid{ "Basic Entity" };
            x_constexprvar auto                 t_help                      = X_STR("This is a base entity with no additional behaviors at all");
            x_constexprvar def                  t_definition                = def::MASK_DEFAULT;
            using                               t_type                      = gb_component::type_entity_pool<t_self>;
        };

        x_constexprvar  auto        t_class_string                  = X_STR("Entity");
        x_constexprvar  auto        t_class_guid                    = class_guid{"Entity"};

        struct guid : public xguid<entity>
        {
            using xguid<entity>::xguid;

            template< typename ...T_ARGS > 
            x_inline void AddAsProperty( xproperty_v2::table& E, xconst_str<xchar> Str, T_ARGS&& ...Args ) const noexcept
            {
                E.AddProperty<u64>( Str, const_cast<u64&>(m_Value), xproperty_v2::info::guid( t_class_string, t_class_guid.m_Value ), std::forward<T_ARGS>(Args)... );
            }
            
            using xguid<entity>::operator == ;

        };

    public:

        x_forceinline                           entity                      ( const base_construct_info& C )                                                                noexcept;
        x_forceinline                          ~entity                      ( void )                                                                                        noexcept { m_Guid.m_Value = 0; }
        x_forceinline       void                setupBlueprintGuid          ( gb_blueprint::guid Guid )                                                                     noexcept { m_BlueprintGuid = Guid; }
        x_inline            void                msgAddToWorld               ( void )                                                                                        noexcept;
        x_inline            void                msgDestroy                  ( void )                                                                                        noexcept;
        x_inline            void                msgRemoveFromWorld          ( void )                                                                                        noexcept;
        x_forceinline       guid                getGuid                     ( void )                                                                                const   noexcept { return m_Guid; }
        x_forceinline       auto                getBlueprintGuid            ( void )                                                                                const   noexcept { return m_BlueprintGuid; }
        virtual             const xmatrix4&     getTransform                ( void )                                                                                const   noexcept { return xmatrix4::Identity(); }
        x_forceinline       void                linearTransformSet          ( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) =[](xmatrix4&D, const xmatrix4&S){D=S;} ) noexcept {onTransformSet(L2W,Callback);}
        x_incppfile         void                msgTransformSet             ( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) =[](xmatrix4&D, const xmatrix4&S){D=S;} ) noexcept;
        template< typename T >
        x_inline            T*                  getComponent                 ( gb_component::base* pFrom = nullptr )                                                const   noexcept;
        x_inline            gb_component::base* getComponent                 ( gb_component::type_base::guid Guid, gb_component::base* pFrom )                      const   noexcept;
        x_inline            gb_component::base* getComponent                 ( narrow_guid gNarrowGuid )                                                            const   noexcept 
        {
            if( getNarrowGUID() == gNarrowGuid )
                return const_cast<gb_component::base*>(static_cast<const gb_component::base*>(this));

            for( gb_component::base* pComponent = m_pNextComp; 
                                     pComponent; 
                                     pComponent = pComponent->getNextComponent() )
            { 
                if( pComponent->getNarrowGUID() == gNarrowGuid )
                    return pComponent;
            }

            return nullptr;
        }
        x_forceinline       void                linearResolve                ( void )                                                                                       noexcept;
        x_forceinline       entity::err         linearCheckResolve           ( xvector<xstring>& WarningList )                                                      const   noexcept; 
        x_incppfile         void                linearSaveToBlueprintBundle  ( gb_blueprint::bundled_entity& Bundle )                                               const   noexcept;
        x_incppfile static  entity&             linearLoadFromBlueprintBundle( guid EnityGuid, const gb_blueprint::bundled_entity& Bundle, gb_game_graph::base& GameMgr )   noexcept;
        template< typename T > void             linearPropEnumV2             (  T&&                                 Function, 
                                                                                bool                                bFullPath   = true, 
                                                                                xproperty_v2::base::enum_mode       Mode        = xproperty_v2::base::enum_mode::REALTIME ) const noexcept { EnumProperty( std::forward<T&&>(Function), bFullPath, Mode ); }
        template< typename T > void             linearPropGetV2             ( T&& Callback )                                                                               noexcept { GetProperties( std::forward<T&&>(Callback) ); }                         
        template< typename T > void             linearPropSetV2             ( T&& Callback )                                                                               noexcept { SetProperties( std::forward<T&&>(Callback) ); }                         
        x_inline            void                linearAddToWorld            ( void )                                                                                       noexcept;
        x_inline            void                linearDestroy               ( void )                                                                                       noexcept;
        x_inline            void                linearDestroyComponent      ( xowner<base&> component )                                                                    noexcept;
        x_inline            narrow_count        linearCreateNarrowCount      ( type_base::guid TypeGuid )                                                                   noexcept;
        template< typename T_COMPONENT > 
        x_inline            narrow_count        linearCreateNarrowCount      ( void )                                                                                       noexcept;

    protected:

        virtual             void                onTransformSet              ( const xmatrix4& L2W, void(*Callback)(xmatrix4&, const xmatrix4&) )                            noexcept {}
        virtual             void                onResolve                   ( void )                                                                                        noexcept override;
        virtual             err                 onCheckResolve              ( xvector<xstring>& WarningList )                                                       const   noexcept override;
        virtual             prop_table&         onPropertyTable             ( void )                                                                                const   noexcept override;

    protected:

        guid                            m_Guid          { nullptr };            // Guid of the entity
        gb_blueprint::guid              m_BlueprintGuid { nullptr };            // Guid of the blue print that was used to create this entity
        zone::guid                      m_ZoneGuid      { nullptr };            // Guid of the zone which this entity is right now

    private:

        x_inline            void                linearInternalAddComponent        ( base& Component, narrow_count NarrowGUID )                                              noexcept;

    public:

        friend class gb_game_graph::base;
        template< typename T_COMP > friend class type_pool;
    };

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    class entity_static : public entity
    {
        x_object_type( entity_static, is_quantum_lock_free, rtti(entity), is_not_copyable, is_not_movable )

    public:

        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_USTR("Entity/Static");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Static Entity" };
            x_constexprvar auto             t_help                      = X_STR("This is a basic static entity and it provides as its feature a constant transform");
            x_constexprvar def              t_definition                = def::MASK_CONSTANT;
            using                           t_type                      = gb_component::type_entity_pool<t_self>;
        };

    public:
        virtual         const xmatrix4&         getTransform                ( void )                            const   noexcept override { return m_L2W; }

    protected:

        x_forceinline                           entity_static               ( const base_construct_info& C )            noexcept : t_parent{ C }{}
        virtual         prop_table&             onPropertyTable             ( void )                                                                                const   noexcept override
        {
            static prop_table E( this, this, X_STR("Static"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( entity::onPropertyTable() );
                E.AddProperty<xvector3>( 
                    X_STR("Scale")
                    , X_STATIC_LAMBDA_BEGIN (entity_static& Class, xvector3& Data, const xproperty_v2::info::vector3& Details, bool isSend)
                    {
                        if (isSend)
                        {
                            Data = Class.m_L2W.getScale();
                        }
                        else
                        {
                            const auto Scale = Data.getMax( xvector3(0.05f) ).getMin( xvector3(1000.0f) );
                            const auto Rot   = Class.m_L2W.getRotationR3();
                            const auto Trans = Class.m_L2W.getTranslation();
                            Class.m_L2W.setup( Scale, Rot, Trans );
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("World scale for a Static entity. The units are in unit-percentage.")
                );

                E.AddProperty<xvector3>( 
                    X_STR("Position")
                    , X_STATIC_LAMBDA_BEGIN (entity_static& Class, xvector3& Data, const xproperty_v2::info::vector3& Details, bool isSend)
                    {
                        if (isSend)     Data = Class.m_L2W.getTranslation();
                        else            Class.m_L2W.setTranslation( Data );
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("World position for a Static entity. The units are in centimeters.")
                );
                E.AddProperty<xradian3>( 
                    X_STR("Rotation")
                    , X_STATIC_LAMBDA_BEGIN (entity_static& Class, xradian3& Data, const xproperty_v2::info::radian3& Details, bool isSend)
                    {
                        if (isSend)     Data = Class.m_L2W.getRotationR3();
                        else
                        {
                            auto    Scale   = Class.m_L2W.getScale();
                            Class.m_L2W.setup( Scale, Data, Class.m_L2W.getTranslation() );
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("World rotation for a Static entity. The units are degrees. Order of the rotations are in Roll->Pitch->Yaw (ZXY).")
                );
            }); 
            return E; 
        }

        virtual void onTransformSet ( const xmatrix4& L2W, void(*Callback)(xmatrix4&, const xmatrix4&) ) noexcept override
        { 
            Callback( m_L2W, L2W );
        }

    protected:

        xmatrix4 m_L2W { xmatrix4::Identity() };
    };

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    class entity_dynamic : public entity
    {
        x_object_type( entity_dynamic, is_quantum_lock_free, rtti(entity), is_not_copyable, is_not_movable )

    public:

        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_USTR("Entity/Dynamic");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Dynamic Entity" };
            x_constexprvar auto             t_help                      = X_STR("This is a basic dynamic entity and it provides as its feature a mutable transform");
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
            using                           t_type                      = gb_component::type_entity_pool<t_self>;
        };

    public:

        struct mutable_data : public entity::mutable_data
        {
            x_object_type( mutable_data, is_linear, rtti(entity::mutable_data) )
            virtual void onUpdateMutableData( const t_mutable_base& Src ) noexcept override { *this = reinterpret_cast<const mutable_data&>(Src); }
            xmatrix4 m_L2W{ xmatrix4::Identity() };
        };

    public:

        virtual             const xmatrix4&     getTransform                ( void )                            const   noexcept override { return getT0Data<mutable_data>().m_L2W; }

    protected:
        x_forceinline                           entity_dynamic              ( const base_construct_info& C )            noexcept : t_parent{ C }{}

        virtual             prop_table&         onPropertyTable             ( void )                                                                                const   noexcept override
        {
            static prop_table E( this, this, X_STR("Dynamic"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( entity::onPropertyTable() );
                E.AddProperty<xvector3>( 
                    X_STR("Scale")
                    , X_STATIC_LAMBDA_BEGIN (entity_dynamic& Class, xvector3& Data, const xproperty_v2::info::vector3& Details, bool isSend)
                    {
                        auto& L2W = Class.getT1Data<mutable_data>().m_L2W;
                        if (isSend)
                        {
                            Data = L2W.getScale();
                        }
                        else
                        {
                            const auto Scale = Data.getMax( xvector3( 0.05f, 0.05f, 0.05f ) ).getMin( xvector3( 100000.0f, 100000.0f, 100000.0f ) );
                            const auto Rot   = L2W.getRotationR3();
                            const auto Trans = L2W.getTranslation();
                            L2W.setup( Scale, Rot, Trans );
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("World scale for a Dynamic entity. The units are in unit-percentage.")
                );
                E.AddProperty<xvector3>( 
                    X_STR("Position")
                    , X_STATIC_LAMBDA_BEGIN (entity_dynamic& Class, xvector3& Data, const xproperty_v2::info::vector3& Details, bool isSend)
                    {
                        auto& L2W = Class.getT1Data<mutable_data>().m_L2W;
                        if (isSend)     Data = L2W.getTranslation();
                        else            L2W.setTranslation(Data);
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("World position for a Dynamic entity. The units are in centimeters.")
                );
                E.AddProperty<xradian3>( X_STR("Rotation")
                    , X_STATIC_LAMBDA_BEGIN (entity_dynamic& Class, xradian3& Data, const xproperty_v2::info::radian3& Details, bool isSend)
                    {
                        auto& L2W = Class.getT1Data<mutable_data>().m_L2W;
                        if (isSend)     Data = L2W.getRotationR3();
                        else
                        {
                            auto    Scale   = L2W.getScale();
                            L2W.setup( Scale, Data, L2W.getTranslation() );
                        }
                    }
                    X_STATIC_LAMBDA_END
                    , X_STR("World rotation for a Dynamic entity. The units are degrees. Order of the rotations are in Roll->Pitch->Yaw (ZXY).")
                );
            }); 
            return E; 
        }

        virtual void onTransformSet ( const xmatrix4& L2W, void(*Callback)(xmatrix4&, const xmatrix4&) ) noexcept override  
        { 
            Callback( getT1Data<mutable_data>().m_L2W, L2W );
        }
    };
}

