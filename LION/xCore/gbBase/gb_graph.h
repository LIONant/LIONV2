//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//       Announce the public function so that we can friend it later
//------------------------------------------------------------------------------
void ScriptMainRegistration( gb_game_graph::base& pBase );

//------------------------------------------------------------------------------
// Description:
//      This class is part of the game graph building
//      This is the base class for all the game components.
//      The class is a quantum object so all relevant rules apply
//      A component has two main states. In the world where the game will try to update it.
//      Out from the world where you can do things like build entities etc, because they are
//      thought to be in a linear world.
//      Every component class needs to have an accompany gb_component_type_base
//------------------------------------------------------------------------------
namespace gb_game_graph
{
    class sync_point : public x_trigger_base
    {
        x_object_type( sync_point, is_quantum_lock_free, rtti_restart, is_not_copyable, is_not_movable )

    public:

        using               t_parent        = x_trigger_base;
        x_constexprvar bool t_is_sync_point = true;
        using               guid            = xguid<t_self>;
        
    public:
        
                                                    sync_point                              ( void )                                        noexcept = default;
        x_orforceconst      u32                     getICurrent                             ( void )                                const   noexcept { x_assert_quantum( m_Debug_LQ ); return static_cast<u32>(m_iCurrent)&1; }
        x_forceconst        guid                    getGuid                                 ( void )                                const   noexcept { return m_Guid;                                       }
        x_inline            sync_point&             setup                                   ( guid Guid, const char* pUniqueName )          noexcept;
        x_inline            void                    setupGraphMgrWillNotifyMe               ( gb_component_mgr::base& Mgr )                 noexcept;
        x_inline            void                    setupGraphManagersToRunsAfterTriggered  ( gb_component_mgr::base& Manager )             noexcept;
        virtual                                    ~sync_point                              ( void )                                        noexcept override;
        virtual             int                     getJobCount                             ( void )                                const   noexcept override { return m_nManagersToBeRetrigger; }
        x_inline            auto&                   getJob                                  ( int Index )                                   noexcept { x_assert( Index < m_nManagersToBeRetrigger ); return *m_ManagersToBeRetrigger[Index]; }

    protected:
        
        enum : u32
        {
            MAX_JOBS                 = 16,
            MAX_PREVIOUS_SYNC_POINTS = 8
        };
        
    protected:
        
        virtual             void                    qt_onTriggered                          ( void )                                        noexcept override;
        x_forceinline static void                   EndOfFrameCleanUpForManager             ( gb_component_mgr::base& Mgr )                 noexcept{ Mgr.onEndOfFrameCleanUp(); }

    protected:
        
        struct
        {
            x_debug_linear_quantum  m_LQ {};
        }                                                       m_Debug                     {};
        
        s32                                                     m_iCurrent                  { 0 };
        s32                                                     m_nManagersToBeRetrigger    { 0 };
        s32                                                     m_nMgrWillNotifyMe          { 0 };
        guid                                                    m_Guid                      { guid::RESET };
        xarray<gb_component_mgr::base*, MAX_JOBS>               m_ManagersToBeRetrigger     {{ nullptr }};
        xarray<char,256>                                        m_Name                      {};
    };

    //---------------------------------------------------------------------------------
    // GAME GRAPH
    //---------------------------------------------------------------------------------
    class base : public x_ll_object_harness< base, x_job<1> >
    {
        x_object_type( base, is_quantum_lock_free, rtti_restart, is_not_copyable, is_not_movable )

    public:
        
        using t_parent  = x_ll_object_harness< base, x_job<1> >;

        //---------------------------------------------------------------------------
        
        struct linear_types
        {
            xrandom_small   m_SmallRnd {};
        };

        //---------------------------------------------------------------------------
        
        class sync_point_end : public sync_point
        {
            x_object_type( sync_point_end, is_quantum_lock_free, rtti(sync_point), is_not_copyable, is_not_movable )

        public:
            
                                            sync_point_end              ( void ) noexcept = default;
            
        protected:
            
            virtual void                qt_onTriggered                 ( void ) noexcept override
            {
                t_parent::qt_onTriggered();
                
                // Flush pending messages from managers
                {
                    x_lk_guard_get_as_const( m_pGameGraph->m_CompManagerDB.m_Vector, ManagerLinearList )
                    x_job_block JobBlock( X_WSTR("EndOfFrameCleanup") );
                    for( auto& pMgr : ManagerLinearList ) JobBlock.SubmitJob( [ this, pMgr ]()
                    {
                        sync_point::EndOfFrameCleanUpForManager( *pMgr->m_pEntry );
                    });
                    JobBlock.Join();   
                }

                // Let the user react to the end frame
                m_pGameGraph->onEndFrame();

                // continue to work
                if( m_pGameGraph->m_bLoop ) 
                {
                    g_context::get().m_Scheduler.AddJobToQuantumWorld( *m_pGameGraph );
                }
                else
                {
                    g_context::get().m_Scheduler.MainThreadStopWorking();
                }
            }
            
            void                setup                        ( base* pGameGraph ) { m_pGameGraph = pGameGraph; }
            
        protected:
            
            base* m_pGameGraph = nullptr;
            
        protected:
            
            friend class base;
        };
        
        //---------------------------------------------------------------------------
        
        class sync_point_start : public sync_point
        {
            x_object_type( sync_point_start, is_quantum_lock_free, rtti(sync_point), is_not_copyable, is_not_movable )

        public:
            
                                            sync_point_start            ( void ) noexcept = default;
            
        protected:
            
            virtual void qt_onTriggered( void ) noexcept override
            {
                m_NotificationCounter++;
                t_parent::qt_onTriggered();
            }
            
            void setup ( gb_game_graph::base* pGameGraph )  noexcept
            {
                pGameGraph->onAppenTrigger( *this );
                m_NotificationCounter++;
                m_nMgrWillNotifyMe++;
            }
            
        protected:
            
            friend class base;
        };

        using errors = x_errdef::errors;
        using err    = x_errdef::err;
        
    public:
        
        x_inline                                base                ( xuptr MaxEntities )                                                           noexcept;
        x_inline        void                    Initialize          ( bool bEditorMode )                                                            noexcept;  
        x_forceinline   auto&                   getMsgPool          ( void )                                                                        noexcept { return m_MsgPool; }
        x_forceinline   u64                     getGameFrame        ( void )                                                                const   noexcept;
        
        template< typename T_COMPONENT_CLASS >
        x_inline        T_COMPONENT_CLASS&      CreateComponent     ( gb_component::entity&             Entity,
                                                                      gb_component::narrow_count        NarrowGUID = gb_component::narrow_count{nullptr} ) noexcept;

        x_inline        gb_component::base&     CreateComponent     ( gb_component::type_base&          ComponentType, 
                                                                      gb_component::entity&             Entity,
                                                                      gb_component::narrow_count        NarrowGUID = gb_component::narrow_count{nullptr} ) noexcept;

        template< typename T_ENTITY_CLASS >
        x_inline        T_ENTITY_CLASS&         CreateEntity        ( gb_component::entity::guid      InstanceGuid )                                noexcept;

        x_inline        gb_component::entity&   CreateEntity        ( gb_component::type_base&          EntityType, 
                                                                      gb_component::entity::guid      InstanceGuid )                                noexcept;

        x_incppfile     gb_blueprint::master&   CreateBlueprint     ( gb_blueprint::master::guid Guid = gb_blueprint::master::guid{ nullptr } )     noexcept;

        template< typename T_ENTITY_CLASS = gb_component::entity>
        x_inline        T_ENTITY_CLASS*         findEntity           ( gb_component::entity::guid Guid )                                   const   noexcept;
        
        void                                    SaveEntity          ( gb_component::entity& Entity, xtextfile& File )                     const     noexcept;
        gb_component::entity&                   LoadEntity          ( xtextfile& File )                                                             noexcept;
        void                                    linearSaveGame      ( xtextfile& TextFile )                                                         noexcept;
        void                                    linearDeleteAllEntities( void )                                                                     noexcept;
        x_incppfile         err                 linearLoadGame      ( xwstring FileName, bool AddToWorld = true )                                   noexcept;
        x_forceinline       void                setLoopMode         ( bool OnOff )                                                                  noexcept { m_bLoop = OnOff; } 

    public:
        
        using type_dbase            = x_ll_dbase<gb_component::type_base>;
        using sync_dbase            = x_ll_dbase<gb_game_graph::sync_point>;
        using mgr_dbase             = x_ll_dbase<gb_component_mgr::base>;
        using const_data_dbase      = x_ll_dbase<gb_component::const_data>;
        using blueprint_dbase       = x_ll_dbase<gb_blueprint::master>;
        using entity_hash           = x_ll_mrmw_hash<gb_component::entity*, u64, x_lk_spinlock::do_nothing>;
        using const_data_type_dbase = x_ll_dbase<gb_component::const_data::type>;

        type_dbase              m_CompTypeDB        {};
        sync_dbase              m_SyncDB            {};
        mgr_dbase               m_CompManagerDB     {};
        const_data_type_dbase   m_ConstDataTypeDB   {};
        const_data_dbase        m_ConstDataDB       {};
        blueprint_dbase         m_BlueprintDB       {};
        entity_hash             m_EntityDB          {};

        x_message::event<gb_component::entity*>     m_eventNewEntity;

        x_forceinline void              setTimeScale        ( float Scale ) noexcept { m_TimeScale = Scale; }
        x_forceinline float             getDeltaTime        ( void )        noexcept { return m_DeltaTime * m_TimeScale; }
        x_forceinline bool              isEditorMode        ( void ) const noexcept { return m_isEditorMode; }
        x_forceinline void              UseLinearTypes      ( xfunction<void(linear_types& LTypes)> Func ) noexcept { x_lk_guard(m_LinearLock); Func(m_LinearTypes); }
        x_forceinline void              RenderEditorFrame   ( void ) { onRenderEditorFrame(); }

    protected:

         using script_interface     = int(*)( gb_game_graph::base* pBase, g_context* pContext );

         struct script
         {
            xguid<script>           m_Guid          {};
            u64                     m_DLL           { 0 };
            const char*             m_pFileName     { nullptr };
            script_interface        m_pInterface    { nullptr };  
         };

    protected:
        
        x_inline    static  void                        InitializeGraphConnection           ( sync_point& From, gb_component_mgr::base& Mgr, sync_point& End )              noexcept;
                            void                        ImportScript                        ( const char* pPath )                                                           noexcept;

        virtual             void                        qt_onRun                            ( void )                                                                        noexcept override final;
        virtual             void                        qt_onDone                           ( void )                                                                        noexcept override final;
        virtual             void                        onInitialize                        ( void )                                                                        noexcept {}
        virtual             void                        onBeginFrame                        ( void )                                                                        noexcept {}
        virtual             void                        onEndFrame                          ( void )                                                                        noexcept;
        virtual             void                        onRenderEditorFrame                 ( void )                                                                        noexcept {}

        template< typename T_COMP >
        x_inline            typename T_COMP::t_descriptors::t_type&    RegisterComponentAndDependencies    ( gb_component_mgr::base& ComponentMgr, gb_game_graph::sync_point& SyncPoint )  noexcept;
        x_inline            void                        qt_onEntityDestroy                  ( const gb_component::entity& Entity )                                          noexcept;

    protected:

        using blueprint_pool = x_ll_page_pool_jitc<gb_blueprint::master,128>;

        using lock = x_lk_spinlock::base< x_lk_spinlock::non_reentrance >;
         
    protected:

        u64                                         m_nFrames           {    0 };
        f32                                         m_DeltaTime         {0};
        f32                                         m_TimeScale         {1};
        sync_point_end                              m_EndSyncPoint      {};
        sync_point_start                            m_StartSyncPoint    {};
        x_ll_circular_pool                          m_MsgPool           { 1024 };
        bool                                        m_bLoop             { true };
        xtimer                                      m_GameTimer         {};
        xtimer                                      m_Stats_Timer       {};
        xtimer::nanoseconds                         m_Stats_LogicTime   {};
        xndptr<script>                              m_lScripts          {};
        blueprint_pool                              m_BlueprintPool     {};
        bool                                        m_isEditorMode      { false };
        lock                                        m_LinearLock        {};
        linear_types                                m_LinearTypes       {};
        x_ll_circular_pool                          m_MsgAllocPool      {};
        xuptr                                       m_MaxEntities;

    protected:

        friend t_parent;
        friend class gb_component_mgr::base;
        friend void ::ScriptMainRegistration( gb_game_graph::base& );
        template< typename T_COMP > friend class gb_component::type_entity_pool;
    };
};


#if _X_TARGET_DYNAMIC_LIB
    #if _X_TARGET_WINDOWS
        #define ScriptMain( A )                                                                             \
        fake(){}                                                                                            \
        __declspec(dllexport) bool ScriptInterface( gb_game_graph::base* pBase, g_context* pContext ) {     \
            g_context::Import( *pContext );                                                                 \
            ScriptMainRegistration( *pBase );                                                               \
            return true; }                                                                                  \
        BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved ) {            \
            switch (ul_reason_for_call) {                                                                   \
            case DLL_PROCESS_ATTACH: case DLL_THREAD_ATTACH: case DLL_THREAD_DETACH: break;                 \
            case DLL_PROCESS_DETACH: g_context::Kill(); break;                                              \
            }                                                                                               \
            return TRUE; }                                                                                  \
        void ScriptMainRegistration( A )
    #elif _X_TARGET_MAC
        #define ScriptMain( A )                                                                             \
        fake( void ){}                                                                                      \
        extern "C" bool ScriptInterface( gb_game_graph::base* pBase, g_context* pContext ) {                \
            g_context::Import( *pContext );                                                                 \
            ScriptMainRegistration( *pBase );  return true; }                                               \
        void ScriptMainRegistration( A )
    #endif
#endif

