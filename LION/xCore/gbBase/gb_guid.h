//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#pragma once

namespace gb_component
{
    template< typename T >
    struct partial_guid
    {
        const u32 m_Value;

        x_forceconst                            partial_guid        ( void )                                    noexcept = default;
        x_forceconst explicit                   partial_guid        ( u32 Value )                               noexcept : m_Value{ Value } {}
        x_forceconst                            partial_guid        ( const char* pStr )                        noexcept : m_Value{ x_constStrCRC32(pStr) } {}

        x_forceconst bool                       operator ==         ( const partial_guid X )            const   noexcept { return m_Value == X.m_Value; }
        x_forceconst bool                       operator !=         ( const partial_guid X )            const   noexcept { return m_Value != X.m_Value; }
        x_forceconst bool                       isValid             ( void )                            const   noexcept { return !!m_Value;            }
    };

    using class_guid = partial_guid<struct tag>;

    template< typename T_NAMED >
    using named_guid = partial_guid<struct tag>;

    template< typename T_TAG, typename T_NAMED, u32 T_CLASS >
    struct extended_guid : xguid<struct tag>
    {

        x_forceconst                        extended_guid   ( void )                                                         noexcept = default;
        x_forceconst explicit               extended_guid   ( u64 RawData )                                                  noexcept : xguid{ RawData } {}
        x_forceconst explicit               extended_guid   ( u32 NamedCRC )                                                 noexcept : xguid{ NamedCRC, T_CLASS } {}
        x_forceconst                        extended_guid   ( class_guid ClassGuid, named_guid<T_NAMED> NamedCRC )           noexcept : xguid{ NamedCRC.m_Value, ClassGuid.m_Value } {}
        x_forceconst                        extended_guid   ( named_guid<T_NAMED> NamedCRC )                                 noexcept : xguid{ NamedCRC.m_Value, class_guid{ T_CLASS } } {}
        x_forceconst                        extended_guid   ( nullptr_t )                                                    noexcept : xguid{ nullptr } {}
        x_forceconst                        extended_guid   ( const char* pStr )                                             noexcept : extended_guid{ class_guid{ T_CLASS }, { pStr } } {}
        x_forceconst named_guid<T_NAMED>    getNameCRC      ( void )                                                 const   noexcept { return named_guid<T_NAMED>{static_cast<u32>(m_Value>>32)}; }
        x_forceconst class_guid             getClassGuid    ( void )                                                 const   noexcept { return class_guid{static_cast<u32>(m_Value)    }; }
    };
}