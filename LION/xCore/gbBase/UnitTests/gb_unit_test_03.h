//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <random>

//---------------------------------------------------------------------------------
// Test 3
// This test is about sending messages, and derived components
//---------------------------------------------------------------------------------
namespace gb_unit_test_03
{
    class my_game_graph;
    class my_derived_entity;
    
    xarray<gb_component::entity::guid, 10000>         s_lEntity;
    x_atomic<s32>                                       s_EntityCount;;

    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class my_entity : public gb_component::entity
    {
        x_object_type( my_entity, is_quantum_lock_free, rtti(gb_component::entity), is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string               = X_USTR("Entity/Unittest03 Base");
            x_constexprvar auto             t_type_guid                     = gb_component::type_base::guid{ "Unittest03-B" };
            x_constexprvar auto             t_help                          = X_STR("This entity should never be use for any production code");
            x_constexprvar def              t_definition                    = def::MASK_DEFAULT;
            using                           t_type                          = gb_component::type_entity_pool<t_self>;
        };

    public: // --- public functions ---
        
        my_entity( const base_construct_info& C ) : t_parent( C ) {}
        
    protected: // --- class hierarchy functions ---
        
        virtual void onExecute( void ) noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert_linear( m_Debug_LQMsg );
            t_parent::onExecute();
        }
    };

    //---------------------------------------------------------------------------------
    // BUILDING MY DERIVED ENTITY
    //---------------------------------------------------------------------------------
    class my_derived_entity : public my_entity
    {
        x_object_type( my_derived_entity, is_quantum_lock_free, rtti(my_entity), is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string               = X_USTR("Entity/Unittest03 Derived");
            x_constexprvar auto             t_type_guid                     = gb_component::type_base::guid{ "Unittest03-D" };
            x_constexprvar auto             t_help                          = X_STR("This entity should never be use for any production code");
            x_constexprvar def              t_definition                    = def::MASK_DEFAULT;
            using                           t_type                          = gb_component::type_entity_pool<t_self>;
        };

    public: // --- public functions ---

        void msgDoSomething( void )
        {
            SendMsg( [&]{ onMyMessage(); } );
        }

    protected: // --- class hierarchy functions ---
        
        my_derived_entity( const base_construct_info& C ) noexcept : t_parent( C )
        {
            // Generate random number from 5 to 10000-1
            std::random_device                  rd;
            std::mt19937                        mt(rd());
            std::uniform_int_distribution<int>  dist(5, 10000 );
            m_Count = dist(mt);
        }

        virtual void onMyMessage( void ) noexcept
        {
            m_Count++;
            if( (m_Count%2000) == 0 ) X_LOG("Hello\n");
        }
        
        virtual void onExecute( void ) noexcept override
        {
            // Test that I can send messages to my self...
            // Note that doing so its stupid for two reasons:
            //   a) Any messages send while inside our class will have to wait for one of this conditions:
            //      1) until the end of all the other messages
            //      2) until the component sync point 
            //   b) It is a waste of computational resources
            msgDoSomething();

            // If for some reason you really want to send the message to your self just call the function directly 
            onMyMessage();

            // Test sending messages to other components 
            const guid Guid { s_lEntity[ rand()%s_EntityCount.load( std::memory_order_relaxed) ] };
            
            auto pEntity = m_GInterface.m_GameMgr.findEntity<my_derived_entity>( Guid );
            pEntity->msgDoSomething();

            // We let the parent deal with whatever he wants to do
            t_parent::onExecute();                
        }
        
    protected:  // --- class hierarchy hidden variables ---
        
        int m_Count = 0;
    };
    
    //---------------------------------------------------------------------------------
    // MY GAME MANAGER
    //---------------------------------------------------------------------------------
    class my_game_graph final : public gb_game_graph::base
    {
        x_object_type( my_game_graph, is_quantum_lock_free, rtti(gb_game_graph::base), is_not_copyable, is_not_movable )

    public: // --- class constructor ---

        my_game_graph( void ) noexcept : t_parent( 20000 ) {}

    protected: // --- class types ---

        using entity_mgr = gb_component_mgr::base;

    protected: // --- class hierarchy functions ---
        
        virtual void onInitialize( void ) noexcept override
        {
            //
            // Notify our parent about initialization
            //
            t_parent::onInitialize();

            //
            // Register Mgrs
            //
            m_CompManagerDB.RegisterButRetainOwnership( m_EntityMgr );
            
            //
            // Build the game graph
            //
            InitializeGraphConnection( m_StartSyncPoint, m_EntityMgr, m_EndSyncPoint );
            
            //
            // Register all the component types
            //
            RegisterComponentAndDependencies<my_entity>( m_EntityMgr, m_EndSyncPoint );
            RegisterComponentAndDependencies<my_derived_entity>( m_EntityMgr, m_EndSyncPoint );
            
            //
            // Create a bunch of entities
            //
            const int nEntities = 1000;
            s_EntityCount.store( nEntities );

            for( xuptr i = 0; i < nEntities; i++ )
            {
                const my_derived_entity::guid Guid( my_derived_entity::guid::RESET );  
                s_lEntity[i] = Guid;
                CreateEntity<my_derived_entity>( Guid ).linearAddToWorld();                   

                if( (i%1000)==0)std::cout << "Created: " << i << " Instances\n";
            }

            int a = 0;
        }
        
        virtual void onEndFrame( void ) noexcept override
        {
            static int x=0;
            gb_game_graph::base::onEndFrame();
            x++;
            if( (x%60)==0 )
            {
                std::cout << "Frame #" << m_nFrames << " FrameMS: " << xtimer::ToMilliseconds( m_Stats_LogicTime ) << "\n";
            }
            
            //
            // We will exit when we do 1000 frames
            //
            if( x > 60*1000 || m_bLoop == false )
            {
                m_bLoop = false;
                g_context::get().m_Scheduler.MainThreadStopWorking();
            }
        }

    protected:

        entity_mgr  m_EntityMgr { entity_mgr::guid{ "EntityMgr" }, *this, X_USTR("EntityMgr") };
    };
    
    //---------------------------------------------------------------------------------
    // Test
    //---------------------------------------------------------------------------------
    void Test( void )
    {
        my_game_graph GameMgr;
        GameMgr.Initialize(false);
        g_context::get().m_Scheduler.AddJobToQuantumWorld( GameMgr );
        g_context::get().m_Scheduler.MainThreadStartsWorking();
    }
};

