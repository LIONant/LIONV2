//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <random>

//---------------------------------------------------------------------------------
// Test 1 - Basic registration and execution one component and a very simple graph
//---------------------------------------------------------------------------------
namespace gb_unit_test_01
{
    class my_game_graph;
    
    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class my_entity : public gb_component::entity
    {
        x_object_type( my_entity, is_quantum_lock_free, rtti(gb_component::entity), is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_USTR("Entity/Unitest01");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Unitest01Entity" };
            x_constexprvar auto             t_help                      = X_STR("This is an example render component used for unit testing");
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
            using                           t_type                      = gb_component::type_entity_pool<t_self>;
        };
        
    protected: // --- class hierarchy types ---

        struct mutable_data : t_parent::mutable_data
        {
            virtual void onUpdateMutableData( const t_mutable_base& Src ) noexcept override
            {
                const mutable_data& NewSrc = reinterpret_cast<const mutable_data&>(Src);
                // copy the hold thing
                *this = NewSrc;
            }
            
            float m_Velocity=0;
        };
        
    protected: // --- class hierarchy functions ---
        
        my_entity( const base_construct_info& C ) noexcept : t_parent( C )
        {
            // Generate random number from 5 to 1000-1
            std::random_device                  rd;
            std::mt19937                        mt(rd());
            std::uniform_int_distribution<int>  dist(5, 1000 );
            m_Death = dist(mt);
        }

        virtual void onExecute( void ) noexcept override
        {
            t_parent::onExecute();
            
            //
            // Check mutable data
            //
            mutable_data& T1 = getT1Data<mutable_data>();
            
            T1.m_Velocity--;
            m_VelocityTester--;
            x_assert( T1.m_Velocity == m_VelocityTester );
            x_assert( getT0Data<mutable_data>().m_Velocity == (m_VelocityTester + 1) );

            //
            // Die when we run out of health
            //
            if( --m_Death == 0 )
            {
                msgDestroy();
                m_GInterface.m_GameMgr.CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ) ).msgAddToWorld();
            }
        }
        
    protected:  // --- class hierarchy hidden variables ---
        
        int m_Death;
        float m_VelocityTester=0;
    };
    
    //---------------------------------------------------------------------------------
    // MY GAME MANAGER
    //---------------------------------------------------------------------------------
    class my_game_graph final : public gb_game_graph::base
    {
        x_object_type( my_game_graph, is_quantum_lock_free, rtti(gb_game_graph::base), is_not_copyable, is_not_movable )

    public: // --- class constructor ---

        my_game_graph( void ) noexcept : t_parent( 20000 ) {}
     
    protected: // --- class types ---

        using entity_mgr = gb_component_mgr::base;
       
    protected: // --- class hierarchy functions ---
         
        virtual void onInitialize( void ) noexcept override
        {
            //
            // Notify our parent about initialization
            //
            t_parent::onInitialize();
            
            //
            // Register Mgrs
            //
            m_CompManagerDB.RegisterButRetainOwnership( m_EntityMgr );

            //
            // Build the game graph
            //
            InitializeGraphConnection( m_StartSyncPoint, m_EntityMgr, m_EndSyncPoint );
            
            //
            // Register all the component types
            //
            RegisterComponentAndDependencies<my_entity>( m_EntityMgr, m_EndSyncPoint );

            //
            // Create a bunch of entities
            //
            const int nEntities = 10000; 
            for( int i = 0; i < nEntities; i++ )
            {
                CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ) ).linearAddToWorld();

                if( (i%1000) == 0 ) std::cout << "Created: " << i << " Instances\n";
            }
        }
        
        virtual void onEndFrame( void ) noexcept override
        {
            static int x=0;

            t_parent::onEndFrame();
            
            x++;
            if( (x%60)==0 )
            {
                std::cout << "Frame #" << m_nFrames << " FrameMS: " << xtimer::ToMilliseconds( m_Stats_LogicTime ) << "\n";
            }
            
            //
            // We will exit when we do 1000 frames
            //
            if( x > 60*100 )
            {
                m_bLoop = false;
                g_context::get().m_Scheduler.MainThreadStopWorking();
            }
        }

    protected:

        entity_mgr  m_EntityMgr { entity_mgr::guid{ "EntityMgr" }, *this, X_USTR("EntityMgr") };
    };
    
    //---------------------------------------------------------------------------------
    // Test
    //---------------------------------------------------------------------------------
    void Test( void )
    {
        my_game_graph GameMgr;
        GameMgr.Initialize(false);
        g_context::get().m_Scheduler.AddJobToQuantumWorld( GameMgr );
        g_context::get().m_Scheduler.MainThreadStartsWorking();
    }
};

