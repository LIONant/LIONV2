                                                    //----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <random>
#include <stdlib.h>

struct i_some_interface : gb_component::interface_base
{
    static const gb_component::interface_type& getType( void )      
    {                                                           
        x_constexprvar gb_component::interface_type EventType         
        {                                                       
            X_STR("SomeInterface"),                            
            gb_component::interface_type::guid{ "SomeInterface" }, 
            X_STR("Help for SomeInterface")                                         
        };                                                      
        return EventType;                                       
    }      

    virtual void DoSomething(void) = 0;
};

//---------------------------------------------------------------------------------
// Test 5 
//---------------------------------------------------------------------------------
namespace gb_unit_test_05
{
    class my_game_graph;
    
    GB_COMPONENT_EVENT_TYPE_DEF( event_Activate,   "Activate", "This event will be trigger when the entity activates", bool );
    GB_COMPONENT_EVENT_TYPE_DEF( event_Deactivate, "Deactivate", "This event will be trigger when the entity activates", bool );

    //---------------------------------------------------------------------------------
    // GENERIC COMPONENT
    //---------------------------------------------------------------------------------
    class my_generic_component : public gb_component::base, public i_some_interface
    {
        x_object_type( my_generic_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

        virtual void DoSomething(void) override {}

    public: // --- class traits ---

        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_USTR("Generic/Unittest05");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Unittest05-Generic" };
            x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
            using                           t_type                      = gb_component::type_pool<t_self>;
        };
        

        void msgActivate(bool bActivate) {}

        struct events : public gb_component::events
        {
            x_object_type( events, is_linear, rtti(gb_component::events), is_not_copyable, is_not_movable )

            events(void) = default;
            event_Activate      m_Active;
            event_Deactivate    m_Deactivate;

            struct table : t_parent::table 
            {
                const definition m_Active           { offsetof( events, m_Active ),                              X_STR("Activate"),       guid{"Activate"},       event_Activate::getType()   };
                const definition m_Deactivate       { offsetof( events, m_Deactivate ),                          X_STR("Deactivate"),     guid{"Deactivate"},     event_Deactivate::getType() };
                const definition m_SomeInterface    { x_offsetof_base<i_some_interface, my_generic_component>(), X_STR("iSomeInterface"), guid{"iSomeInterface"}, i_some_interface::getType() };
            };
        };

        gb_component::interface_ref<i_some_interface> m_Interfaced;
        struct delegates : public gb_component::delegates
        {
            x_object_type( delegates, is_linear, rtti(gb_component::delegates), is_not_copyable, is_not_movable )

            event_Activate::delegate<my_generic_component>  m_Active { &my_generic_component::msgActivate };
            event_Activate::delegate<my_generic_component>  m_Active2{ &my_generic_component::msgActivate };
            event_Activate::delegate<my_generic_component>  m_Active3{ &my_generic_component::msgActivate };

            delegates(void)=default;

            struct table : t_parent::table
            {
                definition  m_Active    { offsetof( delegates, m_Active ),                  X_STR("onActivate"),        decltype(delegates::m_Active)::t_event::getType()  };
                definition  m_Active2   { offsetof( delegates, m_Active2 ),                 X_STR("onActivate1"),       decltype(delegates::m_Active2)::t_event::getType() };
                definition  m_Active3   { offsetof( delegates, m_Active3 ),                 X_STR("onActivate2"),       decltype(delegates::m_Active3)::t_event::getType() };
                definition  m_Interface { offsetof( my_generic_component, m_Interfaced ),   X_STR("rSomeInterface"),     i_some_interface::getType() };
            };
        };

    protected: // --- class hierarchy functions ---
        
        my_generic_component( const base_construct_info& C ) noexcept : t_parent( C ) {}

        virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
        { 
            static prop_table E( this, this, X_STR("Unittest05-Generic"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( t_parent::onPropertyTable() );
                E.AddProperty( X_STR("HiddenData"), m_HiddenData, xproperty_v2::info::s32( 0, 10 ), X_STR("Simple Variable") );
            });

            return E; 
        }

    protected: // --- class hierarchy functions ---
        
        int                         m_HiddenData { 3 };
    };

    //---------------------------------------------------------------------------------
    // RENDER COMPONENT
    //---------------------------------------------------------------------------------
    class my_render_component : public gb_component::base
    {
        x_object_type( my_render_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

    public: // --- class traits ---

        struct const_data : public gb_component::base::const_data
        {
            x_object_type( const_data, is_linear, rtti(gb_component::base::const_data), is_not_copyable, is_not_movable )

            x_constexprvar type t_type = 
            { 
                static_cast<const_data*>(nullptr),
                "gb_unit_test_05::my_render_component::const_data",
                X_STR("gb_unit_test_05::my_render_component"), 
                X_STR("gb_unit_test_05 Simple constant test")
            };

            virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
            { 
                static prop_table E( this, X_STR("Unittest05-constRender"), [&](xproperty_v2::table& E)
                {
                    E.AddChildTable( t_parent::onPropertyTable() );
                    E.AddProperty( X_STR("ShareColor"), m_ShareColor, X_STR("this is an example about a property of color part of the constant data") );
                });

                return E; 
            }

            constexpr                   const_data      ( const guid Guid )         noexcept : t_parent{ Guid } {}
            virtual     const type&     getType         ( void )            const   noexcept override { return t_type; }

            xcolor m_ShareColor {};
        };

        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_USTR("Render/Unittest05");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Unittest05-Render" };
            x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
            using                           t_type                      = gb_component::type_pool<t_self>;
        };

    public: 

        auto& LinearSetup( const_data& ConstData )
        {
            m_ConstData.AddReference( ConstData );
            return *this;
        }
        
    protected: // --- class hierarchy functions ---
        
        my_render_component( const base_construct_info& C ) noexcept : t_parent( C ) {}

        virtual             err                 onCheckResolve              ( xvector<xstring>& WarningList )   const   noexcept
        {
            if( m_ConstData.isValid() == false )
            {
                return x_error_code( errors, ERR_FAILURE, "You must add a constant data for this component");
            }

            return x_error_ok();
        }

        virtual void onExecute( void ) noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert_linear( m_Debug_LQMsg );
            
            t_parent::onExecute();

            // I guess we would do some render call here...
            x_assert( m_ConstData->m_ShareColor >= 1 );
            x_assert( m_ConstData->m_ShareColor <= 2 );
        }

        virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
        { 
            static prop_table E( this, this, X_STR("Unittest05-Render"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable     ( t_parent::onPropertyTable() );
                E.AddProperty<u64>  ( X_STR("ConstantData"), GB_PROP_CONSTANT_DATA( m_ConstData ), X_STR("This is the component constant/read-only data. This data will be use by the component so it must be of the correct type." ) );
                E.AddProperty       ( X_STR("RenderSize"),  m_RenderSize, xproperty_v2::info::s32( 0, 10 ), X_STR("RenderSize") );
                E.AddProperty       ( X_STR("MyColor"),     m_MyColor, X_STR("Color") );

                m_gBlueprint.AddAsProperty( E, X_STR("Blueprint") );
                m_gEntityDep.AddAsProperty( E, X_STR("SomeEntity") );
            });

            return E; 
        }


    protected: // --- class hierarchy functions ---
        
        int                         m_RenderSize { 3 };
        xcolor                      m_MyColor{ 128, 128, 128, 255 };
        x_ll_share_ref<const_data>  m_ConstData; 
        gb_blueprint::guid          m_gBlueprint;
        gb_component::entity::guid  m_gEntityDep;
    };

    //---------------------------------------------------------------------------------
    // TEST CONSTANT DATA AS WELL
    //---------------------------------------------------------------------------------
    xarray< my_render_component::const_data*, 2> s_ConstType;

    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class my_entity : public gb_component::entity_dynamic
    {
        x_object_type( my_entity, is_quantum_lock_free, rtti(gb_component::entity_dynamic), is_not_copyable, is_not_movable )

    public: // --- class traits ---
          
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_USTR("Entity/Unittest05");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ "Unittest05-Entity" };
            x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
            using                           t_type                      = gb_component::type_entity_pool<t_self>;
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
        };

        x_constexprvar int              MAX_LIFE                        = 1000;

    protected: 
        
        struct mutable_data : t_parent::mutable_data
        {
            x_object_type( mutable_data, is_linear, rtti(gb_component::entity_dynamic::mutable_data) )

            virtual void onUpdateMutableData( const t_mutable_base& Src ) noexcept override
            {
                const mutable_data& NewSrc = reinterpret_cast<const mutable_data&>(Src);
                // copy the hold thing
                *this = NewSrc;
            }

            xvector3       m_Velocity;
        };

        X_DEFBITS( mybools, u32, 0,
            X_DEFBITS_ARG( bool, I_HAVE_ARMOR,  1),
            X_DEFBITS_ARG( bool, I_HAVE_HEALTH, 1)
        )

        enum some_enum : u8
        {
            SOMEENUM_NULL,
            SOMEENUM_ONE,
            SOMEENUM_TWO,
            SOMEENUM_FIVE,
        };

    protected: // --- class hierarchy functions ---
        
        my_entity( const base_construct_info& C ) noexcept : t_parent( C )
        {
            auto& Data = C.m_MutableData.SafeCast<mutable_data>();

            xrandom_small Rnd;
            Rnd.setSeed64( static_cast<u64>(xtimer::getNowMs()) );
            Data.m_Velocity.setup( Rnd.RandF32( -1, 1 ),Rnd.RandF32( -1, 1 ),Rnd.RandF32( -1, 1 ));
            Data.m_Velocity.Normalize();
            Data.m_L2W.RotateY( 90_deg );

            m_Death = Rnd.Rand32( 5, MAX_LIFE );

            // change the value
            m_Bools.m_I_HAVE_HEALTH = true;
        }

        virtual void onExecute( void ) noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert_linear( m_Debug_LQMsg );

            t_parent::onExecute();
            
            auto& Mutable = getT1Data<mutable_data>();

            xvector3 NewPos = Mutable.m_L2W.getTranslation() + Mutable.m_Velocity;

            Mutable.m_L2W.setTranslation( NewPos );

            if( --m_Death == 0 )
            {
                msgDestroy();
                auto& Entity = m_GInterface.m_GameMgr.CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ) );
                auto& Render = m_GInterface.m_GameMgr.CreateComponent<my_render_component>( Entity ).LinearSetup( *s_ConstType[ rand()%2 ] );

                Entity.msgAddToWorld();
            }
        }

        virtual void onNotifyInWorld( void ) noexcept override
        {
            // This is an example on how to get components from an entity
            m_pRenderComp = getComponent<my_render_component>();
        }

        virtual prop_table&  onPropertyTable         ( void ) const noexcept override 
        { 
            static prop_table E( this, this, X_STR("Unittest05-Entity"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( t_parent::onPropertyTable() );
                E.AddProperty           ( X_STR("Death"),       m_Death, xproperty_v2::info::s32(0,MAX_LIFE), X_STR("m_Death") );
                E.AddProperty<xvector3> ( X_STR("Velocity"),    GB_PROP_MUTABLE_DATA(m_Velocity), X_STR("Velocity"));
                E.AddProperty           ( X_STR("bHaveArmor"),  (bool&)m_Bools.m_Value, xproperty_v2::info::boolean( mybools::OFFSET_I_HAVE_ARMOR ),  X_STR("m_Death") );
                E.AddProperty           ( X_STR("bHaveHealth"), (bool&)m_Bools.m_Value, xproperty_v2::info::boolean( mybools::OFFSET_I_HAVE_HEALTH ), X_STR("bHaveHealth") );

                static const xarray< std::pair<int, xconst_str<xchar>>, 5> SomeEnumList
                {
                    std::pair<int, xconst_str<xchar>>{ static_cast<int>(SOMEENUM_ONE),  X_STR("ONE")       },
                    std::pair<int, xconst_str<xchar>>{ static_cast<int>(SOMEENUM_TWO),  X_STR("TWO")       },
                    std::pair<int, xconst_str<xchar>>{ static_cast<int>(SOMEENUM_FIVE), X_STR("FIVE")      },
                    std::pair<int, xconst_str<xchar>>{ static_cast<int>(xproperty_data::data_enum::entry::END_DISPLAY), X_STR("") },
                    std::pair<int, xconst_str<xchar>>{ static_cast<int>(SOMEENUM_NULL), X_STR("INVALID")   }
                };
                E.AddProperty( X_STR("SomeEnum"), m_SomeEnum, xproperty_v2::info::enum_t( SomeEnumList ), X_STR("SomeEnum") );
            });

            return E; 
        }

    protected:  // --- class hierarchy hidden variables ---
        
        int                                 m_Death;
        my_render_component*                m_pRenderComp   { nullptr };
        mybools                             m_Bools         {};
        some_enum                           m_SomeEnum      { SOMEENUM_FIVE };
    };
    
    //---------------------------------------------------------------------------------
    // MY GAME MANAGER
    //---------------------------------------------------------------------------------
    class my_game_graph final : public gb_game_graph::base
    {
        x_object_type( my_game_graph, is_quantum_lock_free, rtti(gb_game_graph::base), is_not_copyable, is_not_movable )
  
    public: // --- class constructor ---

        my_game_graph( void ) noexcept : t_parent( 20000 ) {}
   
    protected: // --- class types ---

        using entity_mgr = gb_component_mgr::base;
        using render_mgr = gb_component_mgr::base;
        using logic_sync = gb_game_graph::sync_point;

    protected: // --- class hierarchy functions ---
        
        virtual void onInitialize( void ) noexcept override
        {
            //
            // Notify our parent about initialization
            //
            t_parent::onInitialize();

            //
            // construct const data
            //
            for( auto& Entry : s_ConstType )
            {
                xndptr_s<my_render_component::const_data> ConstData;
                ConstData.New( gb_component::const_data::guid{ gb_component::const_data::guid::RESET } );
                Entry = ConstData;  
                m_ConstDataDB.RegisterAndTransferOwner( ConstData );
            }

            //
            // Register sync points
            // 
            m_LogicSyncPoint.setup( "LogicSyncPoint", "LogicSyncPoint");
            m_SyncDB.RegisterButRetainOwnership( m_LogicSyncPoint );

            //
            // Register managers
            //
            m_CompManagerDB.RegisterButRetainOwnership( m_EntityMgr );            
            m_CompManagerDB.RegisterButRetainOwnership( m_RenderMgr );            
                         
            //
            // Build the game graph
            //
            InitializeGraphConnection( m_StartSyncPoint, m_EntityMgr, m_LogicSyncPoint );
            InitializeGraphConnection( m_LogicSyncPoint, m_RenderMgr, m_EndSyncPoint );
            
            //
            // Register all the component types
            //
            RegisterComponentAndDependencies<gb_component::entity>          ( m_EntityMgr, m_LogicSyncPoint );
            RegisterComponentAndDependencies<gb_component::entity_dynamic>  ( m_EntityMgr, m_LogicSyncPoint );
            RegisterComponentAndDependencies<gb_component::entity_static>   ( m_EntityMgr, m_LogicSyncPoint );

            RegisterComponentAndDependencies<my_entity>                     ( m_EntityMgr, m_LogicSyncPoint );
            RegisterComponentAndDependencies<my_generic_component>          ( m_EntityMgr, m_LogicSyncPoint );
            RegisterComponentAndDependencies<my_render_component>           ( m_RenderMgr, m_EndSyncPoint   );
            

            //
            // Initialize my const types to some value
            //
            s_ConstType[0]->m_ShareColor = 1;
            s_ConstType[1]->m_ShareColor = 2;

            //
            // Create a blue print 
            //
            gb_blueprint::guid gBlueprint;
            {
                //
                // Create a blue print
                //
                auto& Blueprint = CreateBlueprint();
                gBlueprint = Blueprint.getGuid();

                //
                // Create an entity
                //
                auto& Entity    = CreateEntity<my_entity>( gb_component::entity::guid::RESET );
                auto& Render    = CreateComponent<my_render_component>( Entity ).LinearSetup( *s_ConstType[ rand()%2 ] );
                auto& Generic   = CreateComponent<my_generic_component>( Entity );

                // 
                // Add entity to the blue print
                //
                Blueprint.setup( Entity );

                //
                // Pretend that the entity came from a blue print
                //
                Entity.setupBlueprintGuid( gBlueprint );
            }

            //
            // Create a bunch of entities in the world
            //
            x_constexprvar int nEntities = 100;
            for( s32 i=0; i<nEntities; i++ )
            {
                auto& Blueprint = m_BlueprintDB.getEntry( gBlueprint );

                //
                // Create an instance of the blue print
                //
                auto& Entity = [&] () -> my_entity&
                {
                    if( i & 1 )
                    {
                        xmatrix4 L2W;
                        L2W.setIdentity();
                        L2W.setTranslation( xvector3( x_frand(-10,10) ) );
                        auto& Entity = Blueprint.CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), L2W, *this );
                        Entity.msgAddToWorld();
                        return Entity;
                    }
                    else
                    {
                        auto& Entity    = CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ) );
                        auto& Generic   = CreateComponent<my_generic_component>( Entity );
                        auto& Render    = CreateComponent<my_render_component>( Entity ).LinearSetup( *s_ConstType[ rand()%2 ] );
                        auto& Generic2  = CreateComponent<my_generic_component>( Entity );
                        Entity.msgAddToWorld();
                        return Entity;
                    }
                }();

            }
            int b = 12;
        }
        
        virtual void onEndFrame( void ) noexcept override
        {
            static int x=0;
            gb_game_graph::base::onEndFrame();
            x++;
            //if( (x%60)==0 )
            {
                std::cout << "Frame #" << m_nFrames << " SecondsPerFrame: " << xtimer::ToSeconds( m_Stats_LogicTime ) << "\n";
            }

            //
            // Save it to disk
            //
            {
                xtextfile File;
                File.openForWriting( X_WSTR("temp:/testentitysaving.txt") );
                linearSaveGame( File );
            }

            //
            // Delete all the entities
            //
            linearDeleteAllEntities();

            //
            // Load all the entities
            //
            linearLoadGame( X_WSTR("temp:/testentitysaving.txt") );
            {
                xtextfile File;
                File.openForWriting( X_WSTR("temp:/testentitysaving2.txt") );
                linearSaveGame( File );
            }
            
            //
            // We will exit when we do 1000 frames
            //
            if( x > 4 )
            {
                m_bLoop = false;
                g_context::get().m_Scheduler.MainThreadStopWorking();
            }
        }

    protected:

        logic_sync      m_LogicSyncPoint    {};
        entity_mgr      m_EntityMgr         { entity_mgr::guid{ "EntityMgr" }, *this, X_USTR( "EntityMgr" ) };
        render_mgr      m_RenderMgr         { entity_mgr::guid{ "RenderMgr" }, *this, X_USTR( "RenderMgr" ) };
    };
    
    //---------------------------------------------------------------------------------
    // Test
    //---------------------------------------------------------------------------------
    void Test( void )
    {
        my_game_graph GameMgr;
        GameMgr.Initialize(false);
        g_context::get().m_Scheduler.AddJobToQuantumWorld( GameMgr );
        g_context::get().m_Scheduler.MainThreadStartsWorking();
    }
};

