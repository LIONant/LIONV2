//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_game_graph
{
    class base;
    class sync_point;
};

//---------------------------------------------------------------------------------------------------------------------
// Helper for handling properties that are constant data 
//---------------------------------------------------------------------------------------------------------------------
#define GB_PROP_CONSTANT_DATA( VARIABLE )                                                                                       \
    X_STATIC_LAMBDA_BEGIN ( t_self& Class, u64& Data, const xproperty_v2::info::guid& Details, bool isSend )                    \
    {                                                                                                                           \
        if (isSend)                                                                                                             \
        {                                                                                                                       \
            Data = Class.VARIABLE.isValid()? Class.VARIABLE->getGuid().m_Value : 0;                                             \
        }                                                                                                                       \
        else                                                                                                                    \
        {                                                                                                                       \
            const const_data::guid Guid{ Data };                                                                                \
            if( Guid.m_Value )                                                                                                  \
            {                                                                                                                   \
                const auto& ConstDataMgr = Class.getGlobalInterface().m_GameMgr.m_ConstDataDB;                                  \
                using const_t = decltype(Class.VARIABLE)::t_shared;                                                             \
                Class.VARIABLE = const_cast<gb_component::const_data&>( ConstDataMgr.getEntry( Guid ) ).SafeCast<const_t>();    \
            }                                                                                                                   \
            else                                                                                                                \
            {                                                                                                                   \
                Class.VARIABLE.setNull();                                                                                       \
            }                                                                                                                   \
        }                                                                                                                       \
    }                                                                                                                           \
    X_STATIC_LAMBDA_END                                                                                                         \
    , xproperty_v2::info::guid                                                                                                  \
    {                                                                                                                           \
        decltype(VARIABLE)::t_shared::t_type.getEditorName()                                                                    \
        , decltype(VARIABLE)::t_shared::t_type.getGuid().m_Value                                                                \
    }                                                                                                                           \


//---------------------------------------------------------------------------------------------------------------------
// Helper for handling properties that are mutable data 
//---------------------------------------------------------------------------------------------------------------------
#define GB_PROP_MUTABLE_DATA( VARIABLE )                                                                                                \
    X_STATIC_LAMBDA_BEGIN ( t_self& Class, decltype(((mutable_data*)0)->VARIABLE)& Data,                                                \
    const xproperty_v2::details::details_from_type<decltype(((mutable_data*)0)->VARIABLE)>::detail& Details, bool isSend )              \
    {                                                                                                                                   \
        using usertpe = decltype(((mutable_data*)0)->VARIABLE);                                                                         \
        using type    = xproperty_v2::enumeration::valid_usertype<usertpe>::type;                                                       \
        xproperty_v2::enumeration::queries<type>::Query<usertpe>( Class.getT1Data<mutable_data>().VARIABLE, Data, Details, isSend );    \
    }                                                                                                                                   \
    X_STATIC_LAMBDA_END


namespace gb_component
{
    class entity;
    class type_base;

    //------------------------------------------------------------------------------
    // Description:
    //      This is the base class for all the game components.
    //      The class is a quantum object so all relevant rules apply
    //      A component has two main states. In the world where the game will try to update it.
    //      Out from the world where you can do things like build entities etc, because they are
    //      thought to be in a linear world.
    //      Every component class needs to have an accompany gb_component_type_base
    //------------------------------------------------------------------------------
    class base : 
        public      x_ll_object_harness< base >, 
        protected   xproperty_v2::base
    {
        x_object_type( base, is_quantum_lock_free, rtti( xproperty_v2::base ), is_not_copyable, is_not_movable )

    public:

        enum errors
        {
            ERR_OK,
            ERR_FAILURE
        };

        using err = x_err<errors>;

        // t_descriptors:
        //      Every derived component requires to redefine this structure redefining whatever is relevant to it.
        //      This structure defines details about the component at compile time.
        struct t_descriptors
        {
            // definition:
            //      used for general information about the component type and its requirements. This is require for each component that is create or derived.
            //      It must be expose/use in the derived class in the following way: 
            X_DEFBITS( def, u8, 0x00,
                X_DEFBITS_ARG( bool, CONSTANT,      1 ),                   // This means that the component does not need onUpdate to be call, and no state changes will happen internally
                X_DEFBITS_ARG( bool, NETWORK_SYNC,  1 )                    // This component needs to send and receive information from the network
            );

            // REQUIRED:
            //  x_constexprvar auto         t_category_string           = X_STR("...");                             // This is a string used by editors and includes the path and the name of the component, ex: "Render/Mesh"
            //  x_constexprvar auto         t_type_guid                 = gb_component::type_base::guid{ X_STR_CRCINFO( "..." ) }; // Creates a unique ID for the entity type. Note this string is very hard to change later.
            //  x_constexprvar auto         t_help                      = X_STR("...");                             // Text descriptor use for editor explaining the user what this component can do
            //  x_constexprvar def          t_definition                = def::MASK_DEFAULT ...;                    // as seem above
            //  using                       t_type                      = gb_component::type_entity_pool<t_self>;   // defines which pool the component will use

            // DEFAULTS:
            x_constexprvar xuptr            t_pool_paged_size           = 512;                                      // Override to tell the page pool how many components per each page we should allocate
        };

        // global_interface:
        //      This is how component can have access to the rest of the systems in the game. This is a read only structure and the user is not require
        //      to do anything for it. It is part of the component default construction.
        struct global_interface
        {
            gb_component_mgr::base&     m_Manager;                      // This component manager
            type_base&                  m_Type;                         // Type for the component
            gb_game_graph::base&        m_GameMgr;                      // Global Game Manager
            gb_game_graph::sync_point&  m_SyncPoint;                    // Sync where are time is based on
            int                         m_CompactComponentOffset;       // The offset of the compact component from base
        };

        using                       t_gi                            = const global_interface;
        x_constexprvar  auto        t_class_string                  = X_STR("Component");
        x_constexprvar  auto        t_class_guid                    = class_guid{"Component"};

        using delegates     = gb_component::delegates;
        using events        = gb_component::events;
        using narrow_guid   = gb_component::narrow_guid;
        struct const_data : gb_component::const_data { using gb_component::const_data::const_data; };

    public:
                                    base                ( void )                                                 = delete;
        virtual                    ~base                ( void )                                        noexcept { x_assert_linear( m_Debug_LQ ); x_assert_lqnone( m_Debug_LQMsg );     }
        x_orforceconst  t_gi&       getGlobalInterface  ( void )                                const   noexcept { return m_GInterface;                                 }
        x_forceinline   bool        isInWorld           ( void )                                const   noexcept { return hasFlags( flags::MASK_IN_THE_WORLD );         }
        x_orforceconst  base*       getNextComponent    ( void )                                const   noexcept { return m_pNextComp;                                  }
        x_orforceconst  bool        isEntity            ( void )                                const   noexcept;
        x_orforceconst  type_base&  getType             ( void )                                const   noexcept { return m_GInterface.m_Type;                          }         
        x_forceinline   auto&       getEntity           ( void )                                        noexcept { return m_Entity;                                     }            
        x_forceinline   auto&       getEntity           ( void )                                const   noexcept { return m_Entity;                                     }
        x_forceinline   bool        isEditorSelected    ( void )                                const   noexcept { return hasFlags( flags::MASK_EDITOR_SELECTED ); }
        x_forceinline   bool        isActive            ( void )                                const   noexcept { return !hasFlags( flags::MASK_NO_ACTIVE );      }
        x_forceinline   bool        isResolved          ( void )                                const   noexcept { return hasFlags( flags::MASK_RESOLVED );        }
        x_forceinline   bool        isDeleted           ( void )                                const   noexcept { return hasFlags( flags::MASK_DELETED );         }
        x_forceinline   void        setEditorSelect     ( bool bSelect )                                noexcept { if( bSelect ) appendFlags( flags::MASK_EDITOR_SELECTED ); else removeFlags( flags::MASK_EDITOR_SELECTED ); }
        x_forceinline   void        setActive           ( bool bActive )                                noexcept { if( !bActive ) appendFlags( flags::MASK_NO_ACTIVE );   else removeFlags( flags::MASK_NO_ACTIVE ); }
        x_forceinline   err         linearCheckResolve  ( xvector<xstring>& WarningList )       const   noexcept { return onCheckResolve( WarningList ); }
        x_forceinline   auto        getNarrowCount      ( void )                                const   noexcept { return narrow_count{ m_qtFlags.load( std::memory_order_relaxed ).m_NARROW_COUNT }; }
        x_forceinline   narrow_guid getNarrowGUID       ( void )                                const   noexcept;
        virtual         events&                                                   getEvents       ( void )        noexcept = 0;
        x_forceinline   const events&                                             getEvents       ( void ) const  noexcept { return const_cast<base*>(this)->getEvents(); }
        virtual         const xbuffer_view<const typename events::definition>&    getEventTable   ( void ) const  noexcept = 0;
        x_forceinline   event::delegate::guid                                     getEventGuid    ( const typename events::definition& EventDef ) const noexcept
        {
            return event::delegate::guid{ EventDef.m_Guid.getNameCRC(), getNarrowGUID() };
        }
        x_forceinline   event_fake*                                                    getEventByGuid  ( event::delegate::guid Guid ) noexcept
        {
            const auto NarrowGuid = getNarrowGUID();
            const auto GuidNarrow = Guid.getNarrowGuid(); 
            if( GuidNarrow != NarrowGuid ) return nullptr;

            const auto ComponentNamedCRC = Guid.getNamedCRC();
            for( auto& E : getEventTable() )
            {
                // The only part of the guid we are interested on is the lower32 where the unique id is store for the event
                // the upper 32bits are used to identify the component which we assume we are here already
                if( E.m_Guid.getNameCRC() == ComponentNamedCRC )
                {
                    return reinterpret_cast<event_fake*>( &reinterpret_cast<xbyte*>(&getEvents())[ E.m_Offset ] );
                }
            }
            return nullptr;
        }
        x_forceinline xproperty_v2::base* getLinearPropInterface( void ){ return this; }

        x_forceinline   void*                                                           getInterfaceByGuid( event::delegate::guid Guid ) noexcept
        {
            const auto NarrowGuid = getNarrowGUID();
            const auto GuidNarrow = Guid.getNarrowGuid(); 
            if( GuidNarrow != NarrowGuid ) return nullptr;

            const auto ComponentNamedCRC = Guid.getNamedCRC();
            for( auto& E : getEventTable() )
            {
                // The only part of the guid we are interested on is the lower32 where the unique id is store for the event
                // the upper 32bits are used to identify the component which we assume we are here already
                if( E.m_Guid.getNameCRC() == ComponentNamedCRC )
                {
                    return reinterpret_cast<void*>( &reinterpret_cast<xbyte*>(this)[ E.m_Offset ] );
                }
            }
            return nullptr;
        }

        virtual         delegates&                                                getDelegates    ( void )        noexcept = 0;
        x_forceinline   const delegates&                                          getDelegates    ( void ) const  noexcept { return const_cast<base*>(this)->getDelegates(); }
        virtual         const xbuffer_view<const typename delegates::definition>& getDelegateTable( void ) const  noexcept = 0;

    protected:  
    
        X_DEFBITS( flags, 
                    u32,
                    0x00000000,
                    X_DEFBITS_ARG( bool,    T0,                             1 ),
                    X_DEFBITS_ARG( bool,    IN_THE_WORLD,                   1 ), // Tells that this component is in the world
                    X_DEFBITS_ARG( bool,    EDITOR_SELECTED,                1 ), // Tells the editor that this object is part of a selection
                    X_DEFBITS_ARG( bool,    NO_ACTIVE,                      1 ), // Tells the managers not to update this object
                    X_DEFBITS_ARG( bool,    RESOLVED,                       1 ), // Tells the system that this object has already been resolved 
                    X_DEFBITS_ARG( bool,    DELETED,                        1 ), // Tells anyone that this object has been mark as to be deleted
                    X_DEFBITS_ARG( u8,      NARROW_COUNT,                   8 )  // This is a guid that is generated for each component type to separate from components of the same kind
        );
    
        struct mutable_data 
        {
            x_object_type( mutable_data, is_linear, rtti_start )
            virtual void onUpdateMutableData( const mutable_data& Src ) noexcept {}    
        };
        using               t_mutable_base  = mutable_data;

        // base_construct_info:
        //      This structure is what is used as an input to the constructor of a component. For most part it is private to the system and the user can safely ignore it.
        struct base_construct_info
        {
            using mutable_data_offset = u16;

            entity&                         m_Entity;
            const global_interface&         m_GI;
            flags                           m_Flags;
            mutable_data&                   m_MutableData;
        };
        
    protected:

        x_forceinline                       base                    ( const base_construct_info& C )    noexcept;
        x_inline        flags               appendFlags             ( flags Flags )                     noexcept;
        x_inline        flags               removeFlags             ( const flags Flags )               noexcept;
        x_forceinline   bool                hasFlags                ( flags Flags )             const   noexcept { return (m_qtFlags.load( std::memory_order_relaxed ) & Flags) == Flags; }

        template< typename T_CLASS >
        x_forceinline   const T_CLASS&      getT0Data               ( void )                    const   noexcept;

        template< typename T_CLASS >
        x_forceinline   T_CLASS&            getT1Data               ( void )                            noexcept;

        template< typename T_CLASS >
        x_forceinline   const T_CLASS&      getConstData            ( void )                    const   noexcept; 

        x_forceinline   bool                isConstDataValid        ( void )                    const   noexcept;

        template< typename T_CLASS >
        x_inline        void                SendReciveConstantData  (   xproperty_query&            Query, 
                                                                        x_ll_share_ref<T_CLASS>&    ConstData ) const noexcept;
    protected:

        virtual         void                onExecute               ( void )                            noexcept                { x_assert_quantum( m_Debug_LQ ); x_assert_linear( m_Debug_LQMsg ); x_assert( isInWorld() ); }
        virtual         void                onNotifyOutWorld        ( void )                            noexcept                { x_assert_quantum( m_Debug_LQ ); x_assert_linear( m_Debug_LQMsg ); }
        virtual         void                onNotifyInWorld         ( void )                            noexcept;
        virtual         void                onActivate              ( const bool bActivate )            noexcept                { x_assert_quantum( m_Debug_LQ ); x_assert_linear( m_Debug_LQMsg ); }
        virtual         prop_table&         onPropertyTable         ( void )                            const noexcept override { x_assert_quantum( m_Debug_LQ );  x_assert_linear( m_Debug_LQMsg ); static prop_table E( this, this, [&](xproperty_v2::table& E){} ); return E; }
        virtual         void                onResolve               ( void )                            noexcept;
        virtual         err                 onCheckResolve          ( xvector<xstring>& IssuesList )    const   noexcept;

        // This function gets call ones per frame to update the component
        virtual         void                qt_onRun                ( void )                            noexcept override final;
        virtual         const const_data&   internal_ConstData      ( void )                            const   noexcept = 0;
        virtual         bool                internal_ConstDataValid ( void )                            const   noexcept = 0;

    protected:

        entity&                             m_Entity;                                           // Reference to the entity
        const global_interface&             m_GInterface;                                       // How this instance has access to the global state
        x_atomic<flags>                     m_qtFlags                   { flags{} };
        base*                               m_pNextComp                 { nullptr };            // Link list of components for the entity

    private:

        // This function gets call every time a new message comes in
        x_inline        void                t_ProcessMessages       ( void )                noexcept;

    private:

        virtual       mutable_data&         getMutableData          ( const u32 I )                     noexcept = 0;
        x_forceinline const mutable_data&   getMutableData          ( const u32 I )                     const noexcept { return const_cast<gb_component::base*>( this )->getMutableData(I); }

    private:

                                    friend class entity;
                                    friend  x_ll_object_harness< base >;
        template< typename T_COMP > friend class type_pool;
        template< typename T_COMP > friend class type_entity_pool;
                                    friend class type_base;
                                    friend class gb_component_mgr::base;
    };
};

