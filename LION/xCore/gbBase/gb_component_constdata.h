//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component
{
    //------------------------------------------------------------------------------
    // Description:
    //          Constant data is sharable across different components. This data should not change
    //          while the application is running. It is defined as constant data.
    //------------------------------------------------------------------------------
    class const_data : 
        public x_ll_share_object, 
        public xproperty_v2::base
    { 
        x_object_type( const_data, is_linear, rtti(xproperty_v2::base), is_not_copyable, is_not_movable )

    public:

       using guid = xguid<const_data>;

        class type
        {
            x_object_type( type, is_linear, is_not_copyable, is_not_movable )

        public:

            x_constexprvar  auto        t_class_string  = X_STR("ConstantData");
            x_constexprvar  auto        t_class_guid    = class_guid{"ConstantData"};
            using                       guid            = extended_guid<type,const_data, t_class_guid.m_Value >;

        public:

            template< typename T >
            x_forceconst                    type            ( T*, named_guid<const_data> NamedCRC, xstring::const_str EditorName, xstring::const_str Help ) noexcept : 
                                                                    m_NewFn{ t_new<T> }
                                                                ,   m_Guid{ t_class_guid, NamedCRC }
                                                                ,   m_EditorName{ EditorName }
                                                                ,   m_Help{ Help }
                                                                {}
            x_forceconst    guid            getGuid         ( void ) const  noexcept { return m_Guid; }
            x_forceconst    const_data*     New             ( gb_component::const_data::guid Guid ) const  noexcept { return m_NewFn( Guid ); }
            x_forceconst    auto            getEditorName   ( void ) const { return m_EditorName; }
            x_forceconst    auto            getHelp         ( void ) const { return m_Help; }

        protected:

            using           fnnew  = const_data*(*)( gb_component::const_data::guid Guid );

        protected:

            template< typename T >
            static xowner<const_data*> t_new ( gb_component::const_data::guid Guid ) { x_ll_share_ref<T,type> Entry; Entry.New( Guid ); return Entry.TransferOwnerShip(); };
            
        protected: 

            fnnew               m_NewFn;
            guid                m_Guid;
            xstring::const_str  m_EditorName;
            xstring::const_str  m_Help;
        };

    public: 

        x_forceinline               const_data      ( const guid Guid )                 noexcept            : m_Guid{ Guid } { Guid.getStringHex( m_EditorString ); }
        virtual                    ~const_data      ( void )                            noexcept            {}
        x_inline        guid&       getMutableGuid  ( void )                            noexcept            { return m_Guid; }
        x_forceconst    guid        getGuid         ( void )                    const   noexcept            { return m_Guid; }
        
        virtual         prop_table& onPropertyTable ( void )                    const   noexcept override   
        { 
            static prop_table E( this, X_STR("Base"), [&]( xproperty_v2::table& E )
            {
                 E.AddProperty( X_STR("GUID"), m_Guid.m_Value, xproperty_v2::info::guid( type::t_class_string, type::t_class_guid.m_Value ), 
                     X_STR("Unique ID for this constant data"), xproperty_v2::flags::MASK_READ_ONLY | xproperty_v2::flags::MASK_FORCE_SAVE );
            }); 
            return E; 
        }

        virtual         const type& getType         ( void )                    const   noexcept = 0;
        x_forceinline   xstring&    getEditorName   ( void )                            noexcept            { return m_EditorString; }

    protected: 

        guid                    m_Guid;
        xstring                 m_EditorString;
    };
}

