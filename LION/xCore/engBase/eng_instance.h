//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_instance : public eng_base
{
public:

    enum errors : u8 
    {
        ERR_OK,
        ERR_FAILURE,
    };

    using err       = x_err<errors>;
    using handle    = x_ll_share_ref<eng_instance>;

    struct setup
    {
        setup( void* pInstance ) : m_AppInstance{ pInstance } {}

        enum render_driver : u8 
        {
            RENDER_DRIVER_VULKAN,
            RENDER_DRIVER_METAL,
            RENDER_DRIVER_DIRECTX12,
            RENDER_DRIVER_OPENGL,
            RENDER_DRIVER_DIRECTX11,
            RENDER_DRIVER_COUNT,
            #if _X_TARGET_MAC
                RENDER_DRIVER_DEFAULT = RENDER_DRIVER_METAL
            #else
                RENDER_DRIVER_DEFAULT = RENDER_DRIVER_VULKAN
            #endif
        };

        void*           m_AppInstance   { nullptr                   };
        const char*     m_pAppName      { "LIONant - App"           };
        bool            m_bValidation   { true                      };
        render_driver   m_RenderDriver  { RENDER_DRIVER_DEFAULT     };
    };
    
//-------------------------------------------------------------------------------------------
public:

    inline  static  err             CreateInstance      ( xndptr_s<eng_instance>&   Instance,  const eng_instance::setup&  Setup                            ) noexcept;
                    err             CreateDevice        ( eng_device*&              pDevice,   const eng_device::setup&    Setup = eng_device::setup{}      ) noexcept;
                    err             CreateWindow        ( eng_window*&              pWindow,   const eng_window::setup&    Setup                            ) noexcept;  
};




//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------






/*





namespace eng_instancev1
{
    class base;

    //
    // base
    //
    class base
    {
    public:


    public:

        virtual                                    ~base                        ( void );
        static xndptr_s<base>                       CreateInstance              ( void );
        err                                         Initialize                  ( const setup& Setup );
        err                                         CreateSystemWindow          ( xowner<system_window*>& pWindow, system_window::setup& Setup );
        eng_vkswapchain&                            getSwapChain                ( void )        { return m_SwapChain;               }
        VkRenderPass&                               getGlobalRenderPass         ( void )        { return m_VkRenderPass;            }
        xndptr<VkCommandBuffer>&                    getDrawCmdBuffers           ( void )        { return m_DrawCmdBuffers;          }
        xndptr<VkFramebuffer>&                      getFrameBuffer              ( void )        { return m_FrameBuffers;            }
        uint32_t                                    getCurrentFrameIndex        ( void ) const  { return m_iCurrentBuffer;          }
        VkQueue&                                    getDeviceGraphicsQueue      ( void )        { return m_VkQueue;                 }
        VkCommandBuffer&                            getPostPresentCmdBuffer     ( void )        { return m_VkPostPresentCmdBuffer;  }
        bool                                        getMemoryType               ( uint32_t typeBits, VkFlags properties, uint32_t& typeIndex ) const;
        VkPipelineCache&                            getPipelineCache            ( void )        { return m_VkPipelineCache;         }
        const x_reporting::log_channel&             getErrorLogChannel          ( void ) const  { return m_ErrorLogChannel;         }
        const x_reporting::log_channel&             getInfoLogChannel           ( void ) const  { return m_InfoLogChannel;          }
        void                                        RenderEnd                   ( void );
        void                                        RenderBegin                 ( const system_window& SysWindow );
        void                                        Resize                      ( void* pData, uint32_t& Width, uint32_t& Height );
        constexpr VkCommandBuffer                   getDrawCmdBuffer            ( void ) const  noexcept    { return m_DrawCmdBuffers[m_iCurrentBuffer]; }
        constexpr VkRect2D                          getScissor                  ( void ) const  noexcept    { return m_Scissor; }
        constexpr VkViewport                        getViewport                 ( void ) const  noexcept    { return m_Viewport; }
        constexpr VkPhysicalDevice                  getPhysicalDevice           ( void ) const  noexcept    { return m_PhysicalDevices[0]; }
        err                                         CreateSetupCommandBuffer    ( void );
        err                                         FlushSetupCommandBuffer     ( void );
        constexpr VkCommandBuffer                   getSetupCommandBuffer       ( void ) const noexcept     { return m_VkSetupCmdBuffer; }
        VkPhysicalDeviceMemoryProperties            getDeviceMemProps           ( void ) const noexcept     { return m_VkDeviceMemoryProperties; }

    protected:

                                                    base                        ( void ) = default;

    protected:

        x_reporting::log_channel            m_ErrorLogChannel           {};
        x_reporting::log_channel            m_InfoLogChannel            {};
        VkPhysicalDeviceMemoryProperties    m_VkDeviceMemoryProperties  { VK_NULL_HANDLE };     // Stores all available memory (type) properties for the physical device
        VkQueue                             m_VkQueue                   { VK_NULL_HANDLE };     // Handle to the device graphics queue that command buffers are submitted to
        VkFormat                            m_VkDepthFormat             {};                     // Depth buffer format, Depth format is selected during Vulkan initialization
        VkCommandPool                       m_VkCmdPool                 { VK_NULL_HANDLE };     // Command buffer pool
        VkCommandBuffer                     m_VkSetupCmdBuffer          { VK_NULL_HANDLE };     // Command buffer used for setup
        xndptr<VkCommandBuffer>             m_DrawCmdBuffers            {};                     // Command buffers used for rendering
        VkCommandBuffer                     m_VkPostPresentCmdBuffer    { VK_NULL_HANDLE };     // Command buffer for submitting a post present barrier                   
        VkRenderPass                        m_VkRenderPass              { VK_NULL_HANDLE };     // Global render pass for frame buffer writes
        VkPipelineCache                     m_VkPipelineCache           { VK_NULL_HANDLE };     // Pipeline cache object
        eng_vkswapchain                     m_SwapChain                 {};                     // Wraps the swap chain to present images (framebuffers) to the windowing system
        xndptr<VkFramebuffer>               m_FrameBuffers              {};                     // List of available frame buffers (same as number of swap chain images)
        uint32_t                            m_iCurrentBuffer            {0};                    // Active frame buffer index
        VkClearValue                        m_VkClearColorVal           { { 1.00f, 0.25f, 0.25f, 1.0f } };
        VkClearValue                        m_VkClearDepthVal           { 1.0f, 0 };
        VkViewport                          m_Viewport                  {};
        VkRect2D                            m_Scissor                   {};


        struct 
        {
            VkImage             m_VkImage         {};
            VkDeviceMemory      m_VkMem           {};
            VkImageView         m_VkImageView     {};

        }                                   m_DepthStencil              {};

    protected:

        friend struct system_window;
    };
};
      */