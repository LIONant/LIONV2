//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_material_type : public eng_base
{
public:

    using handle    = x_ll_share_ref<eng_material_type>;

public:

    struct sampler_binding_declarations
    {
        int m_iBind {-1};
    };

    class info
    {
        x_object_type( info, is_linear, is_not_copyable, is_not_movable );

    public:

        constexpr info( void ) noexcept = default;

    public:

        int                                                     m_nSamplerBindings      {0};
        xarray<sampler_binding_declarations,16>                 m_SamplerBindings       {};
        eng_shader_vert::handle                                 m_hVertShader           {};
        eng_shader_frag::handle                                 m_hFragShader           {};
        eng_shader_geom::handle                                 m_hGeomShader           {};
        int                                                     m_EntriesPerPage        { 64 };
    };

    struct setup : public info 
    {
    };

public:

    constexpr           const info&     getInfo                     ( void )                            const   noexcept { return m_Info; }

protected:

//    constexpr                           eng_material_type           ( void )                                    noexcept = default;
    inline      static  void            t_DeleteSharedInstance      ( eng_material_type* const pData )          noexcept { pData->Release(); }
    virtual             void            Release                     ( void )                                    noexcept = 0;

protected:

    info                                        m_Info {};

protected:

    friend class x_ll_share_object;
};


//---------------------------------------------------------------------------------------------

class eng_material_instance : public eng_base
{

};
