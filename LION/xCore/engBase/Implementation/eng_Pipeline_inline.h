//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------------

inline      
void eng_pipeline::blend::setupBlendOff( void ) noexcept
{
    m_ColorWriteMask    = 0xf;
    m_bEnable           = false;

    m_ColorSrcFactor    = factor::ONE;
    m_ColorDstFactor    = factor::ZERO;
    m_ColorOperation    = op::ADD;

    m_AlphaSrcFactor    = factor::ONE;
    m_AlphaDstFactor    = factor::ZERO;
    m_AlphaOperation    = op::ADD;
}

//------------------------------------------------------------------------------------
// DestinationColor.rgb = (SourceColor.rgb * SourceColor.a) + (DestinationColor.rgb * (1 - SourceColor.a));
inline      
void eng_pipeline::blend::setupAlphaOriginal( void ) noexcept
{
    m_ColorWriteMask    = 0xf;
    m_bEnable           = true;

    m_ColorSrcFactor    = factor::SRC_ALPHA;
    m_ColorDstFactor    = factor::ONE_MINUS_SRC_ALPHA;
    m_ColorOperation    = op::ADD;

    m_AlphaSrcFactor    = factor::ONE;
    m_AlphaDstFactor    = factor::ZERO;
    m_AlphaOperation    = op::ADD;
}

//------------------------------------------------------------------------------------
// Reference:    https://developer.nvidia.com/content/alpha-blending-pre-or-not-pre
// DestinationColor.rgb = (SourceColor.rgb * One) + (DestinationColor.rgb * (1 - SourceColor.a))
inline      
void eng_pipeline::blend::setupAlphaPreMultiply( void ) noexcept
{
    m_ColorWriteMask    = 0xf;
    m_bEnable           = true;

    m_ColorSrcFactor    = factor::SRC_ALPHA;
    m_ColorDstFactor    = factor::ONE_MINUS_SRC_ALPHA;
    m_ColorOperation    = op::ADD;

    m_AlphaSrcFactor    = factor::ONE;
    m_AlphaDstFactor    = factor::ZERO;
    m_AlphaOperation    = op::ADD;
}

//------------------------------------------------------------------------------------
// DestinationColor.rgb = (SourceColor.rgb * Dst.rgb) + (DestinationColor.rgb * 0)
inline      
void eng_pipeline::blend::setupMuliply( void ) noexcept
{
    m_ColorWriteMask    = 0xf;
    m_bEnable           = true;

    m_ColorSrcFactor    = factor::DST_COLOR;
    m_ColorDstFactor    = factor::ZERO;
    m_ColorOperation    = op::ADD;

    m_AlphaSrcFactor    = factor::ONE;
    m_AlphaDstFactor    = factor::ZERO;
    m_AlphaOperation    = op::ADD;
}

//------------------------------------------------------------------------------------
// DestinationColor.rgb = (SourceColor.rgb * One) + (DestinationColor.rgb * One)
inline      
void eng_pipeline::blend::setupAdd( void ) noexcept
{
    m_ColorWriteMask    = 0xf;
    m_bEnable           = true;

    m_ColorSrcFactor    = factor::ONE;
    m_ColorDstFactor    = factor::ONE;
    m_ColorOperation    = op::ADD;

    m_AlphaSrcFactor    = factor::ONE;
    m_AlphaDstFactor    = factor::ZERO;
    m_AlphaOperation    = op::ADD;
}

//------------------------------------------------------------------------------------
// DestinationColor.rgb = (DestinationColor.rgb * One) - (SourceColor.rgb * One) 
inline      
void eng_pipeline::blend::setupSubSrcFromDest( void ) noexcept
{
    m_ColorWriteMask    = 0xf;
    m_bEnable           = true;

    m_ColorSrcFactor    = factor::ONE;
    m_ColorDstFactor    = factor::ONE;
    m_ColorOperation    = op::REVERSE_SUBTRACT;

    m_AlphaSrcFactor    = factor::ONE;
    m_AlphaDstFactor    = factor::ZERO;
    m_AlphaOperation    = op::ADD;
}

//------------------------------------------------------------------------------------
// DestinationColor.rgb = (SourceColor.rgb * One) - (DestinationColor.rgb * One)
inline      
void eng_pipeline::blend::setupSubDestFromSrc( void ) noexcept
{
    m_ColorWriteMask    = 0xf;
    m_bEnable           = true;

    m_ColorSrcFactor    = factor::ONE;
    m_ColorDstFactor    = factor::ONE;
    m_ColorOperation    = op::SUBTRACT;

    m_AlphaSrcFactor    = factor::ONE;
    m_AlphaDstFactor    = factor::ZERO;
    m_AlphaOperation    = op::ADD;
}
