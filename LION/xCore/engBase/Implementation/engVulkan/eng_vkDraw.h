//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

class draw_system final
{
public:

    constexpr                       draw_system             ( void )                                                                noexcept = delete;
    inline                          draw_system             ( device& Device )                                                      noexcept : m_Device{ Device } {}
                void                Initialize              ( VkRenderPass RenderPass, const VkPipelineCache pipelineCache )        noexcept;
                void                Kill                    ( void )                                                                noexcept;
                VkDescriptorSet     AllocDescriptorSet      ( void )                                                                noexcept;
    constexpr   const auto&         getShaderStages         ( void )                                                        const   noexcept { return m_ShaderStages;       }
    constexpr   const auto          getVkPipelineLayout     ( void )                                                        const   noexcept { return m_VkPipelineLayout;   }
    constexpr   const auto&         getCreationInfo         ( void )                                                        const   noexcept { return m_VPipelineDescritors.getCreationInfo(); }

protected:

    enum
    {
        MAX_DESCRIPTOR_SETS = 256
    };

    using general_lk = x_lk_spinlock::base<x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing>;

    struct descriptor_entry_pool
    {
        x_atomic<int>                                   m_Count     {0};
        xarray<VkDescriptorSet,MAX_DESCRIPTOR_SETS>     m_Pool      { nullptr };
    };

protected:

    void setupDescriptorSetLayout   ( device& Device ) noexcept;
    void setupDescriptorPool        ( device& Device ) noexcept;
    void preparePipelines           ( device& Device, VkRenderPass RenderPass, const VkPipelineCache pipelineCache ) noexcept;
    void prepareVertices            ( device& Device ) noexcept;
    void onJustAfterPageFlip        ( void )           noexcept;

protected:

    device&                                             m_Device;
    tools::vertex_pipeline_descriptors                  m_VPipelineDescritors       {};
    xarray<VkPipelineShaderStageCreateInfo,2>           m_ShaderStages              {};
    VkPipelineLayout                                    m_VkPipelineLayout          {};
    eng_sampler::handle                                 m_hWhiteTextureSampler      {};
    x_locked_object<VkDescriptorPool, general_lk>       m_LockedVKDescriptorPool    {};
    xarray<VkDescriptorSetLayout,1>                     m_VkDescriptorSetLayout     {};
    xarray<descriptor_entry_pool,2>                     m_DescriptorsInUsed         {};
    xarray<tools::shader,2>                             m_Shader                    { m_Device, m_Device };                
    x_message::delegate<draw_system>                    m_DelegateJustAfterPageFlip { *this, &draw_system::onJustAfterPageFlip };

protected:

    friend class draw;
};

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

class draw final : public eng_draw
{
public:

    using vertex = eng_draw::vertex;

    struct setup
    {
         VkRenderPass           RenderPass; 
         const VkPipelineCache  pipelineCache;
    };

public:

                                    draw                        ( display_cmds& DisplayCmds )                                                   noexcept : m_DisplayCmds{ DisplayCmds } {}
                void                Begin                       ( const pipeline PipeLine )                                                     noexcept;
                void                End                         ( void )                                                                        noexcept;

    // Simple draw functions
    virtual     buffers             popBuffers                  ( const int nVertices, const int nIndices )                                     noexcept override;
    virtual     void                DrawBufferTriangles         ( const xmatrix4& L2C, int IndexOffset, int TriangleCount )                     noexcept override;
    virtual     void                DrawBufferLines             ( const xmatrix4& L2C )                                                         noexcept override;
    virtual     void                DrawBufferTriangles2        (   const xmatrix4&                         L2C, 
                                                                    const eng_material_informed::handle&    hHandle,
                                                                    const eng_pipeline::handle&             hPipeline,
                                                                    const int                               IndexOffset, 
                                                                    const int                               IndexCount )                        noexcept override;

    virtual     void                setSampler                  ( const eng_sampler::handle& hSampler )                                         noexcept override;
    virtual     void                setScissor                  ( const xirect& Rect )                                                          noexcept override;
    virtual     void                ClearSampler                ( void )                                                                        noexcept override;
    virtual     void                ClearScissor                ( void )                                                                        noexcept override;
    virtual     void                Release                     ( void )                                                                        noexcept override {}

    // State functions
                void                Initialize                  ( const setup& Setup )                                                          noexcept;
                void                DrawBegin                   ( void )                                                                        noexcept {}
                void                DrawTriangles               ( const xvector3d& Center, const xmatrix4& L2C, int iT )                        noexcept;
                void                SubmitCmds                  ( void )                                                                        noexcept;

protected:

                void                setupDescriptorSetLayout   ( void )                                                                         noexcept;
                void                setupDescriptorPool        ( void )                                                                         noexcept;
                void                preparePipelines           ( VkRenderPass RenderPass, const VkPipelineCache pipelineCache )                 noexcept;
                void                prepareVertices            ( void )                                                                         noexcept;
                void                FlushBufferedData          ( const xmatrix4&    L2C, 
                                                                 const bool         bPolygons,
                                                                 const int          IndexOffset, 
                                                                 const int          IndexCount )                                                noexcept;

protected:

    display_cmds&                           m_DisplayCmds;
    const sampler*                          m_pSampler          { nullptr };
    buffer_double_paged::vertex_alloc       m_VertAlloc         { nullptr, 0, buffer_double_paged::i_vertex{0} };
    buffer_double_paged::index_alloc        m_IndexAlloc        { nullptr, 0, buffer_double_paged::i_index{0}  };
    eng_draw::pipeline                      m_DrawPipeLine      {};
    bool                                    m_bDrawBegin        {false};
    xirect                                  m_Scissor           {0,0,0,0};
};
