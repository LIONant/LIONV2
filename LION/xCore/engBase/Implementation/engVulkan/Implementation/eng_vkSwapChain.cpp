//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//---------------------------------------------------------------------------------
// Macro to get a procedure address based on a vulkan instance
//---------------------------------------------------------------------------------
#define GET_INSTANCE_PROC_ADDR(inst, entrypoint)                        \
{                                                                       \
    m_VK##entrypoint = (PFN_vk##entrypoint) vkGetInstanceProcAddr(inst, "vk"#entrypoint); \
    if( m_VK##entrypoint == nullptr )                                   \
    {                                                                   \
        static const err Error = x_error_code( errors, ERR_INSTANCE_PROC_FAILURE, "Could not get instance of proc " ##"vk"#entrypoint ) ; \
        std::cout << Error.getString() << ": " << " vkGetInstanceProcAddr failure";        \
        return Error;                                                   \
    }                                                                   \
}

//---------------------------------------------------------------------------------
// Macro to get a procedure address based on a vulkan device
//---------------------------------------------------------------------------------
#define GET_DEVICE_PROC_ADDR(dev, entrypoint)                           \
{                                                                       \
    m_VK##entrypoint = (PFN_vk##entrypoint) vkGetDeviceProcAddr(dev, "vk"#entrypoint);   \
    if( m_VK##entrypoint == nullptr )                                   \
    {                                                                   \
        static const err Error = x_error_code( errors, ERR_DEVICE_PROC_FAILURE, "Could not get instance of proc from device, " ##"vk"#entrypoint ) ; \
        std::cout << Error.getString() << ": " << "vkGetDeviceProcAddr failure";    \
        return Error;                                                   \
    }                                                                   \
}

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//---------------------------------------------------------------------------------
// Creates an os specific surface
// Tries to find a graphics and a present queue
swapchain::err swapchain::Initialize( const eng_window::setup& Setup, const window& Window ) noexcept
{
    VkResult VKErr;
    
    //
    // Connect to the instance and device and get all required function pointers
    //
    auto VKInstance         = m_Device.getVKInstance();
    auto VkDevice           = m_Device.getVKDevice();
    auto VKPhysicalDevice   = m_Device.getVKPhysicalDevice();

    GET_INSTANCE_PROC_ADDR( VKInstance, GetPhysicalDeviceSurfaceSupportKHR          );
    GET_INSTANCE_PROC_ADDR( VKInstance, GetPhysicalDeviceSurfaceCapabilitiesKHR     );
    GET_INSTANCE_PROC_ADDR( VKInstance, GetPhysicalDeviceSurfaceFormatsKHR          );
    GET_INSTANCE_PROC_ADDR( VKInstance, GetPhysicalDeviceSurfacePresentModesKHR     );
    GET_DEVICE_PROC_ADDR(   VkDevice,   CreateSwapchainKHR                          );
    GET_DEVICE_PROC_ADDR(   VkDevice,   DestroySwapchainKHR                         );
    GET_DEVICE_PROC_ADDR(   VkDevice,   GetSwapchainImagesKHR                       );
    GET_DEVICE_PROC_ADDR(   VkDevice,   AcquireNextImageKHR                         );
    GET_DEVICE_PROC_ADDR(   VkDevice,   QueuePresentKHR                             );

    //
    // Create surface depending on OS
    //
#ifdef _WIN32
    VkWin32SurfaceCreateInfoKHR surfaceCreateInfo = {};
    surfaceCreateInfo.sType         = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.hinstance     = (HINSTANCE)Setup.m_pInstance;
    surfaceCreateInfo.hwnd          = Window.getWindowHandle();
    VKErr = vkCreateWin32SurfaceKHR( VKInstance, &surfaceCreateInfo, nullptr, &m_VKSurface );

#elif defined __ANDROID__
    VkAndroidSurfaceCreateInfoKHR surfaceCreateInfo = {};
    surfaceCreateInfo.sType         = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.window        = m_pWindow;
    VKErr = vkCreateAndroidSurfaceKHR( VKInstance, &surfaceCreateInfo, NULL, &m_VKSurface );
#else
    VkXcbSurfaceCreateInfoKHR surfaceCreateInfo = {};
    surfaceCreateInfo.sType         = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.connection    = m_Connection;
    surfaceCreateInfo.window        = m_Window;
    VKErr = vkCreateXcbSurfaceKHR( VKInstance, &surfaceCreateInfo, nullptr, &m_VKSurface );
#endif

    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_SURFACE_CREATION, "Could not create OS native surface" ) ;
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    //
    // Get available queue family properties
    //
    uint32_t queueCount;
    vkGetPhysicalDeviceQueueFamilyProperties( VKPhysicalDevice, &queueCount, nullptr );
    if( queueCount == 0 )
    {
        static const err Error = x_error_code( errors, ERR_SURFACE_CREATION, "Could not query the physical device family properties" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }
        
    // Find all the physical device properties
    xndptr<VkQueueFamilyProperties>     queueProps;
    queueProps.New( queueCount );
    vkGetPhysicalDeviceQueueFamilyProperties( VKPhysicalDevice, &queueCount, &queueProps[0] );
    x_assert( queueProps.getCount() == queueCount );

    // Iterate over each queue to learn whether it supports presenting:
    // Find a queue with present support
    // Will be used to present the swap chain images to the windowing system
    xndptr<VkBool32> supportsPresent;
    supportsPresent.New( queueCount );

    for( uint32_t i = 0; i < queueCount; i++) 
    {
        m_VKGetPhysicalDeviceSurfaceSupportKHR( VKPhysicalDevice, i, m_VKSurface, &supportsPresent[i] );
    }

    // Search for a graphics and a present queue in the array of queue
    // families, try to find one that supports both
    uint32_t graphicsQueueNodeIndex = UINT32_MAX;
    uint32_t presentQueueNodeIndex  = UINT32_MAX;
    for (uint32_t i = 0; i < queueCount; i++) 
    {
        if( (queueProps[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0) 
        {
            if (graphicsQueueNodeIndex == UINT32_MAX) 
            {
                graphicsQueueNodeIndex = i;
            }

            if (supportsPresent[i] == VK_TRUE) 
            {
                graphicsQueueNodeIndex = i;
                presentQueueNodeIndex  = i;
                break;
            }
        }
    }

    if (presentQueueNodeIndex == UINT32_MAX) 
    {	
        // If there's no queue that supports both present and graphics
        // try to find a separate present queue
        for (uint32_t i = 0; i < queueCount; ++i) 
        {
            if (supportsPresent[i] == VK_TRUE) 
            {
                presentQueueNodeIndex = i;
                break;
            }
        }
    }

    // Exit if either a graphics or a presenting queue hasn't been found
    if (graphicsQueueNodeIndex == UINT32_MAX || presentQueueNodeIndex == UINT32_MAX) 
    {
        static const err Error = x_error_code( errors, ERR_SURFACE_CREATION, "Could not find a graphics and/or presenting queue!" );
        std::cout << Error.getString() << ": " << "Fatal Error";
        return Error;
    }

    // todo : Add support for separate graphics and presenting queue
    if (graphicsQueueNodeIndex != presentQueueNodeIndex) 
    {
        static const err Error = x_error_code( errors, ERR_SURFACE_CREATION, "Separate graphics and presenting queues are not supported yet!" );
        std::cout << Error.getString() << ": " << "Fatal Error";
        return Error;
    }

    m_QueueNodeIndex = graphicsQueueNodeIndex;

    // Get list of supported surface formats
    uint32_t formatCount;
    VKErr = m_VKGetPhysicalDeviceSurfaceFormatsKHR( VKPhysicalDevice, m_VKSurface, &formatCount, NULL );
    if( VKErr || formatCount == 0 )
    {
        static const err Error = x_error_code( errors, ERR_SURFACE_CREATION, "Error getting the PhysicalDevice SurfaceFormats" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }


    xndptr<VkSurfaceFormatKHR> surfaceFormats;
    surfaceFormats.New( formatCount );

    VKErr = m_VKGetPhysicalDeviceSurfaceFormatsKHR( VKPhysicalDevice, m_VKSurface, &formatCount, &surfaceFormats[0] );
    if( VKErr || formatCount == 0 || formatCount != surfaceFormats.getCount() )
    {
        static const err Error = x_error_code( errors, ERR_SURFACE_CREATION, "Error getting the PhysicalDevice SurfaceFormats [2]" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // If the surface format list only includes one entry with VK_FORMAT_UNDEFINED,
    // there is no preferered format, so we assume VK_FORMAT_B8G8R8A8_UNORM
    if ((formatCount == 1) && (surfaceFormats[0].format == VK_FORMAT_UNDEFINED))
    {
        m_VKColorFormat = VK_FORMAT_B8G8R8A8_UNORM;
    }
    else
    {
        // Always select the first available m_Color format
        // If you need a specific format (e.g. SRGB) you'd need to
        // iterate over the list of available surface format and
        // check for it's presence
        m_VKColorFormat = surfaceFormats[0].format;
    }

    m_VKColorSpace = surfaceFormats[0].colorSpace;

    return x_error_ok( );
}

//---------------------------------------------------------------------------------
// Create the swap chain and get images with given width and height
swapchain::err swapchain::Create( VkCommandBuffer cmdBuffer, int& Width, int& Height ) noexcept
{
    VkResult        VKErr;
    VkSwapchainKHR  oldSwapchain = m_VKSwapChain;

    // Get physical device surface properties and formats
    VkSurfaceCapabilitiesKHR surfCaps {};
    VKErr = m_VKGetPhysicalDeviceSurfaceCapabilitiesKHR( m_Device.getVKPhysicalDevice(), m_VKSurface, &surfCaps );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_SWAPCHAIN_CREATION, "Error getting the PhysicalDevice Surface Capabilities" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // Get available present modes
    uint32_t presentModeCount;
    VKErr = m_VKGetPhysicalDeviceSurfacePresentModesKHR( m_Device.getVKPhysicalDevice(), m_VKSurface, &presentModeCount, NULL );
    if( VKErr || presentModeCount == 0 )
    {
        static const err Error = x_error_code( errors, ERR_SWAPCHAIN_CREATION, "Error getting the PhysicalDevice Presets Modes" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    xndptr<VkPresentModeKHR> presentModes;
    presentModes.New(presentModeCount);

    VKErr = m_VKGetPhysicalDeviceSurfacePresentModesKHR( m_Device.getVKPhysicalDevice(), m_VKSurface, &presentModeCount, &presentModes[0] );
    if( VKErr || presentModeCount == 0 || presentModeCount != presentModes.getCount() )
    {
        static const err Error = x_error_code( errors, ERR_SWAPCHAIN_CREATION, "Error getting the PhysicalDevice Presets Modes [2]" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    VkExtent2D swapchainExtent = {};

    // width and height are either both -1, or both not -1.
    if( surfCaps.currentExtent.width == -1 )
    {
        // If the surface size is undefined, the size is set to
        // the size of the images requested.
        swapchainExtent.width  = Width;
        swapchainExtent.height = Height;
    }
    else
    {
        // If the surface size is defined, the swap chain size must match
        swapchainExtent = surfCaps.currentExtent;
        Width  = surfCaps.currentExtent.width;
        Height = surfCaps.currentExtent.height;
    }

    // If mailbox mode is available, use it, as is the lowest-latency non-
    // tearing mode.  If not, try IMMEDIATE which will usually be available,
    // and is fastest (though it tears).  If not, fall back to FIFO which is
    // always available.
    VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;
    //if(0) // This if 0 forces the VSync to be on
    for( uint32_t i = 0; i < presentModeCount; i++ ) 
    {
        if( presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR ) 
        {
            swapchainPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
            break;
        }
        if((swapchainPresentMode != VK_PRESENT_MODE_MAILBOX_KHR) && (presentModes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR)) 
        {
            swapchainPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
        }
    }

    // Determine the number of VkImage's to use in the swap chain (we desire to
    // own only 1 image at a time, besides the images being displayed and
    // queued for display):
    uint32_t desiredNumberOfSwapchainImages = 2;//surfCaps.minImageCount + 1;
    if ((surfCaps.maxImageCount > 0) && (desiredNumberOfSwapchainImages > surfCaps.maxImageCount))
    {
        // Application must settle for fewer images than desired:
        desiredNumberOfSwapchainImages = surfCaps.maxImageCount;
    }

    VkSurfaceTransformFlagsKHR preTransform;
    if (surfCaps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
    {
        preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }
    else 
    {
        preTransform = surfCaps.currentTransform;
    }

    VkSwapchainCreateInfoKHR swapchainCI = {};
    swapchainCI.sType                   = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCI.pNext                   = NULL;
    swapchainCI.surface                 = m_VKSurface;
    swapchainCI.minImageCount           = desiredNumberOfSwapchainImages;
    swapchainCI.imageFormat             = m_VKColorFormat;
    swapchainCI.imageColorSpace         = m_VKColorSpace;
    swapchainCI.imageExtent             = { swapchainExtent.width, swapchainExtent.height };
    swapchainCI.imageUsage              = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCI.preTransform            = (VkSurfaceTransformFlagBitsKHR)preTransform;
    swapchainCI.imageArrayLayers        = 1;
    swapchainCI.imageSharingMode        = VK_SHARING_MODE_EXCLUSIVE;
    swapchainCI.queueFamilyIndexCount   = 0;
    swapchainCI.pQueueFamilyIndices     = nullptr;
    swapchainCI.presentMode             = swapchainPresentMode;
    swapchainCI.oldSwapchain            = oldSwapchain;
    swapchainCI.clipped                 = true;
    swapchainCI.compositeAlpha          = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    VKErr = m_VKCreateSwapchainKHR( m_Device.getVKDevice(), &swapchainCI, nullptr, &m_VKSwapChain );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_SWAPCHAIN_CREATION, "Error Creating the Swapchain" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // If we just re-created an existing swapchain, we should destroy the old
    // swapchain at this point.
    // Note: destroying the swapchain also cleans up all its associated
    // presentable images once the platform is done with them.
    if( oldSwapchain != VK_NULL_HANDLE ) 
    { 
        m_VKDestroySwapchainKHR( m_Device.getVKDevice(), oldSwapchain, nullptr );
    }

    uint32_t    imageCount;
    VKErr = m_VKGetSwapchainImagesKHR( m_Device.getVKDevice(), m_VKSwapChain, &imageCount, NULL );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_SWAPCHAIN_CREATION, "Error Getting the Swap Chain Images" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // Get the swap chain images
    m_lImages.New( imageCount );
    VKErr = m_VKGetSwapchainImagesKHR( m_Device.getVKDevice(), m_VKSwapChain, &imageCount, &m_lImages[0] );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_SWAPCHAIN_CREATION, "Error Getting the Swap Chain Images [2]" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // Get the swap chain buffers containing the image and imageview
    m_lSCBuffers.New( imageCount );
    for( uint32_t i = 0; i < imageCount; i++ )
    {
        VkImageViewCreateInfo colorAttachmentView           = {};
        colorAttachmentView.sType                           = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        colorAttachmentView.pNext                           = nullptr;
        colorAttachmentView.format                          = m_VKColorFormat;
        colorAttachmentView.components                      = 
        {
            VK_COMPONENT_SWIZZLE_R,
            VK_COMPONENT_SWIZZLE_G,
            VK_COMPONENT_SWIZZLE_B,
            VK_COMPONENT_SWIZZLE_A
        };
        colorAttachmentView.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
        colorAttachmentView.subresourceRange.baseMipLevel   = 0;
        colorAttachmentView.subresourceRange.levelCount     = 1;
        colorAttachmentView.subresourceRange.baseArrayLayer = 0;
        colorAttachmentView.subresourceRange.layerCount     = 1;
        colorAttachmentView.viewType                        = VK_IMAGE_VIEW_TYPE_2D;
        colorAttachmentView.flags                           = 0;

        m_lSCBuffers[i].m_VKImage = m_lImages[i];

        // Transform images from initial (undefined) to present layout
        // Render loop will expect image to have been used before and in
        // VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
        // layout and will change to COLOR_ATTACHMENT_OPTIMAL, so init the image
        // to that state
        tools::setImageLayout(
            cmdBuffer, 
            m_lSCBuffers[i].m_VKImage, 
            VK_IMAGE_ASPECT_COLOR_BIT, 
            VK_IMAGE_LAYOUT_UNDEFINED, 
            VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

        colorAttachmentView.image = m_lSCBuffers[i].m_VKImage;

        VKErr = vkCreateImageView( m_Device.getVKDevice(), &colorAttachmentView, nullptr, &m_lSCBuffers[i].m_VKImageView );
        if( VKErr )
        {
            static const err Error = x_error_code( errors, ERR_SWAPCHAIN_CREATION, "Error creating an image view" );
            std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
            return Error;
        }
    }

    return x_error_ok( errors );
}

//---------------------------------------------------------------------------------

// Acquires the next image in the swap chain
VkResult swapchain::AcquireNextImage( VkSemaphore presentCompleteSemaphore, const uint32_t FrameIndex )  noexcept
{
    uint32_t currentBuffer = FrameIndex;
    return m_VKAcquireNextImageKHR( m_Device.getVKDevice(), 
                                    m_VKSwapChain, 
                                    UINT64_MAX, 
                                    presentCompleteSemaphore, 
                                    (VkFence)nullptr, 
                                    &currentBuffer );

    x_assert( FrameIndex == currentBuffer );
}

//---------------------------------------------------------------------------------

// Present the current image to the queue
VkResult swapchain::QueuePresent( VkQueue queue, uint32_t currentBuffer ) noexcept
{
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType               = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.pNext               = nullptr;
    presentInfo.swapchainCount      = 1;
    presentInfo.pSwapchains         = &m_VKSwapChain;
    presentInfo.pImageIndices       = &currentBuffer;

    return m_VKQueuePresentKHR( queue, &presentInfo );
}

//---------------------------------------------------------------------------------

// Free all Vulkan resources used by the swap chain
void swapchain::Cleanup( void ) noexcept
{
    for( auto& Buffer : m_lSCBuffers )
    {
        vkDestroyImageView( m_Device.getVKDevice(), Buffer.m_VKImageView, nullptr );
        Buffer.m_VKImageView = VK_NULL_HANDLE;
    }

    m_VKDestroySwapchainKHR( m_Device.getVKDevice(), m_VKSwapChain, nullptr );
    m_VKSwapChain = VK_NULL_HANDLE;

    vkDestroySurfaceKHR( m_Device.getVKInstance(), m_VKSurface, nullptr );
    m_VKSurface = VK_NULL_HANDLE;
}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}
