//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//------------------------------------------------------------------------------------

void sampler::Initialize( const setup& Setup, const device& Device ) noexcept
{
    //
    // Copy all the data to the base class
    //
    m_hTexture                  = const_cast<decltype(Setup.m_hTexture)&>(Setup.m_hTexture);
    m_Info                      = Setup;
              
    // Create sampler
    // In Vulkan textures are accessed by samplers
    // This separates all the sampling information from the 
    // texture data
    // This means you could have multiple sampler objects
    // for the same texture with different settings
    // Similar to the samplers available with OpenGL 3.3
    VkSamplerCreateInfo sampler = {};
    sampler.sType                   = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler.pNext                   = nullptr;
    sampler.magFilter               = ConvertSampler    ( m_Info.m_MipmapMag );
    sampler.minFilter               = ConvertSampler    ( m_Info.m_MipmapMin );
    sampler.mipmapMode              = ConvertMipmapMode ( m_Info.m_MipmapMode );
    sampler.addressModeU            = ConvertAddressMode( m_Info.m_AdressMode_U );
    sampler.addressModeV            = ConvertAddressMode( m_Info.m_AdressMode_V );
    sampler.addressModeW            = ConvertAddressMode( m_Info.m_AdressMode_W );
    sampler.mipLodBias              = 0.0f;
    sampler.compareOp               = VK_COMPARE_OP_NEVER;
    sampler.minLod                  = 0.0f;

    // Max level-of-detail should match mip level count
    sampler.maxLod                  = (float)m_hTexture->getMipCount();
    
    // Enable anisotropic filtering
    sampler.anisotropyEnable        = !m_Info.m_bDisableAnisotropy;
    sampler.maxAnisotropy           = m_Info.m_MaxAnisotropy;
    sampler.borderColor             = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
    
    auto VKErr = vkCreateSampler( Device.getVKDevice(), &sampler, nullptr, &m_VKSampler );
    x_assert( !VKErr );
}

//------------------------------------------------------------------------------------

void sampler::Release( void ) noexcept
{
    auto& Texture = reinterpret_cast<texture&>(*m_hTexture);
    auto& Device  = Texture.getTheDevice();
    Device.ReleaseSampler( *this );
}




//------------------------------------------------------------------------------------
/*
void sampler::Bind( 
    const device&           Device,
    VkDescriptorSet         VKDescriptorSet, 
    const int               Binding ) noexcept
{
    // Image descriptor for the color map texture
    xarray<VkDescriptorImageInfo,1> DescriptorImageInfo
    {
        getDescriptorImageInfo()
    };

    // Bind the texture
    VkWriteDescriptorSet writeDescriptorSet = {};
    writeDescriptorSet.sType                = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSet.pNext                = nullptr;
    writeDescriptorSet.dstSet               = VKDescriptorSet;
    writeDescriptorSet.descriptorCount      = 1;
    writeDescriptorSet.descriptorType       = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writeDescriptorSet.pImageInfo           = DescriptorImageInfo;
    writeDescriptorSet.dstBinding           = Binding;

    vkUpdateDescriptorSets( Device.getVKDevice(), 1, &writeDescriptorSet, 0, nullptr );
}
*/
//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}


