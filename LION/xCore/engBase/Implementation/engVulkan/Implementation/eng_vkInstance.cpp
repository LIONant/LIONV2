//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

  #pragma comment(lib, "3rdParty/Vulkan/Bin/vulkan-1")

//------------------------------------------------------------------------------------
// Link with the system
//------------------------------------------------------------------------------------
x_import extern "C" 
xuptr eng_InternalCreateInstance( void* pInstance, const void* pSetup, void* pxGlobalContext )
{
    //
    // Make sure that we hooked up with xcore first
    //
    #if _X_TARGET_DYNAMIC_LIB
        x_assert(pxGlobalContext);
        g_context::Import( *reinterpret_cast<g_context*>(pxGlobalContext) );
    #endif

    //
    // Get the rest of the variables
    //
    x_assert(pSetup);
    const eng_instance::setup&      Setup       = *reinterpret_cast<const eng_instance::setup*>(pSetup);
    
    x_assert(pxGlobalContext);
    xndptr_s<eng_vk::instance>&     Instance    = *reinterpret_cast<xndptr_s<eng_vk::instance>*>(pInstance);  

    //
    // Create our new instance
    //
    Instance.New();
    auto Err = Instance->Initialize( Setup );
    if( Err ) return 0;
    return reinterpret_cast<xuptr>( Err.getString() );
}


//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------

eng_instance::err eng_instance::CreateDevice( eng_device*& pDevice, const eng_device::setup& Setup ) noexcept
{
    return reinterpret_cast<eng_vk::instance*>(this)->CreateDevice( pDevice, Setup );
}


//------------------------------------------------------------------------------------

eng_instance::err eng_instance::CreateWindow( eng_window*& pWindow, const eng_window::setup& Setup ) noexcept
{
    return reinterpret_cast<eng_vk::instance*>(this)->CreateWindow( pWindow, Setup );
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//---------------------------------------------------------------------------------

VkResult instance::CreateVKInstance( const bool enableValidation, const char* pAppName ) noexcept
{
    VkApplicationInfo appInfo   = {};
    appInfo.sType               = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName    = pAppName;
    appInfo.pEngineName         = "LION";

    // Temporary workaround for drivers not supporting SDK 1.0.3 upon launch
    // todo : Use VK_API_VERSION 
    appInfo.apiVersion = VK_MAKE_VERSION( 1, 0, VK_HEADER_VERSION );


    int                        nEnabledExtensions = 0;
    xarray<const char*,8>      pEnabledExtensions;
             
    pEnabledExtensions[nEnabledExtensions++]     = VK_KHR_SURFACE_EXTENSION_NAME;

#ifdef _WIN32
        pEnabledExtensions[nEnabledExtensions++] = VK_KHR_WIN32_SURFACE_EXTENSION_NAME;
#else
        pEnabledExtensions[nEnabledExtensions++] = VK_KHR_XCB_SURFACE_EXTENSION_NAME;
#endif

    if( enableValidation )
        pEnabledExtensions[nEnabledExtensions++] = VK_EXT_DEBUG_REPORT_EXTENSION_NAME;


    // todo : check if all extensions are present
    xvector<const char*> ValidationLayers;
    VkInstanceCreateInfo instanceCreateInfo = {};
    instanceCreateInfo.sType                    = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pNext                    = NULL;
    instanceCreateInfo.pApplicationInfo         = &appInfo;
    instanceCreateInfo.enabledExtensionCount    = nEnabledExtensions;
    instanceCreateInfo.ppEnabledExtensionNames  = &pEnabledExtensions[0];

    if (enableValidation)
    {
        debug::getValidationLayers( *this, ValidationLayers );

        // todo : change validation layer names!
        instanceCreateInfo.enabledLayerCount    = static_cast<uint32_t>(ValidationLayers.getCount()); 
        instanceCreateInfo.ppEnabledLayerNames  = ValidationLayers;
    }

    m_bVerification = enableValidation;

    return vkCreateInstance( &instanceCreateInfo, nullptr, &m_VkInstance );
}

//---------------------------------------------------------------------------------

instance::err instance::EnumerateDevices( xndptr<VkPhysicalDevice>& PhysicalDevices ) noexcept
{
    VkResult    VKErr;
    uint32_t    gpuCount = 0;

    //
    // Get number of available physical devices
    //
    VKErr = vkEnumeratePhysicalDevices( m_VkInstance, &gpuCount, nullptr );
    if( VKErr || gpuCount == 0 )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Vulkan Could not enumerate phyiscal devices with a count" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    //
    // Enumerate devices
    //
    PhysicalDevices.New( (int)gpuCount );
        
    VKErr = vkEnumeratePhysicalDevices( m_VkInstance, &gpuCount, &PhysicalDevices[0] );    
    if( VKErr || gpuCount == 0 )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Vulkan Could not enumerate phyiscal devices" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    return x_error_ok();
}

//---------------------------------------------------------------------------------

instance::err instance::DeviceProperties( 
    xndptr<VkQueueFamilyProperties>&    DeviceProps, 
    int&                                iDeviceQueue, 
    const VkPhysicalDevice&             PhysicalDevice, 
    const VkQueueFlagBits               VKQFlagBits ) noexcept
{
    // Find a queue that supports graphics operations
    uint32_t queueCount         = 0;

    vkGetPhysicalDeviceQueueFamilyProperties( PhysicalDevice, &queueCount, NULL );
    if( queueCount <= 0 )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Could not get device properties" );
        std::cout << Error.getString() << ": " << "Count of properties less than expected";
        return Error;
    }

    DeviceProps.New( queueCount );

    vkGetPhysicalDeviceQueueFamilyProperties( PhysicalDevice, &queueCount, &DeviceProps[0] );
    if( queueCount <= 0 )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Could not get device properties" );
        std::cout << Error.getString() << ": " << "The actual data for them";
        return Error;
    }

    //
    // Make sure that ones of the properties is graphics
    //            
    for( auto& Prop : DeviceProps )
    {
        if( Prop.queueFlags & VKQFlagBits )
        {
            iDeviceQueue = int(&Prop - &DeviceProps[0]);
            break;
        }
	}

    if( iDeviceQueue == -1 )
    {
        DeviceProps.Delete();
    }

    return x_error_ok();
}

//------------------------------------------------------------------------------------

void instance::CreateLogConsole( void ) const noexcept
{
    AllocConsole();
    AttachConsole(GetCurrentProcessId());
    // freopen("CON", "w", stdout);

    FILE* stream;
    freopen_s( &stream, "CON", "w", stdout );
                  
    // SetConsoleTitle( TEXT( title.c_str() ) );

    if( m_bVerification )
    {
        std::cout << "Validation enabled:\n";
    }
}

//------------------------------------------------------------------------------------

instance::err instance::Initialize( const eng_instance::setup& Setup ) noexcept
{
    //
    // Setup the program instance
    //
    m_hInstance = (HINSTANCE)Setup.m_AppInstance;

    //
    // Create the denug console
    //
    CreateLogConsole();

    //
    // Initialize Vulkan
    //
    auto VKErr = CreateVKInstance( Setup.m_bValidation, Setup.m_pAppName );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Error Initializing Vulkan" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    //
    // Setup the validation
    //
    if( Setup.m_bValidation )
    {
        debug::setupDebugging( m_VkInstance, VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT, nullptr );
    }

    //
    // Enumerate all the devices
    //
    auto Err = EnumerateDevices( m_PhysicalDevices );
    if( Err ) return Err;

    // Just allocate a few
    m_lDevices.GrowBy( m_PhysicalDevices.getCount() );

    //
    // Done
    //              
    return x_error_ok();
}

//------------------------------------------------------------------------------------

instance::err instance::CreateDevice( eng_device*& pDevice, const eng_device::setup& Setup ) noexcept
{
    static const xarray<VkQueueFlagBits, eng_device::DEVICE_TYPE_COUNT> QueueType = []() -> auto
    {
        xarray<VkQueueFlagBits, eng_device::DEVICE_TYPE_COUNT> QueueType { VK_QUEUE_SPARSE_BINDING_BIT };

        QueueType[ eng_device::DEVICE_TYPE_RENDER       ]    = VK_QUEUE_GRAPHICS_BIT;
        QueueType[ eng_device::DEVICE_TYPE_COMPUTE_ONLY ]    = VK_QUEUE_COMPUTE_BIT;
        QueueType[ eng_device::DEVICE_TYPE_COPY         ]    = VK_QUEUE_TRANSFER_BIT;

        return QueueType;
    }();

    xndptr<VkQueueFamilyProperties>     DeviceProps     {};
    auto&                               Device          = m_lDevices.append();
    int                                 iDeviceQueue    = -1;
    xuptr                               iPhysicalDevice =  0;
    err                                 Err;
    
    //
    // Search for the right device
    //
    for( auto& PhysicalDevice : x_iter_ref( iPhysicalDevice, m_PhysicalDevices ) )
    {
        Err = DeviceProperties( DeviceProps, iDeviceQueue, m_PhysicalDevices[ iPhysicalDevice ], QueueType[Setup.m_DeviceType] );
        if( Err ) return Err;
        if( iDeviceQueue != -1 ) break;
    }

    // If we can not find the device the let the user know
    if( iDeviceQueue == -1 )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Could not find the right properties for the selected device" );
        std::cout << Error.getString() << ": " << "";
        return Error;
    }

    //
    // Ok create an entry for this device
    //
    Device.New( *this );

    if( Device->Initialize( DeviceProps, iDeviceQueue, m_PhysicalDevices[ iPhysicalDevice ], m_bVerification ).isError() ) 
        return x_error_code( errors, ERR_FAILURE, "Fail to initialize the vulkan device" );

    //
    // Done
    // 
    pDevice = &Device[0];

    return x_error_ok();
}

//------------------------------------------------------------------------------------

instance::err instance::CreateWindow( eng_window*& pWindow, const eng_window::setup& Setup ) noexcept
{
    //
    // Allocate the window
    //
    auto& Window = m_lWindows.append();

    auto& Device = *reinterpret_cast<device*>(&Setup.m_Device);
    Window.New( Device );

    //
    // Initialize the window
    //
    if( Window->Initialize( Setup ).isError() )
        return x_error_code( errors, ERR_FAILURE, "Fail to initialize the swapchain" );

    //
    // Set the returning value
    //
    pWindow = &Window[0];

    return x_error_ok();
}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}
