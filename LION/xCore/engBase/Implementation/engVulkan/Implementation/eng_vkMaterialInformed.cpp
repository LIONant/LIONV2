//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//------------------------------------------------------------------------------------

void material_informed::Initialize( const setup& Setup ) noexcept
{
    //
    // Get the handle for the material type
    // as way as the object
    //
    m_hMaterialType = const_cast<decltype(Setup.m_hMaterialType)&>( Setup.m_hMaterialType );
    auto& MaterialType = reinterpret_cast<material_type&>(*m_hMaterialType);

    // Allocate the descriptor set
    m_VKDescriptorSet = MaterialType.AllocDescriptorSet();

    //
    // Fill in the descriptor set
    //
    xarray<VkDescriptorImageInfo, 16>   ImageInfo           {};
    xarray<VkWriteDescriptorSet, 16>    WriteDescritorSet   {};
    xuptr                               i                   {0};
    for( auto& Node : x_iter_ref( i, Setup.m_SamplerBindings ) )
    {
        sampler& Sampler = reinterpret_cast<sampler&>( *const_cast<decltype( Node.m_hSampler )&>( Node.m_hSampler ) );

        // Image descriptor for the color map texture
        ImageInfo[i] = Sampler.getDescriptorImageInfo();

        // Bind the texture
        auto& writeDescriptorSet = WriteDescritorSet[i];
        writeDescriptorSet.sType                = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescriptorSet.pNext                = nullptr;
        writeDescriptorSet.dstSet               = m_VKDescriptorSet;
        writeDescriptorSet.descriptorCount      = 1;
        writeDescriptorSet.descriptorType       = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writeDescriptorSet.dstArrayElement      = 0;
        writeDescriptorSet.pImageInfo           = &ImageInfo[i];
        writeDescriptorSet.dstBinding           = Node.m_iBind;
        writeDescriptorSet.pBufferInfo          = nullptr;
        writeDescriptorSet.pTexelBufferView     = nullptr;
    }

    vkUpdateDescriptorSets( 
        m_Device.getVKDevice(), 
        static_cast<uint32_t>(Setup.m_SamplerBindings.getCount()), 
        WriteDescritorSet, 
        0, 
        nullptr );
}

//------------------------------------------------------------------------------------

void material_informed::Release( void ) noexcept
{
    m_Device.ReleaseMaterialInformed( *this );
}


//------------------------------------------------------------------------------------

inline u32 ComputeMixHash( const u32 HashPipeline, const xuptr RenderPass, const u32 Primitive )
{
    return HashPipeline ^ static_cast<u32>(x_MurmurHash3( RenderPass ) ^ (x_MurmurHash3( RenderPass )>>32)) ^ Primitive; 
}

//------------------------------------------------------------------------------------

VkPipeline material_informed::getVKPipeLine( 
    VkRenderPass                    RenderPass, 
    const eng_pipeline::handle&     hPipeline,
    bool                            bDrawPolys ) noexcept
{
    auto&       EngPipeLine     = HandleToObject<engpipeline>(hPipeline);
    
    const u32   MixHash         = ComputeMixHash(
        EngPipeLine.getHash(),
        reinterpret_cast<xuptr>(RenderPass),
        bDrawPolys
    );

    auto& PipeLine      = m_Device.getRenderPipelineHash().getPipeLine( 
        RenderPass,
        *this,
        EngPipeLine,
        MixHash );

    return PipeLine.getVkPipeline();
}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}


