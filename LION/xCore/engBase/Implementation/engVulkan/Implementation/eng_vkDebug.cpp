//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{
namespace debug {

//------------------------------------------------------------------------------------
static
void FilterValidationLayers( xvector<const char*>& Layers, xbuffer_view<const char*> FilterView )
{
    uint32_t    ValidationLayerCount = -1;
    auto        VKErr = vkEnumerateInstanceLayerProperties( &ValidationLayerCount, nullptr );
    x_assert( !VKErr );

    xndptr<VkLayerProperties> LayerProperties;
    LayerProperties.New( ValidationLayerCount );

    VKErr = vkEnumerateInstanceLayerProperties( &ValidationLayerCount, &LayerProperties[0] );
    x_assert( !VKErr );

    //
    // First try filtering from list 1
    //    
    Layers.DeleteAllEntries();
    for( const auto& LayerEntry : LayerProperties )
        for( auto pFilterEntry : FilterView )
            if( strcmp( pFilterEntry, LayerEntry.layerName ) == 0 )
            {
                Layers.append() = pFilterEntry;
                break;
            }
}


//------------------------------------------------------------------------------------

void getValidationLayers( instance& Instance, xvector<const char*>& Layers )
{
    x_constexprvar xarray<const char*,1> s_ValidationLayerNames_Alt1  
    { 
        "VK_LAYER_LUNARG_standard_validation",
        //"VK_LAYER_RENDERDOC_Capture"
    };

    x_constexprvar xarray<const char*,8> s_ValidationLayerNames_Alt2 =  
    {
        "VK_LAYER_GOOGLE_threading",     "VK_LAYER_LUNARG_parameter_validation",
        "VK_LAYER_LUNARG_device_limits", "VK_LAYER_LUNARG_object_tracker",
        "VK_LAYER_LUNARG_image",         "VK_LAYER_LUNARG_core_validation",
        "VK_LAYER_LUNARG_swapchain",     "VK_LAYER_GOOGLE_unique_objects",
        //"VK_LAYER_RENDERDOC_Capture"
    };

    // Try alt list first 
    FilterValidationLayers( Layers, s_ValidationLayerNames_Alt1 );
    if( Layers.getCount() == s_ValidationLayerNames_Alt1.getCount() )
        return;

    // if we are not successfull then try our best with the second version
    FilterValidationLayers( Layers, s_ValidationLayerNames_Alt2 );
    if( Layers.getCount() == s_ValidationLayerNames_Alt2.getCount() )
    {
        ENG_WARNINGLOG( Instance, "Fail to get all the validation layers that we wanted");
    }
}

//------------------------------------------------------------------------------------

std::string VulkanErrorString(VkResult errorCode)
{
    switch (errorCode)
    {
        // todo : update to SDK 0.10.1
#define STR(r) case VK_ ##r: return #r
        STR(NOT_READY);
        STR(TIMEOUT);
        STR(EVENT_SET);
        STR(EVENT_RESET);
        STR(INCOMPLETE);
        STR(ERROR_OUT_OF_HOST_MEMORY);
        STR(ERROR_OUT_OF_DEVICE_MEMORY);
        STR(ERROR_INITIALIZATION_FAILED);
        STR(ERROR_DEVICE_LOST);
        STR(ERROR_MEMORY_MAP_FAILED);
        STR(ERROR_LAYER_NOT_PRESENT);
        STR(ERROR_EXTENSION_NOT_PRESENT);
        STR(ERROR_INCOMPATIBLE_DRIVER);
#undef STR
    default:
        return "UNKNOWN_ERROR";
    }
}

//------------------------------------------------------------------------------------

VkBool32 messageCallback(
    VkDebugReportFlagsEXT       flags,
    VkDebugReportObjectTypeEXT  objType,
    uint64_t                    srcObject,
    size_t                      location,
    int32_t                     msgCode,
    const char*                 pLayerPrefix,
    const char*                 pMsg,
    void*                       pUserData )
{
    char* message = x_malloc(char, strlen(pMsg) + 100);
    x_assert(message);

    if ( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT )
    {
        std::cout << "ERROR: " << "[" << pLayerPrefix << "] Code " << msgCode << " : " << pMsg << "\n";
    }
    else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
    {
        // Uncomment to see warnings
        std::cout << "WARNING: " << "[" << pLayerPrefix << "] Code " << msgCode << " : " << pMsg << "\n";
    }
    else
    {
        return false;
    }

    fflush(stdout);

    x_free(message);
    return false;
}

//------------------------------------------------------------------------------------

bool setupDebugging( VkInstance instance, VkDebugReportFlagsEXT flags, VkDebugReportCallbackEXT callBack )
{
    //VkDebugReportCallbackEXT dbgBreakCallback            = (PFN_vkDebugReportMessageEXT)vkGetInstanceProcAddr        ( instance, "vkDebugReportMessageEXT"           );

    VkDebugReportCallbackCreateInfoEXT dbgCreateInfo;
    dbgCreateInfo.sType         = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
    dbgCreateInfo.pNext         = NULL;
    dbgCreateInfo.pfnCallback   = (PFN_vkDebugReportCallbackEXT)messageCallback;
    dbgCreateInfo.pUserData     = NULL;
    dbgCreateInfo.flags         = flags;

    PFN_vkCreateDebugReportCallbackEXT  CreateDebugReportCallback   = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr ( instance, "vkCreateDebugReportCallbackEXT"    );
    VkDebugReportCallbackEXT            debugReportCallback;
         
    if(CreateDebugReportCallback)
    {
        VkResult err = CreateDebugReportCallback(
                        instance,
                        &dbgCreateInfo,
                        NULL,
                        &debugReportCallback );

        if(!err) return true;
    }

    return false;
}
    
//------------------------------------------------------------------------------------

void freeDebugCallback( VkInstance instance )
{
    // if (msgCallback != nullptr)
    {
        // PFN_vkDestroyDebugReportCallbackEXT DestroyDebugReportCallback  = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr( instance, "vkDestroyDebugReportCallbackEXT"   );
        // if(DestroyDebugReportCallback) DestroyDebugReportCallback( instance, msgCallback, nullptr );
    }
}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}
}
