//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-----------------------------------------------------------------------
inline                      
memory_pool::~memory_pool( void ) noexcept  
{
    for( auto& Mem : m_lDeviceAllocations )
        vkFreeMemory( m_Device.getVKDevice(), Mem.m_VKDeviceMemory, nullptr );
}

//-----------------------------------------------------------------------
inline                      
void memory_pool::Free( const allocation& Allocation ) noexcept
{
    // Limit acccess here
    x_lk_guard( m_SpinLock );
    x_assert_linear( m_Debug_LQ );
     
    for( auto& DeviceMemEntry : m_lDeviceAllocations )
    {
        // Search the good memory device
        if( DeviceMemEntry.m_VKDeviceMemory != Allocation.m_VKDeviceMemory )
            continue;
            
        // Search the good offset
        for( auto& Block : DeviceMemEntry.m_lBlocks )
        {
            if( Block.m_Offset == Allocation.m_Offset )
            {
                x_assert( Block.m_isFree == false );
                Block.m_isFree = true;
                return;
            }
        }
    }

    // Coult not find the block to free
    x_assert( false );
}

//-----------------------------------------------------------------------
inline                      
memory_pool::err memory_pool::Alloc( allocation& Allocation, const VkDeviceSize aSize, const VkMemoryPropertyFlags VKMemPropFlags ) noexcept
{
    // Limit acccess here
    x_lk_guard( m_SpinLock );
    x_assert_linear( m_Debug_LQ );

    // 128 bytes alignment
    const auto Size = x_Align( aSize, 128 );
    x_assert( x_isAlign( Size, 128 ) );
 
    for( auto& DeviceMemEntry : m_lDeviceAllocations ) 
    {
        // if flags are okay
        if( (DeviceMemEntry.m_VKMemPropFlags & VKMemPropFlags) == VKMemPropFlags ) 
        {
            // We are looking for a good block
            xuptr i = 0;
            for( auto& BlockEntry : x_iter_ref( i, DeviceMemEntry.m_lBlocks ) ) 
            {
                if( BlockEntry.m_isFree && (BlockEntry.m_Size >= Size) ) 
                    break;
            }
 
            // If a block is found
            x_assert( i <= DeviceMemEntry.m_lBlocks.getCount() );
            if( i != DeviceMemEntry.m_lBlocks.getCount() ) 
            {
                const auto  NewBlockSize    = DeviceMemEntry.m_lBlocks[i].m_Size - Size;
                 
                // Modify the current block
                DeviceMemEntry.m_lBlocks[i].m_isFree     = false;
                DeviceMemEntry.m_lBlocks[i].m_Size       = Size;

                // Set the new block
                if( NewBlockSize != 0 )
                {
                    // Note that due to have this insert we can not have a reference to the DeviceMemEntry.m_lBlocks[i]
                    // since we may reallocate!
                    auto& NewBlock = DeviceMemEntry.m_lBlocks.Insert( i + 1 );

                    NewBlock.m_isFree       = true;
                    NewBlock.m_Size         = NewBlockSize;
                    NewBlock.m_Offset       = DeviceMemEntry.m_lBlocks[i].m_Offset       + Size;
                    NewBlock.m_pSystemPtr   = DeviceMemEntry.m_lBlocks[i].m_pSystemPtr   + Size;
                }

                // Fill the allocation information
                Allocation.m_VKDeviceMemory = DeviceMemEntry.m_VKDeviceMemory;
                Allocation.m_Size           = Size;
                Allocation.m_Offset         = DeviceMemEntry.m_lBlocks[i].m_Offset;
                Allocation.m_pSystemPtr     = DeviceMemEntry.m_lBlocks[i].m_pSystemPtr;

                return x_error_ok();
            }
        }
    }

    // if we reach there, we have to allocate a new chunk
    device_mem_allocation DeviceAlloc;
    err               Err = DeviceAllocate( DeviceAlloc, VKMemPropFlags, x_Max( Size, CHUNCK_DEFAULT_ALLOCATION_SIZE ) ); 
    if( Err ) return Err;
    AddDeviceMemory( DeviceAlloc );

    // Try to find a new allocation since we found a new chunck
    return Alloc( Allocation, Size, VKMemPropFlags );
}

//-------------------------------------------------------------------------------  
inline                      
void memory_pool::AddDeviceMemory( device_mem_allocation& Alloc ) noexcept
{
    x_assert_linear( m_Debug_LQ );

    auto&   DeviceMemEntry  = m_lDeviceAllocations.append();
    auto&   Block           = DeviceMemEntry.m_lBlocks.append();

    DeviceMemEntry.m_VKMemPropFlags  = Alloc.m_VKMemoryPropFlags;
    DeviceMemEntry.m_VKDeviceMemory  = Alloc.m_VKDeviceMemory;
    DeviceMemEntry.m_VKDeviceSize    = Alloc.m_VKDeviceSize;
    DeviceMemEntry.m_pSystemPtr      = Alloc.m_pSystemPtr;

    // Add a block mapped along the whole chunk
    Block.m_isFree          = true;
    Block.m_Offset          = 0;
    Block.m_Size            = Alloc.m_VKDeviceSize;
    Block.m_pSystemPtr      = Alloc.m_pSystemPtr;
}

//-------------------------------------------------------------------------------  
inline                      
memory_pool::err memory_pool::DeviceAllocate( device_mem_allocation& Alloc, VkMemoryPropertyFlags VKMemoryPropFlags, VkDeviceSize Size ) noexcept
{
    x_assert_linear( m_Debug_LQ );

    const VkPhysicalDeviceMemoryProperties& Property    = m_Device.getVKDeviceMemProps();
    xuptr                                   Index       = ~0;
 
    //
    // Looking throw vulkan list of heaps for a good match
    //
    for( xuptr i=0; i < Property.memoryTypeCount; ++i )
    { 
        if( (Property.memoryTypes[i].propertyFlags & VKMemoryPropFlags) != VKMemoryPropFlags )
            continue;

        if( Size < Property.memoryHeaps[Property.memoryTypes[i].heapIndex].size )
        {
            Index = i;
            break;
        }
    }
 
    if( Index == ~0 )
        return x_error_code( error, ERR_DEVICE_MEMORY_FAIL, "Unable to find a compatible memory pool in the device" );
 
    //
    // Allocate from vulkan heap
    //
    VkMemoryAllocateInfo Info = {};
    Info.sType              = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    Info.pNext              = nullptr;
    Info.allocationSize     = Size;
    Info.memoryTypeIndex    = static_cast<uint32_t>(Index);
 
    // Perform the allocation
    auto VKErr = vkAllocateMemory( 
        m_Device.getVKDevice(), 
        &Info, 
        nullptr, 
        &Alloc.m_VKDeviceMemory );
    x_assert( VKErr == 0 );
 
    //
    // Map the memory into system memory if it is host visible
    //
    // COMMENTED SINCE WE ARE GOING TO ALLOW INDIVIDUAL BUFFERS TO MAP THE MEMORY THEMSELVES
    if( VKMemoryPropFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT )
    {
        VKErr = vkMapMemory( 
            m_Device.getVKDevice(), 
            Alloc.m_VKDeviceMemory, 
            0, 
            VK_WHOLE_SIZE, 
            0, 
            (void**)&Alloc.m_pSystemPtr );
        x_assert( VKErr == 0 );
    }
    else
    {
        Alloc.m_pSystemPtr = nullptr;
    }

    //
    // Fill the rest of the information for the user about the allocation
    //
    Alloc.m_VKDeviceSize        = Size;
    Alloc.m_VKMemoryPropFlags   = VKMemoryPropFlags;

    return x_error_ok();
}



