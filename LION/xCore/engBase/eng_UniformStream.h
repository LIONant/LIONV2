//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


class eng_uniform_stream : public eng_base
{
public:

    using handle    = x_ll_share_ref<eng_uniform_stream>;

    enum class entry_type : u8
    {
        SINGLE,                             // There is only one entry for this uniform stream
        ARRAY_FIXED_SIZE,                   // The stream is made of a fixed array of entries
        ARRAY_DYNAMIC_SIZE,                 // the stream is made of variable arrays of entries
        ENUM_COUNT
    };

    enum class stream_type : u8
    {
        CONSTANT,                           // The values will be set only one time and never touched again
        MUTABLE,                            // values can change every frame
        ENUM_COUNT
    };

    struct definition
    {
        int         m_iBind             { -1 };                         // Where is this entry supose to bind in the shader
        int         m_EntryByteSize     { -1 };                         // How big is an entry
        entry_type  m_EntryType         { entry_type::SINGLE };         // Type of the entries 
        stream_type m_Type              { stream_type::CONSTANT };      // Type of the stream
    };

    struct bind_range
    {
        int         m_iBind             { -1 };                         // Which entry this is bound
        int         m_iStart            { -1 };                         // start entry
        int         m_Count             { -1 };                         // how many entries are we sending over
    };

    struct info
    {
        int         m_EntryByteSize     { -1 };                         // How big is each entry
        int         m_EntryCount        { -1 };                         // How many entries does this stream has
        entry_type  m_EntryType         { entry_type::SINGLE };         // Type of the entries 
        stream_type m_Type              { stream_type::CONSTANT };      // Type of the stream
    };

    struct setup : public info
    {
    };

public:

    constexpr           const info&     getInfo                     ( void )                            const   noexcept { return m_Info; }

protected:

    info        m_Info;
};
