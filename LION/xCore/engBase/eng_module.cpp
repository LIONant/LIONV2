
// Common CPP files
#include "Implementation/eng_Device.cpp"
#include "Implementation/eng_Draw.cpp"
#include "Implementation/eng_Input.cpp"
//#include "Implementation/eng_Instance.cpp"

// Taget specific files
#ifdef _X_TARGET_WINDOWS
    #include "Implementation/engPC/eng_PCSystemWindow.cpp"
#endif 

// Vulkan related files
//#include "Implementation/engVulkan/eng_vkBase.h"
#include "Implementation/engVulkan/Implementation/eng_vkBuffer.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkDebug.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkDevice.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkDraw.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkEngPipeline.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkInstance.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkMaterialInformed.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkMaterialType.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkPipeline.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkSampler.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkShader.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkSwapChain.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkTexture.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkTools.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkUniformStream.cpp"
#include "Implementation/engVulkan/Implementation/eng_vkWindow.cpp"


