//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_sampler : public eng_base
{
public:

    using handle    = x_ll_share_ref<eng_sampler>;

    enum address_mode : u8
    {
        ADDRESS_MODE_WRAP,
        ADDRESS_MODE_CLAMP,
        ADDRESS_MODE_BORDER,
        ADDRESS_MODE_MIRROR,
        ADDRESS_MODE_MIRROR_CLAMP,
        ADDRESS_MODE_COUNT
    };

    enum mipmap_sampler
    {
        MIPMAP_SAMPLER_LINEAR,
        MIPMAP_SAMPLER_NEAREST,
        MIPMAP_SAMPLER_COUNT
    };

    enum mipmap_mode
    {
        MIPMAP_MODE_LINEAR,
        MIPMAP_MODE_NEAREST,
        MIPMAP_MODE_COUNT
    };

    struct info
    {
        float               m_MaxAnisotropy         { 8.0f  };
        bool                m_bDisableAnisotropy    { false };
        address_mode        m_AdressMode_U          { ADDRESS_MODE_WRAP  };
        address_mode        m_AdressMode_V          { ADDRESS_MODE_WRAP  };
        address_mode        m_AdressMode_W          { ADDRESS_MODE_WRAP  };
        mipmap_sampler      m_MipmapMag             { MIPMAP_SAMPLER_LINEAR };
        mipmap_sampler      m_MipmapMin             { MIPMAP_SAMPLER_LINEAR };
        mipmap_mode         m_MipmapMode            { MIPMAP_MODE_LINEAR };
    };

    struct setup : public info
    {
        setup( void ) = delete;
        setup( eng_texture::handle& hTexture ) : m_hTexture{ hTexture } {}
        
        eng_texture::handle m_hTexture;              
    };

public:

//    constexpr                       eng_sampler                 ( void )                        noexcept = default;
    x_forceconst            const info&     getInfo                     ( void )                        const   noexcept    { return m_Info; }
    x_forceconst            const auto&     getTextureHandle            ( void )                        const   noexcept    { return m_hTexture; }

protected:

    x_forceinline static    void            t_DeleteSharedInstance      ( eng_sampler* const pData )            noexcept    { pData->Release(); }
    virtual                 void            Release                     ( void )                                noexcept = 0;

protected:

    eng_texture::handle m_hTexture              {};
    info                m_Info                  {};

protected:

    friend class x_ll_share_object;
};




