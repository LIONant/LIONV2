//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#ifndef _ENG_BASE_H
#define _ENG_BASE_H

//------------------------------------------------------------------------------
// system includes
//------------------------------------------------------------------------------

#ifdef _WIN32
    #pragma comment(linker, "/subsystem:windows")
    #include <windows.h>
//    #include <fcntl.h>
//    #include <io.h>

#ifdef CreateWindow
    #undef CreateWindow
#endif

#else 
    // todo : split linux xcb/x11 and android
    #include <xcb/xcb.h>
#endif

//------------------------------------------------------------------------------
// Implementer:
//     Tomas Arce
// Algorithm:
//      Tomas Arce
// Description:
//      The official log system for the scheduler
//------------------------------------------------------------------------------

#include "xCore/xBase/x_base.h"
#define _ENG_TARGET_VULKAN

class eng_base : public x_ll_share_object
{
    x_object_type( eng_base, is_quantum_lock_free );

public:

    virtual ~eng_base( void ) noexcept {}

protected:

    inline      static  void        t_DeleteSharedInstance      ( eng_base* const pData )       noexcept { pData->Release(); }
    virtual             void        Release                     ( void )                        noexcept = 0;

protected:

    friend class x_ll_share_object;
};

class eng_draw;

#include "eng_Pipeline.h"
#include "eng_Texture.h"
#include "eng_UniformStream.h"
#include "eng_Sampler.h"
#include "eng_Shader.h"
#include "eng_MaterialType.h"
#include "eng_MaterialInformed.h"
#include "eng_Draw.h"
#include "eng_DisplayCmds.h"
#include "eng_Device.h"
#include "eng_view.h"
#include "eng_Input.h"
#include "eng_Window.h"
#include "eng_Instance.h"

#include "Implementation/eng_view_inline.h"
#include "Implementation/eng_Draw_inline.h"
#include "Implementation/eng_Instance_inline.h"
#include "Implementation/eng_Pipeline_inline.h"


//------------------------------------------------------------------------------
// END
//------------------------------------------------------------------------------
#endif

