//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

class eng_shader_base : public eng_base
{
public:
};

          
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

class eng_shader_vert : public eng_shader_base
{
public:

    using handle    = x_ll_share_ref<eng_shader_vert>;


    struct setup
    {
        enum class memory_buffer : u8 { IS_FILENAME, IS_SHADER_DATA };

        constexpr setup( 
            const xbuffer_view<xbyte>&                              Memory, 
            const memory_buffer                                     MemoryBufferIs,
            const xbuffer_view<eng_uniform_stream::definition>&     UniformStreams ) : 
            m_MemoryBufferIs    { MemoryBufferIs },
            m_Memory            { Memory },
            m_UniformStreams    { UniformStreams } {}

        const memory_buffer                                         m_MemoryBufferIs;
        const xbuffer_view<xbyte>&                                  m_Memory;                           
        const xbuffer_view<eng_uniform_stream::definition>&         m_UniformStreams;
    };

protected:

    int                                         m_nUniformStreams   { 0 };
    xarray<eng_uniform_stream::definition,4>    m_UniformStreams    {};

protected:

    friend class x_ll_share_object;
};

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
class eng_shader_frag : public eng_shader_base
{
public:

    using handle    = x_ll_share_ref<eng_shader_frag>;
    using setup     = eng_shader_vert::setup;

protected:

    inline      static  void        t_DeleteSharedInstance      ( eng_shader_frag* const pData )    noexcept    { pData->Release(); }
    virtual             void        Release                     ( void )                            noexcept = 0;

protected:

    friend class x_ll_share_object;
};

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
class eng_shader_geom : public eng_shader_base
{
public:

    using handle    = x_ll_share_ref<eng_shader_geom>;
    using setup     = eng_shader_vert::setup;

protected:

    inline      static  void        t_DeleteSharedInstance      ( eng_shader_geom* const pData )    noexcept    { pData->Release(); }
    virtual             void        Release                     ( void )                            noexcept = 0;

protected:

    friend class x_ll_share_object;
};



