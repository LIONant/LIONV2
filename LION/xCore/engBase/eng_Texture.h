//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class x_import eng_texture : public eng_base
{
public:

    enum errors : u8
    {
        ERR_OK,
        ERR_FAIL,
    };

    enum wrap 
    {
        WRAP_REPEAT,
        WRAP_CLAMP_EDGE,
        WRAP_CLAMP_BORDER,
        WRAP_MIRROR_REPEAT,
        WRAP_MIRROR_CLAMP_EDGE,
        WRAP_COUNT
    };

    enum filter
    {
        FILTER_NEAREST,
        FILTER_LINEAR,
        FILTER_CUBIC_IMG,
        FILTER_COUNT
    };

    enum mipmap
    {
        MIPMAP_NEAREST,
        MIPMAP_LINEAR,
        MIPMAP_COUNT
    };

    using err       = x_err<errors>;
    using handle    = x_ll_share_ref<eng_texture>;

public:

    constexpr   int                     getWidth                ( void )                  const noexcept    { return m_Width;   }
    constexpr   int                     getHeight               ( void )                  const noexcept    { return m_Height;  }
    constexpr   int                     getMipCount             ( void )                  const noexcept    { return m_nMips;   }
    virtual                            ~eng_texture             ( void )                        noexcept    {}

protected:

    inline static void                  t_DeleteSharedInstance  ( eng_texture* const pData )    noexcept    { pData->Release(); }
    virtual      void                   Release                 ( void )                        noexcept = 0;
         
protected:

    int         m_Width     {};
    int         m_Height    {};
    int         m_nMips     {};

protected:

    friend class x_ll_share_object;
};


