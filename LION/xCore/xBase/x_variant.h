//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------

namespace xvariant_details
{
    template < typename T_A > x_forceconst
    auto align_max( void )
    {
        return alignof(T_A);
    }

    template < typename T_A, typename T_B, class ... T_ARGS > x_forceconst
    auto align_max( void )
    {
        return alignof(T_A) > alignof(T_B) ? 
                xvariant_details::align_max< T_A, T_ARGS... >() : 
                xvariant_details::align_max< T_B, T_ARGS... >();
    }

    template < typename T_A > x_forceconst
    auto size_max(void)
    {
        return sizeof(T_A);
    }

    template < typename T_A, typename T_B, class ... T_ARGS > x_forceconst
    auto size_max( void )
    {
        return sizeof(T_A) > sizeof(T_B) ? 
                xvariant_details::size_max< T_A, T_ARGS... >() : 
                xvariant_details::size_max< T_B, T_ARGS... >();
    }

    template < typename T_A > x_forceconst
    bool HasType( void )
    {
        return false;
    }

    template < typename T_A, typename T_B, class ... T_ARGS > x_forceconst
    bool HasType()
    {
        return std::is_same< T_A, T_B >::value ? true : HasType< T_A, T_ARGS... >();
    }

    template < int C, typename T_A > x_forceconst
    bool isIndexTypeOf( const int IndexA )
    {
        return false;
    }

    template < int C, typename T_A, typename T_B, class ... T_ARGS > x_forceconst
    bool isIndexTypeOf( const int IndexA )
    {
        return IndexA == C ? std::is_same< T_A, T_B >::value : isIndexTypeOf< C+1, T_A, T_ARGS... >( IndexA );
    }

    template < typename T_A, typename ... T_ARGS > x_forceconst
    bool isIndexTypeOf( int IndexA )
    {
        return isIndexTypeOf< 0, T_A, T_ARGS... >( IndexA );
    }

    template < int C, typename T_A > x_forceconst
    int getIndexByType( void )
    {
        return -1;
    }

    template < int C, typename T_A, typename T_B, class ... T_ARGS > x_forceconst
    int getIndexByType( void )
    {
        return std::is_same< T_A, T_B >::value ? C : getIndexByType< C + 1, T_A, T_ARGS... >();
    }

    template < typename T_A, typename ... T_ARGS > x_forceconst
    int getIndexByType( void )
    {
        return getIndexByType< 0, T_A, T_ARGS... >();
    }

    template< typename T >
    struct constructor
    {
        template< int TYPE_INDEX >
        static void Callback( void* pData ){ new(pData) T{}; }
    };

    template< typename T >
    struct destructor
    {
        template< typename T_TYPE >
        struct dest { T_TYPE m_A; ~dest(void) {} };

        template< int TYPE_INDEX >
        static void Callback( void* pData ){ reinterpret_cast<dest<T>*>(pData)->dest<T>::~dest(); }
    };
}

//------------------------------------------------------------------------------
// Description:
//      This is a variant that works connecting enums and types 
//
// Example:
//        enum class some_type
//        {
//            INVALID, 
//            INT_A, 
//            INT_B, 
//            FLOAT
//        };
//
//        using myc_enum = xvariant_enum
//        < 
//            some_type,  // THE ENUM TYPE
//            char,       // INVALID
//            int,        // INT_A
//            int,        // INT_B
//            float       // FLOAT
//        >;
//
//        myc_enum R2;
//        
//        R2.Construct<some_type::INT_A>( 22 );
//        auto c2  = R2.get<some_type::INT_A>() == 22;
//
//        R2.Construct<some_type::INT_B>( 100 );
//        auto d2  = R2.get<some_type::INT_B>() == 100;
//
//        myc_enum::type_by_enum< some_type::FLOAT >::type X = 2.0f;
//
//        std::cout << c2 << d2;
//------------------------------------------------------------------------------
template< typename T_TYPE_ENUM, typename ...T_ARGS >
struct xvariant_enum 
{
    static constexpr auto           arity               = sizeof...(T_ARGS);
    static constexpr auto           max_aligment        = xvariant_details::align_max< T_ARGS... >(); 
    static constexpr auto           max_size            = xvariant_details::size_max< T_ARGS... >(); 
    using                           enum_t              = T_TYPE_ENUM;
    using                           tuple               = std::tuple<T_ARGS...>;
    using                           table_constructors  = xtable_build_templated_callbacks< xvariant_details::constructor, tuple >; 
    using                           table_destructors   = xtable_build_templated_callbacks< xvariant_details::destructor,  tuple >; 

    alignas( max_aligment ) unsigned char   m_Data[max_size];
    T_TYPE_ENUM                             m_Index = T_TYPE_ENUM::INVALID;

    template< T_TYPE_ENUM N >
    struct enum_to_type
    {
        static_assert( static_cast<std::size_t>(N) < static_cast<std::size_t>(arity), "error: invalid parameter index.");
        using type = typename std::tuple_element<static_cast<int>(N),tuple>::type;
    };

    template< typename T_TYPE, bool bASSERTOnFailure = true >
    struct type_to_enum
    {
        struct details
        {
            template < int C, typename T_A, class ... T_ARGS2 >
            struct findtype
            {
                enum { value = (std::is_same< T_A, T_TYPE >::value) ? ((arity - 1) - C) : findtype< C - 1, T_ARGS2... >::value };
            };

            template< typename T_A, class ... T_ARGS2 >
            struct findtype< 0, T_A, T_ARGS2... >
            {
                enum { value = arity-1 };
            };
        };

        static constexpr const T_TYPE_ENUM value = static_cast<T_TYPE_ENUM>( details::template findtype< arity-1, T_ARGS... >::value );
        static_assert( (!bASSERTOnFailure) || std::is_same< typename enum_to_type<value>::type, T_TYPE >::value, "error: Could not find type.");
    };

    template< typename T_TYPE >
    static x_forceconst bool    isValidType     (void)       
    { 
        enum { value = (type_to_enum<T_TYPE, false>::details::template findtype<arity-1, T_ARGS...>::value) };
        return std::is_same
        < 
            typename enum_to_type< static_cast<T_TYPE_ENUM>( value ) >::type, T_TYPE 
        >::value ; 
    }
    x_forceconst bool           isValid         (void) const { return m_Index != T_TYPE_ENUM::INVALID; }
    
    template< typename T = T_TYPE_ENUM >
    x_forceconst auto   getTypeIndex    (void) const { return static_cast<T>(m_Index); }

    void Destroy( void )
    {
        if( isValid() ) 
        {
            static constexpr const table_destructors t{};
            if( m_Index != T_TYPE_ENUM::INVALID ) xtable_callback( t, static_cast<int>(m_Index), (void*)m_Data );
            m_Index = T_TYPE_ENUM::INVALID; 
        }
    }

    xvariant_enum(void) = default;
    ~xvariant_enum(void) { Destroy(); }

    template< T_TYPE_ENUM T_E > x_forceconst
    typename enum_to_type<T_E>::type& get( void )  
    { 
        static_assert( T_E != T_TYPE_ENUM::INVALID, "" );
        x_assert( m_Index == T_E );
        return *reinterpret_cast<typename enum_to_type<T_E>::type*>( m_Data ); 
    }

    template< T_TYPE_ENUM T_E > x_forceconst
    const typename enum_to_type<T_E>::type& get( void ) const
    { 
        static_assert( T_E != T_TYPE_ENUM::INVALID, "" );
        x_assert( m_Index == T_E );
        return *reinterpret_cast<const typename enum_to_type<T_E>::type*>( m_Data ); 
    }

    void* Construct( const T_TYPE_ENUM Index )
    {
        x_assert( Index != T_TYPE_ENUM::INVALID, "" );

        Destroy();

        // Setup the current index
        m_Index = Index;

        // construct type
        static constexpr const table_constructors t{};
        xtable_callback( t, static_cast<int>(Index), (void*)m_Data );
        return m_Data;
    }

    template< T_TYPE_ENUM T_E, typename ...T_CONSTRUCT_ARGS > x_forceconst
    auto& Construct( T_CONSTRUCT_ARGS&& ...Args )
    {
        static_assert( T_E != T_TYPE_ENUM::INVALID, "" );

        Destroy();

        // Setup the current index
        m_Index = T_E;

        // construct type
        return *new( m_Data ) typename enum_to_type< T_E >::type( std::forward<T_CONSTRUCT_ARGS>(Args)... );
    }

    const void* getRaw(void) const { return m_Data; }
};

//------------------------------------------------------------------------------

namespace xvariant_enum_details
{
    template< typename T >
    struct check_struct
    {
        template< int TYPE_INDEX >
        static void Callback( const void* pA, const void* pB, bool& R ) { R = *reinterpret_cast<const T*>(pA) == *reinterpret_cast<const T*>(pB); }
    };
}

template< typename T_TYPE_ENUM, typename ...T_ARGS > x_forceconst
bool operator == (const xvariant_enum<T_TYPE_ENUM, T_ARGS... >& A, const xvariant_enum<T_TYPE_ENUM, T_ARGS... >& B )  
{
    if( A.m_Index != B.m_Index ) return false;

    using type   = const typename xvariant_enum<T_TYPE_ENUM, T_ARGS... >;
    using tuple  = typename type::tuple;
    using checks = xtable_build_templated_callbacks< xvariant_enum_details::check_struct,  tuple >;
    static constexpr const checks t{};

    bool Result;
    xtable_callback( t, static_cast<int>(A.m_Index), A.getRaw(), B.getRaw(), Result );
    return Result;
}

//------------------------------------------------------------------------------
// Description:
//      Simple variant for types
//
// Example:
//        using myc = xvariant
//        < 
//            int,        // INT_A
//            float       // FLOAT
//        >;
//
//        myc R1;
//        
//        R1.Construct<int>( 22 );
//        auto c1  = R1.get<int>() == 22;
//
//        R1.Construct<float>( 2.23f );
//        auto d1  = R1.get<float>() == 2.23f;
//
//        std::cout << c1 << d1;
//------------------------------------------------------------------------------
template< typename ...T_ARGS >
struct xvariant
{
    using                           t_self              = xvariant< T_ARGS... >;
    static constexpr auto           arity               = sizeof...(T_ARGS);
    static constexpr auto           max_aligment        = xvariant_details::align_max< T_ARGS... >(); 
    static constexpr auto           max_size            = xvariant_details::size_max< T_ARGS... >(); 
    using                           tuple               = std::tuple<T_ARGS...>;
    using                           table_destructors   = xtable_build_templated_callbacks< xvariant_details::destructor,  tuple >; 

    xvariant(void) = default;
    ~xvariant(void) { Destroy(); }

    template< typename T >
    constexpr inline T& get( void ) 
    { 
        static_assert   ( xvariant_details::HasType      < T, T_ARGS... >(), "xvariant does not have the type been requested" ); 
        x_assert(       ( xvariant_details::isIndexTypeOf< T, T_ARGS... >(static_cast<int>(m_Index))) );
        return *reinterpret_cast<T*>( m_Data ); 
    }

    template< typename T >
    constexpr inline const T& get( void ) const
    { 
        static_assert   ( xvariant_details::HasType      < T, T_ARGS... >(), "xvariant does not have the type been requested" ); 
        x_assert(       ( xvariant_details::isIndexTypeOf< T, T_ARGS... >(static_cast<int>(m_Index))) );
        return *reinterpret_cast<const T*>( m_Data ); 
    }

    void Destroy( void )
    {
        if (isValid()) 
        { 
            //xvariant_details::destroy< arity - 1, arity - 1, T_ARGS... >::ByIndex(m_Index, m_Data); 
            //m_Index = -1;
            static constexpr const table_destructors t{};
            if( m_Index !=-1 ) xtable_callback( t, static_cast<int>(m_Index), (void*)m_Data );
            m_Index = -1; 
        }
    }

    template< typename T_TYPE, typename ...T_CONSTRUCT_ARGS >
    constexpr inline void Construct( T_CONSTRUCT_ARGS&& ...Args )
    {
        static_assert   (       xvariant_details::HasType        < T_TYPE, T_ARGS... >(), "xvariant does not have the type been requested" ); 
        static_assert   ( -1 != xvariant_details::getIndexByType < T_TYPE, T_ARGS... >(), "xvarient can't find the type requested" );

        Destroy();

        // construct type
        new( m_Data ) T_TYPE( std::forward<T_CONSTRUCT_ARGS>(Args)... );

        // Setup the current index
        m_Index = xvariant_details::getIndexByType<T_TYPE, T_ARGS...>();
    }

    constexpr inline const void*    getRaw          (void) const { return m_Data; }
    constexpr inline bool           isValid         (void) const { return m_Index != -1; }
    constexpr inline auto           getTypeIndex    (void) const { return static_cast<int>(m_Index); }

    alignas( max_aligment ) unsigned char   m_Data[max_size];
    char                                    m_Index = -1;
};

//------------------------------------------------------------------------------

template< typename ...T_ARGS > x_forceconst
bool operator == ( const xvariant<T_ARGS... >& A, const xvariant<T_ARGS... >& B )  
{
    if( A.m_Index != B.m_Index ) return false;

    using type   = const typename xvariant<T_ARGS... >;
    using tuple  = typename type::tuple;
    using checks = xtable_build_templated_callbacks< xvariant_enum_details::check_struct,  tuple >;
    static constexpr const checks t{};

    bool Result;
    xtable_callback( t, static_cast<int>(A.m_Index), A.getRaw(), B.getRaw(), Result );
    return Result;
}
