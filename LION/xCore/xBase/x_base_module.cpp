
#include "x_base.h"

#include "Implementation/lz4h/lz4.c"
#include "Implementation/lz4h/lz4hc.c"

#include "Implementation/x_base.cpp"
#include "Implementation/x_bitmap.cpp"
#include "Implementation/x_bitmap_scale.cpp"
#include "Implementation/x_bitstream.cpp"
#include "Implementation/x_cmdline.cpp"
#include "Implementation/x_color.cpp"
#include "Implementation/x_file.cpp"
#include "Implementation/x_file_directory.cpp"
#include "Implementation/x_file_ram_device.cpp"
#include "Implementation/x_file_serialfile.cpp"
#include "Implementation/x_file_textfile.cpp"
#include "Implementation/x_global_context.cpp"
#include "Implementation/x_memory_ops.cpp"
#include "Implementation/x_net_socket.cpp"
#include "Implementation/x_net_tcp_transport.cpp"
#include "Implementation/x_net_udp_transport.cpp"
#include "Implementation/x_plus.cpp"
#include "Implementation/x_property.cpp"
#include "Implementation/x_random.cpp"
#include "Implementation/x_reporting.cpp"
#include "Implementation/x_scheduler.cpp"
#include "Implementation/x_string_sprintf.cpp"

#ifdef _X_TARGET_WINDOWS
    #include "Implementation/Windows/x_pc_file_device.cpp"
#endif 
