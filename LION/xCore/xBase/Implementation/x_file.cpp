//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_base.h"

#if _X_TARGET_WINDOWS
    #include <Windows.h>
    #include "Implementation/Windows/x_pc_file_device.h"
#elif _X_TARGET_MAC || _X_TARGET_IOS
    #include <aio.h>
    #include "../Implementation/Darwin/x_darwin_file_device.h"
#elif _X_TARGET_ANDROID
    #include "Implementation/Android/x_android_file_device.h"
#endif


//------------------------------------------------------------------------------

const xwchar* xfile::getTempPath( void ) noexcept
{
    return g_context::get().getFileSystemDevice()->getTempPath();
}

//------------------------------------------------------------------------------
static
xfile_device_i* FileSystemFindDevice( const char* pDeviceName )
{
    auto* const pDeviceHead = g_context::get().getFileSystemDevice();

    // User must be asking for the default device
    // Or if it is a temp folder we also will use the default device
    if ( pDeviceName == nullptr || x_strncmp( pDeviceName, "temp:", sizeof( "temp:" )-1 ) == 0 )
    {
        return pDeviceHead;
    }

    // Search throw all the devices    
    for( xfile_device_i* pDevice = pDeviceHead; pDevice; pDevice = pDevice->m_pNext )
    {
        const char* a = pDevice->getName();
        for( s32 i=0; pDeviceName[i] && *a; i++, a++ )
        {
            if( pDeviceName[i] == ':' && *a == ':' )
                return pDevice;

            if( x_tolower(*a) != x_tolower(pDeviceName[i]) )
            {
                // Skip character and see if it has more devices mapped
                while( *a != ':' && *a ) a++;

                // Check to see if a has more devices
                if( *a == ':' )
                {
                    if( a[1] )
                    {
                        i=-1;
                        continue;                        
                    }
                    break;
                }
            }
        }
    }

    return nullptr;
}

//------------------------------------------------------------------------------

xfile::err xfile::Open( const xwstring& Path, const char* pMode ) noexcept
{
    x_assert( pMode );

    // We cant open a new file in the middle of using another
    x_assert( m_pDevice == NULL );

    //
    // Find the device in question
    //
    xarray< char, xfile::MAX_DRIVE>  DeviceName;
    int                              i;

    // Copy our device into a separate string
    for( i=0; ( DeviceName[i] = static_cast<xchar>(Path[i]) )
              && ( i<(DeviceName.getCount()-1))
              && ( Path[i] != L':');
         i++ )
    {
    }

    xfile_device_i* pDevice = nullptr;
    if( DeviceName[i] != ':' )
    {
        // We didn't find any device we are going to assume the default device
        pDevice = FileSystemFindDevice( nullptr );
    }
    else
    {
        // lets find it!
        DeviceName[++i] = 0;
        pDevice         = FileSystemFindDevice( DeviceName );
    }

    // Make sure that we got a device
    if( pDevice == nullptr )
    {
        return x_error_code( errors, ERR_DEVICE_FAILURE, "Unable to find requested device" );
    }

    //
    // Okay now lets make sure that the mode is correct
    //
    access_types       AccessType;
    bool               bSeekToEnd       = false;
 
    for( i=0; pMode[i]; i++ )
    {
        switch( pMode[i] )
        {
        case 'a':   AccessType.m_READ       = AccessType.m_WRITE = true;                        bSeekToEnd      = true;     break;         
        case 'r':   AccessType.m_READ       = true;                                                                         break; 
        case '+':   AccessType.m_WRITE      = true;                                                                         break; 
        case 'w':   AccessType.m_READ       = AccessType.m_WRITE = AccessType.m_CREATE = true;                              break; 
        case 'c':   AccessType.m_COMPRESS   = true;                                                                         break; 
        case '@':   AccessType.m_ASYNC      = true;                                                                         break; 
        case 't':   AccessType.m_TEXT       = true;                                                                         break;
        case 'b':   AccessType.m_TEXT       = false;                                                                        break; 
        default:
            
            x_assert( false, xstring::Make("Don't understand this[%c] access mode while opening file (%s)", pMode[i], (const char*)Path ) );
        }
    }

    // next thing is to open the file using the device
    // Note this hold initialization could be VERY WORNG as opening the file 
    // may the one of the slowest part of the file access as the DVD may need to seek
    // to it. This ideally should happen Async when an Async mode is requested.
    // May need to review this a bit more careful later.
    void* pFile = nullptr;    
    if( AccessType.m_CREATE )
    {
        pFile = pDevice->Open( Path, AccessType );
        if( pFile == nullptr ) return x_error_code( errors, ERR_CREATING_FILE, "Unable to create file" );
    }
    else
    {
        pFile = pDevice->Open( Path, AccessType );
        if( pFile == nullptr ) return x_error_code( errors, ERR_OPENING_FILE, "Unable to open file" );
        if( bSeekToEnd ) 
        {
            pDevice->Seek( pFile, xfile_device_i::SKM_END, 0 );
        }
    }

    //
    // Set all the member variables
    //
    m_FileName      = Path;
    m_pDevice       = pDevice;
    m_pFile         = pFile;
    m_AccessType    = AccessType;

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

void xfile::Close( void ) noexcept
{
    if( m_pFile )
    {
        x_assert(m_pDevice);
        m_pDevice->Close( m_pFile );
    }

    //
    // Done with the file
    //
    m_pFile         = nullptr;
    m_pDevice       = nullptr;
    m_AccessType.setDefaults();
    m_FileName.setNull();
}

//------------------------------------------------------------------------------

xfile::err xfile::ReadRaw( void* pBuffer, int Size, u64 Count ) noexcept
{
    x_assert( m_pFile );
    x_assert( m_pDevice );
    x_assert( pBuffer );
    x_assert( Size > 0 );
    x_assert( Count >= 0 );

    u64 TotalCount = Size*Count;
    if( m_pDevice->Read( m_pFile, pBuffer, TotalCount ) )
    {
        // If it is text mode try finding '\r\n' to remove the '\r'
        if( m_AccessType.m_TEXT )
        {
            char* pCharSrc = (char*)pBuffer;
            char* pCharDst = (char*)pBuffer;

            for( u64 i=0; i<TotalCount; i++ )
            {
                *pCharDst = *pCharSrc;
                pCharSrc++;
                if( !(*pCharSrc == '\n' && *(pCharSrc-1) == '\r') ) 
                {
                    pCharDst++;
                }
            }

            // Read any additional data that we may need
            if( pCharSrc != pCharDst )
            {
                const int Delta = (int)(pCharSrc - pCharDst);

                // Recurse
                return ReadRaw( &((char*)pBuffer)[TotalCount-Delta], 1, Delta );
            }

            // Sugar a bad case here. We need to read one more character to know what to do.
            if( *--pCharDst == '\r' )
            {
                u8 C;

                if( m_pDevice->Read( m_pFile, &C, 1 ) == false )
                    return x_error_code( errors, ERR_FAILURE, "Fail to read characters from file" );

                if( C == '\n' )
                {
                    *pCharDst = '\n';
                }
                else
                {
                    // Upss the next character didnt match the sequence
                    // lets rewind one
                    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_CURENT, -1 );
                }
            }
        }

        return x_error_ok(errors);
    }

    return x_error_code( errors, ERR_FAILURE, "Fail to read file" );
}

//------------------------------------------------------------------------------

xfile::err  xfile::WriteRaw( const void* pBuffer, int Size, u64 Count ) noexcept 
{
    x_assert(m_pFile);
    x_assert(m_pDevice);
    x_assert( pBuffer );
    x_assert( Size > 0 );
    x_assert( Count >= 0 );

    // If it is text mode try finding '\n' and add a '\r' in front so that it puts in the file '\r\n'
    u64 TotalCount = Size*Count;
    if( m_AccessType.m_TEXT )
    {
        char* pFound = (char*)pBuffer;
        u64   iLast  = 0;
        u64   i;

        for( i=0; i<Count; i++ )
        {
            if( pFound[i] == '\n' )
            {
                x_constexprvar u16 Data = (u16)((((u16)'\n') << 8) | (((u16)'\r') << 0));

                m_pDevice->Write( m_pFile, &pFound[iLast], i-iLast );
                m_pDevice->Write( m_pFile, &Data, 2 );

                // Update the base
                iLast = i+1;
            }
        };

        // Write the remainder 
        if( iLast != i ) 
        {
            m_pDevice->Write( m_pFile, &pFound[iLast], i-iLast );
        }
    }
    else
    {
        m_pDevice->Write( m_pFile, pBuffer, TotalCount );
    }

    if( m_AccessType.m_FORCE_FLUSH )
    {
        m_pDevice->Flush( m_pFile );
    }
    
    return x_error_ok(errors);
}

//------------------------------------------------------------------------------
xfile_device_i* __x_file_new_ram_device( void );

void __x_file_init( g_context& GContext )
{
#if _X_TARGET_WINDOWS
    GContext.getFileSystemDevice() = x_new( pc_device );
#elif _X_TARGET_MAC || _X_TARGET_IOS
    GContext.getFileSystemDevice() = x_new( darwin_file_device );
#elif _X_TARGET_ANDROID

#endif

    GContext.getFileSystemDevice()->m_pNext = __x_file_new_ram_device();
}

//------------------------------------------------------------------------------

void __x_file_kill( g_context& GContext )
{
    //
    // Delete all devices
    //
    for( auto pData = GContext.getFileSystemDevice(); pData; )
    {
        auto pNext = pData->m_pNext ;
        x_delete( pData );
        pData = pNext;
    }
}


