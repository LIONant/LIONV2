//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
xsphere::xsphere( const xvector3d& Pos, f32 R ) :
    m_Pos( Pos ),
    m_R( R ) 
{

}

//------------------------------------------------------------------------------
inline
xsphere::xsphere( const xbbox& BBox )
{
    m_Pos = BBox.getCenter();
    m_R   = BBox.getRadius();
}

//------------------------------------------------------------------------------
inline
void xsphere::setZero( void )
{
    m_Pos.setZero();
    m_R = 0;
}

//------------------------------------------------------------------------------
inline
void xsphere::setup( const xvector3d& Pos, const f32 R )
{
    m_Pos = Pos;
    m_R   = R;
}

//------------------------------------------------------------------------------
inline
xbbox xsphere::getBBox( void ) const
{
    return xbbox( m_Pos, m_R );
}

//------------------------------------------------------------------------------
inline
bool xsphere::TestIntersect( const xvector3d& P0, const xvector3d& P1 ) const
{
    const f32 SqrD = xvector3( m_Pos ).getSquareDistToLineSeg( P0, P1 );
    return SqrD <= (m_R*m_R);
}

//------------------------------------------------------------------------------
inline
s32 xsphere::Intersect( f32& t0, f32& t1, const xvector3d& P0, const xvector3d& P1 ) const
{
    // set up quadratic Q(t) = a*t^2 + 2*b*t + c
    const xvector3  Diff = P0 - m_Pos;
    const xvector3  Dir  = (P1-P0);
    const f32       A    = Dir.getLengthSquared();
    const f32       B    = Diff.Dot( Dir );
    const f32       C    = Diff.getLengthSquared() - (m_R*m_R);

    // no intersection if Q(t) has no real roots
    const f32    Discr = B*B - A*C;

    if( Discr < 0.0f )
    {
        // The ray is completely contain inside the xsphere
        if( C < 0 )
            return -0x1;

        return 0x0;
    }

    if( Discr > 0.0f )
    {
        const f32 Root = x_Sqrt( Discr );
        const f32 InvA = 1.0f/A;
        t0 = (-B - Root) * InvA;
        t1 = (-B + Root) * InvA;

        // assert: t0 < t1 since A > 0        
        x_assert( t0 < t1 );

        if( t0 > 1.0f || t1 < 0.0f )
            return 0x0;

        if( t0 >= 0.0f )
        {
            // there is an entrance point but not an exit
            if ( t1 > 1.0f )
                return 0x1;

            return 0x3;
        }
        else  // t1 >= 0
        {
            x_assert( t1 >= 0 );

            // there is only one exit point
            return 0x2;
        }
    }
    else
    {
        t0 = -B/A;

        // there is an entrance point but not an exit
        if ( t0 >= 0.0f && t0 <= 1.0f )
            return 0x1;
    }

    return 0x0;
}

//------------------------------------------------------------------------------
inline
bool xsphere::Intersect( f32& t0, const xvector3d& P0, const xvector3d& P1 ) const
{
    f32         t1;
    const s32   C = Intersect( t0, t1, P0, P1 );

    // We didn't hit
    if( C == 0 )
        return 0;

    // If the ray is completely contain in the xsphere
    if( C == -1 )
    {
        t0 = 0;
        return true;
    }

    // If there is only an exit point then we consider that we were in.
    if( C == 2 )
    {
        t0 = 0;
        return true;
    }

    // Anything with an entry hit is a good hit
    x_assert( C & 1 );

    return true;
}

//------------------------------------------------------------------------------
inline
s32 xsphere::TestIntersection( const xplane& Plane ) const
{
    const f32 Distance = Plane.Dot( m_Pos ) - Plane.m_D;

    if( Distance < -m_R || Distance > m_R ) 
        return 0;

    if( Distance < 0 ) return -1;
    return 1;
}

//------------------------------------------------------------------------------
inline
bool xsphere::Intersect( const xsphere& Sphere ) const
{
    return (Sphere.m_Pos - m_Pos ).getLengthSquared() <= x_Sqr( Sphere.m_R + m_R );
}

//------------------------------------------------------------------------------
inline
bool xsphere::Intersect( const xbbox& BBox ) const
{
    f32 s, d = 0;

    // find the square of the distance
    // from the xsphere to the box,
    if( m_Pos.m_X < BBox.m_Min.m_X)
    {
        s = m_Pos.m_X - BBox.m_Min.m_X;
        d += s*s;
    }
    else if (m_Pos.m_X > BBox.m_Max.m_X)
    {
        s = m_Pos.m_X - BBox.m_Max.m_X;
        d += s*s;
    }

    if (m_Pos.m_Y < BBox.m_Min.m_Y)
    {
        s = m_Pos.m_Y - BBox.m_Min.m_Y;
        d += s*s;
    }
    else if (m_Pos.m_Y > BBox.m_Max.m_Y)
    {
        s = m_Pos.m_Y - BBox.m_Max.m_Y;
        d += s*s;
    }

    if (m_Pos.m_Z < BBox.m_Min.m_Z)
    {
        s = m_Pos.m_Z - BBox.m_Min.m_Z;
        d += s*s;
    }
    else if (m_Pos.m_Z > BBox.m_Max.m_Z)
    {
        s = m_Pos.m_Z - BBox.m_Max.m_Z;
        d += s*s;
    }

    return d <= m_R*m_R;
}

//------------------------------------------------------------------------------
// check if 2 moving xspheres have contact 
// taken from "Simple Intersection Tests For Games" 
// article in Gamasutra, Oct 18 1999 
inline
bool xsphere::Intersect( f32&              t0,         // out: normalized intro contact t0
                          f32&              t1,         // out: normalized outro contact t1
                          const xvector3d&  Vel1,       // in: distance travelled by 'this'
                          const xsphere&    Sph2,       // in: the other xsphere
                          const xvector3d&  Vel2 )      // in: distance travelled by 'Sph2'
{
    xvector3 VelDelta( Vel2 - Vel1 );
    xvector3 PosDelta( Sph2.m_Pos - m_Pos );

    f32     RDelta = m_R + Sph2.m_R;

    // check if xspheres are currently overlapping...
    if( PosDelta.getLengthSquared() <= (RDelta*RDelta) ) 
    {
        t0 = 0.0f;
        t1 = 0.0f;
        return true;
    } 

    // check if they hit each other
    f32 a = VelDelta.getLengthSquared();

    if( (a < -0.0001f) || (a > 0.0001f) ) 
    {
        // if a is '0' then the objects don't move relative to each other
        f32 b = VelDelta.Dot(PosDelta) * 2.0f;
        f32 c = PosDelta.Dot(PosDelta) - (RDelta * RDelta);
        f32 q = b*b - 4*a*c;

        if( q >= 0.0f ) 
        {
            // 1 or 2 contacts
            f32 sq = x_Sqrt(q);
            f32 d  = 1.0f / (2.0f*a);
            f32 r1 = (-b + sq) * d;
            f32 r2 = (-b - sq) * d;

            if( r1 < r2 ) 
            {
                t0 = r1;
                t1 = r2;
            } 
            else 
            {
                t0 = r2;
                t1 = r1;
            }

            return true;
        } 
    }

    return false;
}

