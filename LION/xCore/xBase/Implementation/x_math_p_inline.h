//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================


//------------------------------------------------------------------------------
inline 
xplane::xplane( const xvector3& P1, const xvector3& P2, const xvector3& P3 )
{
    setup( P1, P2, P3 );
}

//------------------------------------------------------------------------------
inline 
xplane::xplane( const xvector3d& Normal, f32 Distance ) :
    m_Normal( Normal ),
    m_D( Distance ) 
{

}

//------------------------------------------------------------------------------
inline 
xplane::xplane( f32 A, f32 B, f32 C, f32 D ) :
    m_Normal( A, B, C ),
    m_D( D ) 
{

}

//------------------------------------------------------------------------------
inline 
xplane::xplane( const xvector3d& Normal, const xvector3d& Point )
{
    setup( Normal, Point );
}

//------------------------------------------------------------------------------
inline
void xplane::setup( f32 A, f32 B, f32 C, f32 D )
{
    m_Normal.setup( A, B, C );
    m_Normal.Normalize();
    m_D = D;
}

//------------------------------------------------------------------------------
inline
void xplane::ComputeD( const xvector3d& P )
{
    m_D = -m_Normal.Dot( P );
}

//------------------------------------------------------------------------------
inline
void xplane::setup( const xvector3& P1, const xvector3& P2, const xvector3& P3 )
{
    m_Normal = (P2-P1).Cross(P3-P1).Normalize();
    m_D      = -m_Normal.Dot( P1 );
}

//------------------------------------------------------------------------------
inline
void xplane::setup( const xvector3d& Normal, f32 Distance )
{
    m_Normal = Normal;
    m_Normal.Normalize();
    m_D      = Distance;
}

//------------------------------------------------------------------------------
inline
void xplane::setup( const xvector3d& Normal, const xvector3d& Point )
{
    m_Normal = Normal;
    m_Normal.Normalize();
    m_D = -Normal.Dot( Point );
}

//------------------------------------------------------------------------------
inline
void xplane::getOrthovectors ( xvector3d& AxisA,
                               xvector3d& AxisB ) const
{
    f32      AbsA, AbsB, AbsC;
    xvector3 Dir;

    // get a non-parallel axis to normal.
    AbsA = x_Abs( m_Normal.m_X );
    AbsB = x_Abs( m_Normal.m_Y );
    AbsC = x_Abs( m_Normal.m_Z );

    if( (AbsA<=AbsB) && (AbsA<=AbsC) ) Dir.setup(1,0,0);
    else
        if( (AbsB<=AbsA) && (AbsB<=AbsC) ) Dir.setup(0,1,0);
        else                               Dir.setup(0,0,1);

    AxisA = m_Normal.Cross(Dir).Normalize();
    AxisB = m_Normal.Cross(AxisA).Normalize();
}

//------------------------------------------------------------------------------
inline 
f32 xplane::Dot( const xvector3d& P ) const  
{
    return m_Normal.Dot( P );
}

//------------------------------------------------------------------------------
inline 
f32 xplane::getDistance( const xvector3d& P ) const
{
    return m_Normal.Dot( P ) + m_D;
}

//------------------------------------------------------------------------------
inline 
s32 xplane::getWhichSide( const xvector3d& P ) const
{
    f32 Distance = m_Normal.Dot( P ) + m_D;
    if( Distance < -0.99f ) return -1;
    return Distance > 0.01f;
}

//------------------------------------------------------------------------------
inline 
bool xplane::IntersectLineSegment( f32& T, const xvector3d& P0, const xvector3d& P1 ) const
{
    T = (P1 - P0).Dot( m_Normal );

    if( T == 0.0f ) 
        return false;

    T = -getDistance( P0 ) / T;

    return true;
}

//------------------------------------------------------------------------------
inline 
bool xplane::IntersectLine( f32& T, const xvector3d& Start, const xvector3d& Direction ) const
{
    f32 dist = getDistance( Start );

	// behind plane
    if( dist < 0.0f ) 
        return false ;

	f32 len = -Direction.Dot( m_Normal );
	if( len < dist ) 
    {
		// moving away from plane or point too far away
		return false;
	}

	T = dist/len;
	return true;
}

//------------------------------------------------------------------------------
inline
void xplane::getComponents( const xvector3d&    V,
                            xvector3d&          Parallel,
                            xvector3d&          Perpendicular ) const
{
    Perpendicular = m_Normal.Dot( V ) * m_Normal;
    Parallel      = V - Perpendicular;
}

//------------------------------------------------------------------------------
inline
xvector3 xplane::getReflection( const xvector3& V ) const
{
    return xvector3( m_Normal ).getReflection( V );
}

//------------------------------------------------------------------------------
inline 
bool xplane::isValid( void ) const
{
    return m_Normal.isValid() && x_isValid(m_D);
}

//------------------------------------------------------------------------------
inline
bool xplane::ClipNGon( xvector3d*       pDst, 
                        s32&             nDstVerts,                        
                        const xvector3d* pSrc, 
                        s32              nSrcVerts ) const
{
    f32   D0, D1;
    s32   P0, P1;
    bool bClipped = false;

    nDstVerts = 0;
    P1 = nSrcVerts-1;
    D1 = getDistance( pSrc[P1] );

    for( s32 i=0; i<nSrcVerts; i++ ) 
    {
        P0 = P1;
        D0 = D1;
        P1 = i;
        D1 = getDistance(pSrc[P1]);

        // Do we keep starting vert?
        if( D0 >= 0 )
        {
            pDst[nDstVerts++] = pSrc[P0];
        }

        // Do we need to compute intersection?
        if( ((D0>=0)&&(D1<0)) || ((D0<0)&&(D1>=0)) )
        {
            f32 d = (D1-D0);
            if( x_Abs(d) < 0.00001f )  d = 0.00001f;
            f32 t = (0-D0) / d;
            pDst[nDstVerts++] = pSrc[P0] + t*(pSrc[P1]-pSrc[P0]);
            bClipped = true;
        }
    }

    return bClipped;
}

//------------------------------------------------------------------------------
inline
bool xplane::ClipNGon( xvector3*        pDst, 
                        s32&             nDstVerts,                        
                        const xvector3*  pSrc, 
                        s32              nSrcVerts ) const
{
    f32   D0, D1;
    s32   P0, P1;
    bool bClipped = false;

    nDstVerts = 0;
    P1 = nSrcVerts-1;
    D1 = getDistance( pSrc[P1] );

    for( s32 i=0; i<nSrcVerts; i++ ) 
    {
        P0 = P1;
        D0 = D1;
        P1 = i;
        D1 = getDistance(pSrc[P1]);

        // Do we keep starting vert?
        if( D0 >= 0 )
        {
            pDst[nDstVerts++] = pSrc[P0];
        }

        // Do we need to compute intersection?
        if( ((D0>=0)&&(D1<0)) || ((D0<0)&&(D1>=0)) )
        {
            f32 d = (D1-D0);
            if( x_Abs(d) < 0.00001f )  d = 0.00001f;
            f32 t = (0-D0) / d;
            pDst[nDstVerts++] = pSrc[P0] + t*(pSrc[P1]-pSrc[P0]);
            bClipped = true;
        }
    }

    return bClipped;
}

//------------------------------------------------------------------------------
inline
xplane& xplane::operator - ( void )
{
    m_Normal = -m_Normal;
    m_D      = -m_D;
    return *this;
}

//------------------------------------------------------------------------------
inline 
xplane operator * ( const xmatrix4& M, const xplane& Plane )
{
    xplane       Newxplane;
    xvector3     V;

    // Transform a point in the xplane by M
    V = M * ( Plane.m_Normal * - Plane.m_D );

    // Compute the transpose of the Inverse of the Matrix (adjoin)
    // and Transform the normal with it
    Newxplane.m_Normal = (M.getAdjoint() * Plane.m_Normal).Normalize();

    // Recompute D by in the transform point
    Newxplane.ComputeD( V );

    return Newxplane;
}
