//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      This constructor builds by passing a single U32 which represents a 
//      packed version of the color structure.
// Arguments:
//      K       - This is the color packed into a 32 bit variable. Note 
//                 that this may have issues in different endian machines. The Rule is that
//                 the color is packed little endian.
//------------------------------------------------------------------------------
constexpr 
xcolor::xcolor( const u32 K ) : m_Value{ x_EndianSystemToBig( K ) } {}

//------------------------------------------------------------------------------
// Description:
//      This constuctor builds the color by passing each component in a u8 forms.
// Arguments:
//      R   - is the Red component of the color.
//      G   - is the Green component of the color.
//      B   - is the Blue component of the color.
//      A   - is the Alpha component of the color.
//------------------------------------------------------------------------------
constexpr
xcolor::xcolor( f32 R, f32 G, f32 B, f32 A ) : 
    m_R{ static_cast<u8>(R*0xff) },
    m_G{ static_cast<u8>(G*0xff) },
    m_B{ static_cast<u8>(B*0xff) },
    m_A{ static_cast<u8>(A*0xff) } {}

//------------------------------------------------------------------------------
// Description:
//      This constuctor builds the color by passing each component in a u8 forms.
// Arguments:
//      R   - is the Red component of the color.
//      G   - is the Green component of the color.
//      B   - is the Blue component of the color.
//      A   - is the Alpha component of the color.
//------------------------------------------------------------------------------
constexpr
xcolor::xcolor( u8 R, u8 G, u8 B, u8 A ) : 
    m_R(R), 
    m_G(G), 
    m_B(B),
    m_A(A) {}

//------------------------------------------------------------------------------
// Description:
//      This constuctor builds the color by passing each component in a u8 forms.
// Arguments:
//      r   - is the Red component of the color.
//      g   - is the Green component of the color.
//      b   - is the Blue component of the color.
//      a   - is the Alpha component of the color.
//------------------------------------------------------------------------------
constexpr
xcolor::xcolor( int R, int G, int B, int A ) : 
    m_R{ static_cast<u8>(R) },
    m_G{ static_cast<u8>(G) },
    m_B{ static_cast<u8>(B) },
    m_A{ static_cast<u8>(A) } {}

//------------------------------------------------------------------------------
// Description:
//      This constuctor builds the color by passing a vector3 which represents the
//      floating point values of RGB.
// Arguments:
//      C   - C represents a vector which contains values ranges from 0 to 1, 
//             C.X: is the Red component of the color.
//             C.Y: is the Green component of the color.
//             C.Z: is the Blue component of the color.
//------------------------------------------------------------------------------
inline 
xcolor::xcolor( const xvector3d& C )
{
    setupFromRGBA( C.m_X, C.m_Y, C.m_Z, 1 );
}

//------------------------------------------------------------------------------
// Description:
//      This constuctor builds the color by passing a vector4 which represents the
//      floating point values of RGBA.
// <param name="C"> 
//      C   - C represents a vector which contains values ranges from 0 to 1, 
//             C.X: is the Red component of the color.
//             C.Y: is the Green component of the color.
//             C.Z: is the Blue component of the color.
//             C.W: is the Alpha component of the color.
//------------------------------------------------------------------------------
inline 
xcolor::xcolor( const xvector4& C )
{
    setupFromRGBA( C.m_X, C.m_Y, C.m_Z, C.m_W );
}

//------------------------------------------------------------------------------
// Description:
//      This function sets the values directly by passing the u8 components 
//      of the color.
// Arguments:
//      r   - is the Red component of the color.
//      g   - is the Green component of the color.
//      b   - is the Blue component of the color.
//      a   - is the Alpha component of the color.
//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setup( u8 r, u8 g, u8 b, u8 a )
{
    m_A = a;
    m_R = r;
    m_G = g;
    m_B = b;

    return *this;
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setup( const u32 K )
{
    return *this = K; 
}

//------------------------------------------------------------------------------
constexpr 
bool xcolor::operator == ( xcolor C ) const
{
    return m_Value == C.m_Value;
}

//------------------------------------------------------------------------------
constexpr 
bool xcolor::operator != ( xcolor C ) const
{
    return m_Value != C.m_Value;
}

//------------------------------------------------------------------------------
inline 
u8& xcolor::operator[]( int Index )
{
    x_assert( Index >=  0 );
    x_assert( Index <=  3 );
    return ((u8*)this)[ Index ];
}

//------------------------------------------------------------------------------
constexpr 
xcolor::operator u32( void ) const
{
    return x_EndianBigToSystem( m_Value );
}

//------------------------------------------------------------------------------
inline 
const xcolor& xcolor::operator += ( xcolor C )
{
    m_A = x_Min( 255, s32(m_A) + C.m_A );
    m_R = x_Min( 255, s32(m_R) + C.m_R );
    m_G = x_Min( 255, s32(m_G) + C.m_G );
    m_B = x_Min( 255, s32(m_B) + C.m_B );

    return *this;
}

//------------------------------------------------------------------------------
inline 
const xcolor& xcolor::operator -= ( xcolor C )
{
    m_A = x_Max( 0, s32(m_A) - C.m_A );
    m_R = x_Max( 0, s32(m_R) - C.m_R );
    m_G = x_Max( 0, s32(m_G) - C.m_G );
    m_B = x_Max( 0, s32(m_B) - C.m_B );

    return *this;
}

//------------------------------------------------------------------------------
inline 
const xcolor& xcolor::operator *= ( xcolor C )
{
    m_A = x_Max( 0, x_Min( 255, s32(m_A) * C.m_A ) );
    m_R = x_Max( 0, x_Min( 255, s32(m_R) * C.m_R ) );
    m_G = x_Max( 0, x_Min( 255, s32(m_G) * C.m_G ) );
    m_B = x_Max( 0, x_Min( 255, s32(m_B) * C.m_B ) );

    return *this;
}

//------------------------------------------------------------------------------
//"The YIQ system is the colour primary system adopted by NTSC for colour
//television  broadcasting. The YIQ color solid is formed by a linear
//transformation of the RGB cude. Its purpose is to exploit certain
//characteristics of the human visual system to maximize the use of a fixed
//bandwidth" (Funds... op cit).
//------------------------------------------------------------------------------
inline 
void xcolor::getYIQ( f32& Y, f32& I, f32& Q ) const
{
    f32 r,g,b;

    r = m_R*(1/255.0f);
    g = m_G*(1/255.0f);
    b = m_B*(1/255.0f);

    Y = r*0.299f + g*0.587f + b*0.114f;
    I = r*0.596f - g*0.274f - b*0.322f;
    Q = r*0.212f - g*0.523f + b*0.311f;
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromYIQ( f32 Y, f32 I, f32 Q )
{
    f32 r,g,b;

    r = Y*1 + I*0.956f + Q*0.621f;
    g = Y*1 - I*0.272f - Q*0.647f;
    b = Y*1 - I*1.105f + Q*1.702f;

    r *= 255.0f;
    g *= 255.0f;
    b *= 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;

    return *this;
}

//------------------------------------------------------------------------------
// YUV is like YIQ, except that it is the PAL/European standard. It's only trivial
// to many poeple in Northen America (i.e. not only the STATES), but since the
// USENET messages are read all over the world...
//------------------------------------------------------------------------------
inline 
void xcolor::getYUV( f32& Y, f32& U, f32& V ) const
{
    f32 r,g,b;

    r = m_R*(1/255.0f);
    g = m_G*(1/255.0f);
    b = m_B*(1/255.0f);

    Y =  r*0.299f + g*0.587f + b*0.114f;
    U = -r*0.147f - g*0.289f + b*0.437f;
    V =  r*0.615f - g*0.515f - b*0.100f;
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromYUV( f32 Y, f32 U, f32 V )
{
    f32 r,g,b;

    r = Y*1 + U*0.000f + V*1.140f;
    g = Y*1 - U*0.394f - V*0.581f;
    b = Y*1 + U*2.028f + V*0.000f;

    r *= 255.0f;
    g *= 255.0f;
    b *= 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;

    return *this;
}

//------------------------------------------------------------------------------
inline 
void xcolor::getCIE( f32& C, f32& I, f32& E ) const
{
    f32 r, g, b;

    r = m_R*(1/255.0f);
    g = m_G*(1/255.0f);
    b = m_B*(1/255.0f);

    C = r*0.6067f + g*0.1736f + b*0.2001f;
    I = r*0.2988f + g*0.5868f + b*0.1143f;
    E = r*0.0000f + g*0.0661f + b*1.1149f;
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromCIE( f32 C, f32 I, f32 E )
{
    f32 r, g, b;

    r =  C*1.9107f - I*0.5326f - E*0.2883f;
    g = -C*0.9843f + I*1.9984f - E*0.0283f;
    b =  C*0.0583f - I*0.1185f + E*0.8986f;

    r *= 255.0f;
    g *= 255.0f;
    b *= 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;

    return *this;
}

//------------------------------------------------------------------------------
// color to a parametric rgb values
//------------------------------------------------------------------------------
inline 
void xcolor::getRGB( f32& aR, f32& aG, f32& aB  ) const
{
    aR = m_R*(1/255.0f);   
    aG = m_G*(1/255.0f);   
    aB = m_B*(1/255.0f);   
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromRGB( f32 aR, f32 aG, f32 aB )
{
    f32 r,g,b;

    r = aR * 255.0f;
    g = aG * 255.0f;
    b = aB * 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;

    return *this;
}

//------------------------------------------------------------------------------
inline 
void xcolor::getRGBA( f32& aR, f32& aG, f32& aB, f32& aA  ) const
{
    aR = m_R*(1/255.0f);   
    aG = m_G*(1/255.0f);   
    aB = m_B*(1/255.0f);   
    aA = m_A*(1/255.0f);   
}

//------------------------------------------------------------------------------
inline 
xvector4 xcolor::getRGBA( void ) const
{
    return xvector4( m_R*(1/255.0f), m_G*(1/255.0f), m_B*(1/255.0f), m_A*(1/255.0f) );   
}

//------------------------------------------------------------------------------
inline 
xvector3 xcolor::getRGB( void ) const
{
    return xvector3( m_R*(1/255.0f), m_G*(1/255.0f), m_B*(1/255.0f) );
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromRGB( xvector3d& Vector )
{
    return setupFromRGB( Vector.m_X, Vector.m_Y, Vector.m_Z );
}


//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromRGBA( f32 aR, f32 aG, f32 aB, f32 aA )
{
    f32 r,g,b,a;

    r = aR * 255.0f;
    g = aG * 255.0f;
    b = aB * 255.0f;
    a = aA * 255.0f;    

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = (u8)x_Min( 255.0f, a );

    return *this;
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromRGBA( const xvector4& C )
{
    return setupFromRGBA( C.m_X, C.m_Y, C.m_Z, C.m_W );
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromNormal( const xvector3d& Normal )
{
    m_R = (u8)x_Min( 255.0f, ( ( Normal.m_X + 1.0f ) * 127.0f ) + 0.5f );
    m_G = (u8)x_Min( 255.0f, ( ( Normal.m_Y + 1.0f ) * 127.0f ) + 0.5f );
    m_B = (u8)x_Min( 255.0f, ( ( Normal.m_Z + 1.0f ) * 127.0f ) + 0.5f );
    m_A = 255;

    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3 xcolor::getNormal( void ) const
{
    return xvector3(   (((f32)m_R)-127.0f ) * (1/127.0f),
                       (((f32)m_G)-127.0f ) * (1/127.0f),
                       (((f32)m_B)-127.0f ) * (1/127.0f)     );

}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromLight( const xvector3d& LightDir )
{
    return setupFromNormal( -LightDir );
}

//------------------------------------------------------------------------------
inline 
xvector3 xcolor::getLight( void ) const
{
    return -getNormal();
}

//------------------------------------------------------------------------------
// color to Cyan, Magenta and Yellow
//------------------------------------------------------------------------------
inline 
void xcolor::getCMY( f32& C, f32& M, f32& Y ) const
{
    C = 1 - m_R*(1/255.0f);   // ASSUME C
    M = 1 - m_G*(1/255.0f);   // ASSUME M
    Y = 1 - m_B*(1/255.0f);   // ASSUME Y
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromCMY( f32 C, f32 M, f32 Y )
{
    f32 r, g, b;

    r = (1-C)*255;
    g = (1-M)*255;
    b = (1-Y)*255;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;

    return *this;
}

//------------------------------------------------------------------------------
inline 
xcolor::format xcolor::FindClosestFormat( u32 FormatMask, format Match )
{    
    for( const xcolor::format* pMatch = &g_Match[ Match ].m_Format[0]; *pMatch != FMT_END; pMatch++ )
    {
        if( FormatMask & (1<<*pMatch) ) return *pMatch;
    }

    return FMT_NULL;
}

//------------------------------------------------------------------------------
inline 
xcolor& xcolor::setupFromData( u32 Data, format DataFormat )
{
    const fmt_desc& Fmt = g_FormatDesc[ DataFormat ];
    x_assert( Fmt.m_Format == DataFormat );
    
    m_R = (u8)((Fmt.m_RShift<0) ? ((Data & Fmt.m_RMask) << (-Fmt.m_RShift)) : ((Data & Fmt.m_RMask) >> (Fmt.m_RShift)));
    m_G = (u8)((Fmt.m_GShift<0) ? ((Data & Fmt.m_GMask) << (-Fmt.m_GShift)) : ((Data & Fmt.m_GMask) >> (Fmt.m_GShift)));
    m_B = (u8)((Fmt.m_BShift<0) ? ((Data & Fmt.m_BMask) << (-Fmt.m_BShift)) : ((Data & Fmt.m_BMask) >> (Fmt.m_BShift)));

    // force m_A to 255 if the src format doesn't have alpha
    if( Fmt.m_AMask == 0 ) m_A = 255;
    else                   m_A = (u8)((Fmt.m_AShift<0) ? ((Data & Fmt.m_AMask) << (-Fmt.m_AShift)) : ((Data & Fmt.m_AMask) >> (Fmt.m_AShift)));

    return *this;
} 

//------------------------------------------------------------------------------
inline 
u32 xcolor::getDataFromColor( format DataFormat ) const
{
    u32             Data;
    const fmt_desc& Fmt = g_FormatDesc[ DataFormat ];
    x_assert( Fmt.m_Format == DataFormat );
    
    Data  = ~( Fmt.m_AMask | Fmt.m_RMask | Fmt.m_GMask | Fmt.m_BMask );
    Data |= (Fmt.m_AShift<0) ? ((((u32)m_A) >> (-Fmt.m_AShift)) & Fmt.m_AMask ) : ((((u32)m_A) << Fmt.m_AShift) & Fmt.m_AMask );
    Data |= (Fmt.m_RShift<0) ? ((((u32)m_R) >> (-Fmt.m_RShift)) & Fmt.m_RMask ) : ((((u32)m_R) << Fmt.m_RShift) & Fmt.m_RMask );
    Data |= (Fmt.m_GShift<0) ? ((((u32)m_G) >> (-Fmt.m_GShift)) & Fmt.m_GMask ) : ((((u32)m_G) << Fmt.m_GShift) & Fmt.m_GMask );
    Data |= (Fmt.m_BShift<0) ? ((((u32)m_B) >> (-Fmt.m_BShift)) & Fmt.m_BMask ) : ((((u32)m_B) << Fmt.m_BShift) & Fmt.m_BMask );
        
    return Data;
}

//------------------------------------------------------------------------------
inline 
xcolor::format xcolor::FindFormat( u32 AMask, u32 RMask, u32 GMask, u32 BMask  )
{
    for( int i=FMT_NULL; i<FMT_END; i++ )
    {
        if( AMask != g_FormatDesc[i].m_AMask ) continue;
        if( RMask != g_FormatDesc[i].m_RMask ) continue;
        if( GMask != g_FormatDesc[i].m_GMask ) continue;
        if( BMask != g_FormatDesc[i].m_BMask ) continue;

        return g_FormatDesc[i].m_Format;
    }

    return FMT_NULL;
}


//------------------------------------------------------------------------------
inline
xcolor xcolor::getBlendedColors( const xcolor Src1, const xcolor Src2, f32 t )
{
    const xvector4 S1 = Src1.getRGBA();
    xvector4 newColor = S1 + t * ( Src2.getRGBA() - S1 );
    return { newColor };
}

//------------------------------------------------------------------------------
inline
xcolor xcolor::ComputeNewAlpha( f32 Alpha ) const
{
    xcolor Ret( *this );
    
    Ret.m_A = x_Max( 0,x_Min(255, s32(Alpha*m_A)));
    
    return Ret;
}

//------------------------------------------------------------------------------
inline
xcolor xcolor::PremultiplyAlpha( void ) const
{
    xvector4 Color = getRGBA();
    float    Alpha = Color.m_W;

    // Premultiply the color
    Color    *= Alpha;
    Color.m_W = Alpha; 
    
    return xcolor{}.setupFromRGBA( Color );
}
