/*
 *  Modified from glibc by Eric
 */


/* We use an UGLY hack to prevent gcc from finding us cheating.  The
   implementation of aio_error and aio_error64 are identical and so
   we want to avoid code duplication by using aliases.  But gcc sees
   the different parameter lists and prints a warning.  We define here
   a function so that aio_error64 has no prototype.  */
#ifdef TARGET_ANDROID
#include "aio.h"

int aio_error (const struct aiocb *aiocbp)
{
    return aiocbp->__error_code;
}

#endif
