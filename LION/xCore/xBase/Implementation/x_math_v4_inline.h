//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
//
// Here are some helpfully notes to work with the sse instructions
//
// r3, r2, r1, r0
// W,  Z,  Y,  X 
//
// _mm_shuffle_ps( a, b, ( bn, bm, an, am ) )
// r0 := am
// r1 := an
// r2 := bm
// r3 := bn
//
//__m128 _mm_movehl_ps( __m128 a, __m128 b );
// r3 := a3
// r2 := a2
// r1 := b3
// r0 := b2
//
//==============================================================================

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
void xvector4::setIdentity( void )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_setzero_ps();
#else
    m_X = m_Y = m_Z = 0;
#endif
    m_W = 1;
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::setup( f32 X, f32 Y, f32 Z, f32 W )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_set_ps( W, Z, Y, X );
#else
    m_X = X;
    m_Y = Y;
    m_Z = Z;
    m_W = W;
#endif

    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
f32 xvector4::Dot( const xvector4& V ) const
{
#ifdef TARGET_PC_32BIT
    f32 Dest;
    __asm 
    {
        mov         ebx, this
        mov         eax, V
        movaps      xmm0, dword ptr [ebx]
        mulps       xmm0, dword ptr [eax]      
        movaps      xmm1, xmm0                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)  
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm1, xmm0                        
        movss       xmmword ptr [Dest], xmm1
    }
    return Dest;

#elif defined _X_SSE4_SUPPORT

    const f32x4&      V1_WZYX = *((f32x4*)this);
    const f32x4&      V2_WZYX = *((const f32x4*)&V);

    const f32x4       A = _mm_mul_ps( V1_WZYX, V2_WZYX );
    const f32x4       B = _mm_add_ss( _mm_shuffle_ps( A, A, _MM_SHUFFLE(3,3,3,3)), _mm_add_ss(_mm_shuffle_ps( A, A, _MM_SHUFFLE(0,0,0,0)), _mm_add_ss(_mm_shuffle_ps(A, A, _MM_SHUFFLE(1,1,1,1)), _mm_shuffle_ps(A, A, _MM_SHUFFLE(2,2,2,2)))));

    return ((f32*)&B)[0];

#elif defined(TARGET_360)
		f32x4 res = XMVector4Dot( m_XYZW, V.m_XYZW );

		// can return any element, they all contain the result
		return res.x;
#else
    return m_X * V.m_X + 
           m_Y * V.m_Y +
           m_Z * V.m_Z +
           m_W * V.m_W;
#endif
}

//------------------------------------------------------------------------------
inline 
f32 xvector4::getLengthSquared( void ) const
{
    return Dot( *this );
}

//------------------------------------------------------------------------------
inline 
f32 xvector4::getLength( void ) const
{
#ifdef _X_SSE4_SUPPORT
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );
          a = _mm_sqrt_ss(a);

    return ((f32*)&a)[0];

#elif defined(TARGET_360)
		f32x4 res = XMVector4LengthSq( m_XYZW );
		return res.x;
#else
        return x_Sqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z + m_W*m_W);
#endif
}

//------------------------------------------------------------------------------
inline 
bool xvector4::isValid( void ) const
{
    return x_isValid(m_X) && x_isValid(m_Y) && x_isValid(m_Z) && x_isValid(m_W);
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::Normalize( void )
{
#ifdef _X_SSE4_SUPPORT
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );
          a = _mm_rsqrt_ss(a);

    f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    m_XYZW = _mm_mul_ps( m_XYZW, oneDivLen );
    return *this;

#elif defined(TARGET_360)
	return XMVector4Normalize( m_XYZW );
#else
    f32 Div = x_InvSqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z+ m_W*m_W );
    m_X *= Div;
    m_Y *= Div;
    m_Z *= Div;
    m_W *= Div;
    return *this;
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::NormalizeSafe( void )
{
    const f32   Tof  = 0.00001f;

#ifdef _X_SSE4_SUPPORT
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );

    if( ((f32*)&a)[0] < Tof )
    {
        setup( 1, 0, 0, 0 );
        return *this;
    }

                  a = _mm_rsqrt_ss(a);
    f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    m_XYZW = _mm_mul_ps( m_XYZW, oneDivLen );
    return *this;

#elif defined(TARGET_360)
	// boils down to 1 instruction, so not much waste, really
	f32x4 len_sq = XMVector4LengthSq( m_XYZW );		

	if( len_sq.x < Tof ) 
    {		
        setup( 1, 0, 0 );
        return *this;
    }
	WZYX = XMVector4Normalize( m_XYZW );
#else
    f32 dsq = m_X*m_X + m_Y*m_Y + m_Z*m_Z+ m_W*m_W;
	if( dsq < Tof ) 
    {		
        setup( 1, 0, 0 );
        return *this;
    }

    f32 Div = x_InvSqrt(dsq);
    m_X *= Div;
    m_Y *= Div;
    m_Z *= Div;
    m_W *= Div;
    return *this;
#endif
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator += ( const xvector4& V )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_add_ps( m_XYZW, V.m_XYZW );
#elif defined(TARGET_360)
	m_XYZW = XMVectorAdd( m_XYZW, V.m_XYZW );
#else
    m_X += V.m_X;
    m_Y += V.m_Y;
    m_Z += V.m_Z;
    m_W += V.m_W;
#endif

    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator -= ( const xvector4& V )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_sub_ps( m_XYZW, V.m_XYZW );
#elif defined(TARGET_360)
	m_XYZW = XMVectorSub( m_XYZW, V.m_XYZW );
#else
    m_X -= V.m_X;
    m_Y -= V.m_Y;
    m_Z -= V.m_Z;
    m_W -= V.m_W;
#endif

    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator *= ( const xvector4& V )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_mul_ps( m_XYZW, V.m_XYZW );
#elif defined(TARGET_360)
	m_XYZW = XMVectorMul( m_XYZW, V.m_XYZW );
#else
    m_X *= V.m_X;
    m_Y *= V.m_Y;
    m_Z *= V.m_Z;
    m_W *= V.m_W;
#endif

    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator *= ( f32 Scalar )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_mul_ps( m_XYZW, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
	m_XYZW = XMVectorScale( m_XYZW, Scalar);
#else
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
    m_W *= Scalar;
#endif

    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator /= ( f32 Div )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_div_ps( m_XYZW, _mm_set1_ps( Div ) );
#elif defined(TARGET_360)
	m_XYZW = XMVectorScale( m_XYZW, 1.0f/Div);
#else
    f32 Scalar = 1.0f/Div;
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
    m_W *= Scalar;
#endif
    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
constexpr 
bool xvector4::operator == ( const xvector4& V ) const
{
    return !(
        (x_Abs( V.m_X - m_X) > XFLT_TOL)||
        (x_Abs( V.m_Y - m_Y) > XFLT_TOL)||
        (x_Abs( V.m_Z - m_Z) > XFLT_TOL)||
        (x_Abs( V.m_W - m_W) > XFLT_TOL)
        );
}

//------------------------------------------------------------------------------
inline
xvector4 operator + ( const xvector4& V1, const xvector4& V2 )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_add_ps( V1.m_XYZW, V2.m_XYZW ) };
#else
    return xvector4{    V1.m_X + V2.m_X,
                        V1.m_Y + V2.m_Y,
                        V1.m_Z + V2.m_Z,
                        V1.m_W + V2.m_W };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator - ( const xvector4& V1, const xvector4& V2 )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_sub_ps( V1.m_XYZW, V2.m_XYZW ) };
#else
    return xvector4{    V1.m_X - V2.m_X,
                        V1.m_Y - V2.m_Y,
                        V1.m_Z - V2.m_Z,
                        V1.m_W - V2.m_W };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator * ( const xvector4& V1, const xvector4& V2 )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_mul_ps( V1.m_XYZW, V2.m_XYZW ) };
#else
    return xvector4{    V1.m_X * V2.m_X,
                        V1.m_Y * V2.m_Y,
                        V1.m_Z * V2.m_Z,
                        V1.m_W * V2.m_W };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator - ( const xvector4& V )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_sub_ps( _mm_setzero_ps(), V.m_XYZW ) };
#else
    return xvector4{    -V.m_X,
                        -V.m_Y,
                        -V.m_Z,
                        -V.m_W };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator * ( const xvector4& V, f32 Scalar )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_mul_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector4{    V.m_X * Scalar,
                        V.m_Y * Scalar,
                        V.m_Z * Scalar,
                        V.m_W * Scalar );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator * ( f32 Scalar, const xvector4& V )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_mul_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector4{    V.m_X * Scalar,
                        V.m_Y * Scalar,
                        V.m_Z * Scalar,
                        V.m_W * Scalar };
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator + ( f32 Scalar, const xvector4& V )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_add_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector4{    V.m_X + Scalar,
                        V.m_Y + Scalar,
                        V.m_Z + Scalar,
                        V.m_W + Scalar };
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator + ( const xvector4& V, f32 Scalar )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_add_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector4{    V.m_X + Scalar,
                        V.m_Y + Scalar,
                        V.m_Z + Scalar,
                        V.m_W + Scalar };
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator - ( f32 Scalar, const xvector4& V )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_sub_ps( _mm_set1_ps( Scalar ), V.m_XYZW ) };
#else
    return xvector4{    Scalar - V.m_X,
                        Scalar - V.m_Y,
                        Scalar - V.m_Z,
                        Scalar - V.m_W };
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator - ( const xvector4& V, f32 Scalar )
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_sub_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector4{    V.m_X - Scalar,
                        V.m_Y - Scalar,
                        V.m_Z - Scalar,
                        V.m_W - Scalar };
#endif
}


//------------------------------------------------------------------------------
inline 
xvector4 xvector4::getMin( const xvector4& V ) const
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_min_ps( m_XYZW, V.m_XYZW ) };
#else
    return xvector4{    x_Min(V.m_X, m_X),
                        x_Min( V.m_Y,m_Y),
                        x_Min( V.m_Z,m_Z),
                        x_Min( V.m_W,m_W) };
#endif
}
 
//------------------------------------------------------------------------------
inline 
xvector4 xvector4::getMax( const xvector4& V ) const
{
#ifdef _X_SSE4_SUPPORT
    return xvector4{ _mm_max_ps( m_XYZW, V.m_XYZW ) };
#else
    return xvector4{    x_Max( V.m_X,m_X),
                        x_Max( V.m_Y,m_Y),
                        x_Max( V.m_Z,m_Z),
                        x_Max( V.m_W,m_W) };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::Homogeneous( void )
{
    *this /= m_W;
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector4& xvector4::Abs( void )
{
#ifdef _X_SSE4_SUPPORT
    m_XYZW = _mm_max_ps( m_XYZW, _mm_sub_ps( _mm_setzero_ps(), m_XYZW ) );
#else
    m_X = x_Abs(m_X);
    m_Y = x_Abs(m_Y);
    m_Z = x_Abs(m_Z);
    m_W = x_Abs(m_W);
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline
bool xvector4::isInrange( const f32 Min, const f32 Max ) const
{
#ifdef _X_SSE4_SUPPORT
    const f32x4 t1 = _mm_add_ps( _mm_cmplt_ps( m_XYZW, _mm_set1_ps(Min) ), _mm_cmpgt_ps( m_XYZW, _mm_set1_ps(Max) ) );
    return  x_getf32x4ElementByIndex<0>(t1) == 0 &&
            x_getf32x4ElementByIndex<1>(t1) == 0 &&
            x_getf32x4ElementByIndex<2>(t1) == 0 &&
            x_getf32x4ElementByIndex<3>(t1) == 0;
#else
    return  x_InRange( m_X, Min, Max ) &&
            x_InRange( m_Y, Min, Max ) &&
            x_InRange( m_Z, Min, Max ) &&
            x_InRange( m_W, Min, Max );
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 xvector4::OneOver( void ) const
{
#ifdef _X_SSE4_SUPPORT
    return xvector4 { _mm_rcp_ps( m_XYZW ) };
#else
    return xvector4 {   1.0f/m_X,
                        1.0f/m_Y,
                        1.0f/m_Z,
                        1.0f/m_W };
#endif
}
