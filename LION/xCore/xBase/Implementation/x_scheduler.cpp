//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_base.h"

//--------------------------------------------------------------------------------------
// VARS
//--------------------------------------------------------------------------------------

#if defined (_MSC_VER)
    static thread_local int                 s_WorkerID;
//    static thread_local global_context*     s_pLocalContex;

#if _X_JOB_PROFILER
    #include <cvmarkersobj.h>               
    #include <cvmarkers.h>                  
    using namespace Concurrency::diagnostic;
#endif
 
#else
    static __thread int                     s_WorkerID;
//    static __thread global_context*         s_pLocalContex;
#endif

//--------------------------------------------------------------------------------------
// FUNCTIONS
//--------------------------------------------------------------------------------------

void WorkersStartWorking( void ) noexcept;

//--------------------------------------------------------------------------------------
// Description:
//          Setting the name of a thread.
//          For windows: https://msdn.microsoft.com/en-us/library/xcb2z8hs.aspx
//--------------------------------------------------------------------------------------
#if _X_TARGET_WINDOWS
    #include <windows.h>
    const DWORD MS_VC_EXCEPTION = 0x406D1388;
    #pragma pack(push,8)  
    typedef struct tagTHREADNAME_INFO
    {
        DWORD dwType; // Must be 0x1000.  
        LPCSTR szName; // Pointer to name (in user addr space).  
        DWORD dwThreadID; // Thread ID (-1=caller thread).  
        DWORD dwFlags; // Reserved for future use, must be zero.  
    } THREADNAME_INFO;
    #pragma pack(pop)  
    static
    void SetThreadName(void)
    {
        // Force each thread to stay in its core
        // Performance have shown that this is not helping
        //SetThreadAffinityMask(GetCurrentThread(), static_cast<u64>(1) << s_WorkerID );


        char Name[256];
        sprintf_s<256>( Name, "xWorker: %02d", s_WorkerID);

        DWORD dwThreadID = GetCurrentThreadId();
        THREADNAME_INFO info;
        info.dwType = 0x1000;
        info.szName = Name;
        info.dwThreadID = dwThreadID;
        info.dwFlags = 0;
    #pragma warning(push)  
    #pragma warning(disable: 6320 6322)  
        __try {
            RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
        }
        __except (EXCEPTION_EXECUTE_HANDLER) {
        }
    #pragma warning(pop)  
}
#else
    void SetThreadName(void) {}
#endif


//--------------------------------------------------------------------------------------

void x_scheduler::Init( int nWorkers ) noexcept
{
    //
    // Do all the move to initialize
    //
    {
        x_assert_linear( m_Debug_LQ );
        x_assert( m_State == STATE_NO_INITIALIZED );

        // We give ID=0 for the main thread
        s_WorkerID          = 0;
        m_nWorkersActive    = 1;

        // get how many cores we have -1 because the main thread is already created
        m_nAllocatedWorkers = std::thread::hardware_concurrency() - 1;
        if( nWorkers != -1 ) m_nAllocatedWorkers = nWorkers;

        // Allocate the worker kits. These are used by each worker 
        m_WorkerKit.New( m_nAllocatedWorkers + 1 );
            
        // Initialize the kits
        for( xuptr i=0; i<(m_nAllocatedWorkers + 1); i++ )
        {
            m_WorkerKit[i].m_LightJobPool.Init( (m_nAllocatedWorkers+1) * 16 );
            if( i == m_nAllocatedWorkers )  m_WorkerKit[i].m_iNextQueue = 0;
            else                            m_WorkerKit[i].m_iNextQueue = i+1;
        }
    }
    
    //
    // Rename our main thread
    //
    SetThreadName();

    //
    // Ok ready to start working
    //
    if( m_nAllocatedWorkers > 0 )
    {
        m_lWorkers.New( m_nAllocatedWorkers );

        for( auto& Worker : m_lWorkers )
        {
            Worker = std::thread( WorkersStartWorking );
        }
    }
    else
    {
        m_nAllocatedWorkers = 0;
    }

    // Wait for the workers to be running
    if( m_nWorkersActive != (m_nAllocatedWorkers+1) )
    {
        std::unique_lock<std::mutex> Lock( m_SleepWorkerMutex );
        m_SleepWorkerCV.wait( Lock, [this]()
        { 
            return m_nWorkersActive == (m_nAllocatedWorkers+1); 
        });
    }

    //
    // Tell the workers we are ready to go now
    //
    m_State = STATE_WORKING;
}

//--------------------------------------------------------------------------------------

void x_scheduler::Shutdown( void ) noexcept
{
    if( m_lWorkers.getCount() )
    {
        //
        // Get everyone to stop working
        //
        {
            x_assert_quantum( m_Debug_LQ );

            m_State = STATE_EXITING;
            m_SleepWorkerCV.notify_all();

            for( auto& Worker : m_lWorkers )
            {
                Worker.join();
            }
            x_assert( m_nWorkersActive==1);
        }

        //
        // Clean the memory now
        //
        {
            x_assert_linear( m_Debug_LQ );

            //
            // Clean any jobs pending in the queues
            //
            CleanQueues();

            //
            // Clean the workers
            //
            m_lWorkers.Delete();
        }
        
        //
        // Destroy all the threads
        //
        m_lWorkers.Delete();
    }

    //
    // Reset the state
    //
    m_State = STATE_NO_INITIALIZED;
}


//--------------------------------------------------------------------------------------

void x_scheduler::AddJobToQuantumWorld( x_job_base& Job ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    m_Stats_nJobs.inc();

    if( Job.isLightJob() )
    {
        X_SHEDULER_LOG( "Scheduler: A New Light Job, Jobs Left [%d] There are[%d] workers working", m_Stats_nJobs.get(), m_Stats_nWorkersWorking.get() );
        
        // Currently we do not support light jobs with different affinities because that will require a mpmc_queue and we dont want to spend 
        // performance on optimizing that case. If we really need it to we could use the high priority slots of the non-normal affinity queues.
        x_assert( Job.getAffinity() == x_job_base::AFFINITY_NORMAL );
        const auto WID = s_WorkerID;
        auto& Queue = m_WorkerKit[ WID ].m_LightJobQueue;
        Queue.push( Job );
    }
    else
    {   
        X_SHEDULER_LOG( "Scheduler: A New Job, Jobs Left [%d] There are [%d] workers working", m_Stats_nJobs.get(), m_Stats_nWorkersWorking.get() );
        auto& Queue = m_JobQueue[Job.getAffinity()][Job.getPriority()];
        Queue.push( Job );
    }
    
    //
    // If we have a job for the main thread just notify everyone since we don't know which CV is the main thread sitting on
    // Other wise we just need one guy to pick the job.
    //
    if( Job.getAffinity() == x_job_base::AFFINITY_MAIN_THREAD ) m_SleepWorkerCV.notify_all(); 
    else                                                        m_SleepWorkerCV.notify_one();
}
   
//--------------------------------------------------------------------------------------

x_scheduler::~x_scheduler( void ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    Shutdown();
}

//--------------------------------------------------------------------------------------

x_scheduler::worker_kit& x_scheduler::getWorketKit( void ) noexcept
{
    return m_WorkerKit[s_WorkerID];
}

//--------------------------------------------------------------------------------------

x_job_base* x_scheduler::getLightJob( void ) noexcept
{
    worker_kit& WorkerKit = getWorketKit();

    auto* pJob = WorkerKit.m_LightJobQueue.pop();
    if( pJob ) 
    {
        m_Stats_nJobs.dec();
        return pJob;
    }

    // Search to steal any other light job from other workers
    xuptr i = WorkerKit.m_iNextQueue;
    do 
    {
        pJob = m_WorkerKit[i].m_LightJobQueue.steal();
        if( pJob ) 
        {
            m_Stats_nJobs.dec();
            WorkerKit.m_iNextQueue = i;
            return pJob;
        }

        ++i;
        if( i >= (m_nAllocatedWorkers+1) ) i = 0;

    } while( i != WorkerKit.m_iNextQueue );

    return nullptr;
}

//--------------------------------------------------------------------------------------

x_job_base* x_scheduler::getJob( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    //
    // Lets focus on getting a job and only return if we fail after a while
    // if we fail we probably will go to sleep
    //
    for( int iLoop = 0; iLoop <100; iLoop++ )
    {
        // Try to get a light job first
        auto pJob = getLightJob();
        if( pJob ) 
            return pJob;

        // Get a job with the right affinity first
        if( s_WorkerID == 0 )
        {
            for( auto& Queue : m_JobQueue[ x_job_base::AFFINITY_MAIN_THREAD ] )
            {
                pJob = Queue.pop();
                if( pJob )
                {
                    m_Stats_nJobs.dec();
                    return pJob;
                }
            }
        }
        else
        {
            for( auto& Queue : m_JobQueue[ x_job_base::AFFINITY_NOT_MAIN_THREAD ] )
            {
                pJob = Queue.pop();
                if( pJob )
                {
                    m_Stats_nJobs.dec();
                    return pJob;
                }
            }
        }

        // get a regular job
        for( auto& Queue : m_JobQueue[ x_job_base::AFFINITY_NORMAL ] )
        {
            pJob = Queue.pop();
            if( pJob )
            {
                m_Stats_nJobs.dec();
                return pJob;
            }
        }
    }

    return nullptr;
}

//--------------------------------------------------------------------------------------
#if _X_COMPILER_VS_2015
    X_CMD_JOB_PROFILER_ON
    (
        struct profile
        {
            xarray<marker_series,8>     mySeries { _T("0"), _T("1"),_T("2"),_T("3"),_T("4"),_T("5"),_T("6"),_T("7") };
            PCV_PROVIDER                MakerWriter; 
            GUID                        Guid;

            profile()
            {
              //  Guid.Data1 = 123;
             //   CvInitProvider( &Guid, &MakerWriter );
                for( auto& E : mySeries )
                {
                //    CvCreatemarkerSeries( MakerWriter, _T("My Series"), &E );
                }
            }
        };
        static thread_local int  s_SeriesStack=0;
        profile Profile;
    )
#endif

//--------------------------------------------------------------------------------------

void x_scheduler::ProcessWork( x_job_base* pJob ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assume( pJob );

#if _X_COMPILER_VS_2015
    X_CMD_JOB_PROFILER_ON( span s(Profile.mySeries[s_SeriesStack++], s_SeriesStack, (LPCTSTR)pJob->getName().m_pValue ) );
#endif

    //
    // Do the job
    //
    m_Stats_nWorkersWorking.inc();

    X_SHEDULER_LOG( "Worker[%d] took a job, jobs left[%d] There are [%d] workers working", getWorkerUID(), m_Stats_nJobs.get(), m_Stats_nWorkersWorking.get() );

    const bool bDeleteWhenDone = pJob->isDeletedWhenDone();
    pJob->qt_onRun();
    pJob->qt_onDone();

    if( bDeleteWhenDone ) x_delete( pJob );

    m_Stats_nWorkersWorking.dec();

    X_SHEDULER_LOG( "Worker[%d] finished the job, jobs left[%d], There are [%d] workers working", getWorkerUID(), m_Stats_nJobs.get(), m_Stats_nWorkersWorking.get() );
        
#if _X_COMPILER_VS_2015
    X_CMD_JOB_PROFILER_ON( s_SeriesStack-- );
#endif
}

//--------------------------------------------------------------------------------------

int x_scheduler::getWorkerUID( void ) noexcept
{
    // No need to specify which space this is since the variable belongs to the thread
    return s_WorkerID;
}

//--------------------------------------------------------------------------------------

void x_scheduler::ProcessWhileWait( x_trigger_base& Trigger ) noexcept
{
    x_assert_quantum( g_context::get().m_Scheduler.m_Debug_LQ );
    std::atomic<bool> Bool;
    
    Bool = true;
    Trigger.DoTriggerWhenReady( &Bool );
    
    while( Bool )
    {
        auto& S = g_context::get().m_Scheduler;
        x_job_base* pJob = S.getLightJob();
        if( pJob ) S.ProcessWork( pJob );
    }
    ////////////
    // 
    // At this point the trigger could be freed
    // 
    ////////////
}

//--------------------------------------------------------------------------------------

void x_scheduler::CleanQueues( void ) noexcept
{
    x_assert_linear( m_Debug_LQ );

    x_job_base* pJop;

    for( int i = 0; i < m_nAllocatedWorkers + 1; i++ )
    {
        while( (pJop = m_WorkerKit[i].m_LightJobQueue.pop()) )
        {
            if( pJop->isDeletedWhenDone() ) x_delete( pJop );
        }
    }

    // Affinity first
    for( auto& Affinity : m_JobQueue )
    {
        // Then Priority
        for( auto& Queue : Affinity )
        {
            // Then we have a queue
            while( (pJop = Queue.pop()) )
            {
                if( pJop->isDeletedWhenDone() ) x_delete( pJop );
            }
        }
    }
}

//--------------------------------------------------------------------------------------

void x_scheduler::ThreadBecomesWorker( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    std::unique_lock<std::mutex> Lock( m_SleepWorkerMutex );

    SetThreadName();

    // At this point we should be working
    while( m_State == x_scheduler::STATE_WORKING )
    {
        x_job_base* pJob;

        //
        // Get next job or Sleep when we need to
        //
        pJob = getJob();
        if( pJob == nullptr )
        {
            X_SHEDULER_LOG( "Worker[%d] Sleeping", getWorkerUID() );
            m_SleepWorkerCV.wait( Lock, [&pJob,this]()
            { 
                pJob = getJob();
                return pJob || m_State == x_scheduler::STATE_EXITING;
            });

            if( pJob == nullptr )
            {
                if( m_State == x_scheduler::STATE_EXITING )
                {
                    X_SHEDULER_LOG( "Worker [%d] Awakes to terminate itself\n", getWorkerUID() );
                    return;
                }

                X_SHEDULER_LOG( "Worker [%d] Falsely Awake up", getWorkerUID() );
                continue;
            }
            else
            {
                X_SHEDULER_LOG( "Worker [%d] Awakes Ready to work", getWorkerUID() );
            }
        }

        //
        // Process any job
        //
        ProcessWork( pJob );
    }
}

//--------------------------------------------------------------------------------------

void x_scheduler::MainThreadBecomesWorker( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    std::unique_lock<std::mutex> Lock( m_SleepWorkerMutex );

    // At this point we should be working
    while( m_State == x_scheduler::STATE_WORKING )
    {
        x_job_base* pJob;

        //
        // Stop the main thread from working if requested
        //
        if( m_bMainThreadShouldWork.load( std::memory_order_relaxed ) == false )
        {
            // Reset to default
            m_bMainThreadShouldWork.store( true, std::memory_order_relaxed );
            break;
        }

        //
        // Get next job or Sleep when we need to
        //
        pJob = getJob();
        if( pJob == nullptr )
        {
            X_SHEDULER_LOG( "Worker[%d] Sleeping", getWorkerUID() );
            
            m_SleepWorkerCV.wait( Lock,[&pJob,this]()
            { 
                pJob = getJob();
                return pJob || m_bMainThreadShouldWork.load( std::memory_order_relaxed ) == false || m_State == x_scheduler::STATE_EXITING;
            });

            if( pJob == nullptr )
            {
                if( m_bMainThreadShouldWork.load( std::memory_order_relaxed ) == false )
                {
                    X_SHEDULER_LOG( "Main thread worker[%d] Awakes to terminate itself", getWorkerUID() );

                    // Reset to default
                    m_bMainThreadShouldWork.store( true, std::memory_order_relaxed );
                    return;
                }

                if( m_State == x_scheduler::STATE_EXITING )
                {
                    X_SHEDULER_LOG( "Main thread worker[%d] Awakes to terminate itself", getWorkerUID() );
                    return;
                }

                X_SHEDULER_LOG( "Main thread Worker[%d] Falsely Awake up", getWorkerUID() );
                continue;
            }
            else
            {
                X_SHEDULER_LOG( "Main thread  Worker[%d] Awakes Ready to work", getWorkerUID()  );
            }
        }

        //
        // Process any job
        //
        ProcessWork( pJob );
    }
}

//--------------------------------------------------------------------------------------

void x_scheduler::MainThreadStartsWorking( void ) noexcept
{
    X_SHEDULER_LOG( "Main thread becomes a worker[%d]", getWorkerUID() );
    MainThreadBecomesWorker();
    X_SHEDULER_LOG( "Main thread worker[%d] Stops contributing to the workers [%d]", getWorkerUID(), m_nWorkersActive.load()  );
}

//--------------------------------------------------------------------------------------

//static
void WorkersStartWorking( void ) noexcept
{
    x_scheduler& Scheduler = g_context::get().m_Scheduler;
    
    s_WorkerID = Scheduler.m_nWorkersActive++;
    X_SHEDULER_LOG( "Worker[%d] Created", Scheduler.getWorkerUID() );
    Scheduler.m_SleepWorkerCV.notify_all();

    //
    // Wait for the system to finish its initialization
    //
    if( Scheduler.m_State == x_scheduler::STATE_NO_INITIALIZED )
    {
        std::unique_lock<std::mutex> Lock( Scheduler.m_SleepWorkerMutex );
        Scheduler.m_SleepWorkerCV.wait( Lock, [&Scheduler]()
        { 
            return Scheduler.m_State != x_scheduler::STATE_NO_INITIALIZED; 
        });
    }

    //
    // At this point we should be working
    //
    Scheduler.ThreadBecomesWorker();

    // Done
    Scheduler.m_nWorkersActive--;
    X_SHEDULER_LOG( "Worker[%d] Destroyed Number of workers active [%d]", Scheduler.getWorkerUID(), Scheduler.m_nWorkersActive.load() );
}


