//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_base.h"

const xproperty_data::flags::t_flags xproperty_data::flags::MASK_READ_ONLY;
const xproperty_data::flags::t_flags xproperty_data::flags::MASK_ZERO;
const xproperty_data::flags::t_flags xproperty_data::flags::MASK_FORCE_SAVE;
const xproperty_data::flags::t_flags xproperty_data::flags::MASK_NOT_VISIBLE;

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY DATA COLLECTION
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

void xproperty_data_collection::appendSubtractFilterByNameAndValue( const xbuffer_view<xproperty_data> A, const xbuffer_view<xproperty_data> B )
{
    //
    // Todo: Filter array type properly
    //
    for( auto& Entry : A )
    {
        bool bFound = false;
        for( auto& FilterEntry : B )
        {
            if( Entry == FilterEntry )
            {
                bFound = true;
                break;
            }
        }

        if( !bFound )
        {
            m_List.append() = Entry;
        }
    }
}

//------------------------------------------------------------------------------------------------

void xproperty_data_collection::appendUnionFilterByName( const xbuffer_view<xproperty_data> A, const xbuffer_view<xproperty_data> B )
{
    //
    // Todo: Filter array type properly
    //
    for( auto& Entry : A )
    {
        for( auto& FilterEntry : B )
        {
            if( Entry.getType() == FilterEntry.getType() && Entry.m_Name == FilterEntry.m_Name )
            {
                m_List.append() = Entry;
                break;
            }
        }
    }
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY QUERY
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------

bool xproperty_query::NextPropertyOrChangeScope( void )
{     
    //
    // Check to see if we are done
    //
    if( m_Flags.m_STATE == state::DONE )
        return false;

    //
    // Check the case where we are poping scopes
    //
    if( m_Flags.m_STATE == state::POPING_SCOPES )
    {
        --m_nScopesToPop;
        if( m_nScopesToPop == 0 )
        {
            m_Flags.m_STATE = state::SEARCHING;
            return true;
        }
        return false;
    }

    //
    // Check the case that a property fail
    //
    x_assert( m_Flags.m_STATE == state::SEARCHING );
    if( m_bPropFound == false && m_nProccessedProperties > 0 )
    {
        //todo: May be we want to log this situation??? "Was not able to find a match for this property"
    }

    //
    // Get the next property
    //
    m_nProccessedProperties++;

    const char* const pPropName = [&]() -> const char* const
    {
        if( isSending() )
        {
            xproperty_data& Property = getGetProperty();
            if( false == m_GetNextPropertyLambda( *this, Property ) )
            {
                m_Flags.m_STATE = state::DONE;
                return nullptr;
            }
            return Property.m_Name;
        }
        else
        {
            const xproperty_data*& pProperty = getSetProperty();
            if( false == m_SetNextPropertyLambda( *this, pProperty ) )
            {
                m_Flags.m_STATE = state::DONE;
                return nullptr;
            }
            return pProperty->m_Name;
        }
    }();
    if( pPropName == nullptr ) return false;

    // Reset the found property
    m_bPropFound = false;

    //
    // Deal with the case of moving to the next property
    //
    if( pPropName[0] == '.' )  //--- Dealing with the optimized/compact path ---
    {
        // This is an error because we are at the top of the scope
        // todo: we should have this as an error
        x_assert( m_iScope != -1 );

        if( pPropName[1] == '/' )
        {
            // Stay with the current path (we may need to push another path need to loop more details)
            m_CurPropNextStringIndex         = 2;
            PrepareNextScope();
            return true;
        }

        // We should be reading numbers other wise.
        int nPopCount = 0;
        for( m_CurPropNextStringIndex = 1; m_CurPropNextStringIndex < 10; m_CurPropNextStringIndex++ )
        {
            if( x_isdigit<xchar>( pPropName[m_CurPropNextStringIndex] ) )
            {
                nPopCount *= 10;
                nPopCount += static_cast<int>(pPropName[m_CurPropNextStringIndex] - '0');
            }
            else
            {
                // If this is not the case then it is an ill form string
                // todo: make this into an error stead? log it? skip this property?
                x_assert( pPropName[m_CurPropNextStringIndex] == '/' );
                m_CurPropNextStringIndex++;
                break;
            }
        }

        // Finish gartering info about our property
        PrepareNextScope();

        // We need to pop a few scopes
        m_nScopesToPop              = nPopCount;
        m_Flags.m_STATE             = state::POPING_SCOPES; 

        // Update the final scope count when we are done popping
        m_iScope                   -= nPopCount;
    }
    else //--- Handle the case of a fully specify path ---
    {
        int nPopCount = 0;
        m_CurPropNextStringIndex = 0;
        for( int i = 0; i < m_iScope; i++ )
        {
            PrepareNextScope();
            if( m_CurPropNextCRC != m_ScopeData[i].m_CRC )
            {
                nPopCount = m_iScope - i;
                break;
            }
            else
            {
                //x_assert( m_ScopeData[i].m_ArrayIndex == m_CurPropArrayIndex );
            }
        }

        // If not scope changes then we are ready to continue in this scope
        if( nPopCount == 0 )
        {
            // we always should leave the previous loop searching for scopes
            x_assert( m_iScope == 0 || m_Flags.m_IS_SEARCH_SCOPES == true );
            x_assert( m_iScope == 0 || m_CurPropNextCRC == m_ScopeData[m_iScope-1].m_CRC );
            PrepareNextScope();
            return true;
        }

        // We need to pop a few scopes
        m_nScopesToPop              = nPopCount;
        m_Flags.m_STATE             = state::POPING_SCOPES;

        // Update the final scope count when we are done poping
        m_iScope                   -= nPopCount;
    }

    return false;
}

//--------------------------------------------------------------------------------------

void xproperty_query::PrepareNextScope( void )
{
    // Set the next scope string index
    m_CurPropStringIndex      = m_CurPropNextStringIndex;
    m_bPropFound              = false;

    // Compute the next CRC, and set the new m_CurPropNextStringIndex
    const char* pString = isSending()? getGetProperty().m_Name : getSetProperty()->m_Name;
        
    m_CurPropNextCRC    = ~0u;
    for( m_CurPropNextStringIndex = m_CurPropStringIndex; pString[ m_CurPropNextStringIndex ] && pString[ m_CurPropNextStringIndex ] != '/'; ++m_CurPropNextStringIndex )
    {
        if( pString[m_CurPropNextStringIndex] == '[' )
        {
            m_CurPropArrayIndex = 0;
            m_CurPropNextStringIndex++;    
            if( pString[m_CurPropNextStringIndex] == ']' )
            {
                // we must be dealing with the count
                x_assert( x_strcmp( &pString[m_CurPropNextStringIndex+1], "Count") == 0 );
                m_CurPropArrayIndex = -1;
            }
            else do
            {
                // we can not run out of string... if this happens it is an ill formatted string
                x_assert( pString[m_CurPropNextStringIndex] );

                if( x_isdigit( pString[m_CurPropNextStringIndex] ) )
                {
                    m_CurPropArrayIndex *= 10;
                    m_CurPropArrayIndex += pString[m_CurPropNextStringIndex] - '0';
                }
                else
                {
                    // todo: This should be an error
                    x_assert( false );
                }

            } while( pString[++m_CurPropNextStringIndex] != ']' );
        }
        else
        {
            m_CurPropNextCRC = (m_CurPropNextCRC >> 8) ^ x_meta_prog_details::crc32::s_CRCTable[ (m_CurPropNextCRC & 0xFF) ^ pString[ m_CurPropNextStringIndex] ];
        }
    }
    m_CurPropNextCRC = ~m_CurPropNextCRC;
        
    // Let the system know that from the point of view of the user this is an actual property not a scope
    // since it is the last string inside the path
    m_Flags.m_STATE             = state::SEARCHING;
    if( pString[ m_CurPropNextStringIndex ] ) 
    {
        // skip the '/'
        m_CurPropNextStringIndex ++;
        m_Flags.m_IS_SEARCH_SCOPES  = true;
    }
    else
    {
        m_Flags.m_IS_SEARCH_SCOPES  = false;
    }
}


//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY ENUM
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

void xproperty_enum::scope::AddProperty( xstring::const_str PropertyName, xproperty_data::type Symbol, xstring::const_str Help, flags Flags, u32 GizmosFlags )
{
    //
    // Filter properties
    //
    if( m_Enum.m_EnumMode == mode::SAVE )
    {
        if( (Flags & xproperty_data::flags::MASK_FORCE_SAVE) == flags::MASK_ZERO )
        {
            if( (Flags & xproperty_data::flags::MASK_READ_ONLY).m_Value )
                return;
        }
    }
    else
    {
        if( Flags & xproperty_data::flags::MASK_NOT_VISIBLE )
            return;
    }

    //
    // Get ready to compute the scope for this property
    //
    xstring             MyPropName( units_chars{ 200 } );                   // Allocate a long enough string
    auto&               Entry       = m_Enum.m_Scope[m_Enum.m_iScope];    // 
    xbuffer_view<char>  PropName    = MyPropName;                           // ready to be consume 
    int                 iStart      = 0;                                    // How many chars we have written so far in the buffer
    int                 iLast       = -1;                                   // Defaults to create full paths 
    
    //
    // Write how many paths we need to pop and compute the iStart
    //
    if( false == m_Enum.m_bForceFullPaths && m_Enum.m_iLastPropertyScope != -1 )
    {
        //
        // Compute how many scopes we need to pop
        //
        for( iLast = m_Enum.m_iScope; iLast > 0 && m_Enum.m_Scope[iLast].m_Sequence > m_Enum.m_LastPropertySequence; --iLast );

        //
        // Create the start of the path correctly
        //    
        const int nPop = m_Enum.m_iLastPropertyScope - iLast;
        if( nPop > 0 )
        {
            PropName[iStart++] = '.';
            iStart += x_dtoa( nPop, xbuffer_view<char>{ &PropName[iStart], PropName.getCount() - iStart}, 10 );
            PropName[iStart++] = '/';
        }
        else
        {
            if( iLast >= 0 )
            {
                PropName[iStart++] = '.';
                PropName[iStart++] = '/';
            }
        }
    }

    //
    // Write whatever parts of the path we need to
    //
    for( int iScope = iLast + 1; iScope <= m_Enum.m_iScope; ++iScope )
    {
        const int ArrayIndex = iScope > 0 ? m_Enum.m_Scope[iScope-1].m_ArrayIndex : -1; 
        auto& Entry = m_Enum.m_Scope[iScope];

        // if it is an array we must also write its index
        if( ArrayIndex >= 0 )
        {
            // we are dealing with an array entry
            PropName[iStart++] = '[';
            iStart += x_dtoa( ArrayIndex, xbuffer_view<char>( &PropName[iStart], PropName.getCount() - iStart), 10 );
            PropName[iStart++] = ']'; 
        }

        // copy string
        for( int i = 0; (PropName[iStart] = Entry.m_Name.m_pValue[i]); i++, iStart++ );

        // add folder sequence
        PropName[iStart++] = '/'; 
    }

    //
    // Ok now is time to write the actual property in the string
    //
    // if it is an array we must also write its index
    if( Entry.m_ArrayIndex >= 0 )
    {
        // we are dealing with an array entry
        PropName[iStart++] = '['; 
        iStart += x_dtoa( Entry.m_ArrayIndex, xbuffer_view<char>( &PropName[iStart], PropName.getCount() - iStart), 10 );
        PropName[iStart++] = ']'; 
    }
    iStart += x_strcpy( xbuffer_view<char>{ &PropName[iStart], PropName.getCount() - iStart }, PropertyName.m_pValue );
    x_assert( iStart < PropName.getCount() );

    //
    // Let the user know that we just computed the path
    //
    xproperty_data Data;
    Data.setup( Symbol, MyPropName, Flags, Help, GizmosFlags );
    m_Enum.m_Callback( m_Enum, Data );

    //
    // Make a reminder of the last property that we wrote
    //
    m_Enum.m_iLastPropertyScope   = m_Enum.m_iScope;
    m_Enum.m_LastPropertySequence = Entry.m_Sequence; 
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY QUERY
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------

void xproperty::Save( xtextfile& DataFile )
{
    //
    // Setup all the user types
    //
    xproperty_data::RegisterTypes( DataFile );

    //
    // Collect all the properties that we are going to save
    //
    xvector<xproperty_data> PropertyPool;
    bool bFullPath = true;
    PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
    {
        // Just collect them all inside an static array
        PropertyPool.append() = Data;
        
    }, bFullPath, xproperty_enum::mode::SAVE );
    
    //
    // Save all the properties
    //
    DataFile.WriteRecord( "Properties", PropertyPool.getCount<int>() );
    PropQueryGet( [&]( xproperty_query& Query, xproperty_data& Data )
    {
        //
        // First read the results if any
        //
        if( Query.hasResults() )
        {
            Query.getResults().SerializeOut( DataFile );
        }

        //
        // are we done?
        //
        if( Query.getPropertyIndex() == PropertyPool.getCount() ) 
        {
            return false;
        }

        //
        // Tell which property to get next
        //
        xprop_s32::setup( Data, PropertyPool[ Query.getPropertyIndex() ].m_Name );

        return true;
    });
}

//------------------------------------------------------------------------------------------------

bool xproperty::Load( xtextfile& DataFile ) 
{
    if( !(DataFile.getRecordName() == X_STR("Properties")) )
        return false;

    //
    // Setup all the user types
    //
    xproperty_data::RegisterTypes( DataFile );

    //
    // Setting values
    //
    xproperty_data Temp;
    PropQuerySet( [&]( xproperty_query& Query, const xproperty_data*& pData )
    {
        //
        // are we done?
        //
        if( Query.getPropertyIndex() == DataFile.getRecordCount() ) 
        {
            return false;
        }

        //
        // Set all the values to 123
        //
        DataFile.ReadLine();
        Temp.SerializeIn( DataFile );
        pData = &Temp;
        return true;
    });

    return true;
}

//------------------------------------------------------------------------------------------------

void xproperty::Save( const xwstring& FileName, xtextfile::access_type AccessType )
{
    xtextfile TextFile;
    TextFile.openForWriting( FileName, AccessType );
    Save( TextFile );
}

//------------------------------------------------------------------------------------------------

bool xproperty::Load( const xwstring& FileName )
{
    xtextfile TextFile;
    TextFile.openForReading( FileName );
    x_verify( TextFile.ReadRecord() );
    return Load( TextFile );
}


