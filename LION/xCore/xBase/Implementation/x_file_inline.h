//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------

template<class T> x_inline
xfile::err xfile::Write( const T& Val ) noexcept
{
    static_assert( std::is_enum<T>::value || std::is_fundamental<T>::value, "std::is_enum<T>::value || std::is_fundamental<T>::value" );
    x_assert( m_pFile );
    return WriteRaw( &Val, sizeof(T), 1 );
}

//------------------------------------------------------------------------------

template<typename T> x_inline
xfile::err xfile::WriteList( const T& View ) noexcept
{
    x_assert( m_pFile );
    return WriteRaw( &View[0], static_cast<int>(sizeof(typename T::t_entry)), View.getCount() );
}

//------------------------------------------------------------------------------

template<class T> x_inline
xfile::err xfile::Read( T& Val ) noexcept
{
    static_assert( std::is_enum<T>::value || std::is_fundamental<T>::value, "std::is_enum<T>::value || std::is_fundamental<T>::value" );
    x_assert( m_pFile );
    return ReadRaw( &Val, sizeof(T), 1 );
}

//------------------------------------------------------------------------------

template<typename T> x_inline
xfile::err  xfile::ReadList( T& View ) noexcept
{
    x_assert( m_pFile );
    return ReadRaw( &View[0], sizeof(typename std::remove_reference<T>::type::t_entry), View.getCount() );
}
  
//------------------------------------------------------------------------------
x_forceinline
bool xfile::isBinaryMode( void ) const noexcept
{
    x_assert( m_pFile );
    return !m_AccessType.m_TEXT;
}

//------------------------------------------------------------------------------
x_forceinline
bool xfile::isReadMode( void ) const noexcept
{
    x_assert( m_pFile );
    return m_AccessType.m_READ;
}

//------------------------------------------------------------------------------
x_forceinline
bool xfile::isWriteMode( void ) const noexcept
{
    x_assert( m_pFile );
    return m_AccessType.m_WRITE;
}

//------------------------------------------------------------------------------
 x_forceinline
void xfile::setForceFlush( bool bOnOff ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    m_AccessType.m_FORCE_FLUSH = bOnOff;
}

//------------------------------------------------------------------------------
x_forceinline
void xfile::AsyncAbort( void ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    if( m_AccessType.m_ASYNC == false ) return; 
    
    m_pDevice->AsyncAbort( m_pFile );    
}

//------------------------------------------------------------------------------
x_forceinline
xfile::sync_state xfile::Synchronize( bool bBlock ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    if( m_AccessType.m_ASYNC == false ) 
    {
        if( isEOF() ) return sync_state::XEOF;
        return sync_state::COMPLETED;
    }

    return m_pDevice->Synchronize( m_pFile, bBlock );    
}

//------------------------------------------------------------------------------
x_forceinline
void xfile::Flush( void ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);
    m_pDevice->Flush( m_pFile );
}

//------------------------------------------------------------------------------
x_forceinline
void xfile::SeekOrigin( s64 Offset ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_ORIGIN, Offset );
}

//------------------------------------------------------------------------------
x_forceinline
void xfile::SeekEnd( s64 Offset ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_END, Offset );
}

//------------------------------------------------------------------------------
x_forceinline
void xfile::SeekCurrent( s64 Offset ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_CURENT, Offset );
}

//------------------------------------------------------------------------------
x_forceinline
s64 xfile::Tell( void ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    return m_pDevice->Tell( m_pFile );
}

//------------------------------------------------------------------------------
x_forceinline
bool xfile::isEOF( void ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    return m_pDevice->isEOF( m_pFile );
}

//------------------------------------------------------------------------------
x_forceinline
int xfile::getC( void ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    u8 C;
    auto Err = ReadRaw( &C, sizeof(C), 1 );
    if( Err ) { Err.IgnoreError(); return -1; }
    return C;
}

//------------------------------------------------------------------------------
x_forceinline
xfile::err xfile::putC( int aC, int Count, bool bUpdatePos ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);
    if( Count == 0 ) return x_error_ok(errors);
        
    s64 iPos = 0;
    u8  C = aC;

    if( bUpdatePos == false ) iPos = Tell();
    for( s32 i=0; i<Count; i++ )
    {
        auto Err = WriteRaw( &C, static_cast<int>(sizeof(C)), 1 );
        if( Err ) return Err;
    }
    if( bUpdatePos == false ) SeekOrigin( iPos );

    return x_error_ok(errors);
}

//------------------------------------------------------------------------------
x_inline
void xfile::AlignPutC( int C, int Count, int Aligment, bool bUpdatePos ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);

    // First solve the alignment issue
    s64 Pos      = Tell();
    int PutCount = static_cast<int>(x_Align(Count+Pos, Aligment ) - Pos);

    // Put all the necessary characters
    putC( C, PutCount, bUpdatePos );
}

//------------------------------------------------------------------------------
x_forceinline
u64 xfile::getFileLength( void ) noexcept
{
    x_assert(m_pFile);
    x_assert(m_pDevice);
    return m_pDevice->Length( m_pFile );
}

//------------------------------------------------------------------------------
x_inline
void xfile::ToFile( xfile& File ) noexcept
{
    // Seek at the begging of the file
    SeekOrigin( 0 );
    s32     i;
    u8      Buffer[2*256];
    auto    Length = getFileLength();

    // Copy 256 bytes at a time
    Length -= 256;
    for( i=0; i<Length; i+=256 )
    {
        ReadRaw( Buffer, 1, 256 );
        File.WriteRaw( Buffer, 1, 256 );
    }
    Length += 256;

    // Write trailing bytes
    ReadRaw( Buffer, 1, static_cast<int>(Length-i) );
    File.WriteRaw( Buffer, 1, static_cast<int>(Length-i) );
}

//------------------------------------------------------------------------------
x_inline
void xfile::ToMemory( void* pData, s32 BufferSize ) noexcept
{
    // Seek at the begging of the file
    SeekOrigin( 0 );
    //s32 Length = GetFileLength();
    x_assert( getFileLength() >= BufferSize );

    ReadRaw( pData, BufferSize, 1 );
}

//------------------------------------------------------------------------------
// TODO: Add the fail condition
template<class T> x_inline
xfile::err xfile::ReadString( xstring_base<T>& Val ) noexcept
{
    char Buffer[256];

    Val.clear();

    s32  i;
    for( i=0; (Buffer[i] = getC()) != '\0'; i++ )
    {
        if( i > 253 )
        {
            Buffer[255]=0;
            Val.append( Buffer );
            i=0;
        }
    }

    x_assert( Buffer[i]==0 );
    Val.append( Buffer );

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

template<typename T> x_inline   
xfile::err xfile::WriteString( const xstring_base<T>& String ) noexcept
{ 
    using t_str = const xstring_base<T>;
    const xbuffer_view<const typename t_str::t_char> View
    { 
        &String[0], 
        static_cast<xuptr>(String.getLength().m_Value + 1 * sizeof(typename t_str::t_char)) 
    };
     
    return WriteList( View ); 
}


//------------------------------------------------------------------------------

template<typename... T_ARGS> x_inline        
xfile::err xfile::Printf( const xchar* pFormatStr, const T_ARGS&... Args ) noexcept 
{ 
    xstring Data{ xstring::t_characters{ 512 } };
    const xuptr c = x_vsprintf( Data, pFormatStr, xarg_list( sizeof...(T_ARGS), Args... ) ); 
    return WriteList( xbuffer_view<char>{ Data, c } ); 
}
     
