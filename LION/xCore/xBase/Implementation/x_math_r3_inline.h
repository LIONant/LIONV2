//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
void xradian3::setZero( void )
{
    m_Pitch = m_Yaw = m_Roll = xradian{ 0.0_deg };
}   

//------------------------------------------------------------------------------
inline
xradian3::xradian3( const xquaternion& Q )
{
    *this = Q.getRotation();
}

//------------------------------------------------------------------------------
inline
xradian3::xradian3( const xmatrix4& M )
{
    *this = M.getRotationR3();
}

//------------------------------------------------------------------------------
inline
xradian3& xradian3::setup( xradian Pitch, xradian Yaw, xradian Roll )
{
    m_Pitch = Pitch;
    m_Yaw   = Yaw;
    m_Roll  = Roll;

    return *this;
}

//------------------------------------------------------------------------------
inline
xradian3& xradian3::ModAngle( void )
{
    m_Pitch = x_ModAngle( m_Pitch );
    m_Yaw   = x_ModAngle( m_Yaw );
    m_Roll  = x_ModAngle( m_Roll );
    return *this;
}

//------------------------------------------------------------------------------
inline
xradian3& xradian3::ModAngle2( void )
{
    m_Pitch = x_ModAngle2( m_Pitch );
    m_Yaw   = x_ModAngle2( m_Yaw );
    m_Roll  = x_ModAngle2( m_Roll );
    return *this;
}

//------------------------------------------------------------------------------
inline
xradian3 xradian3::getMinAngleDiff( const xradian3& R ) const
{
    return xradian3( x_MinAngleDiff( m_Pitch, R.m_Pitch ),
                     x_MinAngleDiff( m_Yaw,   R.m_Yaw   ),
                     x_MinAngleDiff( m_Roll,  R.m_Roll  ) );
}

//------------------------------------------------------------------------------
inline
constexpr xradian xradian3::Difference( const xradian3& R ) const
{
    return ( m_Pitch - R.m_Pitch ) * ( m_Pitch - R.m_Pitch ) +
           ( m_Yaw   - R.m_Yaw   ) * ( m_Yaw   - R.m_Yaw   ) +
           ( m_Roll  - R.m_Roll  ) * ( m_Roll  - R.m_Roll  );
}

//------------------------------------------------------------------------------
inline
constexpr bool xradian3::isInrange( xradian Min, xradian Max ) const
{
    return ( m_Pitch >= Min ) && ( m_Pitch <= Max ) &&
           ( m_Yaw   >= Min ) && ( m_Yaw   <= Max ) &&
           ( m_Roll  >= Min ) && ( m_Roll  <= Max );
}

//------------------------------------------------------------------------------
x_forceinline
bool xradian3::isValid( void ) const
{
    return x_isValid( m_Pitch.m_Value ) && x_isValid( m_Yaw.m_Value ) && x_isValid( m_Roll.m_Value );
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator += ( const xradian3& R )
{
    m_Pitch += R.m_Pitch; 
    m_Yaw   += R.m_Yaw; 
    m_Roll  += R.m_Roll;

    return *this;
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator -= ( const xradian3& R )
{
    m_Pitch -= R.m_Pitch; 
    m_Yaw   -= R.m_Yaw; 
    m_Roll  -= R.m_Roll;

    return *this;
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator *= ( f32 Scalar  )
{
    m_Pitch.m_Value *= Scalar; 
    m_Yaw.m_Value   *= Scalar; 
    m_Roll.m_Value  *= Scalar;

    return *this;
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator /= ( f32 Scalar  )
{
    Scalar           = 1.0f/Scalar;
    m_Pitch.m_Value *= Scalar; 
    m_Yaw.m_Value   *= Scalar; 
    m_Roll.m_Value  *= Scalar;

    return *this;
}

//------------------------------------------------------------------------------

constexpr bool xradian3::operator == ( const xradian3& R ) const
{
    return  !( ( x_Abs( R.m_Pitch - m_Pitch ) > xradian{ XFLT_TOL } ) ||
               ( x_Abs( R.m_Yaw   - m_Yaw   ) > xradian{ XFLT_TOL } ) ||
               ( x_Abs( R.m_Roll  - m_Roll  ) > xradian{ XFLT_TOL } ) );
}

//------------------------------------------------------------------------------
inline
xradian3 operator + ( const xradian3& R1, const xradian3& R2 )
{
    return xradian3( R1.m_Pitch + R2.m_Pitch,
                     R1.m_Yaw   + R2.m_Yaw,
                     R1.m_Roll  + R2.m_Roll );
}

//------------------------------------------------------------------------------
inline
xradian3 operator - ( const xradian3& R1, const xradian3& R2 )
{
    return xradian3( R1.m_Pitch - R2.m_Pitch,
                     R1.m_Yaw   - R2.m_Yaw,
                     R1.m_Roll  - R2.m_Roll );
}

//------------------------------------------------------------------------------
inline
xradian3 operator - ( const xradian3& R )
{
    return xradian3( -R.m_Pitch,
                     -R.m_Yaw,
                     -R.m_Roll );
}

//------------------------------------------------------------------------------
inline
xradian3 operator * ( const xradian3& R, f32 S )
{
    return xradian3( R.m_Pitch * xradian{ S },
                     R.m_Yaw   * xradian{ S },
                     R.m_Roll  * xradian{ S } );
}

//------------------------------------------------------------------------------
inline
xradian3 operator / ( const xradian3& R, f32 S )
{
    S = 1.0f/S;
    return R * S;
}

//------------------------------------------------------------------------------
inline
xradian3 operator * ( f32 S, const xradian3& R )
{
    return xradian3( R.m_Pitch * xradian{ S },
                     R.m_Yaw   * xradian{ S },
                     R.m_Roll  * xradian{ S } );
}
