//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_base.h"

extern "C" 
{
    int LZ4_decompress_fast(const char* source, char* dest, int originalSize);
    int LZ4_compressHC_limitedOutput(const char* src, char* dst, int srcSize, int maxDstSize);
};

//==============================================================================
//  Quick Sort
//==============================================================================
//  
//  This implementation of the quick sort algorithm is, on average, 25% faster 
//  than most "system" versions.  The improvement ranges from a low of 10% to a 
//  high of 50%.
//  
//  Two functions are used to accomplish the sort: x_qsort and PseudoQuickSort.  
//  
//  The PseudoQuickSort function (abbreviated PQS) will only "mostly" sort a 
//  given array of items.  It is recursive, but it is guaranteed that the 
//  function will not recurse more than log base 2 of n times.
//  
//  The x_qsort function will first perform some set up.  It then optionally
//  invokes the PQS function.  Finally, it will perform an optimized insertion 
//  sort to finish off the sort process.
//  
//==============================================================================

//DOM-IGNORE-BEGIN
// Two thresholds are used to optimize the algorithm.  These values are 
// currently tuned for items having an average size of 48 bytes.

constexpr int RECURSION_THRESH = 4;     //  Items needed to invoke PQS.
constexpr int PARTITION_THRESH = 6;     //  Items needed to seek partition key.


//------------------------------------------------------------------------------
//
//  What the hell is a PseudoQuickSort?  Simple!  Its "sorta quick sort".
//
//  Find a partition element and put it in the first position of the list.  The 
//  partition value is the median value of the first, middle, and last items.  
//  (Using the median value of these three items rather than just using the 
//  first item is a big win.)
//
//  Then, the usual partitioning and swapping, followed by swapping the 
//  partition element into place.
//
//  Then, of the two portions of the list which remain on either side of the 
//  partition, sort the smaller portion recursively, and sort the larger 
//  portion via another iteration of the same code.
//
//  Do not bother "quick sorting" lists (or sub lists) which have fewer than
//  RECURSION_THRESH items.  All final sorting is handled with an insertion sort
//  which is executed by the caller (x_qsort).  This is another huge win.  This 
//  means that this function does not actually completely sort the input list.  
//  It mostly sorts it.  That is, each item will be within RECURSION_THRESH 
//  positions of its correct position.  Thus, an insert sort will be able to 
//  finish off the sort process without a serious performance hit.
//
//  All data swaps are done in-line, which trades some code space for better
//  performance.  There are only three swap points, anyway.
//
//------------------------------------------------------------------------------

static 
void PseudoQuickSort( xbyte*            pBase,          
                      xbyte*            pMax,           
                      const s32         ItemSize,        
                      const s32         RecursionThresh,
                      const s32         PartitionThresh,
                      const xfunction<int(const void*, const void*)>& Compare )
{
    xbyte* i;
    xbyte* j;
    xbyte* jj;
    xbyte* pMid;
    xbyte* pTemp;
    xbyte  c;
    s32    ii;
    xuptr  Low;
    xuptr  High;

    Low = (xuptr)(pMax - pBase);   // Total data to sort in bytes.

    // Deep breath now...
    do
    {
        //
        // At this point, lo is the number of bytes in the items in the current
        // partition.  We want to find the median value item of the first, 
        // middle, and last items.  This median item will become the middle 
        // item.  Set j to the "greater of first and middle".  If last is larger
        // than j, then j is the median.  Otherwise, compare the last item to 
        // "the lesser of the first and middle" and take the larger.  The code 
        // is biased to prefer the middle over the first in the event of a tie.
        //

        pMid = i = pBase + ItemSize * ((xuptr)(Low / ItemSize) >> 1);

        if( Low >= PartitionThresh )
        {
            j = (Compare( (jj = pBase), i ) > 0)  ?  jj : i;

            if( Compare( j, (pTemp = pMax - ItemSize) ) > 0 )
            {
                // Use lesser of first and middle.  (First loser.)
                j = (j == jj ? i : jj);
                if( Compare( j, pTemp ) < 0 )
                    j = pTemp;
            }

            if( j != i )
            {
                // Swap!
                ii = ItemSize;
                do
                {
                    c    = *i;
                    *i++ = *j;
                    *j++ = c;
                } while( --ii );
            }
        }
                
        //
        // Semi-standard quicksort partitioning / swapping...
        //

        for( i = pBase, j = pMax - ItemSize;  ;  )
        {
            while( (i < pMid) && (Compare( i, pMid ) <= 0) )
            {
                i += ItemSize;
            }

            while( j > pMid )
            {
                if( Compare( pMid, j ) <= 0 )
                {
                    j -= ItemSize;
                    continue;
                }

                pTemp = i + ItemSize;     // Value of i after swap.

                if( i == pMid )
                {
                    // j <-> mid, new mid is j.
                    pMid = jj = j;       
                }
                else
                {
                    // i <-> j
                    jj = j;
                    j -= ItemSize;
                }

                goto SWAP;
            }

            if( i == pMid )
            {
                break;
            }
            else
            {
                jj  = pMid;
                pTemp = pMid = i;
                j  -= ItemSize;
            }
SWAP:
            ii = ItemSize;
            do
            {
                c     = *i;
                *i++  = *jj;
                *jj++ = c;
            } while( --ii );

            i = pTemp;
        }

        //
        // Consider the sizes of the two partitions.  Process the smaller
        // partition first via recursion.  Then process the larger partition by
        // iterating through the above code again.  (Update variables as needed
        // to make it work.)
        //
        // NOTE:  Do not bother sorting a given partition, either recursively or
        // by another iteration, if the size of the partition is less than 
        // RECURSION_THRESH items.
        //

        j = pMid;
        i = pMid + ItemSize;

        if( (Low = (j-pBase)) <= (High = (pMax-i)) )
        {
            if( Low >= RecursionThresh )
                PseudoQuickSort( pBase, j, 
                                 ItemSize,        
                                 RecursionThresh,
                                 PartitionThresh,
                                 Compare );
            pBase = i;
            Low    = High;
        }
        else
        {
            if( High >= RecursionThresh )
                PseudoQuickSort( i, pMax,
                                 ItemSize,        
                                 RecursionThresh,
                                 PartitionThresh,
                                 Compare );
            pMax = j;
        }

    } while( Low >= RecursionThresh );
}

//DOM-IGNORE-END
//------------------------------------------------------------------------------
// Summary: 
//     Optimized quick sort function.
// Arguments:
//     pBase        - Address of first item in array.
//     NItems       - Number of items in array.
//     ItemSize     - Size of one item.
//     pCompare     - Compare function.
// Returns:
//     void
// Description:
//     The qsort function implements a quick-sort algorithm to sort an array of 
//     num elements, each of width bytes. The argument base is a pointer to the base 
//     of the array to be sorted. qsort overwrites this array with the sorted elements. 
//     The argument pCompare is a pointer to a user-supplied routine that compares two 
//     array elements and returns a value specifying their relationship. qsort calls the 
//     compare routine one or more times during the sort, passing pointers to two array 
//     elements on each call: pCompare( (void *) elem1, (void *) elem2 );
//
//     The quick sort function is recursive.  It is guaranteed that it will not 
//     recurse more than "log base 2 of NItems".  Use of x_qsort in a critical 
//     program (such as a game) should be tested under the most extreme potential 
//     conditions to prevent stack overflows.
// 
//     This implementation of the quick sort algorithm is, on average, 25% faster 
//     than most "system" versions.  The improvement ranges from a low of 10% to a 
//     high of 50%.
// See Also:
//     x_bsearch
//------------------------------------------------------------------------------
void x_qsort( void*          apBase,     
              const xuptr    NItems,    
              const int      ItemSize,  
              xfunction_view<int(const void*, const void*)> Compare )  
{
    xbyte  c;
    xbyte* i;
    xbyte* j;
    xbyte* pLow;
    xbyte* pHigh;
    xbyte* pMin;
    xbyte* pBase = reinterpret_cast<xbyte*>(apBase);

    x_assume( pBase    );
    x_assume( ItemSize > 0 );

    // Easy out?
    if( NItems <= 1 )
        return;

    // Set up some useful values.
    const auto RecursionThresh = ItemSize * RECURSION_THRESH;   // Recursion threshold in bytes
    const auto PartitionThresh = ItemSize * PARTITION_THRESH;   // Partition threshold in bytes
    const auto pMax            = pBase + (NItems * ItemSize);

    //
    // Set the 'hi' value.
    // Also, if there are enough values, call the PseudoQuickSort function.
    //
    if( NItems >= RECURSION_THRESH )
    {
        PseudoQuickSort( pBase, 
                         pMax, 
                         ItemSize, 
                         RecursionThresh, 
                         PartitionThresh, 
                         Compare );
        pHigh = pBase + RecursionThresh;
    }
    else
    {
        pHigh = pMax;
    }

    //
    // Find the smallest element in the first "MIN(RECURSION_THRESH,NItems)"
    // items.  At this point, the smallest element in the entire list is 
    // guaranteed to be present in this sublist.
    //
    for( j = pLow = pBase; (pLow += ItemSize) < pHigh;  )
    {
        if( Compare( j, pLow ) > 0 )
            j = pLow;
    }

    // 
    // Now put the smallest item in the first position to prime the next 
    // loop.
    //
    if( j != pBase )
    {
        for( i = pBase, pHigh = pBase + ItemSize; i < pHigh;  )
        {
            c    = *j;
            *j++ = *i;
            *i++ = c;
        }
    }

    //
    // Smallest item is in place.  Now we run the following hyper-fast
    // insertion sort.  For each remaining element, min, from [1] to [n-1],
    // set hi to the index of the element AFTER which this one goes.  Then,
    // do the standard insertion sort shift on a byte at a time basis.
    //
    for( pMin = pBase; (pHigh = pMin += ItemSize) < pMax;  )
    {
        while( Compare( pHigh -= ItemSize, pMin ) > 0 )
        {
            // nothing in this loop.
        }        

        if( (pHigh += ItemSize) != pMin )
        {
            for( pLow = pMin + ItemSize; --pLow >= pMin;  )
            {
                c = *pLow;
                for( i = j = pLow; (j -= ItemSize) >= pHigh; i = j )
                {
                    *i = *j;
                }
                *i = c;
            }
        }
    }
}




