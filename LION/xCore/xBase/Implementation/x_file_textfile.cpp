//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"

//------------------------------------------------------------------------------
// Description:
//     After writing the file or reading it is nice to close the file. Although
//     the destructor will close it for you in any event. But if you want to use
//     the class for doing more than one operation then you will need to close the
//     file.
//------------------------------------------------------------------------------
void xtextfile::close( void ) noexcept
{
    if( m_pFile.isValid() )
    {
        m_pFile->Close();
        if( m_States.m_bNewFile ) 
        {
            m_pFile.Delete();
        }
        else
        {
            // Transfer owner ship but don't kill it is not ours
            m_pFile.TransferOwnership();
        }
    }
}

//------------------------------------------------------------------------------
// Description:
//      Before reading a file you need to call this function which is the equivalent 
//      to fopen. The class will identify which type of file it is whether binary
//      or text. So the user doesn't need to worry about that.
//------------------------------------------------------------------------------
xfile::err xtextfile::openForReading( const xwstring& FileName ) noexcept
{
    //
    // Okay make sure that we say that we are not reading the file
    // this will force the user to stick with the writing functions
    //
    m_States.m_bReading = true;
    m_States.m_bBinary  = true;
    m_States.m_bNewFile = true;

    // Allocate the file
    x_assert( m_pFile.isNull() );
    m_pFile.New();

    auto Err = m_pFile->Open( FileName, "r" );
    if( Err ) return Err;

    // Determine signature
    u32 Signature;
    m_pFile->Read( Signature );

    if( Signature != u32('XNEZ') && Signature != u32('ZENX') )
    {
        m_States.m_bBinary = false;
    }

    //
    // Determine whether we are binary or text base
    //
    if( m_States.m_bBinary == false )
    {
        m_States.m_bBinary = false;
        m_pFile->Close();
        Err = m_pFile->Open( FileName, "rt" );
        if( Err ) return Err;
    }
    else
    {
        if( Signature != u32('XNEZ') )
        {
            m_States.m_bEndianSwap = true;
        }
        else
        {
            m_States.m_bEndianSwap = false;
        }
    }

    //
    // Initialize some of the Write variables
    //
    m_Field.Kill();       
    m_Memory.Kill();       

    // Growing this guy is really slow so we create a decent count from the start
    m_Field.GrowBy( 64 );
    m_Memory.appendList( 512 );

    return x_error_ok();
}

//------------------------------------------------------------------------------

xfile::err xtextfile::openForWriting( const xwstring& FileName, access_type Flags ) noexcept
{
    //
    // Okay make sure that we say that we are not reading the file
    // this will force the user to stick with the writing functions
    //
    m_States.m_bReading    = false;
    m_States.m_bEndianSwap = Flags.m_SWAP_ENDIAN;
    m_States.m_bBinary     = Flags.m_BINARY;
    m_States.m_bNewFile    = true;

    xstring Mode;
    Mode.append( "w" );

    if( m_States.m_bBinary == false )
    {
        Mode.append("t");
    }

    if( Flags.m_COMPRESS )
    {
        Mode.append("c");
    }

    //
    // Alloc the file handle
    //
    x_assert( m_pFile.isNull() );
    m_pFile.New();

    //
    // Open the file
    //
    auto Err = m_pFile->Open( FileName, Mode );
    if( Err ) return Err;

    //
    // Determine whether we are binary or text base
    //
    if( m_States.m_bBinary )
    {
        //TODO: Set the endian in the file system

        // Write binary signature
        u32 Signature = u32('XNEZ');
        m_pFile->Write( Signature );
    }

    //
    // Initialize some of the Write variables
    //
    m_Field.Kill();       
    m_Memory.Kill();       

    // Growing this guy is really slow so we create a decent count from the start
    m_Field.GrowBy( 64 );
    m_Memory.appendList( 256 );

    return x_error_ok();
}

//------------------------------------------------------------------------------

void xtextfile::ReadFromFile( xfile& File ) noexcept
{
    x_assert( File.isReadMode() );

    m_States.m_bNewFile    = false;
    m_States.m_bReading    = true;
    m_States.m_bEndianSwap = false; //x_FlagIsOn( Flags, FLAGS_SWAP_ENDIAN );
    m_States.m_bBinary     = File.isBinaryMode();
    m_pFile.setup( &File );

    if( m_Field.getCapacity() < 64  ) m_Field.GrowBy( 64 );
    if( m_Memory.getCount()   < 256 ) m_Memory.appendList( 256 );
}

//------------------------------------------------------------------------------

void xtextfile::WriteToFile( xfile& File ) noexcept
{
    x_assert( File.isWriteMode() );

    m_States.m_bNewFile    = false;
    m_States.m_bReading    = false;
    m_States.m_bEndianSwap = false; //x_FlagIsOn( Flags, FLAGS_SWAP_ENDIAN );
    m_States.m_bBinary     = File.isBinaryMode();
    m_pFile.setup( &File );

    if( m_Field.getCapacity() < 64  ) m_Field.GrowBy( 64 );
    if( m_Memory.getCount()   < 256 ) m_Memory.appendList( 256 );
}

//------------------------------------------------------------------------------

int xtextfile::ReadWhiteSpace( bool bReturnError )
{
    // Read any spaces
    int c = m_pFile->getC();
    while( c != -1 && x_isspace( c ) )
    {
        c = m_pFile->getC();
    }

    if( c == -1 )
    {
        if( bReturnError ) return -1;
        
        if( m_pFile->isEOF() )
        {
            x_throw( "Unexpected end of file" );
        }
        else
        {
            x_throw( "Reading Error, unable to read a character" );
        }
    }
    

    //
    // check for comments
    //
    while( c == '/' )
    {
        c = m_pFile->getC();
        if( c == '/' )
        {
            // Skip the comment
            do
            {
                c = m_pFile->getC();
            } while( c != -1 && c != '\n' );
            
            if( c == -1 )
            {
                if( bReturnError ) return -1;
                
                if( m_pFile->isEOF() )
                    x_throw( "Unexpected end of file" );
                else
                    x_throw( "Reading Error, unable to read a character" );
            }
        }
        else
        {
            x_throw( "Error reading file, unexpected symbol found [/]" );
        }

        // Skip spaces
        return ReadWhiteSpace( bReturnError);
    }

    return c;
}

//------------------------------------------------------------------------------

bool xtextfile::isFloat( s32 c ) noexcept
{
    static const char* pF = "+-Ee.#QNABIF";
    s32 i;
    
    if( (c >= '0' && c <= '9') ) return true;
    
    for( i=0; (c != pF[i]) && pF[i]; i++);
    
    if( pF[i] == 0 ) return false;
    
    return true;
}

//------------------------------------------------------------------------------
// Description:
//      Read a field element from the file. A field may contain one or more types. 
//      Make sure you pass the address for any type that you are reading and make sure
//      that the sizes matches what the file formats specifies.
// Example:
// <CODE> TextFile.ReadField( "Position:fff", &A, &B, &C ); </CODE>
//------------------------------------------------------------------------------
s32 xtextfile::ReadField( const char* pFieldName, ... )
{
    x_assert( m_iLine > 0 );

    //
    // Create a mapping from user order to file order of fields
    //
    if( m_iLine == 1 )
    {
        s32 i;
        for( i=0; i<m_Field.getCount(); i++ )
        {
            s32    j;
            field& Field = m_Field[i];
            
            // Make sure that is the same length
            if( pFieldName[ Field.m_TypeOffset - 1 ] != ':' )
                continue;

            // if it is then double check the string
            for( j=0; (Field.m_Type[j] == pFieldName[j]) && pFieldName[j]!=':'; j++ );

            // The string must not be the same
            if( Field.m_Type[j] != pFieldName[j] )
                continue;

            //
            // Lets check whether is a special type
            //
            field&  FieldD    = m_Field[i];
            user&   User      = m_User.append();

            if( FieldD.m_iFieldDecl >= 0 )
            {
                field&        FieldT = m_Field[ FieldD.m_iFieldDecl ];
                xbool         bFound;
                
                if( m_States.m_bBinary && m_Memory[ FieldT.m_iMemory[0] ] == '.' )
                {
                    const char* pType  = m_UserTypes[ FieldT.m_iUserType ].m_UserType;
                    s32 t=0;
                    for( j++; pType[t] == pFieldName[j] && pFieldName[j]; j++, t++ );
                    
                    bFound = (pType[t] == pFieldName[j]);
                }
                else
                {
                    const char* pType  = &m_Memory[ FieldT.m_iMemory[0] ];
                    
                    // Okay lefts finish comparing
                    s32 t=0;
                    for( j++; pType[t] == pFieldName[j] && pFieldName[j]; j++, t++ );
                    
                    bFound = (pType[t] == pFieldName[j]);
                }

                if( false == bFound )
                {
                    User.m_iFieldType = -1;
                    User.m_iFieldData = -1;
                    X_LOG( "Warning trying to read a dynamic type which is different from the file %s", pFieldName );
                }
                else
                {
                    User.m_iFieldType = FieldD.m_iFieldDecl;
                    User.m_iFieldData = i;
                }
            }
            else
            {
                // Okay lefts finish comparing
                j++;
                bool bFound;
                if( m_States.m_bBinary && Field.m_Type[j] == '.' )
                {
                    const char* p = m_UserTypes[ Field.m_iUserType ].m_UserType;
                    s32 t;
                    for( t=0; p[t] == pFieldName[j] && pFieldName[j]; j++,t++ );
                    bFound = (p[t] == pFieldName[j]);
                }
                else
                {
                    for(  ; Field.m_Type[j] == pFieldName[j] && pFieldName[j]; j++ );
                    bFound = (Field.m_Type[j] == pFieldName[j]);
                }

                // if the types are not the same then the version of the file is not the same
                if( bFound == false )
                {
                    User.m_iFieldType = -1;
                    User.m_iFieldData = -1;
                    X_LOG( "Warning trying to read a type which is different from the file %s", pFieldName );
                }
                else
                {
                    User.m_iFieldType = i;
                    User.m_iFieldData = i;
                }
            }

            // Found what we were looking for
            break;
        }

        // Did we found the type
        if( i == m_Field.getCount() )
        {
            user& User = m_User.append();
            User.m_iFieldType = -1;
            User.m_iFieldData = -1;
            X_LOG( "Unable to find the field amount the types. %s", pFieldName );
        }
    }

    //
    // Okay we should be able to give the user the data now
    //
    user& User = m_User[m_iField];

    // This slot from the user is disable
    if( User.m_iFieldData == -1 )
    {
        m_iField++;
        return -1;
    }

    const char*     pRef      = nullptr;    // This is where the string containing the types is
    field&          FieldData = m_Field[ User.m_iFieldData ];

    // collect where the type id coming from
    if( User.m_iFieldType != User.m_iFieldData )
    {
        field& Field = m_Field[ User.m_iFieldType ];
        pRef         = &m_Memory[ Field.m_iMemory[0] ];
        x_assert( Field.m_nTypes == 1 );
    }
    else
    {
        field& Field = m_Field[ User.m_iFieldType ];
        pRef         = &Field.m_Type[ Field.m_TypeOffset ];
    }

    //
    // Is this a user type?
    //
    if( pRef[0] == '.' )
    {
        s32 Index;
        if( m_States.m_bBinary )
        {
            Index = m_Field[ User.m_iFieldType ].m_iUserType;
        }
        else
        {
            if( false == m_UserTypes.BinarySearch( user_type( pRef ), Index) )
            {
                x_throw( "Unable to find user type [%s]", pRef );
            }
        }
        
        const user_type& UserType = m_UserTypes[Index];
        
        // Set the atomic types
        pRef = UserType.m_SystemType;
    }
    
    //
    // Okay collect data now
    //
    xva_list    Args;
    x_va_start( Args, pFieldName );

    for( s32 i=0; pRef[i] ; i++ )
    {
        m_nTypesLine++;
        switch( pRef[i] )
        {
            case 'h':
            case 'f': 
            case 'd':
                {
                        u32* p = (va_arg( Args, u32* ));     // get the type 
                        *p = *((u32*)&m_Memory[ FieldData.m_iMemory[i] ]);
                        break;
                }
            case 'F':
            case 'D':
            case 'g': 
                {
                        u64* p = (va_arg( Args, u64* ));     // get the type 
                        *p = *((u64*)&m_Memory[ FieldData.m_iMemory[i] ]);
                        break;
                }
            case 'c': 
                {
                        u8* p = (va_arg( Args, u8* ));     // get the type 
                        *p = *((u8*)&m_Memory[ FieldData.m_iMemory[i] ]);
                        break;
                }
            case 'C':
                {
                        u16* p = (va_arg( Args, u16* ));     // get the type 
                        *p = *((u16*)&m_Memory[ FieldData.m_iMemory[i] ]);
                        break;
                }
            case '<':
                {
                    xstring* p = (va_arg( Args, xstring* ));     // get the type
                    if( m_Memory[ FieldData.m_iMemory[0] ] == '.' )
                    {
                        if( m_States.m_bBinary )
                        {
                            x_assert( m_States.m_bReading );
                            const u16 Index = *(u16*)&m_Memory[ FieldData.m_iMemory[0] + 1 ];
                            FieldData.m_iUserType = m_BinReadUserTypeMap[ Index ];
                            (*p) = m_UserTypes[ FieldData.m_iUserType ].m_UserType;
                        }
                        else
                        {
                            s32 Index;
                            p->Copy((const char*)&m_Memory[ FieldData.m_iMemory[0] ]);
                            if( false == m_UserTypes.BinarySearch(user_type(*p), Index) )
                            {
                                x_throw( "Unable to find the user type [%s]",p);
                            }
                            FieldData.m_iUserType = Index;
                        }
                    }
                    else
                    {
                        p->Copy((const char*)&m_Memory[ FieldData.m_iMemory[0] ]);
                    }
                    
                    // Skip the '?' and the '>' symbols
                    i+=2;
                    break;
                }
            case 's': 
            case 'e':            
                {
                    if( m_States.m_bXStrings )
                    {
                        xstring* p = (va_arg( Args, xstring* ));     // get the type 
                        p->Copy((const char*)&m_Memory[ FieldData.m_iMemory[i] ]);
                    }
                    else
                    {
                        char* p = (va_arg( Args, char* ));     // get the type 

                        // We have to be careful here because we expect the user string to be at least 256 bytes long
                        x_strcpy( {p, 256}, ((const char*)&m_Memory[ FieldData.m_iMemory[i] ]) );
                    }
                    break;
                }
            default:
                // Wrong TYPE
                x_assert( false );
                break;
        }
    }

    // Ready for the next field
    m_iField++;

    return FieldData.m_iUserType;// User.m_iFieldType;
}

//------------------------------------------------------------------------------
// Description:
//      Reads a single string from the file. Strings are a tricky thing to read because of its undefined lengths
//      This function makes it safe to read strings from the file. As the xstring is design to cope with any size.
//      We recommend to use this function when ever you need to read a string for safety reasons.
//------------------------------------------------------------------------------
s32 xtextfile::ReadFieldXString( const char* FieldName, xstring& Str )
{
    m_States.m_bXStrings = true;
    const s32 Answer = ReadField( FieldName, &Str );
    m_States.m_bXStrings = false;
    return Answer;
}

//------------------------------------------------------------------------------

s32 xtextfile::ReadComponent( s32 Type )
{
    s32 Offset = 0;
    switch( Type )
    {
    default:
        x_throw( "Error reading the file" );
        break;

    case 'f':
        {
            // Align the Offset to be happy
            Offset=m_iCurrentOffet = x_Align( m_iCurrentOffet, 4 );

            if( m_States.m_bBinary )
            {
                m_pFile->Read( *((u32*)&m_Memory[ m_iCurrentOffet ]) );
            }
            else
            {
                s32                     iBuffer=0;
                xarray<char,256>        Buffer;

                // Read out all the white space
                s32 c = ReadWhiteSpace();

                // Copy all the characters 
                while( isFloat(c) )
                {
                    Buffer[iBuffer++]=c;
                    c = m_pFile->getC();
                }

                Buffer[iBuffer]=0;

                *((f32*)&m_Memory[ m_iCurrentOffet ]) = x_atof32( &Buffer[0] );

                // Sanity check
                x_assert( c == ' ' || c == '\n' || c == 10 );
            }
            m_iCurrentOffet += 4;
            break;
        }
    case 'F':
        {
            // Align the Offset to be happy
            Offset=m_iCurrentOffet = x_Align( m_iCurrentOffet, 8 );

            if( m_States.m_bBinary )
            {
                m_pFile->Read( *((u64*)&m_Memory[ m_iCurrentOffet ]) );
            }
            else
            {
                s32                     iBuffer=0;
                xarray<char,256>        Buffer;

                // Read out all the white space
                s32 c = ReadWhiteSpace();

                // Copy all the characters 
                while( isFloat(c) )
                {
                    Buffer[iBuffer++]=c;
                    c = m_pFile->getC();
                }

                Buffer[iBuffer]=0;

                *((f64*)&m_Memory[ m_iCurrentOffet ]) = x_atof64( &Buffer[0] );

                // Sanity check
                x_assert( c == ' ' || c == '\n' || c == 10 );
            }
            m_iCurrentOffet += 8;
            break;
        }
    case 'd':
        {
            // Align the Offset to be happy
            Offset=m_iCurrentOffet = x_Align( m_iCurrentOffet, 4 );

            if( m_States.m_bBinary )
            {
                m_pFile->Read( *((u32*)&m_Memory[ m_iCurrentOffet ]) );
            }
            else
            {
                s32                     iBuffer=0;
                xarray<char,256>        Buffer;

                // Read out all the white space
                s32 c = ReadWhiteSpace();

                // Copy all the characters 
                while( (c>='0'&&c<='9')||(c=='-')||(c=='+') )
                {
                    Buffer[iBuffer++]=c;
                    c = m_pFile->getC();
                }

                Buffer[iBuffer]=0;

                *((s32*)&m_Memory[ m_iCurrentOffet ]) = x_atoi32( &Buffer[0] );

                // Sanity check
                x_assert( c == ' ' || c == '\n' || c == 10 );
            }
            m_iCurrentOffet += 4;
            break;
        }
    case 'c':
        {
            Offset=m_iCurrentOffet;

            // Align the Offset to be happy
            if( m_States.m_bBinary )
            {
                m_pFile->Read( m_Memory[ m_iCurrentOffet ] );
            }
            else
            {
                s32                  iBuffer=0;
                xarray<char,256>     Buffer;

                // Read out all the white space
                s32 c = ReadWhiteSpace();

                // Copy all the characters 
                while( (c>='0'&&c<='9')||(c=='-')||(c=='+') )
                {
                    Buffer[iBuffer++]=c;
                    c = m_pFile->getC();
                }

                Buffer[iBuffer]=0;

                m_Memory[ m_iCurrentOffet ] = x_atoi32( &Buffer[0] );

                // Sanity check
                x_assert( c == ' ' || c == '\n' || c == 10 );
            }
            m_iCurrentOffet += 1;
            break;
        }
    case 'C':
        {
            // Align the Offset to be happy
            Offset=m_iCurrentOffet = x_Align( m_iCurrentOffet, 2 );

            if( m_States.m_bBinary )
            {
                m_pFile->Read( *((u16*)&m_Memory[ m_iCurrentOffet ]) );
            }
            else
            {
                s32                     iBuffer=0;
                xarray<char,256>        Buffer;

                // Read out all the white space
                s32 c = ReadWhiteSpace();

                // Copy all the characters 
                while( (c>='0'&&c<='9')||(c=='-')||(c=='+') )
                {
                    Buffer[iBuffer++]=c;
                    c = m_pFile->getC();
                }

                Buffer[iBuffer]=0;

                *((s16*)&m_Memory[ m_iCurrentOffet ]) = x_atoi32( &Buffer[0] );

                // Sanity check
                x_assert( c == ' ' || c == '\n' || c == 10 );
            }
            m_iCurrentOffet += 2;
            break;
        }
     case 'e':
         {
             Offset=m_iCurrentOffet;
 
             if( m_States.m_bBinary )
             {
                 // Read all the characters
                 while( !!(m_Memory[ m_iCurrentOffet++ ] = m_pFile->getC()) );
             }
             else
             {
                 // Read out all the white space
                 s32 c = ReadWhiteSpace();
                 
                 // Read all the characters
                 do
                 {
                     m_Memory[ m_iCurrentOffet++ ] = c;
                     c = m_pFile->getC();
                 } while( c>='!' && c <='~' );  // Most printable characters are okay for enums
 
                 if( c != ' ' && c != '\n' && c != 10 ) x_throw( "Unexpected character in an enumerated value" );
 
                 // terminate the string correctly
                 m_Memory[ m_iCurrentOffet++ ] = 0;
             }
             break;
         }
    case 's':                                
        {
            Offset=m_iCurrentOffet;

            if( m_States.m_bBinary )
            {
                // Read all the characters
                while( !!(m_Memory[ m_iCurrentOffet++ ] = m_pFile->getC()) );
            }
            else
            {
                // Handy string for debugging
                //const char* pString = &m_Memory[ m_iCurrentOffet ];
                
                // Read out all the white space
                s32 c = ReadWhiteSpace();
                
                if( c !='"' )
                {
                    x_throw( "Unable to find the beginning of a string. expecting [\"] but found [%c] ascii code %d ", c, c );
                }

                //
                // Read in the string
                //
                s32 curChar = m_pFile->getC();
                while( curChar != -1 && curChar != '"' )
                {
                    m_Memory[ m_iCurrentOffet ] = curChar;
                    m_iCurrentOffet++;
                    curChar = m_pFile->getC();
                }

                if( curChar == -1 )
                {
                    if( m_pFile->isEOF() )
                        x_throw( "Unexpected end of file" );
                    else
                        x_throw( "Reading Error while reading a string from the file" );
                }
                
                // terminate the string correctly
                m_Memory[ m_iCurrentOffet++ ] = 0;
            }
            break;
        }
    case 'h':
        {
            // Align the Offset to be happy
            Offset=m_iCurrentOffet = x_Align( m_iCurrentOffet, 4 );

            if( m_States.m_bBinary )
            {
                m_pFile->Read( *((u32*)&m_Memory[ m_iCurrentOffet ]) );
            }
            else
            {
                // Read out all the white space
                s32 c = ReadWhiteSpace();

                u32 N=0;
                // Copy all the characters 
                while( (c>='0'&&c<='9') || (c>='a'&&c<='f') || (c>='A'&&c<='F') )
                {
                    s32 v;
                    if( c>'9' ) v = (x_tolower(c)-'a')+10;
                    else        v = (x_tolower(c)-'0');
                    N <<= 4;
                    N |= (v&0xF);
                    c = m_pFile->getC();
                }

                // We didn't read anything? that is bad
                *((s32*)&m_Memory[ m_iCurrentOffet ]) = N;

                // Sanity check
                x_assert( c == ' ' || c == '\n' || c == 10 || c == 13 );
            }

            m_iCurrentOffet += 4;
            break;
        }
    case 'D':
    case 'g':
        {
            // Align the Offset to be happy
            Offset=m_iCurrentOffet = x_Align( m_iCurrentOffet, 8 );

            if( m_States.m_bBinary )
            {
                m_pFile->Read( *((u64*)&m_Memory[ m_iCurrentOffet ]) );
            }
            else
            {
                // Read out all the white space
                s32 c = ReadWhiteSpace();

                // Copy all the characters 
                u32 N=0;
                while( (c>='0'&&c<='9') || (c>='a'&&c<='f') || (c>='A'&&c<='F') )
                {
                    s32 v;
                    if( c>'9' ) v = (x_tolower(c)-'a')+10;
                    else        v = (x_tolower(c)-'0');
                    N <<= 4;
                    N |= (v&0xF);
                    c = m_pFile->getC();
                }

                if( c!=':' ) x_throw( "Unexpected character in a guid" );

                u64 Val1 = N;

                // read second part 
                c = m_pFile->getC();
                N=0;
                while( (c>='0'&&c<='9') || (c>='a'&&c<='f') || (c>='A'&&c<='F') )
                {
                    s32 v;
                    if( c>'9' ) v = (x_tolower(c)-'a')+10;
                    else        v = (x_tolower(c)-'0');
                    N <<= 4;
                    N |= (v&0xF);
                    c = m_pFile->getC();
                }

                if( c !=' ' && c!='\n' && c != 10) x_throw( "Unexpected character in a guid" );

                u64 Val2 = N;

                // Combine both numbers and set the final value
                *((u64*)&m_Memory[ m_iCurrentOffet ]) = (Val1<<32)|Val2;
            }
            m_iCurrentOffet += 8;
            break;
        }
    case '?':
        {
            Offset = m_iCurrentOffet;

            if( m_States.m_bBinary )
            {
                s32 c = m_pFile->getC();
                if( c == '.' )
                {
                    // Align the Offset to be happy
                    Offset=m_iCurrentOffet = x_Align( m_iCurrentOffet+1, 2 )-1;
                    
                    // Leave the period to indicate that is a user type
                    m_Memory[m_iCurrentOffet++] = '.';
                    
                    // Read the imapping for the user type
                    u16 iMapping;
                    m_pFile->Read( iMapping );
                    
                    // Write the final remapped type
                    *((u16*)&m_Memory[ m_iCurrentOffet ]) = m_BinReadUserTypeMap[ iMapping ];
                    
                    m_iCurrentOffet += 2;
                }
                else if( c == '<' )
                {
                    // Read all the types
                    for( ; !!(m_Memory[ m_iCurrentOffet ] = m_pFile->getC()); m_iCurrentOffet++ );

                    // Make sure that we are doing okay
                    if( m_Memory[m_iCurrentOffet-1] != '>' ) x_throw ("Expecting a '>' Token but we didn't find it" );

                    // Terminate the string
                    m_Memory[ m_iCurrentOffet-1 ] = 0;
                }
                else
                {
                    // Expecting '<' or '.' but we didn't find it
                    x_assert( 0 );
                }
            }
            else
            {
                // Read out all the white space
                s32 c = ReadWhiteSpace();

                //
                // Read in the user type
                //
                if( c == '.' )
                {
                    // Save the period as it is part of the user type name
                    m_Memory[ m_iCurrentOffet++ ] = c;
                    
                    // Read the name of the type
                    while( x_isspace( c = m_pFile->getC() ) == false )
                    {
                        if( c == -1 )
                        {
                            x_throw( "Unexpected end of file while reading a user type" );
                        }
                        
                        m_Memory[ m_iCurrentOffet++ ] = c;
                    }
                    
                    // If this happens this means that we didn't read any types so we found '<>'
                    // we must have types!
                    x_assert( Offset != m_iCurrentOffet );
                    
                    // Terminate the string
                    m_Memory[ m_iCurrentOffet++ ] = 0;
                }
                else
                {
                    //
                    // we should have the right character by now
                    //
                    if( c != '<' ) x_throw( "Expecting '<' but we didn't find it" );

                    // Read the types
                    c = m_pFile->getC();
                    while( (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') )
                    {
                        m_Memory[ m_iCurrentOffet++ ] = c;                    
                        c = m_pFile->getC();
                        if( c == -1 )
                        {
                            if( m_pFile->isEOF() )
                                x_throw( "Unexpected end of file" );
                            else
                                x_throw( "Reading a type but... Error, unable to read a character" );
                        }
                    }
                        
                    // Make sure that we are doing okay
                    if( c != '>' ) x_throw ("Expecting a '>' Token but we didn't find it" );
                
                    // If this happens this means that we didn't read any types so we found '<>'
                    // we must have types!
                    x_assert( Offset != m_iCurrentOffet );

                    // Terminate the string
                    m_Memory[m_iCurrentOffet++] = 0;
                    
                    //
                    // Sanity check
                    //
                    for( s32 i=0; m_Memory[Offset+i]; i++ )
                    {
                        // Make sure that the user enter a valid type
                        if( isValidType( m_Memory[Offset+i] ) == false )
                            x_throw( "We have read an invalid type %s", m_Memory[Offset]);
                    }
                }
            }


            break;
        }
    }

    // Increment the number of types written
    // m_nTypesLine++;

    //
    // Make sure that there are always plenty of memory left
    //
    if( (m_Memory.getCount()-m_iCurrentOffet) < 512 ) 
        m_Memory.appendList( m_Memory.getCount() + 512 );

    return Offset;
}

//------------------------------------------------------------------------------
// Description:
//      When ever reading you first need to call this function before any fields can be read.
//      The reason for it is that the class handles one line/row at a time worth of data.
//      And the reasons for that is that the user can read as many fields from that line as he wants and skip the rest.
//      This makes it very handy for handling multiple versions of the data.
//------------------------------------------------------------------------------
void xtextfile::ReadLine( void )
{
    s32                 c;
    s32                 Size=0;
    xarray<char,256>    Buffer;

    // Make sure that the user doesn't read more lines than the record has
    x_assert( m_iLine <= m_Record.m_Count );

    //
    // If it is the first line we must read the type information before hand
    //
    if( m_iLine == 0 )
    {
        // Reset the user field offsets
        m_User.DeleteAllEntries();

        // Solve types
        if( m_States.m_bBinary )
        {
            // make sure it starts okay
            if( m_pFile->getC() != '{' ) x_throw( "Error reading file. Expecting '{' but didn't find it" );
            if( m_pFile->getC() !=  0  ) x_throw( "Error reading file. Expecting 'end_of_string' but didn't find it" );

            // Read all the types
            do
            {
                // Read type information
                Size=0;
                c = m_pFile->getC();
                while( c != -1 )
                {
                    Buffer[Size++] = c;
                    if( c == 0 ) break;
                    if( c == '.' )
                    {
                        m_pFile->Read( *((u16*)&Buffer[Size]) );
#ifdef X_DEBUG
                        u16 Index = *((u16*)&Buffer[Size]);
                        x_assert( Index <= m_BinReadUserTypeMap.getCount() );
#endif
                        
                        Size+=2;
                        break;
                    }
                    c = m_pFile->getC();
                }

                if( c == -1 )
                {
                    if( m_pFile->isEOF()  ) x_throw( "Found an expected end of file");
                    else x_throw( "Found an error while reading the types");
                }

                if( Buffer[0] != '}' )
                {
                    // Okay build the type information
                    BuildTypeInformation( Buffer );
                }

            } while( Buffer[0] != '}' );
        }
        else
        {
            // Read out all the white space
            c = ReadWhiteSpace();

            //
            // we should have the right character by now
            //
            if( c != '{' ) x_throw( "Unable to find the types" );

            // Get the next token
            c = ReadWhiteSpace();

            do
            {
                // Read a word
                Size=0;
                while( (c >= 'a' && c <= 'z') || 
                       (c >= 'A' && c <= 'Z') || 
                       (c=='_') ||
                       (c==':') || 
                       (c=='>') || 
                       (c=='<') ||
                       (c=='?') ||
                       (c >= '0' && c <= '9' ) ||
                       ( c == '.') )
                {
                    Buffer[Size++] = c;                    
                    c = m_pFile->getC();
                    if( c == -1 )
                    {
                        if( m_pFile->isEOF()  ) x_throw( "Found an expected end of file");
                        else x_throw( "Unexpected error while reading the file");
                    }
                }
            
                // Terminate the string
                Buffer[Size++] = 0;

                // Okay build the type information
                BuildTypeInformation( Buffer );

                // Read any white space
                if( x_isspace( c ))
                {
                    c = ReadWhiteSpace();
                }

            } while( c != '}' );
        }
    }

    //
    // Okay now we must read a line worth of data
    //    
    for( s32 f=0; f<m_Field.getCount(); f++ )
    {
        field& Field = m_Field[f];

        switch( Field.m_iFieldDecl )
        {
        case -2:
            {
                Field.m_iMemory[0] = ReadComponent( '?' );
                
                //
                // For binary lets move the index of the user type into the right place
                //
                if( m_States.m_bBinary )
                {
                    if( Field.m_iMemory[0] == '.' )
                    {
                        Field.m_iUserType = *((u16*)&Field.m_iMemory[1]);
                        Field.m_iUserType = m_BinReadUserTypeMap[ Field.m_iUserType ];
                    }
                }
                
                // Done here
                break;
            }
        case -1:
            {
                xstring& FieldType = Field.m_Type;

                //
                // First deal with user types
                //
                if( FieldType[ Field.m_TypeOffset ] == '.' )
                {
                    const user_type& UserType = m_UserTypes[ Field.m_iUserType ];
                    
                    for( s32 i=0; i<UserType.m_nSystemTypes; i++ )
                    {
                        Field.m_iMemory[i] = ReadComponent( UserType.m_SystemType[i] );
                    }
                }
                else
                {
                    for( s32 i=0; FieldType[ Field.m_TypeOffset + i]; i++ )
                    {
                        Field.m_iMemory[i] = ReadComponent( FieldType[ Field.m_TypeOffset + i] );
                    }
                }
                break;
            }
        default:
            {
                field&        FieldDecl = m_Field[Field.m_iFieldDecl];
                const char*   pType     = &m_Memory[ FieldDecl.m_iMemory[0] ];
                
                //
                // Read based on user type
                //
                if( pType[0] == '.' )
                {
                    s32 Index;
                    
                    // Deal with fast mapping for binary reads
                    if( m_States.m_bBinary )
                    {
                        Index = *((u16*)&pType[1]);
                        // Not need to remap as it already has been remapped
                    }
                    else
                    {
                        if( m_UserTypes.BinarySearch( user_type( pType ), Index ) == false )
                        {
                            x_throw( "Unable to find user type");
                        }
                    }
                    
                    const user_type& UserType = m_UserTypes[Index];
                    
                    Field.m_nTypes      = UserType.m_nSystemTypes;
                    Field.m_iUserType   = Index;
                    
                    for( s32 i=0; i<Field.m_nTypes; i++ )
                    {
                        Field.m_iMemory[i] = ReadComponent( UserType.m_SystemType[i] );
                    }
                }
                else
                {
                    for( Field.m_nTypes=0; pType[ Field.m_nTypes ]; Field.m_nTypes++ )
                    {
                        Field.m_iMemory[Field.m_nTypes] = ReadComponent( pType[ Field.m_nTypes ] );
                    }
                }
                break;
            }
        }
    }

    //
    // Increment the line count
    // and reset the field count
    // reset the memory count
    //
    m_iLine++;
    m_iField        = 0;
    m_iCurrentOffet = 0;
    m_nTypesLine    = 0;
}

//------------------------------------------------------------------------------
// Description:
//      The class has the ability to skip any record/table from the file. This is very handy
//      to skip older version of data or to skip data that you don't want to be concern about.
//------------------------------------------------------------------------------
void xtextfile::ReadNextRecord( void )
{
    for( s32 i=0; i<m_Record.m_Count; i++ )
    {
        ReadLine();
    }
}

//------------------------------------------------------------------------------

s32 xtextfile::HandleDynamicTable( void )
{
    auto    LastPosition    = m_pFile->Tell();
    s32     Count           = -2;                   // -1. for the current header line, -1 for the types
    s32     c               = m_pFile->getC();
    
    if( c == -1 )
        x_throw("Unexpected end of file while searching the [*] for the dynamic");
    
    do
    {
        if( c == '\n' )
        {
            Count++;
            c = ReadWhiteSpace( true );
            
            if( c == -1 || c == '[' )
            {
                break;
            }
        }
        else
        {
            c = m_pFile->getC();
            
            // if the end of the file is in a line then we need to count it
            if( c == -1 )
            {
                Count++;
                break;
            }
        }
    
    } while(1);

    
    if( Count <= 0  )
        x_throw("Unexpected end of file while counting rows for the dynamic table");
    
    // Rewind to the start
    m_pFile->SeekOrigin( LastPosition );
    
    // Return the count
    return Count;
}

//------------------------------------------------------------------------------
// Description:
//      The second thing you do after the read the file is to read a record header which is what
//      this function will do. After reading the header you probably want to switch base on the 
//      record name. To do that use GetRecordName to get the actual string containing the name. 
//      The next most common thing to do is to get how many rows to read. This is done by calling
//      GetRecordCount. After that you will look throw n times reading first a line and then the fields.
//------------------------------------------------------------------------------
bool xtextfile::ReadRecord( void )
{
    s32                     c;
    s32                     NameSize=0;
    xarray<char,256>        RecordName;

    x_assert( m_States.m_bReading );

    // if not we expect to read something
    if(m_States.m_bBinary ) 
    {
        // If it is the end of the file we are done
        c = m_pFile->getC();
        while( c != -1 && c != '[' && c != '<' )
        {
            c = m_pFile->getC();
        }
        
        if( c == -1 )
        {
            if( m_pFile->isEOF() )
            {
                m_States.m_bEOF = true;
            }
            return false;
        }
        
        //
        // Lets deal with user types
        //
        while( c == '<' )
        {
            xarray<char,64> SystemType;
            xarray<char,64> UserType;

            // Read the system type
            s32 i=0;
            while( (c = m_pFile->getC()) )
            {
                x_assert( c != -1 );
                SystemType[i++] = c;
            }
            
            SystemType[i] = 0;

            // Read the user type
            i=0;
            while( (c = m_pFile->getC()) )
            {
                x_assert( c != -1 );
                UserType[i++] = c;
            }
            
            UserType[i] = 0;

            //
            // Add the type
            //
            s32 Index = AddUserType( SystemType, UserType, 0xff );
            
            // Make sure that we set the right mapping for it
            s32 iSubIndex;
            m_BinReadUserTypeMap.BinarySearch( Index, iSubIndex );
            m_BinReadUserTypeMap.Insert( iSubIndex ) = Index;
            
            // Read the next character
            c = m_pFile->getC();
            x_assert( c != -1 );
        }

        //
        // Deal with a record
        //
        x_assert( c =='[' );
        if( m_pFile->getC() !=  0 ) x_throw( "Error reading file. Expecting 'end_of_string' but didn't find it" );

        // Read the record name
        while( !!(RecordName[NameSize++] = m_pFile->getC()) );
        m_Record.m_Name.Copy( RecordName );

        // Read the record count
        m_pFile->Read( m_Record.m_Count );

        // make sure it terminates okay
        if( m_pFile->getC() != ']' ) x_throw( "Error reading file. Expecting ']' but didn't find it" );
        if( m_pFile->getC() !=  0  ) x_throw( "Error reading file. Expecting 'end_of_string' but didn't find it" );
    }
    else
    {
        //
        // Skip blank spaces and comments
        //
        c = ReadWhiteSpace( true );
        if( c == -1 )
        {
            m_Record.m_Name.setNull();
            m_States.m_bEOF = true;
            return false;
        }

        //
        // Deal with user types
        // We read the type in case the user has not register it already.
        // But the user should have register something....
        //
        while( c == '<' )
        {
            s32                     nUserSystemTypeChars=0;
            xarray<char,64>    UserSystemType;
            s32                     nUserTypeChars=0;
            xarray<char,64>    UserTypeName;
            
            while( (c = m_pFile->getC()) != '>' )
            {
                if( c == -1 )
                {
                    x_throw( "Unexpected end of file while reading a user type" );
                }
                
                if( isValidType(c) == false )
                {
                    x_throw( "Found a non-atomic type in user type definition [%c]", c);
                }
                
                UserSystemType[nUserSystemTypeChars++] = c;
            }
            
            // Terminate the string
            UserSystemType[nUserSystemTypeChars] = 0;
            
            if( (c = m_pFile->getC()) != '.' )
            {
                x_throw( "Expecting a '.' but found a '%c' in user type declaration",c);
            }
            
            // Set the period as it is part of the type
            UserTypeName[nUserTypeChars++] = c;
            
            while( x_isspace(c = m_pFile->getC()) == false )
            {
                if( c == -1 )
                {
                    x_throw( "Unexpected end of file while reading a user type" );
                }
                
                UserTypeName[nUserTypeChars++] = c;
            }
            
            // Terminate the string
            UserTypeName[nUserTypeChars] = 0;
            
            //
            // Add the User type
            //
            AddUserType( UserSystemType, UserTypeName, 0xff );

            //
            // Skip spaces
            //
            if( x_isspace( c ) )
                c = ReadWhiteSpace();
        }
        
        //
        // Make sure that we are dealing with a header now
        //
        if( c != '[' )
        {
            x_throw( "Unable to find the right header symbol '['" );
        }

        // Skip spaces
        c = ReadWhiteSpace();

        do
        {
            RecordName[NameSize++] = c;
            
            c = m_pFile->getC();

        } while( x_isspace( c )==false && c != ':' && c != ']' );

        
        // Terminate the string
        RecordName[NameSize] = 0;
        m_Record.m_Name.Copy( RecordName );

        // Skip spaces
        if( x_isspace( c ) )
            c = ReadWhiteSpace();
        
        //
        // Read the record count number 
        //
        m_Record.m_Count = 1;

        if( c == ':' )
        {
            // skip spaces and zeros
            do
            {
                c = ReadWhiteSpace();
                
            } while( c == '0' );
            
            //
            // Handle the case of dynamic sizes tables
            //
            if( c == '?' )
            {
               // TODO: Handle the special reader
                m_Record.m_Count = HandleDynamicTable();
                
                // Read next character
                c = m_pFile->getC();
            }
            else
            {
                m_Record.m_Count = 0;
                while( c >= '0' && c <= '9' )
                {
                    m_Record.m_Count = m_Record.m_Count * 10 + (c-'0');
                    c = m_pFile->getC();
                }
            }

            // Skip spaces
            if( x_isspace( c ) )
                c = ReadWhiteSpace();
        }

        //
        // Make sure that we are going to conclude the field correctly
        //
        if( c != ']' )
            x_throw( "Fail reading the file. Expecting a '[' but didn't find it." );
    }

    //
    // Reset the line count
    //
    m_iLine         = 0;
    m_iField        = 0;
    m_iCurrentOffet = 0;
    m_nTypesLine    = 0;

    //
    // Kill all the lists
    //
    m_Field.DeleteAllEntries();                

    return true;
}

//------------------------------------------------------------------------------

void xtextfile::WriteRecord( const char* pHeaderName, s32 Count )
{
    x_assert( pHeaderName );
    x_assert( m_States.m_bReading == false );

    //
    // Fill the record info
    //
    m_Record.m_Name.Copy( pHeaderName );
    m_Record.m_bWriteCount  = (Count>=0);
    m_Record.m_Count        = m_Record.m_bWriteCount?Count:1;

    //
    // Reset the line count
    //
    m_iLine         = 0;
    m_iField        = 0;
    m_iCurrentOffet = 0;
    m_nTypesLine    = 0;

    //
    // Kill all the lists
    //
    m_Field.DeleteAllEntries();                
}

//------------------------------------------------------------------------------

bool xtextfile::isValidType( s32 Type ) noexcept
{
    switch( Type )
    {
    // Lets verify that the user enter a valid atomic type
    case 'f': case 'F':
    case 'd': case 'D':
    case 'c': case 'C':
    case 's': case 'e':
    case 'g': case 'h':
        return true;
    }

    // Sorry but I don't know what kind of syntax / type information 
    // the user is trying to provide.
    // Make sure that there are not extra character after the type ex:
    // "Pepe:fff " <- this is bad. "Pepe:fff" <- this is good.
    return false;
}

//------------------------------------------------------------------------------

void xtextfile::BuildTypeInformation( const char* pFieldName )
{
    s32 i;

    x_assert( pFieldName );

    //
    // Set the current field
    //
    field& Field = m_Field.append();
    
    Field.m_Type.Copy( pFieldName );
    Field.m_nTypes = 0;

    //
    // First lest find where in the string the types begin
    //
    for( i=0; pFieldName[i]; i++ )
    {
        if( pFieldName[i] == ':' ) break;

        // Now spaces are allow. It should be like this "Vector3:fff"
        x_assert(pFieldName[i] != ' ' );
    }

    // We must have some name indicating the type. It found ':' way too early
    // either that or the string was blank
    x_assert( i > 0 );

    // We didn't find the ':' character which indicates where the types begging
    x_assert( pFieldName[i] != 0 );

    // Make sure that the only reason we have stop was because we found the correct token
    x_assert( pFieldName[i] == ':' );

    i++;
    Field.m_TypeOffset = i;

    //
    // Okay now lets deal with the types
    //

    // is user is indicating that he want to deal with a complex type
    if( pFieldName[i] == '<' )
    {
        // So lets advance to the next character
        i++;
        x_assert( pFieldName[i] );

        // Now is the user declaring or using a complex type?
        if( pFieldName[i] == '?' )
        {
            // okay here we only we have one type
            Field.m_nTypes = 1;
            
            // Now lets verify that the user knew how to end the complex type
            i++;
            x_assert( pFieldName[i] == '>' );

            // Mark that we are a declaration of a type
            Field.m_iFieldDecl = -2;
        }
        else
        {
            xarray<char,256>    TypeName;
            s32                     j,k;

            // Here we set it to zero because this is going to be dependent
            Field.m_nTypes = 0;

            // Now lets read the type name:
            for( j=0; !!(TypeName[j] = pFieldName[i]); j++, i++ )
            {
                // This is the terminator for a complex type
                if( TypeName[j] == '>' ) break;
            }

            // Lets make sure that we are okay this far.
            // If the use did add a terminator we will be mess up, the complex type need
            // to be use in the fallowing way "Jack:<pepe>" this is the syntax
            x_assert( TypeName[j] == '>' );

            // Okay now lets terminate the type properly
            TypeName[j] = 0;

            //
            // Okay now lets search for the declaration
            //
            k=0;
            for( j=0; j<m_Field.getCount(); j++ )
            {
                field&        Decl  = m_Field[j];

                // Skip this field if it is not a declaration of a type
                if( Decl.m_iFieldDecl != -2 )
                    continue;

                // get the string
                xstring&  FieldName = Decl.m_Type;
                
                // Do a string compare
                for( k=0; TypeName[k] && TypeName[k] == FieldName[k]; k++ );

                // we found the declaration
                if( TypeName[k] == 0 && FieldName[k] == ':'  )
                    break;                        
            }

            // check to make sure that we have found a type declaration for this
            // complex type. If we haven't the user need to make sure that he enters 
            // a complex type declaration. The type declaration should read like this:
            // "jack:<pepe>"
            x_assert( j != m_Field.getCount() );
            x_assert( TypeName[k] == 0 );

            //
            // Okay lets write the info in our structures
            //
            Field.m_iFieldDecl = j;
        }
    }
    else if( pFieldName[i] == '.')
    {
        // If it is a user type lets mark it down as a regular type
        // we will decode it later
        Field.m_iFieldDecl = -1;
        
        if(m_States.m_bBinary && m_States.m_bReading )
        {
            Field.m_iUserType = *(u16*)&pFieldName[i+1];
            Field.m_iUserType = m_BinReadUserTypeMap[Field.m_iUserType];
            Field.m_nTypes    = m_UserTypes[ Field.m_iUserType ].m_nSystemTypes;
        }
        else
        {
            s32 Index;
            x_verify( m_UserTypes.BinarySearch( user_type( &pFieldName[i] ), Index ) );
            Field.m_iUserType = Index;
            Field.m_nTypes    = m_UserTypes[ Field.m_iUserType ].m_nSystemTypes;
        }
    }
    else
    {
        x_assert( isValidType( pFieldName[i] ) );

        // Mark that we are a generic type
        Field.m_iFieldDecl = -1;

        // Try to find how many times we are talking about
        for( Field.m_nTypes=0; pFieldName[i]; i++, Field.m_nTypes++ )
        {
            x_assert( isValidType( pFieldName[i] ) );
        }
    }
}

//------------------------------------------------------------------------------

void xtextfile::WriteComponent( s32 iField, s32 iType, s32 c, xva_list& Args, bool bIndirect )
{   
    x_assert( m_States.m_bReading == false );

    switch( c )
    {
    case 'f':
        {
            f64 p = bIndirect?(*va_arg( Args, f32* )):(va_arg( Args, f64 ));     // get the type
        
            if(m_States.m_bBinary )
            {
                f32 n = (f32)p;
                if( m_States.m_bEndianSwap ) n = x_EndianSwap(n);
                x_memcpy( &m_Memory[m_iCurrentOffet], &n, sizeof(f32));
                m_iCurrentOffet += sizeof(f32);
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet]   = 1;
                m_iCurrentOffet+=2;
                s32 iFloat = m_iCurrentOffet;

                // Okay lets write the floating point number
                m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%g", p );

                // Lets determine how many character does it have before the decimal point
                s32 i;
                for( i=0; m_Memory[iFloat+i]!='.' && m_Memory[iFloat+i]; i++ );

                *(u16*)&m_Memory[iFloat-2]   = m_iCurrentOffet - iFloat;
                Field.m_FracWidth[iType] = x_Max( Field.m_FracWidth[iType], (u8)i );

                s32 Width   = m_iCurrentOffet - iFloat;
                    Width  -= i;
                    Width  += Field.m_FracWidth[iType];
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)Width  );
            }
            break;
        }
    case 'F':
        {
            f64 p = bIndirect?(*va_arg( Args, f64* )):(va_arg( Args, f64 ));     // get the type

            if(m_States.m_bBinary )
            {
                if( m_States.m_bEndianSwap ) p = x_EndianSwap(p);
                x_memcpy( &m_Memory[m_iCurrentOffet], &p, sizeof(f64));
                m_iCurrentOffet += sizeof(f64);
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet]   = 1;
                m_iCurrentOffet+=2;
                s32 iFloat = m_iCurrentOffet;

                // Okay lets write the floating point number
                m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%Lg", p );

                // Lets determine how many character does it have before the decimal point
                s32 i;
                for( i=0; m_Memory[iFloat+i]!='.' && m_Memory[iFloat+i]; i++ );

                *(u16*)&m_Memory[iFloat-2]   = m_iCurrentOffet - iFloat;
                Field.m_FracWidth[iType] = x_Max( Field.m_FracWidth[iType], (u8)i );

                s32 Width   = m_iCurrentOffet - iFloat;
                    Width  -= i;
                    Width  += Field.m_FracWidth[iType];
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(Width)  );
            }
            break;
        }
    case 'd':
        {
            s32 p = bIndirect?*(va_arg( Args, s32* )):(va_arg( Args, s32 ));       // get the type
            if(m_States.m_bBinary )
            {
                if( m_States.m_bEndianSwap ) p = x_EndianSwap(p);
                x_memcpy( &m_Memory[m_iCurrentOffet], &p, sizeof(s32));
                m_iCurrentOffet += sizeof(s32);
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet]   = 1 + p<0?1:0;
                m_iCurrentOffet+=2;
                s32 iInt = m_iCurrentOffet;

                // Okay lets write the floating point number
                m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%d", p );

                // Set the width for this type
                *(u16*)&m_Memory[iInt-2]    = m_iCurrentOffet - iInt;
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - iInt) );
            }
            break;
        }
    case 'c':
        {
            u8 p = bIndirect?(u8)*(va_arg( Args, unsigned int* )) : (u8)(va_arg( Args, unsigned int ));       // get the type
            
            if(m_States.m_bBinary )
            {
                if( m_States.m_bEndianSwap ) p = x_EndianSwap(p);
                x_memcpy( &m_Memory[m_iCurrentOffet], &p, sizeof(u8));
                m_iCurrentOffet += sizeof(u8);
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet]   = 1;
                m_iCurrentOffet+=2;
                s32 iInt = m_iCurrentOffet;

                // Okay lets write the floating point number
                m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%d", p );

                // Set the width for this type
                *(u16*)&m_Memory[iInt-2]    = m_iCurrentOffet - iInt;
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - iInt) );
            }
            break;
        }
    case 'C':
        {
            u16 p = bIndirect?(u16)*(va_arg( Args, unsigned int* )):(u16)(va_arg( Args, unsigned int ));       // get the type
            if(m_States.m_bBinary )
            {
                if( m_States.m_bEndianSwap ) p = x_EndianSwap(p);
                x_memcpy( &m_Memory[m_iCurrentOffet], &p, sizeof(u16));
                m_iCurrentOffet += sizeof(u16);
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet]   = 1;
                m_iCurrentOffet+=2;
                s32 iInt = m_iCurrentOffet;

                // Okay lets write the floating point number
                m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%d", p );

                // Set the width for this type
                *(u16*)&m_Memory[iInt-2]   = m_iCurrentOffet - iInt;
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - iInt) );
            }
            break;
        }
    case 'E': // Not xstring can be use for writing data use MyVar()
    case 'S': x_assert( false ); break;                               
    case 'e':
    case 's':
            {
                // Strings are always indirect
                const char* p = va_arg( Args, const char* );
                x_assert(p);
                if(m_States.m_bBinary)
                {
                    for( s32 m=0; !!(m_Memory[m_iCurrentOffet]=p[m]); m++, m_iCurrentOffet++ );
                    m_iCurrentOffet++;
                }
                else
                {
                    field& Field = m_Field[iField];

                    // We reserve the first byte so that we can add additional spaces to this floating point number
                    m_Memory[m_iCurrentOffet++] = iField;
                    m_Memory[m_iCurrentOffet++] = iType;
                    *(u16*)&m_Memory[m_iCurrentOffet]   = 1;
                    m_iCurrentOffet+=2;
                    s32 indexStr = m_iCurrentOffet;

                    if(c == 'e')
                        m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%s", p);
                    else
                        m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "\"%s\"", p);

                    // Set the width for this type (we may need to have 2 bytes here if this assert is a problem)
                    // x_assert((m_iCurrentOffet - indexStr) <= 255);
                    //m_Memory[indexStr-1]         = m_iCurrentOffet - indexStr;
                    //Field.m_TypeWidth[iType] = x_Max(Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - indexStr));
                    *(u16*)&m_Memory[indexStr-2]         = m_iCurrentOffet - indexStr;
                    Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - indexStr) );
                }
                break;
            }

    case 'h':
        {
            u32 p    = bIndirect?*(va_arg( Args, u32* )):(va_arg( Args, u32 ));       // get the type
            if(m_States.m_bBinary )
            {
                if( m_States.m_bEndianSwap ) p = x_EndianSwap(p);
                x_memcpy( &m_Memory[m_iCurrentOffet], &p, sizeof(u32));
                m_iCurrentOffet += sizeof(u32);
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet] = 1;
                m_iCurrentOffet+=2;
                s32 iInt = m_iCurrentOffet;

                // Okay lets write the floating point number
                m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%X", p );

                // Set the width for this type
                *(u16*)&m_Memory[iInt-2]   = m_iCurrentOffet - iInt;
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - iInt) );
            }
            break;
        }
    case 'D':
    case 'g':
        {
            u64 p    = bIndirect?*(va_arg( Args, u64* )):(va_arg( Args, u64 ));       // get the type
            if(m_States.m_bBinary )
            {
                if( m_States.m_bEndianSwap ) p = x_EndianSwap(p);
                x_memcpy( &m_Memory[m_iCurrentOffet], &p, sizeof(u64));
                m_iCurrentOffet += sizeof(u64);
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet] = 1;
                m_iCurrentOffet+=2;
                s32 iInt = m_iCurrentOffet;

                // Okay lets write the floating point number
                m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%X:%X", (u32)(p>>32), (u32)p );

                // Set the width for this type
                *(u16*)&m_Memory[iInt-2]   = m_iCurrentOffet - iInt;
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - iInt) );
            }
            break;
        }
    case '?':
        {
            // strings are always indirect
            char* p = va_arg( Args, char* );       // get the type   
            if(m_States.m_bBinary )
            {
                // Determine if it is a user type if so make sure we follow the right syntax
                if( p[0] == '.' )
                {
                    field&  Field = m_Field[iField];
                    
                    // For user types we provide the actual index to the type been read
                    // this is faster to load and smaller in memory
                    u16 Index = Field.m_iUserType;
                    
                    // Write to the file the fact that we are a user type
                    m_Memory[m_iCurrentOffet++] = '.';
                    
                    // write the index
                    Index = ( m_States.m_bEndianSwap )?x_EndianSwap(Index) : Index;
                    *((u16*)&m_Memory[m_iCurrentOffet]) = Index;
                    m_iCurrentOffet += 2;
                }
                else
                {
                    m_Memory[m_iCurrentOffet++]='<';
                    for( s32 i=0; !!(m_Memory[m_iCurrentOffet]=p[i]); m_iCurrentOffet++, i++);
                    m_Memory[m_iCurrentOffet++]='>';
                    m_Memory[m_iCurrentOffet++]=0;
                }
            }
            else
            {
                field& Field = m_Field[iField];

                // We reserve the first byte so that we can add additional spaces to this floating point number
                m_Memory[m_iCurrentOffet++] = iField;
                m_Memory[m_iCurrentOffet++] = iType;
                *(u16*)&m_Memory[m_iCurrentOffet] = 1;
                m_iCurrentOffet+=2;
                s32 iStr = m_iCurrentOffet;

                // Determine if it is a user type if so make sure we follow the right syntax
                if( p[0] == '.' )
                {
                    m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "%s", p );
                }
                else
                {
                    m_iCurrentOffet += 1+x_sprintf( { &m_Memory[m_iCurrentOffet], m_Memory.getCount()-m_iCurrentOffet }, "<%s>", p );
                }

                // Set the width for this type
                // Set the width for this type (we may need to have 2 bytes here if this assert is a problem)
                x_assert( (m_iCurrentOffet - iStr) <= 255 );
                *(u16*)&m_Memory[iStr-2]         = m_iCurrentOffet - iStr;
                Field.m_TypeWidth[iType] = x_Max( Field.m_TypeWidth[iType], (u8)(m_iCurrentOffet - iStr) );
            }
            break;
        }
    default:
        // Unknown type
        x_assert( false );
    } 

    // Increment the number of types written
    m_nTypesLine++;

    //
    // Make sure that there are always plenty of memory left
    //
    if( (m_Memory.getCount()-m_iCurrentOffet) < 512 ) 
        m_Memory.appendList( m_Memory.getCount() + 1024*4 );
}


//------------------------------------------------------------------------------

void xtextfile::WriteField( const char* pFieldName, ... )
{
    xva_list    Args1, Args2;
    x_va_start( Args1, pFieldName );
    x_va_start( Args2, pFieldName );
    WriteField( pFieldName, Args1, Args2, false );
}

//------------------------------------------------------------------------------

void xtextfile::WriteFieldIndirect( const char* pFieldName, ... )
{
    xva_list    Args1, Args2;
    x_va_start( Args1, pFieldName );
    x_va_start( Args2, pFieldName );
    WriteField( pFieldName, Args1, Args2,  true );
}

//------------------------------------------------------------------------------

void xtextfile::WriteField( const char*& pFieldName, xva_list& Args1, xva_list& Args2, bool bIndirect )
{   
    x_assert( m_States.m_bReading == false );

    //
    // When we are at the first line we must double check the syntax of the user
    //
    if( m_iLine == 0 )
    {
        BuildTypeInformation( pFieldName );

        //
        // Initialize all the width for formating the fields
        //
        field& Field  = m_Field[ m_Field.getCount()-1 ];
        Field.m_Width = Field.m_Type.getLength().m_Value;
        
        for( s32 i=0; i<Field.m_TypeWidth.getCount(); i++ )
        {
            Field.m_TypeWidth[i] = 0;
            Field.m_FracWidth[i] = 0;
        }
    }

    //
    // Okay ready to write the data
    //
    field&      Field = m_Field[m_iField];

    switch( Field.m_iFieldDecl )
    {
    case -1:
        {
            xstring& FieldName = Field.m_Type;
            
            // Are we writing user types or atomic types
            if( FieldName[ Field.m_TypeOffset ] == '.' )
            {
                const user_type& Entry = m_UserTypes[ Field.m_iUserType ];
                u8               c;
                
                // Now we can set the number of types
                Field.m_nTypes = Entry.m_nSystemTypes;
                
                // Dump all the types
                for( s32 i=0; (c = Entry.m_SystemType[ i ]); i++ )
                {
                    Field.m_iMemory[i] = m_iCurrentOffet + 4;
                    WriteComponent( m_iField, i, c, Args1, bIndirect );
                }
            }
            else
            {
                u8 c;
                for( s32 i=0; (c = FieldName[ Field.m_TypeOffset + i ]); i++ )
                {
                    Field.m_iMemory[i] = m_iCurrentOffet + 4;
                    WriteComponent( m_iField, i, c, Args1, bIndirect );
                }
            }
            break;
        }
    case -2:
        {
            const char*  p = (va_arg( Args2, const char* ));     // get the type

            // Are we writting with a user type?
            if( p[0] == '.' )
            {
                s32       Index;
                
                 if( m_States.m_bReading )
                 {
                     Index = Field.m_iUserType;
                 }
                else
                {
                    // We must be able to find the user type
                    x_verify( m_UserTypes.BinarySearch( user_type( p ), Index ) );
                }
                const user_type& Entry = m_UserTypes[Index];
                
                // Now we can set the number of types
                Field.m_DynamicTypeCount = Entry.m_nSystemTypes;
                Field.m_iUserType        = Index;
            }
            else
            {
                // Make sure that the user is fallowing the syntax of the format.
                // types should be contain like this: "fff"
                Field.m_DynamicTypeCount = 0;
                for( s32 i=0; p[i]; i++ )
                {
                    x_assert( isValidType( p[i] ));
                    Field.m_DynamicTypeCount++;
                }
            }

            // Write type information in the file
            if(m_States.m_bBinary ) Field.m_iMemory[0] = m_iCurrentOffet;
            else            Field.m_iMemory[0] = m_iCurrentOffet + 4;
            
            WriteComponent( m_iField, 0, '?', Args1, bIndirect );

            // Done here
            break;
        }
    default:
        {
            field&      Decl    = m_Field[ Field.m_iFieldDecl ];
            const char* p       = &m_Memory[ Decl.m_iMemory[0] ];
            
            if( p[0] == '.' )
            {
                const user_type& Entry = m_UserTypes[ Decl.m_iUserType ];
                u8               c;
                s32              nTypes = 0;
                
                for( ; (c = Entry.m_SystemType[ nTypes ]); nTypes++ )
                {
                    Field.m_iMemory[nTypes] = m_iCurrentOffet + 4;
                    WriteComponent( m_iField, nTypes, c, Args1, bIndirect );
                }
                
                x_assert( nTypes == Decl.m_DynamicTypeCount );
            }
            else
            {
                x_assert( p[0] == '<' );
                
                s32 nTypes = 0;
                for( s32 i=1; p[ i ]!='>'; i++ )
                {
                    nTypes++;

                    Field.m_iMemory[i-1] = m_iCurrentOffet + 4;
                    WriteComponent( m_iField, i-1, p[ i ], Args1, bIndirect );
                }
                
                x_assert( nTypes == Decl.m_DynamicTypeCount );
            }
            break;
        }
    }

    //
    // Okay we are done advance the field count
    //
    m_iField++;
}
//------------------------------------------------------------------------------
void xtextfile::WriteUserTypes( void )
{
    if(m_States.m_bBinary )
    {
        for( user_type& UserType : m_UserTypes )
        {
            if( UserType.m_bSaved )
                continue;
            
            UserType.m_bSaved = true;
            
            // First write the standard symbol for the user types
            m_pFile->WriteList( xbuffer_view<const char>{"<", 1} );
            
            // Now dump the data for the user type
            m_pFile->WriteList( xbuffer_view<const char>{ &UserType.m_SystemType[0],  static_cast<xuptr>(UserType.m_nSystemTypes+1) } );
            m_pFile->WriteList( xbuffer_view<const char>{ &UserType.m_UserType[0],    static_cast<xuptr>(UserType.m_UserType.getLength().m_Value+1) } );
        }
    }
    else
    {
        //
        // Collect any new types
        //
        xstring Types;
        for( user_type& UserType : m_UserTypes )
        {
            if( UserType.m_bSaved )
                continue;
            
            UserType.m_bSaved = true;
            
            xarray<char,256> temp;
            x_sprintf( temp, "<%s>%s ", (const char*)UserType.m_SystemType,
                                        (const char*)UserType.m_UserType );
            Types.append( temp );
        }

        //
        // Dump all the new types if we have some
        //
        if( Types.isEmpty() == false )
        {
            s32 L = Types.getLength().m_Value;
            
            m_pFile->Printf( "\n// New Types\n");
            
            m_pFile->Printf( "//" );
            L = (L-1)>>1;
            for( s32 i=0; i<L; i++ ) m_pFile->Printf( "--" );
            
            m_pFile->Printf( "\n%s\n",
                            (const char*)Types );
        }
    }
}

//------------------------------------------------------------------------------

void xtextfile::WriteLine( void )
{
    s32 i,j;

    x_assert( m_States.m_bReading == false );

    // Make sure that the user don't try to write more lines than expected
    x_assert( m_iLine < m_Record.m_Count );

    //
    // Lets handle the binary case first
    //
    if(m_States.m_bBinary )
    {
        if( m_iLine == 0 )
        {
            //
            // Write any pending user types
            //
            WriteUserTypes();
            
            //
            // Write record header
            //
            m_pFile->WriteList( xbuffer_view<const char>{ "[", 2 } );
            m_pFile->WriteList( xbuffer_view<const char>{ &m_Record.m_Name[0], static_cast<xuptr>( m_Record.m_Name.getLength().m_Value+1 ) } );
            m_pFile->Write( m_Record.m_Count );
            m_pFile->WriteList( xbuffer_view<const char>{ "]", 2 } );
            
            //
            // Print types
            //
            m_pFile->WriteList( xbuffer_view<const char>{ "{", 2 } );
            
            for( i=0; i<m_Field.getCount(); i++ )
            {
                const field& Field = m_Field[i];
                
                if( Field.m_Type[ Field.m_TypeOffset ] == '.' )
                {
                    m_pFile->WriteList( xbuffer_view<const char>{ &Field.m_Type[0], static_cast<xuptr>( Field.m_TypeOffset + 1 ) } );
                    m_pFile->Write( Field.m_iUserType );
                }
                else
                {
                    m_pFile->WriteList( xbuffer_view<const char>{ &Field.m_Type[0], static_cast<xuptr>( Field.m_Type.getLength().m_Value+1 ) } );
                }
            }
            
            m_pFile->WriteList( xbuffer_view<const char>{ "}", 2 } );
        }

        //
        // Dump line info
        //
        m_pFile->WriteList( xbuffer_view<char>{ &m_Memory[0], static_cast<xuptr>( m_iCurrentOffet ) } );

        //
        // Increment the line count
        // and reset the field count
        // reset the memory count
        //
        m_iLine++;
        m_iField        = 0;
        m_iCurrentOffet = 0;
        m_nTypesLine    = 0;

        return;
    }

    //
    // Increment the line count
    // and reset the field count
    // reset the memory count
    //
    m_iLine++;
    m_iField        = 0;
    m_nTypesLine    = 0;

    //
    // Handle writting text format
    //
    static const s32 MaxLines = 64;

    //
    // We will wait writing the line if we can so we can format
    //
    if( (m_iLine < m_Record.m_Count && (m_iLine%MaxLines) != 0) )
    {
        return;
    }

    //
    // Compute the width for each of the types
    //
    for( i=0; i<m_Field.getCount(); i++ )
    {           
        field&  Field       = m_Field[i];
        s32     TypeWitdh   = 0;

        // Count the total space used for this type
        for( s32 j=0; Field.m_TypeWidth[j]; j++ )
            TypeWitdh += Field.m_TypeWidth[j];

        // We dont count the last type to add spaces
        TypeWitdh -= 1;

        // okay compute the width of the type
        Field.m_Width = x_Max( Field.m_Width, (u32)TypeWitdh );

        //
        // Now lets center all sub columns into the main one 
        //
        s32 ExtraSpace = Field.m_Width - TypeWitdh;

        Field.m_TypesPostWidth  = ExtraSpace / 2;
        Field.m_TypesPreWidth   = Field.m_Width - ( Field.m_TypesPostWidth + TypeWitdh );

        x_assert( Field.m_TypesPreWidth  >= 0 );
        x_assert( Field.m_TypesPostWidth >= 0 );
    }

    //
    // Write the record header
    //
    if( m_iLine <= MaxLines )
    {
        //
        // Write any pending user types
        //
        WriteUserTypes();
        
        //
        // Write header
        //
        if( m_Record.m_bWriteCount )
        {
            m_pFile->Printf( "\n[ %s : %d ]\n", (const char*)m_Record.m_Name, m_Record.m_Count );            
        }
        else
        {
            m_pFile->Printf( "\n[ %s ]\n", (const char*)m_Record.m_Name );            
        }

        //
        // Write the types
        //
        m_pFile->Printf( "{" );
        for( i=0; i<m_Field.getCount(); i++ )
        {               
            m_pFile->Printf( " %s ", (const char*)m_Field[i].m_Type ); 
            const s32 Count = m_Field[i].m_Type.getLength().m_Value;
            if ((u32)Count < m_Field[i].m_Width)
            {
                m_pFile->putC( ' ', m_Field[i].m_Width-Count );
            }
        }
        m_pFile->Printf( "}\n" );

        //
        // Write the under line
        //
        m_pFile->Printf( "//" );
        for( i=0; i<m_Field.getCount(); i++ )
        { 
            m_pFile->putC( '-', m_Field[i].m_Width );

            // Get ready for the next type
            m_pFile->Printf( "  " );
        }
        m_pFile->Printf( "\n" );
    }

    //
    // Write the commented version of the types
    //
    if( (m_iLine%MaxLines) == 0 && (m_iLine>MaxLines) )
    {
        //
        // Write the under line
        //
        m_pFile->Printf( "//" );
        for( i=0; i<m_Field.getCount(); i++ )
        { 
            m_pFile->putC( '-', m_Field[i].m_Width );

            // Get ready for the next type
            m_pFile->Printf( "  " );
        }
        m_pFile->Printf( "\n" );

        //
        // Write the types
        //
        m_pFile->Printf( "//" );
        for( i=0; i<m_Field.getCount(); i++ )
        {   
            m_pFile->Printf( "%s", (const char*)m_Field[i].m_Type );             
            const s32 Count = m_Field[i].m_Type.getLength().m_Value;
            if ((u32)Count < m_Field[i].m_Width)
            {
                m_pFile->putC( ' ', m_Field[i].m_Width - Count );
            }

            // Get ready for the next type
            m_pFile->Printf( "  " );
        }
        m_pFile->Printf( "\n" );

        //
        // Write the under line
        //
        m_pFile->Printf( "//" );
        for( i=0; i<m_Field.getCount(); i++ )
        { 
            m_pFile->putC( '-', m_Field[i].m_Width );

            // Get ready for the next type
            m_pFile->Printf( "  " );
        }
        m_pFile->Printf( "\n" );
    }

    //
    // Dump lines
    //

    // Write each member
    for( i=0; i<m_iCurrentOffet; i++ )
    {
        s32     iField = m_Memory[i++];
        s32     iType  = m_Memory[i++];
        s32     Len    = *(u16*)&m_Memory[i];
        
        i+=2;
        
        field&  Field  = m_Field[ iField ];
        u8      Type   = Field.m_Type[ Field.m_TypeOffset + iType ];

        // Force the type if it is a dynamic type
        if( Field.m_iFieldDecl >= 0 )
        {
            Type   = '<';
        }
        
        //
        // Add any spaces that we may need to center our columns
        //
        if( iType == 0 )
        {
            // acount for the comment
            if( iField == 0 )
            {
                m_pFile->putC( ' ', 2 );
            }

            if( Field.m_TypesPreWidth > 0 ) 
            {
                m_pFile->putC( ' ', Field.m_TypesPreWidth );
            }
        }

        //
        // Okay handle the printing here
        //
        switch( Type )
        {                    
        case 'f':       // Floating point aligment
        case 'F':                    
            {
                // lets find again where the '.' is
                for( j=0; m_Memory[j+i]!='.' && m_Memory[j+i]; j++ );

                // Pad int spaces
                if( Field.m_FracWidth[iType] > j ) 
                {
                    m_pFile->putC( ' ', Field.m_FracWidth[iType] - j );
                }

                // Now we should be able to write the number
                m_pFile->Printf( "%s", &m_Memory[i] );

                // Now lets padd the fractional part
                j = Len + Field.m_FracWidth[iType] - j;
                if( j < Field.m_TypeWidth[iType] )
                {
                    m_pFile->putC( ' ', Field.m_TypeWidth[iType] - j );
                }
            }
            break;
                    
        case 'd':       // Right aligment
        case 'D':
            {
                // Now lets padd the fractional part
                if( Len < Field.m_TypeWidth[iType] )
                {
                    m_pFile->putC( ' ', Field.m_TypeWidth[iType] - Len );
                }

                // Now we should be able to write the number
                m_pFile->Printf( "%s", &m_Memory[i] );
            }
            break;
        case '<':
            {
                if( Field.m_iFieldDecl == -2 )
                {
                    //This is a declaration, retrieve information
                    x_assert(Field.m_iFieldDecl == -2);
                    
                    //Look for '>'
                    s32 iRightBracket = 0;
                    if( m_Memory[i] == '<' )
                    {
                        const char* p = &m_Memory[i];
                        
                        while( p[iRightBracket] != '>')
                        {
                            iRightBracket++;
                        }
                        
                        iRightBracket--;
                    }
                    else if( m_Memory[i] == '.' )
                    {
                        const char* p = &m_Memory[i];
                        s32 Index;
                        
                        x_verify( m_UserTypes.BinarySearch( user_type(p), Index ) );
                        
                        const user_type& Type = m_UserTypes[ Index ];
                        iRightBracket = Type.m_nSystemTypes;
                    }
                    else
                    {
                        x_assert(0);
                    }

                    //For now, I just added this barrier because i don't think there will be a value which contains more than 32 elements. If there is, let me know
                    x_assert(iRightBracket<32);

                    // This is just temporary as it will be overwritten again and again
                    Field.m_DynamicTypeCount = iRightBracket;
                }
                else
                {
                    // This is a temporary variable to store how many types we have
                    Field.m_nTypes = m_Field[ Field.m_iFieldDecl ].m_DynamicTypeCount;
                }
            }
                
                //>>>>>>>>>>>> FALL THOUGHT <<<<<<<<<<<<<<<
                
        default:        // Left aligment
            {
                // Now we should be able to write the number
                m_pFile->Printf( "%s", &m_Memory[i] );

                if( Len < Field.m_TypeWidth[iType] )
                {
                    m_pFile->putC( ' ', Field.m_TypeWidth[iType] - Len );
                }
            }
            break;
        }

        // Set the next i
        i += Len -1;
        x_assert( m_Memory[i] == 0 );

        if( iType == Field.m_nTypes-1 )
        {
            if (iField == m_Field.getCount()-1 ) 
            {
                m_pFile->Printf( "\n" );
            }
            else
            {
                // add any spaces needed to end the column properly
                if( Field.m_TypesPostWidth > 0 ) 
                {
                    m_pFile->putC( ' ', Field.m_TypesPostWidth );
                }

                // ready for the next type
                m_pFile->Printf( "  " );
            }
        }
        else
        {
            // separate any sub fields
            m_pFile->Printf( " " );
        }
    }

    //
    // Make sure to reset the current offset since we are done with the memory
    //
    m_iCurrentOffet = 0;
}

//------------------------------------------------------------------------------

void xtextfile::WriteComment( const xstring& Comment )
{
    x_assert( m_States.m_bReading == false );

    if(m_States.m_bBinary )
    {
        // No comments supported for binary files
        //    m_pFile->Write( Comment(), 1, Comment.GetLength()+1 );
    }
    else
    {
        m_pFile->WriteList( xbuffer_view<const char>{ &Comment[0], static_cast<xuptr>(Comment.getLength().m_Value) } );
    }
}

//------------------------------------------------------------------------------

struct temp_field
{
    xstring                 m_Desc              {};
    xstring                 m_Name              {};
    const char*             m_pType             {nullptr};
    xstring                 m_UserDesc          {};
    xstring                 m_SpecialTypes      {};
    s32                     m_iLink             {-1};
};

void xtextfile::TransformTo( const xwstring& InputFileName, const xwstring& OutputFileName, access_type Flags )
{
    xtextfile                   InTextFile;
    xtextfile                   OutTextFile;
    xvector<temp_field>         Fields;
    xafptr<xbyte>               TempData;
    
    InTextFile.openForReading( InputFileName );
    OutTextFile.openForWriting( OutputFileName, Flags );
    
    TempData.Alloc( 256 * 20 );
    
    //
    // Read all the records
    //
    while( InTextFile.ReadRecord() )
    {
        // Read the first line first
        InTextFile.ReadLine();
        
        // Start thinking
        const s32 nRecords = InTextFile.getRecordCount();
        const s32 nFields  = InTextFile.getNumberFields();
        Fields.DeleteAllEntries();
        
        //
        // Get all the field for this record
        //
        for( s32 i=0; i<nFields; i++ )
        {
            temp_field& Field = Fields.append();
            
            InTextFile.getFieldDesc( i, Field.m_Desc );
            Field.m_pType  = InTextFile.getFieldType( i );
            Field.m_iLink  = -1;
            
            x_assert( x_strlen( Field.m_pType ).m_Value < 20 );
            
            InTextFile.getFieldName( i, Field.m_Name );
        }

        //
        // Link special indirect fields (Example:<?>)
        //
        for( s32 i = 0; i < Fields.getCount(); i++ )
        {
            temp_field& Field = Fields[i];
            if( Field.m_pType[0] != '<' )
                continue;
            
            if( Field.m_pType[1] == '?' )
                continue;

            xchar TempBuffer[256];
            for( s32 i=0; Field.m_pType[1+i] != '>'; i++ )
            {
                TempBuffer[i]   = Field.m_pType[1+i];
                TempBuffer[i+1] = 0;
            }
            
            for( s32 i=0; i<nFields; i++ )
            {
                if( Fields[i].m_Name == TempBuffer )
                {
                    Field.m_iLink = i;
                    break;
                }
            }
        }
        
        //
        // Transfer any user types into the OutFile
        //
        for( s32 i=0; i<InTextFile.m_UserTypes.getCount(); i++ )
        {
            if( i >= OutTextFile.m_UserTypes.getCount() )
            {
                s32 Count = InTextFile.m_UserTypes.getCount<int>() - i;
                
                OutTextFile.m_UserTypes.appendList( Count );
                
                for( ; i<InTextFile.m_UserTypes.getCount(); i++ )
                {
                    OutTextFile.m_UserTypes[i] = InTextFile.m_UserTypes[i];
                }
            }
            else if( OutTextFile.m_UserTypes[i].m_UserType != InTextFile.m_UserTypes[i].m_UserType )
            {
                OutTextFile.m_UserTypes.Insert(i) = InTextFile.m_UserTypes[i];
            }
            else
            {
                // nothing to do... we have this type
            }
        }
        
        //
        // Load all the fields
        //
        
        // First create the record
        OutTextFile.WriteRecord( InTextFile.getRecordName(), nRecords );

        for( s32 i=0; i<nRecords; i++ )
        {
            // Read record from the disk
            // already read record 0
            if( i ) InTextFile.ReadLine();
            
            //
            // Transform all the records to the new file
            //
            for( s32 j = 0; j < Fields.getCount(); j++ )
            {
                const temp_field& Field = Fields[j];
                const char* pReadingFieldDesc  = Field.m_Desc;
                const char* pWrittingFieldDesc = Field.m_Desc;
                const char* pFieldType         = &Field.m_pType[0];
                char Temp[256];
                
                //
                // Read special fields
                //
                if( pFieldType[0] == '<' )
                {
                    if( pFieldType[1] == '?')
                    {
                        InTextFile.ReadField( pReadingFieldDesc, &Field.m_SpecialTypes );
                        OutTextFile.WriteField( pWrittingFieldDesc, (const char*)Field.m_SpecialTypes );
                        continue;
                    }
                    else
                    {
                        // Read indirect fields
                        pFieldType = &Fields[ Field.m_iLink ].m_SpecialTypes[0];
                        
                        x_sprintf( { Temp, 256 }, "%s:%s", (const char*)Field.m_Name, pFieldType);
                        pReadingFieldDesc = Temp;
                    }
                }
                
                //
                // Read normal fields
                //
                InTextFile.ReadField( pReadingFieldDesc,
                            &TempData[256*0],
                            &TempData[256*1],
                            &TempData[256*2],
                            &TempData[256*3],
                            &TempData[256*4],
                            &TempData[256*5],
                            &TempData[256*6],
                            &TempData[256*7],
                            &TempData[256*8],
                            &TempData[256*9],
                            &TempData[256*10],
                            &TempData[256*11],
                            &TempData[256*12],
                            &TempData[256*13],
                            &TempData[256*14],
                            &TempData[256*15],
                            &TempData[256*16],
                            &TempData[256*17],
                            &TempData[256*18],
                            &TempData[256*19] );

                OutTextFile.WriteFieldIndirect( pWrittingFieldDesc,
                                     &TempData[256*0],
                                     &TempData[256*1],
                                     &TempData[256*2],
                                     &TempData[256*3],
                                     &TempData[256*4],
                                     &TempData[256*5],
                                     &TempData[256*6],
                                     &TempData[256*7],
                                     &TempData[256*8],
                                     &TempData[256*9],
                                     &TempData[256*10],
                                     &TempData[256*11],
                                     &TempData[256*12],
                                     &TempData[256*13],
                                     &TempData[256*14],
                                     &TempData[256*15],
                                     &TempData[256*16],
                                     &TempData[256*17],
                                     &TempData[256*18],
                                     &TempData[256*19] );
                
            }
            
            // Write records to disk
            OutTextFile.WriteLine();
        }
    }

    //
    // Be nice and close the files
    //
    InTextFile.close();
    OutTextFile.close();
}

