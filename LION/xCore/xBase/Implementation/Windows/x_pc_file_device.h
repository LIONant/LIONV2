//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// INCLUDES
//==============================================================================
#include "x_base.h"

class pc_device final : public xfile_device_i
{
public:

    pc_device ( void ) : xfile_device_i( X_STR("a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:q:r:s:t:u:v:w:x:y:z:") ){}

protected:

    struct alignas(x_atomic<void*>) file
    {
        HANDLE          m_Handle        {};
        OVERLAPPED      m_Overlapped    {};
        access_types    m_AccessTypes   {};
    };
    
protected:
    
    virtual void*               Open        ( const xwstring& FileName, access_types AccessTypes )  noexcept override;
    virtual void                Close       ( void* pFile )                                         noexcept override;
    virtual bool                Read        ( void* pFile, void* pBuffer, u64 Count )               noexcept override;
    virtual void                Write       ( void* pFile, const void* pBuffer, u64 Count )         noexcept override;
    virtual void                Seek        ( void* pFile, seek_mode Mode, s64 Pos )                noexcept override;
    virtual s64                 Tell        ( void* pFile )                                         noexcept override;
    virtual void                Flush       ( void* pFile )                                         noexcept override;
    virtual u64                 Length      ( void* pFile )                                         noexcept override;
    virtual bool                isEOF       ( void* pFile )                                         noexcept override;
    virtual sync_state          Synchronize ( void* pFile, bool bBlock )                            noexcept override;
    virtual void                AsyncAbort  ( void* pFile )                                         noexcept override;
    virtual const xwchar*       getTempPath ( void )                                        const   noexcept override;
    
protected:

    x_ll_fixed_pool_static_jitc<file, 128>   m_qtlFileHPool;
};

