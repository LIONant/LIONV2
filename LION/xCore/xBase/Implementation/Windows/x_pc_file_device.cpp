//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"
#include <Windows.h>
#include "Implementation/Windows/x_pc_file_device.h"


// Windows defines max messing up the rest of the universe
#ifdef max
    #undef max
#endif


//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

static struct temp_path
{
    temp_path( void )
    {
        GetTempPath( 512, m_Path );
    }

    xwchar m_Path[ 512 ];

} s_TempPath;

//------------------------------------------------------------------------------

const xwchar* pc_device::getTempPath ( void ) const noexcept
{
    return &s_TempPath.m_Path[0];    
}

//------------------------------------------------------------------------------

void* pc_device::Open( const xwstring& paFileName, access_types AccessTypes ) noexcept
{
    u32 FileMode    = 0;
    u32 Disposition = 0;
    u32 AttrFlags   = 0;
    u32 ShareType   = FILE_SHARE_READ;
    
    x_assert( paFileName.isValid() );
    
    xwstring FileName( units_wchars{256} );
    if ( x_strncmp<xwchar>( paFileName, L"temp:", 5 ) == 0 )
    {
        FileName.Copy( s_TempPath.m_Path );
        x_assert( paFileName[ 5 ] == '/' );
        FileName.append( &paFileName[ 5 ] );
    }
    else
    {
        FileName = paFileName;
    }

	FileMode    = GENERIC_WRITE|GENERIC_READ;
    if( AccessTypes.m_CREATE )
    {
        Disposition = CREATE_ALWAYS;
    }
    else
    {
        if( AccessTypes.m_WRITE == false )
        {
            FileMode   &= ~GENERIC_WRITE;
        }
        
        Disposition = OPEN_EXISTING;
    }
    
    if( AccessTypes.m_ASYNC )
    {
        // FILE_FLAG_OVERLAPPED     -	This allows asynchronous I/O.
        // FILE_FLAG_NO_BUFFERING   -	No cached asynchronous I/O.
        AttrFlags = FILE_FLAG_OVERLAPPED;// | FILE_FLAG_NO_BUFFERING;
    }
    else
    {
        AttrFlags = FILE_ATTRIBUTE_NORMAL;
    }
    
    // open the file (or create a new one)
    HANDLE Handle = CreateFile( FileName, FileMode, ShareType, nullptr, Disposition, AttrFlags, nullptr );
    if( Handle == INVALID_HANDLE_VALUE ) 
    {
        return nullptr;
    } 
    
    //
    // Okay we are in business
    //
    file& File = *m_qtlFileHPool.pop();

    File.m_Handle       = Handle;
    File.m_AccessTypes  = AccessTypes;
    x_memset( &File.m_Overlapped, 0, sizeof(File.m_Overlapped) );
    
	if( AccessTypes.m_ASYNC ) 
    {
        // Create an event to detect when an asynchronous operation is done.
        // Please see documentation for further information.
        //HANDLE const Event = CreateEvent( NULL, TRUE, FALSE, NULL );
        //ASSERT(Event != NULL);
        //File.m_Overlapped.hEvent = Event;
    }
    
    // done
    return &File;
}

//------------------------------------------------------------------------------
void pc_device::AsyncAbort( void* pFile ) noexcept
{
    file&   File = *(file*)pFile;

    x_verify( CancelIo( File.m_Handle ) );
}

//------------------------------------------------------------------------------

xfile::sync_state pc_device::Synchronize( void* pFile, bool bBlock ) noexcept
{
    file&   File = *(file*)pFile;

    if( HasOverlappedIoCompleted( &File.m_Overlapped ) )
        return xfile::sync_state::COMPLETED;
    
    // Get the current status.
    DWORD nBytesTransfer = 0;
    BOOL  Result         = GetOverlappedResult( File.m_Handle, &File.m_Overlapped, &nBytesTransfer, bBlock );
    
    // Has the asynchronous operation finished?
    if( Result == TRUE ) 
    {
        // Clear the event's signal since it isn't automatically reset.
        // VERIFY( ResetEvent( File.m_Overlapped.hEvent ) == TRUE );
        
        // The operation is complete.
        return xfile::sync_state::COMPLETED;
    } 
    
    //
    // Deal with errors
    //
    DWORD dwError = GetLastError();
    
    if( dwError == ERROR_HANDLE_EOF )
    {
        // we have reached the end of
        // the file during asynchronous
        // operation
        return xfile::sync_state::XEOF;
    } 
    
    if( dwError == ERROR_IO_INCOMPLETE) 
    {
        return xfile::sync_state::INCOMPLETE;
    } 
    
    if( dwError == ERROR_OPERATION_ABORTED )
    {
        return xfile::sync_state::UNKNOWN_ERR;
    }
    
    // The result is FALSE and the error isn't ERROR_IO_INCOMPLETE, there's a real error!
    return xfile::sync_state::UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void pc_device::Close( void* pFile ) noexcept
{
    file&   File = *(file*)pFile;

    //
    // Close the handle
    //
    if( !CloseHandle( File.m_Handle ) )
    {
        x_constexprvar int BufferSize = 256;
        WCHAR buffer[BufferSize];
        FormatMessage( 
                        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                        NULL,
                        GetLastError(),
                        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                        buffer,
                        BufferSize,
                        NULL );
        MessageBox( NULL, buffer, L"GetLastError", MB_OK|MB_ICONINFORMATION );
        x_assert( false );
    }
    
    //
    // Lets free our entry
    //
    m_qtlFileHPool.push( File );
}

//------------------------------------------------------------------------------

bool pc_device::Read( void* pFile, void* pBuffer, u64 Count ) noexcept
{
    file&   File = *(file* )pFile;

    x_assert( Count < std::numeric_limits<DWORD>::max() );

    DWORD nBytesRead;
    BOOL  bResult = ReadFile( File.m_Handle, pBuffer, static_cast<DWORD>(Count), &nBytesRead, &File.m_Overlapped ); 
    
    // Set the file pointer (We assume we didnt make any errors)
    //if( File.m_Flags&xfile_device_i::ACC_ASYNC) 
    {
        File.m_Overlapped.Offset += static_cast<DWORD>(Count);
    }
    
    if (!bResult) 
    { 
        DWORD dwError = GetLastError();
        
        // deal with the error code 
        switch(dwError ) 
        { 
            case ERROR_HANDLE_EOF: 
            { 
                // we have reached the end of the file 
                // during the call to ReadFile 
                
                // code to handle that 
                return false;
            } 
                
            case ERROR_IO_PENDING: 
            { 
                return true;                    
            }
                
            default:
            {
                x_constexprvar int BufferSize = 256;
                WCHAR buffer[BufferSize];
                FormatMessage( 
                                FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                                NULL,
                                GetLastError(),
                                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                                buffer,
                                BufferSize,
                                NULL );
                MessageBox( NULL, buffer, L"GetLastError", MB_OK|MB_ICONINFORMATION );
                x_assert( false );
                break;
            }
        }
    }
    
    // Not problems
    return true;
}

//------------------------------------------------------------------------------

void pc_device::Write( void* pFile, const void* pBuffer, u64 Count ) noexcept
{
    file& File = *(file*)pFile;
    DWORD nBytesWritten;

    x_assert( Count < std::numeric_limits<DWORD>::max() );

    BOOL  bResult = WriteFile( File.m_Handle, pBuffer, static_cast<DWORD>(Count), &nBytesWritten, &File.m_Overlapped ); 
    
    // Set the file pointer (We assume we didnt make any errors)
    //if( File.m_Flags&xfile_device_i::ACC_ASYNC) 
    {
        File.m_Overlapped.Offset += static_cast<DWORD>(Count);
    }
    
    if (!bResult) 
    { 
        DWORD dwError = GetLastError();
        
        // deal with the error code 
        switch(dwError ) 
        { 
            case ERROR_HANDLE_EOF: 
            { 
                // we have reached the end of the file 
                // during the call to ReadFile 
                
                // code to handle that 
                x_assert(0);
                return;
            } 
                
            case ERROR_IO_PENDING: 
            { 
                return;                    
            }
                
            default:
            {
                x_constexprvar int BufferSize = 256;
                WCHAR buffer[BufferSize];
                FormatMessage( 
                                FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                                NULL,
                                GetLastError(),
                                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                                buffer,
                                BufferSize,
                                NULL );
                MessageBox( NULL, buffer, L"GetLastError", MB_OK|MB_ICONINFORMATION );
                x_assert( false );
                break;
            }
        }
    }
}

//------------------------------------------------------------------------------

void pc_device::Seek( void* pFile, seek_mode aMode, s64 Pos ) noexcept
{
    file&   File = *(file*)pFile;

    s32 HardwareMode = FILE_BEGIN;
    switch( aMode )
    {
        case SKM_ORIGIN: break;
        case SKM_CURENT: HardwareMode = FILE_CURRENT;   break;
        case SKM_END:    HardwareMode = FILE_END;       break; 
        default: x_assert(0);                           break;
    }
    
    // We will make sure we are sync here
    // WARNING: Potencial time wasted here
    if( File.m_AccessTypes.m_ASYNC )
    {
        x_verify( xfile::sync_state::COMPLETED == Synchronize( pFile, true ) );
    }
    
    // Seek!
    LARGE_INTEGER Position;
    LARGE_INTEGER NewFilePointer;
    
    Position.QuadPart = Pos;
    DWORD Result = SetFilePointerEx( File.m_Handle, Position, &NewFilePointer, HardwareMode );
    if( !Result )
    {
        if( Result == INVALID_SET_FILE_POINTER ) 
        {
           // Failed to seek.
            x_assert( 0 );
        }
        
        x_assert( 0 );
    }
    
    // Set the position for async files
    File.m_Overlapped.Offset     = NewFilePointer.LowPart;
    File.m_Overlapped.OffsetHigh = NewFilePointer.HighPart;
}

//------------------------------------------------------------------------------

s64 pc_device::Tell( void* pFile ) noexcept
{
    file&   File = *(file*)pFile;

    LARGE_INTEGER Position;
    LARGE_INTEGER NewFilePointer;
    Position.LowPart  = 0;
    Position.HighPart = 0;
    
    if( FALSE == SetFilePointerEx( File.m_Handle, Position, &NewFilePointer, FILE_CURRENT ) )
    {
        x_constexprvar int BufferSize = 256;
        WCHAR buffer[BufferSize];
        FormatMessage( 
                        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                        NULL,
                        GetLastError(),
                        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                        buffer,
                        BufferSize,
                        NULL );
        MessageBox( NULL, buffer, L"GetLastError", MB_OK|MB_ICONINFORMATION );
        x_assert( false );
    }
    
    return NewFilePointer.QuadPart;
}

//------------------------------------------------------------------------------

void pc_device::Flush( void* pFile )  noexcept
{
    // We will make sure we are sync here. 
    // I dont know what else to do there is not a way to flush anything the in the API
    // WARNING: Potencial time wasted here
    x_verify( xfile::sync_state::COMPLETED == Synchronize( pFile, true ) );
}

//------------------------------------------------------------------------------

u64 pc_device::Length( void* pFile ) noexcept
{
    auto Cursor = Tell( pFile ); Seek( pFile, SKM_END,    0 );
    auto Length = Tell( pFile ); Seek( pFile, SKM_ORIGIN, Cursor );
    
    return Length;
}

//------------------------------------------------------------------------------

bool pc_device::isEOF( void* pFile ) noexcept
{
    file&   File = *(file* )pFile;

    DWORD nBytesTransfer;
    BOOL  bResult  = GetOverlappedResult( File.m_Handle, &File.m_Overlapped, &nBytesTransfer, false );
    
    if (!bResult) 
    { 
        DWORD dwError = GetLastError();
        
        // deal with the error code 
        switch(dwError ) 
        { 
            case ERROR_HANDLE_EOF: 
            { 
                // we have reached the end of the file 
                // during the call to ReadFile 
                
                // code to handle that 
                return true;
            } 
                
            case ERROR_IO_PENDING: 
            { 
                if( Synchronize( pFile, true ) >= xfile::sync_state::COMPLETED )
                    return false;
                
                return true;                    
            }
                
            default:
                x_assert( 0 );
                break;
        }
    }
    
    // Not sure about this yet
    return false;
}
