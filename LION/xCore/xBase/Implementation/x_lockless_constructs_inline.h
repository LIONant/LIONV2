//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// x_ll_tagptr
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

template< class T > inline
x_ll_tagptr<T>::x_ll_tagptr( void ) noexcept 
{
    x_assume( x_Align( this, 8 ) == this );
    static_assert( sizeof( x_ll_tagptr ) == sizeof( u64 ), "" );
}

//-------------------------------------------------------------------------------
template< class T > inline
void x_ll_tagptr<T>::setup( const T* pPtr ) noexcept 
{
#if _X_TARGET_64BITS
    static_assert( sizeof(void*) == 8 , "" );
    local Temp   = *reinterpret_cast<local*>(&m_qtPointer);
    u16   Count  = Temp.m_Tag + 1;
    Temp.m_Data  = reinterpret_cast<xuptr>(pPtr);
    Temp.m_Tag   = Count;
    m_qtPointer.store(Temp, std::memory_order_relaxed );
    x_assume( getLocal().getPtr() == pPtr );
#else
    static_assert( sizeof(void*) == 4 , "" );
    local Temp   = *reinterpret_cast<local*>(&m_qtPointer);
    u32   Count  = Temp.m_Tag + 1;
    Temp.m_Data  = reinterpret_cast<xuptr>(pPtr);
    Temp.m_Tag   = Count;
    m_qtPointer.store(Temp, std::memory_order_relaxed );
    x_assume( getLocal().getPtr() == pPtr );
#endif
}

//-------------------------------------------------------------------------------
template< class T > inline
bool x_ll_tagptr<T>::CompareAndSet( local& LocalPointer, const T* const pNewPtr,  std::memory_order MemoryOrder ) noexcept 
{
    x_assume( x_Align( this, 8 ) == this );

    local	New;

    New.m_Data = reinterpret_cast<xuptr>(pNewPtr);
    New.m_Tag  = LocalPointer.m_Tag + 1;

    return m_qtPointer.compare_exchange_weak( LocalPointer, New, MemoryOrder );
}

//-------------------------------------------------------------------------------
template< class T > inline
bool x_ll_tagptr<T>::CompareAndSet( local& LocalPointer, const local NextPointer,  std::memory_order MemoryOrder ) noexcept
{
    local	New;
    New.m_Data = NextPointer.m_Data;
    New.m_Tag  = LocalPointer.m_Tag + 1;

    return m_qtPointer.compare_exchange_weak( LocalPointer, New, MemoryOrder );
}

//-------------------------------------------------------------------------------
template< class T > inline
bool x_ll_tagptr<T>::CompareAndSet( local& LocalPointer, const T* const pNewPtr,  std::memory_order Success, std::memory_order Failure ) noexcept 
{
    x_assume( x_Align( this, 8 ) == this );

    local	New;

    New.m_Data = reinterpret_cast<xuptr>(pNewPtr);
    New.m_Tag  = LocalPointer.m_Tag + 1;

    return m_qtPointer.compare_exchange_weak( LocalPointer, New, Success, Failure );
}

//-------------------------------------------------------------------------------
template< class T > inline
bool x_ll_tagptr<T>::CompareAndSet( local& LocalPointer, const local NextPointer,  std::memory_order Success, std::memory_order Failure ) noexcept
{
    local	New;
    New.m_Data = NextPointer.m_Data;
    New.m_Tag  = LocalPointer.m_Tag + 1;

    return m_qtPointer.compare_exchange_weak( LocalPointer, New, Success, Failure );
}

//-------------------------------------------------------------------------------
template< class T > inline
void x_ll_tagptr<T>::LinkListSetNext( t_self& Node, std::memory_order MemoryOrder ) noexcept 
{
    local  Local = m_qtPointer.load();
    do
    {
        local   New = Local;
        Node.setup( Local.getPtr() );

#if _X_TARGET_64BITS
        static_assert( sizeof(void*) == 8 , "" );
        New.m_Ptr = reinterpret_cast<xuptr>(&Node);
        New.m_Tag++;
#else
        static_assert( sizeof(void*) == 4 , "" );
        New.m_pPtr = &Node;
        New.m_Tag++;
#endif

        if( m_qtPointer.compare_exchange_weak( Local, New, MemoryOrder ) )
            break;

    } while( 1 );
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// x_ll_circular_pool
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

 //-------------------------------------------------------------------------------
inline
void x_ll_circular_pool::Init( xuptr ByteCount ) noexcept       
{ 
    m_pPtr.setup( reinterpret_cast<xbyte*>( g_context::get().aligned_alloc( ByteCount, 8 ) ), ByteCount );    

#if _X_DEBUG
    m_Debug_PtrList.New( ByteCount / 8 );
#endif 
}

//-------------------------------------------------------------------------------

inline
void x_ll_circular_pool::Free( void* pVoidData ) noexcept
{
#if _X_DEBUG
    m_Stats_AllocVsFree--;
    x_assume( pVoidData );
    for( xuptr i = m_Debug_iTail.load(); true; i = (i+1)%m_Debug_PtrList.getCount() )
    {
        //
        // Did we find our entry?
        //
        if( m_Debug_PtrList[i].m_Ptr.load() == pVoidData )
        {
            m_Stats_CurrUsed -= m_Debug_PtrList[i].m_Size;
            m_Debug_PtrList[i].m_Size = 0;
            m_Debug_PtrList[i].m_Ptr.store( nullptr );

            // Update the max used
            {
                {
                    xuptr   Total;
                    xuptr   Max   = m_Stats_MaxUsed.load();
                    do
                    {
                        Total = m_Stats_CurrUsed;
                        if( Total <= Max ) break;
                    } while( !m_Stats_MaxUsed.compare_exchange_weak( Max, Total ) );
                }

                // Out of memory!
                x_assert( m_Stats_MaxUsed.load() < (m_pPtr.getCount() - ( m_pPtr.getCount() / 10))  );
            }

            // If we are the last entry then try to move the tail forward
            if( i == m_Debug_iTail.load() && i != m_Debug_iHead.load() ) 
            {                
                // Move the tail as forward as we can
                if( m_Debug_iTail.compare_exchange_weak( i, (i+1)%m_Debug_PtrList.getCount() ) )
                {
                    i = (i+1)%m_Debug_PtrList.getCount();

                    while( i != m_Debug_iHead.load() )
                    {
                        if( m_Debug_PtrList[i].m_Ptr.load() == nullptr && m_Debug_PtrList[i].m_Size == 0 )
                        {
                            const xuptr newval  = ((i+1) == m_Debug_PtrList.getCount()) ? 0 : i+1;
                            if( m_Debug_iTail.compare_exchange_weak( i, newval ) ) i = newval;
                            else break;
                        }
                        else break;
                    }
                }
            }

            // Done with the free
           return;
        }
        else
        {
            //
            // check to see if we are done or if we need to advance the head
            //
            {
                xuptr iHead = m_Debug_iHead.load();
                if( i == iHead )
                {
                    do
                    {
                        void* pHead = m_Debug_PtrList[iHead].m_Ptr.load(); 
                        if( pHead == nullptr ) 
                        {
                            // The tail was able to catch up with the head yet we did not find our entry?
                            // that is not possible
                            x_assert( false );
                            break;
                        }

                        xuptr NewHead = ( iHead + 1) % m_Debug_PtrList.getCount();
                        if( m_Debug_iHead.compare_exchange_weak( iHead, NewHead ) ) 
                        {
                            x_assume( iHead != m_Debug_iTail.load() );
                            break;
                        }
                    } while(1);
                }
            }
        }
    }

    x_assume( false );
  
#endif
}

//-------------------------------------------------------------------------------
inline
xbyte* x_ll_circular_pool::ByteAlloc( xuptr ByteCount, u32 Aligment ) noexcept 
{
    const xuptr MaxMemory = m_pPtr.getCount();
    
    if( Aligment < sizeof(u32) ) Aligment = sizeof(u32);
    
    xuptr LocalHead  = m_Head;
    do
    {
        X_CMD_DEBUG( bool  IncNewLoop = false );
        xuptr AllocIndex = static_cast<xuptr>( x_Align( &m_pPtr[0] + LocalHead, Aligment ) - &m_pPtr[0] );
        xuptr NewHead    = AllocIndex + ByteCount;
        
        if( NewHead >= MaxMemory )
        {
            AllocIndex = static_cast<xuptr>( x_Align( &m_pPtr[0], Aligment ) - &m_pPtr[0] );
            NewHead    = AllocIndex + ByteCount;
            X_CMD_DEBUG( IncNewLoop = true );
        }
        
        if( m_Head.compare_exchange_weak( LocalHead, NewHead ) )
        {
#if _X_DEBUG
            m_Stats_AllocVsFree++;
            //
            // Create a new node for our allocation
            //
            {
                xuptr iHead = m_Debug_iHead.load();
                do
                {
                    void* pPtr = m_Debug_PtrList[ iHead ].m_Ptr.load();
                    if( pPtr )
                    {
                        const xuptr newval  = (iHead+1)%m_Debug_PtrList.getCount();
                        x_assume( newval != m_Debug_iTail.load() );

                        m_Debug_iHead.compare_exchange_weak( iHead, newval );
                    }
                    else if( m_Debug_PtrList[ iHead ].m_Ptr.compare_exchange_weak( pPtr, &m_pPtr[AllocIndex] ) )
                    {
                        // Set the count
                        x_assert( m_Debug_PtrList[ iHead ].m_Size == 0 );
                        m_Debug_PtrList[ iHead ].m_Size = ByteCount;
                        m_Stats_CurrUsed += ByteCount;

                        const xuptr newval  = (iHead+1)%m_Debug_PtrList.getCount();
                        // if == then run out of entries in the debug array 
                        // or the tail somehow went pass the head
                        // or our iHead is too old
                        if( newval == m_Debug_iTail.load() )
                        {
                            x_assert( false == m_Debug_iHead.compare_exchange_weak( iHead, newval ) );
                        }
                        else
                        {
                            m_Debug_iHead.compare_exchange_weak( iHead, newval );
                        }
                        break;
                    }

                } while(true);
            }

            //
            // Move the tail as forward as we can
            //
            {
                xuptr i = m_Debug_iTail.load();
                while( i != m_Debug_iHead.load() )
                {
                    if( m_Debug_PtrList[i].m_Ptr.load() == nullptr && m_Debug_PtrList[i].m_Size == 0 )
                    {
                        const xuptr newval  = (i+1) % m_Debug_PtrList.getCount();
                        if( m_Debug_iTail.compare_exchange_weak( i, newval ) ) i = newval;
                    }
                    else break;
                }
            }

            //
            // Update the max used
            //
            {
                {
                    xuptr Total;
                    xuptr Max     = m_Stats_MaxUsed.load();
                    do
                    {
                        Total   = m_Stats_CurrUsed.load();
                        if( Total <= Max ) break;
                    } while( !m_Stats_MaxUsed.compare_exchange_weak( Max, Total ) );
                }

                // Out of memory!
                x_assert( m_Stats_MaxUsed.load() < (m_pPtr.getCount() - ( m_pPtr.getCount() / 10))  );
            }

            
#endif            
            return &m_pPtr[AllocIndex];
        }
        
    } while( 1 );
    
    return nullptr;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// x_ll_mrmw_hash
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::Initialize( xuptr MaxEntries ) noexcept
{
    m_EntryMemPool.Init( MaxEntries );
    m_qtHashTable.New( MaxEntries * 10 );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::DeleteAllEntries( void ) noexcept
{
    //
    // Clean all used entries
    //
    x_job_block Block( X_WSTR("x_ll_mrmw_hash::DeleteALlEntires") );
    Block.ForeachLog( m_qtHashTable, 8, 200, [&]( xbuffer_view<hash_table_entry> List )
    {
        for( auto& Entry : List )
        {
            hash_entry* pNext = Entry.m_Head.load( std::memory_order_relaxed );
            
            if( pNext )
            {
                do 
                {
                    hash_entry* pRealNext = pNext->m_Next.load( std::memory_order_relaxed );
                    m_EntryMemPool.push( *pNext );
                    pNext = pRealNext;

                } while( pNext );

                Entry.m_Head.store( nullptr, std::memory_order_relaxed );
            }
        }
    });
    Block.Join();
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
KEY x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::getKeyFromEntry( const t_entry& Entry ) const noexcept 
{
    return GetHashEntryFromEntry(Entry).m_Key;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
bool x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::isEntryInHash( t_key Key ) const noexcept 
{
    const s32   Index       = KeyToIndex( Key );
    const auto& TableEntry  = m_qtHashTable[ Index ];

    TableEntry.m_Semaphore.lock();
    const bool bAnswer = (NULL != FindEntry( TableEntry, Key ));
    TableEntry.m_Semaphore.unlock();

    return bAnswer;
}

//-------------------------------------------------------------------------------
// Deleting locks one table entry in the hash table
// other wise this hold hash table is lockfree 
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpDelEntry( t_key Key, t_function Function ) noexcept
{                                                                          
    hash_entry* pEntry = nullptr;

    //
    // First lets remove the entry from the hash table
    // 
    {
        const xuptr             Index           = KeyToIndex( Key );
        hash_table_entry&       HashTaleEntry   = m_qtHashTable[ Index ];
        hash_entry*             pPrevEntry      = nullptr;

        HashTaleEntry.m_Semaphore.lock();
        pEntry      = HashTaleEntry.m_Head.load();
        while( pEntry )
        {
            if( pEntry->m_Key == Key )
                break;

            pPrevEntry  = pEntry;
            pEntry      = pEntry->m_Next.load();
        }

        // Could not find the entry
        x_assume( pEntry );
               
        if( pPrevEntry ) 
        {
            pPrevEntry->m_Next.store( pEntry->m_Next.load() );
        }
        else
        {
            x_assume( HashTaleEntry.m_Head.load() == pEntry );
            HashTaleEntry.m_Head.store( pEntry->m_Next.load() );
        }

        HashTaleEntry.m_Semaphore.unlock(); 
    }

    //
    // let the user know that we are about to nuke it
    //
    Function( pEntry->m_UserData );
                       
    //
    // Destroy and putback in the empty pool
    //
    m_EntryMemPool.push( *pEntry );
}
 
//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
typename x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::hash_entry& x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::AddHashEntry( hash_table_entry& TableEntry, t_key Key ) noexcept 
{
    hash_entry* pHashEntry = m_EntryMemPool.pop();
    x_assume( pHashEntry );
    x_assume( pHashEntry->m_Next.load() == nullptr );

    pHashEntry->m_Key = Key;

    // Add it to the head of the link list
    auto* pHead = TableEntry.m_Head.load();
    do
    {
        // Check the list again to make sure that still not there
        x_assert( nullptr == FindEntry( TableEntry, Key ) );

        pHashEntry->m_Next.store( pHead );
    } while( !TableEntry.m_Head.compare_exchange_weak( pHead, pHashEntry ) );

    return *pHashEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
typename x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::hash_entry* x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::FindEntry( const hash_table_entry& TableEntry, t_key Key ) const noexcept 
{
    hash_entry* pEntry = TableEntry.m_Head.load();
    while( pEntry )
    {
        if( pEntry->m_Key == Key )
            return pEntry;

        pEntry = pEntry->m_Next.load();
    }

    return pEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
xuptr x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::KeyToIndex( t_key Key ) const noexcept 
{
    return xuptr( Key % m_qtHashTable.getCount() );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
typename x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::hash_entry& x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::FindOrAddHashEntry( hash_table_entry& TableEntry, t_key Key ) noexcept
{
    //
    // Try to find the entry if we are able to find it the just return
    //
    hash_entry* pEntry = FindEntry( TableEntry, Key );
    if( pEntry ) return *pEntry;

    //
    // Most likely we are going to have to create one so lets continue assuming this is the case
    //
    hash_entry* pHashEntry = m_EntryMemPool.pop();
    x_assume( pHashEntry );
    x_assume( pHashEntry->m_Next.load() == nullptr );

    pHashEntry->m_Key = Key;

    // Add it to the head of the link list
    auto* pHead = TableEntry.m_Head.load();
    pHashEntry->m_Next.store( pHead );
    while( !TableEntry.m_Head.compare_exchange_weak( pHead, pHashEntry ) )
    {
        // 
        // We failed so we must try again to make sure still not there
        //
        pEntry = FindEntry( TableEntry, Key );
        if( pEntry )
        {
            // if is there then release the new allocated entry
            // and return the entry
            m_EntryMemPool.push( *pHashEntry );
            return *pEntry;
        }

        pHashEntry->m_Next.store( pHead );
    } 

    return *pHashEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
ENTRY& x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::FindOrAddPopEntry( t_key Key ) noexcept
{
    const xuptr  Index              = KeyToIndex( Key );
    auto&        MutableTableEntry  = m_qtHashTable[ Index ];
    const auto&  ConstTableEntry    = MutableTableEntry;

    ConstTableEntry.m_Semaphore.lock();
    hash_entry&  HashEntry   = FindOrAddHashEntry( MutableTableEntry, Key );

    HashEntry.m_Semaphore.lock();
    return HashEntry.m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
ENTRY& x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::AddPopEntry( t_key Key ) noexcept
{
    const xuptr Index               = KeyToIndex( Key );
    auto&        MutableTableEntry  = m_qtHashTable[ Index ];
    const auto&  ConstTableEntry    = MutableTableEntry;

    ConstTableEntry.m_Semaphore.lock();
    hash_entry&  HashEntry          = AddHashEntry( MutableTableEntry, Key );

    HashEntry.m_Semaphore.lock();
    return HashEntry.m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
ENTRY* x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::getPopEntry( t_key Key ) noexcept 
{
    const xuptr  Index              = KeyToIndex( Key );
    auto&        MutableTableEntry  = m_qtHashTable[ Index ];
    const auto&  ConstTableEntry    = MutableTableEntry;

    ConstTableEntry.m_Semaphore.lock();
    hash_entry* pHashEntry = FindEntry( MutableTableEntry, Key );
    if( pHashEntry == nullptr ) return nullptr;

    pHashEntry->m_Semaphore.lock();
    return &pHashEntry->m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
const ENTRY* x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::getPopEntry( t_key Key ) const noexcept 
{
    const xuptr Index              = KeyToIndex( Key );
    const auto&  ConstTableEntry    = m_qtHashTable[ Index ];

    ConstTableEntry.m_Semaphore.lock();
    const hash_entry* pHashEntry = FindEntry( ConstTableEntry, Key );
    if( pHashEntry == nullptr ) 
    {
        ConstTableEntry.m_Semaphore.unlock();
        return nullptr;
    }

    pHashEntry->m_Semaphore.lock();
    return &pHashEntry->m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::PushEntry( t_entry& Entry ) noexcept
{
    hash_entry&     HashEntry        = getHashEntryFromEntry( Entry );
    const xuptr     Index            = KeyToIndex( HashEntry.m_Key );
    const auto&     ConstTableEntry  = m_qtHashTable[ Index ];

    HashEntry.m_Semaphore.unlock();
    ConstTableEntry.m_Semaphore.unlock();
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::PushEntry( const t_entry& Entry ) const noexcept
{
    const hash_entry&   HashEntry        = getHashEntryFromEntry( Entry );
    const xuptr         Index            = KeyToIndex( HashEntry.m_Key );
    const auto&         ConstTableEntry  = m_qtHashTable[ Index ];

    HashEntry.m_Semaphore.unlock();
    ConstTableEntry.m_Semaphore.unlock();
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
const typename x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::hash_entry& x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::getHashEntryFromEntry( const t_entry& Entry ) const noexcept 
{
    const s32           nBytes    = offsetof( hash_entry, m_UserData );
    const xbyte*        pBytes    = reinterpret_cast<const xbyte*>(&Entry) - nBytes;
    const hash_entry&   HashEntry = *reinterpret_cast<const hash_entry*>(pBytes);
    
    return HashEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
typename x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::hash_entry& x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::getHashEntryFromEntry( t_entry& Entry ) noexcept
{
    const s32           nBytes    = offsetof( hash_entry, m_UserData );
    xbyte*              pBytes    = reinterpret_cast<xbyte*>(&Entry) - nBytes;
    hash_entry&         HashEntry = *reinterpret_cast<hash_entry*>(pBytes);
    
    return HashEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpFindOrAddEntry( t_key Key, t_function Function ) noexcept 
{
    t_entry& Entry = FindOrAddPopEntry( Key );
    Function( Entry );
    PushEntry( Entry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpAddEntry( t_key Key, t_function Function ) noexcept 
{
    t_entry& Entry = AddPopEntry( Key );
    Function( Entry );
    PushEntry( Entry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpGetEntry( t_key Key, t_function Function ) noexcept
{
    t_entry* pEntry = getPopEntry( Key );
    x_assume( pEntry );
    Function( *pEntry );
    PushEntry(*pEntry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpGetEntry( t_key Key, t_const_function Function ) const noexcept 
{
    const t_entry* pEntry = getPopEntry( Key );
    x_assume( pEntry );
    Function( *pEntry );
    PushEntry(*pEntry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
bool x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpGetOrFailEntry( t_key Key, t_function Function ) noexcept 
{
    t_entry* pEntry = getPopEntry( Key );
    if( nullptr == pEntry ) return false;
    Function( *pEntry );
    PushEntry(*pEntry );
    return true;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
bool x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpGetOrFailEntry( t_key Key, t_const_function Function ) const noexcept 
{
    const t_entry* pEntry = getPopEntry( Key );
    if( nullptr == pEntry ) return false;
    Function( *pEntry );
    PushEntry(*pEntry );
    return true;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpIterateGetEntry( t_function Function ) noexcept 
{
    for( s32 i=0; i<m_qtHashTable.getCount(); i++ )
    {
        const hash_table_entry& TableEntry = m_qtHashTable[i];
        if( TableEntry.m_LinkList.m_Head.GetPtr() )
        {
            TableEntry.m_Semaphore.lock();
            for( hash_entry* pHashEntry = (hash_entry*)TableEntry.m_LinkList.m_Head.GetPtr();
                 pHashEntry; pHashEntry = (hash_entry*)pHashEntry->GetPtr() )
            {
                pHashEntry->m_Semaphore.lock();
                Function( pHashEntry->m_UserData );
                pHashEntry->m_Semaphore.unlock();
            }
            TableEntry.m_Semaphore.unlock();
        }
    }
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY, typename DOWORK > inline
void x_ll_mrmw_hash<ENTRY,KEY,DOWORK>::cpIterateGetEntry( t_const_function Function ) const noexcept 
{
    for( xuptr i=0; i<m_qtHashTable.getCount(); i++ )
    {
        const hash_table_entry& TableEntry = m_qtHashTable[i];
        if( TableEntry.m_LinkList.m_Head.GetPtr() )
        {
            TableEntry.m_Semaphore.lock();
            for( const hash_entry* pHashEntry = (const hash_entry*)TableEntry.m_LinkList.m_Head.GetPtr();
                 pHashEntry;       pHashEntry = (const hash_entry*)pHashEntry->GetPtr() )
            {
                pHashEntry->m_Semaphore.lock();
                Function( pHashEntry->m_UserData );
                pHashEntry->m_Semaphore.unlock();
            }
            TableEntry.m_Semaphore.unlock();
        }
    }
}
