//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_base.h"

const xbitmap::bit_pack_fields::t_flags xbitmap::bit_pack_fields::MASK_OWNS_MEMORY;
const xbitmap::bit_pack_fields::t_flags xbitmap::bit_pack_fields::MASK_ZERO;

//////////////////////////////////////////////////////////////////////////////////
// VARS
//////////////////////////////////////////////////////////////////////////////////
x_constexprvar u32 s_DefaultBitmapSize  = 128; 
using t_default_data = xarray<xbyte,s_DefaultBitmapSize*s_DefaultBitmapSize*sizeof(xcolor)+sizeof(s32)>;
static const t_default_data Data = []
{
    static t_default_data Data = { {0} };
    x_constexprvar xcolor C1{ 128, 128, 128, 250 };
    x_constexprvar xcolor C2{ 200, 200, 200, 255 };
    x_constexprvar xcolor CT[] = {  xcolor{ 0,  70, 70, 0 },
                                    xcolor{ 0,  70,  0, 0 },
                                    xcolor{ 70, 70,  0, 0 } };
    x_constexprvar s32    nCheckers    = 8;
    x_constexprvar s32    CheckerSize  = s_DefaultBitmapSize/nCheckers;
        
    xcolor* pData = reinterpret_cast<xcolor*>(&Data[4]);
        
    // Create basic checker pattern
    for( s32 y = 0; y < s_DefaultBitmapSize; y++ )
    {
        for( s32 x = 0; x < s_DefaultBitmapSize; x++)
        {
            // Create the checker pattern
            pData[ x+ s_DefaultBitmapSize*y ] = ((y&CheckerSize)==CheckerSize)^((x&CheckerSize)==CheckerSize)?C1:C2;
        }
    }
        
    // Draw a simple arrows at the top left pointing up...
    for( s32 k=0; k<3; k++ )
    {
        s32 yy = 1;
        for( s32 y=1; y<(CheckerSize-1); ++y )
        {
            for( s32 x = yy; x < (CheckerSize-1)-yy; x++)
            {
                if( k&1) pData[ k*CheckerSize + x + s_DefaultBitmapSize*(CheckerSize-y - 1) ] += CT[k];
                else     pData[ k*CheckerSize + x + s_DefaultBitmapSize*(CheckerSize-y - 1) ] -= CT[k];
            }
                
            if (y&1) yy++;
        }
    }

    return Data; 
}();
static const xbitmap s_DefaultBitmap{ Data, s_DefaultBitmapSize, s_DefaultBitmapSize, false };

//////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////

xbitmap::~xbitmap( void ) noexcept
{
    if( m_pData && m_Flags.m_OWNS_MEMORY )
        x_free( m_pData );
}

//-------------------------------------------------------------------------------

void xbitmap::Kill( void ) noexcept
{
    // Do we have anything to do?
    if( m_pData == nullptr )
        return;
    
    if( m_pData && m_Flags.m_OWNS_MEMORY )
        x_free( m_pData );

    m_pData         = nullptr;          
    m_DataSize      = 0;                
    m_FrameSize     = 0;                
    m_Height        = 0;                
    m_Width         = 0;                
    m_Flags.m_Value = 0;                
    m_nMips         = 0;                
    m_nFrames       = 0;                
}

//-------------------------------------------------------------------------------
/*
bool xbitmap::Load( const char* pFileName ) noexcept
{
    Kill();
  
    xfile File;
    
    if( File.Open( pFileName, "rb" ) == FALSE )
        return FALSE;
    
    //
    // Read the signature
    //
    {
        u32 Signature;
        File.Read( Signature );
        
        if( Signature != u32('XBMP') )
            return FALSE;
    }
    
    File.Read( (u8&)m_Format );
    File.Read( m_DataSize );
    File.Read( m_FrameSize );
    File.Read( m_Height );
    File.Read( m_Width );
    File.Read( m_Flags );
    File.Read( m_nMips );
    File.Read( m_nFrames );
    
    //
    // Read the big data
    //
    m_pData = (mip*)x_malloc( sizeof(xbyte), m_DataSize, 0 );
    File.ReadRaw( m_pData, m_DataSize, 1 );
    
    return TRUE;
}

//-------------------------------------------------------------------------------

xbool xbitmap::Save( const char* pFileName ) const noexcept
{
    xfile File;
    
    if( File.Open( pFileName, "wb" ) == FALSE )
        return FALSE;
    
    File.Write( u32('XBMP') );
    File.Write( (u8&)m_Format );
    File.Write( m_DataSize );
    File.Write( m_FrameSize );
    File.Write( m_Height );
    File.Write( m_Width );
    File.Write( m_Flags );
    File.Write( m_nMips );
    File.Write( m_nFrames );
    File.WriteRaw( m_pData, m_DataSize, 1 );
    
    return TRUE;
}
*/

//-------------------------------------------------------------------------------
/*
void xbitmap::SerializeIO( xserialfile& SerialFile ) const noexcept
{
    SerialFile.Serialize( m_RawData, m_DataSize );
    SerialFile.Serialize( m_DataSize );
    SerialFile.Serialize( m_FrameSize );
    SerialFile.Serialize( m_Height );
    SerialFile.Serialize( m_Width );
    SerialFile.Serialize( m_Flags );
    SerialFile.Serialize( m_nMips );
    SerialFile.SerializeEnum( m_Format );
    SerialFile.Serialize( m_nFrames );
}
  */
//-------------------------------------------------------------------------------

void xbitmap::setup(
    const u32                   Width                   ,
    const u32                   Height                  ,
    const xbitmap::format       BitmapFormat            ,
    const xuptr                 FrameSize               ,
    xbuffer_view<xbyte>         Data                    ,
    const bool                  bFreeMemoryOnDestruction,
    const int                   nMips                   ,
    const int                   nFrames               ) noexcept
{
    x_assert( Data.getCount()   >  4 );
    x_assert( FrameSize         >  0 );
    x_assert( FrameSize         <  Data.getCount() );   // In fact this should be equal to: DataSize - ((nMips*sizeof(s32)) * nFrames) which means it should be removed
    x_assert( FrameSize         <= X_U32_MAX );         // Since we are going to pack it into a u32 it can not be larger than that
    x_assert( nMips             >  0 );
    x_assert( nFrames           >  0 );
    x_assert( Width             >  0 );
    x_assert( Height            >  0 );
    x_assert( BitmapFormat      >  FORMAT_NULL );
    x_assert( BitmapFormat      <  FORMAT_TOTAL );

    Kill();

    m_pData             = reinterpret_cast<xbitmap::mip *>( &Data[0] );
    m_DataSize          = Data.getCount();
    
    x_assert( FrameSize == m_DataSize - ((nMips*sizeof(s32)) * nFrames) );
    
    m_FrameSize         = static_cast<u32>(FrameSize);
    m_Height            = Height;   
    m_Width             = Width;
    
    m_Flags.m_OWNS_MEMORY = bFreeMemoryOnDestruction;
    
    m_nMips             = nMips;    
    m_Flags.m_FORMAT    = BitmapFormat;   
    m_nFrames           = nFrames;  
}

//-------------------------------------------------------------------------------

void xbitmap::setupFromColor( const u32                     Width                           ,
                              const u32                     Height                          ,
                                    xbuffer_view<xcolor>    Data                           ,
                              const bool                    bFreeMemoryOnDestruction        ) noexcept
{
    setup( Width,
           Height,
           FORMAT_XCOLOR,
           sizeof(xcolor)*Width*Height,
           { reinterpret_cast<xbyte*>(&Data[0]), sizeof(xcolor)*( 1 + Width*Height ) },
           bFreeMemoryOnDestruction,
           1,
           1 );
}

//-------------------------------------------------------------------------------

bool xbitmap::hasAlphaChannel( void ) const noexcept
{
    struct entry
    {
        s8        m_bSupportsAlpha;
    };

    static const xarray< entry, FORMAT_TOTAL > SupportAlphaTable = []() noexcept -> auto
    {
        xarray< entry, FORMAT_TOTAL > SupportAlphaTable = {{ entry{ -1 } }};

        SupportAlphaTable[ FORMAT_R4G4B4A4              ] = entry{ true      };
        SupportAlphaTable[ FORMAT_R8G8B8                ] = entry{ false     };
        SupportAlphaTable[ FORMAT_R8G8B8U8              ] = entry{ false     };
        SupportAlphaTable[ FORMAT_R8G8B8A8              ] = entry{ true      };
        SupportAlphaTable[ FORMAT_A8R8G8B8              ] = entry{ true      };
        SupportAlphaTable[ FORMAT_U8R8G8B8              ] = entry{ false     };
                                                                              
        SupportAlphaTable[ FORMAT_PAL4_R8G8B8A8         ] = entry{ true      };
        SupportAlphaTable[ FORMAT_PAL8_R8G8B8A8         ] = entry{ true      };
                                                                              
        SupportAlphaTable[ FORMAT_ETC2_4RGB             ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ETC2_4RGBA1           ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ETC2_8RGBA            ] = entry{ true      };
                                                                              
        SupportAlphaTable[ FORMAT_BC1_4RGB              ] = entry{ false     };
        SupportAlphaTable[ FORMAT_BC1_4RGBA1            ] = entry{ true      };
        SupportAlphaTable[ FORMAT_BC2_8RGBA             ] = entry{ true      };
        SupportAlphaTable[ FORMAT_BC3_8RGBA             ] = entry{ true      };
        SupportAlphaTable[ FORMAT_BC4_4R                ] = entry{ false     };
        SupportAlphaTable[ FORMAT_BC5_8RG               ] = entry{ false     };
        SupportAlphaTable[ FORMAT_BC6H_8RGB_FLOAT       ] = entry{ false     };
        SupportAlphaTable[ FORMAT_BC7_8RGBA             ] = entry{ false     };
                                                                              
        SupportAlphaTable[ FORMAT_ASTC_4x4_8RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_5x4_6RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_5x5_5RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_6x5_4RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_6x6_4RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_8x5_3RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_8x6_3RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_8x8_2RGB         ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_10x5_3RGB        ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_10x6_2RGB        ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_10x8_2RGB        ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_10x10_1RGB       ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_12x10_1RGB       ] = entry{ false     };
        SupportAlphaTable[ FORMAT_ASTC_12x12_1RGB       ] = entry{ false     };
                                                                              ;
        SupportAlphaTable[ FORMAT_PVR1_2RGB             ] = entry{ false     };
        SupportAlphaTable[ FORMAT_PVR1_2RGBA            ] = entry{ true      };
        SupportAlphaTable[ FORMAT_PVR1_4RGB             ] = entry{ false     };
        SupportAlphaTable[ FORMAT_PVR1_4RGBA            ] = entry{ true      };
        SupportAlphaTable[ FORMAT_PVR2_2RGBA            ] = entry{ true      };
        SupportAlphaTable[ FORMAT_PVR2_4RGBA            ] = entry{ true      };
                                                                              ;
        SupportAlphaTable[ FORMAT_D24S8_FLOAT           ] = entry{ false     };
        SupportAlphaTable[ FORMAT_D24S8                 ] = entry{ false     };
        SupportAlphaTable[ FORMAT_R8                    ] = entry{ false     };
        SupportAlphaTable[ FORMAT_R32                   ] = entry{ false     };
        SupportAlphaTable[ FORMAT_R8G8                  ] = entry{ false     };
        SupportAlphaTable[ FORMAT_R16G16B16A16          ] = entry{ true      };
        SupportAlphaTable[ FORMAT_R16G16B16A16_FLOAT    ] = entry{ true      };
        SupportAlphaTable[ FORMAT_A2R10G10B10           ] = entry{ true      };
        SupportAlphaTable[ FORMAT_B11G11R11_FLOAT       ] = entry{ false     };

        return SupportAlphaTable;
    }();

    // Must insert this format into the table and assign avalue to it
    x_assume( SupportAlphaTable[m_Flags.m_FORMAT].m_bSupportsAlpha == 1 || SupportAlphaTable[m_Flags.m_FORMAT].m_bSupportsAlpha == 0 );
    static_assert( static_cast<int>(true)  == 1, "True should be just one" );
    static_assert( static_cast<int>(false) == 0, "False should be just zero" );
    return !!SupportAlphaTable[m_Flags.m_FORMAT].m_bSupportsAlpha;
}

//-------------------------------------------------------------------------------

void xbitmap::ComputeHasAlphaInfo( void ) noexcept
{
    //
    // Do the easy case first
    //
    if( hasAlphaChannel() == false )
    {
        setHasAlphaInfo( false );
        return;
    }
    
    
    //
    // Check all the pixels to see if they have information other than the default
    //
    
    // We can handle anything with the compress formats and such
    x_assert( m_Flags.m_FORMAT < FORMAT_XCOLOR_END );
    
    const xcolor::fmt_desc& Dest   = xcolor::getFormatDesc( (xcolor::format)m_Flags.m_FORMAT );
    const xcolor::format    Format = xcolor::format( m_Flags.m_FORMAT );
    if( Dest.m_TB == 16 )
    {
        u32     X=0;
        xcolor  C1;
        u16*    pData = reinterpret_cast<u16*>(getMipPtr(0));
        for( u32 y=0; y<m_Height; y++ )
        for( u32 x=0; x<m_Width; x++ )
        {
            u16 Data = pData[ x + y*m_Width ];
            C1.setupFromData( Data, Format );
            
            X |= (C1.m_A == 0xff);
            X |= (C1.m_A == 0x00)<<1;
            if( X == 1 || X == 2 )
                continue;
            
            setHasAlphaInfo( true );
            return;
        }
    }
    else
    {
        u32     X=0;
        xcolor  C1;
        u32*    pData = reinterpret_cast<u32*>(getMipPtr(0));
        for( u32 y=0; y<m_Height; y++ )
        for( u32 x=0; x<m_Width; x++ )
        {
            u32 Data = pData[ x + y*m_Width ];
            C1.setupFromData( Data, Format );
            
            X |= (C1.m_A == 0xff);
            X |= (C1.m_A == 0x00)<<1;
            if( X == 1 || X == 2 )
                continue;
            
            setHasAlphaInfo( true );
            return;
        }
    }
}

//-------------------------------------------------------------------------------

void xbitmap::setDefaultTexture( void ) noexcept
{
    Kill();
    x_memcpy( this, &s_DefaultBitmap, sizeof(*this) );
}

//-------------------------------------------------------------------------------

const xbitmap& xbitmap::getDefaultBitmap( void ) noexcept
{
    static xbitmap  Bitmap;
    
    if( Bitmap.getHeight() == 0 )
    {
        Bitmap.setDefaultTexture();
    }
    
    return Bitmap;
}

//-------------------------------------------------------------------------------

void xbitmap::CreateBitmap( const u32 Width, const u32 Height ) noexcept
{
    x_assert( Width  >= 1 );
    x_assert( Height >= 1 );
    
    // Allocate the necesary data
    s32* pPtr = x_malloc( s32, 1 + Width * Height );
    
    // Initialize the offset table
    *pPtr = 0;
    
    setupFromColor( 
        Width, 
        Height, 
        xbuffer_view<xcolor>
        { 
            reinterpret_cast<xcolor*>(pPtr), 
            sizeof(xcolor)*(1 + Width * Height) 
        }, 
        true );
}

//-------------------------------------------------------------------------------
/*
bool xbitmap::SaveTGA( const xstring FileName ) const noexcept
{
    xbyte   Header[18];
    
    // The format of this picture must be in color format
    ASSERT( m_Format == FORMAT_XCOLOR || m_Format == FORMAT_R8G8B8U8 );
    
    // Build the header information.
    x_memset( Header, 0, 18 );
    Header[ 2] = 2;     // Image type.
    Header[12] = (getWidth()  >> 0) & 0xFF;
    Header[13] = (getWidth()  >> 8) & 0xFF;
    Header[14] = (getHeight() >> 0) & 0xFF;
    Header[15] = (getHeight() >> 8) & 0xFF;
    Header[16] = 32;    // Bit depth.
    Header[17] = 32;    // NOT flipped vertically.
    
    // Open the file.
    xfile File;
    
    if( !File.Open( FileName, "wb" ) )
    {
        return FALSE;
    }
    
    // Write out the data.
    File.WriteRaw( Header, 1, 18 );
    
    //
    // Convert to what tga expects as a color
    //
    const xcolor* pColor = (xcolor*)getMip(0);
    const s32     Size   = getWidth() * getHeight();
    
    for( s32 i=0; i<Size; ++i )
    {
        xcolor Color = pColor[i];
        x_Swap( Color.m_R, Color.m_B );
        File.WriteRaw( &Color, 4, 1 );
    }
    
    return true;
}
*/

//-------------------------------------------------------------------------------

void xbitmap::CreateFromMips( const xbuffer_view<xbitmap> MipList ) noexcept
{
    //
    // Compute the total size
    //
    xuptr TotalSize=0;
    for( s32 i=0; i<MipList.getCount(); i++ )
    {
        const xbitmap& Mip = MipList[i];
        
        TotalSize += Mip.m_DataSize;
        
        x_assert( Mip.m_nMips   == 1 );
        x_assert( Mip.m_nFrames == 1 );
    }
    
    //
    // Allocate data
    //
    xbyte* pBaseData    = x_malloc( xbyte, TotalSize );
    s32*   pOffsetTable = reinterpret_cast<s32*>(pBaseData);
    xbyte* pData        = reinterpret_cast<xbyte*>(&pOffsetTable[MipList.getCount()]);
    
    //
    // Copy the actual data
    //
    {
        xuptr TotalOffset=0;
        for( s32 i=0; i<MipList.getCount(); i++ )
        {
            const xbitmap& Mip         = MipList[i];
            s32&           Offset      = pOffsetTable[i];
            const xuptr    MipDataSize = Mip.m_DataSize - sizeof(s32);
            
            Offset = static_cast<s32>( TotalOffset );
            
            // Copy Data
            x_memcpy( &pData[ Offset ], &Mip.m_pData[1], MipDataSize );
            
            // Get ready for the next entry
            TotalOffset += MipDataSize;
        }
    }
    
    //
    // OK we are ready to setup the bitmap
    //
    const xbitmap& Mip         = MipList[0];
    setup(
          Mip.m_Width,
          Mip.m_Height,
          static_cast<xbitmap::format>(Mip.getFormat()),
          static_cast<int>( TotalSize - sizeof(s32) * MipList.getCount() ),
          { reinterpret_cast<xbyte*>(pBaseData), TotalSize },
          true,
          static_cast<int>( MipList.getCount() ),
          1 );
}

//-------------------------------------------------------------------------------

void xbitmap::ComputePremultiplyAlpha( void ) noexcept
{
    if( m_Flags.m_ALPHA_PREMULTIPLIED )
        return ;
         
    x_assert( static_cast<int>(m_Flags.m_FORMAT) == static_cast<int>(xcolor::FMT_XCOLOR) );

    if( m_Flags.m_HAS_ALPHA_INFO == false ) ComputeHasAlphaInfo();

    // It must have alpha information if we are about to do this
    x_assert( m_Flags.m_HAS_ALPHA_INFO );

    auto Data = getMip<xcolor>(0);
    for( xcolor& C : Data ) 
    {
        C = C.PremultiplyAlpha();
    }

    // Remember that we did this
    m_Flags.m_ALPHA_PREMULTIPLIED = true;
}

//-------------------------------------------------------------------------------

void xbitmap::FlipImageInY( void ) noexcept
{
    x_assert( isValid() );
    x_assert( getFormat() == format::FORMAT_XCOLOR );
    x_assert( getMipCount() == 1 );

    xcolor* m_pData = getMip<xcolor>(0);
    for( u32 y = 0; y < m_Height/2; y++ )
    for( u32 x = 0; x < m_Width;  x++ )
    {
        x_Swap( m_pData[ x + y * m_Width], 
                m_pData[ x + (m_Height-y-1) * m_Width] );
    }
}



