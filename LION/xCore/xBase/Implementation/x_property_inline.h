//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY DATA
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------
x_inline
xproperty_data& xproperty_data::operator = ( const xproperty_data& A ) 
{
    // setup the new type properly
    setup( A.getType() );

    // copy over the trivial types
    CopyInfo( A );
  
    // copy the actual data
    switch( A.getType() ) 
    {
        case type::BOOL:        m_Data.m_Bool       = A.m_Data.m_Bool;      break;
        case type::BUTTON:      m_Data.m_Button     = A.m_Data.m_Button;    break;
        case type::RGBA:        m_Data.m_Color      = A.m_Data.m_Color;     break;
        case type::ENUM:        m_Data.m_Enum       = A.m_Data.m_Enum;      break;
        case type::INT:         m_Data.m_S32        = A.m_Data.m_S32;       break;
        case type::FLOAT:       m_Data.m_S32        = A.m_Data.m_S32;       break;
        case type::ANGLES3:     m_Data.m_Radian3    = A.m_Data.m_Radian3;   break;
        case type::STRING:      m_Data.m_String     = A.m_Data.m_String;    break;
        case type::GUID:        m_Data.m_Guid       = A.m_Data.m_Guid;      break;
        case type::VECTOR2:     m_Data.m_Vector2    = A.m_Data.m_Vector2;   break;
        case type::VECTOR3:     m_Data.m_Vector3    = A.m_Data.m_Vector3;   break;
        case type::DELEGATE:    m_Data.m_Delegate   = A.m_Data.m_Delegate;  break;
        case type::EVENT:       m_Data.m_Event      = A.m_Data.m_Event;     break;
        default: x_assert( false );
    }  

    return *this; 
}

//------------------------------------------------------------------------------------------------
x_forceinline           
void xproperty_data::CopyInfo( const xproperty_data& B )
{
    m_Name          = B.m_Name;
    m_Info.m_Flags  = B.m_Info.m_Flags;
    m_Info.m_Gizmos = B.m_Info.m_Gizmos;
    m_Help          = B.m_Help;
}

//------------------------------------------------------------------------------------------------
x_inline
void xproperty_data::RegisterTypes   ( xtextfile& DataFile ) 
{
    for( int i=1; i<getTable().getCount(); i++ )
    {
        auto& Entry = getTable()[i];
        DataFile.AddUserType( Entry.m_pTextFileType, &Entry.m_pStringType[x_constStrLength("Value:")], static_cast<int>(Entry.m_EnumType) );
    }
}
    
//------------------------------------------------------------------------------------------------
x_inline
void xproperty_data::SerializeOut    ( xtextfile& DataFile ) const
{
    DataFile.WriteField( "Type:<?>", getWriteTypeString()    );
    DataFile.WriteField( "Name:s",   (const char*)m_Name );

    static const char* pValueType = "Value:<Type>";
    switch( getType() )
    {
        case type::BOOL:     DataFile.WriteField( pValueType, m_Data.m_Bool );                                                                                          break;
        case type::BUTTON:   DataFile.WriteField( pValueType, (const char*) m_Data.m_Button );                                                                          break;
        case type::RGBA:     DataFile.WriteField( pValueType, m_Data.m_Color.m_R, m_Data.m_Color.m_G, m_Data.m_Color.m_B, m_Data.m_Color.m_A );                         break;
        case type::ENUM:     DataFile.WriteField( pValueType, (const char*)m_Data.m_Enum.m_String );                                                                    break;
        case type::INT:      DataFile.WriteField( pValueType, m_Data.m_S32.m_Value );                                                                                   break;
        case type::FLOAT:    DataFile.WriteField( pValueType, m_Data.m_F32.m_Value );                                                                                   break;
        case type::ANGLES3:  DataFile.WriteField( pValueType, m_Data.m_Radian3.m_Value.m_Pitch.getDegrees().m_Value,
                                                              m_Data.m_Radian3.m_Value.m_Yaw.getDegrees().m_Value,
                                                              m_Data.m_Radian3.m_Value.m_Roll.getDegrees().m_Value   );                                                 break;
        case type::STRING:   DataFile.WriteField( pValueType, (const char*) m_Data.m_String );                                                                          break;
        case type::GUID:     DataFile.WriteField( pValueType, m_Data.m_Guid.m_Value );                                                                                  break;
        case type::VECTOR2:  DataFile.WriteField( pValueType, m_Data.m_Vector2.m_Value.m_X, m_Data.m_Vector2.m_Value.m_Y  );                                            break;
        case type::VECTOR3:  DataFile.WriteField( pValueType, m_Data.m_Vector3.m_Value.m_X, m_Data.m_Vector3.m_Value.m_Y, m_Data.m_Vector3.m_Value.m_Z  );              break;
        case type::DELEGATE: DataFile.WriteField( pValueType, m_Data.m_Delegate.m_ObjectGuid, m_Data.m_Delegate.m_EventInstanceGuid  );                                 break;
        case type::EVENT:    DataFile.WriteField( pValueType, m_Data.m_Event.m_ObjectGuid, m_Data.m_Event.m_EventInstanceGuid   );                                      break;
        default: x_assert( false );
    }

    DataFile.WriteLine  ();
}

//------------------------------------------------------------------------------------------------
x_inline
void xproperty_data::SerializeIn    ( xtextfile& DataFile )
{
    xstring         TypeUID;
    xstring         PropName;

    auto Type = static_cast<type>( DataFile.ReadField( "Type:<?>", &TypeUID ) + 1 );
    x_assert( static_cast<int>(Type) >= static_cast<int>(type::INVALID) );
    x_assert( static_cast<int>(Type) <  static_cast<int>(type::ENUM_COUNT) );
    setup( Type );

    DataFile.ReadFieldXString( "Name:s", m_Name );

    switch( getType() )
    {
        case type::BOOL:    DataFile.ReadField( getReadTypeString(),        &m_Data.m_Bool );                                                                                   break;
        case type::BUTTON:  DataFile.ReadFieldXString( getReadTypeString(), m_Data.m_Button       );                                                                            break;
        case type::RGBA:    DataFile.ReadField( getReadTypeString(),        &m_Data.m_Color.m_R, &m_Data.m_Color.m_G, &m_Data.m_Color.m_B, &m_Data.m_Color.m_A );               break;
        case type::ENUM:    DataFile.ReadFieldXString( getReadTypeString(), m_Data.m_Enum.m_String );                                                                           break;
        case type::INT:     DataFile.ReadField( getReadTypeString(),        &m_Data.m_S32.m_Value  );                                                                           break;
        case type::FLOAT:   DataFile.ReadField( getReadTypeString(),        &m_Data.m_F32.m_Value  );                                                                           break;
        case type::ANGLES3: 
        {
            xdegree Pitch, Yaw, Roll;
            DataFile.ReadField( getReadTypeString(), &Pitch.m_Value, &Yaw.m_Value, &Roll.m_Value  );
            m_Data.m_Radian3.m_Value.setup( xradian{ Pitch }, xradian{ Yaw }, xradian{ Roll } );
            break;
        }
        case type::STRING:  DataFile.ReadFieldXString( getReadTypeString(), m_Data.m_String       );                                                                            break;
        case type::GUID:    DataFile.ReadField( getReadTypeString(),        &m_Data.m_Guid.m_Value );                                                                           break;
        case type::VECTOR2: DataFile.ReadField( getReadTypeString(),        &m_Data.m_Vector2.m_Value.m_X, &m_Data.m_Vector2.m_Value.m_Y );                                     break;
        case type::VECTOR3: DataFile.ReadField( getReadTypeString(),        &m_Data.m_Vector3.m_Value.m_X, &m_Data.m_Vector3.m_Value.m_Y, &m_Data.m_Vector3.m_Value.m_Z );      break;
        case type::DELEGATE: DataFile.ReadField( getReadTypeString(),       &m_Data.m_Delegate.m_ObjectGuid, &m_Data.m_Delegate.m_EventInstanceGuid );                          break;
        case type::EVENT:    DataFile.ReadField( getReadTypeString(),       &m_Data.m_Event.m_ObjectGuid, &m_Data.m_Event.m_EventInstanceGuid );                                break;
        default: x_assert( false );
    }
}

//------------------------------------------------------------------------------------------------
x_inline
typename xproperty_data::t_self& xproperty_data::setup( type Type, xstring& Name, flags Flags, xstring::const_str Help, u32 GizmosFlags )
{
    setup( Type );
    m_Name          = Name;
    m_Help          = Help;
    m_Info.m_Flags  = Flags;
    m_Info.m_Gizmos = GizmosFlags;
    return *this;
}

//------------------------------------------------------------------------------------------------
x_inline
typename xproperty_data::t_self& xproperty_data::setup( type Type )
{
    if( getType() == Type ) return *this;
    //
    // Release formats if we have to
    //
    switch( getType() )
    {
        case type::BUTTON: m_Data.m_Button.~xstring_base();             break;
        case type::STRING: m_Data.m_String.~xstring_base();             break;
        case type::ENUM:   m_Data.m_Enum.m_String.~xstring_base();      break;
        default: break;
    }

    //
    // Prepare formats if we have to
    //
  
    switch( Type )
    {
        case type::BUTTON: (void)x_construct( xstring,    &m_Data.m_Button ); break;
        case type::STRING: (void)x_construct( xstring,    &m_Data.m_String ); break;
        case type::ENUM:   (void)x_construct( data_enum,  &m_Data.m_Enum );   break;
        default: break;
    }
     
    //
    // Finally set the type
    //
    m_Info.m_Type = Type;

    return *this;
}

//------------------------------------------------------------------------------------------------
x_inline
bool xproperty_data::operator == ( const xproperty_data& B ) const
{
    if( getType() != B.getType() ) return false;
    if( m_Name != B.m_Name ) return false;

    switch( getType() )
    {
        case type::BOOL:        return m_Data.m_Bool                            == B.m_Data.m_Bool;
        case type::BUTTON:      return m_Data.m_Button                          == B.m_Data.m_Button;
        case type::RGBA:        return m_Data.m_Color.m_Value                   == B.m_Data.m_Color.m_Value;
        case type::ENUM:        return m_Data.m_String                          == B.m_Data.m_String;
        case type::INT:         return m_Data.m_S32.m_Value                     == B.m_Data.m_S32.m_Value;
        case type::FLOAT:       return m_Data.m_F32.m_Value                     == B.m_Data.m_F32.m_Value;
        case type::ANGLES3:     return m_Data.m_Radian3.m_Value                 == B.m_Data.m_Radian3.m_Value; 
        case type::STRING:      return m_Data.m_String                          == B.m_Data.m_String;
        case type::GUID:        return m_Data.m_Guid.m_Value                    == B.m_Data.m_Guid.m_Value;
        case type::VECTOR2:     return m_Data.m_Vector2.m_Value                 == B.m_Data.m_Vector2.m_Value;
        case type::VECTOR3:     return m_Data.m_Vector3.m_Value                 == B.m_Data.m_Vector3.m_Value;
        case type::DELEGATE:    return m_Data.m_Delegate.m_ObjectGuid   == B.m_Data.m_Delegate.m_ObjectGuid && m_Data.m_Delegate.m_EventInstanceGuid    == B.m_Data.m_Delegate.m_EventInstanceGuid;
        case type::EVENT:       return m_Data.m_Event.m_ObjectGuid      == B.m_Data.m_Event.m_ObjectGuid    && m_Data.m_Event.m_EventInstanceGuid       == B.m_Data.m_Event.m_EventInstanceGuid;
        default: x_assert( false );
    }

    return false;
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY 
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------
x_inline        
void xproperty::PropEnum( xproperty_data_collection& Collection, bool bFullPath, xproperty_enum::mode Mode ) const
{
    PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
    {
        Collection.m_List.append() = Data;
    }, bFullPath, Mode );
}

//------------------------------------------------------------------------------------------------
x_inline
void xproperty::PropEnum ( xproperty_enum::callback Function, bool bFullPath, xproperty_enum::mode Mode ) const
{
    xproperty_enum Enum{ Function, Mode };
    Enum.setForceFullPaths( bFullPath );
    onPropEnum( Enum );
}

//------------------------------------------------------------------------------------------------
x_inline
bool xproperty::PropQueryGet ( xproperty_data_collection& Collection ) const noexcept
{
    return PropQueryGet( [&]( xproperty_query& Query, xproperty_data& Data )
    {
        //
        // First read the results if any
        //
        if( Query.hasResults() )
        {
            const int   Index  = Query.getResultsIndex();
            const auto& Res    = Query.getResults();
                
            // Result must match the type and the name at least 
            x_assert( Res.getType() == Collection.m_List[Index].getType() );
            x_assert( 0 == x_strcmp<xchar>( Res.m_Name, Collection.m_List[Index].m_Name ) );

            // Store the data in our result array
            Collection.m_List[Index] = Res;
        }

        //
        // are we done?
        //
        if( Query.getPropertyIndex() == Collection.m_List.getCount() ) 
        {
            return false;
        }

        //
        // Tell which property to get next
        //
        const auto& Prop = Collection.m_List[ Query.getPropertyIndex() ];
        Data.CopyInfo( Prop );

        return true;
    });
}

//------------------------------------------------------------------------------------------------
x_inline
bool xproperty::PropQueryGet( xproperty_query::get_callback Function ) const noexcept
{
    xproperty_query Query;
    return Query.RunGet( const_cast<xproperty&>(*this), Function );
}

//------------------------------------------------------------------------------------------------
x_inline
bool xproperty::PropQuerySet( xproperty_query::set_callback Function ) noexcept
{
    xproperty_query Query;
    return Query.RunSet( *this, Function );
}

//------------------------------------------------------------------------------------------------
x_inline
void xproperty::PropToCollection( xproperty_data_collection& Collection, bool bFullPath ) const
{
    //
    // Enumerate all the properties
    //
    PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
    {
        // Just collect them all inside an static array
        Collection.m_List.append() = Data;
        
    }, bFullPath );

    //
    // Test getting values
    //
    PropQueryGet( [&]( xproperty_query& Query, xproperty_data& Data )
    {
        //
        // First read the results if any
        //
        if( Query.hasResults() )
        {
            Collection.m_List[ Query.getResultsIndex() ] = Query.getResults();
        }

        //
        // are we done?
        //
        if( Query.getPropertyIndex() == Collection.m_List.getCount() ) 
        {
            return false;
        }

        //
        // Tell which property to get next
        //
        Data.CopyInfo( Collection.m_List[ Query.getPropertyIndex() ] );

        return true;
    });
}


//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY QUERY
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------
x_forceinline   
bool xproperty_query::RunGet( const xproperty& Property, get_callback CallBack ) noexcept   
{ 
    Reset(); 
    m_Flags.m_IS_GET = false; 
    m_GetNextPropertyLambda = CallBack; 
    NextPropertyOrChangeScope(); 
    return Property.onPropQuery( *this ); 
}

//------------------------------------------------------------------------------------------------
x_forceinline   
bool xproperty_query::RunSet( xproperty& Property, set_callback CallBack ) noexcept         
{ 
    Reset(); 
    m_Flags.m_IS_GET = true;  
    m_SetNextPropertyLambda = CallBack; 
    NextPropertyOrChangeScope(); 
    return Property.onPropQuery( *this ); 
}

//------------------------------------------------------------------------------------------------
x_forceinline        
bool xproperty_query::isVar( const u32 CRC ) noexcept
{
    x_assert( m_Flags.m_STATE == state::SEARCHING );
    x_assert( isSearchingForScopes() == false );

    if( CRC != m_CurPropNextCRC )
        return false;

    m_bPropFound = true;
    return true;
}

//------------------------------------------------------------------------------------------------

template< typename T_CHAR, u32 T_CRC, int T_LENGTH > x_forceinline  
bool xproperty_query::isVar( xstring_crcinfo<T_CHAR, T_CRC,T_LENGTH> Node ) noexcept
{
    return isVar( T_CRC );
/*
    x_assert( m_Flags.m_STATE == state::SEARCHING );
    x_assert( isSearchingForScopes() == false );

    if( T_CRC != m_CurPropNextCRC )
        return false;

    m_bPropFound = true;
    return true;
*/
}

//------------------------------------------------------------------------------------------------

template< typename T_CHAR, u32 T_CRC, int T_LENGTH > x_inline 
bool xproperty_query::isScope( xstring_crcinfo<T_CHAR, T_CRC, T_LENGTH> Node )
{
    x_assert( m_Flags.m_STATE == state::SEARCHING );
    x_assert( isSearchingForScopes() == true );

    if( T_CRC != m_CurPropNextCRC )
        return false;

    // officially move to the next scope
    m_ScopeData[m_iScope].m_CRC                 = T_CRC;
    m_ScopeData[m_iScope].m_ArrayIndex          = m_CurPropArrayIndex;
#if _X_DEBUG
    m_ScopeData[m_iScope].m_DEBUG_ScopeName     = Node.m_DEBUG_Data;
    m_ScopeData[m_iScope].m_DEBUG_Length        = T_LENGTH;
#endif
    m_iScope++;

    // Get ready for the next scope/property
    PrepareNextScope();
    return true;
}

//------------------------------------------------------------------------------------------------

x_inline 
bool xproperty_query::isScope( u32 CRC, xstring::const_str pStr, int StrLenth )
{
    x_assert( m_Flags.m_STATE == state::SEARCHING );
    x_assert( isSearchingForScopes() == true );

    if( CRC != m_CurPropNextCRC )
        return false;

    // officially move to the next scope
    m_ScopeData[m_iScope].m_CRC                 = CRC;
    m_ScopeData[m_iScope].m_ArrayIndex          = m_CurPropArrayIndex;
#if _X_DEBUG
    m_ScopeData[m_iScope].m_DEBUG_ScopeName     = pStr;
    m_ScopeData[m_iScope].m_DEBUG_Length        = StrLenth;
#endif
    m_iScope++;

    // Get ready for the next scope/property
    PrepareNextScope();
    return true;
}

//------------------------------------------------------------------------------------------------

template< typename T > x_forceinline
xproperty_data& xproperty_query::setProp( void ) 
{
    x_assert( false == isReceiving() );
    auto& Prop = getGetProperty();
    Prop.setup( T::t_symbol ); 
    return Prop;
}


//------------------------------------------------------------------------------------------------
x_inline
void xproperty_query::Reset( void )
{
    m_Flags.m_STATE             = state::SEARCHING;
    m_Flags.m_IS_SEARCH_SCOPES  = true;
    m_iScope                    = 0;
    m_nProccessedProperties     = -1;
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY ENUM
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------
template< typename T > x_forceinline
void xproperty_enum::scope::AddProperty( xstring::const_str PropertyName, xstring::const_str Help, flags Flags, typename T::gizmo_flags GizmosFlags  )
{
    static_assert( T::t_symbol != xproperty_data::type::INVALID, "This symbol is not supported as a property type" );
    AddProperty( PropertyName, T::t_symbol, Help, Flags, static_cast<u32>( GizmosFlags.m_Value ) );
}

//------------------------------------------------------------------------------------------------
template< typename T >
void xproperty_enum::scope::beginScope ( xstring::const_str ScopeName, T&& CallBack )
{
    using fa = xfunction_traits< typename std::remove_reference<T>::type >;
    using fb = xfunction_traits< typename x_make_function<void, scope& >::type >;
    static_assert( xfunction_traits_compare<fa,fb>::value, "" );

    auto& Entry = m_Enum.m_Scope[++m_Enum.m_iScope];
    Entry.m_Name        = ScopeName;
    Entry.m_Sequence    = m_Enum.m_Sequence++;
    CallBack( *this );

    // clean entry when we leave
    --m_Enum.m_iScope;
    Entry.m_ArrayIndex  = -1;
    Entry.m_ArrayCount  = -1;
}

//------------------------------------------------------------------------------------------------

template< typename T >
void xproperty_enum::scope::beginScopeArray ( xstring::const_str ScopeName, int Count, T&& CallBack )
{
    using fa = xfunction_traits< typename std::remove_reference<T>::type >;
    using fb = xfunction_traits< typename x_make_function<void, scope_array& >::type >;
    static_assert( xfunction_traits_compare<fa,fb>::value, "" );

    auto& Entry = m_Enum.m_Scope[++m_Enum.m_iScope];
    Entry.m_Name        = ScopeName;
    Entry.m_Sequence    = m_Enum.m_Sequence++;
    Entry.m_ArrayCount  = Count;

    // output the count
    AddProperty<xprop_s32>( X_STR("[]Count"), X_STR("Array Count") );

    // handle the user 
    CallBack( reinterpret_cast<scope_array&>(*this) );
    --m_Enum.m_iScope;

    // clean entry when we leave
    Entry.m_ArrayIndex  = -1;
    Entry.m_ArrayCount  = -1;
}

//------------------------------------------------------------------------------------------------

template< typename T >
void xproperty_enum::scope_array::beginScopeArrayEntry   ( int Index, T&& CallBack )
{
    using fa = xfunction_traits< typename std::remove_reference<T>::type >;
    using fb = xfunction_traits< typename x_make_function<void, scope& >::type >;
    static_assert( xfunction_traits_compare<fa,fb>::value, "" );

    X_CMD_DEBUG( const int ArrayCount = m_Enum.m_Scope[m_Enum.m_iScope].m_ArrayCount );
    x_assert( Index < ArrayCount );
    auto& Entry = m_Enum.m_Scope[m_Enum.m_iScope];
    Entry.m_ArrayIndex  = Index;

    CallBack( reinterpret_cast<scope&>(*this) );
}



