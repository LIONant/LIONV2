//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <math.h>

//==============================================================================
// Types
//==============================================================================
union f32_forge
{
    constexpr f32_forge( void ) = default;
    constexpr f32_forge( const u32 b ) : m_Bits(b) {}
    constexpr f32_forge( const f32 f ) : m_F32 (f) {}

    u32     m_Bits {};
    f32     m_F32;
};

//==============================================================================
// Functions
//==============================================================================

// calculate the sin value of a number
inline      f32         x_Sin        ( const xradian   X )              { return sinf   ( X.m_Value );                      }
inline      f64         x_SinF64     ( const xradian64 X )              { return sin    ( X.m_Value );                      }
inline      f32         x_Cos        ( const xradian   X )              { return cosf   ( X.m_Value );                      }
inline      f64         x_CosF64     ( const xradian64 X )              { return cos    ( X.m_Value );                      }
inline      f32         x_Tan        ( const xradian   X )              { return tanf   ( X.m_Value );                      }
inline      f64         x_TanF64     ( const xradian64 X )              { return tan    ( X.m_Value );                      }
inline      xradian     x_ATan       ( const f32       X )              { return xradian   { atanf  ( X )      };           }
inline      xradian64   x_ATanF64    ( const f64       X )              { return xradian64 { atan   ( X )      };           }
inline      xradian     x_ATan2      ( const f32 X, const f32 Y )       { return xradian   { atan2f ( X, Y )   };           }
inline      f32         x_Exp        ( const f32 X )                    { return expf   ( X );                              }
inline      f64         x_ExpF64     ( const f64 X )                    { return exp    ( X);                               }
inline      f32         x_Pow        ( const f32 X, const f32 Y )       { return powf   ( X, Y );                           }
inline      f64         x_PowF64     ( const f64 X, const f64 Y )       { return pow    ( X, Y );                           }
inline      f32         x_FMod       ( const f32 X, const f32 Y )       { return fmodf  ( X, Y );                           }
inline      f64         x_FModF64    ( const f64 X, const f64 Y )       { return fmod   ( X, Y );                           }
inline      f32         x_ModF       ( const f32 X, f32& Y )            { return modff  ( X, &Y );                          }
inline      f64         x_ModFF64    ( const f64 X, f64& Y )            { return modf   ( X, &Y );                          }
inline      f32         x_Log        ( const f32 X )                    { return logf   ( X );                              }
inline      f64         x_LogF64     ( const f64 X )                    { return log    ( X );                              }
inline      f32         x_Log2       ( const f32 X )                    { return logf   ( X ) * 1.442695041f;               }
inline      f32         x_Log10      ( const f32 X )                    { return log10f ( X );                              }
constexpr   f32         x_I2F        ( const s32 X )                    { return static_cast<const f32>( X );               }
constexpr   s32         x_F2I        ( const f32 X )                    { return static_cast<const s32>( X );               }

//------------------------------------------------------------------------------

#ifdef TARGET_360
    #define x_FSel(a,b,c)		__fsel( (a), (b), (c) )
#else
    constexpr f32 x_FSel( const f32 a, const f32 b, const f32 c ) { return (((a) >= 0) ? (b) : (c)); } 
#endif

//------------------------------------------------------------------------------

template< class T > constexpr  T                    x_Abs       ( const T a )                                   { return ( (a) < T{0} ? -(a) : (a) ); }
template< class T > constexpr  f32                  x_Abs       ( const f32 a )                                 { return x_FSel(a, a, -a); }
//template< typename T1, typename T2 > constexpr auto x_Min( const T1 a, const T2 b ) -> decltype( a + b )        { return ( ( a ) < ( b ) ? ( a ) : ( b ) ); }
//template< typename T1, typename T2 > constexpr auto x_Max( const T1 a, const T2 b ) -> decltype( a + b )        { return ( ( a ) > ( b ) ? ( a ) : ( b ) ); }
                    constexpr  f32                  x_Min       ( const f32 a, const f32 b )                    { return x_FSel( ((a)-(b)), b, a); }
                    constexpr  f32                  x_Max       ( const f32 a, const f32 b )                    { return x_FSel( ((a)-(b)), a, b); }
template< class T > constexpr  bool                 x_Sign      ( const T a )                                   { return a >= 0; }
template< class T > constexpr  T                    x_Sqr       ( const T a )                                   { return a * a; }
template< class T > constexpr  bool                 x_isInrange ( const T   X, const T   Min, const T   Max )   { return (Min <= X) && (X <= Max); }
                    constexpr  bool                 x_isInrange ( const f32 X, const f32 Min, const f32 Max )   { return (Min <= X) && (X <= Max); }  
template< class T > constexpr  T                    x_Range     ( const T   X, const T   Min, const T   Max )   { return ( X < Min ) ? Min : (X > Max) ? Max : X; }
template< class T > constexpr  T                    x_Lerp      ( const f32 t, const T   a,   const T   b )     { return (T)(a + t*(b-a)); }


//------------------------------------------------------------------------------
#if _X_TARGET_WINDOWS || _X_TARGET_IOS || _X_TARGET_ANDROID || _X_TARGET_MAC
    #define x_Sqrt(f)       sqrtf(f)
    #define x_SqrtF64(f)    sqrt(f)
#else
    #error x_Sqrt
#endif

//------------------------------------------------------------------------------
#ifdef TARGET_PC_32BIT
inline 
void x_SinCos( const xradian Angle, f32& S, f32& C )
{
    __asm 
    {         
        fld        Angle;
        fsincos;
        mov        eax,[C]    
        mov        edx,[S]    
        fstp       dword ptr[eax];
        fstp       dword ptr[edx];
    }
}
#elif defined(TARGET_360)
inline 
void x_SinCos( const xradian Angle, f32& S, f32& C )
{
	XMScalarSinCos(&S, &C, Angle);  
    // XMScalarSinCosEst(&s, &c, angle); 
}
#else
inline 
void x_SinCos( const xradian Angle, f32& S, f32& C )
{
    S = x_Sin( Angle );
    C = x_Cos( Angle );
}
#endif

//------------------------------------------------------------------------------
#ifdef TARGET_PC_32BIT
inline 
f32 x_InvSqrt( const f32 x )
{ 
    x_assert( x > 0.0f );
    f32 v;
    _asm
    {
            movss   xmm0, x
            rsqrtss xmm0, xmm0
            movss   x,    xmm0
    }

    return v;
}
#else
inline 
f32 x_InvSqrt( const f32 x )
{
    // Fast invert sqrt technique similar to Jim Blinn's.
    x_assert( x > 0.0f );

    const f32 x2 = x * 0.5f;
    const s32 i  = 0x5f3759df - ( *reinterpret_cast<const s32*>(&x) >> 1 );
    f32       y  = *reinterpret_cast<const f32*>(&i);

    y = y * ( 1.5f - ( x2 * y * y ) );
    y = y * ( 1.5f - ( x2 * y * y ) );

    return y;
}
#endif

//------------------------------------------------------------------------------
inline 
xradian x_ACos( const f32 X )
#ifdef TARGET_PC_32BIT
{
    f32 outX;
    _asm
    {
            fld         X               
            fld1             
            fadd        st,st(1) 
            fld1             
            fsub        st,st(2) 
            fmulp       st(1),st 
            fsqrt            
            fxch        st(1) 
            fpatan           
            fstp        outX
    }

    return xradian{ outX };
}
#else
{
    return xradian{ acosf( X ) };
}
#endif

//------------------------------------------------------------------------------
inline 
xradian x_ASin( const f32 X )
#ifdef TARGET_PC_32BIT
{
    f32 outX;
    _asm
    {
            fld         X               
            fld1             
            fadd        st,st(1)  
            fld1             
            fsub        st,st(2)  
            fmulp       st(1),st  
            fsqrt            
            fpatan        
            fstp        outX
    }

    return xradian{ outX };
}
#else
{
    return xradian{ asinf( X ) };
}
#endif

//------------------------------------------------------------------------------
inline
xradian x_ModAngle( xradian A )
{
    if( ( A > xradian{  1440.0_deg } ) || 
        ( A < xradian{ -1440.0_deg } ) )
    {
        A.m_Value = x_FMod( A.m_Value, (360.0_deg).m_Value );
    }

    while( A >= 360.0_deg )  A -= 360.0_deg;
    while( A <  0.0_deg   )  A += 360.0_deg;

    x_assert( A >= ( 0.0_deg - xradian{ 0.01f } ) && A < ( 360.0_deg + xradian{ 0.01f } ) );

    return A;
}

//------------------------------------------------------------------------------
inline
xradian x_ModAngle2( xradian A )
{
    A += xradian{ 180.0_deg };
    A  = x_ModAngle( A );
    A -= xradian{ 180.0_deg };

    return A;
}

//------------------------------------------------------------------------------
inline
xradian x_LerpAngle( const f32 t, const xradian Angle1, const xradian Angle2 )
{
    return x_ModAngle( Angle1 + x_ModAngle( Angle2 - Angle1 ) * xradian{ t } );
}

//------------------------------------------------------------------------------
inline
xradian x_MinAngleDiff( const xradian a, const xradian b )
{
    return x_ModAngle2( a - b );
}

//------------------------------------------------------------------------------
inline
f32 x_LPR( const f32 x, const f32 b )
{
    x_assert( b > 0.0f );

    f32 a = x_FMod( x, b );
    if( a < 0.0f )  
        a += b;
    return a;
}

//------------------------------------------------------------------------------

x_forceinline bool x_isValid( const f32 a )
{
    //return ( (*reinterpret_cast<const u32*>(&a)) & 0x7F800000 ) != 0x7F800000;
    return std::isfinite(a) && a == a ; //a != INFINITY && a != -INFINITY && a == a; 
}

//------------------------------------------------------------------------------
inline 
f32 x_Floor( const f32 a )
{
    f32_forge f;
    u32       s;

    // Watch out for trouble!
    x_assert( x_isValid(a) );

    // Watch out for 0!
    if( a == 0.0f )  return 0.0f;

    // We need the exponent...
    f.m_F32 = a;
    const s32 e = static_cast<s32>( ((f.m_Bits & 0x7F800000) >> 23) - 127 );

    // Extremely large numbers have no precision bits available for fractional
    // data, so there is no work to be done.
    if( e > 23 )  return a ;

    // We need the sign bit.
    s = f.m_Bits & 0x80000000;

    // Numbers between 1 and -1 must be handled special.
    if( e < 0 )  return s ? -1.0f : 0.0f ;

    // Mask out all mantissa bits used in storing fractional data.
    f.m_Bits &= (0xffffffff << (23-e));

    // If value is negative, we have to be careful.
    // And otherwise, we're done!
    if( s && (a < f.m_F32) ) return f.m_F32 - 1.0f;

    return f.m_F32 ;
}

//------------------------------------------------------------------------------
inline 
f32 x_Ceil( const f32 a )
{
    f32_forge f;
    u32       s;

    // Watch out for trouble!
    x_assert( x_isValid(a) );

    // Watch out for 0!
    if( a == 0.0f )  return 0.0f;

    // We need the exponent...
    f.m_F32 = a;
    const s32 e = static_cast<s32>( ((f.m_Bits & 0x7F800000) >> 23) - 127 );

    // Extremely large numbers have no precision bits available for fractional
    // data, so there is no work to be done.
    if( e > 23 )  return a ;

    // We need the sign bit.
    s = f.m_Bits & 0x80000000;

    // Numbers between 1 and -1 must be handled special.
    if( e < 0 )  return s ? 0.0f : 1.0f ;

    // Mask out all mantissa bits used in storing fractional data.
    f.m_Bits &= (0xffffffff << (23-e));

    // If value is positive, we have to be careful.
    // And otherwise, we're done!
    if( !s && (a > f.m_F32) ) return f.m_F32 + 1.0f;
    return f.m_F32;
}

//------------------------------------------------------------------------------

inline
s32 x_LRound( const f32 x )
{
#if _X_SSE4_SUPPORT
    return static_cast<s32>(_mm_cvtss_si32(_mm_set_ss(x)));
#else
    return static_cast<s32>(x + 0.5f);
#endif
}

//------------------------------------------------------------------------------
inline 
f32 x_Round( const f32 a, const f32 b )
{
    x_assert( !x_isInrange( b, -0.00001f, 0.00001f ) );

    const f32 Quotient = a / b;

    if( Quotient < 0.0f ) return x_Ceil ( Quotient - 0.5f ) * b;

    return x_Floor( Quotient + 0.5f ) * b;
}

//------------------------------------------------------------------------------
inline
bool x_SolvedQuadraticRoots( f32& Root1, f32& Root2, f32 a, f32 b, f32 c )
{
    if( a == 0.0f ) 
    {
        // It's not a quadratic, only one root: bx + c = 0
        if(b != 0.0f) 
        {
            Root1 = -c / b;
            Root2 = Root1;
            return true;
        }
         
        // There are no roots!
        return false;
    }

    if( b == 0.0f ) 
    {
        // We won't be able to handle this case after rearranging the formula.
        // ax^2 + c = 0
        // x = +-sqrt(-c / a)
        const f32 neg_c_over_a = -c / a;

        if( neg_c_over_a >= 0.0f ) 
        {
            Root1 = x_Sqrt( neg_c_over_a );
            Root2 = -Root1;
            return true;

        }
         
        // No real roots.
        return false;
    }

    // If b^2 >> 4ac, then "loss of significance" can occur because two numbers that are
    // almost the same are subtracted from each other.  If b^2 >> 4ac, then x is roughly equal
    // to -b +- |b| / 2a.  If b > 0 then "loss of significance" can happen to the root -b + |b| / 2a,
    // if b < 0 then it happens to the root -b - |b| / 2a.  We want to choose the better root and
    // derive the other root from it.  If you factor out a from ax^2 + bx + c = 0, then a(x^2 + xb/a + c/a) = 0
    // and a(x - r1)(x - r2) = 0 so r1 * r2 = c/a!  If we have one root, we can figure the other one out!!
    // We want to avoid an if statement on the sign of b, so factor it out:
    //		r1,r2 = (b / 2a)(-1 +- sqrt(1 - 4ac / b^2))
    //		A = b / 2a
    //		B = 4ac / b^2 (3 multiplies, 1 divide...can we do better?? YES!)
    //		  = 2c / b * 2a / b = 2c / b * 1 / A = 2c / 2aA * 1 / A 
    //		  = c / aA^2 (2 multiplies, 1 divide)
    //		C = -1 - sqrt(1 - B)
    // The good root is then:
    //		r1 = AC
    //	And the other root is:
    //		r1r2 = c / a
    //		r2 = c / ar1 = c / aAC = (A / A)(c / aAC) = Ac / aA^2C 
    //			= AB / C

    // A: part_1
    f32 denom = 2.0f * a;
    if(denom == 0.0f) 
    {
        return false;
    }

    f32 const part_1 = b / denom;

    // B: part_2
    denom = a * part_1 * part_1;
    if(denom == 0.0f) 
    {
        return false;
    }

    const f32 part_2 = c / denom;

    // C: part_3
    const f32 one_minus_part_2 = 1.0f - part_2;
    if( one_minus_part_2 < 0.0f ) 
    {
        return false;
    }

    const f32 part_3 = -1.0f - x_Sqrt(one_minus_part_2);
    x_assert( part_3 < 0.0f );

    // The good root.
    Root1 = part_1 * part_3;

    // The other root.
    Root2 = (part_1 * part_2) / part_3;

    return true;
}

//------------------------------------------------------------------------------
inline 
bool x_FEqual( const f32 f0, const f32 f1, const f32 tol ) 
{
    const f32 f = f0-f1;
    return ((f>(-tol)) && (f<tol));
}

//------------------------------------------------------------------------------
constexpr 
bool x_FLess( const f32 f0, const f32 f1, const f32 tol ) 
{
    return ((f0-f1)<tol);
}

//------------------------------------------------------------------------------
constexpr 
bool x_FGreater( const f32 f0, const f32 f1, const s32 tol ) 
{
    return ((f0-f1)>tol);
}

//==============================================================================
// SOME PRIVATE FUNCTIONS
//==============================================================================
void x_MathInit( void );
void x_MathKill( void );

