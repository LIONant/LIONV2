//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//-------------------------------------------------------------------------------------------

template<typename... T_ARGS> constexpr  
xarg_list::xarg_list( const int Count, const T_ARGS&... Args ) noexcept : 
    m_Count { Count },
    m_lArgs {{ {arg(Args)}... }}
{ 
    static_assert( sizeof...(T_ARGS) < t_max_args, "Too many arguments!" );
    x_assert( sizeof...(T_ARGS) == Count );
}

//-------------------------------------------------------------------------------------------

template<typename... T_ARGS> x_inline 
xarg_list& xarg_list::setup( const T_ARGS&... Args ) noexcept
{
    static_assert( sizeof...(T_ARGS) < t_max_args, "Too many arguments!" );
    m_Count = sizeof...(T_ARGS);
    m_lArgs = xarray<arg, t_max_args>{ arg(Args)... };
    return *this;
}

//-------------------------------------------------------------------------------------------

x_inline  
u64 xarg_list::arg::getGenericInt ( void ) const noexcept
{ 
    x_assert(isInt()); 
    switch(m_Type)
    {
        case xarg_list_args::type::U8 : return (u64)(u64)*reinterpret_cast<const u8*> (m_pValue);
        case xarg_list_args::type::S8 : return (u64)(s64)*reinterpret_cast<const s8*> (m_pValue);
        case xarg_list_args::type::U16: return (u64)(u64)*reinterpret_cast<const u16*>(m_pValue);
        case xarg_list_args::type::S16: return (u64)(s64)*reinterpret_cast<const s16*>(m_pValue);
        case xarg_list_args::type::U32: return (u64)(u64)*reinterpret_cast<const u32*>(m_pValue);
        case xarg_list_args::type::S32: return (u64)(s64)*reinterpret_cast<const s32*>(m_pValue);
        case xarg_list_args::type::U64: return (u64)(u64)*reinterpret_cast<const u64*>(m_pValue);
        case xarg_list_args::type::S64: return (u64)(s64)*reinterpret_cast<const s64*>(m_pValue);
        default:
            x_assume( false );
    }
    x_assume( false );
    return 0;
}

//-------------------------------------------------------------------------------------------

x_inline  
f64 xarg_list::arg::getGenericFloat ( void ) const noexcept
{ 
    x_assert(isFloat()); 
    switch(m_Type)
    {
        case type::F32: return *reinterpret_cast<const f32*>(m_pValue);
        case type::F64: return *reinterpret_cast<const f64*>(m_pValue);
        default:
            x_assume( false );
    }
    x_assume( false );
    return 0;
}

//-------------------------------------------------------------------------------------------
x_inline  
const xchar* xarg_list::arg::getGenericCharString ( void ) const noexcept
{ 
    x_assert(isString()); 
    switch(m_Type)
    {
        case type::CHAR_PTR: return *reinterpret_cast<char* const*>(m_pValue);
        case type::XSTRING:  return *reinterpret_cast<const xstring*>(m_pValue);
        default:
            x_assume( false );
    }
    x_assume( false );
    return 0;
}
