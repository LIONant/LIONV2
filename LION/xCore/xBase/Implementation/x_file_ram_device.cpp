//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"

//==============================================================================
//  MEMORY FILE CLASS
//==============================================================================
//  xmemfile 
//      xmemfile is a class that contains the interface to access the memory files.
//      currently, it is implemented as an array of xmemblock's of the size
//      xmemblock::block::SIZE.
//==============================================================================

class xmemfile
{
public:

    bool    Read            (       void* pBuffer, const u64 Count )        noexcept;  
    void    Write           ( const void* pBuffer, const u64 Count )        noexcept;
    void    SeekOrigin      ( s64 Offset )                                  noexcept { m_SeekPosition  = Offset;            }
    void    SeekCurrent     ( s64 Offset )                                  noexcept { m_SeekPosition += Offset;            }
    void    SeekEnd         ( s64 Offset )                                  noexcept { m_SeekPosition  = m_EOF - Offset;    }
    s64     Tell            ( void )                                        noexcept { return m_SeekPosition;               }
    bool    isEOF           ( void )                                        noexcept { return m_SeekPosition > m_EOF;       }
    s32     getC            ( void )                                        noexcept;
    void    putC            ( s32 C )                                       noexcept;
    s64     getFileLength   ( void )                                        noexcept { return m_EOF;                        } 

protected:

    using xmemblock = xarray< xbyte, 1024 * 10 >;

protected:

    xphvector<xmemblock,64>     m_Block;
    s64                         m_SeekPosition  { 0 };
    s64                         m_EOF           { 0 };    
};

//------------------------------------------------------------------------------

class xfile_ram_device final : public xfile_device_i
{
public:
                    xfile_ram_device ( void ) : xfile_device_i{ X_STR("ram:") }{}

protected:

    virtual         void*               Open            ( const xwstring& FileName, access_types Flags )        noexcept override { return x_new( xmemfile );    }
    virtual         void                Close           ( void* pFile )                                         noexcept override { x_assert(pFile); x_delete( reinterpret_cast<xmemfile*>(pFile) ); }
    virtual         bool                Read            ( void* pFile, void* pBuffer, u64 Count )               noexcept override { return x_assert(pFile), x_assert(pBuffer), reinterpret_cast<xmemfile*>(pFile)->Read( pBuffer, Count ); }
    virtual         void                Write           ( void* pFile, const void* pBuffer, u64 Count )         noexcept override { return x_assert(pFile), x_assert(pBuffer), reinterpret_cast<xmemfile*>(pFile)->Write( pBuffer, Count ); }
    virtual         void                Seek            ( void* pFile, seek_mode Mode, s64 Pos )                noexcept override ;
    virtual         s64                 Tell            ( void* pFile )                                         noexcept override { return x_assert(pFile), reinterpret_cast<xmemfile*>(pFile)->Tell(); }
    virtual         void                Flush           ( void* pFile )                                         noexcept override { x_assert(pFile); }
    virtual         u64                 Length          ( void* pFile )                                         noexcept override { x_assert(pFile); return reinterpret_cast<xmemfile*>(pFile)->getFileLength(); }
    virtual         bool                isEOF           ( void* pFile )                                         noexcept override { x_assert(pFile); return reinterpret_cast<xmemfile*>(pFile)->isEOF(); }
    virtual         sync_state          Synchronize     ( void* pFile, bool bBlock )                            noexcept override { x_assert(pFile); return xfile::sync_state::COMPLETED; }
    virtual         void                AsyncAbort      ( void* pFile )                                         noexcept override { x_assert(pFile); }
    virtual         const xwchar*       getTempPath     ( void )                                        const   noexcept override { static const xwchar* temp = X_WSTR("Temp:/").m_pValue; return temp; }
};

//------------------------------------------------------------------------------

void xfile_ram_device::Seek( void* pFile, seek_mode Mode, s64 Pos ) noexcept
{
    x_assert(pFile);
    xmemfile* pMemFile = (xmemfile*)pFile;

    switch( Mode )
    {
        case xfile_device_i::SKM_ORIGIN: pMemFile->SeekOrigin( Pos ); break;
        case xfile_device_i::SKM_CURENT: pMemFile->SeekCurrent( Pos ); break;
        case xfile_device_i::SKM_END:    pMemFile->SeekEnd( Pos ); break;
        default: x_assert(0); break;
    }
}

//------------------------------------------------------------------------------

bool xmemfile::Read( void* pBuffer, const u64 Count ) noexcept
{
    if( m_SeekPosition >= m_EOF )
        return false;

    auto currentBlockIndex  = m_SeekPosition / xmemblock::t_count;
    auto currentBlockOffset = m_SeekPosition % xmemblock::t_count;
    u64 bufferOffset = 0;

    x_assert( currentBlockIndex >= 0 && currentBlockIndex < m_Block.getCount() );
    
    while( bufferOffset < Count )
    {
        // Copy data from blocks.
        reinterpret_cast<xbyte*>(pBuffer)[bufferOffset] = m_Block[currentBlockIndex][currentBlockOffset];
        currentBlockOffset++;
        bufferOffset++;
        m_SeekPosition++;
        if( currentBlockOffset >= xmemblock::t_count )
        {
            // Since we've completed the current block, increment to next block.
            currentBlockIndex++;
            currentBlockOffset = 0;

            //ASSERT(currentBlockIndex < m_Block.getCount());
            if (currentBlockIndex >= m_Block.getCount()) 
                return true;
        }
    }

    return true;
}

//------------------------------------------------------------------------------

void xmemfile::Write( const void* pBuffer, const u64 Count ) noexcept
{
    // Check current position and size of data being added.
    const s64 NewDataPosition = m_SeekPosition + Count;
    if( m_EOF < NewDataPosition ) m_EOF = NewDataPosition;

    // If we need to allocate more memory for the blocks, then do so.
    if (m_EOF >= (m_Block.getCount<s64>() * static_cast<s64>(xmemblock::t_count) ) )
    {
        auto NumBlocksRequired = m_EOF/xmemblock::t_count + 1;
        NumBlocksRequired -= m_Block.getCount();

        // Allocate list of blocks
        for( int i=0; i<NumBlocksRequired; i++ ) m_Block.pop(  );
    }

    auto currentBlockIndex  = m_SeekPosition / xmemblock::t_count;
    auto currentBlockOffset = m_SeekPosition % xmemblock::t_count;
    u64 bufferOffset = 0;

    x_assert( currentBlockIndex >= 0 && currentBlockIndex < m_Block.getCount() );
    
    while (bufferOffset < Count)
    {
        // Copy data into blocks.
        m_Block[currentBlockIndex][currentBlockOffset] = reinterpret_cast<const xbyte*>(pBuffer)[bufferOffset];

        bufferOffset++;
        currentBlockOffset++;
        m_SeekPosition++;

        if (currentBlockOffset >= xmemblock::t_count )
        {
            // Since we've completed the current block, increment to next block.
            currentBlockIndex++;
            currentBlockOffset = 0;
            x_assert( currentBlockIndex < m_Block.getCount() );
        }
    }
}

//------------------------------------------------------------------------------

s32 xmemfile::getC( void ) noexcept
{
    if ( m_SeekPosition > m_EOF )
        return xfile::XEOF ;

    const auto currentBlockIndex  = m_SeekPosition / xmemblock::t_count;
    const auto currentBlockOffset = m_SeekPosition % xmemblock::t_count;
    
    m_SeekPosition++;
    return x_static_cast<s32>( m_Block[currentBlockIndex][currentBlockOffset] );
}

//------------------------------------------------------------------------------

void xmemfile::putC( s32 C ) noexcept           
{
    if ( m_SeekPosition > m_EOF )
        m_EOF = m_SeekPosition;

    // If we need to allocate more memory for the blocks, then do so.
    if ( m_EOF >= m_Block.getCount<s64>() * static_cast<s64>(xmemblock::t_count) )
    {
        // Make sure we don't have this strange degenerate case.
        x_assert(xmemblock::t_count > 1 );

        // Add one block, since it is just a char being added, 
        // it should never add more than one block.
        if ( m_Block.getCount() == 0 )
        {
            m_Block.pop();
        }
    }

    const auto currentBlockIndex  = m_SeekPosition / xmemblock::t_count;
    const auto currentBlockOffset = m_SeekPosition % xmemblock::t_count;
    
    m_SeekPosition++;
    m_Block[currentBlockIndex][currentBlockOffset] = x_static_cast<xbyte>( C );
}

//------------------------------------------------------------------------------

xfile_device_i* __x_file_new_ram_device( void ){ return x_new(xfile_ram_device); }

