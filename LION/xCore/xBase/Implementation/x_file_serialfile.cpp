//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"

//------------------------------------------------------------------------------
static
void* MemoryAllocaterDefaultFunction( xuptr Size, xserialfile::mem_type Flags ) noexcept
{
    if( Flags.m_VRAM )
    {
        // allocate default longterm vram memory
    }
    else if ( Flags.m_TEMP_MEMORY )
    {
        // deal with temp memory (system memory)
    }
    else
    {
        // default longterm system memory
    }

    return g_context::get().aligned_alloc( Size, 16 );
}

//------------------------------------------------------------------------------

xserialfile::xserialfile( void ) noexcept
{
    m_pMemoryCallback = MemoryAllocaterDefaultFunction;  
}

//------------------------------------------------------------------------------

s32 xserialfile::writting::AllocatePack( xserialfile::mem_type DefaultPackFlags ) noexcept
{
    xwstring Name; 

    Name = X_WSTR("ram:\\Whatever");

    // Create the default pack
    pack_writting& WPack = m_Packs.append();
    WPack.m_Data.Open( Name, "w+" );        
    WPack.m_PackFlags    = DefaultPackFlags;

    return m_Packs.getCount<int>()-1;
}


//------------------------------------------------------------------------------

void xserialfile::HandlePtrDetails( u8* pA, s32 SizeofA, s32 Count, xserialfile::mem_type MemoryFlags ) noexcept
{
    // If the parent is in not in a common pool then its children must also not be in a common pool.
    // The theory is that if the parent is not in a common pool it could be deallocated and if the child 
    // is in a common pool it could be left orphan. However this may need to be thought out more carefully
    // so I am playing it safe for now.
    if( m_pWrite->m_Packs[m_iPack].m_PackFlags.m_UNIQUE )
    {
        x_assert( MemoryFlags.m_UNIQUE );
    }
    else if ( m_pWrite->m_Packs[ m_iPack ].m_PackFlags.m_TEMP_MEMORY )
    {
        x_assert( MemoryFlags.m_TEMP_MEMORY );
    }

    //
    // If we don't have any elements then just write the pointer raw
    //
    if( Count == 0 )
    {
        // always write 64 bits worth (assuming user is using xserialfile::ptr)
        // if we do not do this the upper bits of the 64 bits may contain trash
        // and if the compiler is 32bits and the game 64 then that trash can crash
        // the game.
        Serialize( *((u64*)(pA)) );
        /*
             if( sizeof( void* ) == sizeof( u32 )) Serialize( *((u32*)(pA)) );
        else if( sizeof( void* ) == sizeof( u64 )) Serialize( *((u64*)(pA)) );
        else
        {
            // What the hell is the size of a pointer?
            x_assert( 0 );
        }
        */
        return;
    }

    //
    // Choose the right pack for this allocation        
    //

    // Back up the current pack
    auto BackupPackIndex = m_iPack;

    if( MemoryFlags.m_UNIQUE )
    {
        // Create a pack
        m_iPack = m_pWrite->AllocatePack( MemoryFlags );
    }
    else
    {
        // Search for a pool which matches our attributes
        s32 i;
        for( i=0; i<m_pWrite->m_Packs.getCount(); i++ )
        {
            if( (m_pWrite->m_Packs[i].m_PackFlags & ~mem_type::MASK_UNIQUE) == MemoryFlags ) 
                break;
        }

        // Could not find a pack with compatible flags so just create a new one
        if( i == m_pWrite->m_Packs.getCount() )
        {
            // Create a pack
            m_iPack = m_pWrite->AllocatePack( MemoryFlags );
        }
        else
        {
            // Set the index to the compatible pack
            m_iPack = i;
        }
    }

    // Make sure we are at the end of the buffer before preallocating
    // I have change the alignment from 4 to 8 because of 64 bits OS.
    // it may help. In the future will be nice if the user could specify the alignment.
    getW().SeekEnd(0);
    getW().AlignPutC( ' ', SizeofA * Count, 8, false );

    //
    // Store the pointer
    //
    {
        ref& Ref = m_pWrite->m_PointerTable.append();

        Ref.m_PointingAT        = x_static_cast<u32>(getW().Tell());
        Ref.m_OffsetPack        = BackupPackIndex;
        Ref.m_OffSet            = m_ClassPos + ComputeLocalOffset( pA );
        Ref.m_Count             = Count;
        Ref.m_PointingATPack    = m_iPack;

        // We better be at the write spot that we are pointing at 
        x_assert( Ref.m_PointingAT == getW().Tell() );
    }
}


//------------------------------------------------------------------------------

xfile::err xserialfile::SaveFile( void ) noexcept
{
    s32     i;
    
    //
    // Go throw all the packs and compress them
    //
    for( i = 0; i<m_pWrite->m_Packs.getCount(); i++ )
    {
        xafptr<xbyte>   RawData;
        pack_writting&  Pack        = m_pWrite->m_Packs[i];

        Pack.m_CompressSize         = 0; 
        Pack.m_UncompressSize       = x_static_cast<s32>(Pack.m_Data.getFileLength());
        Pack.m_BlockSize            = x_Min( MAX_BLOCK_SIZE, Pack.m_UncompressSize );

        // Copy the pack into a memory buffer
        RawData.Alloc( Pack.m_UncompressSize );                         // This memory it is short term we should tell the mem system about it
        Pack.m_Data.ToMemory( RawData, Pack.m_UncompressSize );

        //
        // Now compress the memory
        //
        s32 nBlocks = Pack.m_UncompressSize / Pack.m_BlockSize;
        Pack.m_CompressData.Alloc( (nBlocks + 1 )*Pack.m_BlockSize + 1024 );

        s32 j;
        for( j=0; j<nBlocks; j++ )
        {
            const auto CompressSize = x_static_cast<int>( 
                                            x_CompressMem( 
                                                Pack.m_CompressData.ViewFrom(Pack.m_CompressSize),
                                                xbuffer_view<xbyte>(
                                                    &RawData[j*Pack.m_BlockSize], 
                                                    x_static_cast<xuptr>(Pack.m_BlockSize)) 
                                                ) );
                                                
            x_assert( CompressSize > 0 );
            Pack.m_CompressSize             += CompressSize;
            m_pWrite->m_CSizeStream.append() = CompressSize;
        }

        // Do we need to add one more block?
        if( x_static_cast<u32>(nBlocks*Pack.m_BlockSize) < Pack.m_UncompressSize )
        {
            const s32 UncompressSize = Pack.m_UncompressSize - x_static_cast<u32>(nBlocks*Pack.m_BlockSize);
            const s32 CompressSize   = x_static_cast<int>(
                                            x_CompressMem( 
                                                 Pack.m_CompressData.ViewFrom( Pack.m_CompressSize ),
                                                 xbuffer_view<xbyte>( 
                                                    &RawData[j*Pack.m_BlockSize], 
                                                    x_static_cast<xuptr>(UncompressSize)) 
                                                 ) ); 
                                                                                                              
            nBlocks++;

            x_assert( CompressSize > 0 );
            Pack.m_CompressSize             += CompressSize;
            m_pWrite->m_CSizeStream.append() = CompressSize;
        }

        //
        // Close the pack file
        //
        Pack.m_Data.Close();
    }

    //
    // Take the references and the packs headers and compress them as well
    //
    xafptr<xbyte> CompressInfoData;
    s32           CompressInfoDataSize;

    {
        xafptr<xbyte> InfoData;
    
        // First update endianess 
        if( m_pWrite->m_bEndian ) 
        {
            for( i=0; i<m_pWrite->m_PointerTable.getCount(); i++ )
            {
                m_pWrite->m_PointerTable[i].m_OffSet            = x_EndianSwap( m_pWrite->m_PointerTable[i].m_OffSet );
                m_pWrite->m_PointerTable[i].m_Count             = x_EndianSwap( m_pWrite->m_PointerTable[i].m_Count );
                m_pWrite->m_PointerTable[i].m_PointingAT        = x_EndianSwap( m_pWrite->m_PointerTable[i].m_PointingAT );
                m_pWrite->m_PointerTable[i].m_OffsetPack        = x_EndianSwap( m_pWrite->m_PointerTable[i].m_OffsetPack );
                m_pWrite->m_PointerTable[i].m_PointingATPack    = x_EndianSwap( m_pWrite->m_PointerTable[i].m_PointingATPack );
            }

            for( i=0; i<m_pWrite->m_Packs.getCount(); i++ )
            {
                m_pWrite->m_Packs[i].m_PackFlags.m_Value = x_EndianSwap( m_pWrite->m_Packs[i].m_PackFlags.m_Value );
                m_pWrite->m_Packs[i].m_UncompressSize    = x_EndianSwap( m_pWrite->m_Packs[i].m_UncompressSize );
            }
            
            for( i=0; i<m_pWrite->m_CSizeStream.getCount(); i++ )
            {
                m_pWrite->m_CSizeStream[i] = x_EndianSwap( m_pWrite->m_CSizeStream[i] );
            }
        }

        // Allocate all the memory that we will need
        InfoData.Alloc( sizeof(pack) * m_pWrite->m_Packs.getCount()         + 
                        sizeof(ref)  * m_pWrite->m_PointerTable.getCount()  +
                        sizeof(s32)  * m_pWrite->m_CSizeStream.getCount()  );

        pack*   pPack           = (pack*)  &InfoData[0];
        ref*    pRef            = (ref*)   &pPack   [m_pWrite->m_Packs.getCount()];
        s32*    pBlockSizes     = (s32*)   &pRef    [m_pWrite->m_PointerTable.getCount() ];

        CompressInfoData.Alloc( InfoData.getCount() );

#ifdef _X_DEBUG
        CompressInfoData.MemSet(0xBE);
#endif
        
        
        // Now copy all the info starting with the packs
        for( i=0; i <m_pWrite->m_Packs.getCount(); i++ )
        {
            pPack[i] = m_pWrite->m_Packs[i];
        }

        // Now we copy all the references
        for( i=0; i <m_pWrite->m_PointerTable.getCount(); i++ )
        {
            pRef[i] = m_pWrite->m_PointerTable[i];
        }

        // Now we copy all the block sizes
        for( i=0; i <m_pWrite->m_CSizeStream.getCount(); i++ )
        {
            pBlockSizes[i] = m_pWrite->m_CSizeStream[i];
        }

        // 
        // to compress it
        //
        CompressInfoDataSize = x_static_cast<int>( x_CompressMem( CompressInfoData, InfoData ) );
    }

    //
    // Fill up all the header information
    //
    m_Header.m_SerialFileVersion  = VERSION_ID;       // Major and minor version ( version pattern helps Identify file format as well)

    m_Header.m_nPacks             = m_pWrite->m_Packs.getCount<u16>();
    m_Header.m_nPointers          = m_pWrite->m_PointerTable.getCount<u16>();
    m_Header.m_nBlockSizes        = m_pWrite->m_CSizeStream.getCount<u16>();
    m_Header.m_SizeOfData         = 0;
    m_Header.m_PackSize           = CompressInfoDataSize;
    m_Header.m_AutomaticVersion   = m_ClassSize;

    header  Header;

    if( m_pWrite->m_bEndian )
    {
        Header.m_SerialFileVersion = x_EndianSwap( m_Header.m_SerialFileVersion );    
        Header.m_PackSize          = x_EndianSwap( m_Header.m_PackSize );             
        Header.m_MaxQualities      = x_EndianSwap( m_Header.m_MaxQualities );           
        Header.m_SizeOfData        = x_EndianSwap( m_Header.m_SizeOfData );
        Header.m_nPointers         = x_EndianSwap( m_Header.m_nPointers );               
        Header.m_nPacks            = x_EndianSwap( m_Header.m_nPacks );               
        Header.m_nBlockSizes       = x_EndianSwap( m_Header.m_nBlockSizes );          
        Header.m_ResourceVersion   = x_EndianSwap( m_Header.m_ResourceVersion );
        Header.m_AutomaticVersion  = x_EndianSwap( m_Header.m_AutomaticVersion );
    }
    else
    {
        Header = m_Header;
    }

    //
    // Save everything into a file
    //
    const auto Pos = m_pWrite->m_pFile->Tell();

    m_pWrite->m_pFile->WriteList( xbuffer_view<xbyte>( reinterpret_cast<xbyte*>(&Header), sizeof(Header) ) );
    m_pWrite->m_pFile->WriteList( CompressInfoData.ViewTo( CompressInfoDataSize ) );

    for( i=0; i<m_pWrite->m_Packs.getCount(); i++ )
    {
        pack_writting&  Pack        = m_pWrite->m_Packs[i];
    
        if( m_pWrite->m_bEndian ) 
        {
            m_pWrite->m_pFile->WriteList( Pack.m_CompressData.ViewTo( x_EndianSwap( Pack.m_CompressSize ) ) );
        }
        else
        {
            m_pWrite->m_pFile->WriteList( Pack.m_CompressData.ViewTo( Pack.m_CompressSize ) );
        }
    }

    // Write the size of the data
    Header.m_SizeOfData = x_static_cast<u32>( m_pWrite->m_pFile->Tell() - Pos - sizeof(header) );
    m_pWrite->m_pFile->SeekOrigin( Pos + offsetof( header, m_SizeOfData ) );

    if( m_pWrite->m_bEndian )
    {
        Header.m_SizeOfData         = x_EndianSwap( Header.m_SizeOfData );
    }

    m_pWrite->m_pFile->Write( Header.m_SizeOfData );

    // Go to the end of the file
    m_pWrite->m_pFile->SeekEnd(0);

    return x_error_ok();
}



//------------------------------------------------------------------------------

xfile::err xserialfile::LoadHeader( xfile& File, s32 SizeOfT ) noexcept
{
    //
    // Check signature (version is encoded in signature)
    //
    auto Err = File.ReadView( xbuffer_view<xbyte>( reinterpret_cast<xbyte*>(&m_Header), sizeof(m_Header) ) );
    File.Synchronize( true );
    if( m_Header.m_SerialFileVersion != VERSION_ID )
    {
        if( x_EndianSwap( m_Header.m_SerialFileVersion ) == VERSION_ID )
        {
            return x_error_code( xfile::errors, ERR_FAILURE, "File can not be read. Probably it has the wrong endian." );
        }

        return x_error_code( xfile::errors, ERR_FAILURE, "Unknown file format (Could be an older version of the file format)" );
    }
    
    if( m_Header.m_AutomaticVersion != SizeOfT )
    {
        return x_error_code( xfile::errors, ERR_FAILURE, "The size of the structure that was used for writing this file is different from the one reading it");
    }

    return Err;
}

//------------------------------------------------------------------------------

void* xserialfile::LoadObject( xfile& File ) noexcept
{
    xafptr<xbyte>             InfoData;                   // Buffer which contains all those arrays
    xafptr<decompress_block>  ReadBuffer;
    s32                       iCurrentBuffer = 0;
    
    //
    // Allocate the read temp double buffer
    //
    ReadBuffer.Alloc( 2 );
    
    //
    // Read the refs and packs
    //
    {
        // Create uncompress buffer for the packs and references
        const s32 DecompressSize = m_Header.m_nPacks      * sizeof(pack)  + 
                                   m_Header.m_nPointers   * sizeof(ref)   +
                                   m_Header.m_nBlockSizes * sizeof(s32);

        // InfoData.Alloc( x_Max( (s32)m_Header.m_PackSize, DecompressSize ) + m_Header.m_nPacks * sizeof(xbyte**) );
        
        x_assert( m_Header.m_PackSize <= DecompressSize );
        InfoData.Alloc( DecompressSize + m_Header.m_nPacks * sizeof(xbyte**) );

        // Uncompress in place for packs and references
        if( m_Header.m_PackSize < DecompressSize )
        {
            xafptr<char>  CompressData;
            
            CompressData.Alloc( m_Header.m_PackSize );
            
            File.ReadList( CompressData );
            File.Synchronize( true );
            
            x_DecompressMem( xbuffer_view<xbyte>( InfoData, DecompressSize), xbuffer_view<xbyte>( reinterpret_cast<xbyte*>(&CompressData[0]), m_Header.m_PackSize) );
        }
        else
        {
            File.ReadView( xbuffer_view<xbyte>( &InfoData[0], m_Header.m_PackSize ) );
            File.Synchronize( true );
        }
    }
    
    //
    // Set up the all the pointers
    //
    const pack* const           pPack           = reinterpret_cast<const pack*>     ( &InfoData    [0]                      );
    const ref*  const           pRef            = reinterpret_cast<const ref*>      ( &pPack       [m_Header.m_nPacks]      );
    const s32*  const           pBlockSizes     = reinterpret_cast<const s32*>      ( &pRef        [m_Header.m_nPointers]   );
    xbyte** const               pPackPointers   = const_cast<xbyte**>( reinterpret_cast<const xbyte*const*>( &pBlockSizes [m_Header.m_nBlockSizes] ) );

    //
    // Start the reading and decompressing of the packs
    //
    {
        s32 iBlock = 0;

        for( s32 iPack=0; iPack<m_Header.m_nPacks; iPack++ )
        {
            const pack&     Pack            = pPack[iPack];
            s32             nBlocks         = 0;
            s32             ReadSoFar       = 0;

            // Start reading block immediately
            // Note that the rest of the first block of a pack is interleave with the last pack last block
            // except for the very first one (this one)
            if( iPack == 0 )
            {
                File.ReadView( xbuffer_view<xbyte>( reinterpret_cast<xbyte*>(&ReadBuffer[ iCurrentBuffer ]), pBlockSizes[iBlock] ) );
            }

            // Allocate the size of this pack
            pPackPointers[iPack] = (xbyte*)m_pMemoryCallback( Pack.m_UncompressSize, Pack.m_PackFlags );
            
            // Store a block that is mark as temp (can/should only be one)
            if ( Pack.m_PackFlags.m_TEMP_MEMORY )
            {
                x_assert( m_pTempBlockData == nullptr );
                m_pTempBlockData = pPackPointers[ iPack ];
            }

            // Make sure that is just one that we need to read
            if( Pack.m_UncompressSize > MAX_BLOCK_SIZE )
            {
                nBlocks   = (s32)(Pack.m_UncompressSize / MAX_BLOCK_SIZE);
                nBlocks  += ((nBlocks*MAX_BLOCK_SIZE) == Pack.m_UncompressSize)?0:1;
            }

            // Read each block
            for( s32 i=1; i<nBlocks; i++ )
            {
                iCurrentBuffer = !iCurrentBuffer;
                iBlock++;

                // Start reading the next block
                File.Synchronize( true );
                File.ReadView( xbuffer_view<xbyte>{ reinterpret_cast<xbyte*>( &ReadBuffer[ iCurrentBuffer ] ), x_static_cast<xuptr>( pBlockSizes[iBlock] ) } );

                //
                // Start the decompressing at the same time
                //
                x_assume( pPackPointers[iPack] );
                x_DecompressMem( xbuffer_view<xbyte>( &pPackPointers[iPack][ReadSoFar], MAX_BLOCK_SIZE ), 
                                 xbuffer_view<xbyte>( reinterpret_cast<xbyte*>( &ReadBuffer[ !iCurrentBuffer ]), pBlockSizes[iBlock-1] ) );
                
                ReadSoFar += MAX_BLOCK_SIZE;
            }

            // Finish reading the block
            File.Synchronize( true );

            // Interleave next pack block with this last pack block
            if( (iPack+1)<m_Header.m_nPacks )
            {
                File.ReadView( xbuffer_view<xbyte>( reinterpret_cast<xbyte*>(&ReadBuffer[ !iCurrentBuffer ]), pBlockSizes[iBlock+1] ) );
            }

            //
            // Decompress last block for this pack
            //
            x_assume( pPackPointers[iPack] );
            x_DecompressMem( xbuffer_view<xbyte>( &pPackPointers[iPack][ReadSoFar], Pack.m_UncompressSize - ReadSoFar ), 
                             xbuffer_view<xbyte>( reinterpret_cast<xbyte*>(&ReadBuffer[iCurrentBuffer]), pBlockSizes[iBlock] ) );

            // Get ready for next block
            iCurrentBuffer = !iCurrentBuffer;
            iBlock++;
        }
    }

    //
    // Resolve pointers
    //
    for( s32 i=0; i<m_Header.m_nPointers; i++ )
    {
        const ref&      Ref       = pRef[i];
        void* const     pSrcData  = &pPackPointers[ Ref.m_PointingATPack ][Ref.m_PointingAT];
        xdataptr<void>* pDestData = reinterpret_cast<xdataptr<void>*>( &pPackPointers[ Ref.m_OffsetPack ][Ref.m_OffSet] );
        
        pDestData->m_pPtr = pSrcData;        
    }

    // Return the basic pack
    return pPackPointers[0];
}
