//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      general - An allocator object is require and it will handle how to allocate memory
//------------------------------------------------------------------------------
template< typename T_ENTRY, template<typename,typename> class T_ALLOCATOR, typename T_COUNTER = xuptr >
class x_ll_fixed_pool_general 
{
    x_object_type( x_ll_fixed_pool_general, is_quantum_lock_free, is_not_copyable, is_not_movable );

    public:     using                           t_entry         = T_ENTRY;
    public:     enum {                          t_aligment      = x_Max( alignof(x_atomic<void*>), alignof(t_entry) ) };
    protected:  struct alignas( t_aligment )    t_real_node; 
    public:     using                           t_allocator     = T_ALLOCATOR< t_real_node, T_COUNTER >;
    public:     using                           t_counter       = typename t_allocator::t_counter;
    
public:

    x_forceinline x_ll_fixed_pool_general( bool bClear = true ) noexcept { if( bClear ) Clear(); }
    
    inline void Clear ( void ) noexcept  
    {
        const t_counter Count = m_Allocator.getCount() - 1;
        
        for( t_counter i = 0; i<Count; i++ )
        {
            m_Allocator[i].m_Next.store( &m_Allocator[i + 1], std::memory_order_relaxed );
        }
        
        m_Allocator[Count].m_Next.store( nullptr, std::memory_order_relaxed );
        m_Head.store( &m_Allocator[0] );
    }
    
    inline t_entry* pop ( void ) noexcept  
    {
        t_real_node* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            if( m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_Next, std::memory_order_acquire, std::memory_order_relaxed ) )
            {
                //SanityCheck();
                return &pLocalReality->m_Entry;
            }
        }
        return nullptr;
    }
    
    inline void push ( t_entry& userEntry ) noexcept  
    {
        t_real_node& Node            = *reinterpret_cast<t_real_node*>(&(reinterpret_cast<xbyte*>(&userEntry)[-ENTRY_OFFSET]));
        t_real_node* pLocalReality   = m_Head.load( std::memory_order_relaxed );
        do
        {
            Node.m_Next.store( pLocalReality, std::memory_order_relaxed );
        } while( !m_Head.compare_exchange_weak( pLocalReality, &Node, std::memory_order_release, std::memory_order_relaxed ) );

        //SanityCheck();
    }
    
    inline t_entry& get ( const xuptr Index ) noexcept { return m_Allocator[Index].m_Entry; }
    
    constexpr   bool                    Belongs         ( const void* pPtr )    const   noexcept { return m_Allocator.Belongs(pPtr);        }

    inline void SanityCheck( void )
    {
        // Walk the link list to make sure everything is ok
        t_real_node* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            pLocalReality = pLocalReality->m_Next;
        }
    }

protected:
    
    struct alignas( t_aligment ) t_real_node 
    {
        x_atomic<t_real_node*>      m_Next      {};
        t_entry                     m_Entry     {};
    };
    
    enum : s32 { ENTRY_OFFSET = offsetof( t_real_node, m_Entry ) };
    
protected:
    
    x_atomic<t_real_node*>          m_Head         { nullptr };
    t_allocator                     m_Allocator    {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      general - An allocator object is require and it will handle how to allocate memory
//      jitc    - ( Just In Time Construction/Destruction )
//                  Means that when an entry is pop then we will constructed and push destruct it
// Algorithm:
//      Note the algorithm reuses the entry memory as a pointer to the next node.
//------------------------------------------------------------------------------
template< typename T_ENTRY, template<typename,typename> class T_ALLOCATOR >
class x_ll_fixed_pool_general_jitc 
{
    x_object_type( x_ll_fixed_pool_general_jitc, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using t_allocator   = T_ALLOCATOR< T_ENTRY, xuptr >;
    using t_entry       = typename t_allocator::t_entry;
    using t_counter     = typename t_allocator::t_counter;

public:

    x_forceinline x_ll_fixed_pool_general_jitc( bool bClear = true ) noexcept { if( bClear ) Clear(); }
    
    ~x_ll_fixed_pool_general_jitc( void )
    {
        DestructAllocatedNodes();
    }

    inline void DestructAllocatedNodes( void )
    {
        // Check
        const t_counter Count = m_Allocator.getCount();
        if( Count == 0 ) return;

        xafptr<bool>    Checks;

        // Alloc and clear the memory to mark what it has been freed so far
        Checks.Alloc( Count );
        Checks.MemSet( false );
        
        // Mark all nodes that have been free so far
        for( head Next = m_Head; Next.load(); Next.store( Next.load()->m_Next ) )
        {
            const auto Index = static_cast<int>( reinterpret_cast<t_entry*>(Next.load()) - reinterpret_cast<t_entry*>( &m_Allocator[0] ) );
            
            // make sure there is not strange situations
            x_assert( Checks[ Index ] == false );

            // mark the node as free
            Checks[ Index ] = true;
        }

        // Destruct all nodes that have not been freed by the user
        // These nodes could be consider a leak....
        for( int i = 0; i < Count; i++ )
        {
            if( Checks[i] == false )
            {
                x_destruct( &m_Allocator[i] );
            }
        }
    }


    inline void Clear ( void ) noexcept 
    {
        if( m_Allocator.getCount() == 0 ) return;

        const t_counter Count = m_Allocator.getCount() - 1;
        
        for( t_counter i = 0; i<Count; i++ )
        {
            reinterpret_cast<next*>(&m_Allocator[i])->m_Next.store( reinterpret_cast<next*>(&m_Allocator[i + 1]), std::memory_order_relaxed );
        }
        
        reinterpret_cast<next*>(&m_Allocator[Count])->m_Next.store( nullptr, std::memory_order_relaxed );
        m_Head.store( reinterpret_cast<next*>(&m_Allocator[0]), std::memory_order_relaxed );
    }

    inline t_entry* popDontConstruct( void ) noexcept 
    {
        next* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            if( m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_Next.load(std::memory_order_relaxed), std::memory_order_acquire, std::memory_order_relaxed ) )
            {
                t_entry* pEntry = reinterpret_cast<t_entry*>(pLocalReality);
                return pEntry;
            }
        }
        return nullptr;
    }
    
    template<typename ...T_ARG>
    inline t_entry* pop ( T_ARG&&...Args ) noexcept 
    {
        next* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            if( m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_Next.load(std::memory_order_relaxed), std::memory_order_acquire, std::memory_order_relaxed ) )
            {
                t_entry* pEntry = reinterpret_cast<t_entry*>(pLocalReality);
                (void)x_construct( t_entry, pEntry, { std::forward<T_ARG>(Args)... } );
                return pEntry;
            }
        }
 
        return nullptr;
    }
    
    inline void pushDontDestruct ( t_entry& Node ) noexcept 
    {
        next* pLocalReality   = m_Head.load( std::memory_order_relaxed );
        do
        {
            reinterpret_cast<next*>(&Node)->m_Next.store( pLocalReality, std::memory_order_relaxed );
        } while( !m_Head.compare_exchange_weak( pLocalReality, reinterpret_cast<next*>(&Node), std::memory_order_release, std::memory_order_relaxed ) );
    }
    
    inline void push ( t_entry& Node ) noexcept 
    {
        x_destruct( &Node );
        pushDontDestruct( Node );
    }

    inline      t_entry&                get             ( xuptr Index )                 noexcept { return m_Allocator[Index].m_Entry;       }
    inline      t_allocator&            getAllocator    ( void )                        noexcept { return m_Allocator;                      }
    constexpr   const t_allocator&      getAllocator    ( void )                const   noexcept { return m_Allocator;                      }
    constexpr   bool                    Belongs         ( const void* pPtr )    const   noexcept { return m_Allocator.Belongs(pPtr);        }
    constexpr   int                     getIndexByEntry ( const t_entry& Entry ) const  noexcept { return m_Allocator.getIndexByEntry( Entry); }
    template< typename T >
    constexpr   const t_entry&          getEntryByIndex ( const T Index )       const   noexcept { return m_Allocator[Index]; }
    template< typename T >
    x_forceinline   t_entry&            getEntryByIndex ( const T Index )               noexcept { return m_Allocator[Index]; }

protected:

    struct next;
    using  head  = x_atomic<next*>;
    struct next { head m_Next; };
     
    static_assert( alignof(t_entry) >= alignof(next), "Please make sure that your entry has the alignment of an x_atomic<void*>" );
    static_assert( sizeof(t_entry)  >= sizeof(next),  "Please make sure that the entry is byte size is larger than an atomic pointer" );
    
protected:
    
    alignas( x_target::getCacheLineSize() )  head                       m_Head         { nullptr };
    t_allocator                                                         m_Allocator    {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      dynamic - The pool will be allocated in the heap, must Init to create the pool size
//------------------------------------------------------------------------------
template< class T_ENTRY >
class x_ll_fixed_pool_dynamic : public x_ll_fixed_pool_general< T_ENTRY, xndptr >
{
public:

    using t_self   = x_ll_fixed_pool_dynamic;
    using t_parent = x_ll_fixed_pool_general< T_ENTRY, xndptr >;
    using t_entry  = typename t_parent::t_entry;
    
public:

    x_forceinline           x_ll_fixed_pool_dynamic     ( void )        noexcept    : t_parent( false ) {}
    inline                 ~x_ll_fixed_pool_dynamic     ( void )        noexcept    { Kill();                                                       }
    
  //  template< class T, typename ...T_ARG >
  //  inline  void            Init                        ( s32 Count, T_ARG&&...Args)   noexcept { t_parent::m_Allocator.New( Count, std::forward<T_ARG>(Args)... ); t_parent::Clear(); }
    inline  void            Init                        ( s32 Count )   noexcept    { t_parent::m_Allocator.New( Count ); t_parent::Clear();        }
    inline  void            Kill                        ( void )        noexcept    { t_parent::m_Allocator.Delete();                               }
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      dynamic - The pool will be allocated in the heap, must Init to create the pool size
//      jitc    - ( Just In Time Construction/Destruction )
//                  Means that when an entry is pop then we will constructed and push destruct it
//------------------------------------------------------------------------------
template< class T_ENTRY >
class x_ll_fixed_pool_dynamic_jitc : public x_ll_fixed_pool_general_jitc<T_ENTRY,xafptr>
{
public:

    using t_self   = x_ll_fixed_pool_dynamic_jitc< T_ENTRY >;
    using t_parent = x_ll_fixed_pool_general_jitc< T_ENTRY, xafptr >;
    
public:

    x_forceinline       x_ll_fixed_pool_dynamic_jitc    ( void )         noexcept    : t_parent( false ) {}
    inline             ~x_ll_fixed_pool_dynamic_jitc    ( void )         noexcept   { Kill();                                                               }
    inline void         Init                            ( xuptr Count )  noexcept   { t_parent::m_Allocator.Alloc( Count ); t_parent::Clear();              }
    inline void         Kill                            ( void )         noexcept   { t_parent::DestructAllocatedNodes();   t_parent::m_Allocator.Free();   }
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      static  - The pool is created as an array in the class.
//      jitc    - ( Just In Time Construction/Destruction )
//                  Means that when an entry is pop then we will constructed and push destruct it
//------------------------------------------------------------------------------
template< class T_ENTRY, int T_COUNT, typename T_COUNTER = xuptr >
using x_ll_fixed_pool_static_jitc = x_ll_fixed_pool_general_jitc
< 
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray_raw, T_COUNT >::template allocator 
>;

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      static  - The pool is created as an array in the class.
//------------------------------------------------------------------------------
template< class T_ENTRY, int T_COUNT, typename T_COUNTER = xuptr >
using x_ll_fixed_pool_static = x_ll_fixed_pool_general
<
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray, T_COUNT >::template allocator,
    T_COUNTER 
>;

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      static  - The pool is created as an array in the class
//      raw     - Elements in the pool wont be constructed or destructed
//------------------------------------------------------------------------------
template< class T_ENTRY, int T_COUNT, typename T_COUNTER = xuptr >
using x_ll_fixed_pool_static_raw = x_ll_fixed_pool_general
< 
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray_raw, T_COUNT >::template allocator 
>;

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      Lockless circular pool with debug build
//------------------------------------------------------------------------------
class x_ll_circular_pool
{
    x_object_type( x_ll_circular_pool, is_quantum_lock_free, is_not_copyable, is_not_movable );

public:
    
                                        x_ll_circular_pool  ( void )  = default;
    inline                              x_ll_circular_pool  ( xuptr ByteCount )                 noexcept { Init( ByteCount );                                                            }
    template< class T > inline  T&      Alloc               ( void )                            noexcept { return *reinterpret_cast<T*>(ByteAlloc( sizeof(T), alignof( T ) ));           }
    
    template< class T, typename ...T_ARG > 
    inline                      T&      New                 ( T_ARG&&...Args )                  noexcept { return *x_construct( T, ByteAlloc( sizeof(T), alignof( T ) ), { std::forward<T_ARG>(Args)... } );               }
    inline                      void    Free                ( void* pVoidData )                 noexcept;
    inline                      void    Init                ( xuptr ByteCount )                 noexcept;
    constexpr                   xuptr   getSize             ( void ) const                      noexcept { return m_pPtr.getCount();                                                     }
    
protected:
    
    inline                      xbyte*  ByteAlloc           ( xuptr ByteCount, u32 Aligment )   noexcept ;
    inline                      void    MoveHeadForward     ( void )                            noexcept ;
    
protected:
    
#if _X_DEBUG
    struct node
    {
        x_atomic<void*>     m_Ptr       { nullptr };
        xuptr               m_Size      { 0 };
    };

    xndptr<node>                m_Debug_PtrList  {};
    x_atomic<xuptr>             m_Debug_iTail    { 0 };
    x_atomic<xuptr>             m_Debug_iHead    { 0 };
    x_atomic<xuptr>             m_Stats_MaxUsed  { 0 };
    x_atomic<xuptr>             m_Stats_CurrUsed { 0 };
    x_atomic<s64>               m_Stats_AllocVsFree { 0 };

    x_atomic<xuptr>             m_Debug_HeadLoop{ 0 };
    x_atomic<xuptr>             m_Debug_TailLoop{ 0 };
    x_atomic<void*>             m_Debug_TailPtr { nullptr };
#endif
    
    alignas( x_target::getCacheLineSize() ) x_atomic<xuptr>     m_Head  { 0 };
    alignas( x_target::getCacheLineSize() ) xafptr<xbyte>       m_pPtr  {};
};

//------------------------------------------------------------------------------
// Description:
//      Quantum object used whenever you want to have a quantum base class
//------------------------------------------------------------------------------
class x_ll_object_debug
{
    x_object_type( x_ll_object_debug, is_quantum_lock_free, rtti_start, is_not_copyable, is_not_movable );

public:

    x_ll_object_debug(void) = default;

protected:

    x_debug_linear_quantum m_Debug_LQ {};
    virtual void qt_onRun( void ) {}
    virtual ~x_ll_object_debug( void ) = default;
};

template< typename T_CHILD, typename T_PARENT = x_ll_object_debug >
class x_ll_object_harness : public T_PARENT
{
    x_object_type( x_ll_object_harness, is_quantum_lock_free, rtti(T_PARENT), is_not_copyable, is_not_movable );

public:
    
    using   t_child   = T_CHILD;
    using   queue     = x_ll_mpsc_process_node_queue;

protected:

    struct local_message : queue::node
    {
        template< typename T >
        x_forceinline local_message( xbyte& Data, std::size_t Size, T&& Lambda ) noexcept : m_Callback{ Data, Size, std::forward<T>(Lambda) } {}
        const xfunction_withptr<void(void)>   m_Callback;
    };

    template< typename T >
    struct local_message_dynamic_sized : local_message
    {
        x_forceinline local_message_dynamic_sized( T&& Lambda ) noexcept : local_message{ m_Data[0], sizeof(T), std::forward<T>(Lambda) }{}
        alignas( alignof(T) ) xbyte m_Data[sizeof(T)];
    };

protected:

    x_orforceconst          x_ll_object_harness     ( x_ll_circular_pool& CircularPool )    noexcept : m_MsgAllocPool{ CircularPool }
    {
        x_assert_linear( t_parent::m_Debug_LQ );
    }

    x_inline        void                            t_ProcessMessages       ( void )                                                                    noexcept{}

    template< typename T >
    x_inline        void    SendMsg                 ( T&& Callback ) noexcept
    {
        x_assert_quantum( t_parent::m_Debug_LQ );
        if( TryToLockMsgQueue() )
        {
            // linear world
            {
                x_assert_linear ( m_Debug_LQMsg );
                reinterpret_cast<t_child*>(this)->t_ProcessMessages();
                Callback();
            }
            UnLockMsgQueue();
        }
        else
        {
            // Append the message in the queue to be consumed later
            using data = local_message_dynamic_sized<decltype(Callback)>;
            Push( m_MsgAllocPool.x_ll_circular_pool::New<data>( std::forward<T>(Callback) ) );
        }
    }

    x_inline bool TryToLockMsgQueue( void )
    {
        if( m_Queue.TryLock() )
        {
            x_assert_linear ( m_Debug_LQMsg );
            reinterpret_cast<t_child*>(this)->t_ProcessMessages();
            return true;
        }
        return false;
    }

    x_inline void UnLockMsgQueue( void )
    {
        // This will process the remaining messages and unlock when done
        x_assert_quantum( t_parent::m_Debug_LQ );

        // When this for loop finish is guaranteed that there is not more messages in the queue
        for( queue::node* pNode = m_Queue.PopProcessNode(); pNode; pNode = m_Queue.PopProcessNode() )
        {
            // can not take this guy out of this local scope because 
            // the destructor will be call in the quantum world other wise
            // unlocks inside the pop
            x_assert_linear ( m_Debug_LQMsg );

            auto& Msg = reinterpret_cast<local_message&>( *pNode );
            Msg.m_Callback();
            m_MsgAllocPool.Free( &Msg );
        }
    }

    x_forceinline   void    Push                    ( xowner<local_message&> Node )    noexcept 
    { 
        m_Queue.push( Node, [this]
        {
            // linear world
            {
                x_assert_linear ( m_Debug_LQMsg );
                reinterpret_cast<t_child*>(this)->t_ProcessMessages();
            }
            UnLockMsgQueue();
        } ); 
    }
    
private:
        
    queue                                           m_Queue                 {};

protected:
    
    x_debug_linear_quantum                          m_Debug_LQMsg           {};            // LQ of the mutable data
    x_ll_circular_pool&                             m_MsgAllocPool;
};

//------------------------------------------------------------------------------
// Description:
//      This hash table is meant to be use in a multi-core environment. There are key events
//      were the hash table must lock parts of it. However it does this in the least disruptive way possible.
//      For example one reason to lock is when the user is mutating entry.
//      we lock only that entry in the hash preventing other simultaneous access which would result in 
//      unpredictable behaviors. In other words we must take that entry into a linear world because until the mutation
//      is complete is could be in a bad state.
//      The only other reason why we lock is when an entry must be deleted. In that case we lock a hash table index entry.
//      So that we can access that index table link list and fix up the pointers. All the other hash index table entries remain lockfree.
//      Note that in most cases index table entries and entries may have a 1:1 relation ship which means there is not much locking at all.
//
// TODO: Right now the Adding is Relax, which means that more of one similar key could be added because the adding is lockless
//       To guaranteed that I can add a restrictive add and leave the relax adding as an option.
//------------------------------------------------------------------------------
template< class ENTRY, class KEY, class DOWORK = x_lk_spinlock::do_nothing >
class x_ll_mrmw_hash 
{
    x_object_type( x_ll_mrmw_hash, is_quantum_lock_free, is_not_copyable, is_not_movable )

protected: struct hash_entry; 
public:

    using t_entry           = ENTRY;
    using t_key             = KEY;
    using t_function        = xfunction_view<void (t_entry&)>;
    using t_const_function  = xfunction_view<void (const t_entry&)>;

    struct iterator
    {
        inline iterator( t_self& S, bool Start ) : m_Hash{S}{ if(Start) this->operator ++(); }
        inline iterator& operator ++ ( void )                                                    
        { 
            if( m_pHashEntry ) 
            {
                m_pHashEntry = m_pHashEntry->m_Next;
                if( m_pHashEntry ) return *this;
            }

            m_iTableEntry++;
            while( m_iTableEntry < m_Hash.getCount() )
            {
                m_pHashEntry = m_Hash.m_qtHashTable[m_iTableEntry].m_Head.load();
                if( m_pHashEntry ) break;
                m_iTableEntry++;
            }

            return *this; 
        }

        inline iterator operator ++ ( int )                                                    
        { 
            auto I = *this;
            this->operator ++();
            return I; 
        }

        constexpr bool operator != ( const iterator& ) const 
        { 
            return m_iTableEntry < m_Hash.m_qtHashTable.getCount(); 
        }

        inline  t_entry&            operator *  ( void ) noexcept       { return m_pHashEntry->m_UserData; }
        inline  const t_entry&      operator *  ( void ) const noexcept { return m_pHashEntry->m_UserData; }
    
        t_self&     m_Hash;
        int         m_iTableEntry {-1};
        hash_entry* m_pHashEntry  {nullptr};
    };

public:

    constexpr                       x_ll_mrmw_hash          ( void )                                            noexcept = default;
    inline      void                Initialize              ( xuptr MaxEntries )                                noexcept;
    inline      void                DeleteAllEntries        ( void )                                            noexcept;
    inline      t_key               getKeyFromEntry         ( const t_entry& Entry ) const                      noexcept;
    inline      bool                isEntryInHash           ( t_key Key ) const                                 noexcept;
    constexpr   xuptr               getCount                ( void ) const                                      noexcept { return m_qtHashTable.getCount(); }
    inline      void                cpFindOrAddEntry        ( t_key Key, t_function Function )                  noexcept;
    inline      void                cpDelEntry              ( t_key Key, t_function Function )                  noexcept;
    inline      void                cpAddEntry              ( t_key Key, t_function Function )                  noexcept;
    inline      void                cpGetEntry              ( t_key Key, t_function Function )                  noexcept;
    inline      void                cpGetEntry              ( t_key Key, t_const_function Function ) const      noexcept;
    inline      bool                cpGetOrFailEntry        ( t_key Key, t_function Function )                  noexcept;
    inline      bool                cpGetOrFailEntry        ( t_key Key, t_const_function Function ) const      noexcept;

    inline      void                cpIterateGetEntry       ( t_function Function )                             noexcept;
    inline      void                cpIterateGetEntry       ( t_const_function Function ) const                 noexcept;

    inline      iterator            begin                   ( void )                                            noexcept { return iterator{ *this, true }; }
    inline      iterator            end                     ( void )                                            noexcept { return iterator{ *this, false }; }

protected:

    struct hash_entry 
    {
        x_atomic<hash_entry*>                   m_Next          { nullptr };
        x_lk_semaphore_smart_lock::base<DOWORK> m_Semaphore     {};
        t_key                                   m_Key           {};
        t_entry                                 m_UserData      {};
    };

    struct hash_table_entry
    {
        x_lk_semaphore_smart_lock::base<DOWORK> m_Semaphore     {};
        x_atomic<hash_entry*>                   m_Head          { nullptr };
    };

protected:

    inline      hash_entry&         getHashEntryFromEntry   ( t_entry& Entry )                                          noexcept;
    inline      const hash_entry&   getHashEntryFromEntry   ( const t_entry& Entry ) const                              noexcept;
    inline      xuptr               KeyToIndex              ( t_key Key ) const                                         noexcept;

    inline      t_entry&            FindOrAddPopEntry       ( t_key Key )                                               noexcept;
    inline      t_entry&            AddPopEntry             ( t_key Key )                                               noexcept;
    inline      t_entry*            getPopEntry             ( t_key Key )                                               noexcept;
    inline      const t_entry*      getPopEntry             ( t_key Key ) const                                         noexcept;
    inline      void                PushEntry               ( t_entry& Entry )                                          noexcept;
    inline      void                PushEntry               ( const t_entry& Entry ) const                              noexcept;

    inline      hash_entry&         FindOrAddHashEntry      ( hash_table_entry& TableEntry, t_key Key )                 noexcept;
    inline      hash_entry&         AddHashEntry            ( hash_table_entry& TableEntry, t_key Key )                 noexcept;
    inline      hash_entry*         FindEntry               ( const hash_table_entry& TableEntry, t_key Key ) const     noexcept;

protected:

    xndptr<hash_table_entry>                    m_qtHashTable       {};
    x_ll_fixed_pool_dynamic_jitc<hash_entry>    m_EntryMemPool      {};

    friend struct iterator;
};


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// EXPERIMENTAL
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Description:
//      Simple qt lockless stack aba is here
//------------------------------------------------------------------------------
template< typename T >
class x_ll_mpmc_stack 
{
    x_object_type( x_ll_mpmc_stack, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:
    
    inline void push( T& Node ) noexcept
    {
        T* pLocalReality = m_Head.load( std::memory_order_relaxed );
        do
        {
            Node.m_pNext.store( pLocalReality, std::memory_order_relaxed );
        } while( !m_Head.compare_exchange_weak( pLocalReality, &Node, std::memory_order_release, std::memory_order_relaxed ) );
    }
    
    inline T* pop( void ) noexcept
    {
        T* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality && !m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_pNext, std::memory_order_acquire, std::memory_order_relaxed ) );
        return pLocalReality;
    }
    
protected:
    
    alignas( x_target::getCacheLineSize() )  std::atomic<T*> m_Head  {nullptr};
};

//------------------------------------------------------------------------------
// Description:
//      This hash table algorithm is unable to delete entries
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_KEY >
class x_ll_mpmc_nondeletable_hashtable
{
    x_object_type( x_ll_mpmc_nondeletable_hashtable, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    inline void setup( s32 HashSize ) noexcept
    {
        m_List.Alloc(HashSize);
        m_List.MemSet(0);
    }

    inline void msgDestroy( void ) noexcept
    {
        m_List.msgDestroy();
    }

    inline xuptr computeHash( T_KEY Key ) const noexcept
    {
        return static_cast<xuptr>( x_murmurHash3( Key )%m_List.getCount() );
    }

    constexpr xuptr IncIterator( xuptr i ) const noexcept
    {
        return ( (i+1) == m_List.getCount() ) ? 0 : (i+1);
    }

    constexpr xuptr DecIterator( xuptr i ) const noexcept
    {
        return ( (i-1) == -1 ) ? m_List.getCount() : (i-1);
    }

    inline void AddItem ( T_KEY Key, T_ENTRY Value ) noexcept
    {
        assert( Key != 0 );
        assert( Value != 0 );

        xuptr i = computeHash(Key);
        do
        {
            auto& Node      = m_List[i];
            T_KEY StoredKey = Node.m_Key.load( std::memory_order_relaxed );
            do
            {
                if( StoredKey != 0 ) 
                {
                    // you can not add the same key twice
                    assert( StoredKey != Key );
                    break;
                }
                               
                if( !Node.m_Key.compare_exchange_weak( StoredKey, Key, std::memory_order_release ) )
                {
                    Node.m_Value.store( Value, std::memory_order_release );
                    return;
                }

            }  while( true );

            // Increment the counter
            i = IncIterator( i );

        } while( true );
    }

    inline T_ENTRY getItem ( T_KEY Key ) const noexcept
    {
        xuptr i = computeHash(Key);
        do
        {
            const auto& Node      = m_List[i];
            const T_KEY StoredKey = Node.m_Key.load( std::memory_order_relaxed );

            if( StoredKey == Key )
                return Node.m_Value.load( std::memory_order_relaxed );

            if( StoredKey == 0 )
                return 0;
            
            // Increment the counter
            i = IncIterator( i );

        } while( true );
    }

    inline xuptr getItemCountSlow( void ) noexcept
    {
        xuptr Count = 0;
        for( xuptr i = 0; i < m_List.getCount(); i++ )
            if( m_List[i].m_Key.load( std::memory_order_relaxed ) ) Count++;
        return Count;
    }

protected:

    using spinlock = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing >;
    
    struct node
    {
        x_atomic< T_KEY >           m_Key;
        x_atomic< T_ENTRY >         m_Value;
        spinlock                    m_Lock;
    };
    
private:
    
    xndptr<node>    m_List {};
};

//------------------------------------------------------------------------------
// Description:
//      A lockless paged pool. It does however locks when a new page needs to be created.
//------------------------------------------------------------------------------

template< typename T_ENTRY, xuptr T_ENTRIES_PER_PAGE_COUNT >
class x_ll_page_pool_jitc
{
    x_object_type( x_ll_page_pool_jitc, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using   t_fixedpool = x_ll_fixed_pool_static_jitc<T_ENTRY, T_ENTRIES_PER_PAGE_COUNT >;
    using   t_entry     = typename t_fixedpool::t_entry;

    struct page : public t_fixedpool 
    {
        page*  m_pNext {};
        page*  m_pPrev {};
    };

    using pagepool_linked_list  =  x_locked_object< x_linear_link_list_pool< page >,  x_lk_semaphore_smart_lock::base<> >;

public:

    constexpr x_ll_page_pool_jitc( void ) noexcept = default;

    //------------------------------------------------------------------------
    template<typename ...T_ARG> x_inline
    t_entry& pop( T_ARG&&...Args ) noexcept
    {
        return *x_construct( t_entry, &popDontConstruct(), { std::forward<T_ARG>(Args)... } );
    }

    //------------------------------------------------------------------------
    t_entry& popDontConstruct( void ) noexcept
    {    
        xuptr CurrentPageCount;
        
        //
        // Try to find a free page
        //
        {
            // we can lock as read only since the pagepool itself is lockless
            // and we get the pagepool as a normal
            x_lk_guard_as_const_get( m_PageList, PageList );
           
            CurrentPageCount = PageList.getCount();
            for( auto& Entry : PageList )
            {
                auto* pEntry = Entry.popDontConstruct();
                if( pEntry ) return *pEntry;
            }
        }

        //
        // Lets add a page 
        //
        {
            // we lock here as write because we are about to add a page
            x_lk_guard_get( m_PageList, PageList );

            // Did someone added a page since last time we checked?
            if( CurrentPageCount == PageList.getCount() )
            {
                // Lets add a page
                page& Page = PageList.pop();
                return *Page.popDontConstruct();
            }
        }
      
        //
        // We fail to add a page lets try again
        //
        return popDontConstruct();
    }

    constexpr xuptr getCapacity ( void ) const noexcept { x_lk_guard_get( m_PageList, PageList ); return PageList.getCount(); }
    inline    xuptr getCount    ( void ) const noexcept 
    {
        x_lk_guard_get( m_PageList, PageList ); 
        xuptr Total = 0; 
        for( auto& PageEntry : PageList ) 
            Total += PageEntry.getCount(); return Total; 
    }

    //------------------------------------------------------------------------
    void push( t_entry& Entry ) noexcept
    {
        //
        // Check which page it belongs
        //
        {
            // we lock as constant since the pagepool is lockless so readonly is fine
            // we get it normally
            x_lk_guard_as_const_get( m_PageList, PageList );
            for( auto& PageEntry : PageList )
            {
                if( PageEntry.Belongs( &Entry ) )
                {
                    PageEntry.push( Entry );
                    return;
                }
            }
        }
     
        // We could not find the page that this entry belongs
        x_assert( false );
    }

protected:

 //   using pagepool_linked_list  =   x_linear_link_list< t_fixedpool >;
 //   using page_list             =   x_locked_object< pagepool_linked_list, x_lk_semaphore_smart_lock<> >;
 //   using page                  =   typename pagepool_linked_list::t_entry;

//    static_assert( page_list::t_is_quantum && page_list::t_is_lockfree == false, "Error creating a locked object" );

protected:

    pagepool_linked_list       m_PageList{};
};
  

//------------------------------------------------------------------------------
// Description:
//      A lockless paged pool. It does however locks when a new page needs to be created.
//------------------------------------------------------------------------------

template< typename T_ENTRY, xuptr T_ENTRIES_PER_PAGE_COUNT >
class x_ll_page_pool
{
    x_object_type( x_ll_page_pool, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using   t_fixedpool = x_ll_fixed_pool_static< T_ENTRY, T_ENTRIES_PER_PAGE_COUNT >;
    using   t_entry     = typename t_fixedpool::t_entry;

public:

    constexpr x_ll_page_pool( void ) noexcept = default;

    //------------------------------------------------------------------------
    x_inline t_entry& pop    ( void ) noexcept
    {    
        xuptr CurrentPageCount;
       
        //
        // Try to find a free page
        //
        {
            // we can lock as read only since the pagepool itself is lockless
            // and we get the pagepool as a normal
            x_lk_guard_as_const( m_Lock );
           
            CurrentPageCount = m_PageList.getCount();
            for( auto& Entry : m_PageList )
            {
                auto* pEntry = Entry.pop();
                if( pEntry ) return *pEntry;
            }
        }
       
        //
        // Lets add a page 
        //
        {
            // we lock here as write because we are about to add a page
            x_lk_guard( m_Lock );

            // Did someone added a page since last time we checked?
            if( CurrentPageCount == m_PageList.getCount() )
            {
                t_fixedpool& Page = m_PageList.pop();
                return *Page.pop();
            }
        }
        
        //
        // We fail to add a page lets try again
        //
        return pop();
    }

    constexpr xuptr getCapacity ( void ) const noexcept { x_lk_guard_get( m_PageList, PageList ); return PageList.getCount(); }
    inline    xuptr getCount    ( void ) const noexcept 
    {
        x_lk_guard_get( m_PageList, PageList ); 
        xuptr Total = 0; 
        for( auto& PageEntry : PageList ) 
            Total += PageEntry.getCount(); return Total; 
    }

    //------------------------------------------------------------------------
    void push( t_entry& Entry ) noexcept
    {
        //
        // Check which page it belongs
        //
        {
            // we lock as constant since the pagepool is lockless so readonly is fine
            // we get it normally
            x_lk_guard_as_const( m_Lock );
            for( auto& PageEntry : m_PageList )
            {
                if( PageEntry.Belongs( &Entry ) )
                {
                    PageEntry.push( Entry );
                    return;
                }
            }
        }
     
        // We could not find the page that this entry belongs
        x_assert( false );
    }

    //------------------------------------------------------------------------
    void SanityCheck( void ) noexcept
    {
        //
        // Check which page it belongs
        //
        {
            // we lock as constant since the pagepool is lockless so readonly is fine
            // we get it normally
            x_lk_guard_as_const( m_Lock );
            for( auto& PageEntry : m_PageList )
            {
                PageEntry.SanityCheck();
            }
        }
    }

protected:

    x_lk_semaphore_smart_lock::base<>           m_Lock      {};
    x_linear_link_list_pool< t_fixedpool >      m_PageList  {};
};

//------------------------------------------------------------------------------
// Description:
//          This guys is a quantum object
//------------------------------------------------------------------------------
template< typename, typename T_UNIQUE_TYPE = xuptr > class x_ll_share_ref;

//------------------------------------------------------------------------------
class x_ll_share_object 
{
    x_object_type( x_ll_share_object, is_quantum_lock_free );

public:

    //---------------------------------------------------------------------------------------
    template< typename T_CLASS > inline 
    static bool t_AddSharedReference ( T_CLASS* const pData ) noexcept
    { 
        x_assert( pData );
        pData->x_ll_share_object::m_RefCount++;
        return true;
    }

    //---------------------------------------------------------------------------------------
    template< typename T = int > inline
    T getReferenceCount( void ) const noexcept
    {
        return x_static_cast<T>( m_RefCount.load( std::memory_order_relaxed ) );
    }

    //---------------------------------------------------------------------------------------
    template< typename T_CLASS > inline 
    static void t_SubtractSharedReference ( T_CLASS* const pData ) noexcept
    { 
        x_assert( pData );
        if( --pData->x_ll_share_object::m_RefCount == 0 )
        {
            T_CLASS::t_DeleteSharedInstance( pData );
        }
    }

protected:

    enum : bool { t_share_object = true };

private:

    //---------------------------------------------------------------------------------------
    // Default shared instance free from memory, override this one when new method is needed
    template< typename T_CLASS > inline 
    static void t_DeleteSharedInstance ( T_CLASS* const pData ) noexcept
    {
        x_assert( pData );
        x_delete( pData );
    }

    //---------------------------------------------------------------------------------------
    // Default shared instance new from memory, override this one when new method is needed
    template< typename T_CLASS, typename ...T_ARG > inline 
    static T_CLASS* t_NewSharedInstance ( T_ARG&&...Args ) noexcept
    {
        return x_new( T_CLASS, { Args... } );
    }
    
private:

    x_atomic<xuptr>             m_RefCount { 1 };

private:

    template< typename, typename > friend class x_ll_share_ref;
};

//------------------------------------------------------------------------------
// Description:
//          This class is similar to the std::shareptr but more restrictive.
//          it can only point to a x_ll_share_object where it has a ref count.
//          There is a T_UNIQUE_TYPE which helps diferenciate an intance from another.
//------------------------------------------------------------------------------
template< typename T_SHARE_OBJECT, typename T_UNIQUE_TYPE > 
class x_ll_share_ref
{
    x_object_type( x_ll_share_ref, is_linear );

public:

    using t_shared      = T_SHARE_OBJECT;
    using t_unique_type = T_UNIQUE_TYPE;

    static_assert( t_shared::t_share_object, "The share object must be derived from a x_ll_share_object" );

public:

    x_ll_share_ref( void ) noexcept = default;
    
    constexpr bool isValid( void ) const noexcept { return m_pShareObject != nullptr; }

    // create an instance
    template< typename ...T_ARG >
    void New( T_ARG&&...Args ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );

        m_pShareObject = t_shared::template t_NewSharedInstance<t_shared,T_ARG...>( std::forward<T_ARG>(Args)... );
    }

    // add ref
    inline void AddReference( void ) noexcept
    {
        x_assert(m_pShareObject);
        t_shared::t_AddSharedReference( m_pShareObject );
    }

    // Takes a pointer and adds it self as a reference
    void AddReference( xowner<t_shared&> Object ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = &Object;
        t_shared::t_AddSharedReference( m_pShareObject );
    }

    // sub ref
    inline void SubReference( void ) noexcept
    {
        x_assert(m_pShareObject);
        t_shared::t_SubtractSharedReference( m_pShareObject );
    }

    // set to null
    void setNull( void ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = nullptr;
    }

    // Transfer to me from an owner
    void TransferOwnerShip( xowner<t_shared&> Object ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = &Object;
    }

    // Transfer away to an owner
    xowner<t_shared*> TransferOwnerShip( void ) noexcept
    {
        auto t = m_pShareObject;
        m_pShareObject = nullptr;
        return t;
    }

    // add ref
    constexpr x_ll_share_ref( t_shared* pShareObj ) noexcept 
    : m_pShareObject{ pShareObj }
    {
        if( m_pShareObject ) t_shared::t_AddSharedReference( m_pShareObject );
    }

    // add ref
    constexpr x_ll_share_ref( t_shared& ShareObj ) noexcept 
    : m_pShareObject{ &ShareObj }
    {
        t_shared::t_AddSharedReference( m_pShareObject );
    }

    // add ref
    constexpr x_ll_share_ref( t_shared&& ShareObj ) noexcept
    : m_pShareObject { &ShareObj }
    {
        t_shared::t_AddSharedReference( m_pShareObject );
    }

    // copy ref
    constexpr x_ll_share_ref( t_self& ShareObj ) noexcept 
    : m_pShareObject{ ShareObj.m_pShareObject }
    {
        if( ShareObj.m_pShareObject ) t_shared::t_AddSharedReference( m_pShareObject );
    }

    // move ref
    constexpr x_ll_share_ref( t_self&& ShareObj ) noexcept
    : m_pShareObject { ShareObj.m_pShareObject }
    {
        ShareObj.m_pShareObject = nullptr;
    }

    // smart add ref
    inline x_ll_share_ref& operator = ( t_shared& ShareRef ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = &ShareRef;
        t_shared::t_AddSharedReference( m_pShareObject );
        return *this;
    }

    // smart move ref
    inline x_ll_share_ref& operator = ( t_self&& ShareObj ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = ShareObj.m_pShareObject;
        ShareObj.m_pShareObject = nullptr;
        return *this;
    }

    // smart copy ref
    inline x_ll_share_ref& operator = ( t_self& ShareObj ) noexcept
    {
        if( m_pShareObject == ShareObj.m_pShareObject ) 
            return *this;

        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = ShareObj.m_pShareObject;
        t_shared::t_AddSharedReference( m_pShareObject );

        return *this;
    }

    // smart release
    inline ~x_ll_share_ref( void ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
    }

    inline      t_shared*               operator ->                     ( void )        noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    constexpr   const t_shared*         operator ->                     ( void ) const  noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    inline                              operator T_SHARE_OBJECT*        ( void )        noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    inline                              operator T_SHARE_OBJECT&        ( void )        noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }
    constexpr                           operator const T_SHARE_OBJECT*  ( void ) const  noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    constexpr                           operator const T_SHARE_OBJECT&  ( void ) const  noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }
    constexpr const T_SHARE_OBJECT&     operator *                      ( void ) const  noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }
    inline      T_SHARE_OBJECT&         operator *                      ( void )        noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }

protected:

    t_shared*                   m_pShareObject{ nullptr };
};

//-------------------------------------------------------------------------------------------
// Description: 
//          Pool which try its best to keep allocated nodes as contiguous as possible.
//          So as cache friendly as possible.
//          Certain patterns of allocations could make this version very slow.  
// todo: it may be possible to make it faster by replacing m_nFreeEntries with another 64bit mask
//       that summarizes the status of the page. This mask will not only tell you if there are free
//       entries but also where you will need to jump in the mask array to search for them.
template< typename T_ENTRY, xuptr T_MAX_ENTRIES >
class x_ll_page_ordered_pool_jitc
{
    x_object_type( x_ll_page_ordered_pool_jitc, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using           t_entry         = T_ENTRY;

    struct iterator
    {
        t_self&     m_Obj;
        u64         m_Bits;
        int         m_iPage;
        int         m_iEntry; 

        inline iterator( t_self& Obj ) noexcept :
            m_Obj{ Obj },
            m_Bits{ 0 },
            m_iPage{ 0 },
            m_iEntry{ -1 } 
            { 
                // Search for a page that has something in it
                while( m_Obj.m_PageList[m_iPage].m_JumpToFree.load().m_Next >= ENTRIES_PER_BIT )
                {
                    m_iPage++;
                    if( m_iPage == NUM_PAGES ) return;
                }

                // Set all the index
                this->operator++(); 
            }

        constexpr iterator( t_self& Obj, bool ) noexcept :
            m_Obj{ Obj },
            m_Bits{ 0 },
            m_iPage{ 0 },
            m_iEntry{ 0 } {}

        inline  const iterator& operator ++ ( void ) noexcept                                               
        { 
            int ModedEmptyIndex;
            do 
            {
                ++m_iEntry;
                ModedEmptyIndex = (m_iEntry & 0x3f);
                
                // Move to the next set of bit blocks
                if( 0 == ModedEmptyIndex  )
                {
                    // Check to see if we are done with this page
                    if( m_iEntry == ENTRIES_PER_PAGE )
                    {
                        m_iEntry = 0;

                        // Search for a page that has something in it
                        do 
                        {
                            m_iPage++;
                            if( m_iPage == NUM_PAGES ) return *this;

                        } while( m_Obj.m_PageList[m_iPage].m_JumpToFree.load().m_Next >= ENTRIES_PER_BIT );
                    }

                    // get a new set of bits
                    m_Bits = m_Obj.m_PageListFreeMask[m_iPage][m_iEntry/64].load();
                    if( m_Bits == 0xffffffffffffffffu )
                    {
                        m_iEntry += 64 - 1;
                        continue;
                    }
                }

            } while( 1 & (m_Bits >> ModedEmptyIndex) ); 

            return *this; 
        }
        
        constexpr bool operator != ( const iterator& ) const noexcept                          
        { 
            return ( m_iPage != NUM_PAGES ) ? m_Obj.m_PageList[m_iPage].m_RawPool.isValid() : false;
        }

        constexpr       const auto&         operator *      ( void )        const   noexcept        { return (*m_Obj.m_PageList[m_iPage].m_RawPool)[m_iEntry]; }
        inline          auto&               operator *      ( void )                noexcept        { return (*m_Obj.m_PageList[m_iPage].m_RawPool)[m_iEntry]; }
    };

public:

    //------------------------------------------------------------------------
    inline      iterator        begin   ( void )            noexcept    { return { *this };             }
    inline      iterator        end     ( void )            noexcept    { return { *this, false };      }

    constexpr   iterator        begin   ( void )    const   noexcept    { return { *this };             }
    constexpr   iterator        end     ( void )    const   noexcept    { return { *this, false };      }

    //------------------------------------------------------------------------
    template<typename ...T_ARG>
    t_entry& pop    ( T_ARG&&...Args ) noexcept
    {
        xuptr i = 0;
        for( auto& Page : x_iter_ref( i, m_PageList ) )
        {
            //
            // Make sure that are enough entries allocated
            //
            if( Page.m_RawPool.isValid() == false )
            {
                x_lk_guard( m_PageAllocLock );
                if( Page.m_RawPool.isValid() == false )
                {
                    Page.m_RawPool.New();
                }
                else
                {
                    // Someone allocated before us then try again the hold thing just to make sure
                    i = -1;
                    continue;
                }
            }

            // Since we have allocated one entry in this page we are force to pick one from here
            // so keep looping until we find one
            auto&   FreeMaskArray   = m_PageListFreeMask[i];
            auto    TagFree         = Page.m_JumpToFree.load();
            for( auto iFree = TagFree.m_Next; iFree < ENTRIES_PER_BIT; iFree++  )
            {
                auto& FreeMask = FreeMaskArray[iFree];
                auto  Local    = FreeMask.load();

                // Find which bit is free if none then move on to next mask
                while( Local )
                {
                    // bin search for the first bit which is 1 (free entry)
                    int  c = 1;
                    {
                        auto v = Local;
                        if( (v & 0xffffffff) == 0 ) {  v >>= 32;  c += 32;}
                        if( (v & 0xffff)     == 0 ) {  v >>= 16;  c += 16;}
                        if( (v & 0xff)       == 0 ) {  v >>= 8;   c += 8; }
                        if( (v & 0xf)        == 0 ) {  v >>= 4;   c += 4; }
                        if( (v & 0x3)        == 0 ) {  v >>= 2;   c += 2; }
                        c -= v & 0x1;
                    }

                    // Try setting the new entry
                    const auto New = Local & (~(u64{1} << c));
                    if( FreeMask.compare_exchange_weak( Local, New ) )
                    {
                        const u16 NodeMoreNodes = (New == ~u64{ 0 });

                        // Update the next free if we can
                        if( iFree != TagFree.m_Next || NodeMoreNodes )
                        {
                            tagnext NewTagFree { static_cast<u16>(iFree + NodeMoreNodes), TagFree.m_Tag + 1u };
                            Page.m_JumpToFree.compare_exchange_strong( TagFree, NewTagFree );
                        }

                        // We got it
                        const auto EntryIndex = (FreeMaskArray.getIndexByEntry( FreeMask )<<6) + c;
                        return *x_construct( t_entry, &(*Page.m_RawPool)[ EntryIndex ],  { std::forward<T_ARG>(Args)... } );
                    }
                }
            }
        }

        // out of memory!
        x_assume( false );
    }

    //------------------------------------------------------------------------

    void push( t_entry& Entry ) noexcept
    {
        xuptr i = 0;
        for( auto& Page : x_iter_ref( i, m_PageList ) )
        {
            // Does this entry belongs to this pool?
            if( Page.m_RawPool->Belongs( &Entry ) == false )
                continue;
            
            // Ok we can destroy the object now
            x_destruct( &Entry );

            // need to mark the object as free now
            const auto  Index         = Page.m_RawPool->getIndexByEntry( Entry );
            const auto  subIndex      = Index >> 6;         // Index / 64
            const auto  bitIndex      = Index & 0x3f;       // Index % 64
            auto&       FreeMaskArray = m_PageListFreeMask[i];
            auto&       FreeMask      = FreeMaskArray[subIndex];
            auto        TagFree       = Page.m_JumpToFree.load();

            // Remove from the allocated list 
            auto Local = FreeMask.load();
            x_assert( (Local & (u64{1u}<<bitIndex)) == 0 );
            while( false == FreeMask.compare_exchange_weak( Local, Local | (u64{1u}<<bitIndex)) )
            {
                x_assert( (Local & (u64{1u}<<bitIndex)) == 0 );
            }

            // Since the priority is always lower free nodes try to update the hint if nothing has changed
            if( TagFree.m_Next > subIndex )
            {
                const auto NewTagFree = tagnext{ static_cast<u16>(subIndex), TagFree.m_Tag + 1u };
                Page.m_JumpToFree.compare_exchange_strong( TagFree, NewTagFree );
            }

            return;
        }

        x_assume( false );
    }

    //------------------------------------------------------------------------

    x_ll_page_ordered_pool_jitc( void )
    {
        m_PageListFreeMask.MemSet( ~0 );
    }

protected:

    enum
    {
        MAX_PAGES               = 24,
        ENTRIES_PER_PAGE        = x_Align( x_Max(64, T_MAX_ENTRIES / (MAX_PAGES-1) ), 64 ),
        ENTRIES_PER_BIT         = (ENTRIES_PER_PAGE / 64),                    
        NUM_PAGES               = (T_MAX_ENTRIES / ENTRIES_PER_PAGE) + (!!(T_MAX_ENTRIES % ENTRIES_PER_PAGE))
    };

    static_assert( ENTRIES_PER_BIT*64 == ENTRIES_PER_PAGE,      "make sure we have enough bits per entry" );
    static_assert( NUM_PAGES*ENTRIES_PER_PAGE >= T_MAX_ENTRIES, "The computed number of entries is less than the one requested" );
    static_assert( NUM_PAGES < MAX_PAGES,                       "Number of pages should always be less than max pages" );

    struct alignas(int) tagnext  
    {
        u16                                                     m_Next;                                 // Next block where it could be a free node
        u16                                                     m_Tag;                                  // make sure we know that has not changed
    };

    static_assert( ENTRIES_PER_BIT < 0xffff, "u16 can not hold all the max number of bits" );

    struct page_list_entry
    {
        xndptr_s<xarray_raw<t_entry,ENTRIES_PER_PAGE>>          m_RawPool       {};                     // pointer to the actual page of entries
        x_atomic<tagnext>                                       m_JumpToFree    { tagnext{0,0} };       // Number of free nodes
    };

    using  page_alloc_lock = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing >;
      
protected:

    xarray<page_list_entry, NUM_PAGES>                          m_PageList          {};                 // List of pages
    xarray<xarray<x_atomic<u64>,ENTRIES_PER_BIT>, NUM_PAGES>    m_PageListFreeMask  {};                 // 1 bit for each entry saying if it is been use
    page_alloc_lock                                             m_PageAllocLock     {};                 // Lock whenever we need to allocate a new page
};


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

template< typename T_ENTRY, typename T_GUID = typename T_ENTRY::guid >
struct x_ll_dbase
{
public:

    using guid = T_GUID;

    //------------------------------------------------------------------------------
    struct node
    {
        enum class delete_flag : bool 
        { 
            DONT_DELETE_WHEN_DONE   = false,
            DELETE_WHEN_DONE        = true 
        };

        node( void ) = default;
        inline ~node( void )
        {
            if( m_DeleteFlag == delete_flag::DONT_DELETE_WHEN_DONE ) m_pEntry.TransferOwnership();
        }

        xndptr_s<T_ENTRY>   m_pEntry        {};
        int                 m_Index         {};
        delete_flag         m_DeleteFlag    { delete_flag::DELETE_WHEN_DONE };
    };

public:

    //------------------------------------------------------------------------------
    x_ll_dbase   ( void )        = default;
    x_ll_dbase   ( int Count )   { Init( Count ); }

    //------------------------------------------------------------------------------
    void Init( int Count )
    {
        m_Hash.Initialize   ( Count );
    }

    //------------------------------------------------------------------------------
    
    void RegisterButRetainOwnership( T_ENTRY& Entry )
    {
        m_Hash.cpAddEntry( Entry.getGuid().m_Value, [this,&Entry](node& Node)
        {
            Node.m_pEntry.setup( &Entry );

            x_lk_guard_get( m_Vector, Vector );
            Node.m_Index        = Vector.template getCount<int>();
            Node.m_DeleteFlag   = node::delete_flag::DONT_DELETE_WHEN_DONE;
            Vector.append( &Node );
        });
    }

    //------------------------------------------------------------------------------
    
    void RegisterButRetainOwnership( const T_ENTRY& Entry )
    {
        RegisterButRetainOwnership( const_cast<T_ENTRY&>(Entry) );
    }

    //------------------------------------------------------------------------------
    template< typename T >
    void RegisterAndTransferOwner( xndptr_s<T>& Entry )
    {
        static_assert( std::is_base_of<T_ENTRY,T>::value, "Types are not convertible" );
        m_Hash.cpAddEntry( Entry->getGuid().m_Value, [this,&Entry]( node& Node )
        {
            Node.m_pEntry.setup( Entry.TransferOwnership() );

            x_lk_guard_get( m_Vector, Vector );
            Node.m_Index = Vector.template getCount<int>();
            Vector.append( &Node ); 
        });
    }

    //------------------------------------------------------------------------------
    T_ENTRY& FindOrRegister( guid Guid, typename node::delete_flag DeleteFlag, xfunction_view<T_ENTRY&(void)> New )
    {
        T_ENTRY* pEntry = nullptr;
        m_Hash.cpFindOrAddEntry( Guid.m_Value, [&](node& Node)
        { 
            if( Node.m_pEntry.isValid() )
            {
                pEntry = Node.m_pEntry;
                return;
            } 

            //
            // create an entry 
            //
            pEntry = &New();
            Node.m_pEntry.setup( pEntry );

            x_lk_guard_get( m_Vector, Vector );
            Node.m_Index        = Vector.template getCount<int>();
            Node.m_DeleteFlag   = DeleteFlag; 
            Vector.append( &Node ); 
        } );

        return *pEntry;
    }

    //------------------------------------------------------------------------------
    void Delete( guid Guid )
    {
        m_Hash.cpDelEntry( Guid.m_Value, [this]( node& Node )
        {
            x_lk_guard_get( m_Vector, Vector );
            Vector[Vector.getCount()-1]->m_Index = Node.m_Index;
            Vector.DeleteWithSwap( Node.m_Index );
        });
    }

    //------------------------------------------------------------------------------
    const T_ENTRY* Find( guid Guid ) const
    {
        const T_ENTRY* pEntry = nullptr;
        m_Hash.cpGetOrFailEntry( Guid.m_Value, [&](const node& Node) { pEntry = &Node.m_pEntry[0]; } );
        return pEntry;
    }

    //------------------------------------------------------------------------------

    T_ENTRY* Find( guid Guid )
    {
        T_ENTRY* pEntry = nullptr;
        m_Hash.cpGetOrFailEntry( Guid.m_Value, [&](node& Node) { pEntry = &Node.m_pEntry[0]; } );
        return pEntry;
    }

    //------------------------------------------------------------------------------

    const T_ENTRY& getEntry( guid Guid ) const
    {
        const T_ENTRY* pEntry = nullptr;
        m_Hash.cpGetEntry( Guid.m_Value, [&](const node& Node) { pEntry = &Node.m_pEntry[0]; } );
        return *pEntry;
    }

    //------------------------------------------------------------------------------

    T_ENTRY& getEntry( guid Guid )
    {
        T_ENTRY* pEntry = nullptr;
        m_Hash.cpGetEntry( Guid.m_Value, [&](node& Node) { pEntry = &Node.m_pEntry[0]; } );
        return *pEntry;
    }

    //------------------------------------------------------------------------------

    void DeleteAllEntries( void )
    {
        m_Hash.DeleteAllEntries();

        x_lk_guard_get( m_Vector, Vector );
        Vector.DeleteAllEntries();
    }

public:

    using lk_vector = x_locked_object< xvector<node*>, x_lk_semaphore_smart_lock::base<x_lk_semaphore_smart_lock::do_nothing, x_lk_semaphore_smart_lock::reentrance> >;

public:

    x_ll_mrmw_hash<node, u64,x_lk_spinlock::do_nothing>                     m_Hash;
    lk_vector                                                               m_Vector;
};
