﻿//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
#ifdef min
    #undef min // one of those great Microsoft gifts 
#endif

namespace xproperty_v2
{
    struct base;

    struct button   
    { 
        xstring m_Value; 
        bool operator == ( const button& B ) const { return m_Value == B.m_Value; } 
        auto& operator = ( const button& B ) { m_Value = B.m_Value; return *this; } 
    };

    //---------------------------------------------------------------------------------------
    // property types
    //---------------------------------------------------------------------------------------
    enum class prop_type : u8
    {
        INVALID,
        ANGLES3,
        BOOL,
        BUTTON,
        ENUM,
        FLOAT,
        GUID,
        INT,
        RGBA,
        STRING,
        VECTOR2,
        VECTOR3,
        ENUM_COUNT
    };

    template<typename T>
    using _enum_string = xstring;
    struct enum_string : public xstring 
    { 
        using xstring::xstring_base; 
        enum_string& operator = (const xstring& X ) { static_cast<xstring&>(*this) = X;/*Copy( X )*/; return *this; }
    };

    using property_variant = xvariant_enum
    <
        prop_type,                  // ENUMERATION TYPE
        char,                       // prop_type::INVALID 
        xradian3,                   // prop_type::ANGLES3 
        bool,                       // prop_type::BOOL:   
        button,                     // prop_type::BUTTON:
        enum_string,                // prop_type::ENUM:
        float,                      // prop_type::FLOAT:  
        u64,                        // prop_type::GUID
        int,                        // prop_type::INT:    
        xcolor,                     //    
        xstring,                    // prop_type::STRING: 
        xvector2,                   // prop_type::VECTOR2: 
        xvector3                    // prop_type::VECTOR3: 
    >;
        
    // If the enum gets reorder we want to know since the xvariant_enum must match the types
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::INVALID>::type,  char        >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::ANGLES3>::type,  xradian3    >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::BOOL>::type,     bool        >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::BUTTON>::type,   button      >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::ENUM>::type,     enum_string >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::FLOAT>::type,    float       >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::GUID>::type,     u64         >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::INT>::type,      int         >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::RGBA>::type,     xcolor      >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::STRING>::type,   xstring     >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::VECTOR2>::type,  xvector2    >::value, "" );
    static_assert( std::is_same< property_variant::enum_to_type<prop_type::VECTOR3>::type,  xvector3    >::value, "" );
    
    static_assert( property_variant::type_to_enum<char>::value              == prop_type::INVALID,      "" );
    static_assert( property_variant::type_to_enum<xradian3>::value          == prop_type::ANGLES3,      "" );
    static_assert( property_variant::type_to_enum<bool>::value              == prop_type::BOOL,         "" );
    static_assert( property_variant::type_to_enum<button>::value            == prop_type::BUTTON,       "" );
    static_assert( property_variant::type_to_enum<enum_string>::value       == prop_type::ENUM,         "" );
    static_assert( property_variant::type_to_enum<float>::value             == prop_type::FLOAT,        "" );
    static_assert( property_variant::type_to_enum<u64>::value               == prop_type::GUID,         "" );
    static_assert( property_variant::type_to_enum<int>::value               == prop_type::INT,          "" );
    static_assert( property_variant::type_to_enum<xcolor>::value            == prop_type::RGBA,         "" );
    static_assert( property_variant::type_to_enum<xstring>::value           == prop_type::STRING,       "" );
    static_assert( property_variant::type_to_enum<xvector2>::value          == prop_type::VECTOR2,      "" );
    static_assert( property_variant::type_to_enum<xvector3>::value          == prop_type::VECTOR3,      "" );

    static_assert( property_variant::arity == static_cast<int>(prop_type::ENUM_COUNT), "" );

    //---------------------------------------------------------------------------------------
    // Flags
    //---------------------------------------------------------------------------------------

    X_DEFBITS( flags,
               u8,
               0x0000,
               X_DEFBITS_ARG ( bool,      READ_ONLY,           1 ),    
               X_DEFBITS_ARG ( bool,      FORCE_SAVE,          1 ),                                         
               X_DEFBITS_ARG ( bool,      DONT_SAVE,           1 ),                                         
               X_DEFBITS_ARG ( bool,      REFRESH_ONCHANGE,    1 ),                                         
               X_DEFBITS_ARG ( bool,      NOT_VISIBLE,         1 )                                         
    );

    //---------------------------------------------------------------------------------------
    // Details
    //---------------------------------------------------------------------------------------
    namespace info
    {
        struct base 
        { 
            constexpr base( void ) = default;
            
            xstring::const_str  m_Help  {nullptr};
            flags               m_Flags {};
            base* getRaw( void ) const { return const_cast<base*>(this); }
        };

        struct button : public base 
        {
            enum { value = (int)prop_type::BUTTON            }; 

            button(void) = default;
        };

        struct enum_t : public base 
        {
            enum { value = (int)prop_type::ENUM            }; 

            enum_t(void) = default;
            template< int N >
            constexpr enum_t( const xarray<std::pair<int, xconst_str<xchar>>, N>& List) : m_List{ List } {}

            const xbuffer_view<std::pair<int, xconst_str<xchar>>>   m_List {nullptr,0};
        };

        struct s32 : public base 
        {
            enum { value = (int)prop_type::INT            }; 

                      s32( void ) = default;
            constexpr s32( int a, int b ) : m_Min(a), m_Max(b) {}

            int         m_Min = std::numeric_limits<int>::min();
            int         m_Max = std::numeric_limits<int>::max();
        };

        struct f32 : public base 
        {
            enum { value = (int)prop_type::FLOAT          }; 

                      f32( void ) = default;
            constexpr f32( float a, float b ) : m_Min(a), m_Max(b) {}

            float       m_Min = std::numeric_limits<float>::min();
            float       m_Max = std::numeric_limits<float>::max();
        };

        struct vector3 : public base
        {
            enum { value = (int)prop_type::VECTOR3        }; 

            enum class gizmo_type : u8
            {
                DRAG            ,
                SLIDER          ,
                EDIT            ,
                ENUM_COUNT
            };

            enum class gizmo_units : u8
            {
                NUMBER,
                POSITION_CM,
                UNIT_SCALE,
                ENUM_COUNT
            };

                      vector3( void ) = default;
            constexpr vector3( const xvector3 a, const xvector3 b ) : m_Min(a), m_Max(b) {}
            constexpr vector3( const xvector3 a, const xvector3 b, gizmo_units GizmoUnitType, gizmo_type GizmoType, gizmo_type DetailGizmoType ) : 
                m_Min                   { a }
                , m_Max                 { b }
                , m_GizmoUnitType       { GizmoUnitType }
                , m_GizmoType           { GizmoType }
                , m_DetailGizmoType     { DetailGizmoType }
            {}

            xvector3    m_Min{  std::numeric_limits<float>::min(), 
                                std::numeric_limits<float>::min(),
                                std::numeric_limits<float>::min() };
            
            xvector3    m_Max{  std::numeric_limits<float>::max(), 
                                std::numeric_limits<float>::max(),
                                std::numeric_limits<float>::max() };

            gizmo_units m_GizmoUnitType     { gizmo_units::NUMBER };
            gizmo_type  m_GizmoType         { gizmo_type::SLIDER  };
            gizmo_type  m_DetailGizmoType   { gizmo_type::EDIT    };
        };

        struct vector2 : public base
        {
            enum { value = (int)prop_type::VECTOR2        }; 

            using gizmo_type  = vector3::gizmo_type;
            using gizmo_units = vector3::gizmo_units;

                      vector2( void ) = default;
            constexpr vector2( const xvector2 a, const xvector2 b ) : m_Min(a), m_Max(b) {}

            xvector2    m_Min{  std::numeric_limits<float>::min(), 
                                std::numeric_limits<float>::min() };
            
            xvector2    m_Max{  std::numeric_limits<float>::max(), 
                                std::numeric_limits<float>::max() };

            gizmo_units m_GizmoUnitType     { gizmo_units::NUMBER };
            gizmo_type  m_GizmoType         { gizmo_type::SLIDER  };
            gizmo_type  m_DetailGizmoType   { gizmo_type::EDIT    };
        };

        struct radian3 : public base
        {
            enum { value = (int)prop_type::ANGLES3        }; 

                      radian3( void ) = default;
            constexpr radian3( const xradian3 a, const xradian3 b ) : m_Min(a), m_Max{b} {}

            xradian3    m_Min{ -360_deg,   -360_deg,   -360_deg };
            xradian3    m_Max{  360_deg,    360_deg,    360_deg };
        };

        struct boolean : public base
        {
            enum { value = (int)prop_type::BOOL           }; 

                      boolean( void ) = default;
            constexpr boolean( int Shift ) : m_Shift( Shift ) {}

            int         m_Shift{ 0 };
        };

        struct guid : public base
        {
            enum { value = (int)prop_type::GUID           }; 

            enum class edit_style : u8
            {
                DEFAULT,
                RAW,
            };

                      guid( void ) = default;
            constexpr guid( xstring::const_str Category, u64 Type ) : m_Category( Category ), m_Type(Type) {}
            constexpr guid( xstring::const_str Category, u64 Type, edit_style EditStyle ) : m_Category( Category ), m_Type(Type), m_EditStyle{EditStyle} {}

            xstring::const_str      m_Category  { nullptr               };  // Category which this guid represents
            u64                     m_Type      { 0                     };  // Numeric type which this guid represents
            edit_style              m_EditStyle { edit_style::DEFAULT   };
        };
    };

    //---------------------------------------------------------------------------------------
    // DO NOT USE THIS STRUCTURE
    // TODO: This hold enum_t should be clean up
    //---------------------------------------------------------------------------------------
    struct tag_enum {};
    template< typename T >
    struct enum_t : tag_enum  
    { 
        using type = T;
        T m_Value; 
        static_assert( sizeof(T) == 1, "Enum should have a max of 256 numbers" );
        enum_t() = default;
        constexpr enum_t(T val) : m_Value{ val } {}
        bool operator == ( const enum_t& B ) const  { return m_Value == B.m_Value; } 
        auto& operator = ( const enum_t& B )        { m_Value = B.m_Value; return *this; } 
    };

    //---------------------------------------------------------------------------------------
    // Convert from a regular C++ type to details
    //---------------------------------------------------------------------------------------

    namespace details
    {
        template< typename T > struct details_from_type
        {
            using detail = xproperty_v2::info::base; 
        };
        template<> struct details_from_type<bool>                { using detail = xproperty_v2::info::boolean;    };
        template<> struct details_from_type<float>               { using detail = xproperty_v2::info::f32;        };
        template<> struct details_from_type<int>                 { using detail = xproperty_v2::info::s32;        };
        template<> struct details_from_type<xradian3>            { using detail = xproperty_v2::info::radian3;    };
        template<> struct details_from_type<xvector2>            { using detail = xproperty_v2::info::vector2;    };
        template<> struct details_from_type<xvector3>            { using detail = xproperty_v2::info::vector3;    };
        template<> struct details_from_type<u64>                 { using detail = xproperty_v2::info::guid;       };
        template<> struct details_from_type<enum_t<u8>>          { using detail = xproperty_v2::info::enum_t;     };
    }

    template< typename T > struct type_to_enum
    {
        enum { value = static_cast<int>(property_variant::type_to_enum< typename std::remove_const<T>::type >::value) };
        using detail = typename details::details_from_type< typename std::remove_const<T>::type >::detail;
    };

    //--------------------------------------------------------------------------------
    using details_variant = xvariant
    <
        xproperty_v2::info::boolean,   
        xproperty_v2::info::f32,       
        xproperty_v2::info::s32,       
        xproperty_v2::info::vector2,
        xproperty_v2::info::vector3,
        xproperty_v2::info::radian3,
        xproperty_v2::info::guid,
        xproperty_v2::info::enum_t,
        xproperty_v2::info::base            // Default for anything that does not really have a detail type
    >;

    //---------------------------------------------------------------------------------------
    // Entry for a property
    //---------------------------------------------------------------------------------------
    struct entry
    {
        xstring                 m_Name      {};
        property_variant        m_Data      {};
        info::base*             m_pDetails  { nullptr };

        bool operator == ( const entry& Entry ) const
        {
            return m_Data == Entry.m_Data && m_Name == Entry.m_Name;
        }

        //------------------------------------------------------------------------------------------------
        x_inline
        void SerializeOut    ( xtextfile& DataFile ) const
        {
            DataFile.WriteField( "Type:<?>", getWriteTypeString()    );
            DataFile.WriteField( "Name:s",   (const char*)m_Name );

            static const char* pValueType = "Value:<Type>";
            switch( m_Data.getTypeIndex() )
            {
                case prop_type::BOOL:     DataFile.WriteField( pValueType, m_Data.get<prop_type::BOOL>() );                                                                         break;
                case prop_type::BUTTON:   DataFile.WriteField( pValueType, (const char*)m_Data.get<prop_type::BUTTON>().m_Value );                                                  break;
                case prop_type::RGBA:    
                {
                    const auto Color = m_Data.get<prop_type::RGBA>();
                    DataFile.WriteField( pValueType, Color.m_R, Color.m_G, Color.m_B, Color.m_A );                            
                    break;
                }
                case prop_type::ENUM:     DataFile.WriteField( pValueType, (const char*)m_Data.get<prop_type::ENUM>() );                                                            break;
                case prop_type::INT:      DataFile.WriteField( pValueType, m_Data.get<prop_type::INT>() );                                                                          break;
                case prop_type::FLOAT:    DataFile.WriteField( pValueType, m_Data.get<prop_type::FLOAT>() );                                                                        break;
                case prop_type::ANGLES3:
                {    
                    const auto& Angles = m_Data.get<prop_type::ANGLES3>();
                    DataFile.WriteField( pValueType, Angles.m_Pitch.getDegrees().m_Value,
                                                     Angles.m_Yaw.getDegrees().m_Value,
                                                     Angles.m_Roll.getDegrees().m_Value   );
                    break;
                }
                case prop_type::STRING:   DataFile.WriteField( pValueType, (const char*) m_Data.get<prop_type::STRING>() );                                                         break;
                case prop_type::GUID:     DataFile.WriteField( pValueType, m_Data.get<prop_type::GUID>() );                                                                         break;
                case prop_type::VECTOR2:  
                {
                    const auto& V2 = m_Data.get<prop_type::VECTOR2>();
                    DataFile.WriteField( pValueType, V2.m_X, V2.m_Y );
                    break;
                }
                case prop_type::VECTOR3: 
                {
                    const auto& V3 = m_Data.get<prop_type::VECTOR3>();
                    DataFile.WriteField(pValueType, V3.m_X, V3.m_Y, V3.m_Z);
                    break;
                }
                default: x_assert( false );
            }

            DataFile.WriteLine  ();
        }

        //------------------------------------------------------------------------------------------------
        x_inline
        void SerializeIn    ( xtextfile& DataFile )
        {
            xstring         TypeUID;
            xstring         PropName;

            auto Type = static_cast<prop_type>( DataFile.ReadField( "Type:<?>", &TypeUID ) + 1 );
            x_assert( static_cast<int>(Type) >= static_cast<int>(prop_type::INVALID) );
            x_assert( static_cast<int>(Type) <  static_cast<int>(prop_type::ENUM_COUNT) );
            m_Data.Construct( static_cast<prop_type>(Type) );

            DataFile.ReadFieldXString( "Name:s", m_Name );

            switch( Type )
            {
                case prop_type::BOOL:    DataFile.ReadField         ( getReadTypeString(), &m_Data.get<prop_type::BOOL>() );                                                                break;
                case prop_type::BUTTON:  DataFile.ReadFieldXString  ( getReadTypeString(), m_Data.get<prop_type::BUTTON>().m_Value );                                                       break;
                case prop_type::RGBA:    
                {
                    auto& Color = m_Data.get<prop_type::RGBA>();
                    DataFile.ReadField( getReadTypeString(), &Color.m_R, &Color.m_G, &Color.m_B, &Color.m_A);
                    break;
                }
                case prop_type::ENUM:    DataFile.ReadFieldXString  ( getReadTypeString(), m_Data.get<prop_type::ENUM>()    );                                                              break;
                case prop_type::INT:     DataFile.ReadField         ( getReadTypeString(), &m_Data.get<prop_type::INT>()    );                                                              break;
                case prop_type::FLOAT:   DataFile.ReadField         ( getReadTypeString(), &m_Data.get<prop_type::FLOAT>()  );                                                              break;
                case prop_type::ANGLES3: 
                {
                    xdegree Pitch, Yaw, Roll;
                    DataFile.ReadField( getReadTypeString(), &Pitch.m_Value, &Yaw.m_Value, &Roll.m_Value  );
                    m_Data.get<prop_type::ANGLES3>().setup( xradian{ Pitch }, xradian{ Yaw }, xradian{ Roll } );
                    break;
                }
                case prop_type::STRING:  DataFile.ReadFieldXString( getReadTypeString(), m_Data.get<prop_type::STRING>() );                                                                 break;
                case prop_type::GUID:    DataFile.ReadField       ( getReadTypeString(), &m_Data.get<prop_type::GUID>() );                                                                  break;
                case prop_type::VECTOR2: 
                {
                    auto& V2 = m_Data.get<prop_type::VECTOR2>();
                    DataFile.ReadField( getReadTypeString(), &V2.m_X, &V2.m_Y );
                    break;
                }
                case prop_type::VECTOR3: 
                {
                    auto& V3 = m_Data.get<prop_type::VECTOR3>();
                    DataFile.ReadField( getReadTypeString(), &V3.m_X, &V3.m_Y, &V3.m_Z );
                    break;
                }
                default: x_assert( false );
            }
        }

        //------------------------------------------------------------------------------------------------
        x_inline
        static void RegisterTypes( xtextfile& DataFile ) 
        {
            for( int i=1; i<s_Table.getCount(); i++ )
            {
                auto& Entry = s_Table[i];
                DataFile.AddUserType( Entry.m_pTextFileType, &Entry.m_pStringType[x_constStrLength("Value:")], static_cast<int>(Entry.m_EnumType) );
            }
        }

    protected:

        struct type_info
        {
            prop_type       m_EnumType;
            const char*     m_pStringType;
            const char*     m_pTextFileType;
        };

    protected:

        const char* getWriteTypeString( void ) const
        {
            return &s_Table[m_Data.getTypeIndex()].m_pStringType[x_constStrLength("Value:")];
        }

        const char* getReadTypeString( void ) const
        {
            return s_Table[m_Data.getTypeIndex()].m_pStringType;
        }

        x_constexprvar xarray< type_info, static_cast<xuptr>(prop_type::ENUM_COUNT) > s_Table =
        {{
            { prop_type::INVALID,    "",                 ""                     },
            { prop_type::ANGLES3,    "Value:.ANGLES3",   "fff"                  },
            { prop_type::BOOL,       "Value:.BOOL",      "d"                    },
            { prop_type::BUTTON,     "Value:.BUTTON",    "s"                    },
            { prop_type::ENUM,       "Value:.ENUM",      "e"                    },
            { prop_type::FLOAT,      "Value:.FLOAT",     "f"                    },
            { prop_type::GUID,       "Value:.GUID",      "g"                    },
            { prop_type::INT,        "Value:.INT",       "d"                    },
            { prop_type::RGBA,       "Value:.RGBA",      "cccc"                 },
            { prop_type::STRING,     "Value:.STRING",    "s"                    },
            { prop_type::VECTOR2,    "Value:.VECTOR2",   "ff"                   },
            { prop_type::VECTOR3,    "Value:.VECTOR3",   "fff"                  }
        }};
    };

    //------------------------------------------------------------------------------------------------

    using collection = xvector<xproperty_v2::entry>;

    x_inline 
    void appendSubtractFilterByNameAndValue( xvector<xproperty_v2::entry>& Dest, const xbuffer_view<xproperty_v2::entry> A, const xbuffer_view<xproperty_v2::entry> B )
    {
        //
        // Todo: Filter array type properly
        //
        for( auto& Entry : A )
        {
            bool bFound = false;
            for( auto& FilterEntry : B )
            {
                if( Entry == FilterEntry )
                {
                    bFound = true;
                    break;
                }
            }

            if( !bFound )
            {
                Dest.append() = Entry;
            }
        }
    }

    //--------------------------------------------------------------------------------
    // Enumeration types
    //--------------------------------------------------------------------------------
    struct table;
    namespace enumeration
    {
        //--------------------------------------------------------------------------------
        // Default Queries for properties
        //--------------------------------------------------------------------------------
        template< typename T_TYPE >
        struct queries
        {
            using type = T_TYPE;
            template< typename T >
            static void Query( T_TYPE& UserData, T_TYPE& QueryData, const xproperty_v2::info::base& Details, bool isSend )
            {
                if( isSend )  QueryData = UserData;
                else          UserData  = QueryData;
            }
        };

        template<>
        struct queries<xstring>
        {
            using type = xstring;
            template< typename T >
            static void Query( xstring& UserData, xstring& QueryData, const xproperty_v2::info::base& Details, bool isSend )
            {
                if( isSend )  QueryData.Copy( UserData );
                else          UserData.Copy( QueryData );
            }
        };

        template< typename T_FLAGS >
        struct queries_bool_mask
        {
            using type = typename T_FLAGS::t_type;
            template< typename T >
            static void Query( T& UserData, bool& QueryData, const xproperty_v2::info::boolean& Details, bool isSend )
            {
                if (isSend)  QueryData = !!(static_cast<type>(UserData) & (1 << Details.m_Shift));
                else         UserData  = static_cast<T>
                (
                    QueryData ?
                    (static_cast<type>(UserData) | (static_cast<type>(QueryData) << Details.m_Shift))
                        :
                    (static_cast<type>(UserData) & (~(1u << Details.m_Shift)))
                );
            }
        };

        // Specialize the interger queries to match the data of the user
        // User data can be any interger however the property is always a int
        template<>
        struct queries<int>
        {
            using type = int;
            template< typename T >
            static void Query( T& UserData, int& QueryData, const xproperty_v2::info::s32& Details, bool isSend )
            {
                if( isSend )  QueryData = x_Range( static_cast<int>(UserData),  Details.m_Min, Details.m_Max );
                else          UserData  = x_Range( QueryData,                   Details.m_Min, Details.m_Max );
            }
        };

        // Specialize the float queries to match the data of the user
        template<>
        struct queries<float>
        {
            using type = float;
            template< typename T >
            static void Query( T& UserData, float& QueryData, const xproperty_v2::info::f32& Details, bool isSend )
            {
                if( isSend )  QueryData = x_Range( UserData,  Details.m_Min, Details.m_Max );
                else          UserData  = x_Range( QueryData, Details.m_Min, Details.m_Max );
            }
        };

        template<>
        struct queries<enum_t<u8>>
        {
            using type = enum_t<u8>;
            template< typename T >
            static void Query( T& UserData, xstring& QueryData, const xproperty_v2::info::enum_t& Details, bool isSend )
            {
                if( isSend )
                {
                    for( const auto& EnumDetailEntry : Details.m_List )
                    {
                        if( EnumDetailEntry.first == x_static_cast<int>( UserData ) )
                        {
                            QueryData = EnumDetailEntry.second;
                            return;
                        }
                    }
                }
                else
                {
                    for( const auto& EnumDetailEntry : Details.m_List )
                    {
                        if( QueryData == EnumDetailEntry.second )
                        {
                            UserData = x_static_cast<typename std::remove_reference<decltype(UserData)>::type>(EnumDetailEntry.first);
                            return;
                        }
                    }
                }

                // could not find the enum any more...
                // This could/should be a warning stead... X_LOG it some where...
                x_assert( false );
            }
        };

        //--------------------------------------------------------------------------------
        // Valid User Type
        //--------------------------------------------------------------------------------
        template< typename T_USER_TYPE >
        struct valid_usertype
        {
            using constantless_type = typename std::remove_const<T_USER_TYPE>::type;
            using type              = 
                typename std::conditional
                < 
                    std::is_enum< constantless_type >::value || 
                    xis_instantiation_of< enum_t, constantless_type >::value,                   // Enums
                    enum_t<u8>, 
                    typename std::conditional
                    <
                        std::is_base_of< xdefbits_t, constantless_type >::value,                // booleans
                        bool,
                        typename std::conditional
                        < 
                            std::is_same< constantless_type, bool >::value,                     // booleans
                            bool,
                            typename std::conditional
                            < 
                                std::is_same< constantless_type, u64 >::value,                  // Guid type
                                u64,
                                typename std::conditional
                                < 
                                    std::is_integral<constantless_type>::value,                 // Ints
                                    int, 
                                    typename std::conditional
                                    <
                                        std::is_floating_point<constantless_type>::value,       // Floats
                                        float,
                                        constantless_type 
                                    >::type
                                >::type
                            >::type
                        >::type
                    >::type
                >::type;
            static constexpr const bool value = property_variant::isValidType<type>() || std::is_base_of< tag_enum, type >::value;
        };

        //--------------------------------------------------------------------------------
        // Table Entry Details
        //--------------------------------------------------------------------------------
        namespace table_entry_details
        {
            //--------------------------------------------------------------------------------

            using fn_filter_query                   = bool ( char& Class);
            using fn_property_query                 = void ( char& Class, int& UserData, int& Detail, bool isRecive );
            using fn_property_query_count           = void ( char& Class, int& UserData, bool isRecive );
            using fn_dynamic_query                  = base*( base& );
            using fn_dynamic_list_query             = base*( char& Class, u64 Index, bool isRecive );
            using fn_dynamic_list_iterator_query    = u64  ( char& Class, void*& pNext );
            using fn_atomic_dynamic_list_query      = void ( char& Class, u64 Index, int& UserData, int& Detail, bool isRecive );
 
            //--------------------------------------------------------------------------------

            enum class data_types
            {
                INVALID,
                PROPERTY,                       // Property types 
                SCOPE,
                CLASS_ARRAY,                    // Array of complex structures
                ATOMIC_ARRAY,                   // Array of simple types
                DYNAMIC_SCOPE,                  // Potentially external structures (such pointers to some structure and such)
                DYNAMIC_LIST,                   // Generic list which can be external.. such std:vector, etc
                ATOMIC_DYNAMIC_LIST,            // Atomic Dynamic List
                CHILD_TABLE,                    // Child table
                ENUM_COUNT
            };

            //--------------------------------------------------------------------------------

            struct data_property // Properties
            {

                enum { value = (int)data_types::PROPERTY        };

                fn_property_query*      m_pPropertyQuery;
                int                     m_Offset;
                prop_type               m_PropType;
                details_variant         m_Details{};

                void clear( void )
                {
                    m_pPropertyQuery    = nullptr;
                    m_Offset            = 0;
                    m_PropType          = prop_type::INVALID;
                }

                void SanityCheck( void )
                {
                    x_assert(m_pPropertyQuery);
                }
            };
        
            //--------------------------------------------------------------------------------

            struct data_array // Array
            {
                enum { value = (int)data_types::CLASS_ARRAY     }; 

                fn_property_query*      m_pQueryCount;
                int                     m_Offset;
                int                     m_nSkip;
                int                     m_EntrySize;
                int                     m_Max;

                void clear( void )
                {
                    m_pQueryCount       = nullptr;
                    m_Offset            = 0;
                    m_nSkip             = 0;
                    m_EntrySize         = 0;
                    m_Max               = 0;
                }

                void SanityCheck( void )
                {
                    // assert(m_pQueryCount); // this is not require, the query is optional
                    x_assert(m_Offset>0);
                    x_assert(m_nSkip>=0);
                    x_assert(m_EntrySize>0);
                    x_assert(m_Max>0);
                }
            };

            //--------------------------------------------------------------------------------

            struct data_atomic_array // Atomic Array
            {
                enum { value = (int)data_types::ATOMIC_ARRAY    };

                fn_property_query*          m_pPropertyQuery;
                fn_property_query_count*    m_pQueryCount;
                int                         m_Offset;
                int                         m_EntrySize;
                int                         m_Max;
                prop_type                   m_PropType;
                details_variant             m_Details{};

                void clear( void )
                {
                    m_pPropertyQuery    = nullptr;
                    m_pQueryCount       = nullptr;
                    m_Offset            = 0;
                    m_EntrySize         = 0;
                    m_Max               = 0;
                    m_PropType          = prop_type::INVALID;
                }

                void SanityCheck( void )
                {
                    x_assert(m_pPropertyQuery);
                    x_assert(m_pQueryCount);
                    x_assert(m_Offset>0);
                    x_assert(m_EntrySize>0);
                    x_assert(m_Max>0);
                }
            };

            //--------------------------------------------------------------------------------

            struct data_scopes // Scopes
            {
                enum { value = (int)data_types::SCOPE           };

                int                     m_nSkip;

                void clear( void )
                {
                    m_nSkip         = 0;
                }

                void SanityCheck( void )
                {
                    x_assert(m_nSkip>=0);
                }
            };

            //--------------------------------------------------------------------------------
            // Links a callback which returns a new xproperty::base* which then is search for properties
            // this allows to both rebase and to seach a new table
            struct data_dynamic_scopes // dyanmic Scopes and dyanmic lists
            {
                enum { value = (int)data_types::DYNAMIC_SCOPE   };

                fn_dynamic_query*       m_pDyanmicQuery;

                void clear( void )
                {
                    m_pDyanmicQuery        = nullptr;
                }

                void SanityCheck( void )
                {
                    x_assert(m_pDyanmicQuery);
                }
            };

            //--------------------------------------------------------------------------------

            struct data_dynamic_lists // lists
            {
                 enum { value = (int)data_types::DYNAMIC_LIST    }; 

                fn_dynamic_list_query*              m_pDynamicListQuery;
                fn_property_query_count*            m_pQueryCount;
                fn_dynamic_list_iterator_query*     m_pQuertIterator;
                info::base                          m_Details{};

                void clear( void )
                {
                    m_pDynamicListQuery = nullptr;
                    m_pQueryCount       = nullptr;
                    m_pQuertIterator    = nullptr;
                }

                void SanityCheck( void )
                {
                    x_assert(m_pDynamicListQuery);
                    x_assert(m_pQuertIterator);
                }
            };

            //--------------------------------------------------------------------------------

            struct data_atomic_dynamic_lists // atomic lists
            {
                enum { value = (int)data_types::ATOMIC_DYNAMIC_LIST    }; 

                fn_atomic_dynamic_list_query*       m_pAtomicDynamicListQuery;
                fn_property_query_count*            m_pQueryCount;
                fn_dynamic_list_iterator_query*     m_pQuertIterator;
                prop_type                           m_PropType;
                details_variant                     m_Details{};

                void clear( void )
                {
                    m_pAtomicDynamicListQuery   = nullptr;
                    m_pQueryCount               = nullptr;
                    m_pQuertIterator            = nullptr;
                    m_PropType                  = prop_type::INVALID;
                }

                void SanityCheck( void )
                {
                    x_assert( m_pAtomicDynamicListQuery );
                    x_assert( m_pQuertIterator );
                    x_assert( m_PropType != prop_type::INVALID );
                }
            };

            //--------------------------------------------------------------------------------
            // Links a table in our table
            struct data_child
            {
                enum { value = (int)data_types::CHILD_TABLE     };

                const table*            m_pTable;

                void clear( void )
                {
                    m_pTable            = nullptr;
                }

                void SanityCheck( void )
                {
                    x_assert(m_pTable);
                }
            };

            //--------------------------------------------------------------------------------

            using data = xvariant_enum
            <
                data_types,                 // ENUMERATION TYPE
                char,                       // INVALID 
                data_property,              // PROPERTY
                data_scopes,                // SCOPE
                data_array,                 // CLASS_ARRAY 
                data_atomic_array,          // ATOMIC_ARRAY 
                data_dynamic_scopes,        // DYNAMIC_SCOPE 
                data_dynamic_lists,         // DYNAMIC_LIST 
                data_atomic_dynamic_lists,  // ATOMIC_DYNAMIC_LIST
                data_child                  // CHILD_TABLE
            >;

            // If the enum gets reorder we want to know since the xvariant_enum must match the types
            static_assert( std::is_same< data::enum_to_type<data_types::INVALID                 >::type,    char                        >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::PROPERTY                >::type,    data_property               >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::SCOPE                   >::type,    data_scopes                 >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::CLASS_ARRAY             >::type,    data_array                  >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::ATOMIC_ARRAY            >::type,    data_atomic_array           >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::DYNAMIC_SCOPE           >::type,    data_dynamic_scopes         >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::DYNAMIC_LIST            >::type,    data_dynamic_lists          >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::ATOMIC_DYNAMIC_LIST     >::type,    data_atomic_dynamic_lists   >::value, "" );
            static_assert( std::is_same< data::enum_to_type<data_types::CHILD_TABLE             >::type,    data_child                  >::value, "" );
            static_assert( data::arity == static_cast<int>(data_types::ENUM_COUNT), "" );

        }  // ******* end of namespace table_entry_details ********

        //--------------------------------------------------------------------------------
        struct entry
        {
        public:            
            xconst_str<xchar>                       m_pName;
            table_entry_details::fn_filter_query*   m_pFilter;
        
            //--------------------------------------------------------------------------------
        protected:
            table_entry_details::data               m_Data;

            //--------------------------------------------------------------------------------
        public:            
            
            //---------------------------------------------------------------------------------------
            const auto& getData( void ) const { return m_Data; }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG >
            auto& setupOffset( int Offset )
            {
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                m_Data.get< value >().m_Offset = Offset;

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG >
            auto& setupArray( int Offset, int Max, int EntrySize )
            {
                x_assert( Offset >= 0 );

                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                m_Data.get<value>().m_Offset        = Offset;
                m_Data.get<value>().m_Max           = Max;
                m_Data.get<value>().m_EntrySize     = EntrySize;

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG  >
            auto& setupNSkip( int nSkip )
            {
                x_assert( nSkip >= 0 );

                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                m_Data.get<value>().m_nSkip = nSkip;

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename ...T_ARGS >
            auto& setup ( xconst_str<xchar> pName, T_ARGS&&... Args )
            {
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_pName     = pName;
                m_pFilter   = nullptr; 

                m_Data.Construct< value >();
                m_Data.get< value >().clear();

                setupComplete< T_TAG >( std::forward<T_ARGS>(Args)... );

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T_TYPE >
            typename std::enable_if
            <
                std::is_base_of< xdefbits_t, T_TYPE >::value == true
                , entry&
            >::type
            setupPropertyFunction(void)
            {
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                auto& Data = m_Data.get<value>();

                Data.m_pPropertyQuery = (enumeration::table_entry_details::fn_property_query*)queries_bool_mask< T_TYPE >::
                    template Query< typename std::remove_const<T_TYPE>::type::t_type >;

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T_TYPE >
            typename std::enable_if
            <
                std::is_base_of< xdefbits_t, T_TYPE >::value == false
                , entry&
            >::type
            setupPropertyFunction( void )
            { 
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                auto& Data = m_Data.get<value>();

                Data.m_pPropertyQuery = (enumeration::table_entry_details::fn_property_query*)queries< typename valid_usertype<T_TYPE>::type >::
                        template Query< typename std::remove_const<T_TYPE>::type >;

                return *this;
            }
            
            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T_TYPE >
            typename std::enable_if
            <
                std::is_enum<T_TYPE>::value == false 
                , entry&
            >::type
            setupPropertyType( void ) 
            { 
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                auto& Type = m_Data.get<value>();
                Type.m_PropType = static_cast<prop_type>( type_to_enum< typename valid_usertype< T_TYPE >::type >::value );

                return *this;
            }

            template< typename T_TAG, typename T_TYPE >
            typename std::enable_if
            <
                xis_instantiation_of<xproperty_v2::enum_t, T_TYPE>::value == true
                || std::is_enum<T_TYPE>::value == true 
                , entry&
            >::type
            setupPropertyType( void ) 
            { 
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_Data.get<value>().m_PropType = prop_type::ENUM;

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T_TYPE >
            typename std::enable_if
            <
                xis_instantiation_of<xproperty_v2::enum_t, T_TYPE>::value == false 
                && std::is_enum<T_TYPE>::value == false
                , entry&
            >::type
            setupDetails( void ) 
            { 
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                auto& Details = m_Data.get<value>().m_Details;

                if( Details.isValid() == false )
                {
                    using details =typename xproperty_v2::type_to_enum
                    < 
                        typename valid_usertype< T_TYPE >::type 
                    >::detail; 

                    info::base B = *(info::base*)Details.getRaw(); 
                    Details.template Construct<details>();
                    Details.get<details>().m_Help  = B.m_Help;
                    Details.get<details>().m_Flags = B.m_Flags;
                }

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T_TYPE >
            typename std::enable_if
            <
                xis_instantiation_of<xproperty_v2::enum_t, T_TYPE>::value == true 
                || std::is_enum<T_TYPE>::value == true
                , entry&
            >::type
            setupDetails( void ) 
            { 
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                auto& Details = m_Data.get<value>().m_Details;

                if( Details.isValid() == false )
                    Details.template Construct
                    < 
                        xproperty_v2::info::enum_t 
                    >();

                return *this;
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG >
            void setupEnd( void ) 
            { 
                static constexpr auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                m_Data.get<value>().SanityCheck();
            }

            //---------------------------------------------------------------------------------------
        protected:

            //---------------------------------------------------------------------------------------
            template< typename T_TAG >
            void setupComplete( void ) {}

            //---------------------------------------------------------------------------------------
            // Details with property flags
            template< typename T_TAG, typename T, typename ...T_ARGS > 
            typename std::enable_if
            <
                std::is_convertible< T, flags >::value 
                ,   void
            >::type
            setupComplete( T&& Flags, T_ARGS&&... Args )
            {
                static_assert(  std::is_same<T_TAG, enumeration::table_entry_details::data_property         >::value 
                            ||  std::is_same<T_TAG, enumeration::table_entry_details::data_atomic_array     >::value
                            ||  std::is_same<T_TAG, enumeration::table_entry_details::data_dynamic_lists    >::value, "Only atomic arrays and properties can have Flags" );

                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                auto& Details = m_Data.get<value>();
                ((info::base*)&Details.m_Details)->m_Flags = Flags;
                setupComplete<T_TAG>( std::forward<T_ARGS>( Args )... );
            }

            //---------------------------------------------------------------------------------------
            // Details with property help
            template< typename T_TAG, typename T, typename ...T_ARGS > 
            typename std::enable_if
            <
                std::is_same< T, xconst_str<xchar> >::value
                || std::is_same< T, const xconst_str<xchar>& >::value
                ,   void
            >::type
            setupComplete( T&& Help, T_ARGS&&... Args )
            {
                static_assert( std::is_same<T_TAG, enumeration::table_entry_details::data_property >::value 
                           ||  std::is_same<T_TAG, enumeration::table_entry_details::data_atomic_array     >::value, "Only atomic arrays and properties can have Help" );
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                
                info::base& Base = *(info::base*)m_Data.get<value>().m_Details.getRaw();
                Base.m_Help = Help;
                setupComplete<T_TAG>( std::forward<T_ARGS>( Args )... );
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T, typename ...T_ARGS > 
            typename std::enable_if
            <
                xis_funtion_ptr< T >::value
                &&  std::is_same< typename xreturn_type< T >::type, bool >::value
                &&  xfunction_traits<T>::t_arg_count == 1
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_arg<0>::type >::value
                ,   void
            >::type
            setupComplete( T&& Callback, T_ARGS&&... Args )
            {
                m_pFilter = (enumeration::table_entry_details::fn_filter_query*)Callback;
                setupComplete<T_TAG>( std::forward<T_ARGS>( Args )... );
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T, typename ...T_ARGS >
            typename std::enable_if
            <
                (
                        std::is_same<T_TAG, enumeration::table_entry_details::data_array                    >::value 
                    ||  std::is_same<T_TAG, enumeration::table_entry_details::data_atomic_array             >::value
                    ||  std::is_same<T_TAG, enumeration::table_entry_details::data_dynamic_lists            >::value 
                    ||  std::is_same<T_TAG, enumeration::table_entry_details::data_atomic_dynamic_lists     >::value 
                )
                &&  xis_funtion_ptr< T >::value
                &&  std::is_same< typename xreturn_type< T >::type, void >::value
                &&  xfunction_traits<T>::t_arg_count == 3
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<0>::type    >::value
                &&  std::is_same< typename xfunction_traits<T>::template t_safe_arg<1>::type, int&          >::value
                &&  std::is_same< typename xfunction_traits<T>::template t_safe_arg<2>::type, bool          >::value
                ,   void
            >::type
            setupComplete( T&& Callback, T_ARGS&&... Args )
            {
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_Data.get<value>().m_pQueryCount = (enumeration::table_entry_details::fn_property_query_count*)Callback;
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }

            //---------------------------------------------------------------------------------------

            template< typename T_TAG, typename T_CALLBACK, typename ...T_ARGS >
            typename std::enable_if
            <
                !(
                        std::is_same< T_TAG, enumeration::table_entry_details::data_array                   >::value 
                    ||  std::is_same< T_TAG, enumeration::table_entry_details::data_atomic_array            >::value        // Atomic Arrays use: setupPropertyFunction
                    ||  std::is_same< T_TAG, enumeration::table_entry_details::data_dynamic_lists           >::value 
                    ||  std::is_same< T_TAG, enumeration::table_entry_details::data_atomic_dynamic_lists    >::value 
                )
                &&  xis_funtion_ptr         < T_CALLBACK                                                                 >::value
                &&  std::is_same            < typename xreturn_type< T_CALLBACK >::type, void                            >::value
                &&  xfunction_traits        < T_CALLBACK                                                                 >::t_arg_count == 4
                &&  std::is_lvalue_reference< typename xfunction_traits<T_CALLBACK>::template t_safe_arg<0>::type        >::value
                &&  std::is_lvalue_reference< typename xfunction_traits<T_CALLBACK>::template t_safe_arg<1>::type        >::value
                &&  std::is_lvalue_reference< typename xfunction_traits<T_CALLBACK>::template t_safe_arg<2>::type        >::value
                &&  std::is_same            < typename xfunction_traits<T_CALLBACK>::template t_safe_arg<3>::type, bool  >::value
                ,   void
            >::type
            setupComplete( T_CALLBACK&& Callback, T_ARGS&&... Args )
            {
                x_constexprvar auto value  = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                x_constexprvar auto value2 = static_cast<table_entry_details::data::enum_t>(std::remove_reference<typename xfunction_traits<T_CALLBACK>::template t_safe_arg<2>::type>::type::value);
                x_constexprvar auto value3 = static_cast<table_entry_details::data::enum_t>(type_to_enum<typename std::remove_reference<typename xfunction_traits<T_CALLBACK>::template t_safe_arg<1>::type>::type>::value);
                
                static_assert( value2 == value3, "ERROR - Please note the callback function should be of type: void( TYPE& UserData, TYPE& QueryData, const xproperty_v2::detail::DETAIL& Details, bool isSend )" );

                m_Data.get<value>().m_pPropertyQuery = (enumeration::table_entry_details::fn_property_query*)Callback;
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T, typename ...T_ARGS >
            typename std::enable_if
            <
                xis_funtion_ptr< T >::value
                &&  std::is_same< typename xreturn_type< T >::type, xproperty_v2::base* >::value
                &&  xfunction_traits<T>::t_arg_count == 1
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<0>::type >::value
                , void
            >::type
            setupComplete( T&& Callback, T_ARGS&&... Args )
            {
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_Data.get<value>().m_pDyanmicQuery = (enumeration::table_entry_details::fn_dynamic_query*)Callback;
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T, typename ...T_ARGS >
            typename std::enable_if
            <
                xis_funtion_ptr< T >::value
                &&  std::is_same< typename xreturn_type< T >::type, xproperty_v2::base* >::value
                &&  xfunction_traits<T>::t_arg_count == 3
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<0>::type        >::value
                &&  std::is_same            < typename xfunction_traits<T>::template t_safe_arg<1>::type, u64   >::value
                &&  std::is_same            < typename xfunction_traits<T>::template t_safe_arg<2>::type, bool  >::value
                ,   void
            >::type
            setupComplete( T&& Callback, T_ARGS&&... Args )
            {
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_Data.get<value>().m_pDynamicListQuery = (enumeration::table_entry_details::fn_dynamic_list_query*)Callback;
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T, typename ...T_ARGS >
            typename std::enable_if
            <
                xis_funtion_ptr< T >::value
                &&  std::is_same< typename xreturn_type< T >::type, void >::value
                &&  xfunction_traits<T>::t_arg_count == 5
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<0>::type        >::value
                &&  std::is_same            < typename xfunction_traits<T>::template t_safe_arg<1>::type, u64   >::value
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<2>::type        >::value
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<3>::type        >::value
                &&  std::is_same            < typename xfunction_traits<T>::template t_safe_arg<4>::type, bool  >::value
                ,   void
            >::type
            setupComplete( T&& Callback, T_ARGS&&... Args )
            {
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_Data.get<value>().m_pAtomicDynamicListQuery = (enumeration::table_entry_details::fn_atomic_dynamic_list_query*)Callback;
                
                // Setup the user type
                using user_type = valid_usertype
                < 
                    std::remove_reference
                    <
                        typename xfunction_traits<T>::template t_safe_arg<2>::type
                    >::type 
                >::type;

                m_Data.get<value>().m_PropType = static_cast<prop_type>( type_to_enum< typename valid_usertype< user_type >::type >::value );

                // Continue
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename T, typename ...T_ARGS >
            typename std::enable_if
            <
                xis_funtion_ptr             < T                                                             >::value
                &&  std::is_same            < typename xreturn_type< T >::type, u64                         >::value
                &&      xfunction_traits    < T                                                             >::t_arg_count == 2
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<0>::type    >::value
                &&  std::is_lvalue_reference< typename xfunction_traits<T>::template t_safe_arg<1>::type    >::value
                ,   void
            >::type
            setupComplete( T&& Callback, T_ARGS&&... Args )
            {
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_Data.get<value>().m_pQuertIterator = (enumeration::table_entry_details::fn_dynamic_list_iterator_query*)Callback;
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }

            //---------------------------------------------------------------------------------------
            // Deals with property Info
            template< typename T_TAG, typename T, typename ...T_ARGS >
            typename std::enable_if
            <
                std::is_base_of< xproperty_v2::info::base, T >::value
                && ( std::is_same<T_TAG, enumeration::table_entry_details::data_property        >::value
                ||   std::is_same<T_TAG, enumeration::table_entry_details::data_atomic_array    >::value )       // Atomic Arrays use: setupPropertyFunction
                ,   void
            >::type
            setupComplete( const T& Detail, T_ARGS&&... Args )
            {
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);
                m_Data.get<value>().m_Details.template Construct<T>( Detail );
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }

            //---------------------------------------------------------------------------------------
            template< typename T_TAG, typename ...T_ARGS >
            typename std::enable_if
            <
                std::is_same< T_TAG, xproperty_v2::enumeration::table_entry_details::data_child >::value
                , void
            >::type
            setupComplete( const table& ChildTable, T_ARGS&&... Args )
            {
                x_constexprvar auto value = static_cast<table_entry_details::data::enum_t>(T_TAG::value);

                m_Data.get<value>().m_pTable = &ChildTable;
                setupComplete<T_TAG>( std::forward<T_ARGS>(Args)... );
            }
        };    

    } //  ****** end of namespace enumeration ********* 

    //---------------------------------------------------------------------------------------
    // base
    //---------------------------------------------------------------------------------------
    struct base
    {
        x_object_type( xproperty_v2::base, is_linear, rtti_start );

        using prop_table = const xproperty_v2::table;

        virtual prop_table& onPropertyTable( void ) const = 0;
        
        template< typename T_TYPE >
        typename std::enable_if< std::is_same< typename enumeration::valid_usertype<T_TYPE>::type, T_TYPE >::value, bool >::type
        SetProperty( const char* pName, T_TYPE Var );

        template< typename T >
        typename std::enable_if 
        < 
                std::is_same< typename xfunction_traits<T>::t_return_type, const xproperty_v2::entry* >::value 
            &&  xfunction_traits<T>::t_arg_count == 1 
            &&  std::is_same< typename xfunction_traits<T>::template t_safe_arg<0>::type, int >::value
            ,   void 
        >::type
        SetProperties( T&& Callback );

        template< typename T_TYPE >
        typename std::enable_if< std::is_same< typename enumeration::valid_usertype<T_TYPE>::type, T_TYPE >::value, std::pair<bool, T_TYPE> >::type
        GetProperty( const char* pName ) const;

        template< typename T >
        typename std::enable_if 
        < 
                std::is_same< typename xfunction_traits<T>::t_return_type, xproperty_v2::entry* >::value 
            &&  xfunction_traits<T>::t_arg_count == 1 
            &&  std::is_same< typename xfunction_traits<T>::template t_safe_arg<0>::type, int >::value
            ,   void 
        >::type
        GetProperties( T&& Callback ) const;

        enum class enum_mode : u8 
        {
            SAVE,
            REALTIME 
        };

        template< typename T >
        typename std::enable_if< std::is_same< typename xfunction_traits<T>::t_return_type, xproperty_v2::entry& >::value && xfunction_traits<T>::t_arg_count == 0, void >::type
        EnumProperty( T&& CallBack, bool bFullPath, enum_mode Mode = enum_mode::REALTIME ) const;

        inline void Save( xtextfile& File, bool bFullPath = true ) const;
        inline bool Load( xtextfile& File );
    };
    
    //--------------------------------------------------------------------------------
    // Table
    //--------------------------------------------------------------------------------
    struct table
    {
        using entry = enumeration::entry;

        template< typename T_CLASS, typename T >
        table(T_CLASS* pClass, const base* pBase, xconst_str<xchar> pName, T&& Func) :
            m_pTempBase{ pBase },
            m_Count{ 0 },
            m_BaseOffset{ static_cast<int>(reinterpret_cast<const char*>(pClass) - reinterpret_cast<const char*>(pBase)) }
        {
            // Let the user add properties
            if( pName.m_pValue ) AddScope( pName, std::forward<T>(Func) );
            else                 Func( *this );
        }
    
        template< typename T_CLASS, typename T >
        table(T_CLASS* pClass, const base* pBase, T&& Func) : table{ pClass, pBase, {nullptr}, std::forward<T&&>(Func) } {}

        template< typename T_CLASS, typename T >
        table(T_CLASS* pClass, xconst_str<xchar> pName, T&& Func) : table{ pClass, pClass, pName, std::forward<T&&>(Func) } {}

        template< typename T_CLASS, typename T >
        table(T_CLASS* pClass, T&& Func) : table{ pClass, pClass, {nullptr}, std::forward<T&&>(Func) } {}

        int             m_Count;
        const int       m_BaseOffset;        
        entry           m_pTable[32];

        const base*     m_pTempBase;
    
        //---------------------------------------------------------------------------------------
        template< typename T_USER_TYPE, typename ...T_ARGS >
        typename std::enable_if
        <
            enumeration::valid_usertype<T_USER_TYPE>::value
            ,   void
        >::type
        AddProperty( xconst_str<xchar> pName, T_USER_TYPE& Int, T_ARGS&&... Args )
        {
            x_assert( pName.m_pValue );

            m_pTable[m_Count++]
                .template setup                 < enumeration::table_entry_details::data_property               >   ( pName, std::forward<T_ARGS>(Args)... )
                .template setupOffset           < enumeration::table_entry_details::data_property               >   ( int((char*)&Int - (char*)m_pTempBase) )
                .template setupPropertyFunction < enumeration::table_entry_details::data_property, T_USER_TYPE  >   ()
                .template setupPropertyType     < enumeration::table_entry_details::data_property, T_USER_TYPE  >   ()
                .template setupDetails          < enumeration::table_entry_details::data_property, T_USER_TYPE  >   ()
                .template setupEnd              < enumeration::table_entry_details::data_property               >   ()
            ;
        }
    
        //---------------------------------------------------------------------------------------
        template< typename T_USER_TYPE, typename ...T_ARGS >
        void AddProperty( xconst_str<xchar> pName, T_ARGS&&... Args )
        {
            x_assert( pName.m_pValue );
            m_pTable[m_Count++]
                .template setup             < enumeration::table_entry_details::data_property               >   ( pName, std::forward<T_ARGS>(Args)... )
                .template setupOffset       < enumeration::table_entry_details::data_property               >   ( m_BaseOffset )
                .template setupPropertyType < enumeration::table_entry_details::data_property, T_USER_TYPE  >   ()
                .template setupDetails      < enumeration::table_entry_details::data_property, T_USER_TYPE  >   ()
                .template setupEnd          < enumeration::table_entry_details::data_property               >   ()
            ;
        }
    
        //---------------------------------------------------------------------------------------
        template< typename T_LAMBDA, typename ...T_ARGS >
        typename std::enable_if< !xis_funtion_ptr< T_LAMBDA >::value, void >::type
        AddScope( xconst_str<xchar> pName, T_LAMBDA&& Funcs, T_ARGS&&... Args )
        {
            const int c = m_Count++;

            // let the user to continue enumerating
            Funcs( *this );

            // Now we can set our entry;
            m_pTable[c]
                .template setup     < enumeration::table_entry_details::data_scopes >   ( pName, std::forward<T_ARGS>(Args)... )
                .template setupNSkip< enumeration::table_entry_details::data_scopes >   ( m_Count - c - 2 )
                .template setupEnd  < enumeration::table_entry_details::data_scopes >   ()
            ;
        }
    
        //---------------------------------------------------------------------------------------
        template<typename T_ARRAY_TYPE, typename T_LAMBDA, typename ...T_ARGS >
        typename std::enable_if
        <
            !xis_funtion_ptr< T_ARRAY_TYPE  >::value 
            &&  !xis_funtion_ptr< T_LAMBDA      >::value
            &&  xis_callable    < T_LAMBDA      >::value
            ,   void
        >::type
        AddArray( xconst_str<xchar> pName, const T_ARRAY_TYPE* pArray, int count, T_LAMBDA&& Funcs, T_ARGS&&... Args )
        {
            x_assert( count >= 1 );
            x_assert( pArray );
        
            const int c = m_Count++;

            // let the user to continue enumerating
            {
                auto pTemp = m_pTempBase;
                m_pTempBase = reinterpret_cast<const base*>(pArray);
                Funcs( *this );
                m_pTempBase = pTemp;
            }

            // setup the array
            m_pTable[c]
                .template setup         < enumeration::table_entry_details::data_array  >   ( pName, std::forward<T_ARGS>(Args)... )
                .template setupArray    < enumeration::table_entry_details::data_array  >   ( int((char*)pArray - (char*)m_pTempBase), count, sizeof(T_ARRAY_TYPE) )
                .template setupNSkip    < enumeration::table_entry_details::data_array  >   ( m_Count - c - 2 )
                .template setupEnd      < enumeration::table_entry_details::data_array  >   ()
            ;
        }
        
        //---------------------------------------------------------------------------------------
        template< typename T_ARRAY_TYPE, typename ...T_ARGS >
        typename std::enable_if
        <
            !xis_funtion_ptr< T_ARRAY_TYPE >::value
            ,   void
        >::type
        AddArray( xconst_str<xchar> pName, const T_ARRAY_TYPE* pArray, int count, T_ARGS&&... Args )
        {
            x_assert( pArray );
            x_assert( count>0 );

            m_pTable[m_Count++]
                .template setup                 < enumeration::table_entry_details::data_atomic_array               >   ( pName, std::forward<T_ARGS>(Args)... )
                .template setupArray            < enumeration::table_entry_details::data_atomic_array               >   ( int((char*)pArray - (char*)m_pTempBase), count, sizeof(T_ARRAY_TYPE) )
                .template setupPropertyFunction < enumeration::table_entry_details::data_atomic_array, T_ARRAY_TYPE >   ()
                .template setupPropertyType     < enumeration::table_entry_details::data_atomic_array, T_ARRAY_TYPE >   ()
                .template setupDetails          < enumeration::table_entry_details::data_atomic_array, T_ARRAY_TYPE >   ()
                .template setupEnd              < enumeration::table_entry_details::data_atomic_array               >   ()
            ; 
        }

        //---------------------------------------------------------------------------------------
        template< typename ...T_ARG >
        void AddDynamicScope( xconst_str<xchar> pName, T_ARG&&... Args )
        {
            m_pTable[m_Count++]
                .template setup     < enumeration::table_entry_details::data_dynamic_scopes >           ( pName, std::forward<T_ARG>(Args)... )
                .template setupEnd  < enumeration::table_entry_details::data_dynamic_scopes >           ()
            ;
        }
    
        //---------------------------------------------------------------------------------------
        template< typename ...T_ARG >
        void AddDynamicList( xconst_str<xchar> pName, T_ARG&&... Args )
        {
            m_pTable[m_Count++]
                .template setup     < enumeration::table_entry_details::data_dynamic_lists >            ( pName, std::forward<T_ARG>(Args)... )
                .template setupEnd  < enumeration::table_entry_details::data_dynamic_lists >            ()
            ;
        }

        //---------------------------------------------------------------------------------------
        template< typename ...T_ARG >
        void AddAtomicDynamicList( xconst_str<xchar> pName, T_ARG&&... Args )
        {
            m_pTable[m_Count++]
                .template setup     < enumeration::table_entry_details::data_atomic_dynamic_lists >     ( pName, std::forward<T_ARG>(Args)... )
                .template setupEnd  < enumeration::table_entry_details::data_atomic_dynamic_lists >     ()
            ;
        }

        //---------------------------------------------------------------------------------------
        template< typename ...T_ARG >
        void AddChildTable( const xproperty_v2::table& ChildTable, T_ARG&&... Args )
        {
            m_pTable[m_Count++]
                .template setup     < xproperty_v2::enumeration::table_entry_details::data_child >      ({ nullptr }, ChildTable, std::forward<T_ARG>(Args)...)
                .template setupEnd  < xproperty_v2::enumeration::table_entry_details::data_child >      ()
            ;
        }

        //---------------------------------------------------------------------------------------

        template< typename T_BASE,  typename T_TYPE >
        bool QueryFromTable( T_BASE* const pProperty, const char* pName, T_TYPE& Var, const bool isSend ) const
        {
            //---------------------------------------------------------------------------------------
            x_constexprvar auto SearchPath = [](const char* pFullString, int FullIndex, const char* pPartialString )
            {
                if( nullptr == pPartialString ) return -1;

                int i = 0;
                for (; pFullString[FullIndex + i] == pPartialString[i] && pPartialString[i]; i++);
                if (pFullString[FullIndex + i] == pPartialString[i]) return i;
                if (pFullString[FullIndex + i] == '/') return i+1;
                if (pFullString[FullIndex + i] == '[') return i+1;
                if (pFullString[FullIndex + i] == '(') return i+1;
                return -1;
            };
            
            //---------------------------------------------------------------------------------------
            x_constexprvar auto getIndex = []( const char* pName, int& c )
            {
                int mul     = 1;
                int iArray  = 0;
                
                x_assert( isdigit(pName[ c ]) );
                do
                {
                    iArray += (pName[c++] -'0')*mul;
                    if( isdigit(pName[ c ] ) == false ) break;
                    mul *= 10;
                } while(true);
                
                x_assert( pName[ c ] == ']' );
                return iArray;
            };

            //---------------------------------------------------------------------------------------
            x_constexprvar auto getGUID = []( const char* pName, int& i )
            {
                char c = pName[ i ];
                u32 N=0;
                while( (c>='0'&&c<='9') || (c>='a'&&c<='f') || (c>='A'&&c<='F') )
                {
                    s32 v;
                    if( c>'9' ) v = (x_tolower(c)-'a')+10;
                    else        v = (x_tolower(c)-'0');
                    N <<= 4;
                    N |= (v&0xF);
                    c = pName[++i];
                }

                x_assert( c ==':' );

                const u64 Val1 = N;

                // read second part 
                c = pName[ ++i ];
                N=0;
                while( (c>='0'&&c<='9') || (c>='a'&&c<='f') || (c>='A'&&c<='F') )
                {
                    s32 v;
                    if( c>'9' ) v = (x_tolower(c)-'a')+10;
                    else        v = (x_tolower(c)-'0');
                    N <<= 4;
                    N |= (v&0xF);
                    c = pName[ ++i ];
                }

                const u64 Val2 = N;

                x_assert( pName[ i ] == ')' );

                // Combine both numbers and set the final value
                return (Val1<<32)|Val2;
            };

            //---------------------------------------------------------------------------------------
            x_constexprvar auto FilterProps = []( const enumeration::entry& Prop, int& i )
            {
                auto& Data = Prop.getData();
                switch( Data.getTypeIndex() )
                {
                    case enumeration::table_entry_details::data_types::SCOPE:
                        i += Data.get<enumeration::table_entry_details::data_types::SCOPE>().m_nSkip;
                        break;
                    case enumeration::table_entry_details::data_types::CLASS_ARRAY:
                        i += Data.get<enumeration::table_entry_details::data_types::CLASS_ARRAY>().m_nSkip;
                        break;
                    default:
                        break;
                }
            };
            
            //---------------------------------------------------------------------------------------
            int     ArrayBase   = 0;
            int     c           = 0;
    
            for (int i = 0; i < m_Count; i++)
            {
                const auto& Prop = m_pTable[i];

                // if the property wants us to filter it out we will
                if( Prop.m_pFilter && false == Prop.m_pFilter( (char&)*(((char*)pProperty) + m_BaseOffset ) ) )
                {
                    FilterProps( Prop, i );
                    continue;
                }
        
                auto& Data = Prop.getData();
                switch( Data.getTypeIndex() )
                {
                    case enumeration::table_entry_details::data_types::DYNAMIC_SCOPE:
                    {
                        const auto& DynamicScopes = Data.get<enumeration::table_entry_details::data_types::DYNAMIC_SCOPE>();    

                        auto* pPropperties = DynamicScopes.m_pDyanmicQuery( (base&)(*(((char*)pProperty) + m_BaseOffset )) );
                        if( pPropperties && pPropperties->onPropertyTable().QueryFromTable( pPropperties, &pName[c], Var, isSend ) ) return true;
                        continue;
                    }
                    case enumeration::table_entry_details::data_types::CHILD_TABLE:
                    {
                        const auto& ChildTable = Data.get<enumeration::table_entry_details::data_types::CHILD_TABLE>(); 
                        if( const_cast<table*>(ChildTable.m_pTable)->QueryFromTable( pProperty, &pName[c], Var, isSend ) ) return true;
                        continue;
                    }
                    default:
                        break;
                }
        
                const int k = SearchPath(pName, c, Prop.m_pName.m_pValue);
                if (k < 0)
                {
                    FilterProps( Prop, i );
                    continue;
                }
        
                // Set the property
                switch( Data.getTypeIndex() )
                {
                    case enumeration::table_entry_details::data_types::ATOMIC_ARRAY:
                    {
                        const auto& AtomicArray = Data.get<enumeration::table_entry_details::data_types::ATOMIC_ARRAY>();   
                        c += k;

                        x_assert( type_to_enum<T_TYPE>::value == (int)AtomicArray.m_PropType );

                        if( pName[c-1] == '[' && pName[c] == ']' )
                        {
                            x_assert(pName[c+1]==0);
                            if( AtomicArray.m_pQueryCount) AtomicArray.m_pQueryCount( (char&)*(((char*)pProperty) + m_BaseOffset ), (int&)Var, isSend );
//                            assert( static_cast<int>(Var) < AtomicArray.m_Max );
                        }
                        else
                        {
                            const int iArray = getIndex( pName, c );
                            c+= 1;   //skip ']' 
                            ArrayBase += AtomicArray.m_Offset + AtomicArray.m_EntrySize * iArray;
                            AtomicArray.m_pPropertyQuery( *(((char*)pProperty) + ArrayBase ), (int&)Var, (int&)AtomicArray.m_Details, isSend );
                        }
                        return true;
                    }
                    case enumeration::table_entry_details::data_types::CLASS_ARRAY:
                    {
                        const auto& ClassArray = Data.get<enumeration::table_entry_details::data_types::CLASS_ARRAY>();     
                        c += k;

                        if( pName[c-1] == '[' && pName[c] == ']' )
                        {
                            x_assert(pName[c+1]==0);
                            if (ClassArray.m_pQueryCount)
                            {
                                ClassArray.m_pQueryCount((char&)*(((char*)pProperty) + m_BaseOffset ), (int&)Var, (int&)ClassArray.m_Max, isSend);
    //                            assert(static_cast<int>(Var) < ClassArray.m_Max);
                            }
                        }
                        else
                        {
                            const int iArray = getIndex( pName, c );
                            c+= 2;   //skip ']' '/'             
                            ArrayBase += ClassArray.m_Offset + ClassArray.m_EntrySize * iArray;
                        }
                        continue;
                    }
                    case enumeration::table_entry_details::data_types::SCOPE:
                    {
                        c += k;
                        continue;
                    }
                    case enumeration::table_entry_details::data_types::DYNAMIC_LIST:
                    {
                        const auto& DynamicList = Data.get<enumeration::table_entry_details::data_types::DYNAMIC_LIST>();   

                        c += k;
                        if( pName[c-1] == '[' && pName[c] == ']' )
                        {
                            x_assert(pName[c+1]==0);
                            if( DynamicList.m_pQueryCount) DynamicList.m_pQueryCount( (char&)*(((char*)pProperty) + m_BaseOffset ), (int&)Var, isSend  );
                            return true;
                        }
                        else
                        {
                            const u64 iArray = ( pName[c-1] == '[' ) ? getIndex( pName, c ) : getGUID( pName, c );
                            c += 2;  // skip ']' and '/'
                            auto* pPropperties = DynamicList.m_pDynamicListQuery( (char&)*(((char*)pProperty) + m_BaseOffset ), iArray, isSend );
                            return pPropperties && pPropperties->onPropertyTable().QueryFromTable( pPropperties, &pName[c], Var, isSend );
                        }
                    }
                    case enumeration::table_entry_details::data_types::ATOMIC_DYNAMIC_LIST:
                    {
                        const auto& DynamicList = Data.get<enumeration::table_entry_details::data_types::ATOMIC_DYNAMIC_LIST>();   

                        c += k;
                        if( pName[c-1] == '[' && pName[c] == ']' )
                        {
                            x_assert(pName[c+1]==0);
                            if( DynamicList.m_pQueryCount) DynamicList.m_pQueryCount( (char&)*(((char*)pProperty) + m_BaseOffset ), (int&)Var, isSend  );
                            return true;
                        }
                        else
                        {
                            const u64 iArray = ( pName[c-1] == '[' ) ? getIndex( pName, c ) : getGUID( pName, c );
                            c += 2;  // skip ']' and '/'
                            DynamicList.m_pAtomicDynamicListQuery( (char&)*(((char*)pProperty) + m_BaseOffset ), iArray, (int&)Var, (int&)DynamicList.m_Details, isSend );
                            return true;
                        }
                    }
                    case enumeration::table_entry_details::data_types::PROPERTY:
                    {
                        auto& Properties = Data.get<enumeration::table_entry_details::data_types::PROPERTY>();  

                        x_assert( type_to_enum<T_TYPE>::value == (int)Properties.m_PropType );

                        Properties.m_pPropertyQuery( *(((char*)pProperty) + ArrayBase + Properties.m_Offset ), (int&)Var, (int&)Properties.m_Details, isSend );
                        return true;
                    }
                    default:
                    {
                        x_assert(false);
                    }
                    break;
                }
            }
    
            return false;
        }

        //---------------------------------------------------------------------------------------

        template< typename T_BASE >
        struct query_lookup_entry
        {
            template< typename T >
            struct callback_struct
            {
                template< int TYPE_INDEX > x_forceinline
                static void Callback( const table& This, T_BASE& Base, xproperty_v2::entry& Entry, bool& isGet, bool& R ) 
                { 
                    constexpr static const auto Enum = static_cast<property_variant::enum_t>(TYPE_INDEX);
                    if (isGet) Entry.m_Data.Construct<Enum>();
                    R = This.QueryFromTable( &Base, Entry.m_Name, Entry.m_Data.get< Enum >(), isGet );
                }

                // first entry is invalid 
                template<> x_forceinline
                static void Callback<0>( const table& This, T_BASE& Base, xproperty_v2::entry& Entry, bool& isGet, bool& R) 
                {
                    x_assert(false);
                    R = false;
                }
            };
        };

        template< typename T_BASE, typename T > inline
        void QueryProperties( T_BASE* const pBase, bool isGet, T&& Callback ) const
        {
            int Index = 0;
            for( xproperty_v2::entry* pEntry = const_cast<xproperty_v2::entry*>(Callback(Index++)); pEntry; pEntry = const_cast<xproperty_v2::entry*>(Callback(Index++)) )
            {
                using tuple  = typename property_variant::tuple;
                using checks = xtable_build_templated_callbacks< query_lookup_entry<T_BASE>::callback_struct,  tuple >;
                static constexpr const checks t{};

                bool bWeGotIt;
                xtable_callback( t, pEntry->m_Data.getTypeIndex<int>(), *this, *pBase, *pEntry, isGet, bWeGotIt );
                if( bWeGotIt == false )
                {
                    X_LOG( "Fail to %s a property: %s", (isGet ? "get" : "set"), (const char*)pEntry->m_Name );
                }
            }
        }

        //---------------------------------------------------------------------------------------
        template< typename T, typename T_FILTER_CALLBACK >
        void getPropertyEntries( char* const pProperty, T&& Callback, const xstring& Path, int& i, int BaseOffset, int Count, T_FILTER_CALLBACK&& FilterCallback )
        {
            constexpr static const bool  isSend = true;

            while( i < Count )
            {
                const auto& Prop = m_pTable[i++];

                if (Prop.m_pFilter && false == Prop.m_pFilter((char&)*(((char*)pProperty) + m_BaseOffset)))
                    continue;

                auto& Data = Prop.getData();
                switch( Data.getTypeIndex() )
                {
                    case enumeration::table_entry_details::data_types::PROPERTY:
                    {
                        auto&       Property = Data.get<enumeration::table_entry_details::data_types::PROPERTY>();  
                        if( FilterCallback( ((info::base*)Property.m_Details.getRaw())->m_Flags ) )
                        {
                            const auto  offset = BaseOffset + Property.m_Offset;

                            // Get a free entry
                            auto&       Entry    = Callback();

                            // Set the name of the property
                            Entry.m_Name.setup( "%s%s", (const char*)Path, Prop.m_pName );    

                            // Set the value of the property
                            Property.m_pPropertyQuery( 
                                    *( pProperty + offset ) 
                                ,   (int&)*(int*)Entry.m_Data.Construct(Property.m_PropType)
                                ,   (int&)Property.m_Details
                                ,   isSend );

                            // Set the details
                            Entry.m_pDetails = (info::base*)&Property.m_Details;
                        }
                    }
                    break;
                    case enumeration::table_entry_details::data_types::SCOPE:
                    {
                        auto        Scopes = Data.get<enumeration::table_entry_details::data_types::SCOPE>();   
                        const int   Count  = i + Scopes.m_nSkip + 1;
                        if (Path.isEmpty() )    getPropertyEntries(pProperty, std::forward<T&&>(Callback), xstring::Make("%s/",                       Prop.m_pName ), i, BaseOffset, Count, std::forward<T_FILTER_CALLBACK&&>(FilterCallback) );
                        else                    getPropertyEntries(pProperty, std::forward<T&&>(Callback), xstring::Make("%s%s/", (const char*)Path,  Prop.m_pName ), i, BaseOffset, Count, std::forward<T_FILTER_CALLBACK&&>(FilterCallback) );
                    }
                    break;
                    case enumeration::table_entry_details::data_types::ATOMIC_ARRAY:
                    {
                        auto& AtomicArray = Data.get<enumeration::table_entry_details::data_types::ATOMIC_ARRAY>();     
                        if( FilterCallback( ((info::base*)AtomicArray.m_Details.getRaw())->m_Flags ) )
                        {
                            // set the default count base on max entries
                            const int Count = [&]()
                            {
                                int Count = AtomicArray.m_Max;

                                // override the count if user wants to set its own
                                if (AtomicArray.m_pQueryCount)
                                {
                                    AtomicArray.m_pQueryCount((char&)*(pProperty + m_BaseOffset), Count, isSend);
                                    x_assert(Count <= AtomicArray.m_Max);
                                }

                                return Count;
                            }();

                            // Add the count into the list
                            {
                                auto&       Entry = Callback();
                                Entry.m_Name.setup("%s%s[]", (const char*)Path, Prop.m_pName, Count);
                                Entry.m_Data.template Construct< prop_type::INT >() = Count;
                            }

                            // no we can go thought all the elements
                            for( int iArray = 0; iArray < Count; iArray++ )
                            {
                                auto&       Entry = Callback();
                                const auto  offset = BaseOffset + AtomicArray.m_Offset + AtomicArray.m_EntrySize * iArray;

                                // fill the entry 
                                Entry.m_Name.setup("%s%s[%d]", (const char*)Path, Prop.m_pName, iArray);

                                // Set the valur of the property
                                AtomicArray.m_pPropertyQuery(
                                    *(pProperty + offset)
                                    , (int&)*(int*)Entry.m_Data.Construct(AtomicArray.m_PropType)
                                    , (int&)*(int*)&AtomicArray.m_Details
                                    , isSend);

                                // Set the details
                                Entry.m_pDetails = (info::base*)&AtomicArray.m_Details;
                            }
                        }
                    }
                    break;
                    case enumeration::table_entry_details::data_types::CLASS_ARRAY:
                    {
                        auto& ClassArray = Data.get<enumeration::table_entry_details::data_types::CLASS_ARRAY>();   

                        // set the default count base on max entries
                        const int Count = [&]()
                        {
                            int Count = ClassArray.m_Max;

                            // override the count if user wants to set its own
                            if (ClassArray.m_pQueryCount)
                            {
                                ClassArray.m_pQueryCount((char&)*(pProperty + m_BaseOffset), Count, (int&)ClassArray.m_Max, isSend);
                                x_assert(Count <= ClassArray.m_Max);
                            }

                            return Count;
                        }();

                        // Add the count into the list
                        {
                            auto&       Entry   = Callback();
                            Entry.m_Name.setup( "%s%s[]", (const char*)Path, Prop.m_pName, Count ) ;    
                            Entry.m_Data.template Construct<prop_type::INT>() = Count;
                        }

                        // now we can go thought all the elements
                        const int IndexOffsetBackup = i;
                        const int ArrayCount        = i + ClassArray.m_nSkip + 1;
                        for( int iArray = 0; iArray < Count; iArray++ )
                        {
                            const auto  offset  = BaseOffset + ClassArray.m_Offset + ClassArray.m_EntrySize * iArray;
                            i = IndexOffsetBackup;
                            if( Path.isEmpty() ) getPropertyEntries( pProperty, std::forward<T>(Callback), xstring::Make( "%s[%d]/",                      Prop.m_pName, iArray ), i, offset, ArrayCount, std::forward<T_FILTER_CALLBACK&&>(FilterCallback) );
                            else                 getPropertyEntries( pProperty, std::forward<T>(Callback), xstring::Make( "%s%s[%d]/", (const char*)Path, Prop.m_pName, iArray ), i, offset, ArrayCount, std::forward<T_FILTER_CALLBACK&&>(FilterCallback) );
                        }
                    }
                    break;
                    case enumeration::table_entry_details::data_types::DYNAMIC_SCOPE:
                    {
                        const auto&     DynamicScopes   = Data.get<enumeration::table_entry_details::data_types::DYNAMIC_SCOPE>();  
                        auto*           pPropperties    = DynamicScopes.m_pDyanmicQuery( (base&)(* (pProperty + m_BaseOffset) ) );
                        
                        if( pPropperties )
                        {
                            int     IndexOffset = 0;
                            auto&   Table       = const_cast<table&>(pPropperties->onPropertyTable());
                            Table.getPropertyEntries( (char*)pPropperties, std::forward<T>(Callback), Path, IndexOffset, 0, Table.m_Count, std::forward<T_FILTER_CALLBACK&&>(FilterCallback) );
                        }
                    }
                    break;
                    case enumeration::table_entry_details::data_types::CHILD_TABLE:
                    {
                        auto&   ChildTable  = Data.get<enumeration::table_entry_details::data_types::CHILD_TABLE>();    
                        int     IndexOffset = 0;
                        auto&   Table       = const_cast<table&>(*ChildTable.m_pTable);
                        Table.getPropertyEntries( pProperty, std::forward<T>(Callback), Path, IndexOffset, BaseOffset, Table.m_Count, std::forward<T_FILTER_CALLBACK&&>(FilterCallback) );
                    }
                    break;
                    case enumeration::table_entry_details::data_types::ATOMIC_DYNAMIC_LIST:
                    {
                        const auto& AtomicDynamicList = Data.get<enumeration::table_entry_details::data_types::ATOMIC_DYNAMIC_LIST>();   

                        // Filter base on the flags
                        if( false == FilterCallback(  ((info::base*)AtomicDynamicList.m_Details.getRaw())->m_Flags ) ) break; 

                        // Read the count if we need to
                        if( AtomicDynamicList.m_pQueryCount )
                        {
                            int Count = -1;

                            AtomicDynamicList.m_pQueryCount( (char&)*(pProperty+m_BaseOffset), Count, isSend );

                            // Add the count into the list
                            {
                                auto&       Entry   = Callback();
                                Entry.m_Name.setup( "%s%s[]", Path?(const char*)Path:"", Prop.m_pName, Count ) ;    
                                Entry.m_Data.template Construct< prop_type::INT >() = Count;
                            }
                        }

                        // Check every entry
                        void* pNext = nullptr;
                        for( u64 id = AtomicDynamicList.m_pQuertIterator( (char&)*(pProperty+m_BaseOffset), pNext ); id != ~0ul; id = AtomicDynamicList.m_pQuertIterator( (char&)*(pProperty+m_BaseOffset), pNext ) )
                        {
                            auto&       Entry = Callback();

                            // fill the entry 
                            if( Path.isEmpty() ) 
                            {
                                if( id >= 0xfffff ) Entry.m_Name.setup( "%s(%X:%X)/", Prop.m_pName, (u32)(id>>32), (u32)id ); 
                                else                  Entry.m_Name.setup( "%s[%d]/", Prop.m_pName, id );
                            }
                            else
                            {
                                if( id >= 0xfffff ) Entry.m_Name.setup( "%s%s(%X:%X)/",(const char*)Path, Prop.m_pName, (u32)(id>>32), (u32)id ); 
                                else                  Entry.m_Name.setup( "%s%s[%d]/",(const char*)Path, Prop.m_pName, id );
                            }

                            // Set the valur of the property
                            AtomicDynamicList.m_pAtomicDynamicListQuery
                            ( 
                                (char&)*(pProperty+m_BaseOffset)
                                , id
                                , (int&)*(int*)Entry.m_Data.Construct(AtomicDynamicList.m_PropType)
                                , (int&)*(int*)&AtomicDynamicList.m_Details
                                , isSend 
                            );

                            // Set the details
                            Entry.m_pDetails = (info::base*)&AtomicDynamicList.m_Details;
                        }
                    }
                    break;
                    case enumeration::table_entry_details::data_types::DYNAMIC_LIST:
                    {
                        const auto& DynamicList = Data.get<enumeration::table_entry_details::data_types::DYNAMIC_LIST>();   

                        // Filter base on the flags
                        if( false == FilterCallback( ((info::base*)DynamicList.m_Details.getRaw())->m_Flags ) ) break; 

                        // Read the count if we need to
                        if( DynamicList.m_pQueryCount )
                        {
                            int Count = -1;

                            DynamicList.m_pQueryCount( (char&)*(pProperty+m_BaseOffset), Count, isSend );

                            // Add the count into the list
                            {
                                auto&       Entry   = Callback();
                                Entry.m_Name.setup( "%s%s[]", Path?(const char*)Path:"", Prop.m_pName, Count ) ;    
                                Entry.m_Data.template Construct< prop_type::INT >() = Count;
                            }
                        }

                        // Check every entry
                        void* pNext = nullptr;
                        for( u64 id = DynamicList.m_pQuertIterator( (char&)*(pProperty+m_BaseOffset), pNext ); id != ~0ul; id = DynamicList.m_pQuertIterator( (char&)*(pProperty+m_BaseOffset), pNext ) )
                        {
                            // Ask the user for data
                            auto pPropBase = DynamicList.m_pDynamicListQuery( (char&)*(pProperty+m_BaseOffset), id, isSend );
                            if( pPropBase )
                            {
                                // Recurse and let the user fill the data
                                int     IndexOffset = 0;
                                auto&   Table       = const_cast<table&>(pPropBase->onPropertyTable());

                                if( Path.isEmpty() ) 
                                    Table.getPropertyEntries
                                    ( 
                                        (char*)pPropBase 
                                        , std::forward<T>(Callback) 
                                        , id >= 0xfffff ? xstring::Make( "%s(%X:%X)/", Prop.m_pName, (u32)(id>>32), (u32)id ) : xstring::Make( "%s[%d]/", Prop.m_pName, id )
                                        , IndexOffset
                                        , 0
                                        , Table.m_Count
                                        , std::forward<T_FILTER_CALLBACK&&>(FilterCallback) 
                                    );
                                else                 
                                    Table.getPropertyEntries
                                    ( 
                                        (char*)pPropBase 
                                        , std::forward<T>(Callback) 
                                        , id >= 0xfffff ? xstring::Make( "%s%s(%X:%X)/",(const char*)Path, Prop.m_pName, (u32)(id>>32), (u32)id ) : xstring::Make( "%s%s[%d]/",(const char*)Path, Prop.m_pName, id )
                                        , IndexOffset 
                                        , 0 
                                        , Table.m_Count 
                                        , std::forward<T_FILTER_CALLBACK&&>(FilterCallback) 
                                    );
                            }
                        }
                    }
                    break;
                    default:
                    {
                        x_assert(false);
                    }
                    break;
                }
            }
        }
        
    };  //   ******** end of struct table ********

    //---------------------------------------------------------------------------------------
    // base
    //---------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------
    // Parameters should be: [&]() -> xproperty::entry& {}
    template< typename T >
    typename std::enable_if< std::is_same< typename xfunction_traits<T>::t_return_type, xproperty_v2::entry& >::value && xfunction_traits<T>::t_arg_count == 0, void >::type
    base::EnumProperty( T&& Callback, bool FullPath, enum_mode Mode ) const
    {
        xstring         Path;
        int             IndexOffset     = 0;
        auto&           Table           = const_cast<table&>(onPropertyTable());
        
        return Table.getPropertyEntries
        ( 
            reinterpret_cast<char*>( const_cast<base*>(this) ), 
            std::forward<T>(Callback), 
            Path, 
            IndexOffset, 
            0, 
            Table.m_Count,
            [Mode]( flags Flags ) 
            {
                switch( Mode )
                {
                case enum_mode::SAVE:
                    if( (Flags & flags::MASK_FORCE_SAVE) == flags::MASK_ZERO )
                    {
                        if( (Flags & flags::MASK_READ_ONLY).m_Value )
                            return false;
                    }
                break;
                case enum_mode::REALTIME:
                    if( Flags & flags::MASK_NOT_VISIBLE )
                        return false;
                break;
                }

                return true;
            }
        );
    }

    //---------------------------------------------------------------------------------------
    template< typename T_TYPE > inline
    typename std::enable_if
    < 
        std::is_same< typename enumeration::valid_usertype<T_TYPE>::type, T_TYPE >::value
        , bool 
    >::type
    base::SetProperty( const char* pName, T_TYPE Var )
    {
        return onPropertyTable().QueryFromTable( this, pName, Var, false );
    }

    //---------------------------------------------------------------------------------------   
    template< typename T_TYPE > inline
    typename std::enable_if
    < 
        std::is_same< typename enumeration::valid_usertype<T_TYPE>::type, T_TYPE >::value
        , std::pair<bool, T_TYPE> 
    >::type
    base::GetProperty( const char* pName ) const
    {
        std::pair<bool, T_TYPE> Var;
        Var.first = onPropertyTable().QueryFromTable( this, pName, Var.second, true ); 
        return Var;
    }

    //---------------------------------------------------------------------------------------   
    template< typename T > inline
    typename std::enable_if
    < 
            std::is_same< typename xfunction_traits<T>::t_return_type, xproperty_v2::entry* >::value 
        &&  xfunction_traits<T>::t_arg_count == 1 
        &&  std::is_same< typename xfunction_traits<T>::template t_safe_arg<0>::type, int >::value
        ,   void 
    >::type
    base::GetProperties( T&& Callback ) const
    {
        onPropertyTable().QueryProperties( this, true, std::forward<T>(Callback) );
    }

    //---------------------------------------------------------------------------------------   
    template< typename T > inline
    typename std::enable_if
    < 
            std::is_same< typename xfunction_traits<T>::t_return_type, const xproperty_v2::entry* >::value 
        &&  xfunction_traits<T>::t_arg_count == 1 
        &&  std::is_same< typename xfunction_traits<T>::template t_safe_arg<0>::type, int >::value
        ,   void 
    >::type
    base::SetProperties( T&& Callback )
    {
        onPropertyTable().QueryProperties( this, false, std::forward<T>(Callback) );
    }

    //---------------------------------------------------------------------------------------

    inline bool base::Load( xtextfile& File )
    {
        if( File.getRecordName() != X_STR("Properties") )
            return false;

        const int Count = File.getRecordCount();
        xproperty_v2::entry Entry;
        SetProperties( [&]( const int Index ) -> const xproperty_v2::entry*
        {
            if( Index < Count )
            {
                File.ReadLine();
                Entry.SerializeIn(File);
                return &Entry;
            }

            return nullptr;
        });

        File.ReadRecord();
        return true;
    }

    //---------------------------------------------------------------------------------------

    inline void base::Save( xtextfile& File, bool bFullPath ) const
    {
        xvector<xproperty_v2::entry> List;

        if( File.getUserTypeCount() == 0 ) entry::RegisterTypes( File );

        // Okay collect all properties
        EnumProperty( [&]() -> xproperty_v2::entry&
        {
            return List.append();
        }, bFullPath, enum_mode::SAVE );

        File.WriteRecord( "Properties", List.getCount<int>() );
        for( const auto& Entry : List )
        {
            Entry.SerializeOut(File);
        }
    }



} // ********* end of namespace xproperty_v2 **********

