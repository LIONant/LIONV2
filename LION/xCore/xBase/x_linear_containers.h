//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//      Transforms a static allocator such an array into something that
//      looks like a memory owner
//------------------------------------------------------------------------------
template
< 
    template< typename, xuptr, typename > class     T_ALLOCATOR, 
    xuptr                                           T_COUNT 
>
struct x_static_to_dynamic_interface
{ 
    template< typename T_NODE, typename T_COUNTER > 
    using allocator = T_ALLOCATOR< T_NODE, T_COUNT, T_COUNTER >;
};

//------------------------------------------------------------------------------
// Description:
//          The vector base class
//------------------------------------------------------------------------------
template< typename T_ALLOCATOR >
class xvector_base
{
    x_object_type( xvector_base, is_linear, is_not_movable, is_not_copyable );

public:
    
    using                       t_counter       =   typename T_ALLOCATOR::t_counter;
    using                       t_entry         =   typename T_ALLOCATOR::t_entry;
    using                       t_view          =   typename T_ALLOCATOR::t_view;
    enum : t_counter {          DEFAULT_GROWTH  =   50 };

    #define x_linear_constructs_buffer_hardness
    #include "Implementation/x_linear_constructs_hardness.h"

public:

    constexpr                                               xvector_base                ( void )                                            noexcept = default;
    inline                                                 ~xvector_base                ( void )                                            noexcept { Kill(); }
    inline      void                                        CopyToFromDynamically       ( const t_view View, t_counter DestinationOffset )  noexcept;
    inline      void                                        CopyClean                   ( const t_view View )                               noexcept    { DeleteAllEntries(); CopyToFrom(View); }
    inline      void                                        DeleteWithCollapse          ( const t_counter Index, const t_counter Count=1 )  noexcept;
    inline      void                                        DeleteWithSwap              ( const t_counter Index, const t_counter Count=1 )  noexcept;
    inline      void                                        DeleteAllEntries            ( void )                                            noexcept;
    inline      void                                        Kill                        ( void )                                            noexcept;
    
    inline      t_entry&                                    Insert                      ( const t_counter Index )                           noexcept    { InsertList(Index,1); return m_pMem[Index]; }
    inline      void                                        InsertList                  ( const t_counter Index, const t_counter Count )    noexcept;        
    inline      void                                        InsertList                  ( const t_counter Index, const t_view View )        noexcept;

    template<typename ...T_ARG>
    inline      t_entry&                                    append                      ( T_ARG&&...Args )                                  noexcept    { auto Index = m_Count; appendList(1, std::forward<T_ARG>(Args)... ); return m_pMem[Index]; }
    
    template<typename ...T_ARG>
    inline      t_entry&                                    append                      ( t_counter& Index, T_ARG&&...Args )                noexcept    {      Index = m_Count; appendList(1, std::forward<T_ARG>(Args)... ); return m_pMem[Index]; }
    
    inline      t_entry&                                    getLast                     ( void )                                            noexcept    { return m_pMem[m_Count-1]; }

    template<typename ...T_ARG>
    inline      void                                        appendList                  ( const t_counter Count, T_ARG&&...Args )           noexcept;

    inline      void                                        appendList                  ( const t_view View )                               noexcept;
    
    template< typename T = t_counter >
    constexpr   T                                           getCount                    ( void ) const                                      noexcept    { return x_static_cast<T>(m_Count); }
    inline      void                                        GrowBy                      ( const t_counter NewItems )                        noexcept;
    constexpr   t_counter                                   getCapacity                 ( void ) const                                      noexcept    { return m_pMem.getCount(); }

/*    
    inline      void                                        ShrinkToFit                 ( void )                            noexcept;
    
    template <class KEY>
    inline      bool                                        BinarySearch                ( const KEY& Key, t_index& t_index ) const noexcept;
    
    template <class KEY>
    inline      bool                                        BinarySearch                ( const KEY& Key ) const            noexcept;
*/    
protected:
    
    T_ALLOCATOR         m_pMem              {};
    t_counter           m_Count             {0};
};

//------------------------------------------------------------------------------
// Description:
//          Standard vector class
//------------------------------------------------------------------------------

template< typename T_ENTRY > 
using xvector = xvector_base<xafptr<T_ENTRY>>;


//------------------------------------------------------------------------------
// Description:
//     xpage_array is similar to a xharray except it guarantees that the memory will always be valid.
//     in other words it wont realloc memory stead it allocates pages and the entries are manage there.
//     this avoids reallocations and makes the memory safer. 
//------------------------------------------------------------------------------
template
< 
    template< typename, typename > class    T_ALLOCATOR,
    typename                                T_ENTRY, 
    xuptr                                   T_ENTRIES_PER_PAGE, 
    typename                                T_COUNTER 
>
class xphvector_base
{
    x_object_type( xphvector_base, is_linear, is_not_movable, is_not_copyable );

protected:

    enum constants : u32
    {
        ENTRIES_PER_PAGE_POW2   = x_Log2IntRoundUp( T_ENTRIES_PER_PAGE ),
        ENTRIES_PER_PAGE        = (1<<ENTRIES_PER_PAGE_POW2),
        HANDLE_PAGE_SHIFT       = ENTRIES_PER_PAGE_POW2,
        HANDLE_PAGE_MASK        = ((1<<HANDLE_PAGE_SHIFT)-1)<<HANDLE_PAGE_SHIFT,
        HANDLE_ENTRY_MASK       = ~HANDLE_PAGE_MASK,
    };

    struct page
    {
        u16                                     m_iFreeHead         {};
        u16                                     m_nAlloced          {};
        u16                                     m_iNextFreePage     {};
        xarray_raw<T_ENTRY,ENTRIES_PER_PAGE>    m_Entry             {};
    };

public:

    using                       t_page_allocator    =   T_ALLOCATOR<page,T_COUNTER>;
    using                       t_counter           =   typename t_page_allocator::t_counter;
    using                       t_entry             =   T_ENTRY;
    using                       t_view              =   typename t_page_allocator::t_view;
    using                       t_handle            =   xhandle<t_entry>;

public:

    constexpr                               xphvector_base      ( void )                                noexcept = default;
    x_inline                               ~xphvector_base      ( void )                                noexcept;
    template< typename T = t_counter > 
    x_forceconst        T                   getCount            ( void )                        const   noexcept { return x_static_cast<T>(m_Count); }

    template< typename T > 
    x_forceinline       t_entry&            operator[]          ( const T index )                       noexcept; 
    template< typename T > 
    x_forceconst        const t_entry&      operator[]          ( const T index )               const   noexcept;
    x_inline            t_entry&            operator()          ( const t_handle hHandle  )             noexcept;
    x_inline            const t_entry&      operator()          ( const t_handle hHandle  )     const   noexcept;

    x_forceconst        t_counter           getIndexByHandle    ( const t_handle   Handle )     const   noexcept;
    template< typename T >
    x_forceconst        t_handle            getHandleByIndex    ( const T          Index )      const   noexcept;

    x_inline            void                SanityCheck         ( void )                        const   noexcept;

    x_inline            t_entry&            pop                 ( void )                                noexcept;
    template<typename ...T_ARG> 
    x_inline            t_entry&            pop                 ( t_handle& Handle,T_ARG&&...Args )     noexcept;
    template< typename T >
    x_inline            void                push                ( T index )                             noexcept;
    x_inline            void                push                ( t_handle Handle )                     noexcept; 

 //   xiter< xparray<T>, T >              begin               ( void )     { return xiter< xparray<T>, T >(0,*this); }
 //   xiter< xparray<T>, T >              end                 ( void )     { return xiter< xparray<T>, T >(m_Count,*this); }
    
 //   xiter< const xparray<T>, const T >  begin               ( void ) const    { return xiter< const xharray<T>, const T >(0,*this); }
 //   xiter< const xparray<T>, const T >  end                 ( void ) const    { return xiter< const xharray<T>, const T >(m_Count,*this); }

protected:

    struct iterator_entry
    {
        t_handle                                m_hEntry    {};             // Handle to the actual entry
        u32                                     m_iHandle   {};             // Given an [hEntry] we give back the iterator index
    };

protected:

    void    AllocPage( void ) noexcept;

protected:

    xndptr<xndptr_s<page>>          m_lPages            {};                 // An array of pointer to the actual pages
    xafptr<iterator_entry>          m_Iterator          {};                 // Lager linear buffer to map the entries
    t_counter                       m_Count             { 0 };              // Total Allocated Entries
    u16                             m_iFreePage         { 0xffff };         // Link List of empty pages
};

//------------------------------------------------------------------------------
// Description:
//            pending
//------------------------------------------------------------------------------
template< typename T_ENTRY, xuptr T_ENTRIES_PER_PAGE, typename T_COUNTER = xuptr >
using xphvector = xphvector_base< xafptr, T_ENTRY, T_ENTRIES_PER_PAGE, T_COUNTER >;


//------------------------------------------------------------------------------
// Description:
//      A General link list
//    struct t_entry : public T_ENTRY
//    {
//        t_entry*    m_pNext {};           // This will create a linear link list
//        t_entry*    m_pPrev {};           // This will create a circular link list
//    };
//
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER = xuptr > 
class x_linear_link_list 
{
    x_object_type( x_linear_link_list, is_linear, is_not_copyable )

public:

    using   t_entry     = T_ENTRY;
    using   t_counter   = T_COUNTER;

    struct iterator
    {
        x_forceconst                            iterator        ( t_entry* pNode )              noexcept : m_pNode{ pNode }{}
        x_forceconst        const auto&         operator *      ( void )                const   noexcept { return *m_pNode; }
        x_forceinline       auto&               operator *      ( void )                        noexcept { return *m_pNode; }
        x_forceinline       const iterator&     operator ++     ( void )                        noexcept { m_pNode = m_pNode->m_pNext; return *this; }
        x_forceinline       const iterator      operator ++     ( int )                         noexcept { auto t = m_pNode; m_pNode = m_pNode->m_pNext; return { t }; }
        x_forceconst        const t_entry*      operator ->     ( void )                const   noexcept { return m_pNode; }
        x_forceinline       t_entry*            operator ->     ( void )                        noexcept { return m_pNode; }
        x_forceconst        bool                operator !=     ( const iterator& End ) const   noexcept { return m_pNode != End.m_pNode; }
        
        t_entry*    m_pNode;
    };

public:
    
    constexpr                           x_linear_link_list  ( void )                                        noexcept = default;
   
    template< typename T = t_counter > 
    x_forceconst    T                   getCount            ( void )                                const   noexcept { return x_static_cast<T>(m_Count);       }
    x_inline        void                AppendFirst         ( t_entry& Entry )                              noexcept;
    x_inline        void                Append              ( t_entry& Entry )                              noexcept;
    x_inline        void                Remove              ( t_entry& Entry )                              noexcept;
    x_inline        void                InsertAfter         ( t_entry& AfterEntry,  t_entry& Entry )        noexcept;
    x_inline        void                InsertBefore        ( t_entry& BeforeEntry, t_entry& Entry )        noexcept;
    x_forceinline   iterator            begin               ( void )                                        noexcept { return { m_pHead  };  }
    x_forceinline   iterator            end                 ( void )                                        noexcept { return { nullptr  };  }
    x_forceconst    const iterator      begin               ( void )                                const   noexcept { return { m_pHead  };  }
    x_forceconst    const iterator      end                 ( void )                                const   noexcept { return { nullptr  };  }
    x_forceconst    const t_entry*      getHead             ( void )                                const   noexcept { return m_pHead;       }
    x_forceinline   t_entry*            getHead             ( void )                                        noexcept { return m_pHead;       }

protected:

    t_entry*    m_pHead { nullptr   };
    t_counter   m_Count { 0         };
};

//------------------------------------------------------------------------------
// Description:
//      A link list Pool
//------------------------------------------------------------------------------

template< typename T_ENTRY, typename T_COUNTER = xuptr > 
class x_linear_link_list_pool 
{
    x_object_type( x_linear_link_list_pool, is_linear, is_not_copyable )
    protected: struct t_list_entry;  

public:

    using   t_entry     = T_ENTRY;
    using   t_counter   = T_COUNTER;

    struct iterator 
    {
        x_forceconst        const auto&         operator *      ( void )                const   noexcept { return m_pNode->m_Entry; }
        x_forceinline       auto&               operator *      ( void )                        noexcept { return m_pNode->m_Entry; }
        x_forceinline       const iterator&     operator ++     ( void )                        noexcept { m_pNode = m_pNode->m_pNext; return *this; }
        x_forceinline       const iterator      operator ++     ( int )                         noexcept { auto t = m_pNode; m_pNode = m_pNode->m_pNext; return { t }; }
        x_forceconst        const t_entry*      operator ->     ( void )                const   noexcept { return &m_pNode->m_Entry; }
        x_forceinline       t_entry*            operator ->     ( void )                        noexcept { return &m_pNode->m_Entry; }
        x_forceconst        bool                operator !=     ( const iterator& )     const   noexcept { return !!m_pNode; }
        
        t_list_entry*    m_pNode;
    };

public:
    
    constexpr x_linear_link_list_pool( void ) noexcept = default;

    x_forceinline t_entry& pop( void ) noexcept
    {
        auto* pEntry = x_new( t_list_entry );
        m_LinkList.Append( *pEntry );
        return pEntry->m_Entry;
    }

    x_forceinline void push( t_entry& Entry ) noexcept
    {
        auto& TheEntry = reinterpret_cast<t_list_entry&>( reinterpret_cast<xbyte*>(&Entry)[ -offsetof( t_list_entry, m_Entry) ] );
        m_LinkList.Remove( TheEntry );
        x_delete( &TheEntry );
    }

    x_forceinline ~x_linear_link_list_pool( void ) noexcept
    {
        for( auto* pNode = m_LinkList.getHead(); pNode; )
        {
            auto* const pNext = pNode->m_pNext;
            x_delete( pNode );
            pNode = pNext;
        }
    }

    x_forceinline   iterator            begin               ( void )                                        noexcept { return { m_LinkList.getHead()  };  }
    x_forceinline   iterator            end                 ( void )                                        noexcept { return { nullptr  };  }
    x_forceconst    const iterator      begin               ( void )                                const   noexcept { return { m_LinkList.getHead() };  }
    x_forceconst    const iterator      end                 ( void )                                const   noexcept { return { nullptr  };  }
    template< typename T = t_counter > 
    x_forceinline   T                   getCount            ( void )                                const   noexcept { return m_LinkList.template getCount<T>(); }

protected:

    struct t_list_entry  
    {
        t_list_entry*   m_pNext     {};
        t_list_entry*   m_pPrev     {};
        t_entry         m_Entry     {};
    };

protected:

    x_linear_link_list<t_list_entry,t_counter> m_LinkList {};
};


//------------------------------------------------------------------------------
// Description:
//      A linear General pool allocator
//------------------------------------------------------------------------------
template< typename T_ENTRY, template<typename,typename> class T_ALLOCATOR, typename T_COUNTER = xuptr >
class x_fixed_pool_general_base 
{
    x_object_type( x_fixed_pool_general_base, is_linear, is_not_copyable, is_not_movable );

protected: struct t_real_node;
public:
    using  t_entry      = T_ENTRY;
    using  t_counter    = typename T_ALLOCATOR< t_real_node, T_COUNTER >::t_counter; 
    
public:

    x_forceinline       x_fixed_pool_general_base( bool bClear = true ) noexcept { if(bClear) Clear(); }
    
    void                Clear           ( void )
    {
        const t_counter Count = m_Allocator.getCount() - 1;
        
        for( t_counter i = 0; i<Count; i++ )
        {
            m_Allocator[i].m_pNext = &m_Allocator[i + 1];
        }
        
        m_Allocator[Count].m_pNext = nullptr;
        m_pHead                    = &m_Allocator[0];
    }
    
    xowner<t_entry*>     pop           ( void )
    {
        if( m_pHead == nullptr ) return nullptr;
        
        t_real_node* pNode  = m_pHead;
        m_pHead             = m_pHead->m_pNext;
        return &pNode->m_Entry;
    }
    
    void                push            ( xowner<t_entry&> userEntry )
    {
        t_real_node& Node            = *reinterpret_cast<t_real_node*>( &reinterpret_cast<xbyte*>(&userEntry)[-ENTRY_OFFSET] );
        Node.m_pNext    = m_pHead;
        m_pHead         = &Node;
    }
    
    t_entry&            get             ( xuptr Index ) { return m_Allocator[Index].m_Entry; }
    
protected:
    
    struct t_real_node
    {
        xowner<t_real_node*>    m_pNext;
        t_entry                 m_Entry;
    };
    
    enum : int { ENTRY_OFFSET = static_cast<int>(offsetof(t_real_node,m_Entry)) };
    
protected:
    
    xowner<t_real_node*>                    m_pHead         { nullptr };
    T_ALLOCATOR<t_real_node,t_counter>      m_Allocator     {};
};

//------------------------------------------------------------------------------
// Description:
//      A specialization of the memory pool with an array as the memory owner
//      - Entry memory is part of the class, so no heap allocations happen
//------------------------------------------------------------------------------
template< class T_ENTRY, xuptr T_COUNT, typename T_COUNTER = xuptr >
using x_fixed_pool_static = x_fixed_pool_general_base
< 
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray, T_COUNT >::template allocator, 
    T_COUNTER 
>;

//------------------------------------------------------------------------------
// Description:
//      A specialization of the memory pool with an array_raw as the memory owner
//      - Entry memory is part of the class, so no heap allocations happen
//      - The object/entries construction are left to the user
//------------------------------------------------------------------------------
template< class T_ENTRY, int T_COUNT, typename T_COUNTER = xuptr >
using x_fixed_pool_static_raw = x_fixed_pool_general_base
<
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray_raw, T_COUNT >::template allocator,
    T_COUNTER 
>;

//------------------------------------------------------------------------------
// Description:
//      A specialization of the memory pool with a memory new/delete ptr owner
//      - The entry memory will be allocated in the heap
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER = xuptr >
class x_fixed_pool_dynamic : public x_fixed_pool_general_base< T_ENTRY, xndptr, T_COUNTER >
{
public:

    using t_self    = x_fixed_pool_dynamic;
    using t_parent  = x_fixed_pool_general_base< T_ENTRY, xndptr, T_COUNTER >;
    using t_entry   = typename t_parent::t_entry;
    using t_counter = typename t_parent::t_counter;
    
public:

    x_forceinline           x_fixed_pool_dynamic        ( void )      noexcept : t_parent{ false } {}
    inline      void        Init                        ( s32 Count ) noexcept { t_parent::m_Allocator.New( Count ); t_parent::Clear(); }
    inline      void        Kill                        ( void )      noexcept { t_parent::m_Allocator.Delete();                        }
    inline                 ~x_fixed_pool_dynamic        ( void )      noexcept { Kill();                                                }
};


