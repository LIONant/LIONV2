//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#define X_PROP_MAINSCOPE( QUERY, SCOPE_NAME, SCOPES, PROPS )      X_VARG_EXPAND( auto& __PropQuery = QUERY; if( __PropQuery.isScope( X_STR_CRCINFO(SCOPE_NAME) ) ) do {                                                    if( Query.isSearchingForScopes() ) { if(0){} SCOPES; } else { if(0){} PROPS; } } while( __PropQuery.NextPropertyOrChangeScope() );  else return false; return __PropQuery.isDone();      )
#define X_PROP_SCOPE_CHILD( CHILD_SCOPE, ... )                    X_VARG_EXPAND( else                       if( CHILD_SCOPE )                                    {  __VA_ARGS__; }                                                                                                                                                                                                               )
#define X_PROP_IS_SCOPE( PROP_NAME, SCOPES, PROPS )               X_VARG_EXPAND( else                       if( __PropQuery.isScope( X_STR_CRCINFO(PROP_NAME)  ) ) do {                                                    if( Query.isSearchingForScopes() ) { if(0){} SCOPES; } else { if(0){} PROPS; } } while( __PropQuery.NextPropertyOrChangeScope() );                                    )
#define X_PROP_IS_ARRAY( PROP_NAME, INDEX_VAR, SCOPES, PROPS )    X_VARG_EXPAND( else                       if( __PropQuery.isScope( X_STR_CRCINFO(PROP_NAME)  ) ) do { const int INDEX_VAR = __PropQuery.getArrayIndex(); if( Query.isSearchingForScopes() ) { if(0){} SCOPES; } else { if(0){} PROPS; } } while( __PropQuery.NextPropertyOrChangeScope() );                                    )
#define X_PROP_IS_PROP( PROP_NAME, ... )                          X_VARG_EXPAND( else                       if( __PropQuery.isVar  ( X_STR_CRCINFO(PROP_NAME)  ) )    {  __VA_ARGS__; }                                                                                                                                                                                                          )

//--------------------------------------------------------------------------
namespace x_property_details { template< typename, char > class xprop_general_atomic; };

#define xproperty_friends                                               \
    friend class xprop_s32;                                             \
    friend class xprop_string;                                          \
    friend class xprop_f32;                                             \
    friend class xprop_guid;                                            \
    friend class xprop_vector3;                                         \
    friend class xproperty_query;                                       \
    friend class xproperty_enum;                                        \
    friend class xproperty_data;


//--------------------------------------------------------------------------

class  xproperty_query;
class  xproperty_enum;
class  xproperty_query;
class  xproperty_data;
class  xproperty;
 
 //--------------------------------------------------------------------------

class xproperty_data
{
    x_object_type( xproperty_data, is_linear );

public:

    // Note that this list bust be alphabetically sorted 
    enum class type : u8
    {
        INVALID,
        ANGLES3,
        BOOL,
        BUTTON,
        DELEGATE,
        ENUM,
        EVENT,
        FLOAT,
        GUID,
        INT,
        RGBA,
        STRING,
        VECTOR2,
        VECTOR3,
        ENUM_COUNT
    };

    X_DEFBITS( flags,
               u8,
               0x0000,
               X_DEFBITS_ARG ( bool,      READ_ONLY,           1 ),    
               X_DEFBITS_ARG ( bool,      FORCE_SAVE,          1 ),                                         
               X_DEFBITS_ARG ( bool,      DONT_SAVE,           1 ),                                         
               X_DEFBITS_ARG ( bool,      REFRESH_ONCHANGE,    1 ),                                         
               X_DEFBITS_ARG ( bool,      NOT_VISIBLE,         1 )                                         
    );

    struct data_s32
    {
        s32         m_Value {};
        s32         m_Min   {};                     // This is for get only
        s32         m_Max   {};                     // This is for get only
    };

    struct data_guid
    {       
        u64                     m_Value;
        xstring::const_str      m_Category;         // This is for get only
        u64                     m_Type;             // This is for get only
    };

    struct data_v3
    {
        xvector3    m_Value;
        xvector3    m_Min;                          // This is for get only
        xvector3    m_Max;                          // This is for get only
    };

    struct data_v2
    {
        xvector2    m_Value;
        xvector2    m_Min;                          // This is for get only
        xvector2    m_Max;                          // This is for get only
    };

    struct data_f32
    {
        f32         m_Value;
        f32         m_Min;                           // This is for get only
        f32         m_Max;                           // This is for get only
    };

    struct data_r3
    {
        xradian3    m_Value;
        xradian3    m_Min;                           // This is for get only
        xradian3    m_Max;                           // This is for get only
    };

    struct data_enum
    {
        struct entry
        {
            enum special_values
            {
                 END_DISPLAY = -1,
                 END_ENUM    = -2
            };

            int             m_Value;
            const xstring   m_Name;
        };

        const entry*    m_pEnumList {};              // This is for get only
        xstring         m_String    {};
    };

    struct event
    {
        u64                 m_ObjectGuid;                   // This is for get only
        u64                 m_EventInstanceGuid;            // This is for get only
        xstring::const_str  m_EventTypeName;                // This is for get only
    };

    struct delegate
    {
        u64                 m_ObjectGuid;
        u64                 m_EventInstanceGuid;
        xstring::const_str  m_EventTypeName;                // This is for get only
    };

public:

    xproperty_data( void ) {}
    xproperty_data( const xproperty_data& A ) { (*this) = A; }
    xproperty_data& operator = ( const xproperty_data& A ); 

    ~xproperty_data( void )
    {
        // exit safely
        setup( type::INVALID );
    }

    x_inline                bool                        operator ==     ( const xproperty_data& B ) const;
    x_forceconst            type                        getType         ( void ) const          { return m_Info.m_Type;   }
    x_forceconst            flags                       getFlags        ( void ) const          { return m_Info.m_Flags;  }
    template< typename T >
    x_forceconst            T                           getGizmosFlags  ( void ) const          { return static_cast<T>(m_Info.m_Gizmos); }
    x_forceinline           void                        CopyInfo        ( const xproperty_data& B );
    x_forceinline           bool&                       getBool         ( void )                { return x_assert( getType() == type::BOOL     ), m_Data.m_Bool;       }
    x_forceinline           xstring&                    getButton       ( void )                { return x_assert( getType() == type::BUTTON   ), m_Data.m_Button;     }
    x_forceinline           xcolor&                     getColor        ( void )                { return x_assert( getType() == type::RGBA     ), m_Data.m_Color;      }
    x_forceinline           data_enum&                  getEnum         ( void )                { return x_assert( getType() == type::ENUM     ), m_Data.m_Enum;       }
    x_forceinline           data_s32&                   getS32          ( void )                { return x_assert( getType() == type::INT      ), m_Data.m_S32;        }
    x_forceinline           data_f32&                   getF32          ( void )                { return x_assert( getType() == type::FLOAT    ), m_Data.m_F32;        }
    x_forceinline           data_guid&                  getGUID         ( void )                { return x_assert( getType() == type::GUID     ), m_Data.m_Guid;       }
    x_forceinline           data_v3&                    getV3           ( void )                { return x_assert( getType() == type::VECTOR3  ), m_Data.m_Vector3;    }
    x_forceinline           data_v2&                    getV2           ( void )                { return x_assert( getType() == type::VECTOR2  ), m_Data.m_Vector2;    }
    x_forceinline           data_r3&                    getR3           ( void )                { return x_assert( getType() == type::ANGLES3  ), m_Data.m_Radian3;    }
    x_forceinline           xstring&                    getString       ( void )                { return x_assert( getType() == type::STRING   ), m_Data.m_String;     }
    x_forceinline           event&                      getEvent        ( void )                { return x_assert( getType() == type::EVENT    ), m_Data.m_Event;      }
    x_forceinline           delegate&                   getDelegate     ( void )                { return x_assert( getType() == type::DELEGATE ), m_Data.m_Delegate;   }
    x_forceconst            const bool                  getBool         ( void ) const          { return x_assert( getType() == type::BOOL     ), m_Data.m_Bool;       }
    x_forceconst            const xstring&              getButton       ( void ) const          { return x_assert( getType() == type::BUTTON   ), m_Data.m_Button;     }
    x_forceconst            const xcolor                getColor        ( void ) const          { return x_assert( getType() == type::RGBA     ), m_Data.m_Color;      }
    x_forceconst            const data_enum&            getEnum         ( void ) const          { return x_assert( getType() == type::ENUM     ), m_Data.m_Enum;       }
    x_forceconst            const data_s32&             getS32          ( void ) const          { return x_assert( getType() == type::INT      ), m_Data.m_S32;        }
    x_forceconst            const data_f32&             getF32          ( void ) const          { return x_assert( getType() == type::FLOAT    ), m_Data.m_F32;        }
    x_forceconst            const data_guid&            getGUID         ( void ) const          { return x_assert( getType() == type::GUID     ), m_Data.m_Guid;       }
    x_forceconst            const data_v3&              getV3           ( void ) const          { return x_assert( getType() == type::VECTOR3  ), m_Data.m_Vector3;    }
    x_forceconst            const data_v2&              getV2           ( void ) const          { return x_assert( getType() == type::VECTOR2  ), m_Data.m_Vector2;    }
    x_forceinline           const data_r3&              getR3           ( void ) const          { return x_assert( getType() == type::ANGLES3  ), m_Data.m_Radian3;    }
    x_forceinline           const xstring&              getString       ( void ) const          { return x_assert( getType() == type::STRING   ), m_Data.m_String;     }
    x_forceinline           const event&                getEvent        ( void ) const          { return x_assert( getType() == type::EVENT    ), m_Data.m_Event;      }
    x_forceinline           const delegate&             getDelegate     ( void ) const          { return x_assert( getType() == type::DELEGATE ), m_Data.m_Delegate;   }
    x_inline        static  void                        RegisterTypes   ( xtextfile& DataFile ); 
    x_inline                void                        SerializeOut    ( xtextfile& DataFile ) const;
    x_inline                void                        SerializeIn     ( xtextfile& DataFile );
    x_inline                t_self&                     setup           ( type Type );
    x_inline                t_self&                     setup           ( type Type, xstring& Name, flags Flags, xstring::const_str Help, u32 GizmosFlags );
            
public:

    xstring                 m_Name  {};             // Name of the property
    xstring::const_str      m_Help  {nullptr};

protected:

    union data_raw
    {
        constexpr           data_raw  ( void )          {}
                           ~data_raw  ( void )          {}

        data_s32            m_S32                       {};
        bool                m_Bool;
        xcolor              m_Color;
        data_f32            m_F32;
        float               m_Float;
        data_guid           m_Guid;
        data_v3             m_Vector3;
        data_v2             m_Vector2;
        xstring             m_String;
        xwstring            m_WString;
        data_enum           m_Enum;
        xstring             m_Button;
        data_r3             m_Radian3;
        event               m_Event;
        delegate            m_Delegate;
    };

    struct type_info
    {
        type            m_EnumType;
        const char*     m_pStringType;
        const char*     m_pTextFileType;
    };

    union compact_info
    {
        u32         m_Value;
        struct
        {
            type    m_Type; 
            flags   m_Flags;
            u8      m_Gizmos;
            u8      WASTED_ALIGMENT;
        };
    };
    static_assert( sizeof(compact_info) == 4, "We are trying to keep this structure nice and compact" );

protected:

    const char* getWriteTypeString( void ) const
    {
        return &getTable()[getType()].m_pStringType[x_constStrLength("Value:")];
    }

    const char* getReadTypeString( void ) const
    {
        return getTable()[getType()].m_pStringType;
    }
    
    x_forceinline static const xarray< type_info, static_cast<xuptr>(type::ENUM_COUNT) >& getTable( void )
    {
        static const xarray< type_info, static_cast<xuptr>(type::ENUM_COUNT) > s_Table =
        {{
            { type::INVALID,    "",                 ""                     },
            { type::ANGLES3,    "Value:.ANGLES3",   "fff"                  },
            { type::BOOL,       "Value:.BOOL",      "d"                    },
            { type::BUTTON,     "Value:.BUTTON",    "s"                    },
            { type::DELEGATE,   "Value:.DELEGATE",  "gg"                   },
            { type::ENUM,       "Value:.ENUM",      "e"                    },
            { type::EVENT,      "Value:.EVENT",     "gg"                   },
            { type::FLOAT,      "Value:.FLOAT",     "f"                    },
            { type::GUID,       "Value:.GUID",      "g"                    },
            { type::INT,        "Value:.INT",       "d"                    },
            { type::RGBA,       "Value:.RGBA",      "cccc"                 },
            { type::STRING,     "Value:.STRING",    "s"                    },
            { type::VECTOR2,    "Value:.VECTOR2",   "ff"                   },
            { type::VECTOR3,    "Value:.VECTOR3",   "fff"                  }
        }};
        return s_Table;
    }

protected:

    compact_info        m_Info  {};
    data_raw            m_Data  {};


    // VS2016 is crashing
    /*
    static_assert( s_Table[static_cast<int>(type::FLOAT)].m_EnumType      == type::FLOAT, "" );
    static_assert( s_Table[static_cast<int>(type::INT)].m_EnumType        == type::INT, "" );
    static_assert( s_Table[static_cast<int>(type::STRING)].m_EnumType     == type::STRING, "" );
    static_assert( s_Table[static_cast<int>(type::GUID)].m_EnumType       == type::GUID, "" );
    static_assert( s_Table[static_cast<int>(type::VECTOR3)].m_EnumType    == type::VECTOR3, "" );
    */

};

//--------------------------------------------------------------------------

struct xproperty_data_collection
{
    // Creates a new collection with the result of filtering out all the properties from a FilterList
    void                        appendSubtractFilterByNameAndValue                  ( const xbuffer_view<xproperty_data> A, const xbuffer_view<xproperty_data> B );
    void                        appendUnionFilterByName                             ( const xbuffer_view<xproperty_data> A, const xbuffer_view<xproperty_data> B );
                                operator const xbuffer_view<const xproperty_data>   ( void )                                                                        const   { return { &m_List[0], m_List.getCount() }; }
                                operator xbuffer_view<xproperty_data>               ( void )                                                                                { return { &m_List[0], m_List.getCount() }; }
    
    xvector<xproperty_data> m_List {};
};

//--------------------------------------------------------------------------

class xproperty_enum
{
public:

    using callback = xfunction_view<void( xproperty_enum& Enum, xproperty_data& Data )>;
    using flags    = xproperty_data::flags;

    enum class mode
    {
        LOAD,
        SAVE,
        REALTIME 
    };

    class scope_array;
    class scope 
    {
    public:
        
                                        scope                       ( void ) = delete;
        constexpr                       scope                       ( xproperty_enum& E ) : m_Enum{ E } {}
   
        x_forceinline                   operator xproperty_enum&    ( void ) { return m_Enum; }
        
        template< typename T >
        x_forceinline   void            AddProperty                 ( xstring::const_str PropertyName, xstring::const_str Help, flags Flags = flags::MASK_DEFAULT, typename T::gizmo_flags GizmosFlags = T::gizmo_flags::MASK_DEFAULT );
        
        template< typename T >
        x_inline        void            beginScope                  ( xstring::const_str ScopeName, T&& CallBack );
        template< typename T >
        x_inline        void            beginScopeArray             ( xstring::const_str ScopeName, int Count, T&& CallBack );

    protected:

        x_incppfile     void            AddProperty                 ( xstring::const_str PropertyName, xproperty_data::type Symbol, xstring::const_str Help, flags Flags, u32 GizmosFlags );

    protected:
        
        xproperty_enum& m_Enum;
    };

    class scope_array 
    {
    public:
                                        scope_array                 ( void ) = delete;
        constexpr                       scope_array                 ( xproperty_enum& E ) : m_Enum{ E } {}

        template< typename T >
        x_inline        void            beginScopeArrayEntry        ( int Index, T&& CallBack );

    protected:
        
        xproperty_enum& m_Enum;
    };

public:

                                    xproperty_enum                  ( void ) = delete;
                                    xproperty_enum                  ( callback CallBack, mode Mode = mode::REALTIME ) : m_EnumMode{ Mode }, m_Callback{ CallBack } {}
    x_forceinline   void            setForceFullPaths               ( bool bForceThem )                                                                             { m_bForceFullPaths = bForceThem; }

    template< typename T >
    x_forceinline   void            beginScope                      ( xstring::const_str ScopeName, T&& CallBack )                   { scope{ *this }.beginScope( ScopeName, CallBack ); }

    template< typename T >
    x_forceinline   void            beginScopeArray                 ( xstring::const_str ScopeName, int Count, T&& CallBack )  { scope{ *this }.beginScopeArray( ScopeName, Count, CallBack ); }

protected:

    struct scope_entry
    {
        xstring::const_str      m_Name       {nullptr};
        int                     m_ArrayIndex {-1};    //
        int                     m_ArrayCount {-1};
        int                     m_nProp      {0};
        int                     m_Sequence   {0};
    };

protected:

    mode                        m_EnumMode;
    xarray<scope_entry, 16>     m_Scope                     {};
    int                         m_iScope                    {-1};
    int                         m_Sequence                  {0};
    callback                    m_Callback;
    xarray<xproperty_data,2>    m_PropertyData              {};
    int                         m_nProccessedProperties     {-1};
    bool                        m_bForceFullPaths           { false };
    int                         m_iLastPropertyScope        {-1};
    int                         m_LastPropertySequence      {-1};

protected:

    xproperty_friends;
};

//--------------------------------------------------------------------------

class xproperty_query
{
public:

    using get_callback =  xfunction_view<bool( xproperty_query& Query, xproperty_data&        PropertyToQuery  )>;
    using set_callback =  xfunction_view<bool( xproperty_query& Query, const xproperty_data*& pPropertyToQuery )>;

public: // --- To be used by the person who is doing the query ---

//    constexpr bool isRealTime       ( void ) const { return m_Flags.m_IS_REAL_TIME; }
    
//    constexpr                   xproperty_query                     ( void )                                                = default;
    constexpr       bool        isDone                              ( void )                                    const       { return m_Flags.m_STATE == state::DONE;                                                                    }           
    x_forceinline   bool        RunGet                              ( const xproperty& Property, get_callback CallBack )    noexcept;
    x_forceinline   bool        RunSet                              ( xproperty&       Property, set_callback CallBack )    noexcept;

public: // --- To be used by the lambda function ----

    constexpr       auto&       getResults                          ( void )                                    const       { return m_GetPropertyData[1-(m_nProccessedProperties&1)];                                                     }
    constexpr       int         getResultsIndex                     ( void )                                    const       { return m_nProccessedProperties - 1;                                                                       }
    constexpr       int         hasResults                          ( void )                                    const       { return x_assert(false==isReceiving()), m_nProccessedProperties > 0;                                        }

public: // ---- To be used inside the property class ----

    constexpr       bool        isSearchingForScopes                ( void )                                    const       { return m_Flags.m_STATE == state::SEARCHING && m_Flags.m_IS_SEARCH_SCOPES;                                 }
    constexpr       int         getArrayIndex                       ( int MaxRange = 10000 )                    const       { return x_assert( m_CurPropArrayIndex < MaxRange ), m_CurPropArrayIndex;                                   }
    constexpr       int         getPropertyIndex                    ( void )                                    const       { return m_nProccessedProperties;                                                                           }
    x_incppfile     bool        NextPropertyOrChangeScope           ( void );

    template< typename T_CHAR, u32 T_CRC, int T_LENGTH > 
    x_forceinline   bool        isVar                               ( xstring_crcinfo<T_CHAR,T_CRC,T_LENGTH> Node ) noexcept;
    x_forceinline   bool        isVar                               ( const u32 CRC )                               noexcept;

    template< typename T_CHAR, u32 T_CRC, int T_LENGTH > 
    x_inline        bool        isScope                             ( xstring_crcinfo<T_CHAR, T_CRC, T_LENGTH> Node );
    x_inline        bool        isScope                             ( u32 CRC, xstring::const_str pStr, int StrLenth );
    constexpr       bool        isReceiving                         ( void )                                    const       { return m_Flags.m_IS_GET; }
    constexpr       bool        isSending                           ( void )                                    const       { return m_Flags.m_IS_GET == false; }
    x_forceconst    bool        isAtRoot                            ( void )                                    const       { return m_iScope == 0 && m_Flags.m_STATE == state::SEARCHING; }

    template< typename T >  
    constexpr       const xproperty_data&       getProp             ( void )                                    const       { return x_assert( T::t_symbol == getSetProperty().getType() ), x_assert( isReceiving() ), getSetProperty(); }

    template< typename T > 
    x_forceinline   xproperty_data&             setProp             ( void ); 

protected:
    
    enum class state : u16
    {
        SEARCHING,
        POPING_SCOPES,
        DONE,
        ENUM_COUNT
    };

    X_DEFBITS( flags,
               u16,
               0x0000,
               X_DEFBITS_ARG ( state,       STATE,                x_Log2IntRoundUp( int(state::ENUM_COUNT) - 1) ),    
               X_DEFBITS_ARG ( u16,         IS_GET,               1 ),                                         
               X_DEFBITS_ARG ( u16,         IS_REAL_TIME,         1 ),                                        
               X_DEFBITS_ARG ( u16,         IS_SEARCH_SCOPES,     1 )                                         
    );

    struct scope
    {
        u32                 m_CRC;              // current scope CRC
        int                 m_ArrayIndex;       // If the scope is an array then this will be the array index
        bool                m_isArray;          // tells the system if this entry is an array
        xstring::const_str  m_DEBUG_ScopeName;  // current scope Name
        int                 m_DEBUG_Length;     // Length of the scope name
    };

protected:

    x_forceinline   const xproperty_data*&      getSetProperty      ( void )       { x_assert( isReceiving() ); return m_SetPropertyData[m_nProccessedProperties&1]; }
    constexpr       const xproperty_data&       getSetProperty      ( void ) const { return x_assert( isReceiving() ), *m_SetPropertyData[m_nProccessedProperties&1]; }
    x_forceinline   xproperty_data&             getGetProperty      ( void )       { x_assert( false == isReceiving() ); return m_GetPropertyData[m_nProccessedProperties&1]; }
    constexpr       const xproperty_data&       getGetProperty      ( void ) const { return x_assert( false == isReceiving() ), m_GetPropertyData[m_nProccessedProperties&1]; }
                    void                        PrepareNextScope    ( void );
    x_inline        void                        Reset               ( void );

protected:

    get_callback                    m_GetNextPropertyLambda         {};
    set_callback                    m_SetNextPropertyLambda         {};
    xarray<xproperty_data, 2>       m_GetPropertyData               {{ xproperty_data{}, xproperty_data{} }};
    xarray<const xproperty_data*,2> m_SetPropertyData               {{ nullptr, nullptr }};
    u32                             m_CurPropNextCRC                {};
    int                             m_CurPropStringIndex            {};
    int                             m_CurPropNextStringIndex        {};
    int                             m_CurPropArrayIndex             {};
    int                             m_nScopesToPop                  {};
    bool                            m_bPropFound                    {};
    flags                           m_Flags                         {};
    int                             m_nProccessedProperties         {-1};
    xarray<scope,32>                m_ScopeData                     {};
    int                             m_iScope                        {0};

protected:

    xproperty_friends;
};

//-----------------------------------------------------------------------------

class xproperty
{
    x_object_type( xproperty, is_linear, rtti_start );

public:

    x_inline        void            PropEnum        ( xproperty_data_collection&    Collection, bool bFullPath = true, xproperty_enum::mode Mode = xproperty_enum::mode::REALTIME ) const;
    x_inline        void            PropEnum        ( xproperty_enum::callback      Function,   bool bFullPath = true, xproperty_enum::mode Mode = xproperty_enum::mode::REALTIME ) const;
    x_inline        bool            PropQueryGet    ( xproperty_query::get_callback Function )          const   noexcept;
    x_inline        bool            PropQuerySet    ( xproperty_query::set_callback Function )                  noexcept;
    x_inline        bool            PropQueryGet    ( xproperty_data_collection&    Collection )        const   noexcept;
    x_inline        void            PropToCollection( xproperty_data_collection&    Collection, bool bFullPath = true ) const;
                    void            Save            ( xtextfile& DataFile  ) ;
                    bool            Load            ( xtextfile& DataFile  ) ;
                    void            Save            ( const xwstring& FileName, xtextfile::access_type AccessType = xtextfile::access_type::MASK_DEFAULT );
                    bool            Load            ( const xwstring& FileName );

protected:

    x_forceinline   bool            onPropQuery     ( xproperty_query& Query ) const    noexcept { return const_cast<xproperty*>(this)->onPropQuery( Query ); }
    virtual         bool            onPropQuery     ( xproperty_query& Query )          noexcept = 0;
    virtual         void            onPropEnum      ( xproperty_enum&  Enum  ) const    noexcept = 0;

protected:

    xproperty_friends

//            const xproperty_type*   getPropertyType ( const char* pPropertyName );
           
};

