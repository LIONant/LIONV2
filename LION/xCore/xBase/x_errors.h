//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      A simple error system for functions
//------------------------------------------------------------------------------
template< typename T_ERROR_ENUM_TYPE >
class x_err 
{
    x_object_type( x_err, is_linear  );

public:

    x_constexprvar T_ERROR_ENUM_TYPE t_null_v = static_cast<T_ERROR_ENUM_TYPE>(0x00);

public:

    constexpr                       x_err           ( void )                            = default;
    constexpr                       x_err           ( const x_err& Error )  noexcept    : m_pError{ Error.m_pError }, m_Code{Error.m_Code} {}
    constexpr                       x_err           ( x_err&& Error )       noexcept    : m_pError{ Error.m_pError }, m_Code{Error.m_Code} { Error.m_pError = nullptr; Error.m_Code = t_null_v;        }
#if _X_DEBUG
    inline                         ~x_err           ( void )                noexcept    { x_assert(!m_pError); }
#endif
    constexpr                       x_err           ( std::nullptr_t )      noexcept    : m_pError{ nullptr }, m_Code{ t_null_v } {}
    constexpr                       x_err           ( const char* pString, const T_ERROR_ENUM_TYPE Code ) noexcept : m_pError{ pString }, m_Code{ Code } {}
    inline      x_err&              operator =      ( x_err&& Error )       noexcept    { m_pError = Error.m_pError; Error.m_pError = nullptr; m_Code = Error.m_Code; return *this;                  }
    constexpr   T_ERROR_ENUM_TYPE   getCode         ( void )    const       noexcept    { return static_cast<T_ERROR_ENUM_TYPE>(m_Code);                                                             } 
    constexpr   int                 getCodeAsInt    ( void )    const       noexcept    { return static_cast<int>(m_Code);                                                                           } 
    constexpr   const char*         getString       ( void )    const       noexcept    { return m_pError;                                                                                           } 
    constexpr   bool                isError         ( void )    const       noexcept    { return m_pError != nullptr;                                                                                }
    constexpr   bool                isOK            ( void )    const       noexcept    { return m_pError == nullptr;                                                                                }
    constexpr                       operator bool   ( void )    const       noexcept    { return m_pError != nullptr;                                                                                }
    inline      void                IgnoreError     ( void )                noexcept    { m_pError = nullptr; m_Code = t_null_v;                                                                     }

protected:

    const char*         m_pError  { nullptr };
    T_ERROR_ENUM_TYPE   m_Code    { t_null_v };
};

namespace x_errdef
{
    enum errors : u8
    {
        ERR_OK,
        ERR_FAILURE
    };

    using err = x_err<errors>;
}

#define x_error_code( ERROR_ENUM_TYPE, ENUM_ERROR_CODE, STRING )        { STRING, ERROR_ENUM_TYPE::ENUM_ERROR_CODE }
#define x_error_ok( ... )                                               nullptr 


//------------------------------------------------------------------------------
// Description:
//     These macros are the replacement for the standard exceptions functions.
//     <b>Please remember</b> that all exceptions are converted into x_assert ones they
//     are compile in game code. Exceptions in general are mainly used in editor and tools.
//     
//<TABLE>
//    x_try              Enable the exception handling from here down. It also 
//                        creates an scope. Such any variable declare between 
//                        this and a "terminator" will only exits inside that scope.
//</TABLE>
//
//<P> * After the x_try you must put one of the fallowing "terminators":
//
//<TABLE>
//    x_catch_display    This terminates the exception handling, and displays
//                        the message to the user. It doesn't throw the
//                        exception anywhere else.
//
//    x_catch_append     Appends a string and make the exception continue.
//
//    x_catch_begin      Terminates the exception handling, and begins a block
//                        where the exceptions that are thrown between x_trys
//                        and x_catch_begin can be handle. This requires x_catch_end.
//
//    x_catch_end        This command comes after the x_catch_begin and it is used
//                        to indicate that the handling block is over. This doesn't
//                        continue the exception. 
//
//    x_catch_end_ret    This command comes after the x_catch_begin and it is used
//                        to indicate that the handling block is over. Then it 
//                        continues the exception up.
//
//    x_append_throw     This command can only be use inside the x_catch_ begin/end. It is
//                        use to continue the exception going up the stack. But also
//                        allows to add more information about an exception been
//                        thrown. For instance you can put the function name where
//                        the block is so the user can get a call-stack back. 
//                        <B>EX:</B><CODE>
//                        x_append_throw( xfs( "CString::malloc( %d )", nBytes) ); 
//                            // or you can simple use
//                        x_append_throw( NULL );
//                            // is you have nothing to add for the exception.</CODE>
//</TABLE>
//
//<P>  You can throw any error any time you want, must you must pay attention
//     where you actually throw them. If you doing between the x_trys and any 
//     of the "terminators" you will be able to handle your own throw, if not it
//     will go up to the next function. 
//
//<TABLE>
//     x_throw         This command is the only one that the system understand 
//                      for getting errors. The use is simple: x_throw( Error message );
//                      The error message must be a String. You can of course use the 
//                      fs class to build a more complex message. x_throw should 
//                      not be use inside a x_catch_begin unless your intencion is to 
//                      terminate an ongoing exception and start a new one.
//</TABLE>
//    
// Example:
//<CODE>
//  void Test01( void )
//  {
//      char* pAlloc01 = NULL;  // Keep variables that may need to be 
//      char* pAlloc02 = NULL;  // clean up at the top of the file.
//  
//      
//  x_try;                // From here down we can catch exceptions
//
//      pAlloc01 = x_new( char, 1000, 0);
//      if( pAlloc01 == NULL ) 
//          x_throw( "Meaning full message here" );
//               
//      pAlloc02 = x_new( char, 1, 0);
//      if( pAlloc02 == NULL ) 
//          x_throw( "Meaning full message here" );
//
//      
//  x_catch_begin;              // Begin handle the errors
//
//      if( pAlloc01 ) x_delete( pAlloc01 );
//      if( pAlloc02 ) x_delete( pAlloc02 );
//
//  x_catch_end_ret;            // End and throw the exception up
//
//      x_memset( pAlloc01, 0, 1000 );
//  }
//
//  void Main( void )
//  {
//
//  x_try;                  // We want to catch any errors
//
//      Test01();           // We want to stop app if this ones fails
//      Test01();
//                          // Handle the next one individually
//      x_try;              // Lets catch the error if it throws any
//         Test01();
//      x_catch_begin;      // Begin handle 
//         ReleaseMemory();
//      x_catch_end;        // Must always finish the block
//      
//
//  x_catch_display;        // Nothing to clean up. This is the top level so
//                          // Lets display any error
//
//      x_printf( "We didn't crash!"); 
//  }  
//</CODE>
//------------------------------------------------------------------------------

struct xthrow
{
    char m_String[256];
};

template<typename... T_ARGS> x_inline  
void x_throw( const char* pFormatStr, const T_ARGS&... Args );

#ifdef X_EXCEPTIONS
    #define x_try                           try{ ((void)0)
    #define x_catch_display                 } catch( E_PARAM ) { static u32 exceptions_Flags=0;        if( x_ExceptionHandler( __FILE__, __LINE__, NULL, exceptions_Flags)){ X_BREAK }; } ((void)0)
    #define x_display_exception                          { static u32 exceptions_Flags=0;        if( x_ExceptionHandler( __FILE__, __LINE__, NULL, exceptions_Flags)){ BREAK }; x_ExceptionDisplay(); }((void)0)
    #define x_append_exception_msg(S)                    { static u32 exceptions_Flags=X_BIT(2); if( x_ExceptionHandler( __FILE__, __LINE__, S,    exceptions_Flags)){ BREAK };  }((void)0)
    #define x_catch_begin                   } catch( E_PARAM ) { ((void)0)
    #define x_catch_end                     } ((void)0)
    #define x_throw(S, ...)                 do{ static u32 exceptions_Flags=0; if( x_ExceptionHandler( __FILE__, __LINE__, (const char*)xfs(S, ##__VA_ARGS__ ), exceptions_Flags )){ X_BREAK }; throw(0); } while(0)
    #define x_append_throw(S)               x_append_exception_msg(S); throw(0)
    #define x_catch_append(S)               x_catch_begin; x_append_throw(S); x_catch_end
    #define x_catch_end_ret                 x_append_throw(NULL); x_catch_end   
#else
    #define x_try                     if( 1 ) {
    #define x_catch_display           }
    #define x_display_exception     
    #define x_append_exception_msg(S) x_assert(0 && S);
    #define x_catch_begin             } if( 0 ) {
    #define x_catch_end               }
//    #define x_throw( ... )            x_assert( false );//ASSERTS(0, (const char*)xfs( __VA_ARGS__ ) )
    #define x_append_throw( S )       x_assert( false );//ASSERTS(0,S)

    #define x_catch_append(S)         x_catch_begin; x_append_throw(S); x_catch_end
    #define x_catch_end_ret           x_append_throw(NULL); x_catch_end
#endif

#define x_catch_end_display x_display_exception; x_catch_end
