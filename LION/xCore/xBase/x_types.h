//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//      Define the basic types. 
//------------------------------------------------------------------------------
#if _X_TARGET_INTEL || _X_TARGET_MAC || _X_TARGET_LINUX
    using s8        = char;
    using u8        = unsigned char;
    using s16       = short;
    using u16       = unsigned short;
    using s32       = int;
    using u32       = unsigned int;
    using s64       = long long;
    using u64       = unsigned long long;
    using f32       = float;
    using f64       = double;
#endif

static_assert( sizeof(u8)  == 1, "" );
static_assert( sizeof(u16) == 2, "" );
static_assert( sizeof(u32) == 4, "" );
static_assert( sizeof(u64) == 8, "" );
static_assert( sizeof(f32) == 4, "" );
static_assert( sizeof(f64) == 8, "" );

using xbool     = u32;
using xbyte     = u8;


//------------------------------------------------------------------------------
// Description:
//      The standard character atomic types. Please note that on different system xwchar
//      will be of different sizes pc(16), mac(32), etc...
//------------------------------------------------------------------------------
using xchar     = char;
using xwchar    = wchar_t;

//------------------------------------------------------------------------------
// Description:
//      Add support for a common types for pointers 
//------------------------------------------------------------------------------
#if _X_TARGET_64BITS
    using xuptr   = u64;
#else
    using xuptr   = u32;
#endif

template< typename T >
union xdataptr
{
    u64     m_uPtr;
    T*      m_pPtr;

    constexpr                                   xdataptr            ( void )                noexcept = default;
    x_forceconst                    explicit    xdataptr            ( T* p )                noexcept : m_pPtr(p) { }
    x_forceinline   T*                          operator ->         ( void )                noexcept { return m_pPtr; }
    x_forceconst    const T*                    operator ->         ( void )        const   noexcept { return m_pPtr; }
    x_forceinline   xdataptr<T>&                operator =          ( T* pPtr )             noexcept { m_pPtr = pPtr; return *this; }
    x_forceconst    bool                        isValid             ( void )        const   noexcept { return nullptr != m_pPtr; }
    x_forceinline   void                        setNull             ( void )                noexcept { m_pPtr = nullptr; }
};

//------------------------------------------------------------------------------
// Description:
//      defines the type of object and what it can do.
// Algorithm:
//      Usually this is implemented using the hierarchy of objects, but there is a
//      problem doing this. even for classes that have not member variables and virtual
//      functions their size is non-zero. When using multiple in-heritage some compilers
//      add extra bytes to the structures. (I am looking at you VS2015). So this new 
//      method is just as clean without any of the related issues. 
// Example:
//      class test
//      {
//          x_object( test, is_linear, not_copyable, not_movable );
//      }
//------------------------------------------------------------------------------

// not thread safe and meant to only work in single threaded situations
#define x_object_type_is_linear(A)                                              \
public:                                                                         \
    enum : bool { t_is_quantum  = false };                                      \
    enum : bool { t_is_lockfree = false };                                      \
    enum : bool { t_is_lock     = false };

// thread safe lock object
#define x_object_type_is_quantum_lock(A)                                        \
public:                                                                         \
    enum : bool { t_is_quantum  = true  };                                      \
    enum : bool { t_is_lockfree = false };                                      \
    enum : bool { t_is_lock     = true };

// thread safe lockless object
#define x_object_type_is_quantum_lock_free(A)                                   \
public:                                                                         \
    enum : bool { t_is_quantum  = true  };                                      \
    enum : bool { t_is_lockfree = true  };                                      \
    enum : bool { t_is_lock     = false };                                      

// If it is not allocatable it must only live in the stack
#define x_object_type_is_not_allocatable(A)                                     \
protected:                                                                      \
    void* operator new      ( std::size_t sz, bool b )  = delete;               \
    void* operator new[]    ( std::size_t sz, bool b )  = delete;               \
    void  operator delete[] ( void* ptr, bool b )       = delete;               \
    void  operator delete   ( void* ptr, bool b )       = delete;

// objects that are not copyable
#define x_object_type_is_not_copyable(NO_COPYABLE)                              \
public:                                                                         \
                        NO_COPYABLE     ( const NO_COPYABLE& )     = delete;    \
      NO_COPYABLE&      operator =      ( const NO_COPYABLE& )     = delete;    

// objects that are not movable
#define x_object_type_is_not_movable(NO_MOVABLE)                                \
public:                                                                         \
                        NO_MOVABLE      ( NO_MOVABLE&& )           = delete;    \
      NO_MOVABLE&       operator =      ( NO_MOVABLE&& )           = delete;


// Dealing with object that have RTTI
// Use: rtti_start                          - for the base classes without parents
// Use: rtti( parent list with commas )     - for classes that are derived from base classes after the rtti_start, example: rtti( xproperty )
#define RTTI_FIRST_PARENT(...) X_VARG_EXPAND( RTTI_FIRST_PARENT_HELPER(__VA_ARGS__) )
#define RTTI_FIRST_PARENT_HELPER(first, ...) first

namespace x_types_details
{
    template< typename T, typename T_DST > inline
    typename std::enable_if
    <
        // Up cast to DST
        std::is_same< T, T_DST >::value == false && true == std::is_base_of< T, T_DST >::value
        , T_DST*
    >::type
    internal_cast( T* A )
    {
        const static auto offset = -(((s64)(xbyte*)(T*)(T_DST*)1) - 1);
        return reinterpret_cast<T_DST*>( &((xbyte*)A)[ offset ] );
    }

    template< typename T, typename T_DST > inline
    typename std::enable_if
    <
        // Down cast to T_DST
        true == std::is_base_of< T_DST, T >::value
        , T_DST*
    >::type
    internal_cast( T* A )
    {
        return static_cast<T_DST*>(A);
    }

    template< typename T, typename T_DST > inline
    typename std::enable_if
    <
        false == std::is_base_of< T_DST, T >::value 
        && false == std::is_base_of< T, T_DST >::value
        , T_DST*
    >::type
    internal_cast( T* A )
    {
        static_assert( false, "x_RTTI SafeCast: Don't know how to cast this type");
        return reinterpret_cast<T_DST*>(A);
    }
}

#define __rtti_common_functions                                                                                                                \
    template< typename T > x_forceinline bool       isKindOf( void ) const { return getVRTTI().isKindofV( T::getRTTI() ); }                    \
    template< typename T > x_forceinline const T&   SafeCast( void ) const { x_assert( isKindOf<T>() ); return *x_types_details::internal_cast<typename std::remove_reference<decltype(*this)>::type, T >( this ); }  \
    template< typename T > x_forceinline T&         SafeCast( void )       { x_assert( isKindOf<T>() ); return *x_types_details::internal_cast<typename std::remove_reference<decltype(*this)>::type, T >( this ); }  \

//return *reinterpret_cast<T*>(this); }  \


#define x_object_type_rtti_class_name( CLASSNAME )  X_STRINGIFY( CLASSNAME ) "->" __FUNCSIG__ ":" X_STRINGIFY( __LINE__ ) 

#define x_object_type_rtti_restart( CLASSNAME )                                                         \
public:                                                                                                 \
    virtual const x_rtti_base& getVRTTI( void ) const noexcept override { return getRTTI(); }           \
    __rtti_common_functions                                                                             \
    x_forceinline static const x_rtti_base& getRTTI(void){ static const x_rtti_base s_RTTI{ x_object_type_rtti_class_name(CLASSNAME) }; return s_RTTI; }

#define x_object_type_rtti_start( CLASSNAME )                                                           \
public:                                                                                                 \
    virtual const x_rtti_base& getVRTTI( void ) const noexcept { return getRTTI(); }                    \
    __rtti_common_functions                                                                             \
    x_forceinline static const x_rtti_base& getRTTI(void){ static const x_rtti_base s_RTTI{ x_object_type_rtti_class_name(CLASSNAME) }; return s_RTTI; }

#define x_object_type_rtti( ... )  X_VARG_EXPAND( _x_object_type_rtti( __VA_ARGS__ ) ) _x_object_type_rtti_createhook  
#define _x_object_type_rtti_createhook( CLASSNAME )   x_object_type_rtti_class_name(CLASSNAME) };  return s_RTTI; } 
#define _x_object_type_rtti( ... )                                                                      \
public:                                                                                                 \
    using t_parent = RTTI_FIRST_PARENT(__VA_ARGS__);                                                    \
    virtual const x_rtti_base& getVRTTI( void ) const noexcept override { return getRTTI(); }           \
    __rtti_common_functions                                                                             \
    x_forceinline static const x_rtti<t_self, X_VARG_EXPAND( __VA_ARGS__ ) >& getRTTI(void){ static const x_rtti<t_self, X_VARG_EXPAND( __VA_ARGS__ ) > s_RTTI{

//------------------------------------------------------------------------------
// Main macros for object types
//------------------------------------------------------------------------------
#define __x_object_type_arg( N_PARAMS, INC, DEC, CLASS_NAME, PARAM ) X_ARG_CONCATENATE( x_object_type_ , PARAM )( CLASS_NAME )   
#define x_object_type( CLASS_NAME, ... ) using t_self = CLASS_NAME; X_FOR_EACH_ONE_PARAM( CLASS_NAME, __x_object_type_arg,  __VA_ARGS__  ) public: 

//------------------------------------------------------------------------------
// Description:
//      Add support for SSE math and types 
//------------------------------------------------------------------------------
#ifdef _X_SSE4_SUPPORT
    using f32x4     = __m128;
    
    template<unsigned i> constexpr
    f32 x_getf32x4ElementByIndex( f32x4 V )
    {
        static_assert( i>=0 && i<=4, "Index out of range" );
        #if defined __SSE4_1__ || defined __SSE4_2__ 
            return _mm_extract_epi32(V, i);
        #else
            // shuffle V so that the element that you want is moved to the least-
            // significant element of the vector (V[0])
            // return the value in V[0]
            return _mm_cvtss_f32( _mm_shuffle_ps(V, V, _MM_SHUFFLE(i, i, i, i)) );
        #endif
    }
#else
    union f32x4
    {
        struct { f32 x, y, z, w; };
        f32 m[4];
    };

    template<unsigned i> constexpr
    f32 x_getf32x4ElementByIndex( f32x4 V )
    {
        static_assert( i>=0 && i<=4, "Index out of range" );
        return V.m[i];
    }

#endif

//------------------------------------------------------------------------------
// Description:
//      Numeric limits 
//------------------------------------------------------------------------------
#define X_U8_MAX                           255
#define X_U8_MIN                             0
                                
#define X_U16_MAX                        65535
#define X_U16_MIN                            0
                               
#define X_U32_MAX             ((u32)4294967295)
#define X_U32_MIN                            0

#define X_U64_MAX       u64(0xffffffffffffffff)
#define X_U64_MIN                            0

#define X_S8_MAX                           127
#define X_S8_MIN                          -128
                            
#define X_S16_MAX                        32767
#define X_S16_MIN                       -32768
                            
#define X_S32_MAX             ((s32)2147483647)
#define X_S32_MIN            -((s32)2147483647)

#define X_S64_MAX          9223372036854775807
#define X_S64_MIN         -9223372036854775808

#define X_F32_MIN              1.175494351e-38f    // Smallest POSITIVE f32 value.
#define X_F32_MAX              3.402823466e+38f

#define X_F64_MIN       2.2250738585072014e-308    // Smallest POSITIVE f64 value.
#define X_F64_MAX       1.7976931348623158e+308

//------------------------------------------------------------------------------
// Description:
//      Converts from a type size to an unsigned integer type equivalent
//      Check x_Align for an example on why to use this
//------------------------------------------------------------------------------
template< int T_SIZE >  struct x_size_to_unsigned       {};
template<>              struct x_size_to_unsigned<1>    { using type = u8;  };
template<>              struct x_size_to_unsigned<2>    { using type = u16; };
template<>              struct x_size_to_unsigned<4>    { using type = u32; };
template<>              struct x_size_to_unsigned<8>    { using type = u64; };

template< int T_SIZE >  struct x_size_to_signed         {};
template<>              struct x_size_to_signed<1>      { using type = s8;  };
template<>              struct x_size_to_signed<2>      { using type = s16; };
template<>              struct x_size_to_signed<4>      { using type = s32; };
template<>              struct x_size_to_signed<8>      { using type = s64; };

//------------------------------------------------------------------------------
// Description:
//      Converts from a type to a proper sign or unsigned integer of the correct size.
//------------------------------------------------------------------------------
namespace detail
{
    template< bool T_IS_UNSIGNED >
    struct typesize_to_proper_int;
    
    template<>
    struct typesize_to_proper_int<true>
    {
        template< typename T_TYPE >
        using type = typename x_size_to_unsigned<sizeof( T_TYPE )>::type;
    };

    template<>
    struct typesize_to_proper_int<false>
    {
        template< typename T_TYPE >
        using type = typename x_size_to_signed<sizeof( T_TYPE )>::type;
    };
};

template< typename T >
struct x_type_to_proper_sizedint
{
    using type = typename detail::typesize_to_proper_int< 
        std::is_unsigned<T>::value 
        >::template type<T>;
};

//------------------------------------------------------------------------------
// Description:
//      The standard atomic support
//------------------------------------------------------------------------------
template< typename T > 
class x_atomic : public std::atomic<T>
{
    x_object_type( x_atomic, is_quantum_lock_free )

public:                          
    constexpr x_atomic( void )                  noexcept = default;
    constexpr x_atomic( const T&& e )           noexcept :  std::atomic<T>( e ) {} // this->store( e, std::memory_order_relaxed );                                  }
    constexpr x_atomic( const T&  e )           noexcept :  std::atomic<T>( e ) {} // this->store( e, std::memory_order_relaxed );                                  }
    constexpr x_atomic( const x_atomic<T>& e )  noexcept :  std::atomic<T>( e.load(std::memory_order_relaxed) ) {} // this->store( e.load(std::memory_order_relaxed), std::memory_order_relaxed );  }
};

//------------------------------------------------------------------------------
// Description:
//      The standard atomic support
//------------------------------------------------------------------------------
class x_atomic_flag : public std::atomic_flag
{
public:
    x_forceinline   x_atomic_flag   ( bool flag )           noexcept : std::atomic_flag{ flag }{}
    constexpr       x_atomic_flag   ( void ) noexcept : std::atomic_flag{}  {};
    inline  bool    isLocked        ( void )        const   noexcept { using t = const x_size_to_unsigned<sizeof(*this)>::type; return !!*reinterpret_cast< t* >(this); }
};

//------------------------------------------------------------------------------
// Description:
//      Use for all pointers that are going to be owners (pointers responsable for releasing the memory)
// Algorithm:
//      Following the new C++ core guidelines
//------------------------------------------------------------------------------
template< class T > using xowner = T;

template< typename T_ENTRY, typename T_COUNTER = xuptr > class xbuffer_view;



//------------------------------------------------------------------------------
// Description:
//      C++ 11 Standard Iterator for xBase structures 
//------------------------------------------------------------------------------

namespace _x_types
{
    template< typename T_CONTAINER, typename T_COUNTER >
    class iterator_base 
    {
        x_object_type( iterator_base, is_linear );

    public:

        using           t_container     = typename std::remove_reference<T_CONTAINER>::type;
        using           t_entry         = typename t_container::t_entry;
        using           t_counter_type  = T_COUNTER;
        using           t_counter       = typename t_container::t_counter;

    public:

        constexpr                           iterator_base   ( t_counter_type i, t_container& Ref ) : m_i{i}, m_Ref{Ref}        { }
        constexpr       const auto&         operator *      ( void ) const                                          { return m_Ref[m_i]; }
        inline          auto&               operator *      ( void )                                                { return m_Ref[m_i]; }

    protected:

        t_counter_type      m_i;
        t_container&        m_Ref;
    };
}

//------------------------------------------------------------------------------

template< typename T_CONTAINER >
struct x_iter : _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter >
{
        using t_self        =  x_iter<T_CONTAINER>;
        using t_parent      =  _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter >;
        using t_counter     = typename t_parent::t_counter;
        using t_container   = typename t_parent::t_container;

        constexpr                           x_iter          ( t_counter i, t_container& Ref ) : t_parent( i, Ref ) { }
        constexpr                           x_iter          ( t_container& Ref ) : t_parent( t_counter{0}, Ref )   { }
        inline          const auto&         operator ++     ( void )                                                { ++t_parent::m_i; return *this; }
        constexpr       bool                operator !=     ( const t_parent& ) const                               { return t_parent::m_i != t_parent::m_Ref.getCount(); }
};

//------------------------------------------------------------------------------

template< class T_CONTAINER >
struct x_iter_r : _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter >
{
        using t_self        =  x_iter_r<T_CONTAINER>;
        using t_parent      =  _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter >;
        using t_counter     = typename t_parent::t_counter;
        using t_container   = typename t_parent::t_container;

        constexpr                           x_iter_r        ( t_counter i, t_container& Ref ) : t_parent( i, Ref ) { }
        constexpr                           x_iter_r        ( t_container& Ref ) : t_parent( t_counter{0}, Ref )   { }
        inline          const auto&         operator ++     ( void )                                               { --t_parent::m_i; return *this; }
        constexpr       bool                operator !=     ( const t_parent& ) const                              { return t_parent::m_i != static_cast<t_counter>(-1); }
};

//------------------------------------------------------------------------------
// Description:
//      Allows range loop to have a variable reference for the iterator
//------------------------------------------------------------------------------
namespace _x_types
{
    //------------------------------------------------------------------------------
    template< typename T_CONTAINER >
    struct iter_ref : _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter& >
    {
        using t_self            =  iter_ref<T_CONTAINER>;
        using t_parent          =  _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter& >;
        using t_container       = typename t_parent::t_container;
        using t_counter         = typename t_parent::t_counter;
        using t_counter_type    = typename t_parent::t_counter_type;

        constexpr                           iter_ref        ( t_counter_type i, T_CONTAINER& Ref ) : t_parent{ i, Ref } { }
        inline          const auto&         operator ++     ( void )                                                    { ++t_parent::m_i; return *this; }
        constexpr       bool                operator !=     ( const t_parent& ) const                                   { return t_parent::m_i != t_parent::m_Ref.getCount(); }
    };

    //------------------------------------------------------------------------------
    template< typename T_CONTAINER >
    struct iter_ref_r : _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter& >
    {
        using t_self            =  iter_ref_r<T_CONTAINER>;
        using t_parent          =  _x_types::iterator_base< T_CONTAINER, typename T_CONTAINER::t_counter& >;
        using t_container       = typename t_parent::t_container;
        using t_counter         = typename t_parent::t_counter;
        using t_counter_type    = typename t_parent::t_counter_type;

        constexpr                           iter_ref_r      ( t_counter_type i, t_container& Ref )  : t_parent( i, Ref )    { }
        inline          const auto&         operator ++     ( void )                                { --t_parent::m_i; return *this; }
        constexpr       bool                operator !=     ( const t_parent& ) const               { return t_parent::m_i != t_counter{-1}; }
    };

    //------------------------------------------------------------------------------
    template< typename T_CONTAINER >
    struct iter_ref_wrapper 
    { 
        using           t_container = typename std::remove_reference<T_CONTAINER>::type;
        using           t_entry     = typename t_container::t_entry;
        using           t_counter   = typename t_container::t_counter;

        t_counter&      m_I;
        T_CONTAINER&    m_Iter; 
    };
}

//------------------------------------------------------------------------------
template <typename T_CONTAINER > inline
// Seems visual studio does not like this line... 
//auto x_iter_ref( typename T_CONTAINER::t_counter& Iter, T_CONTAINER&& Iterable ) { return _x_types::iter_ref_wrapper<T_CONTAINER>{ Iter, Iterable }; }
auto x_iter_ref( xuptr& Iter, T_CONTAINER&& Iterable ) { return _x_types::iter_ref_wrapper<T_CONTAINER>{ Iter, Iterable }; }

//------------------------------------------------------------------------------
template <typename T_CONTAINER> inline
auto begin ( _x_types::iter_ref_wrapper<T_CONTAINER> I )  
{ 
    return _x_types::iter_ref< typename std::remove_reference<T_CONTAINER>::type >( I.m_I, I.m_Iter ); 
}

//------------------------------------------------------------------------------
template <typename T_CONTAINER> inline
auto end   ( _x_types::iter_ref_wrapper<T_CONTAINER> I )  
{ 
    return _x_types::iter_ref< typename std::remove_reference<T_CONTAINER>::type >( I.m_I, I.m_Iter ); 
}

//------------------------------------------------------------------------------
// Description:
//      function to reverse the direction of range loops
// Algorithm:
//      http://stackoverflow.com/questions/8542591/c11-reverse-range-based-for-loop
//------------------------------------------------------------------------------
namespace _x_types
{
    template <typename T_CONTAINER>
    struct iter_reversion_wrapper { T_CONTAINER& m_Iter; };
}

template <typename T_CONTAINER> inline
auto begin ( _x_types::iter_reversion_wrapper<T_CONTAINER> I )  { return I.m_Iter.rbegin(); }

template <typename T_CONTAINER> inline
auto end   ( _x_types::iter_reversion_wrapper<T_CONTAINER> I )  { return I.m_Iter.rend(); }

template <typename T_CONTAINER> inline
_x_types::iter_reversion_wrapper<T_CONTAINER> x_iter_reverse( T_CONTAINER&& Iterable ) { return { Iterable }; }

//------------------------------------------------------------------------------
// Description:
//      get the thread id in 16 bits
//------------------------------------------------------------------------------
inline u16 x_getThreadID16( void )
{
#if _X_TARGET_64BITS
    static_assert(sizeof( xuptr ) == 8, "");
    const xuptr ThreadHash64 = std::hash<std::thread::id>()(std::this_thread::get_id());
    const u32   ThreadHash32 = static_cast<u32>((ThreadHash64 >> 32) ^ ThreadHash64);
    return static_cast<u16>(((ThreadHash32 >> 16) ^ ThreadHash32));
#else
    static_assert( sizeof( xuptr ) == 4, "" );
    const xuptr ThreadHash32 = std::hash<std::thread::id>()(std::this_thread::get_id());
    return static_cast<unsigned short>( ((ThreadHash32 >> 16) ^ ThreadHash32) );
#endif
}

//------------------------------------------------------------------------------
// Description:
//     This class is used to define a handle which is 32bits wide. Handles have the property
//     that can be pass to function as a unique type. This makes function type checking much 
//     safer. The only thing which is standard in a handle is its NULL value which is xhandle::HNULL
//     while it has a member variable and it is public it is not recommended to access it if
//     you don't have to.
//------------------------------------------------------------------------------
template< typename T_TYPE >
struct xhandle
{
    u32 m_Value {};

    x_constexprvar u32 null = 0xffffffff; 

    constexpr                               xhandle         ( void )                        noexcept = default;
    x_forceconst explicit                   xhandle         ( std::nullptr_t )                   noexcept : m_Value{ null }             {}
    x_forceconst            bool            isValid         ( void )                const   noexcept { return m_Value != null;     }    
    x_forceconst            bool            isNull          ( void )                const   noexcept { return m_Value == null;     }    
    x_forceinline           void            setNull         ( void )                        noexcept { m_Value =  null;            }  
    x_forceconst            u32             get             ( void )                const   noexcept { return m_Value;             } 
    x_forceinline           void            set             ( u32 V )                       noexcept { m_Value = V;                } 
    x_forceconst            bool            operator  ==    ( xhandle H )           const   noexcept { return H.m_Value==m_Value; }
    x_forceconst            bool            operator  !=    ( xhandle H )           const   noexcept { return H.m_Value!=m_Value; }
};

//------------------------------------------------------------------------------
// Description:
//     This is for general flags
//------------------------------------------------------------------------------
template< typename T_UNIQUE, typename T_ATOMIC >
class xflags
{
public:

    using t_self    = xflags;
    using t_atomic  = T_ATOMIC;

public:

    constexpr                               xflags          ( void )                      noexcept    = default;
    constexpr                               xflags          ( const xflags& )             noexcept    = default;
    x_forceconst                explicit    xflags          ( const t_atomic V )          noexcept    : m_Value{ V } {}
    x_forceconst    t_self                  operator |      ( const t_self S )    const   noexcept    { return t_self{ static_cast<t_atomic>(m_Value | S.m_Value) }; }
    x_forceinline   t_self&                 operator |=     ( const t_self S )            noexcept    { m_Value |= S.m_Value; return *this; }
    x_forceconst    t_self                  operator &      ( const t_self S )    const   noexcept    { return t_self{ static_cast<t_atomic>(m_Value & S.m_Value) }; }
    x_forceinline   t_self&                 operator &=     ( const t_self S )            noexcept    { m_Value &= S.m_Value; return *this; }
    x_forceconst    t_self                  operator ~      ( void )              const   noexcept    { return t_self{ static_cast<t_atomic>(~m_Value) }; }
    x_forceconst    t_self                  operator ^      ( const t_self S )    const   noexcept    { return t_self{ static_cast<t_atomic>(m_Value ^ S.m_Value) }; }
    
    x_forceconst    bool                    operator ==     ( const t_self S )    const   noexcept    { return m_Value == S.m_Value; }
    x_forceconst    bool                    operator !=     ( const t_self S )    const   noexcept    { return m_Value != S.m_Value; }
    x_forceconst    bool                    isSupersetOf    ( const t_self S )    const   noexcept    { return (m_Value&S.m_Value) == S.m_Value; }
    x_forceconst    bool                    isSubsetOf      ( const t_self S )    const   noexcept    { return (m_Value&S.m_Value) == m_Value; }

public:

    t_atomic m_Value {};
};

