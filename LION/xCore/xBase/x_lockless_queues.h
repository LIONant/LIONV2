//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      Quantum Tag Pointer. It is an atomic<T*> but the pointer has a tag inside of it.
//      This tag is modify every time the pointer changes. This is a well establish technique
//      used to avoid the lockless ABA issue.
//
//      Standard memory ordering:
//      ACQUIRE semantics is a property which can only apply to operations which READS 
//      from (Quantum/shared memory), whether they are read-modify-write operations or plain loads. 
//      The operation is then considered a read-acquire. Acquire semantics prevent memory 
//      reordering of the read-acquire with any read or write operation which follows it 
//      in program order.
//      Synchronizes all visible side effects from the last release or sequentially consistent operation.
//      "All quantum memory operations stay below this line"
//
//      RELEASE semantics is a property which can only apply to operations which WRITE to 
//      shared memory, whether they are read-modify-write operations or plain stores. 
//      The operation is then considered a write-release. Release semantics prevent memory 
//      reordering of the write-release with any read or write operation which precedes it 
//      in program order.
//      Release	Synchronizes side effects with the next consume or acquire operation.
//      "All quantum memory operations stay above this line"
//------------------------------------------------------------------------------
template<class T_ENTRY>
class x_ll_tagptr 
{
    x_object_type( x_ll_tagptr, is_quantum_lock_free );

public:
    
    using t_entry       = T_ENTRY;
    using t_tagptr_base = t_self;

public:
    
	#if _X_TARGET_64BITS
        using tag       = u16;

        struct local
        {
        public:

            constexpr               local           ( void )                        noexcept = default;
            constexpr bool          operator !=     ( const local LocalB )  const   noexcept { return m_Data != LocalB.m_Data;   }
            constexpr bool          operator ==     ( const local LocalB )  const   noexcept { return m_Data == LocalB.m_Data;   }
            constexpr t_entry*	    getPtr          ( void )                const   noexcept { return reinterpret_cast<t_entry*>(m_Data&PTR_MASK);     }
            constexpr bool          isNull          ( void )                const   noexcept { return getPtr() == nullptr;       }
            constexpr bool          isValid         ( void )                const   noexcept { return getPtr() != nullptr;       }
            constexpr tag           getTag          ( void )                const   noexcept { return m_Tag;                     }

        protected:
            
            enum flags : u64
            {
                PTR_MASK = 0xffffffffffffUL
            };

            union
            {
                u64                m_Data {};      // bits of the qt pointer
                u64                m_Ptr : (3*16);

                struct
                {
                    u16            m_Pad[3];       // Reserve for the actual pointer
                    tag            m_Tag;          // Guard/Tag
                };
            };

            template<class> friend class x_ll_tagptr;
        };

    #else

        using tag       = u32;
    
        union local
        {
        public:
            constexpr bool          operator !=     ( const local& LocalB ) const   noexcept { return m_Data != LocalB.m_Data;   }
            constexpr bool          operator ==     ( const local& LocalB ) const   noexcept { return m_Data == LocalB.m_Data;   }
            constexpr t_entry*      getPtr          ( void ) const                  noexcept { return m_pPtr;                    }
            constexpr bool          isNull          ( void ) const                  noexcept { return getPtr() == nullptr;       }
            constexpr bool          isValid         ( void ) const                  noexcept { return getPtr() != nullptr;       }
            constexpr tag           getTag          ( void ) const                  noexcept { return m_Tag;                     }

            u64                 m_Data;         // bits of the qt pointer

        protected:

            struct
            {
                t_entry*        m_pPtr;         // Actual 32 bits pointer
                tag             m_Tag;          // Guard/Tag
            };

            template<class> friend class x_qt_tagptr;
        };
    #endif

public:

    inline                      x_ll_tagptr     ( void )                                                        noexcept;
    inline explicit             x_ll_tagptr     ( const t_self& Q )                                             noexcept { setup(Q);                                                     }
    inline explicit             x_ll_tagptr     ( const t_self& Q, void* pPtr )                                 noexcept { setup(Q); setup(pPtr);                                        }
    inline                      x_ll_tagptr     ( t_entry* pPtr )                                               noexcept { setup(pPtr);                                                  }

    inline      local           getLocal        ( std::memory_order order = std::memory_order_seq_cst ) const   noexcept { return m_qtPointer.load( order );                             }
    inline      void            setup           ( const t_entry* pPtr )                                         noexcept;
    inline      void            setup           ( const t_self& B )                                             noexcept { m_qtPointer.store( B.m_qtPointer.load(std::memory_order_relaxed), std::memory_order_relaxed );  }
    inline      void            setupNull       ( void )                                                        noexcept { setup(NULL);                                                  }
    constexpr   bool            isValid         ( void ) const                                                  noexcept { return getLocal().getPtr() != NULL;                           }
    constexpr   bool            isNull          ( void ) const                                                  noexcept { return getLocal().getPtr() == NULL;                           }

    inline      bool            CompareAndSet   ( local& LocalPointer, const t_entry* const pNewPtr, std::memory_order Success, std::memory_order Failure )         noexcept;
    inline      bool            CompareAndSet   ( local& LocalPointer, const t_entry* const pNewPtr, std::memory_order MemoryOrder = std::memory_order_seq_cst )    noexcept;
    inline      bool            CompareAndSet   ( local& LocalPointer, const local NextPointer, std::memory_order Success, std::memory_order Failure )              noexcept;
    inline      bool            CompareAndSet   ( local& LocalPointer, const local NextPointer, std::memory_order MemoryOrder = std::memory_order_seq_cst )         noexcept;
    inline      void            LinkListSetNext ( t_self& Node, std::memory_order MemoryOrder = std::memory_order_seq_cst )                                          noexcept;

protected:

    x_atomic<local> m_qtPointer      {};

protected:

    template<typename, template<typename,typename> class, typename> friend class x_ll_fixed_pool_general;
    template< class > friend class x_ll_mpsc_node_queue;
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpmc    - Multi-Producer(push), Multi-Consumer(pop) queue
//      bounded - Means that there is an upper bound on how many nodes could be in the queue
//      queue   - FIFO ( First In first out )
//      Queue is ABA resistant.
//      Main advantage between a bounded queue vs a node queue is that bounded queue are faster.
//------------------------------------------------------------------------------
template<typename T_ENTRY, xuptr T_COUNT = 1024 >
class x_ll_mpmc_bounded_queue
{
    x_object_type( x_ll_mpmc_bounded_queue, is_quantum_lock_free, is_not_copyable, is_not_movable );

public:

    using               t_entry             = T_ENTRY;
    static_assert( ((T_COUNT-1)&T_COUNT) == 0, "Queue size must be power of 2" );

public:

    constexpr x_ll_mpmc_bounded_queue( void ) noexcept = default;

    x_inline void push( t_entry& Node ) noexcept
    {
        auto EnqueuePos  = m_EnqueuePos.load( std::memory_order_relaxed );
        do
        {
            x_assume( (m_DequeuePos.load()&t_buffer_mask) != ((m_EnqueuePos.load()+1)&t_buffer_mask) );

            auto&       qtTagPtr    = m_qtTagPtrArray[ EnqueuePos & t_buffer_mask ];
            t_tagptr    LocalTagPtr = qtTagPtr.load( );
            const auto  iNext       = EnqueuePos + 1;

            // If the pointer is null and the tag are the same then this is a good retry to set
            // The tag is needed because:
            //      A push worker may fall sleep inside this 'while' then when it tries to do the exchange 
            //      It could find the pointer to be null still and in reality he is completely out of sequence
            //      with the rest of the universe. So the tag allows him to know that he is out of sequence.
            
            while( LocalTagPtr.getPtr() == nullptr && !((LocalTagPtr.m_Tag ^ (EnqueuePos >> t_pow_of_two )) & t_tag_mask) )
            { 
                const t_tagptr NewTagPtr{ &Node, static_cast<u16>(LocalTagPtr.m_Tag + 1u) };
                if( qtTagPtr.compare_exchange_weak( LocalTagPtr, NewTagPtr ) )
                {
                    m_EnqueuePos.compare_exchange_weak( EnqueuePos, iNext );
                    return;
                }
            }

            if( m_EnqueuePos.compare_exchange_weak( EnqueuePos, iNext ) )
                EnqueuePos = iNext;

        } while(true);
    }

    x_inline t_entry* pop( void ) noexcept
    {
        auto DequeuePos  = m_DequeuePos.load( std::memory_order_relaxed );
        do
        {
            auto&       qtTagPtr    = m_qtTagPtrArray[ DequeuePos & t_buffer_mask ];
            auto        LocalTagPtr = qtTagPtr.load( );
            const auto  iNext       = DequeuePos + 1;
            const auto  iEnqueue    = m_EnqueuePos.load( );
           
            while( LocalTagPtr.getPtr() )
            {
                const t_tagptr NewTagPtr{ nullptr, LocalTagPtr.m_Tag };
                if( qtTagPtr.compare_exchange_weak( LocalTagPtr, NewTagPtr ) )
                {
                    if( (iNext ^ iEnqueue) & t_buffer_mask ) 
                        m_DequeuePos.compare_exchange_weak( DequeuePos, iNext );

                    return LocalTagPtr.getPtr();
                }
            }

            // No more entry in the queue
            if( !((iNext ^ iEnqueue) & t_buffer_mask ) )
                return nullptr;

            if( m_DequeuePos.compare_exchange_strong( DequeuePos, iNext ) )
                DequeuePos = iNext;

        } while(1);
    }

    x_forceinline t_entry* steal( void ) noexcept { return pop(); }

protected:

    x_constexprvar u32  t_buffer_size       = static_cast<u32>(T_COUNT);
    x_constexprvar u32  t_buffer_mask       = t_buffer_size - 1;
    x_constexprvar s32  t_pow_of_two        = x_Log2Int( t_buffer_size );

    using               t_tagptr            = xtagptr<T_ENTRY,u16>;

    // where: 16bits is the size of the tag, 32bits is the size of EnqueuePos 
    x_constexprvar u32 t_tag_mask = (1 << x_Min( 16, 32 - t_pow_of_two )) - 1;
    static_assert( t_tag_mask >= 1,                         "This should never be less than 1" );
    static_assert( 1 << (t_pow_of_two) == t_buffer_size,    "This should be the right power of two!" );

protected:

    alignas (x_target::getCacheLineSize()) x_atomic<u32>                             m_EnqueuePos   {  0u };
    alignas (x_target::getCacheLineSize()) x_atomic<u32>                             m_DequeuePos   { ~0u };
    alignas (x_target::getCacheLineSize()) xarray<x_atomic<t_tagptr>, t_buffer_size> m_qtTagPtrArray{ {t_tagptr{0u}} };
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      spmc    - Single-Producer(push), Multi-Consumer(pop) queue
//      bounded - Means that there is an upper bound on how many nodes could be in the queue
//      queue   - FIFO ( First In first out )
//      Queue is ABA resistant.
//      Main advantage between a bounded queue vs a node queue is that bounded queue are faster.
//------------------------------------------------------------------------------
template< typename T_ENTRY, xuptr T_COUNT = 1024 >
class x_ll_spmc_bounded_queue
{
    x_object_type( x_ll_spmc_bounded_queue, is_quantum_lock_free, is_not_copyable, is_not_movable );
       
public:
    
    using t_entry = T_ENTRY;
    static_assert( ((T_COUNT-1)&T_COUNT) == 0, "Queue size must be power of 2" );
    
public:
    
    constexpr  x_ll_spmc_bounded_queue ( void ) noexcept = default;
    inline t_entry* pop( void ) noexcept 
    {
        xuptr    iLocal          = m_iTail.load( std::memory_order_relaxed );
        do
        {
            const xuptr iNext =  (iLocal+1)&MASK;  
            t_entry*    pNode = m_pArray[iLocal].load( std::memory_order_relaxed );
            
            while( pNode )
            {
                if( m_pArray[iLocal].compare_exchange_weak( pNode, nullptr, std::memory_order_acquire ) )
                {
                    if( iNext != m_iHead.load( std::memory_order_relaxed ) ) 
                        m_iTail.compare_exchange_weak( iLocal, iNext, std::memory_order_relaxed );

                    return pNode;
                }
            };
            
            if( iNext == m_iHead.load( std::memory_order_relaxed ) )
                return nullptr;

            if( m_iTail.compare_exchange_weak( iLocal, iNext, std::memory_order_relaxed ) ) 
                iLocal = iNext;
            
        } while(1);
    }
    
    inline void push( t_entry& Node ) noexcept 
    {
        x_assert_linear( m_Debug_LQ );
        
        const xuptr iLocal = m_iHead.load( std::memory_order_relaxed );
#if 0
        // Check for out of memory condition... right now we simply loop and wait
        while( (iLocal+1) == m_iTail.load( std::memory_order_acquire ) )
            iLocal = m_iHead.load( std::memory_order_relaxed );
#else
        x_assume( ((iLocal+1)&MASK) != m_iTail.load( std::memory_order_acquire ) );
#endif
        
        m_pArray[iLocal].store( &Node,  std::memory_order_relaxed );
        m_iHead.store( (iLocal+1)&MASK, std::memory_order_relaxed  );
    }
    
    inline t_entry* steal( void )  noexcept { return pop(); }
    
protected:
    
    enum : xuptr { MASK = T_COUNT-1 };
    
protected:
    
    alignas( x_target::getCacheLineSize() )  std::atomic<xuptr>                        m_iHead             { 1 };
    alignas( x_target::getCacheLineSize() )  std::atomic<xuptr>                        m_iTail             { 0 };
    alignas( x_target::getCacheLineSize() )  xarray<x_atomic<t_entry*>,T_COUNT>        m_pArray            {{ nullptr }};
    x_debug_linear_quantum                                                             m_Debug_LQ          {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpsc    - Multi-Producer(push), Single-Consumer(pop) queue
//      node    - Requires a x_tagptr as the base class of the entry
//      queue   - FIFO ( First In first out )
//
//      Queue is ABA resistant thanks for the tagptr. However the user should be carefull
//      when it frees the memory from the node as the node *may* still be in used.
//      Other interesting queues:
//      * https://kjellkod.wordpress.com/2012/11/28/c-debt-paid-in-full-wait-free-lock-free-queue/
//
// Algorithm:
//      Original algorithm by: Dominique Fober, Yann Orlarey, St�ephane Letz
//      This version was inspire by: Randall Tunner
//      Basically a 2005 Fober implementation.
//      Detail of the implementation (Optimized Lock-Free FIFO Queue continued) linked here:
//      http://www.grame.fr/ressources/publications/TR-050523.pdf
//------------------------------------------------------------------------------
template< class T_ENTRY >
class x_ll_mpsc_node_queue
{
    x_object_type( x_ll_mpsc_node_queue, is_quantum_lock_free, is_not_copyable );

public:

    inline x_ll_mpsc_node_queue  ( void ) noexcept 
    {
        m_Tail.setup( getDummyNode() );
        m_pHead = m_Tail.getLocal().getPtr();
        getDummyNode()->setupNull();
    }
    
    inline void push ( T_ENTRY& Node )                        noexcept { push(Node,Node); }
    inline void push ( T_ENTRY& NodeStart, T_ENTRY& NodeEnd ) noexcept
    {
        // Lets make sure that our end node is pointing to NULL
        NodeEnd.setupNull();
        
        do
        {
            local           Tail                { m_Tail.getLocal( std::memory_order_acquire )        };
            t_entry*        qtLastNodeNext      { Tail.getPtr()                                       };
            local           Next                { qtLastNodeNext->getLocal(std::memory_order_acquire) };
            
            // if our local reality does not longer match with the
            // quantum version of the tail then we can't assume we are at the last node.
            // Someone must have inserted something new. Go lets re-grab things again.
            if( Tail != m_Tail.getLocal( std::memory_order_acquire ) )
                continue;
            
            // If the next pointer is not NULL then we are not at the last node
            // move the tail forward.
            if( Next.isValid() )
            {
                // here, pNext is likely to be pTail->pNext
                m_Tail.CompareAndSet( Tail, Next, std::memory_order_relaxed );
                continue;
            }
            
            // If the tail is pointing to the last node then
            // append the new node.
            if( qtLastNodeNext->CompareAndSet( Next, &NodeStart, std::memory_order_release, std::memory_order_relaxed ) )
            {
                X_CMD_DEBUG( if( &NodeStart != getDummyNode() ) m_Debug_PushVsPop++ );
                // If the tail points to what we thought was the last node
                // then update the tail to point to the new node.
                m_Tail.CompareAndSet( Tail, &NodeEnd, std::memory_order_release, std::memory_order_relaxed );
                break;
            }
            
        } while( true );
    }
    
    inline T_ENTRY* pop ( void ) noexcept
    {
        x_assert_linear( m_Debug_LQ );
        
        local Tail = m_Tail.getLocal( std::memory_order_relaxed);
        do
        {
            t_entry* const pHead = m_pHead;
            t_entry* const pNext = pHead->getLocal( std::memory_order_relaxed ).getPtr();
            
            // Check if the queue is empty.
            if( pHead == Tail.getPtr() )
            {
                if( pNext == nullptr ) return nullptr;
                
                // Special case cause by the tail been equal to the head.
                // Help move the m_Tail forward because it seems to have falling behind.
                m_Tail.CompareAndSet( Tail, pNext, std::memory_order_relaxed );
            }
            else if( pNext )
            {
                m_pHead = pNext;
                if( pHead == getDummyNode() )
                {
                    push( *getDummyNode() );
                    return pop();
                }

                X_CMD_DEBUG( m_Debug_PushVsPop-- );
                return reinterpret_cast<T_ENTRY*>(pHead);
            }
            else
            {
                Tail = m_Tail.getLocal( std::memory_order_relaxed );
            }
            
        } while(1);
    }
    
    T_ENTRY*                  steal               ( void )    { return pop(); }
    
protected:
    
    enum : s32      {   OFFSET_PTR  = offsetof(T_ENTRY, m_qtPointer)      };
    using               ptr         = typename T_ENTRY::t_tagptr_base;
    using               t_entry     = typename ptr::t_entry;
    using               local       = typename ptr::local;
    
protected:
    
    constexpr T_ENTRY* getDummyNode( void ) const noexcept { return reinterpret_cast<T_ENTRY*>( const_cast<xbyte*>(&reinterpret_cast<const xbyte*>(&m_DummyNode)[-OFFSET_PTR]) ); }
    
protected:
    
    alignas( x_target::getCacheLineSize() )  t_entry*               m_pHead         { nullptr };
    alignas( x_target::getCacheLineSize() )  ptr                    m_Tail          {};
    alignas( x_target::getCacheLineSize() )  ptr                    m_DummyNode     {};
    x_debug_linear_quantum                                          m_Debug_LQ      {};
    X_CMD_DEBUG( x_atomic<s64>                                        m_Debug_PushVsPop{0} );
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpmc    - Multi-Producer(push), Multi-Consumer(pop) queue
//      node    - Requires a x_tagptr as the base class of the entry
//      queue   - FIFO ( First In first out )
//
//      Queue is ABA resistant thanks for the tagptr. However the user should be carefull
//      when it frees the memory from the node as the node *may* still be in used.
//      Other interesting queues:
//      * https://kjellkod.wordpress.com/2012/11/28/c-debt-paid-in-full-wait-free-lock-free-queue/
//
// Algorithm:
//      Original algorithm by: Dominique Fober, Yann Orlarey, St�ephane Letz
//      This version was inspire by: Randall Tunner
//      Basically a 2005 Fober implementation.
//      Detail of the implementation (Optimized Lock-Free FIFO Queue continued) linked here:
//      http://www.grame.fr/ressources/publications/TR-050523.pdf
//-------------------------------------------------------------------------------
template< class T >
class x_ll_mpmc_node_queue 
{
    x_object_type( x_ll_mpmc_node_queue, is_quantum_lock_free, is_not_copyable );

public:
    
    inline x_ll_mpmc_node_queue( void ) noexcept
    {
        m_Tail.setup( getDummyNode() );
        m_Head.setup( m_Tail );
        getDummyNode()->setupNull();
    }
    
    inline void push    ( T& Node )                     noexcept { push( Node, Node ); }
    inline void push    ( T& NodeStart, T& NodeEnd )    noexcept 
    {
        // Lets make sure that our end node is pointing to NULL
        NodeEnd.setupNull();
        
        do
        {
            local		Tail                { m_Tail.getLocal( std::memory_order_acquire )       };
            ptr&        qtLastNodeNext =    *Tail.getPtr();
            local		Next                { qtLastNodeNext.getLocal(std::memory_order_acquire) };
            
            // if our local reality does not longer match with the
            // quantum version of the tail then we can't assume we are at the last node.
            // Someone must have inserted something new. Go lets re-grab things again.
            if( Tail != m_Tail.getLocal( std::memory_order_acquire ) )
                continue;
            
            // If the next pointer is not NULL then we are not at the last node
            // move the tail forward.
            if( Next.isValid() )
            {
                // here, pNext is likely to be pTail->pNext
                m_Tail.CompareAndSet( Tail, Next, std::memory_order_relaxed );
                continue;
            }
            
            // If the tail is pointing to the last node then
            // append the new node.
            if( qtLastNodeNext.CompareAndSet( Next, &NodeStart, std::memory_order_release, std::memory_order_relaxed ) )
            {
                // If the tail points to what we thought was the last node
                // then update the tail to point to the new node.
                m_Tail.CompareAndSet( Tail, &NodeEnd, std::memory_order_release, std::memory_order_relaxed );
                break;
            }
            
        } while( true );
    }
    
    inline T* pop( void ) noexcept
    {
        do
        {
            local Head        { m_Head.getLocal( std::memory_order_acquire ) };
            ptr*  pHead =     Head.getPtr();
            local Next        { pHead->getLocal( std::memory_order_acquire ) };
            
            // Verify that we did not get the pointers in the middle
            // of another update.
            if( Head != m_Head.getLocal( std::memory_order_acquire ) )
                continue;
            
            // Check if the queue is empty.
            if( Head == m_Tail.getLocal( std::memory_order_acquire ) )
            {
                if( Next.isNull() )	return NULL;
                
                // Special case cause by the tail been equal to the head.
                // Help move the m_Tail forward because it seems to have falling behind.
                m_Tail.CompareAndSet( Head, Next, std::memory_order_relaxed );
            }
            else if( Next.isValid() )
            {
                // Move the head pointer, effectively removing the node
                if( m_Head.CompareAndSet( Head, Next, std::memory_order_release, std::memory_order_relaxed ) )
                {
                    // Well so if it turns out that we just got the dummy node.
                    // Re-insert it again and get a new one
                    if( pHead == getDummyNode() )
                    {
                        push( *getDummyNode() );
                        return pop();
                    }
                    
                    assert( Head.getPtr() != getDummyNode() );
                    assert( m_Tail.getLocal(std::memory_order_acquire).getPtr() != pHead );
                    
                    return reinterpret_cast<T*>(pHead);
                }
            }
            
        } while( true );
    }

    inline T* steal ( void ) noexcept { return pop(); }
    
protected:
    
    enum : xuptr   {   OFFSET_PTR  = offsetof(T, m_qtPointer) };
    using               ptr         = x_ll_tagptr<typename T::t_base>;
    using               local       = typename ptr::local;
    
protected:
    
    constexpr T*    getDummyNode( void ) const noexcept { return reinterpret_cast<T*>( &reinterpret_cast<xbyte*>(&m_DummyNode)[-OFFSET_PTR] ); }
    
protected:
    
    alignas( x_target::getCacheLineSize() )  ptr    m_Head          {};
    alignas( x_target::getCacheLineSize() )  ptr    m_Tail          {};
    alignas( x_target::getCacheLineSize() )  ptr    m_DummyNode     {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpsc    - Multi-Producer(push), Single-Consumer(pop) queue
//      node    - Requires its own node
//      queue   - FIFO ( First In first out )
//      process - means that together with the push you can pass a callback that will
//                keep poping messages until the queue is free.
//
//      Queue is ABA resistant.
//
// Algorithm:
//      http://www.1024cores.net/home/lock-free-algorithms/queues/intrusive-mpsc-node-based-queue
//------------------------------------------------------------------------------
class x_ll_mpsc_process_node_queue
{
public:

    struct node
    {
        x_atomic<node*> m_Next;
    };

public:

    //--------------------------------------------------------------------------------------

    x_ll_mpsc_process_node_queue( void )
    {
        m_Head.store( tagptr{ &m_DummyNode, {false} }, std::memory_order_relaxed );
        m_pTail = &m_DummyNode;
        m_DummyNode.m_Next.store( nullptr, std::memory_order_relaxed );
    }

    //--------------------------------------------------------------------------------------
    template< typename T >
    void push( node& Node, T&& ProcessCallback )
    {
        static_assert( xfunction_is_lambda_signature_same< T, void(void) >::value, "The lambda should be void(void) type it is not " );

        Node.m_Next.store( nullptr, std::memory_order_relaxed );

        auto localHead = m_Head.load( std::memory_order_relaxed );
        do
        {
            tagptr New{ &Node, {true} }; 
            if( m_Head.compare_exchange_weak( localHead, New, std::memory_order_acquire ) )
            {
                localHead.getPtr()->m_Next.store( &Node, std::memory_order_relaxed );

                if( localHead.m_Tag.m_Processing == false ) ProcessCallback();
                break;
            }
    
        } while( true );
    }

    //--------------------------------------------------------------------------------------

    bool TryLock( void )
    {
        auto localHead = m_Head.load( std::memory_order_relaxed );
        
        while( localHead.m_Tag.m_Processing == false ) 
        {
            auto NewHead = localHead;
            NewHead.m_Tag.m_Processing = true;
            if( m_Head.compare_exchange_weak( localHead, NewHead, std::memory_order_relaxed ) )
            {
                x_assert( NewHead.m_Tag.m_Processing );
                return true;
            }
        }

        return false;
    }

    //--------------------------------------------------------------------------------------
    
    void unlock( void )
    {
        auto localHead = m_Head.load( std::memory_order_relaxed );
        
        do  
        {
            x_assert( localHead.m_Tag.m_Processing );

            auto NewHead = localHead;
            NewHead.m_Tag.m_Processing = false;
            if( m_Head.compare_exchange_weak( localHead, NewHead, std::memory_order_relaxed ) )
                return;

        } while( true );
    }

    //--------------------------------------------------------------------------------------
    // this should be used inside the callback!
    node* PopProcessNode( void )
    {
        x_assert( m_Head.load( std::memory_order_relaxed ).m_Tag.m_Processing );

        node* pPop;
        while( (pPop = pop()) == nullptr )
        {
            // Turn off processing if we need to
            auto localHead = m_Head.load( std::memory_order_relaxed );
            x_assert( localHead.m_Tag.m_Processing );

            auto NewHead = localHead;
            NewHead.m_Tag.m_Processing = false;
            if( m_Head.compare_exchange_weak( localHead, NewHead, std::memory_order_relaxed ) )
                return nullptr;
        }

        return pPop;
    }

protected:

    struct tag
    {
        u16    m_Processing;
    };

    using tagptr = xtagptr<node, tag>;

protected:

    //--------------------------------------------------------------------------------------

    void PushDummy( void )
    {
        m_DummyNode.m_Next.store( nullptr, std::memory_order_relaxed );

        auto localHead = m_Head.load( std::memory_order_relaxed );
        do
        {
            tagptr New{ &m_DummyNode, {true} }; 
            if( m_Head.compare_exchange_weak( localHead, New, std::memory_order_acquire ) )
            {
                localHead.getPtr()->m_Next.store( &m_DummyNode, std::memory_order_relaxed );
                break;
            }
    
        } while( true );
    }

    //--------------------------------------------------------------------------------------

    node* pop( void )
    {
        auto tail = m_pTail;
        auto next = tail->m_Next.load( std::memory_order_relaxed );

        if( tail == &m_DummyNode )
        {
            // is Dummy pointing to null?
            if( nullptr == next )
                return nullptr;

            // Skip over dummy node
            m_pTail = next;
            tail = next;
            next = tail->m_Next.load( std::memory_order_relaxed );
        }

        // Pop a node if we are in the middle of the chain
        if( next )
        {
            m_pTail = next;
            return tail;
        }

        auto head = m_Head.load( std::memory_order_relaxed );
        // This case deals with the fact that the Push has not yet set the next pointer.
        // from the point of view of the pop we are done since the push is not completed yet.
        if( tail != head.getPtr() )
            return nullptr;

        // Head and tail we must make sure we insert the dummy node again
        PushDummy();
        
        // After inserting the dummy node we must refresh the next pointer
        next = tail->m_Next.load( std::memory_order_relaxed );

        // check if we now have a node
        if( next )
        {
            m_pTail = next;
            return tail;
        }

        return nullptr;
    } 

protected:

    x_atomic<tagptr>    m_Head;
    node*               m_pTail;
    node                m_DummyNode;
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpsc    - Multi-Producer(push), Single-Consumer(pop) queue
//      node    - Requires its own node
//      queue   - FIFO ( First In first out )
//
//      Queue is ABA resistant.
//
// Algorithm:
//      http://www.1024cores.net/home/lock-free-algorithms/queues/intrusive-mpsc-node-based-queue
//------------------------------------------------------------------------------
template< typename T >
class x_ll_mpsc_node_queue_v2
{
public:

    using node = x_ll_mpsc_process_node_queue::node;
    static_assert( std::is_base_of<node,T>::value, "Your type must be derived from a x_ll_mpsc_process_node_queue::node" );

public:

    //--------------------------------------------------------------------------------------

    x_ll_mpsc_node_queue_v2( void )
    {
        m_Head.store( reinterpret_cast<T*>( &m_DummyNode ), std::memory_order_relaxed );
        m_pTail = reinterpret_cast<T*>( &m_DummyNode );
        m_DummyNode.m_Next.store( nullptr, std::memory_order_relaxed );
    }

    //--------------------------------------------------------------------------------------

    void push( T& Node )
    {
        Node.m_Next.store( nullptr );

        auto localHead = m_Head.load( std::memory_order_relaxed );
        do
        {
            if( m_Head.compare_exchange_weak( localHead, &Node ) )
            {
                localHead->m_Next.store( &Node, std::memory_order_relaxed );
                break;
            }
    
        } while( true );
    }

    //--------------------------------------------------------------------------------------

    T* pop( void )
    {
        auto tail = m_pTail;
        auto next = reinterpret_cast<T*>( tail->m_Next.load( std::memory_order_relaxed ) );

        if( tail == &m_DummyNode )
        {
            // is Dummy pointing to null?
            if( nullptr == next )
                return nullptr;

            // Skip over dummy node
            m_pTail = next;
            tail = next;
            next = reinterpret_cast<T*>( tail->m_Next.load( std::memory_order_relaxed ) );
        }

        // Pop a node if we are in the middle of the chain
        if( next )
        {
            m_pTail = next;
            return tail;
        }

        auto head = m_Head.load( std::memory_order_relaxed );
        // This case deals with the fact that the Push has not yet set the next pointer.
        // from the point of view of the pop we are done since the push is not completed yet.
        if( tail != head )
            return nullptr;

        // Head and tail we must make sure we insert the dummy node again
        push( *reinterpret_cast<T*>(&m_DummyNode) );
        
        // After inserting the dummy node we must refresh the next pointer
        next = reinterpret_cast<T*>( tail->m_Next.load( std::memory_order_relaxed ) );

        // check if we now have a node
        if( next )
        {
            m_pTail = next;
            return tail;
        }

        return nullptr;
    } 

protected:

    x_atomic<T*>        m_Head;
    T*                  m_pTail;
    node                m_DummyNode;
};
