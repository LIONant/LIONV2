//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Basic functionality 
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test { namespace file 
{

    //-----------------------------------------------------------------------------------------
    
    void syncModeTest( xwstring SYNC_MODE_FILE_NAME_PACH, bool bCloseFile )
    {
        xfile   file;

        //
        // Write file
        //
        file.Open( SYNC_MODE_FILE_NAME_PACH, "w" );
    
        const xstring header{ X_STR("TestFileHeader") };
        file.WriteString(header);
    
        auto    FileSize = file.Tell();
        auto    RealSize = units_bytes{ header.getLength() } + 1_bytes;
        x_assert( FileSize == RealSize.m_Value );
    
        xarray< s32, 3245> buffer {};
        for ( s32 i = 0; i < buffer.getCount(); i++ )
        {
            buffer[i] = i;
        }
    
        file.WriteList( buffer );
    
        FileSize  = file.Tell();
        RealSize += buffer.getCount()*sizeof(s32);
        x_assert( FileSize == RealSize.m_Value );
    
        file.Write( buffer.getCount() );
    
        FileSize  = file.Tell();
        RealSize += sizeof(decltype(buffer.getCount()) );
        x_assert( FileSize == RealSize.m_Value );
    
        // Done
        if( bCloseFile ) file.Close();
    
        //
        // Clear the buffer
        //
        for( s32 i = 0; i < buffer.getCount(); i++ )
        {
            buffer[i] = 0;
        }
    
        //
        // Read file
        //
        if( bCloseFile ) file.Open( SYNC_MODE_FILE_NAME_PACH, "r" );
        else             file.SeekOrigin(0);
    
        FileSize  = file.getFileLength();
        RealSize  = units_bytes{ header.getLength() } + 1_bytes;
        RealSize += sizeof(s32)*buffer.getCount() + sizeof(decltype(buffer.getCount()));
        x_assert( FileSize == RealSize.m_Value );
    
        // Read the first string
        xstring NewHeader;
        file.ReadString(NewHeader);
        x_assert( NewHeader == header );
    
        // Read the buffer size
        auto position = file.Tell();
        file.SeekCurrent( sizeof(s32)*buffer.getCount() );
    
        s32 BufferCount;
        file.Read( BufferCount );
        x_assert( BufferCount == buffer.getCount() );
    
        // Read the buffer
        file.SeekOrigin( position );
        file.ReadList( buffer );
    
        for( s32 i = 0; i < buffer.getCount(); i++ )
        {
            x_assert(buffer[i] == i);
        }
    
        // Done
        file.Close();
    }

    //-----------------------------------------------------------------------------------------
    
    void asyncModeTest( xwstring ASYNC_MODE_FILE_NAME_PACH, bool bCloseFile  )
    {
        x_constexprvar int      Steps       = 10;
        x_constexprvar int      DataSize    = 1024 * Steps;
        xarray<xafptr<s32>,2>   Buffer;
        xfile                   file;

        Buffer[0].Alloc( DataSize );
        Buffer[1].Alloc( DataSize );
    
        for( s32 t=0; t<10; t++ )
        {
            //
            // Write something
            //
            if( 1 )
            {
                //
                // Write Something
                //
                auto result = file.Open( ASYNC_MODE_FILE_NAME_PACH, "w@" );
                x_assert( !result );
            
                //
                // Safe some random data in 10 steps
                //
                int k=0;
                for( s32 i=0; i<Steps; i++ )
                {
                    // this is overlap/running in parallel with actual writing the file
                    for ( auto& E : Buffer[i&1] )
                    {
                        E = k++;
                    }
            
                    file.Synchronize( true );
                    result = file.WriteList( Buffer[i&1] );
                    x_assert( !result );
                }
            
                file.Synchronize( true );
                if( bCloseFile ) file.Close();
            }
        
            //
            // Clear buffer
            //
            Buffer[0].MemSet( 0 );
            Buffer[1].MemSet( 0 );
        
            //
            // Read something
            //
            if( 1 )
            {
                if( bCloseFile )
                {
                    auto result = file.Open(ASYNC_MODE_FILE_NAME_PACH, "r@");
                    x_assert( !result );
                }
                else
                {
                    file.SeekOrigin(0);
                }
                int     k    = 0;

                // Read the first entry
                auto result = file.ReadList( Buffer[0] );
                x_assert( !result );

                // Start reading 
                for( s32 i=1; i<Steps; i++ )
                {
                    result = file.ReadList( Buffer[i&1] );
                    x_assert( !result );

                    // this is overlap/running in parallel with actual reading of the file
                    for ( auto& E : Buffer[(i^1)&1] )
                    {
                        x_assert( E == k++ );
                    }
                    file.Synchronize( true );
                }

                // Check the last thing we read            
                for ( auto& E : Buffer[(Steps^1)&1] )
                {
                    x_assert( E == k++ );
                }

                // We are done!
                file.Close();
            }
        }
    }    

    //-----------------------------------------------------------------------------------------

    void Test( void )
    {
        syncModeTest( X_WSTR("temp:/test.dat" ),            true );
        asyncModeTest( X_WSTR("temp:/asyncMode.dat"),       true );

        syncModeTest( X_WSTR("ram:/test.dat" ),             false );
        asyncModeTest( X_WSTR("ram:/asyncMode.dat"),        false );
    }
}}