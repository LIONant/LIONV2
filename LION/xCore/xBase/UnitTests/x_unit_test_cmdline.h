//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace x_unit_test { namespace cmdline 
{
    //-----------------------------------------------------------------------------------------

    void BasicTest( void )
    {
        xcmdline        CmdLine;
        const char*     pString = "-FileName SomeFile.txt -Res 640 480";
    
        // Add the command switches
        CmdLine.AddCmdSwitch( X_STR_CRCSTR("FileName"), 1, 1, 1, 1, false, xcmdline::type::STRING );
        CmdLine.AddCmdSwitch( X_STR_CRCSTR("Res"), 2, 2, 1, 1, false, xcmdline::type::INT );
    
        // Parse the command line
        CmdLine.Parse( pString );
        if( CmdLine.DoesUserNeedsHelp() )
        {
            X_LOG( "-FileName <filename> -Res <xres> <yres> \n" );
            return;
        }
    
        // Handle commands
        for( s32 i=0; i<CmdLine.getCommandCount(); i++ )
        {
            const auto  Cmd     = CmdLine.getCommand(i);

            switch( Cmd.getCmdCRC() )
            {
                case x_constStrCRC32( "FileName" ):
                {
                    x_assert( Cmd.getArgumentCount() == 1 );
                    const xstring&  String = Cmd.getArgument( 0 );
                    x_assert( x_strcmp( "SomeFile.txt", &String[0] ) == 0 );
                    break;
                }
                case x_constStrCRC32( "Res" ):
                {
                    x_assert( Cmd.getArgumentCount() == 2 );
                    const s32 XRes   = x_atoi32<xchar>( Cmd.getArgument( 0 ) );
                    const s32 YRes   = x_atoi32<xchar>( Cmd.getArgument( 1 ) );
                    x_assert( XRes == 640 );
                    x_assert( YRes == 480 );
                    break;
                }
                default: 
                    x_assert( false );
            }
        }
    }

    //-----------------------------------------------------------------------------------------

    void Test( void )
    {
        BasicTest();
    }
}}
