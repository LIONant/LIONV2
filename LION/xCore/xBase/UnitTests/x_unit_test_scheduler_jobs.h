//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include <mutex>

namespace x_unit_test { namespace scheduler_jobs
{
    //-------------------------------------------------------------------------------------------
    // Random stress test
    void Test1( void )
    {
        auto RunningInstance = x_job_chain<float(int)>::Create( [&](int x)
        {
            x_assert(x==5);
            return 2.0f;
        })
        // We can start the job_chain even before we have finish defining it
        .RunNormal( 5 );
 
        // Finish defining the job_chain 
        auto& MyJobChain = RunningInstance.getTask().Then( [&]( float x )
        {
            x_assert(x==2.0f);
            return x_job_chain<int()>::Create( [&](void)
            {
                return 5.0f;
            })
            .Then( [](float i)
            {
                x_assert(i==5.0f);
                return 6.0f;
            }); 
        })
        .Then( [&]( x_job_chain<int(),float> Task ) 
        {
            int x = Task.Then( [](float i)
            {
                x_assert(i==6.0f);
                return 6;
            }).get();

            x_assert( x == 6 );
            return 2.0f;
        });

        // Note that we run linearly this one while the running instance still working
        auto res1 = MyJobChain.get( 5 );
        x_assert( res1 == 2.0f );

        // Ok now is time to get the second one
        auto res2 = RunningInstance.get();
        x_assert( res2 == 2.0f );
    }

    //-------------------------------------------------------------------------------------------
    // This example shows a linear copy. It does not really utilize the multi-core rather is just
    // a chain of events happening. This is not a fast way to do things.
    void Test2( void )
    {
        struct copyfile
        {
            using temp_owner = xafptr<xbyte>;

            static auto readFileChunk( int& file, int chunkLength )
            {
                return x_job_chain<bool(void)>::Create( 
                    [&file, chunkLength]( void ) -> temp_owner
                    {
                        temp_owner buffer;
                        buffer.Alloc( chunkLength );

                        // Pretend we are reading something
                        xuptr c = chunkLength;
                        if( (file + c) > 10024 ) c = 10024 - file;
                        for( int i=0; i<c; i++ ) buffer[i] = u8(i);
                        
                        file += int(c);
                        return temp_owner{ buffer.TransferOwnership(), c };
                    });
            }

            static auto writeFileChunk( int& file, x_job_chain<bool(void), temp_owner> JobChain )
            {
                return JobChain.Then( 
                [&file]( temp_owner chunk  )
                {
                    // pretend we are writing something
                    for( int i=0; i<chunk.getCount(); i++ ) 
                        x_assert( chunk[i] == u8(i) );

                    file += int(chunk.getCount());

                    return chunk.getCount() == 0;
                });
            }

            static auto FromFile ( const xwstring inFilePath )
            {
                return x_job_chain<void(const xwstring)>::Create( [inFilePath]( const xwstring outFilePath )
                {
                    int  inFile          = 0;
                    int  outFile         = 0;
                    auto ReadWriteChain  = writeFileChunk( outFile, readFileChunk ( inFile, 4096 ) );

                    // Write while reading
                    while( ReadWriteChain.get() );
                    x_assert( inFile == outFile );
                });
            };
        };

        xwstring fromFile = X_WSTR("Hellow1");
        xwstring toFile   = X_WSTR("Hellow2");

        copyfile::FromFile( fromFile ).Join( toFile );

        x_assume(true);
    }

    //-------------------------------------------------------------------------------------------
    // This is a proper multicore example. One way how you know it is because of the use of RUN
    // In this example we are always writting while reading. Mind you there is an allocation per 
    // iteration and that is not good but other wise is fine.
    void Test3( void )
    {
        struct copyfile
        {
            using temp_owner = xafptr<xbyte>;

            static auto readFileChunk( int& file, int chunkLength )
            {
                return x_job_chain<temp_owner(void)>::Create( 
                    [&file, chunkLength]( void ) -> temp_owner
                    {
                        temp_owner buffer;
                        buffer.Alloc( chunkLength );

                        // Pretend we are reading something
                        xuptr c = chunkLength;
                        if( (file + c) > 10024 ) c = 10024 - file;
                        for( int i=0; i<c; i++ ) buffer[i] = u8(i);
                        
                        file += int(c);
                        return { buffer.TransferOwnership(), c };
                    });
            }

            static auto writeFileChunk( int& file )
            {
                return x_job_chain<bool( temp_owner chunk )>::Create( 
                [&file]( temp_owner chunk  )
                {
                    // pretend we are writing something
                    for( int i=0; i<chunk.getCount(); i++ ) 
                        x_assert( chunk[i] == u8(i) );

                    file += int(chunk.getCount());

                    return chunk.getCount() == 0;
                });
            }

            static auto FromFile ( const xwstring inFilePath ) 
            {
                return x_job_chain<void(const xwstring)>::Create( [inFilePath]( const xwstring outFilePath )
                {
                    int  inFile      = 0;
                    int  outFile     = 0;
                    auto ReadChain   = readFileChunk ( inFile, 4096 );
                    auto WriteChain  = writeFileChunk( outFile );

                    // Read the first buffer
                    temp_owner s = ReadChain.get();
                    if ( s.getCount() == 0 ) return;

                    // Write while reading
                    do 
                    {
                        auto WriteExec = WriteChain.RunNormal( s );
                        s = ReadChain.get();
                        WriteExec.get();
                    } while( s.getCount() );

                    x_assert( inFile == outFile );
                });
            };
        };

        xwstring fromFile = X_WSTR("Hellow1");
        xwstring toFile   = X_WSTR("Hellow2");

        copyfile::FromFile( fromFile ).Join( toFile );

        x_assume(true);
    }

    //-------------------------------------------------------------------------------------------

    std::tuple<std::string , std::string, int> findPerson() 
    {
        return std::make_tuple("Joe", "Sixpack", 42);
    }

    void Test( void )
    {
        std::string first_name, last_name;
        int age;
        std::tie(first_name, last_name, age) = findPerson();
   
        Test1();
        Test2();
        Test3();
    }
}}