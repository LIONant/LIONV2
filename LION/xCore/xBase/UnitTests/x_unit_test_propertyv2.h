//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Basic functionality 
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test{ namespace propertyv2
{
    //---------------------------------------------------------------------------------------

    struct TestChild : xproperty_v2::base
    {
        X_DEFBITS( some_flags, u8, 0x0u,
            X_DEFBITS_ARG(bool, A, 1 )
            , X_DEFBITS_ARG(bool, B, 1)
            , X_DEFBITS_ARG(bool, C, 1)
            )

        float       m_x     = -100.001f;
        bool        m_Bool  = true;
        xstring     m_Name;
        some_flags  m_Flags;

        virtual const xproperty_v2::table& onPropertyTable( void ) const override
        {
            static const xproperty_v2::table E( this, X_STR("TestChild"), [&](xproperty_v2::table& E)
            {
                E.AddProperty( X_STR("x"),      m_x,    X_STR("Simple Variable"), xproperty_v2::info::f32{ 0, 100 }, xproperty_v2::flags::MASK_DONT_SAVE | xproperty_v2::flags::MASK_NOT_VISIBLE );
                E.AddProperty( X_STR("bool"),   m_Bool, X_STR("Simple Variable") );
                E.AddProperty( X_STR("String"), m_Name, X_STR("Simple Variable") );
                E.AddProperty( X_STR("Flags"),  m_Flags, X_STR("Simple Variable"), xproperty_v2::info::boolean{ some_flags::OFFSET_A } );
            });

            return E;
        }
    };

    //---------------------------------------------------------------------------------------

    struct Test01 : TestChild
    {
        struct pepe
        {
            int k;
        };
    
        struct some
        {
            int a;
            int b;
            pepe Pepe[4];
        };
    
        short   m_a;
        int     m_b;
        char    m_c;
        int     m_d;
        long    m_t;
        int     m_s;
    
        int     m_myarray_Count;
        int     m_array[3];
        Test01*   m_pSomething = nullptr;
        int     m_C_Count;
        some    m_array2[3];
    
        virtual const xproperty_v2::table& onPropertyTable( void ) const override
        {
            // "This is a test",
            static const xproperty_v2::table E( this, X_STR("MainScope"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable(TestChild::onPropertyTable());

                E.AddProperty(X_STR("a"), m_a, X_STR("Simple Variable"));
                E.AddProperty(X_STR("b"), m_b, X_STR("Simple Variable"));
                E.AddProperty<int>(X_STR("c"), X_STR("Simple Variable"),
                    X_STATIC_LAMBDA((Test01& A)->bool
                {
                    return A.m_c != 5;
                }),
                    X_STATIC_LAMBDA_BEGIN (Test01& Class, int& Data, const xproperty_v2::info::s32& Details, bool isSend)
                {
                    if (isSend) Data = Class.m_c;
                    else         Class.m_c = Data;
                }
                X_STATIC_LAMBDA_END
                    );

                E.AddDynamicScope(
                    { nullptr }
                    , X_STATIC_LAMBDA_BEGIN (Test01& A) -> xproperty_v2::base*
                    {
                        return A.m_pSomething;
                    }
                    X_STATIC_LAMBDA_END
                );
             
                E.AddDynamicList( X_STR("C")
                    // This lambda is used to iterate through all the items. get return the id for each item, and next is used to keep state for out iterator
                    , X_STATIC_LAMBDA( ( Test01& A, void*& pNext ) -> u64
                    {
                        // if this was a vector (xvector m_Array;) we will do this:
                        //      int iArray = (pNext==nullptr) ? 0 : 1 + int((array_type*)pNext - &A.m_Array[0]);
                        //      if( iArray >= A.count ) return -1; 
                        //      pNext      = &A.m_Array[iArray];
                        //      return iArray;
                        // if this was a link list(node* m_pHead)  we will do something like this.
                        //      pNext = (pNext == nullptr) ? A.m_pHead : ( ((node*)pNext)->m_pNext );
                        //      if( pNext == nullptr ) return -1;
                        //      return pNext->m_id;
                        // if this was a map (std::map m_Map):
                        //      if( pNext == nullptr )
                        //      {
                        //          if( A.m_Map.size == 0) return -1; 
                        //          else                   pNext = new std::map::iterator( A.m_Map.begin() );
                        //      }
                        //      else 
                        //      { 
                        //          (*((std::map::iterator*)pNext))++; 
                        //          if( pNext == A.m_Map.end() ) { delete pNext; return -1; }
                        //      }
                        //      return (*((std::map::iterator*)pNext))->first;

                        if( pNext==nullptr )
                        {
                            // for this example we just put some fake data into next so that next time we know to stop
                            pNext = &A;
                            // we always have one entry of id 101
                            return 101;
                        }
                        return ~0ul;
                    })
                    ,
                    X_STATIC_LAMBDA_BEGIN ( Test01& A, u64 id, bool isSend ) -> xproperty_v2::base*
                    {
                        // get the entry 101
                        x_assert(id == 101);
                        return A.m_pSomething;
                    }
                    X_STATIC_LAMBDA_END
                    ,
                    X_STATIC_LAMBDA_BEGIN ( Test01& A, int& Count, bool isSend )
                    {
                        // this lambda is optional. Depends if the user wants to keep track of the count
                        if(isSend) Count = A.m_C_Count;
                        else       A.m_C_Count = Count;
                    }
                    X_STATIC_LAMBDA_END
                );

                // handles atomic arrays such ints...
                E.AddArray( X_STR("MyArray"), &m_array[0], 3, 
                    X_STR("Simple Variable"),
                    X_STATIC_LAMBDA_BEGIN ( Test01& A, int& Count, bool isSend )
                    {
                        if(isSend) Count = A.m_myarray_Count ;
                        else       A.m_myarray_Count = Count;
                    }
                    X_STATIC_LAMBDA_END
                );

                // handle array of structures
                E.AddArray( X_STR("MyArray2"), &m_array2[0], 3, [&](xproperty_v2::table& E )
                {
                    E.AddProperty( X_STR("a"), m_array2[0].a, X_STR("Simple Variable") );
                    E.AddProperty( X_STR("b"), m_array2[0].b, X_STR("Simple Variable") );
                    E.AddArray( X_STR("Pepe"), m_array2[0].Pepe, 4, [&]( xproperty_v2::table& E )
                    {
                        E.AddProperty( X_STR("k"), m_array2[0].Pepe[0].k, X_STR("Simple Variable") );
                    });
                });
            
                // Create random scopes because we can            
                E.AddScope( X_STR("Second"), [&]( xproperty_v2::table& E )
                {
                    E.AddProperty( X_STR("t"), m_t, X_STR("Simple Variable") );
                                         
                    E.AddScope( X_STR("Wow"), [&]( xproperty_v2::table& E )
                    {
                        E.AddProperty( X_STR("s"), m_s, X_STR("Simple Variable") );
                    });
                });
                E.AddProperty( X_STR("d"), m_d, X_STR("Simple Variable") );
            });
        
            return E;
        }
    };

    //---------------------------------------------------------------------------------------

    void Test( void )
    {
        Test01 a1,b1;
        Test01 a2,b2;
        bool   isCorrect = false;
        a1.m_pSomething = &b1;
        a2.m_pSomething = &b2;
    
        // setting the value inside an atomic array of ints
        a1.SetProperty( "MainScope/TestChild/String", xstring::Make("Hello world") );
        isCorrect = a1.GetProperty<xstring>( "MainScope/TestChild/String" ).second == X_STR("Hello world");
        x_assert(isCorrect);
        
        // setting the value inside an atomic array of ints
        a1.SetProperty("MainScope/TestChild/Flags", true );
        isCorrect = a1.GetProperty<bool>("MainScope/TestChild/Flags").second == true;
        x_assert(isCorrect);

        // setting the value inside an atomic array of ints
        a1.SetProperty( "MainScope/TestChild/x", 22.222f );
        isCorrect = a1.GetProperty<float>( "MainScope/TestChild/x" ).second == 22.222f;
        x_assert(isCorrect);

        // setting the value inside an atomic array of ints
        a1.SetProperty( "MainScope/TestChild/bool", false );
        isCorrect = a1.GetProperty< bool>( "MainScope/TestChild/bool" ).second == false;
        x_assert(isCorrect);

        // setting the value inside an atomic array of ints
        a1.SetProperty( "MainScope/MyArray[1]", 33);
        x_assert( a1.GetProperty<int>( "MainScope/MyArray[1]" ).second == 33 );

        // This is used to set the count or to get the count
        a1.SetProperty( "MainScope/C[]", 100 );
        x_assert( a1.GetProperty<int>( "MainScope/C[]" ).second == 100 );

        // This is accessing the first element of this dynamic list
        a1.SetProperty( "MainScope/C[101]/MainScope/c", 123 );
        x_assert( a1.GetProperty<int>( "MainScope/C[101]/MainScope/c" ).second == 123 );
    
        // Settting an interger value inside a dynamic scope
        a1.SetProperty( "MainScope/MainScope/a", 333);
        x_assert( a1.GetProperty<int>( "MainScope/MainScope/a" ).second == 333 );

        // Setting the count of an static array
        a1.SetProperty( "MainScope/MyArray[]", 2 );
        x_assert( a1.GetProperty<int>( "MainScope/MyArray[]" ).second == 2);

        // Setting the value inside an array of structures
        a1.SetProperty( "MainScope/MyArray2[1]/a", 33);
        x_assert( a1.GetProperty<int>( "MainScope/MyArray2[1]/a" ).second == 33 );

        a1.SetProperty( "MainScope/MyArray2[1]/Pepe[2]/k", 303);
        x_assert( a1.GetProperty<int>( "MainScope/MyArray2[1]/Pepe[2]/k" ).second == 303 );
    
        // Setting more intergers
        a1.SetProperty( "MainScope/b", 33);
        x_assert( a1.GetProperty<int>( "MainScope/b" ).second == 33 );

        a1.SetProperty( "MainScope/a", 1);
        x_assert( a1.GetProperty<int>( "MainScope/a" ).second == 1 );

        a1.SetProperty( "MainScope/c", 100);
        x_assert( a1.GetProperty<int>( "MainScope/c" ).second == 100 );
    
        // Setting inside local scopes
        a1.SetProperty( "MainScope/Second/Wow/s", 500);
        x_assert( a1.GetProperty<int>( "MainScope/Second/Wow/s" ).second == 500 );

        a1.SetProperty( "MainScope/Second/t", 600);
        x_assert( a1.GetProperty<int>( "MainScope/Second/t" ).second == 600 );

        // Enumerate all properties
        xvector<xproperty_v2::entry> List1;
        xvector<xproperty_v2::entry> List2;
        a1.EnumProperty( [&]() -> xproperty_v2::entry&
        {
            return List1.append();
        }, true );

        // Setting properties
        a2.SetProperties( [&]( const int Index ) -> const xproperty_v2::entry*
        {
            return Index < List1.getCount<int>() ? &List1[Index] : nullptr;
        });

        // Okay collect all properties
        a2.EnumProperty( [&]() -> xproperty_v2::entry&
        {
            return List2.append();
        }, true );

        // Check properties
        x_assert( List1.getCount() == List2.getCount() );

        for( int i=0; i<List1.getCount(); ++i )
        {
            x_assert( List1[i] == List2[i] );
        }

        /*
        // Getting properties
        a1.GetProperties( &a1, [&]( const int Index ) -> xproperty_v2::entry*
        {
            return Index < List1.getCount<int>() ? &List1[Index] : nullptr;
        });
        */

        std::cout << "Hello World!";
    }
}}

