//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

// Based on:
// http://www.gamedev.net/page/resources/_/technical/general-programming/using-varadic-templates-for-a-signals-and-slots-implementation-in-c-r3782
//
// Usage Example:
//      #include <iostream>
//      class body
//      {
//      public:
//           void UpdateDelegates( void ) {  m_MyEvent.Notify(22, 0.123f); }    // Call all delegates for this event type
//           x_message::event<int,float> m_MyEvent {};                          // Define an event type that we will emitted from this class 
//      };
//      
//      class other
//      {
//      public: 
//                  other           ( body& Body )      { m_MyEventDelegate.Connect( Body.m_MyEvent ); }
//      protected:
//          void    msgHandleEvent  ( int a, float b )  { std::cout << a << " " << b; }                     // Handle my messages here
//          x_message::delegate<other,int,float> m_MyEventDelegate   { *this, &other::msgHandleEvent };     // Define the delegate
//      };
//      
//      int main( void )
//      {
//          body    TestBody;
//          other   TestOther{ TestBody };
//          
//          TestBody.UpdateDelegates();
//          
//          return 0;    
//      }

namespace x_message
{
    template< typename... T_ARGS > class event;

    //--------------------------------------------------------------------------------
    template< typename... T_ARGS > 
    class abstract_delegate 
    {
        x_object_type( abstract_delegate, is_linear )

    public:

        x_forceconst        bool       isConnected          ( void )                const   noexcept { return !!m_pEventType; }

    protected:

        using event_type = event<T_ARGS...>;

    protected:

        constexpr                       abstract_delegate   ( void )                        noexcept = default;
        x_forceconst                    abstract_delegate   ( event_type& EventType )       noexcept : m_pEventType{ &EventType } {}
        x_forceinline       void        Attach              ( event_type& S )               noexcept { x_assert( m_pEventType == nullptr  ); m_pEventType = &S;                                    }
        x_forceinline       void        Dettach             ( event_type& S )               noexcept { x_assert( m_pEventType == nullptr || m_pEventType == &S       ); m_pEventType = nullptr;  m_pNext = m_pPrev = nullptr; }
        virtual             void        Call                ( T_ARGS... Args )       const  noexcept = 0;

    protected:

        event_type*                     m_pEventType    { nullptr };
        abstract_delegate<T_ARGS...>*   m_pNext         { nullptr };
        abstract_delegate<T_ARGS...>*   m_pPrev         { nullptr };

    protected:

        friend class event<T_ARGS...>;
    };
    
    //--------------------------------------------------------------------------------
    template< typename T_CLASS, typename... T_ARGS > 
    class delegate : public abstract_delegate<T_ARGS...>
    {
        x_object_type( delegate, is_linear, is_not_copyable, is_not_movable )

    public:

        x_forceconst                delegate            ( void(T_CLASS::*pFunction)( T_ARGS... ) )                                              noexcept : m_pFunction{ pFunction } {x_assert(m_pFunction);}
        x_forceconst                delegate            ( T_CLASS& Class, void(T_CLASS::*pFunction)( T_ARGS... ) )                              noexcept : m_pClass{ &Class }, m_pFunction{ pFunction } {x_assert(m_pFunction);}
        x_forceconst                delegate            ( T_CLASS& Class, void(T_CLASS::*pFunction)( T_ARGS... ), event<T_ARGS...>& EventType ) noexcept;
        x_forceinline              ~delegate            ( void )                                                                                noexcept { Disconnect(); }

        x_forceinline   void        Connect             ( T_CLASS& Class, event<T_ARGS...>& EventType )                                         noexcept { x_lk_guard(m_Lock); m_pClass = &Class; EventType.Connect( *this );                           }
        x_forceinline   void        Connect             ( event<T_ARGS...>& EventType )                                                         noexcept { x_lk_guard(m_Lock); EventType.Connect( *this );                           }
        x_forceinline   void        Disconnect          ( void )                                                                                noexcept { x_lk_guard(m_Lock); if( isConnected() ) t_parent::m_pEventType->Disconnect( *this ); }

    protected:

        using       callback    = void(T_CLASS::*)(T_ARGS...); 
        using       event_type  = event<T_ARGS...>;
        using       t_parent    = abstract_delegate<T_ARGS...>;
        using       t_self      = delegate;
        using       lock        = x_lk_spinlock::base<x_lk_spinlock::non_reentrance>;

    protected:

        virtual         void        Call                ( T_ARGS... Args )                                          const   noexcept override final { (m_pClass->*m_pFunction)(Args...); }

    protected:

        lock            m_Lock              {};
        T_CLASS*        m_pClass            { nullptr };
        callback        m_pFunction         { nullptr };

    protected:

        friend class event<T_ARGS...>;
    };

    //--------------------------------------------------------------------------------
    template< class... T_ARGS > 
    class event 
    {
        x_object_type( event, is_linear, is_not_copyable, is_not_movable )

    public:

        using base_delegate_concreate = abstract_delegate<T_ARGS...>;

    public:

                                        event               ( void )                                noexcept = default;
        x_forceinline                  ~event               ( void )                                noexcept { x_lk_guard(m_Lock); for( auto pDelegate = m_pHead; pDelegate; ) { auto p = pDelegate; pDelegate = p->m_pNext; p->Dettach(*this); } m_pHead = nullptr;        }
        x_forceinline       void        Notify              ( T_ARGS... Args )                      noexcept { x_lk_guard(m_Lock); for( auto pDelegate = m_pHead; pDelegate; pDelegate = pDelegate->m_pNext ) pDelegate->Call( std::forward<T_ARGS>(Args)... );             }

    protected:

        using lock = x_lk_spinlock::base<x_lk_spinlock::non_reentrance>;

    protected:

        x_forceinline       void        Connect             ( base_delegate_concreate& S )          noexcept { x_lk_guard(m_Lock); Append(S); S.Attach(*this);   }
        x_forceinline       void        Disconnect          ( base_delegate_concreate& S )          noexcept { x_lk_guard(m_Lock); Remove(S); S.Dettach(*this);  }

    private:

        x_forceinline       void        Append              ( base_delegate_concreate& S )          noexcept { S.m_pNext = m_pHead; if( m_pHead ) m_pHead->m_pPrev = &S; m_pHead = &S;                                                 }
        x_forceinline       void        Remove              ( base_delegate_concreate& S )          noexcept { if( S.m_pNext ) S.m_pNext->m_pPrev = S.m_pPrev; if(S.m_pPrev) S.m_pPrev->m_pNext = S.m_pNext; else m_pHead = S.m_pNext; }

    private:

        lock                        m_Lock;
        base_delegate_concreate*    m_pHead { nullptr };

    protected:
        template< typename, typename... > friend class delegate;
    };

    //--------------------------------------------------------------------------------
    template< typename T_CLASS, typename... T_ARGS > x_forceconst 
    delegate<T_CLASS, T_ARGS...>::delegate( T_CLASS& Class, callback pFunction, event_type& Event ) noexcept : 
        abstract_delegate<T_ARGS...>{ Event },
        m_pClass                    { &Class },
        m_pFunction                 { pFunction }
    {
        x_assert(m_pFunction);
        Event.Append( *this );
    }
};

