//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

// https://wandbox.org/
// https://godbolt.org/

namespace xtable_callback_details
{
    // This should be part of the C++17 standard but clang is having issues
    template <typename F, typename Tuple, size_t... I> constexpr
    decltype(auto) apply_impl(F&& f, Tuple&& t, std::index_sequence<I...>) 
    {
        return std::forward<F>(f)(std::get<I>(std::forward<Tuple>(t))...);
    }

    // This should be part of the C++17 standard but clang is having issues
    template <typename F, typename Tuple> constexpr
    decltype(auto) apply(F&& f, Tuple&& t) 
    {
        using Indices = std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>;
        return apply_impl(std::forward<F>(f), std::forward<Tuple>(t), Indices{});
    }

    template< typename... T > void expand(T&&... ) {}

    // No need to be variadic on the tuple elements as we don't care about them
    // So I'm using forward referencing for the tuple
    template< typename T_TUPLE, size_t... T_ARG_INDICES, typename... T_ARG >
    void table_callback( T_TUPLE&& Tuple, const int Index, std::index_sequence<T_ARG_INDICES...>, T_ARG&&... Arg )
    {
        // If we have too many entries favor a look up table
        // Other wise we just do a few if statements
        // this if statement should be constexp but is not working in vs2017
        if ( static_cast<int>(sizeof...(T_ARG_INDICES)) > 3 ) 
        {
        
            using tuple_args    = std::tuple<T_ARG&&...>;
            using element_type  = void (*)( const T_TUPLE&&, tuple_args&& );

            //constexpr //visual studio does not like it...
            static const element_type Table[] = 
            {
                []( const T_TUPLE&& tuple, tuple_args&& TupleArgs )
                { 
                    xtable_callback_details::apply
                    ( 
                        std::get<T_ARG_INDICES>( std::forward<T_TUPLE>(tuple) ).Callback<T_ARG_INDICES>
                        ,  std::forward<tuple_args>(TupleArgs)
                    ); 
                }
                ...        
            };

            x_assert( Index < sizeof...(T_ARG_INDICES) );
            Table[ Index ]
            ( 
                std::forward<T_TUPLE>( Tuple ) 
                , std::forward<tuple_args>
                ( 
                    tuple_args{ std::forward<T_ARG>(Arg)... }
                ) 
            );
        }
        else
        {
            using callback     = void (*)( T_ARG... );
            callback pCallBack = nullptr;

            expand
            (
                (
                    ( T_ARG_INDICES == Index) ? 
                    pCallBack = (&std::get<T_ARG_INDICES>( std::forward<T_TUPLE>( Tuple ) ).Callback<T_ARG_INDICES>),1:0
                ) ...
            );

            // Call the user callback
            x_assert(pCallBack);
            pCallBack( std::forward<T_ARG>(Arg)... );
        } 
    }
}

// Proverbial layer of indirection to get the indices
template< typename T_TUPLE, typename... T_ARG > constexpr
void xtable_callback( T_TUPLE&& Tuple, const int index, T_ARG&&... Arg )
{
    using indices_type = const std::make_index_sequence<std::tuple_size<std::decay_t<T_TUPLE>>::value>;
    xtable_callback_details::table_callback( std::forward<T_TUPLE>(Tuple), index, indices_type{}, std::forward<T_ARG>(Arg)... );
}

namespace xtable_build_templated_callbacks_details
{
    template< template< typename > class T_CALLBACK_TYPE, typename T_TUPLE, size_t... T_ARG_INDICES >
    using cons = std::tuple
    <
        decltype(T_CALLBACK_TYPE< typename std::tuple_element< T_ARG_INDICES, T_TUPLE >::type >{})...
    >;

    template< template< typename > class T_CALLBACK_TYPE, typename T_TUPLE, size_t... T_ARG_INDICES >
    constexpr auto Filter( std::index_sequence<T_ARG_INDICES...> )
    {
        return xtable_build_templated_callbacks_details::cons< T_CALLBACK_TYPE, T_TUPLE, T_ARG_INDICES... >{};
    }
}

template< template< typename > class T_CALLBACK_TYPE, typename T_TUPLE >
using xtable_build_templated_callbacks = decltype( xtable_build_templated_callbacks_details::Filter< T_CALLBACK_TYPE, T_TUPLE >
(   
    std::make_index_sequence<std::tuple_size<std::decay_t<T_TUPLE>>::value>{}
));

/*
template< typename T >
struct construc2
{
    static void Callback( void* pData ){ new(pData) T{}; std::cout << "Hello2"; }
};

struct pepe
{
    static void Callback(   ) { std::cout << "Hello\n"; }
};

struct pepe1
{
    static void Callback(  ) { std::cout << "Hello2\n"; }
};

struct pepe2
{
    static void Callback(  ) { std::cout << "Hello3\n"; }
};

constexpr static const std::tuple
<
    pepe, 
    pepe1,
    pepe2

> table;

constexpr static const build_templated_callbacks
< 
    construc2, 
    decltype(table) 
> table_const;

// Type your code here, or load an example.
int main100() 
{
    volatile char Something[256];
    for( int i=0; i<100; i++ )
    {
        table_callback( table_const, i%3, (void*)&Something[i] );
        table_callback( table, i%3  );
    }

    return 0;
}
*/