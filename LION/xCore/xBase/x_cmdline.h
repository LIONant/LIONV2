//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//     The command line class is used to help process command line options. These
//     options usually come from the CLI but they can also come from any other source.
//     The command handling is very verbose the help handle any time of commands. 
//
//<P>  There is two steps needed for the use of the class. First is to define valid commands.
//     This is done with the AddCmdSwitch switch. These will define all valid switches that the
//     program can understand. Each switch in turn can be of different types. The list can 
//     be found here xcmdline::type.  The class supports response files. This means that if the your 
//     enters @FileName it will  read command switches from that file. This is very handy to create 
//     scripts and very large command line lists. Inside the file the user can also enter other 
//     response file switches.
//
//<P>  One of the key commands which by default is supported is the questions mark '?'.
//     This will indicate to the class that the user needs help. This can be detected by
//     calling DoesUserNeedsHelp right after the Parse function.
//
//<P>  The second step is to call the appropriate Parse function. This function will start the  
//     process of parsing. The function GetCommandCount will allow you to loop throw all the switches.
//     Each switch will return any parameters as an xstring. You are responsible for converting 
//     those arguments to the actual type. Via x_atof32 or what ever other function you may need.
//     The class is design to allow reprocessing of the command line. Simply call ClearArguments
//     and ask the class to Parse some more.
//
//<P>  Explaining AddCmdSwitch(         // * The return value of the AddCmdSwitch is the ID of the switch
//          pName,                      // First is the name of the switch
//          MinArgCount,                // The switch itself may contain arguments so this is min count of them
//          MaxArgCount                 // This is MAX count of the arguments. Note -1 is infinity
//          nMinTimes                   // Number of times this switch can appear in the arguments
//          nMaxTimes                   // This how the maximum of times... and again -1 == infinity
//          MustFallowOrder             // This indicate if the switch must follow the order of its declaration via (AddCmdSwitch)
//          Type                        // The type that the arguments will be in
//          bMainSwitch                 // There is only 1 main switch allowed at a time. So in a way it to make the switch exclusive.
//                                      // Main switches may have children switches that are not main switches.
//          iParentID );                // Switches can be hierarchical so one switch may have other children switches
//
// Example:
//<CODE>
//  void BasicTest( void )
//  {
//      xcmdline        CmdLine;
//      const char*     pString = "-FileName SomeFile.txt -Res 640 480";
//  
//      // Add the command switches
//      CmdLine.AddCmdSwitch( X_STR_CRCSTR("FileName"), 1, 1, 1, 1, false, xcmdline::type::STRING );
//      CmdLine.AddCmdSwitch( X_STR_CRCSTR("Res"), 2, 2, 1, 1, false, xcmdline::type::INT );
//  
//      // Parse the command line
//      CmdLine.Parse( pString );
//      if( CmdLine.DoesUserNeedsHelp() )
//      {
//          X_LOG( "-FileName <filename> -Res <xres> <yres> \n" );
//          return;
//      }
//  
//      // Handle commands
//      for( s32 i=0; i<CmdLine.getCommandCount(); i++ )
//      {
//          const auto  Cmd     = CmdLine.getCommand(i);
// 
//          switch( Cmd.getCmdCRC() )
//          {
//              case x_constStrCRC32( "FileName" ):
//              {
//                  x_assert( Cmd.getArgumentCount() == 1 );
//                  const xstring&  String = Cmd.getArgument( 0 );
//                  x_assert( x_strcmp( "SomeFile.txt", &String[0] ) == 0 );
//                  break;
//              }
//              case x_constStrCRC32( "Res" ):
//              {
//                  x_assert( Cmd.getArgumentCount() == 2 );
//                  const s32 XRes   = x_atoi32<xchar>( Cmd.getArgument( 0 ) );
//                  const s32 YRes   = x_atoi32<xchar>( Cmd.getArgument( 1 ) );
//                  x_assert( XRes == 640 );
//                  x_assert( YRes == 480 );
//                  break;
//              }
//              default: 
//                  x_assert( false );
//          }
//     }
// }
//<CODE>                         
//------------------------------------------------------------------------------
class xcmdline
{
public:
    
    enum class type : u32
    {
        NONE,
        INT,
        HEX,
        FLOAT,
        GUID,
        STRING,
        STRING_RETAIN_QUOTES
    };

    class cmd
    {
    public:

                                        cmd                 ( void )                        = delete;
        x_forceinline   s32             getArgumentCount    ( void )        const noexcept { return m_CmdLine.getCmdArgumentCount(m_CmdIndex)    ;   }
        x_forceinline   const xstring&  getArgument         ( int Index )   const noexcept { return m_CmdLine.getArgument( Index + m_CmdArgOffset ); }
        x_forceinline   u32             getCmdCRC           ( void )        const noexcept { return m_CmdLine.getCmdCRC(m_CmdIndex)           ;      }
        x_forceinline   const xstring&  getCmdName          ( void )        const noexcept { return m_CmdLine.getCmdName(m_CmdIndex)          ;      }
        x_forceinline   s32             getCmdArgumentOffset( void )        const noexcept { return m_CmdLine.getCmdArgumentOffset(m_CmdIndex);      }
        x_forceinline   s32             getCmdArgumentCount ( void )        const noexcept { return m_CmdLine.getCmdArgumentCount(m_CmdIndex) ;      }

    protected:

        x_forceinline                   cmd                 ( const xcmdline& CmdLine, int CmdIndex ) noexcept : m_CmdLine{ CmdLine }, m_CmdIndex{ CmdIndex }, m_CmdArgOffset{ m_CmdLine.getCmdArgumentOffset(CmdIndex) } {}

    protected:

        const xcmdline&     m_CmdLine;
        const int           m_CmdIndex;
        const int           m_CmdArgOffset;

        friend class  xcmdline;
    };

public:

    x_incppfile     void            ClearArguments      ( void )                                                            noexcept;

    template< u32 T_CRC, int T_LENGTH > 
    x_forceinline   s32             AddCmdSwitch        (   xstring_crcstr<xchar,T_CRC,T_LENGTH>    Name, 
                                                            s32                                     MinArgCount     =  0, 
                                                            s32                                     MaxArgCount     = -1, 
                                                            s32                                     nMinTimes       =  0, 
                                                            s32                                     nMaxTimes       = -1, 
                                                            bool                                    MustFallowOrder = false, 
                                                            type                                    Type            = type::STRING, 
                                                            bool                                    bMainSwitch     = false, 
                                                            s32                                     iParentID       = -1 )  noexcept;

    x_inline        bool            DoesUserNeedsHelp   ( void )                                                    const   noexcept;

    x_incppfile     void            Parse               ( s32 argc, const char** argv )                                     throw( xthrow );
    x_incppfile     void            Parse               ( const char* pString )                                             throw( xthrow );
    x_inline        s32             getCommandCount     ( void )                                                    const   noexcept;
    x_inline        cmd             getCommand          ( int Index )                                               const   noexcept { return { *this, Index }; }

protected:

    struct cmd_def
    {
        xstring         m_Name          {}; // switch name
        u32             m_crcName       {}; // CRC of the switch name    
        s32             m_MinArgCount   {}; // Minimum number of arguments that this switch can have
        s32             m_MaxArgCount   {}; // Max number of arguments that this switch can have    
        s32             m_nMaxTimes     {}; // MAximun of times that this switch can exits (-1) infinite
        s32             m_nMinTimes     {}; // Minimum of times that this switch can exits (-1) infinite
        s32             m_RefCount      {}; // Number of instances at this time pointing at this type
        bool            m_bFallowOrder  {}; // This argument is in the proper order can't happen before
        type            m_Type          {}; // Type of arguments that this switch can have
        bool            m_bMainSwitch   {}; // This to show whether is a main switch. The user can only enter one switch from this category
        s32             m_iParentID     {}; // This indicates that this switch can only exits if iParentID command is already active
    };

    struct cmd_entry
    {
        s32             m_iCmdDef   {};
        s32             m_iArg      {};
        s32             m_ArgCount  {};
    };

protected:

    x_incppfile     void                AppendCmd               ( s32 iCmd, xstring& String )               noexcept;
    x_incppfile     void                AppendCmd               ( s32 iCmd )                                noexcept;
    x_incppfile     void                ProcessResponseFile     ( xwstring& PathName )                      throw( xthrow );
    x_incppfile     void                Parse                   ( xvector<xstring>& Arguments )             throw( xthrow );
    x_inline        s32                 getArgumentCount        ( void )                            const   noexcept;

    x_inline        u32                 getCmdCRC               ( s32 Index )                       const   noexcept;
    x_inline        const xstring&      getCmdName              ( s32 Index )                       const   noexcept;
    x_inline        s32                 getCmdArgumentOffset    ( s32 Index )                       const   noexcept;
    x_inline        s32                 getCmdArgumentCount     ( s32 Index )                       const   noexcept;
    x_inline        const xstring&      getArgument             ( s32 Index )                       const   noexcept;

protected:

    bool                    m_bNeedHelp     { false };
    xvector<cmd_def>        m_CmdDef        {};
    xvector<xstring>        m_Arguments     {};
    xvector<cmd_entry>      m_Command       {};

    friend class cmd;
};
