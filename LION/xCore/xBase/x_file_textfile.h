//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//     This class is used to save and load data files formatted in a particular manner.
//     The format is simple to understand and to look at and it provides the user
//     with tools that makes versions of data easy to maintain and to structure.
//     This file format is best used for generic data such exporters, object properties,
//     etc. Because unlike a serializer it needs to get/set one field at a time.
//     The class is relatively fast and easy with memory which it makes it friendly to
//     use in game code base.
//<P>  Here is a quick list of the features:
//     * Endian support
//     * Binary support
//     * Text support
//     * Handle versions
//     * Handle random fields
//     * Handle comments
//
//     The basics of the file format is simple. It is a table base format where a table can
//     have 1 to n entries. A table is defined by a header which looks like this:
//<CODE>
//     [ TableHeader ]
//</CODE>
//<P>  The header is fallow by the actual structure definition of the data. 
//     The structure of the data is straight forward and fallows very lousy the printf format.
//     here is an example on how a table header fallow by the type structure with some data looks like:
//<CODE>
//     [ TableHeader ]
//     { Name:s Age:d Position:fff }
//      "pepe"  32    0.3 0.6 0.2
//</CODE>
//<P>  There are two kind of headers. One that is meant to have only one data entry like the previous 
//     example and one that is mean to have 1..n entries which looks like this:
//<CODE>
//     [ TableHeader : 2 ]
//     { Name:s Age:d Position:fff }
//      "pepe"  32    0.3 0.6 0.2
//      "jaime" 43    0.6 0.8 0.1
//</CODE>
//<P>  Each column can have one or more types associated with it. As shown in the previous
//     example. Each column has a name fallow by its type as you can see. Here is the table for the types:
//<TABLE>
//     Type    Description
//     ======  -------------------------------------------------------------------
//      f      32 bit float
//      F      64 bit double
//      d      32 bit integer
//      D      64 bit integer
//      c       8 bit integer
//      C      16 bit integer
//      s      this is a string
//      S      undefined (DONT USE)
//      h      32 bit HexInteger     
//      H      undefined (DONT USE)
//      g      64 bit Guid 
//      G      undefined (DONT USE)
//      e      enumerated keyword which is similar to a string but without spaces
//      E      undefined (DONT USE)
//</TABLE>
//<P>  A table has also the ability to redefine the type at each row. This is call dependent types.
//     This dependent types are very useful when dealing with a list of properties of different types,
//     here is an example on how to dependent types looks like:
//<CODE>
//      {  name:s        type:<?>        value:<type>   }
//      //------------   --------------  ----------------
//      "Property one"   <fff>           0.1 0.2 0.3
//      "property two"   <s>             "hello world"
//      "property 3"     <fds>           0.1 2 "3"
//</CODE>
//     We also support dynamic table sizing. This is used when the user wants to hand-edit the files but
//     he does not want to keep counting the rows for his table. In that case you can use the following:
//<CODE>
//     [ TableHeader : ? ]              // Note here how we use a '?' which tells the reader to count automatically
//     { Name:s Age:d Position:fff }
//      "pepe"  32    0.3 0.6 0.2
//      "jaime" 43    0.6 0.8 0.1
//</CODE>
// Example:
//      This is an example of writing one of the text files
//<CODE>
//        xtextfile   TextFile;
//        xstring     FileName( X_STR( "TextFileTest" ));
//
//        TextFile.OpenForWriting ( FileName );
//        TextFile.WriteRecord    ( "FistRecord", 512 );
//        for( s32 i=0; i<512; i++ )
//        {
//            TextFile.WriteField ( "Description:s", "String Test" );
//            TextFile.WriteField ( "Position:fff", x_frand(-100,100), x_frand(-100,100), x_frand(-100,100) );
//            TextFile.WriteField ( "Number:d", x_rand() );
//            TextFile.WriteField ( "EnumParam:e", "TEST1" );
//            TextFile.WriteLine  ();
//        }
//        TextFile.Close();
//</CODE>
//
//      To read the file is also very simple it looks like this:
//<CODE>
//        TextFile.OpenForReading    ( FileName );
//        VERIFY( TextFile.ReadRecord() );
//        for( s32 i=0; i<TextFile.GetRecordCount(); i++ )
//        {
//            xstring String;
//            xstring String2;
//            f32 A, B, C;
//            s32 I;
//            TextFile.ReadLine();
//            TextFile.ReadFieldXString ( "Description:s", String );
//            TextFile.ReadField        ( "Position:fff", &A, &B, &C );
//            TextFile.ReadField        ( "Number:d", &I );
//            TextFile.ReadFieldXString ( "EnumParam:e", String2 );
//        }
//        TextFile.Close();
//</CODE>
//
//     The file format also supports user types. User types always have a '.' in front
//     of their names and they are used to create aliases for atomic types. Just like a structure in 'C'
//     Here is how the file format looks like with them:
//<CODE>
//      // New Types
//      //--------------------------------------------------------------------------------------------------
//      <h>.BOOL <s>.BUTTON <cccc>.COLOR <ddd>.DATE <e>.ENUM <f>.FLOAT <g>.GUID <d>.INT <s>.STRING <fff>.V3
//
//      [ Properties : ? ]
//      { Type:<?>  Name:s                                         Value:<Type> }
//      //--------  ---------------------------------------------  ------------
//          .INT      "Key\EntryList\Count"                               3
//          .ENUM     "Default\Entry\Global\HotpointXMode"             MIDDLE
//          .STRING   "Key\EntryList\Entry[0]\Global\FileName"        "GameData/General/DeamonKingBox/xemB/ani_xemB_move000.png"
//          .INT      "Key\EntryList\Entry[0]\Global\Animation\FPS"      15
//</CODE>
//
// TODO: 
//      * Do a pass and review the use of string vs (char*)
//      * Text formatting.
//      * Can we eliminate the nTypes from the field structure? should we?
//      * Solve and test endian issues
//      * Table names should have the ability to have a unique ID for table references
//         such: [ TableHeader : 2 ] #123123::123123
//------------------------------------------------------------------------------

class xtextfile
{
public:

    X_DEFBITS( access_type, u32, 0,
        X_DEFBITS_ARG( bool,  COMPRESS        ,   1  ),  
        X_DEFBITS_ARG( bool,  BINARY          ,   1  ),  
        X_DEFBITS_ARG( bool,  SWAP_ENDIAN     ,   1  )  
    );

public:
    
    x_incppfile     static  void                TransformTo        ( const xwstring& InputFileName, const xwstring& OutputFileName, access_type Flags = access_type::MASK_DEFAULT );
                            
    x_forceinline                              ~xtextfile          ( void )                                                                                    noexcept { close(); }

    x_incppfile             xfile::err          openForReading     ( const xwstring& FileName )                                                                noexcept;
    x_incppfile             xfile::err          openForWriting     ( const xwstring& FileName, access_type Flags = access_type::MASK_DEFAULT )                 noexcept;    
    x_incppfile             void                close              ( void )                                                                                    noexcept;

    x_incppfile             void                ReadFromFile       ( xfile& File )                                                                             noexcept;
    x_incppfile             void                WriteToFile        ( xfile& File )                                                                             noexcept;

    x_incppfile             bool                ReadRecord         ( void );
    x_incppfile             s32                 ReadField          ( const char* FieldName, ... );
    x_incppfile             s32                 ReadFieldXString   ( const char* FieldName, xstring& Str );
    x_incppfile             void                ReadLine           ( void );
    x_incppfile             void                ReadNextRecord     ( void );

    x_forceconst            int                 getNumberFields    ( void )                                                                            const   noexcept;
    x_inline                void                getFieldDesc       ( int Index, xstring& String )                                                      const   noexcept;
    x_inline                void                getFieldName       ( int Index, xstring& String )                                                      const   noexcept;
    x_inline                const char*         getFieldType       ( int Index )                                                                       const   noexcept;
    x_inline                u8                  getUserTypeUID     ( int Index )                                                                       const   noexcept;
    x_inline                int                 getUserTypeCount   ( void )                                                                            const   noexcept;
    x_forceinline   static  bool                isAtomicString     ( char Type )                                                                               noexcept;
    x_forceinline   static  s32                 getAtomicSize      ( char Type )                                                                               noexcept;
    x_forceconst            const xstring&      getRecordName      ( void )                                                                            const   noexcept;
    x_forceconst            int                 getRecordCount     ( void )                                                                            const   noexcept;

    x_inline                int                 AddUserType        ( const char* pSystemTypes, const char* pUserType, u8 UID )                                  noexcept;
    x_incppfile             void                WriteRecord        ( const char* pHeaderName, s32 Count = -1 );
    x_incppfile             void                WriteField         ( const char* FieldName, ... );
    x_incppfile             void                WriteFieldIndirect ( const char* FieldName, ... );

    x_incppfile             void                WriteFieldV3       ( const char* FieldName, const xvector3d&   V );
    x_incppfile             void                WriteFieldV4       ( const char* FieldName, const xvector4&    V );
    x_incppfile             void                WriteFieldQ        ( const char* FieldName, const xquaternion& Q );
    x_incppfile             void                WriteFieldR3       ( const char* FieldName, const xradian3&    R );
    x_incppfile             void                WriteFieldColor    ( const char* FieldName, const xcolor&      C );

    x_incppfile             void                WriteComment       ( const xstring& Comment );
    x_incppfile             void                WriteLine          ( void );
    x_forceconst            bool                isEOF              ( void )                                                                             const  noexcept { return m_States.m_bEOF; }

protected:

    enum 
    {
        MAX_TYPES_PER_FIELD = 32
    };

    struct record_info
    {
        xstring                                 m_Name;
        s32                                     m_Count;
        bool                                    m_bWriteCount;
    };

    struct field
    {
        xstring                                 m_Type;             // Field name such "jack:ff"
        s32                                     m_nTypes;           // Number of types for this field
        s32                                     m_TypeOffset;       // This is the offset to the string which gives the type ex:"fff"
        xarray<s32,MAX_TYPES_PER_FIELD>         m_iMemory;          // The location in the memory buffer for each of these fields    
        s32                                     m_iFieldDecl;       //  * When this is -1 means this is a normal field type
                                                                    //  * When this is -2 this means is a declaration
                                                                    //  * When is positive then it is a "pepe:<test>" and
                                                                    //  this has an index to the field which contain the type declaration
        u32                                     m_Width;            // What is the width in character for this field. This is only used when writing.
        u16                                     m_TypesPreWidth;
        u16                                     m_TypesPostWidth;
        xarray<u8,MAX_TYPES_PER_FIELD>          m_TypeWidth;        // This is the width that each column has per type
        xarray<u8,MAX_TYPES_PER_FIELD>          m_FracWidth;        // This is the additional offset for floats/ints so that the integer part can align
        s32                                     m_DynamicTypeCount; // This is just used for debugging but its intent is a type count for dynamic types
        u16                                     m_iUserType=0xffff; // A place to put the user type
    };

    struct user
    {
        s32                                     m_iFieldType;
        s32                                     m_iFieldData;
    };
    
    struct user_type
    {
        xstring                                 m_UserType          {};
        xstring                                 m_SystemType        {};
        bool                                    m_bSaved            {};
        s32                                     m_nSystemTypes      {};
        u8                                      m_UID               {};
        
                user_type   ( void )                        {}
                user_type   ( const xstring& Str )          { m_UserType = Str; }
                user_type   ( const char* pStr )            { m_UserType.Copy( pStr ); }
        bool    operator <  ( const user_type& Type ) const { return x_strcmp( &m_UserType[0], &Type.m_UserType[0] ) < 0; }
    };

    X_DEFBITS( states, u32, 0,
        X_DEFBITS_ARG( bool,  bReading         ,   1  ),    // Tells the system whether we are reading or writing
        X_DEFBITS_ARG( bool,  bBinary          ,   1  ),    // Tells the system whether we are dealing with a text file or binary
        X_DEFBITS_ARG( bool,  bEndianSwap      ,   1  ),    // Tells whether we should be doing endian swapping
        X_DEFBITS_ARG( bool,  bXStrings        ,   1  ),    // Tells to read strings as xstring rather than char*
        X_DEFBITS_ARG( bool,  bNewFile         ,   1  ),    // Tells whether we have allocated the file pointer
        X_DEFBITS_ARG( bool,  bEOF             ,   1  )
    );

protected:

    x_incppfile         void        BuildTypeInformation    ( const char* FieldName );
    x_incppfile         void        WriteField              ( const char*& FieldName, xva_list& Args1, xva_list& Args2, bool bIndirect );
    x_incppfile         void        WriteComponent          ( s32 iField, s32 iType, s32 c, xva_list& Args, bool bIndirect );
    x_incppfile         void        WriteUserTypes          ( void );
    x_incppfile         s32         ReadWhiteSpace          ( bool bReturnError = false );
    x_incppfile         bool        isFloat                 ( s32 Char )                                                                            noexcept;
    x_incppfile         s32         ReadComponent           ( s32 Type );
    x_incppfile         bool        isValidType             ( s32 Type )                                                                            noexcept;
    x_incppfile         s32         HandleDynamicTable      ( void );

protected:

    xndptr_s<xfile>         m_pFile                 {};                 // File pointer
    record_info             m_Record                {};                 // This contains information about the current record
    s32                     m_iLine                 {};                 // Which line we are in the current record
    s32                     m_iField                {};                 // Current Field that we are trying to read
    s32                     m_nTypesLine            {};                 // Number of types per line

    xvector<user>           m_User                  {};                 // This is used only when reading
    xvector<field>          m_Field                 {};                 // Contain a list of field names and their types
    xvector<user_type>      m_UserTypes             {};                 // List of types created by the user
    
    xvector<s16>            m_BinReadUserTypeMap    {};                 // This is an optimization done when loading binary files using usertypes
                                                                        // this tables have index to types that were loaded from the file rather
                                                                        // than added by the user. This table is always shorted as well.

    s32                     m_iCurrentOffet         {};                 // Index to the next location ready to be allocated
    xvector<char>           m_Memory                {};                 // Buffer where the fields are store (Only one line worth)

    states                  m_States                {};                 // States
};
