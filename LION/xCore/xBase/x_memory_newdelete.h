//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      heap management functions
//      DO NOT USE THE STANDARD NEW/DELETE!!! Use the following ones stead.
//      This is necessary to make sure memory gets allocated in the main process
//      Also these functions solve all the annoying C++ issues with memory alignment
//      Also remove all the hidden extra memory needed for standard templates
//------------------------------------------------------------------------------
#define x_construct(TYPE,MEMORY, ... )          X_VARG_EXPAND( static_cast< xowner<TYPE*> >( new(MEMORY) TYPE __VA_ARGS__ ) )
#define x_new_arrayv(TYPE,VAR_COUNT, ... )      [&]( const xuptr Count ) -> xowner<TYPE*> { xowner<TYPE*> __p = xowner<TYPE*>( g_context::get().aligned_alloc( sizeof(TYPE)*Count, alignof(TYPE) )); if( std::is_class<TYPE>::value ) for(xuptr i=0; i!=Count; ++i) X_VARG_EXPAND( (void)x_construct( TYPE, &__p[i], __VA_ARGS__  ) ); return __p; }(VAR_COUNT)
#define x_new_arrayc(TYPE,CONST_COUNT, ... )    [&]() -> xowner<TYPE*> { struct __x { __x() : m_Item { __VA_ARGS__ } {} TYPE m_Item[CONST_COUNT]; }; return reinterpret_cast<xowner<TYPE*>>(x_new(__x)); } ()
#define x_new(TYPE, ... )                       X_VARG_EXPAND( x_construct( TYPE, static_cast< xowner<TYPE*> >( g_context::get().aligned_alloc( sizeof(TYPE), alignof(TYPE) ) ), __VA_ARGS__ ) )
#define x_delete(PTR)                           if(PTR) { x_destruct(PTR); g_context::get().aligned_free(PTR);               }
#define x_delete_arrayc(PTR,COUNT)              if(PTR) { x_destruct_arrayc(PTR,COUNT); g_context::get().aligned_free( PTR ); }
#define x_delete_arrayv(PTR,COUNT)              if(PTR) { x_destruct_arrayv(PTR,COUNT); g_context::get().aligned_free( PTR ); }
#define x_free(PTR)                             if(PTR) { g_context::get().aligned_free( PTR );             }
#define x_malloc(TYPE,COUNT)                    (TYPE*)g_context::get().aligned_alloc( sizeof(TYPE)*(COUNT), alignof(TYPE) )
#define x_realloc(TYPE,PTR,COUNT)               (TYPE*)g_context::get().aligned_realloc( PTR, sizeof(TYPE)*(COUNT), alignof(TYPE) )

//------------------------------------------------------------------------------
// Description:
//          Deals with stack allocation. Windows uses a safer call when in debug mode.
//------------------------------------------------------------------------------
#if _X_TARGET_WINDOWS && _X_DEBUG
    #define x_alloca( SIZE ) _malloca( SIZE )
#else
    #define x_alloca( SIZE ) alloca( SIZE )
#endif

//------------------------------------------------------------------------------
// Description:
//          Calls the destructor of an object in a safe way. It returns the pointer
//          of the destroyed object. Note that the object should be a non array.
//------------------------------------------------------------------------------
template< typename T > x_inlineconst  
typename std::enable_if< std::is_trivially_destructible<T>::value, T* >::type
x_destruct( T* pPtr )
{
    return x_assume(pPtr), pPtr;
}

template< typename T > x_inlineconst  
typename std::enable_if< std::has_virtual_destructor<T>::value && (!std::is_trivially_destructible<T>::value), T* >::type
x_destruct( T* pPtr )
{
    return x_assume(pPtr), pPtr->~T(), pPtr;
}

// WARNING HACK HACK HACK!!!!!! Visual studio is crashing unless the following fix is used.
// Here should just be pPtr->~T();
template< typename T > x_inlineconst  
typename std::enable_if< !std::has_virtual_destructor<T>::value && (!std::is_trivially_destructible<T>::value), T* >::type
x_destruct( T* pPtr )
{
    static_assert( std::is_polymorphic<T>::value == std::has_virtual_destructor<T>::value, "You have an object which has a virtual table, it should also have a virtual destructor" ); 
    return x_assume(pPtr), pPtr->T::~T(), pPtr;
}

//------------------------------------------------------------------------------
// Description:
//          Calls the destructor for an array of objects. The count of the list
//          can be dynamic. In other words the size of the array was not determine at compile time.
//------------------------------------------------------------------------------
template<typename T> x_forceinline 
typename std::enable_if< !std::is_trivially_destructible<T>::value, void >::type
x_destruct_arrayv( T* pPtr, const xuptr Count ) 
{ 
    x_assume(pPtr);
    for( xuptr i=0; i!=Count; ++i ) 
        x_destruct( &pPtr[i] ); 
}

template<typename T> x_forceinline 
typename std::enable_if< std::is_trivially_destructible<T>::value, void >::type
x_destruct_arrayv( T* pPtr, const xuptr Count ) 
{ 
    x_assume(pPtr);
}

//------------------------------------------------------------------------------
// Description:
//          Calls the destructor for an array of objects. The count of the list
//          must be a constexpr. This is useful for regular arrays
//------------------------------------------------------------------------------
template<typename T, xuptr CONST_COUNT > x_forceinline 
typename std::enable_if< !std::is_trivially_destructible<T>::value, void >::type
x_destruct_arrayc( T* pPtr ) 
{ 
    x_assume(pPtr);
    struct __x { T m_Item[CONST_COUNT]; };
    x_destruct( reinterpret_cast<__x*>(pPtr) );
}

template<typename T, xuptr CONST_COUNT > x_forceinline 
typename std::enable_if< std::is_trivially_destructible<T>::value, void >::type
x_destruct_arrayc( T* pPtr ) 
{ 
    x_assume(pPtr);
}

