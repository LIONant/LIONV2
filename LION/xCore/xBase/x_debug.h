//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once



//------------------------------------------------------------------------------
// Description:
//      Prints as an error the type. It is useful for debugging templates
//------------------------------------------------------------------------------
template<typename T>
void x_type_is();

//------------------------------------------------------------------------------
// Description:
//      Print a constant expression which returns a number
//      Example on how to use it:
//      static_assert( x_display_non_zero_int_value<x_EndianSwap(s64(0xabcdefAA12345678))>::value,"");
//------------------------------------------------------------------------------
namespace _x_debug
{
    template <typename T, T k >
    struct print_constexpr_value;
}

#define x_print_constexpr_value(A) static_assert( _x_debug::print_constexpr_value<decltype(A),A>::value, "" );

//------------------------------------------------------------------------------
// Description:
//      Debug class to know when you are in linear vs quantum space
//------------------------------------------------------------------------------
#if _X_DEBUG
class x_debug_linear_quantum 
{
    x_object_type( x_debug_linear_quantum, is_quantum_lock_free );

protected:
    
    void LockLinear( void ) const
    {
        data LocalData = m_Data;
        data NewData;
        do
        {
            NewData = LocalData;
            
            if( LocalData.m_bBreak ) while( 1 ) { std::this_thread::sleep_for( std::chrono::seconds( 1 ) );  } //__debugbreak();
            
            if( NewData.m_Count == 0 )
            {
                if( NewData.m_Flag != FLAG_NONE )
                {
                    AddBreakToTryCatchOffender( LocalData );
                    assert( NewData.m_Flag == FLAG_NONE );
                }
                
                NewData.m_Flag      = FLAG_LINEAR;
                NewData.m_ThreadID  = x_getThreadID16();
            }
            else
            {
                uint16_t Thread = x_getThreadID16();
                if( NewData.m_ThreadID != Thread )
                {
                    AddBreakToTryCatchOffender( LocalData );
                    const auto Threadid = std::this_thread::get_id();
                    (void)Threadid;
                    assert( NewData.m_Flag     == FLAG_LINEAR );
                    assert( NewData.m_ThreadID == Thread );
                }
            }
            
            NewData.m_Count++;
            
        } while( m_Data.compare_exchange_weak( LocalData, NewData ) == false );
        
        // If we are locking linear lets also backup the full thread id
        if( NewData.m_Count == 1 )
        {
            auto id = std::this_thread::get_id();
            m_FullThreadID = *reinterpret_cast<thread_id*>(&id);
        }
    }
    
    void UnlockLinear( void ) const
    {
        data LocalData = m_Data;
        data NewData;
        do
        {
            NewData = LocalData;
            
            if( LocalData.m_bBreak ) while(1){ std::this_thread::sleep_for( std::chrono::seconds( 1 ) ); } //__debugbreak();
            assert( NewData.m_Flag == FLAG_LINEAR );
            assert( NewData.m_Count > 0 );
            NewData.m_Count--;
            
            if( NewData.m_Count == 0 )
            {
                NewData.m_Flag      = FLAG_NONE;
                NewData.m_ThreadID  = 0;
            }
            
        } while( m_Data.compare_exchange_weak( LocalData, NewData ) == false );
    }
    
    void LockQuantum( void ) const
    {
        data LocalData = m_Data;
        data NewData;
        do
        {
            NewData = LocalData;
            
            if( LocalData.m_bBreak ) while(1){ std::this_thread::sleep_for( std::chrono::seconds( 1 ) ); } //__debugbreak();
            if( NewData.m_Count == 0 )
            {
                // It is invalid to have a zero count with a defined space
                if( NewData.m_Flag != FLAG_NONE )
                {
                    AddBreakToTryCatchOffender( LocalData );
                    assert( NewData.m_Flag == FLAG_NONE );
                }
                
                NewData.m_Flag = FLAG_QUANTUM;
            }
            else
            {
                // It is invalid if we have some count yet not space is define in the flag
                // We can not access a linear space this is invalid
                if( NewData.m_Flag == FLAG_NONE ||
                    NewData.m_Flag == FLAG_LINEAR )
                {
                    if( NewData.m_Flag == FLAG_LINEAR && NewData.m_ThreadID == x_getThreadID16())
                    {
                        // If we are linear but someone tries to lock it quantum but we are in the same
                        // thread then we are ok
                    }
                    else
                    {
                        AddBreakToTryCatchOffender( LocalData );
                        assert( NewData.m_Flag != FLAG_NONE );
                    }
                }
            }
            
            NewData.m_Count++;
            
        } while( m_Data.compare_exchange_weak( LocalData, NewData ) == false );
    }
    
    void UnlockQuantum( void ) const
    {
        data LocalData = m_Data.load();
        data NewData;

        if( LocalData.m_Count != 0 ) do
        {
            NewData = LocalData;
            
            if( LocalData.m_bBreak ) while(1){ std::this_thread::sleep_for( std::chrono::seconds( 1 ) );  } //__debugbreak();
            x_assert( NewData.m_Flag != FLAG_NONE );      // Flag here could be either linear or quantum
            x_assert( NewData.m_Count > 0 );
            NewData.m_Count--;
            
            if( NewData.m_Count == 0 )
            {
                NewData.m_Flag      = FLAG_NONE;
                NewData.m_ThreadID  = 0;
            }
            
        } while( m_Data.compare_exchange_weak( LocalData, NewData ) == false );
    }
    
    void LockNone( void ) const
    {
        data LocalData = m_Data;
        data NewData;
        do
        {
            NewData = LocalData;
            
            if( LocalData.m_bBreak ) while(1){ std::this_thread::sleep_for( std::chrono::seconds( 1 ) ); } 
            if( NewData.m_Count == 0 )
            {
                if( NewData.m_Flag != FLAG_NONE )
                {
                    AddBreakToTryCatchOffender( LocalData );
                    assert( NewData.m_Flag == FLAG_NONE );
                }
                
                assert( NewData.m_Flag == FLAG_NONE );
            }
            else
            {
                // A linear space can go into the quantum space but not the other way around
            }
            
            NewData.m_Count++;
            
        } while( m_Data.compare_exchange_weak( LocalData, NewData ) == false );
    }
    
    void UnlockNone( void ) const
    {
        data LocalData = m_Data;
        data NewData;
        do
        {
            NewData = LocalData;
            
            if( LocalData.m_bBreak ) while(1){ std::this_thread::sleep_for( std::chrono::seconds( 1 ) ); } //__debugbreak();
            assert( NewData.m_Flag == FLAG_NONE );      // Flag here could be either linear or quantum
            assert( NewData.m_Count > 0 );
            NewData.m_Count--;
            
            if( NewData.m_Count == 0 )
            {
                NewData.m_Flag     = FLAG_NONE;
                NewData.m_ThreadID = 0;
            }
            
        } while( m_Data.compare_exchange_weak( LocalData, NewData ) == false );
    }
    
protected:
    
    enum flags : u8
    {
        FLAG_NONE,
        FLAG_LINEAR,
        FLAG_QUANTUM,
    };
    
    struct alignas(u64) data 
    {
        constexpr data( void ) noexcept {};
        constexpr data( bool ) noexcept :
            m_Count     { 0 },
            m_ThreadID  { 0 },
            m_Flag      { FLAG_NONE },
            m_bBreak    { false }
        {
        }

        u32             m_Count     {};
        u16             m_ThreadID  {};
        flags           m_Flag      {};
        u8              m_bBreak    {};
    };
    
    static_assert( sizeof(data) == 8, "this is atomic so must remain 64bit max" );
    
protected:
    
    void AddBreakToTryCatchOffender( data LocalData ) const
    {
        data NewData;
        do
        {
            x_assert( LocalData.m_bBreak == false );
            NewData            = LocalData;
            NewData.m_bBreak   = true;
            
        } while( m_Data.compare_exchange_weak( LocalData, NewData ) == false );
    }
    
protected:

    // we replace the std::thread::id with our own so that we can keep the constructor as a constexpr 
    using thread_id = x_size_to_unsigned<sizeof(std::thread::id)>::type;

    mutable thread_id           m_FullThreadID      { 0 };
    mutable x_atomic<data>      m_Data              { data(true) };
    
protected:
    
    friend class _assert_linear;
    friend class _assert_quantum;
    friend class _assert_qtnone;
};

#else

class x_debug_linear_quantum {};

#endif

//------------------------------------------------------------------------------
// Description:
//      System for debugging. It asserts that something is either in the linear world
//      or in the quantum world.
//------------------------------------------------------------------------------
#if _X_DEBUG
    #define x_assert_linear( X )  _assert_linear  X_CREATE_UNIQUE_VARNAME( x_assert_linear  ) ( X );
    #define x_assert_quantum( X ) _assert_quantum X_CREATE_UNIQUE_VARNAME( x_assert_quantum ) ( X );
    #define x_assert_lqnone( X )  _assert_qtnone  X_CREATE_UNIQUE_VARNAME( x_assert_qtnone  ) ( X );
    class _assert_qtnone
    {
    public:
        x_forceinline   _assert_qtnone( const x_debug_linear_quantum& D ) : m_DebugLQ( D )    { m_DebugLQ.LockNone();      }
        x_forceinline  ~_assert_qtnone( void )                                                { m_DebugLQ.UnlockNone();    }
    protected:
        const x_debug_linear_quantum& m_DebugLQ;
    };

    class _assert_linear
    {
    public:
        x_forceinline  _assert_linear( const x_debug_linear_quantum& D ) : m_DebugLQ( D )    { m_DebugLQ.LockLinear();      }
        x_forceinline ~_assert_linear( void )                                                { m_DebugLQ.UnlockLinear();    }
    protected:
        const x_debug_linear_quantum& m_DebugLQ;
    };

    class _assert_quantum
    {
    public:
        x_forceinline  _assert_quantum( const x_debug_linear_quantum& D ) : m_DebugLQ( D )   { m_DebugLQ.LockQuantum(); }
        x_forceinline ~_assert_quantum( void )                                               { m_DebugLQ.UnlockQuantum(); }
    protected:
        const x_debug_linear_quantum& m_DebugLQ;
    };
#else
    #define x_assert_linear( X )
    #define x_assert_quantum( X )
    #define x_assert_lqnone( X )
#endif

//------------------------------------------------------------------------------
// Description:
//          Simple debug line that goes away on release builds
//------------------------------------------------------------------------------
#if _X_DEBUG
    #define X_DEBUG_CMD(A) A
#else
    #define X_DEBUG_CMD(A) 
#endif

//------------------------------------------------------------------------------
// Description:
//          asserts if an object has the wrong type to be used inside a thread/task ask been in the quantum world.
//------------------------------------------------------------------------------

#define x_assert_safe_quantum( A ) static_assert( x_is_quantum_safe<                                            \
                                        decltype(A),                                                            \
                                        std::is_pod<std::remove_reference<std::remove_pointer<decltype(A)>::type>::type>::value >::value,  \
                                        "This type is not safe to use in the quantum world is neither a ::t_is_quantum nor is constant"   )


//------------------------------------------------------------------------------
// Description:
//          safe capturing for lamdbas.
//          The following are the constexpr that check the types
//------------------------------------------------------------------------------
namespace _x_debug
{ 
    template< typename T_VARIABLE_TYPE, typename T_CAPTURE_TYPE >
    constexpr T_CAPTURE_TYPE& capture_as_val_check( T_VARIABLE_TYPE& Val ) noexcept
    {
        using type = typename std::remove_reference< typename std::remove_pointer<T_CAPTURE_TYPE>::type >::type;
        static_assert( x_is_quantum_safe< T_CAPTURE_TYPE, std::is_pod< type >::value >::value,
                                            "This type is not safe to use in the quantum world is neither a ::t_is_quantum nor is constant"   );
        return Val;
    }

    template< typename T_VARIABLE_TYPE, typename T_CAPTURE_TYPE >
    constexpr T_CAPTURE_TYPE& capture_as_ref_check( T_VARIABLE_TYPE& Val ) noexcept
    {
        using type = typename std::remove_reference< typename std::remove_pointer<T_CAPTURE_TYPE>::type >::type;
        static_assert( x_is_quantum_safe< T_CAPTURE_TYPE, std::is_pod<type>::value >::value,
                                            "This type is not safe to use in the quantum world is neither a ::t_is_quantum nor is constant"   );
        return Val;
    }
};

//------------------------------------------------------------------------------
// Description:
//          safe capturing for quantum/thread/task lamdbas
//------------------------------------------------------------------------------
#define x_qcapture_val_const(A)    A = _x_debug::capture_as_val_check<decltype(A),const decltype(A)>(A) 
#define x_qcapture_ref_const(A)   &A = _x_debug::capture_as_ref_check<decltype(A),const decltype(A)&>(A) 
#define x_qcapture_ref(A)         &A = _x_debug::capture_as_ref_check<decltype(A),      decltype(A)&>(A) 
#define x_qcapture_val(A)          A = _x_debug::capture_as_val_check<decltype(A),      decltype(A)>(A) 

//------------------------------------------------------------------------------
// Description:
//          safe capturing for linear lamdbas
//------------------------------------------------------------------------------
#define x_capture_val_const(A)    A = std::as_const(A) 
#define x_capture_ref_const(A)   &A = std::as_const(A) 
#define x_capture_ref(A)         &A = A 
#define x_capture_val(A)          A = A 
