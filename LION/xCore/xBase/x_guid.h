//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once



//------------------------------------------------------------------------------
// Description:
//      Generation of a Global Unique Identifier
//
// Algorithm:
//      The random number generator (RNG) was found in the ALGORITHMS Notesfile.
//      See "The Multiple Prime Random Number Generator" by Alexander
//      Hass pp. 368-381, ACM Transactions on Mathematical Software, 12/87.
//------------------------------------------------------------------------------
class xguid64_generation
{
    x_object_type( xguid64_generation, is_quantum_lock, is_not_movable, is_not_copyable );

public:
    
    unsigned long long Generate( void )
    {
        std::lock_guard<std::mutex> Lock( m_Mutex );
        const u64 Sequence  = getSequence16();
        const u64 Time      = getTime16();
        const u64 Random    = getRandom16();
        const u64 UniqueTag = getUniqueTag();
        return ( Sequence   << ( 16 * 0 ) )
        |  ( Time       << ( 16 * 1 ) )
        |  ( Random     << ( 16 * 2 ) )
        |  ( UniqueTag  << ( 16 * 3 ) );
    }
    
    xguid64_generation( void )
    {
        m_Rand_m        = 971;
        m_Rand_ia       = 11113;
        m_Rand_ib       = 104322;
        m_Rand_irand    = 4181;
        
        m_Rand_irand += getTime16() + x_getThreadID16();
    }
    
protected:
    
    unsigned short getUniqueTag( void )
    {
#if defined(_WIN64) || defined(__x86_64__)
        const unsigned short A = []
        {
            unsigned short A   = 0;
            const char*    pA  = reinterpret_cast<const char*>(&A);
            long long      llA = reinterpret_cast<long long>(pA);
            llA ^= (llA >> 32);
            llA ^= (llA >> 16);
            return  static_cast<unsigned short>(llA);
        }();
#else
        const unsigned short A = []
        {
            unsigned short A;
            const char*    pA  = reinterpret_cast<const char*>(&A);
            long           llA = reinterpret_cast<long>(pA);
            llA ^= (llA >> 16);
            return static_cast<unsigned short>(llA);
        }();
#endif
        const unsigned short B = x_getThreadID16();
        return m_LastResetTime^A^B;
    }
    
    unsigned short getSequence16( void )
    {
        if( m_Sequence == 0xffff )
        {
            unsigned short Time;
            while( m_LastResetTime == ( Time = getTime16() ) )
            {
                // We are generating too guids too fast lets wait for things
                std::this_thread::sleep_for( std::chrono::microseconds( 1 ) );
            }
            m_LastResetTime = Time;
        }
        return (unsigned short) m_Sequence++;
    }
    
    unsigned short getTime16( void )
    {
        using namespace std;
        using namespace std::chrono;
        system_clock::time_point Now = system_clock::now();
        time_t                   t   = system_clock::to_time_t(Now);
        
        unsigned short        Seed;
        Seed  = (t >> (16*0)) & 0xFFFF;
        Seed ^= (t >> (16*1)) & 0xFFFF;
        Seed ^= (t >> (16*2)) & 0xFFFF;
        Seed ^= (t >> (16*3)) & 0xFFFF;
        
        return Seed;
    }
    
    unsigned short getRandom16( void )
    {
        if ((m_Rand_m  +=     7) >=   9973) m_Rand_m  -=  9871;
        if ((m_Rand_ia +=  1907) >=  99991) m_Rand_ia -= 89989;
        if ((m_Rand_ib += 73939) >= 224729) m_Rand_ib -= 96233;
        
        m_Rand_irand = (m_Rand_irand * m_Rand_m) + m_Rand_ia + m_Rand_ib;
        
        return (m_Rand_irand >> 16) ^ (m_Rand_irand & 0x3FFF);
    }
    
protected:
    
    std::mutex  m_Mutex;
    unsigned    m_Rand_m;
    unsigned    m_Rand_ia;
    unsigned    m_Rand_ib;
    unsigned    m_Rand_irand;
    unsigned    m_Sequence       = 0;
    unsigned    m_LastResetTime  = 179426549;
};

//------------------------------------------------------------------------------
// Description:
//      Basic guid type. By making it a template it will force unique types
//------------------------------------------------------------------------------
template< typename T >
struct xguid
{
    x_object_type( xguid, is_linear );

public:

    enum reset : int { RESET };
    
public:

    constexpr                               xguid           ( void )                                            noexcept = default;
    constexpr                               xguid           ( const xguid& )                                    noexcept = default;
    x_forceconst explicit                   xguid           ( std::nullptr_t )                                  noexcept : m_Value{ 0 } {}
    x_forceconst explicit                   xguid           ( const u64 Guid )                                  noexcept : m_Value{ Guid } {}
    x_forceconst explicit                   xguid           ( const u32 Upper, const u32 Lower )                noexcept : m_Value{ (static_cast<u64>(Upper)<<32) | Lower } {}
    x_forceconst                            xguid           ( const char* pString )                             noexcept : m_Value{ x_constStrCRC32( pString ) } {}
    x_forceconst                            xguid           ( reset )                                           noexcept : m_Value{  g_context::get().m_GuidGeneration.Generate() } {}
    x_forceinline           void            Reset           ( void )                                            noexcept { m_Value = g_context::get().m_GuidGeneration.Generate(); }
    x_forceinline           void            getStringHex    ( xstring& String, char Separator = ':' )   const   noexcept { String.setup( "%X%c%X", (u32)(m_Value>>32), Separator, (u32)m_Value ); }
    x_forceconst            bool            operator ==     ( const xguid A )                           const   noexcept { return m_Value == A.m_Value; }
    x_forceconst            bool            operator !=     ( const xguid A )                           const   noexcept { return m_Value != A.m_Value; }
    x_forceconst            bool            operator <      ( const xguid A )                           const   noexcept { return m_Value < A.m_Value; }

public:

    u64  m_Value{};
};
