//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      Finding the traits from a function.
//      https://github.com/kennytm/utils/blob/master/traits.hpp
//      https://functionalcpp.wordpress.com/2013/08/05/function-traits/
// Example:
//      template< typename T_FUNCTION >
//      struct task_continue
//      {
//          using t_self                = task_continue;
//          using t_function_traits     = xfunction_traits<T_FUNCTION>;
//          using t_function_arg        = typename t_function_traits::template t_arg<0>::type;
//          using t_function_return     = typename t_function_traits::t_return_type;
//          using t_function_type       = typename x_make_function< function_return, function_arg >::type
//          using t_lambda_job          = lambda_job< t_function_return, t_function_arg >;
//      };
//------------------------------------------------------------------------------
template< typename T_RET, typename... T_ARGS >
struct x_make_function
{
    using type = T_RET( T_ARGS... );
};

template< typename T_RET >
struct x_make_function< T_RET, void >
{
    using type = T_RET();
};

//------------------------------------------------------------------------------

template< int T_I, std::size_t T_MAX, typename... T_ARGS  >
struct xfunction_traits_args
{
    using type = typename std::tuple_element< T_I, std::tuple<T_ARGS...> >::type;
};

template<>
struct xfunction_traits_args<0,0>
{
    using type = void;
};

//------------------------------------------------------------------------------

// as seen on http://functionalcpp.wordpress.com/2013/08/05/function-traits/
template<class F>
struct xfunction_traits;

template< typename T_RETURN_TYPE, typename... T_ARGS >
struct xfunction_traits< T_RETURN_TYPE(T_ARGS...) >
{
    using                        t_return_type      = T_RETURN_TYPE;
    x_constexprvar std::size_t   t_arg_count        = sizeof...(T_ARGS);
    using                        t_class_type       = void;
    using                        t_self             = xfunction_traits< T_RETURN_TYPE(T_ARGS...) >;
    using                        t_func_type        = typename x_make_function< T_RETURN_TYPE, T_ARGS... >::type;

    template< std::size_t T_I > 
    struct t_arg      
    { 
        using type = typename xfunction_traits_args<T_I ,t_arg_count,T_ARGS...>::type;
    };

    template< std::size_t T_I >
    struct t_safe_arg
    {
        using type = typename std::conditional< T_I < t_arg_count, typename std::tuple_element<(T_I<t_arg_count?T_I:t_arg_count-1),std::tuple<T_ARGS...>>::type, void >::type;
    };
};

// function pointer
template< typename T_RETURN, typename... T_ARGS >
struct xfunction_traits< T_RETURN(*)(T_ARGS...) > : public xfunction_traits< T_RETURN(T_ARGS...) >
{};

// member function pointer
template<class T_CLASS, typename T_RETURN, typename... T_ARGS>
struct xfunction_traits< T_RETURN(T_CLASS::*)( T_ARGS... ) > : public xfunction_traits< T_RETURN( T_CLASS&, T_ARGS... ) >
{
    using   t_func_type        = typename x_make_function< T_RETURN, T_ARGS... >::type;
};

// const member function pointer
template<class T_CLASS, typename T_RETURN, typename... T_ARGS >
struct xfunction_traits< T_RETURN(T_CLASS::*)(T_ARGS...) const > : public xfunction_traits<T_RETURN(T_CLASS&, T_ARGS...)>
{
    using   t_func_type        = typename x_make_function< T_RETURN, T_ARGS... >::type;
};

// member object pointer
template< class T_CLASS, typename T_RETURN >
struct xfunction_traits<T_RETURN(T_CLASS::*)> : public xfunction_traits<T_RETURN(T_CLASS&)>
{
    using   t_func_type        = typename x_make_function< T_RETURN, void >::type;
};

// functor
template< class F >
struct xfunction_traits
{
    using                           t_self          = xfunction_traits<F>;
    using                           call_type       = xfunction_traits<decltype(&F::operator())>;
    using                           t_return_type   = typename call_type::t_return_type;
    x_constexprvar std::size_t      t_arg_count     = call_type::t_arg_count - 1;
    using                           t_class_type    = F;
    using                           t_func_type     = typename call_type::t_func_type;

    template< std::size_t T_I > 
    using t_arg = typename call_type::template t_arg<T_I+1>;

    template< std::size_t T_I >
    using t_safe_arg = typename call_type::template t_safe_arg<T_I+1>;
};
 
// functor references
template<class F>
struct xfunction_traits<F&> : public xfunction_traits<F>
{};

template<class F>
struct xfunction_traits<F&&> : public xfunction_traits<F>
{};

//---------------------------------------------------------------------------------------

template< typename T_A, typename T_B, int T_ARG_I >
struct xfunction_traits_compare_args
{
    static_assert( std::is_same
                   < 
                        typename T_A::template t_arg< T_ARG_I >::type , 
                        typename T_B::template t_arg< T_ARG_I >::type 
                   >::value, "Argument Don't match" );
    x_constexprvar bool value = xfunction_traits_compare_args<T_A, T_B, T_ARG_I - 1 >::value;
};

template< typename T_A, typename T_B >
struct xfunction_traits_compare_args< T_A, T_B, -1 >
{
    x_constexprvar bool value = true;
};

template< typename T_A, typename T_B >
struct xfunction_traits_compare
{
    static_assert( T_A::t_arg_count == T_B::t_arg_count, "Function must have the same number of arguments" );
    static_assert( std::is_same< typename T_A::t_return_type, typename T_B::t_return_type >::value, "Different return types" );
    static_assert( xfunction_traits_compare_args<T_A, T_B, static_cast<int>( T_B::t_arg_count ) - 1>::value, "Arguments don't match" );
    static_assert( std::is_same< typename T_A::t_func_type, typename T_B::t_func_type>::value, "Function signatures don't match" );
    x_constexprvar bool value = true;
};

template< typename T_LAMBDA, typename T_FUNCTION_TYPE >
struct xfunction_is_lambda_signature_same
{
    x_constexprvar bool value = std::is_same
    < 
        typename xfunction_traits< typename std::remove_reference<T_LAMBDA>::type >::t_func_type, 
        T_FUNCTION_TYPE
    >::value; 
};

//---------------------------------------------------------------------------------------

template< typename T_FUNCTION >
struct xreturn_type;

template< typename T_RETURN, typename... T_ARGS >
struct xreturn_type<T_RETURN (*)( T_ARGS... )>
{
    using type = T_RETURN;
};

//---------------------------------------------------------------------------------------

template <typename T_FUNCTION >
struct xis_funtion_ptr : std::integral_constant
<
    bool, 
    std::is_pointer<T_FUNCTION>::value && std::is_function< typename std::remove_pointer<T_FUNCTION>::type >::value
>
{};

//---------------------------------------------------------------------------------------

#define X_STATIC_LAMBDA_BEGIN   [&]() { struct static_lambda { static auto StaticLambda 
#define X_STATIC_LAMBDA_END     }; return &static_lambda::StaticLambda; }()
#define X_STATIC_LAMBDA( ... ) X_STATIC_LAMBDA_BEGIN  __VA_ARGS__  X_STATIC_LAMBDA_END

//------------------------------------------------------------------------------
// Description:
//      Replacement for std::invoke
//      Seems like invoke is not fully standard and some compiler such clang does
//      have it.
//------------------------------------------------------------------------------
#if _X_TARGET_WINDOWS
#include<type_traits>
    #define xinvoke  std::invoke
#else
    namespace x_function_detail
    {
        template <class F, class... Args> x_forceinline
        auto invoke(F&& f, Args&&... args) ->
        decltype(std::forward<F>(f)(std::forward<Args>(args)...))
        {
            return std::forward<F>(f)(std::forward<Args>(args)...);
        }
    
        template <class Base, class T, class Derived> x_forceinline
        auto invoke(T Base::*pmd, Derived&& ref) ->
        decltype(std::forward<Derived>(ref).*pmd)
        {
            return std::forward<Derived>(ref).*pmd;
        }
    
        template <class PMD, class Pointer> x_forceinline
        auto invoke(PMD pmd, Pointer&& ptr) ->
        decltype((*std::forward<Pointer>(ptr)).*pmd)
        {
            return (*std::forward<Pointer>(ptr)).*pmd;
        }
    
        template <class Base, class T, class Derived, class... Args> x_forceinline
        auto invoke(T Base::*pmf, Derived&& ref, Args&&... args) ->
        decltype((std::forward<Derived>(ref).*pmf)(std::forward<Args>(args)...))
        {
            return (std::forward<Derived>(ref).*pmf)(std::forward<Args>(args)...);
        }
    
        template <class PMF, class Pointer, class... Args> x_forceinline
        auto invoke(PMF pmf, Pointer&& ptr, Args&&... args) ->
        decltype(((*std::forward<Pointer>(ptr)).*pmf)(std::forward<Args>(args)...))
        {
            return ((*std::forward<Pointer>(ptr)).*pmf)(std::forward<Args>(args)...);
        }
    } // namespace detail

    template< class F, class... ArgTypes> x_forceinline
    decltype(auto) xinvoke(F&& f, ArgTypes&&... args)
    {
        return x_function_detail::invoke(std::forward<F>(f), std::forward<ArgTypes>(args)...);
    }
#endif

//------------------------------------------------------------------------------
// Description:
//      Replacement for std::function
//      This version should inline much more aggressively than the std::function
//------------------------------------------------------------------------------
template< typename T_LAMBDA > class xfunction;
template< typename T_RET, typename ... T_ARG >
class xfunction< T_RET( T_ARG ... ) > final
{
    x_object_type( xfunction, is_linear );
    
public:
    
    constexpr xfunction( void ) noexcept = default;
    
    template< typename T_LAMBDA >
    x_forceconst xfunction( const T_LAMBDA& Lambda ) noexcept : m_Invoker( &xfunctor<T_LAMBDA>::Invoke )
    {
        // Make sure that the lambda given by the user matches the expected signature
        using fa = xfunction_traits< typename std::remove_reference<T_LAMBDA>::type >;
        using fb = xfunction_traits< typename x_make_function<T_RET, T_ARG ... >::type >;
        static_assert( xfunction_traits_compare<fa,fb>::value, "" );
        
        // If this assert kicks in then our storage may be too small
        static_assert( sizeof( m_Storage ) >= sizeof( xfunctor<T_LAMBDA> ), "Storage Size for xfunction is too small, increase m_Storage if you have to");
        static_assert( sizeof(*this) == sizeof(void*)*8, "The storage if this class may need tweaking since it looks like is waste full" );
        
        // Call the constructor
        new( m_Storage ) xfunctor<T_LAMBDA>( Lambda );
    }
    
    x_forceconst T_RET operator()( T_ARG ... Args ) const  noexcept
    {
        return m_Invoker( m_Storage, std::forward<T_ARG>(Args)... );
    }
    
protected:
    
    template<typename T_LAMBDA>
    class xfunctor
    {
    public:
        x_forceconst                xfunctor    ( const T_LAMBDA& xfunctor ) noexcept : m_pFunctor( xfunctor ) { }
        x_forceconst static T_RET   Invoke      ( const void* pStorage, T_ARG ... Args ) noexcept
        {
            return reinterpret_cast< const xfunctor<T_LAMBDA>* >( pStorage )->m_pFunctor( std::forward<T_ARG>(Args)... );
        }
    protected:
        const T_LAMBDA  m_pFunctor;
    };
    
    using function_invoker = T_RET(*)( const void*, T_ARG ... );
    
protected:
    
    function_invoker  m_Invoker     {};     // Pointer to the Invoke function ones all the t_type magic has happen
    void*             m_Storage[7]  {};     // Keep the data used to call the function
};

//------------------------------------------------------------------------------
// Description:
//      This function is somewhat similar to std::function except stead of owning
//      the memory reserved for the lambda structure it is require for the user
//      to pass the correct memory in the constructor.
// Example:
//    struct local_message : queue::node
//    {
//        template< typename T >
//        x_forceinline local_message( xbyte& Data, std::size_t Size, T&& Lambda ) noexcept : m_Callback{ Data, Size, std::forward<T>(Lambda) } {}
//        const xfunction_withptr<void(void)>   m_Callback;
//    };
//
//    template< typename T >
//    struct local_message_dynamic_sized : local_message
//    {
//        x_forceinline local_message_dynamic_sized( T&& Lambda ) noexcept : local_message{ m_Data[0], sizeof(T), std::forward<T>(Lambda) }{}
//        alignas( alignof(T) ) xbyte m_Data[sizeof(T)];
//    };
//
//    template< typename T >
//    x_inline        void    SendMsg                 ( T&& Callback ) noexcept
//    {
//        // Append the message in the queue to be consumed later
//        using data = local_message_dynamic_sized<decltype(Callback)>;
//        Push( m_MsgAllocPool.x_ll_circular_pool::New<data>( std::forward<T>(Callback) ) );
//    }
//------------------------------------------------------------------------------
template< typename T_LAMBDA > class xfunction_withptr;
template< typename T_RET, typename ... T_ARG >
class xfunction_withptr< T_RET( T_ARG ... ) > final
{
public:
    
    constexpr xfunction_withptr( void ) noexcept = default;
    
    template< typename T_LAMBDA >
    x_forceconst xfunction_withptr( xbyte& Data, std::size_t Size, const T_LAMBDA& Lambda ) noexcept :
    m_Data      { *reinterpret_cast<xbyte*>(new( &Data ) xfunctor<T_LAMBDA>( Lambda )) },
    m_Invoker   { &xfunctor<T_LAMBDA>::Invoke }
    {
        x_assert( Size >= sizeof( xfunctor<T_LAMBDA> ) );
        x_assert( &Data == &m_Data );
        
        // Make sure that the function has the right type
        using fa = xfunction_traits< typename std::remove_reference<T_LAMBDA>::type >;
        using fb = xfunction_traits< typename x_make_function<T_RET, T_ARG ... >::type >;
        static_assert( xfunction_traits_compare<fa,fb>::value, "" );
    }
    
    x_forceconst T_RET operator()( T_ARG ... Args ) const  noexcept
    {
        return m_Invoker( m_Data, std::forward<T_ARG>(Args)... );
    }
    
protected:
    
    template<typename T_LAMBDA>
    class xfunctor
    {
    public:
        x_forceconst                xfunctor    ( const T_LAMBDA& xfunctor ) noexcept : m_pFunctor( xfunctor ) { }
        x_forceconst static T_RET   Invoke      ( const xbyte& Storage, T_ARG ... Args ) noexcept
        {
            return reinterpret_cast< const xfunctor<T_LAMBDA>* >( &Storage )->m_pFunctor( std::forward<T_ARG>(Args)... );
        }
    protected:
        const T_LAMBDA  m_pFunctor;
    };
    
    using function_invoker = T_RET(*)( const xbyte&, T_ARG ... );
    
protected:
    
    function_invoker  m_Invoker    {};      // Pointer to the Invoke function ones all the t_type magic has happen
    const xbyte&      m_Data;               // Keep the data used to call the function
};

//------------------------------------------------------------------------------
// Description:
//      Function view is very similar to xfunction except this is used when the
//      lambda is going to be call in the same context. In other words when the
//      lambda is short lived (no pass the current context). View as always are
//      generally faster than owners.
// Reference:
//      https://vittorioromeo.info/index.html
//------------------------------------------------------------------------------
template< typename T_LAMBDA > class xfunction_view;
template< typename T_RET, typename ... T_ARG >
class xfunction_view< T_RET( T_ARG ... ) > final
{
public:
    
    constexpr xfunction_view( void ) noexcept = default;
    
    template< typename T_LAMBDA >
    x_forceconst xfunction_view( T_LAMBDA&& Lambda ) noexcept : m_pLambdaRef( reinterpret_cast<xbyte*>( &Lambda ) )
    {
        // Make sure that the lambda given by the user matches the expected signature
        using fa = xfunction_traits< typename std::remove_reference<T_LAMBDA>::type >;
        using fb = xfunction_traits< typename x_make_function<T_RET, T_ARG ... >::type >;
        static_assert( xfunction_traits_compare<fa,fb>::value, "" );
        
        // static_assert( std::is_same< fa, fb::type >::value, "Wrong signature" );
        m_Invoker = []( xbyte* pLambda, T_ARG... Args ) -> T_RET
        {
            return xinvoke( reinterpret_cast<T_LAMBDA&>(*pLambda), std::forward<T_ARG>(Args)... );
        };
    }
    
    x_forceconst T_RET operator()( T_ARG... Args ) const noexcept
    {
        return m_Invoker( m_pLambdaRef, std::forward<T_ARG>(Args)... );
    }
    
protected:
    
    using fn_ptr_type     = T_RET(*)( xbyte*, T_ARG ... );
    
    xbyte*             m_pLambdaRef    {};
    fn_ptr_type        m_Invoker       {};
};


template<typename T, typename = void>
struct xis_callable : std::is_function<T> { };

template<typename T>
struct xis_callable<T, typename std::enable_if<
    std::is_same<decltype(void(&T::operator())), void>::value
    >::type> : std::true_type { };


